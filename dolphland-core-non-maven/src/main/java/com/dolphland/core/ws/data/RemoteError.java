
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour RemoteError.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="RemoteError">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ERR_DOLPHLAND_000_UNEXPECTED_ERROR"/>
 *     &lt;enumeration value="ERR_DOLPHLAND_001_NOT_EXISTED_USER"/>
 *     &lt;enumeration value="ERR_DOLPHLAND_002_BAD_USER_PASSWORD"/>
 *     &lt;enumeration value="ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY"/>
 *     &lt;enumeration value="ERR_CHESS_001_USER_ALREADY_PLAYER"/>
 *     &lt;enumeration value="ERR_CHESS_002_PLAYERS_NO_CHANGE"/>
 *     &lt;enumeration value="ERR_CHESS_003_PLAYER_ALREADY_TAKEN"/>
 *     &lt;enumeration value="ERR_CHESS_004_MOVEMENT_FORBIDDEN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RemoteError")
@XmlEnum
public enum RemoteError {

    ERR_DOLPHLAND_000_UNEXPECTED_ERROR,
    ERR_DOLPHLAND_001_NOT_EXISTED_USER,
    ERR_DOLPHLAND_002_BAD_USER_PASSWORD,
    ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY,
    ERR_CHESS_001_USER_ALREADY_PLAYER,
    ERR_CHESS_002_PLAYERS_NO_CHANGE,
    ERR_CHESS_003_PLAYER_ALREADY_TAKEN,
    ERR_CHESS_004_MOVEMENT_FORBIDDEN;

    public String value() {
        return name();
    }

    public static RemoteError fromValue(String v) {
        return valueOf(v);
    }

}
