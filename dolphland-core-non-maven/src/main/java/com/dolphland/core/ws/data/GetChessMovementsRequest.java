
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="partyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="piece" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPiece"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clientId",
    "partyId",
    "piece"
})
@XmlRootElement(name = "GetChessMovementsRequest")
public class GetChessMovementsRequest {

    @XmlElement(required = true)
    protected String clientId;
    @XmlElement(required = true)
    protected String partyId;
    @XmlElement(required = true)
    protected ChessPiece piece;

    /**
     * Obtient la valeur de la propri�t� clientId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * D�finit la valeur de la propri�t� clientId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Obtient la valeur de la propri�t� partyId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyId() {
        return partyId;
    }

    /**
     * D�finit la valeur de la propri�t� partyId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyId(String value) {
        this.partyId = value;
    }

    /**
     * Obtient la valeur de la propri�t� piece.
     * 
     * @return
     *     possible object is
     *     {@link ChessPiece }
     *     
     */
    public ChessPiece getPiece() {
        return piece;
    }

    /**
     * D�finit la valeur de la propri�t� piece.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessPiece }
     *     
     */
    public void setPiece(ChessPiece value) {
        this.piece = value;
    }

}
