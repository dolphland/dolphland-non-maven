
package com.dolphland.core.ws.data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessMovement complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChessMovement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="squares" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessSquare" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChessMovement", propOrder = {
    "squares"
})
public class ChessMovement {

    @XmlElement(required = true)
    protected List<ChessSquare> squares;

    /**
     * Gets the value of the squares property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the squares property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSquares().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChessSquare }
     * 
     * 
     */
    public List<ChessSquare> getSquares() {
        if (squares == null) {
            squares = new ArrayList<ChessSquare>();
        }
        return this.squares;
    }

}
