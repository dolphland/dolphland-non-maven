
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessPlayerState.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ChessPlayerState">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NORMAL"/>
 *     &lt;enumeration value="CHESS"/>
 *     &lt;enumeration value="MAT"/>
 *     &lt;enumeration value="PAT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChessPlayerState")
@XmlEnum
public enum ChessPlayerState {

    NORMAL,
    CHESS,
    MAT,
    PAT;

    public String value() {
        return name();
    }

    public static ChessPlayerState fromValue(String v) {
        return valueOf(v);
    }

}
