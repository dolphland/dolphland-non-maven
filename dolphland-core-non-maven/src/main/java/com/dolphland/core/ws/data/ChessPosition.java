
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessPosition complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChessPosition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="row" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessRow"/>
 *         &lt;element name="column" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessColumn"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChessPosition", propOrder = {
    "row",
    "column"
})
public class ChessPosition {

    @XmlElement(required = true)
    protected ChessRow row;
    @XmlElement(required = true)
    protected ChessColumn column;

    /**
     * Obtient la valeur de la propri�t� row.
     * 
     * @return
     *     possible object is
     *     {@link ChessRow }
     *     
     */
    public ChessRow getRow() {
        return row;
    }

    /**
     * D�finit la valeur de la propri�t� row.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessRow }
     *     
     */
    public void setRow(ChessRow value) {
        this.row = value;
    }

    /**
     * Obtient la valeur de la propri�t� column.
     * 
     * @return
     *     possible object is
     *     {@link ChessColumn }
     *     
     */
    public ChessColumn getColumn() {
        return column;
    }

    /**
     * D�finit la valeur de la propri�t� column.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessColumn }
     *     
     */
    public void setColumn(ChessColumn value) {
        this.column = value;
    }

}
