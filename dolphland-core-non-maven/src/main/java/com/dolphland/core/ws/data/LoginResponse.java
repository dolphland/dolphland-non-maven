
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="error" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}RemoteError"/>
 *         &lt;element name="user" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}User"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "error",
    "user"
})
@XmlRootElement(name = "LoginResponse")
public class LoginResponse {

    @XmlElement(required = true)
    protected RemoteError error;
    @XmlElement(required = true)
    protected User user;

    /**
     * Obtient la valeur de la propri�t� error.
     * 
     * @return
     *     possible object is
     *     {@link RemoteError }
     *     
     */
    public RemoteError getError() {
        return error;
    }

    /**
     * D�finit la valeur de la propri�t� error.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoteError }
     *     
     */
    public void setError(RemoteError value) {
        this.error = value;
    }

    /**
     * Obtient la valeur de la propri�t� user.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getUser() {
        return user;
    }

    /**
     * D�finit la valeur de la propri�t� user.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setUser(User value) {
        this.user = value;
    }

}
