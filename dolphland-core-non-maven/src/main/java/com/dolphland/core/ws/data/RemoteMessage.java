
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour RemoteMessage complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="RemoteMessage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="type" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}RemoteMessageType"/>
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="senderUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="receiverUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="partyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteMessage", propOrder = {
    "type",
    "content",
    "senderUserId",
    "receiverUserId",
    "partyId"
})
public class RemoteMessage {

    @XmlElement(required = true)
    protected RemoteMessageType type;
    @XmlElement(required = true)
    protected String content;
    @XmlElement(required = true)
    protected String senderUserId;
    @XmlElement(required = true)
    protected String receiverUserId;
    @XmlElement(required = true)
    protected String partyId;

    /**
     * Obtient la valeur de la propri�t� type.
     * 
     * @return
     *     possible object is
     *     {@link RemoteMessageType }
     *     
     */
    public RemoteMessageType getType() {
        return type;
    }

    /**
     * D�finit la valeur de la propri�t� type.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoteMessageType }
     *     
     */
    public void setType(RemoteMessageType value) {
        this.type = value;
    }

    /**
     * Obtient la valeur de la propri�t� content.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * D�finit la valeur de la propri�t� content.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

    /**
     * Obtient la valeur de la propri�t� senderUserId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderUserId() {
        return senderUserId;
    }

    /**
     * D�finit la valeur de la propri�t� senderUserId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderUserId(String value) {
        this.senderUserId = value;
    }

    /**
     * Obtient la valeur de la propri�t� receiverUserId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiverUserId() {
        return receiverUserId;
    }

    /**
     * D�finit la valeur de la propri�t� receiverUserId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiverUserId(String value) {
        this.receiverUserId = value;
    }

    /**
     * Obtient la valeur de la propri�t� partyId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyId() {
        return partyId;
    }

    /**
     * D�finit la valeur de la propri�t� partyId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyId(String value) {
        this.partyId = value;
    }

}
