
package com.dolphland.core.ws.data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour Party complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="Party">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="applicationId" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ApplicationId"/>
 *         &lt;element name="ownerUserId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ranked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="finished" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="paused" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="membersUsersIds" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Party", propOrder = {
    "id",
    "applicationId",
    "ownerUserId",
    "ranked",
    "finished",
    "paused",
    "membersUsersIds"
})
public class Party {

    @XmlElement(required = true)
    protected String id;
    @XmlElement(required = true)
    protected ApplicationId applicationId;
    @XmlElement(required = true)
    protected String ownerUserId;
    protected boolean ranked;
    protected boolean finished;
    protected boolean paused;
    @XmlElement(required = true)
    protected List<String> membersUsersIds;

    /**
     * Obtient la valeur de la propri�t� id.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * D�finit la valeur de la propri�t� id.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Obtient la valeur de la propri�t� applicationId.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationId }
     *     
     */
    public ApplicationId getApplicationId() {
        return applicationId;
    }

    /**
     * D�finit la valeur de la propri�t� applicationId.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationId }
     *     
     */
    public void setApplicationId(ApplicationId value) {
        this.applicationId = value;
    }

    /**
     * Obtient la valeur de la propri�t� ownerUserId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerUserId() {
        return ownerUserId;
    }

    /**
     * D�finit la valeur de la propri�t� ownerUserId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerUserId(String value) {
        this.ownerUserId = value;
    }

    /**
     * Obtient la valeur de la propri�t� ranked.
     * 
     */
    public boolean isRanked() {
        return ranked;
    }

    /**
     * D�finit la valeur de la propri�t� ranked.
     * 
     */
    public void setRanked(boolean value) {
        this.ranked = value;
    }

    /**
     * Obtient la valeur de la propri�t� finished.
     * 
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * D�finit la valeur de la propri�t� finished.
     * 
     */
    public void setFinished(boolean value) {
        this.finished = value;
    }

    /**
     * Obtient la valeur de la propri�t� paused.
     * 
     */
    public boolean isPaused() {
        return paused;
    }

    /**
     * D�finit la valeur de la propri�t� paused.
     * 
     */
    public void setPaused(boolean value) {
        this.paused = value;
    }

    /**
     * Gets the value of the membersUsersIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the membersUsersIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMembersUsersIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMembersUsersIds() {
        if (membersUsersIds == null) {
            membersUsersIds = new ArrayList<String>();
        }
        return this.membersUsersIds;
    }

}
