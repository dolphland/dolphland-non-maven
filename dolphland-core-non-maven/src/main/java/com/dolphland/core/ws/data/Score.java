
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour Score complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="Score">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="applicationId" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ApplicationId"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="win" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lost" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Score", propOrder = {
    "applicationId",
    "userId",
    "win",
    "lost"
})
public class Score {

    @XmlElement(required = true)
    protected ApplicationId applicationId;
    @XmlElement(required = true)
    protected String userId;
    protected int win;
    protected int lost;

    /**
     * Obtient la valeur de la propri�t� applicationId.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationId }
     *     
     */
    public ApplicationId getApplicationId() {
        return applicationId;
    }

    /**
     * D�finit la valeur de la propri�t� applicationId.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationId }
     *     
     */
    public void setApplicationId(ApplicationId value) {
        this.applicationId = value;
    }

    /**
     * Obtient la valeur de la propri�t� userId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * D�finit la valeur de la propri�t� userId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Obtient la valeur de la propri�t� win.
     * 
     */
    public int getWin() {
        return win;
    }

    /**
     * D�finit la valeur de la propri�t� win.
     * 
     */
    public void setWin(int value) {
        this.win = value;
    }

    /**
     * Obtient la valeur de la propri�t� lost.
     * 
     */
    public int getLost() {
        return lost;
    }

    /**
     * D�finit la valeur de la propri�t� lost.
     * 
     */
    public void setLost(int value) {
        this.lost = value;
    }

}
