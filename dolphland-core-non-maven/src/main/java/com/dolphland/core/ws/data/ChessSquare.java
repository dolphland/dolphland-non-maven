
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessSquare complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChessSquare">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="position" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPosition"/>
 *         &lt;element name="color" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessColor"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChessSquare", propOrder = {
    "position",
    "color"
})
public class ChessSquare {

    @XmlElement(required = true)
    protected ChessPosition position;
    @XmlElement(required = true)
    protected ChessColor color;

    /**
     * Obtient la valeur de la propri�t� position.
     * 
     * @return
     *     possible object is
     *     {@link ChessPosition }
     *     
     */
    public ChessPosition getPosition() {
        return position;
    }

    /**
     * D�finit la valeur de la propri�t� position.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessPosition }
     *     
     */
    public void setPosition(ChessPosition value) {
        this.position = value;
    }

    /**
     * Obtient la valeur de la propri�t� color.
     * 
     * @return
     *     possible object is
     *     {@link ChessColor }
     *     
     */
    public ChessColor getColor() {
        return color;
    }

    /**
     * D�finit la valeur de la propri�t� color.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessColor }
     *     
     */
    public void setColor(ChessColor value) {
        this.color = value;
    }

}
