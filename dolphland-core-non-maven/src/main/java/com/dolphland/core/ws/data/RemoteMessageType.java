
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour RemoteMessageType.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="RemoteMessageType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CHAT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RemoteMessageType")
@XmlEnum
public enum RemoteMessageType {

    CHAT;

    public String value() {
        return name();
    }

    public static RemoteMessageType fromValue(String v) {
        return valueOf(v);
    }

}
