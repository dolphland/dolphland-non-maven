
package com.dolphland.core.ws.data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessBoard complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChessBoard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="players" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPlayer" maxOccurs="2"/>
 *         &lt;element name="squares" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessSquare" maxOccurs="64" minOccurs="64"/>
 *         &lt;element name="pieces" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPiece" maxOccurs="unbounded"/>
 *         &lt;element name="defeatedPieces" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPiece" maxOccurs="unbounded"/>
 *         &lt;element name="currentColor" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessColor"/>
 *         &lt;element name="lastMovement" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessMovement"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChessBoard", propOrder = {
    "players",
    "squares",
    "pieces",
    "defeatedPieces",
    "currentColor",
    "lastMovement"
})
public class ChessBoard {

    @XmlElement(required = true)
    protected List<ChessPlayer> players;
    @XmlElement(required = true)
    protected List<ChessSquare> squares;
    @XmlElement(required = true)
    protected List<ChessPiece> pieces;
    @XmlElement(required = true)
    protected List<ChessPiece> defeatedPieces;
    @XmlElement(required = true)
    protected ChessColor currentColor;
    @XmlElement(required = true)
    protected ChessMovement lastMovement;

    /**
     * Gets the value of the players property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the players property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlayers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChessPlayer }
     * 
     * 
     */
    public List<ChessPlayer> getPlayers() {
        if (players == null) {
            players = new ArrayList<ChessPlayer>();
        }
        return this.players;
    }

    /**
     * Gets the value of the squares property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the squares property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSquares().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChessSquare }
     * 
     * 
     */
    public List<ChessSquare> getSquares() {
        if (squares == null) {
            squares = new ArrayList<ChessSquare>();
        }
        return this.squares;
    }

    /**
     * Gets the value of the pieces property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pieces property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPieces().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChessPiece }
     * 
     * 
     */
    public List<ChessPiece> getPieces() {
        if (pieces == null) {
            pieces = new ArrayList<ChessPiece>();
        }
        return this.pieces;
    }

    /**
     * Gets the value of the defeatedPieces property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the defeatedPieces property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDefeatedPieces().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChessPiece }
     * 
     * 
     */
    public List<ChessPiece> getDefeatedPieces() {
        if (defeatedPieces == null) {
            defeatedPieces = new ArrayList<ChessPiece>();
        }
        return this.defeatedPieces;
    }

    /**
     * Obtient la valeur de la propri�t� currentColor.
     * 
     * @return
     *     possible object is
     *     {@link ChessColor }
     *     
     */
    public ChessColor getCurrentColor() {
        return currentColor;
    }

    /**
     * D�finit la valeur de la propri�t� currentColor.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessColor }
     *     
     */
    public void setCurrentColor(ChessColor value) {
        this.currentColor = value;
    }

    /**
     * Obtient la valeur de la propri�t� lastMovement.
     * 
     * @return
     *     possible object is
     *     {@link ChessMovement }
     *     
     */
    public ChessMovement getLastMovement() {
        return lastMovement;
    }

    /**
     * D�finit la valeur de la propri�t� lastMovement.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessMovement }
     *     
     */
    public void setLastMovement(ChessMovement value) {
        this.lastMovement = value;
    }

}
