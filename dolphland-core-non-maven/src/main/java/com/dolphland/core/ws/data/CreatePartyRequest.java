
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="applicationId" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ApplicationId"/>
 *         &lt;element name="ranked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clientId",
    "applicationId",
    "ranked"
})
@XmlRootElement(name = "CreatePartyRequest")
public class CreatePartyRequest {

    @XmlElement(required = true)
    protected String clientId;
    @XmlElement(required = true)
    protected ApplicationId applicationId;
    protected boolean ranked;

    /**
     * Obtient la valeur de la propri�t� clientId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * D�finit la valeur de la propri�t� clientId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Obtient la valeur de la propri�t� applicationId.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationId }
     *     
     */
    public ApplicationId getApplicationId() {
        return applicationId;
    }

    /**
     * D�finit la valeur de la propri�t� applicationId.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationId }
     *     
     */
    public void setApplicationId(ApplicationId value) {
        this.applicationId = value;
    }

    /**
     * Obtient la valeur de la propri�t� ranked.
     * 
     */
    public boolean isRanked() {
        return ranked;
    }

    /**
     * D�finit la valeur de la propri�t� ranked.
     * 
     */
    public void setRanked(boolean value) {
        this.ranked = value;
    }

}
