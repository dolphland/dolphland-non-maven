
package com.dolphland.core.ws.data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="error" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}RemoteError"/>
 *         &lt;element name="partyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="movements" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessMovement" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "error",
    "partyId",
    "movements"
})
@XmlRootElement(name = "GetChessMovementsResponse")
public class GetChessMovementsResponse {

    @XmlElement(required = true)
    protected RemoteError error;
    @XmlElement(required = true)
    protected String partyId;
    protected List<ChessMovement> movements;

    /**
     * Obtient la valeur de la propri�t� error.
     * 
     * @return
     *     possible object is
     *     {@link RemoteError }
     *     
     */
    public RemoteError getError() {
        return error;
    }

    /**
     * D�finit la valeur de la propri�t� error.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoteError }
     *     
     */
    public void setError(RemoteError value) {
        this.error = value;
    }

    /**
     * Obtient la valeur de la propri�t� partyId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyId() {
        return partyId;
    }

    /**
     * D�finit la valeur de la propri�t� partyId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyId(String value) {
        this.partyId = value;
    }

    /**
     * Gets the value of the movements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the movements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMovements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChessMovement }
     * 
     * 
     */
    public List<ChessMovement> getMovements() {
        if (movements == null) {
            movements = new ArrayList<ChessMovement>();
        }
        return this.movements;
    }

}
