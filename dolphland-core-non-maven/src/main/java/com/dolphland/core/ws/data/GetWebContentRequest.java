
package com.dolphland.core.ws.data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="socketId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="browserRequest" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "clientId",
    "socketId",
    "browserRequest"
})
@XmlRootElement(name = "GetWebContentRequest")
public class GetWebContentRequest {

    @XmlElement(required = true)
    protected String clientId;
    @XmlElement(required = true)
    protected String socketId;
    @XmlElement(type = Integer.class)
    protected List<Integer> browserRequest;

    /**
     * Obtient la valeur de la propri�t� clientId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * D�finit la valeur de la propri�t� clientId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Obtient la valeur de la propri�t� socketId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocketId() {
        return socketId;
    }

    /**
     * D�finit la valeur de la propri�t� socketId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocketId(String value) {
        this.socketId = value;
    }

    /**
     * Gets the value of the browserRequest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the browserRequest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBrowserRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getBrowserRequest() {
        if (browserRequest == null) {
            browserRequest = new ArrayList<Integer>();
        }
        return this.browserRequest;
    }

}
