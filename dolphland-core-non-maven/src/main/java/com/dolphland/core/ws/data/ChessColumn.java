
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessColumn.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ChessColumn">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="COLUMN 1"/>
 *     &lt;enumeration value="COLUMN 2"/>
 *     &lt;enumeration value="COLUMN 3"/>
 *     &lt;enumeration value="COLUMN 4"/>
 *     &lt;enumeration value="COLUMN 5"/>
 *     &lt;enumeration value="COLUMN 6"/>
 *     &lt;enumeration value="COLUMN 7"/>
 *     &lt;enumeration value="COLUMN 8"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChessColumn")
@XmlEnum
public enum ChessColumn {

    @XmlEnumValue("COLUMN 1")
    COLUMN_1("COLUMN 1"),
    @XmlEnumValue("COLUMN 2")
    COLUMN_2("COLUMN 2"),
    @XmlEnumValue("COLUMN 3")
    COLUMN_3("COLUMN 3"),
    @XmlEnumValue("COLUMN 4")
    COLUMN_4("COLUMN 4"),
    @XmlEnumValue("COLUMN 5")
    COLUMN_5("COLUMN 5"),
    @XmlEnumValue("COLUMN 6")
    COLUMN_6("COLUMN 6"),
    @XmlEnumValue("COLUMN 7")
    COLUMN_7("COLUMN 7"),
    @XmlEnumValue("COLUMN 8")
    COLUMN_8("COLUMN 8");
    private final String value;

    ChessColumn(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChessColumn fromValue(String v) {
        for (ChessColumn c: ChessColumn.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
