
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessPiece complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChessPiece">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pieceId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="position" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPosition"/>
 *         &lt;element name="color" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessColor"/>
 *         &lt;element name="type" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPieceType"/>
 *         &lt;element name="moved" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChessPiece", propOrder = {
    "pieceId",
    "position",
    "color",
    "type",
    "moved"
})
public class ChessPiece {

    @XmlElement(required = true)
    protected String pieceId;
    @XmlElement(required = true)
    protected ChessPosition position;
    @XmlElement(required = true)
    protected ChessColor color;
    @XmlElement(required = true)
    protected ChessPieceType type;
    protected boolean moved;

    /**
     * Obtient la valeur de la propri�t� pieceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPieceId() {
        return pieceId;
    }

    /**
     * D�finit la valeur de la propri�t� pieceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPieceId(String value) {
        this.pieceId = value;
    }

    /**
     * Obtient la valeur de la propri�t� position.
     * 
     * @return
     *     possible object is
     *     {@link ChessPosition }
     *     
     */
    public ChessPosition getPosition() {
        return position;
    }

    /**
     * D�finit la valeur de la propri�t� position.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessPosition }
     *     
     */
    public void setPosition(ChessPosition value) {
        this.position = value;
    }

    /**
     * Obtient la valeur de la propri�t� color.
     * 
     * @return
     *     possible object is
     *     {@link ChessColor }
     *     
     */
    public ChessColor getColor() {
        return color;
    }

    /**
     * D�finit la valeur de la propri�t� color.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessColor }
     *     
     */
    public void setColor(ChessColor value) {
        this.color = value;
    }

    /**
     * Obtient la valeur de la propri�t� type.
     * 
     * @return
     *     possible object is
     *     {@link ChessPieceType }
     *     
     */
    public ChessPieceType getType() {
        return type;
    }

    /**
     * D�finit la valeur de la propri�t� type.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessPieceType }
     *     
     */
    public void setType(ChessPieceType value) {
        this.type = value;
    }

    /**
     * Obtient la valeur de la propri�t� moved.
     * 
     */
    public boolean isMoved() {
        return moved;
    }

    /**
     * D�finit la valeur de la propri�t� moved.
     * 
     */
    public void setMoved(boolean value) {
        this.moved = value;
    }

}
