
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ApplicationId.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ApplicationId">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DOLPHLAND"/>
 *     &lt;enumeration value="LOGIN"/>
 *     &lt;enumeration value="LOGOUT"/>
 *     &lt;enumeration value="CHESS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ApplicationId")
@XmlEnum
public enum ApplicationId {

    DOLPHLAND,
    LOGIN,
    LOGOUT,
    CHESS;

    public String value() {
        return name();
    }

    public static ApplicationId fromValue(String v) {
        return valueOf(v);
    }

}
