
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="error" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}RemoteError"/>
 *         &lt;element name="partyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="board" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessBoard"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "error",
    "partyId",
    "board"
})
@XmlRootElement(name = "SetChessPlayerResponse")
public class SetChessPlayerResponse {

    @XmlElement(required = true)
    protected RemoteError error;
    @XmlElement(required = true)
    protected String partyId;
    @XmlElement(required = true)
    protected ChessBoard board;

    /**
     * Obtient la valeur de la propri�t� error.
     * 
     * @return
     *     possible object is
     *     {@link RemoteError }
     *     
     */
    public RemoteError getError() {
        return error;
    }

    /**
     * D�finit la valeur de la propri�t� error.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoteError }
     *     
     */
    public void setError(RemoteError value) {
        this.error = value;
    }

    /**
     * Obtient la valeur de la propri�t� partyId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyId() {
        return partyId;
    }

    /**
     * D�finit la valeur de la propri�t� partyId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyId(String value) {
        this.partyId = value;
    }

    /**
     * Obtient la valeur de la propri�t� board.
     * 
     * @return
     *     possible object is
     *     {@link ChessBoard }
     *     
     */
    public ChessBoard getBoard() {
        return board;
    }

    /**
     * D�finit la valeur de la propri�t� board.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessBoard }
     *     
     */
    public void setBoard(ChessBoard value) {
        this.board = value;
    }

}
