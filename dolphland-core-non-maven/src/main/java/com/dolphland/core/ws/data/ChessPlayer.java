
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessPlayer complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ChessPlayer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="color" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessColor"/>
 *         &lt;element name="state" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}ChessPlayerState"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChessPlayer", propOrder = {
    "userId",
    "color",
    "state"
})
public class ChessPlayer {

    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected ChessColor color;
    @XmlElement(required = true)
    protected ChessPlayerState state;

    /**
     * Obtient la valeur de la propri�t� userId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * D�finit la valeur de la propri�t� userId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Obtient la valeur de la propri�t� color.
     * 
     * @return
     *     possible object is
     *     {@link ChessColor }
     *     
     */
    public ChessColor getColor() {
        return color;
    }

    /**
     * D�finit la valeur de la propri�t� color.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessColor }
     *     
     */
    public void setColor(ChessColor value) {
        this.color = value;
    }

    /**
     * Obtient la valeur de la propri�t� state.
     * 
     * @return
     *     possible object is
     *     {@link ChessPlayerState }
     *     
     */
    public ChessPlayerState getState() {
        return state;
    }

    /**
     * D�finit la valeur de la propri�t� state.
     * 
     * @param value
     *     allowed object is
     *     {@link ChessPlayerState }
     *     
     */
    public void setState(ChessPlayerState value) {
        this.state = value;
    }

}
