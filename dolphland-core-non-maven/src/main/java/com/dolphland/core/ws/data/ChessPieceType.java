
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessPieceType.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ChessPieceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PAWN"/>
 *     &lt;enumeration value="BISHOP"/>
 *     &lt;enumeration value="KNIGHT"/>
 *     &lt;enumeration value="ROOK"/>
 *     &lt;enumeration value="QUEEN"/>
 *     &lt;enumeration value="KING"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChessPieceType")
@XmlEnum
public enum ChessPieceType {

    PAWN,
    BISHOP,
    KNIGHT,
    ROOK,
    QUEEN,
    KING;

    public String value() {
        return name();
    }

    public static ChessPieceType fromValue(String v) {
        return valueOf(v);
    }

}
