
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.dolphland.core.ws.data package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.dolphland.core.ws.data
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetUsersResponse }
     * 
     */
    public GetUsersResponse createGetUsersResponse() {
        return new GetUsersResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link RelayerResponse }
     * 
     */
    public RelayerResponse createRelayerResponse() {
        return new RelayerResponse();
    }

    /**
     * Create an instance of {@link RemoteEvent }
     * 
     */
    public RemoteEvent createRemoteEvent() {
        return new RemoteEvent();
    }

    /**
     * Create an instance of {@link RemoteMessage }
     * 
     */
    public RemoteMessage createRemoteMessage() {
        return new RemoteMessage();
    }

    /**
     * Create an instance of {@link SetChessPlayerResponse }
     * 
     */
    public SetChessPlayerResponse createSetChessPlayerResponse() {
        return new SetChessPlayerResponse();
    }

    /**
     * Create an instance of {@link ChessBoard }
     * 
     */
    public ChessBoard createChessBoard() {
        return new ChessBoard();
    }

    /**
     * Create an instance of {@link CreatePartyResponse }
     * 
     */
    public CreatePartyResponse createCreatePartyResponse() {
        return new CreatePartyResponse();
    }

    /**
     * Create an instance of {@link Party }
     * 
     */
    public Party createParty() {
        return new Party();
    }

    /**
     * Create an instance of {@link HelloRequest }
     * 
     */
    public HelloRequest createHelloRequest() {
        return new HelloRequest();
    }

    /**
     * Create an instance of {@link RelayerRequest }
     * 
     */
    public RelayerRequest createRelayerRequest() {
        return new RelayerRequest();
    }

    /**
     * Create an instance of {@link GetWebContentRequest }
     * 
     */
    public GetWebContentRequest createGetWebContentRequest() {
        return new GetWebContentRequest();
    }

    /**
     * Create an instance of {@link LoginRequest }
     * 
     */
    public LoginRequest createLoginRequest() {
        return new LoginRequest();
    }

    /**
     * Create an instance of {@link MoveChessPieceResponse }
     * 
     */
    public MoveChessPieceResponse createMoveChessPieceResponse() {
        return new MoveChessPieceResponse();
    }

    /**
     * Create an instance of {@link GetApplicationsRequest }
     * 
     */
    public GetApplicationsRequest createGetApplicationsRequest() {
        return new GetApplicationsRequest();
    }

    /**
     * Create an instance of {@link CreatePartyRequest }
     * 
     */
    public CreatePartyRequest createCreatePartyRequest() {
        return new CreatePartyRequest();
    }

    /**
     * Create an instance of {@link SendMessageRequest }
     * 
     */
    public SendMessageRequest createSendMessageRequest() {
        return new SendMessageRequest();
    }

    /**
     * Create an instance of {@link SendMessageResponse }
     * 
     */
    public SendMessageResponse createSendMessageResponse() {
        return new SendMessageResponse();
    }

    /**
     * Create an instance of {@link SetChessPlayerRequest }
     * 
     */
    public SetChessPlayerRequest createSetChessPlayerRequest() {
        return new SetChessPlayerRequest();
    }

    /**
     * Create an instance of {@link GetPartiesResponse }
     * 
     */
    public GetPartiesResponse createGetPartiesResponse() {
        return new GetPartiesResponse();
    }

    /**
     * Create an instance of {@link GetPartiesRequest }
     * 
     */
    public GetPartiesRequest createGetPartiesRequest() {
        return new GetPartiesRequest();
    }

    /**
     * Create an instance of {@link GetWebContentResponse }
     * 
     */
    public GetWebContentResponse createGetWebContentResponse() {
        return new GetWebContentResponse();
    }

    /**
     * Create an instance of {@link AbortPartyResponse }
     * 
     */
    public AbortPartyResponse createAbortPartyResponse() {
        return new AbortPartyResponse();
    }

    /**
     * Create an instance of {@link LogoutRequest }
     * 
     */
    public LogoutRequest createLogoutRequest() {
        return new LogoutRequest();
    }

    /**
     * Create an instance of {@link JoinPartyRequest }
     * 
     */
    public JoinPartyRequest createJoinPartyRequest() {
        return new JoinPartyRequest();
    }

    /**
     * Create an instance of {@link MoveChessPieceRequest }
     * 
     */
    public MoveChessPieceRequest createMoveChessPieceRequest() {
        return new MoveChessPieceRequest();
    }

    /**
     * Create an instance of {@link ChessPiece }
     * 
     */
    public ChessPiece createChessPiece() {
        return new ChessPiece();
    }

    /**
     * Create an instance of {@link ChessPosition }
     * 
     */
    public ChessPosition createChessPosition() {
        return new ChessPosition();
    }

    /**
     * Create an instance of {@link GetChessBoardResponse }
     * 
     */
    public GetChessBoardResponse createGetChessBoardResponse() {
        return new GetChessBoardResponse();
    }

    /**
     * Create an instance of {@link LeavePartyRequest }
     * 
     */
    public LeavePartyRequest createLeavePartyRequest() {
        return new LeavePartyRequest();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link GetChessBoardRequest }
     * 
     */
    public GetChessBoardRequest createGetChessBoardRequest() {
        return new GetChessBoardRequest();
    }

    /**
     * Create an instance of {@link GetChessMovementsRequest }
     * 
     */
    public GetChessMovementsRequest createGetChessMovementsRequest() {
        return new GetChessMovementsRequest();
    }

    /**
     * Create an instance of {@link AbortPartyRequest }
     * 
     */
    public AbortPartyRequest createAbortPartyRequest() {
        return new AbortPartyRequest();
    }

    /**
     * Create an instance of {@link GetUsersRequest }
     * 
     */
    public GetUsersRequest createGetUsersRequest() {
        return new GetUsersRequest();
    }

    /**
     * Create an instance of {@link JoinPartyResponse }
     * 
     */
    public JoinPartyResponse createJoinPartyResponse() {
        return new JoinPartyResponse();
    }

    /**
     * Create an instance of {@link GetApplicationsResponse }
     * 
     */
    public GetApplicationsResponse createGetApplicationsResponse() {
        return new GetApplicationsResponse();
    }

    /**
     * Create an instance of {@link Application }
     * 
     */
    public Application createApplication() {
        return new Application();
    }

    /**
     * Create an instance of {@link LeavePartyResponse }
     * 
     */
    public LeavePartyResponse createLeavePartyResponse() {
        return new LeavePartyResponse();
    }

    /**
     * Create an instance of {@link HelloResponse }
     * 
     */
    public HelloResponse createHelloResponse() {
        return new HelloResponse();
    }

    /**
     * Create an instance of {@link GetChessMovementsResponse }
     * 
     */
    public GetChessMovementsResponse createGetChessMovementsResponse() {
        return new GetChessMovementsResponse();
    }

    /**
     * Create an instance of {@link ChessMovement }
     * 
     */
    public ChessMovement createChessMovement() {
        return new ChessMovement();
    }

    /**
     * Create an instance of {@link ChessPlayer }
     * 
     */
    public ChessPlayer createChessPlayer() {
        return new ChessPlayer();
    }

    /**
     * Create an instance of {@link ChessSquare }
     * 
     */
    public ChessSquare createChessSquare() {
        return new ChessSquare();
    }

    /**
     * Create an instance of {@link Score }
     * 
     */
    public Score createScore() {
        return new Score();
    }

}
