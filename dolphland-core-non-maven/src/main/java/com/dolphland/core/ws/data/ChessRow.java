
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ChessRow.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ChessRow">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ROW 1"/>
 *     &lt;enumeration value="ROW 2"/>
 *     &lt;enumeration value="ROW 3"/>
 *     &lt;enumeration value="ROW 4"/>
 *     &lt;enumeration value="ROW 5"/>
 *     &lt;enumeration value="ROW 6"/>
 *     &lt;enumeration value="ROW 7"/>
 *     &lt;enumeration value="ROW 8"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ChessRow")
@XmlEnum
public enum ChessRow {

    @XmlEnumValue("ROW 1")
    ROW_1("ROW 1"),
    @XmlEnumValue("ROW 2")
    ROW_2("ROW 2"),
    @XmlEnumValue("ROW 3")
    ROW_3("ROW 3"),
    @XmlEnumValue("ROW 4")
    ROW_4("ROW 4"),
    @XmlEnumValue("ROW 5")
    ROW_5("ROW 5"),
    @XmlEnumValue("ROW 6")
    ROW_6("ROW 6"),
    @XmlEnumValue("ROW 7")
    ROW_7("ROW 7"),
    @XmlEnumValue("ROW 8")
    ROW_8("ROW 8");
    private final String value;

    ChessRow(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ChessRow fromValue(String v) {
        for (ChessRow c: ChessRow.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
