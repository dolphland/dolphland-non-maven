
package com.dolphland.core.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour RemoteEvent complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="RemoteEvent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="type" type="{http://www.noussommestousdesdauphins.com/dolphland/ws/definitions}RemoteEventType"/>
 *         &lt;element name="sourceId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="targetId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteEvent", propOrder = {
    "type",
    "sourceId",
    "targetId"
})
public class RemoteEvent {

    @XmlElement(required = true)
    protected RemoteEventType type;
    @XmlElement(required = true)
    protected String sourceId;
    @XmlElement(required = true)
    protected String targetId;

    /**
     * Obtient la valeur de la propri�t� type.
     * 
     * @return
     *     possible object is
     *     {@link RemoteEventType }
     *     
     */
    public RemoteEventType getType() {
        return type;
    }

    /**
     * D�finit la valeur de la propri�t� type.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoteEventType }
     *     
     */
    public void setType(RemoteEventType value) {
        this.type = value;
    }

    /**
     * Obtient la valeur de la propri�t� sourceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * D�finit la valeur de la propri�t� sourceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceId(String value) {
        this.sourceId = value;
    }

    /**
     * Obtient la valeur de la propri�t� targetId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetId() {
        return targetId;
    }

    /**
     * D�finit la valeur de la propri�t� targetId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetId(String value) {
        this.targetId = value;
    }

}
