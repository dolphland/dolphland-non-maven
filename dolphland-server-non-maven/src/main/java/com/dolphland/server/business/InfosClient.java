package com.dolphland.server.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.core.ws.data.RemoteEvent;
import com.dolphland.core.ws.data.RemoteMessage;
import com.dolphland.core.ws.data.User;
import com.dolphland.server.util.string.StrUtil;

/**
 * Information for a client
 * 
 * @author JayJay
 * 
 */
public class InfosClient {

    private static final Logger LOG = Logger.getLogger(InfosClient.class);

    // Client id
    private String clientId = null;

    // User id
    private String userId = null;

    // Offline
    private boolean offline = false;

    // Last online date
    private Date lastOnlineDate = null;

    // Events
    private List<RemoteEvent> events = new ArrayList<RemoteEvent>();

    // Messages
    private List<RemoteMessage> messages = new ArrayList<RemoteMessage>();



    public InfosClient() {
        clientId = new Long(new Date().getTime()).toString();
        if (LOG.isDebugEnabled()) {
            LOG.debug("InfosClient(): Create client " + clientId);
        }
    }



    /**
     * Get client id
     * 
     * @return Client id
     */
    public synchronized String getClientId() {
        return clientId;
    }



    /**
     * @return the userId
     */
    public synchronized String getUserId() {
        return StrUtil.isEmpty(userId) ? null : userId;
    }



    /**
     * Set user
     * 
     * @param user
     *            User
     */
    public synchronized void setUser(User user) {
        this.userId = user == null ? null : user.getId();
    }



    /**
     * Tell if client is offline
     * 
     * @return False if client is offline
     */
    public synchronized boolean isOffline() {
        return offline;
    }



    /**
     * Set offline state for client
     * 
     * @param offline
     *            False if client is offline
     */
    public synchronized void setOffline(boolean offline) {
        this.offline = offline;
    }



    /**
     * Remove all events
     * 
     * @return events removed
     */
    public synchronized List<RemoteEvent> removeEvents() {
        List<RemoteEvent> result = new ArrayList<RemoteEvent>();
        while (!events.isEmpty()) {
            result.add(events.remove(0));
        }
        return result;
    }



    /**
     * Add event
     * 
     * @param event
     *            Event
     */
    public synchronized void addEvent(RemoteEvent event) {
        if (event != null) {
            this.events.add(event);
        }
    }



    /**
     * @return the lastOnlineDate
     */
    public synchronized Date getLastOnlineDate() {
        return lastOnlineDate == null ? null : new Date(lastOnlineDate.getTime());
    }



    /**
     * @param lastOnlineDate
     *            the lastOnlineDate to set
     */
    public synchronized void setLastOnlineDate(Date lastOnlineDate) {
        this.lastOnlineDate = lastOnlineDate == null ? null : new Date(lastOnlineDate.getTime());
    }



    /**
     * Remove all messages
     * 
     * @return message removed
     */
    public synchronized List<RemoteMessage> removeMessages() {
        List<RemoteMessage> result = new ArrayList<RemoteMessage>();
        while (!messages.isEmpty()) {
            result.add(messages.remove(0));
        }
        return result;
    }



    /**
     * Add message
     * 
     * @param message
     *            Message
     */
    public synchronized void addMessage(RemoteMessage message) {
        if (message != null) {
            this.messages.add(message);
        }
    }
}
