package com.dolphland.server.business;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.dolphland.server.util.string.StrUtil;

/**
 * Business implementation for web
 * 
 * @author JayJay
 * 
 */
@Service
public class BOWeb extends AbstractBO {

    private static final Logger LOG = Logger.getLogger(BOWeb.class);

    private static final long SLEEP_MS = 100;

    private static Map<String, InfosSocket> mapServerInfosSocket = new TreeMap<String, InfosSocket>();



    private static InfosSocket getServerInfosSocket(String socketId) {
        InfosSocket socket = mapServerInfosSocket.get(socketId);
        if (socket == null) {
            mapServerInfosSocket.put(socketId, new InfosSocket());
            socket = mapServerInfosSocket.get(socketId);
        }
        return socket;
    }



    /**
     * Get server response from socket id and browser request
     * 
     * @param socketId
     *            Socket id
     * @param browserRequest
     *            Browser request
     * 
     * @return Server response
     */
    public List<Integer> getContent(String socketId, List<Integer> bytesCustomerRequest) {
        List<Integer> bytesServerResponse = new ArrayList<Integer>();
        // InputStream serverIn = null;
        // OutputStream serverOut = null;
        int r0, i, ch;

        try {
            System.out.println("D�but URL");

            InfosSocket infosSocket = getServerInfosSocket(socketId);
            InputStream serverIn = infosSocket.getServerIn();
            OutputStream serverOut = infosSocket.getServerOut();
            String customerRequest = new String();
            String serverResponse = new String();

            for (int aByte : bytesCustomerRequest) {
                serverOut.write(aByte);
                customerRequest += codeToAscii(aByte);
            }
            serverOut.flush();

            Thread.sleep(SLEEP_MS);

            while ((r0 = serverIn.available()) > 0) {
                for (i = 0; i < r0; i++) {
                    ch = serverIn.read();
                    if (ch != -1) {
                        serverResponse += codeToAscii(ch);
                        bytesServerResponse.add(ch);
                    }
                }
            }
            if (LOG.isDebugEnabled()) {
                if (!StrUtil.isEmpty(customerRequest)) {
                    LOG.debug("getContent(): Customer's request :\n" + customerRequest);
                }
                if (!StrUtil.isEmpty(serverResponse)) {
                    LOG.debug("getContent(): Server's response :\n" + serverResponse);
                }
            }

            System.out.println("Fin URL");
        } catch (Exception exc) {
            LOG.error("Error when getting content", exc);
        } finally {
            // StreamUtil.safeClose(serverIn);
            // StreamUtil.safeClose(serverOut);
        }

        return bytesServerResponse;
    }



    /**
     * Convert code to ascii
     * 
     * @param code
     *            Code
     * @return Ascii
     */
    private String codeToAscii(int code) {
        byte b = (byte) (code & 0x000000FF);
        byte[] codes = new byte[] { b };
        return new String(codes);
    }

    private static class InfosSocket {

        private InputStream serverIn = null;
        private OutputStream serverOut = null;



        public InfosSocket() {
            try {
                // ProxyUtil.setProxyId(ProxyUtil.ID_PROXY_VALLOUREC);
                // Socket socket = new Socket(ProxyUtil.getProxyHost(),
                // ProxyUtil.getProxyPort());//www.noussommestousdesdauphins.com",
                // 3128);
                Socket socket = null;
                serverIn = new BufferedInputStream(socket.getInputStream());
                serverOut = new BufferedOutputStream(socket.getOutputStream());
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }



        public InputStream getServerIn() {
            return serverIn;
        }



        public OutputStream getServerOut() {
            return serverOut;
        }
    }
}
