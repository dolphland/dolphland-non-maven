package com.dolphland.server.business;

import org.apache.log4j.Logger;

import com.dolphland.core.ws.data.RemoteEvent;
import com.dolphland.core.ws.data.RemoteEventType;
import com.dolphland.core.ws.data.RemoteMessage;
import com.dolphland.server.context.ContextServer;
import com.dolphland.server.util.assertion.AssertUtil;
import com.dolphland.server.util.introspection.IntrospectionUtil;

/**
 * Abstract business
 * 
 * @author JayJay
 * 
 */
public abstract class AbstractBO {

    private static final Logger LOG = Logger.getLogger(AbstractBO.class);



    /**
     * Create an event from its type
     * 
     * @param type
     *            Event's type
     * 
     * @return Event created
     */
    protected RemoteEvent createRemoteEvent(RemoteEventType type) {
        return createRemoteEvent(type, null, null);
    }



    /**
     * Create an event from its type and source
     * 
     * @param type
     *            Event's type
     * @param sourceId
     *            Event's source
     * 
     * @return Event created
     */
    protected RemoteEvent createRemoteEvent(RemoteEventType type, String sourceId) {
        return createRemoteEvent(type, sourceId, null);
    }



    /**
     * Create an event from its type, source and target
     * 
     * @param type
     *            Event's type
     * @param sourceId
     *            Event's source
     * @param targetId
     *            Event's target
     * 
     * @return Event created
     */
    protected RemoteEvent createRemoteEvent(RemoteEventType type, String sourceId, String targetId) {
        RemoteEvent event = new RemoteEvent();
        event.setType(type);
        event.setSourceId(sourceId);
        event.setTargetId(targetId);
        return event;
    }



    /**
     * Dispatch an event to all clients
     * 
     * @param event
     *            Event
     */
    protected void dispatchEvent(RemoteEvent event) {
        for (InfosClient infosClient : ContextServer.getInfosClient()) {
            dispatchEvent(infosClient, event);
        }
    }



    /**
     * Dispatch an event to party's clients
     * 
     * @param infosParty
     *            Party
     * @param event
     *            Event
     */
    protected void dispatchEvent(InfosParty infosParty, RemoteEvent event) {
        AssertUtil.notNull(infosParty, "infosParty is null !");
        for (InfosClient infosClient : ContextServer.getInfosClient()) {
            if (infosClient.getUserId() != null && infosParty.getMembers().contains(infosClient.getUserId())) {
                dispatchEvent(infosClient, event);
            }
        }
    }



    /**
     * Dispatch an event to a client
     * 
     * @param infosClient
     *            Client
     * @param event
     *            Event
     */
    protected void dispatchEvent(InfosClient infosClient, RemoteEvent event) {
        AssertUtil.notNull(event, "event is null !");
        AssertUtil.notNull(event.getType(), "event's type is null !");
        AssertUtil.notNull(infosClient, "infosClient is null !");
        if (LOG.isDebugEnabled()) {
            LOG.debug("Dispatch " + IntrospectionUtil.toString(event) + " to client " + infosClient.getClientId());
        }
        infosClient.addEvent(event);
    }



    /**
     * Dispatch a message to all clients
     * 
     * @param message
     *            Message
     */
    protected void dispatchMessage(RemoteMessage message) {
        for (InfosClient infosClient : ContextServer.getInfosClient()) {
            if (infosClient.getUserId() != null) {
                dispatchMessage(infosClient, message);
            }
        }
    }



    /**
     * Dispatch a message to party's clients
     * 
     * @param infosParty
     *            Party
     * @param message
     *            Message
     */
    protected void dispatchMessage(InfosParty infosParty, RemoteMessage message) {
        AssertUtil.notNull(infosParty, "infosParty is null !");
        for (InfosClient infosClient : ContextServer.getInfosClient()) {
            if (infosClient.getUserId() != null && infosParty.getMembers().contains(infosClient.getUserId())) {
                dispatchMessage(infosClient, message);
            }
        }
    }



    /**
     * Dispatch a message to a client
     * 
     * @param infosClient
     *            Client
     * @param message
     *            Message
     */
    protected void dispatchMessage(InfosClient infosClient, RemoteMessage message) {
        AssertUtil.notNull(message, "message is null !");
        AssertUtil.notNull(message.getType(), "message's type is null !");
        AssertUtil.notNull(message.getSenderUserId(), "message's sender is null !");
        AssertUtil.notNull(infosClient, "infosClient is null !");
        if (LOG.isDebugEnabled()) {
            LOG.debug("Dispatch " + IntrospectionUtil.toString(message) + " to client " + infosClient.getClientId());
        }
        infosClient.addMessage(message);
    }
}
