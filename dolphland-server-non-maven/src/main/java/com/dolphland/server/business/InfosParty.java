package com.dolphland.server.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;
import com.dolphland.server.util.assertion.AssertUtil;

/**
 * Information for a party
 * 
 * @author JayJay
 * 
 */
public class InfosParty {

    private static final Logger LOG = Logger.getLogger(InfosParty.class);

    // Party
    private Party party = null;

    // Party's business
    private AbstractBOParty boParty;



    /**
     * Constructor with party's business, user id, application id and ranked
     * information
     * The party is ranked if ranked indicator is true
     * 
     * @param boParty
     *            Party's business
     * @param userId
     *            User id
     * @param applicationId
     *            Application id
     * @param ranked
     *            True if party is ranked
     */
    public InfosParty(AbstractBOParty boParty, String userId, ApplicationId applicationId, boolean ranked) {
        AssertUtil.notNull(boParty, "boParty is null or empty !");
        AssertUtil.notEmpty(userId, "userId is null or empty !");
        AssertUtil.notNull(applicationId, "applicationId is null !");
        this.boParty = boParty;
        Date currentDate = new Date();
        party = new Party();
        party.setId(new Long(currentDate.getTime()).toString());
        party.setApplicationId(applicationId);
        party.setOwnerUserId(userId);
        party.setRanked(ranked);
        if (LOG.isDebugEnabled()) {
            LOG.debug("InfosParty(): Create party (id=" + party.getId() + " ; applicationId=" + party.getApplicationId() + " ; ownerUserId=" + party.getOwnerUserId() + " ; ranked=" + party.isRanked() + ")");
        }
    }



    /**
     * Get party's business
     * 
     * @return Party's business
     */
    public AbstractBOParty getBoParty() {
        return boParty;
    }



    /**
     * Get party
     * 
     * @return Party
     */
    public synchronized Party getParty() {
        Party result = new Party();
        result.setId(party.getId());
        result.setApplicationId(party.getApplicationId());
        result.setOwnerUserId(party.getOwnerUserId());
        result.setRanked(party.isRanked());
        result.setFinished(party.isFinished());
        result.getMembersUsersIds().addAll(new ArrayList<String>(party.getMembersUsersIds()));
        return result;
    }



    /**
     * Get members
     * 
     * @return members
     */
    public Set<String> getMembers() {
        return new TreeSet<String>(party.getMembersUsersIds());
    }



    /**
     * Add a member
     * 
     * @param userId
     *            User id
     */
    public synchronized void addMember(String userId) {
        if (!party.getMembersUsersIds().contains(userId)) {
            party.getMembersUsersIds().add(userId);
        }
    }



    /**
     * Remove a member
     * 
     * @param userId
     *            User id
     */
    public synchronized void removeMember(String userId) {
        party.getMembersUsersIds().remove(userId);
    }



    /**
     * Finish the party
     */
    public synchronized void finishParty() {
        party.setFinished(true);
    }
}
