package com.dolphland.server.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.Application;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.RemoteError;
import com.dolphland.core.ws.data.RemoteEventType;
import com.dolphland.core.ws.data.RemoteMessage;
import com.dolphland.core.ws.data.RemoteMessageType;
import com.dolphland.core.ws.data.User;
import com.dolphland.server.business.dao.DAOUser;
import com.dolphland.server.business.exception.RemoteException;
import com.dolphland.server.context.ContextServer;

/**
 * Business implementation for user
 * 
 * @author JayJay
 * 
 */
@Service
public class BOUser extends AbstractBO {

    private static final Logger LOG = Logger.getLogger(BOUser.class);

    private DAOUser daoUser;

    private BOParty boParty;



    @Autowired
    public void setDaoUser(DAOUser daoUser) {
        this.daoUser = daoUser;
    }



    @Autowired
    public void setBoParty(BOParty boParty) {
        this.boParty = boParty;
    }



    /**
     * Login a user
     * 
     * @param clientId
     *            Client id
     * @param userId
     *            User id
     * @param userPassword
     *            User password
     * 
     * @return User logged
     */
    public User login(String clientId, String userId, String userPassword) {
        User user = null;

        // Search user in context
        for (User contextUser : getUsers()) {
            if (contextUser.getId().equals(userId)) {
                user = contextUser;
                break;
            }
        }

        // If user not in context, get the user from bdd
        if (user == null) {
            user = daoUser.getUser(userId);
            // If user exists, add the user in context
            if (user != null) {
                ContextServer.addUser(user);
            }
        }

        // If user doesn't exist
        if (user == null) {
            throw new RemoteException(RemoteError.ERR_DOLPHLAND_001_NOT_EXISTED_USER);
        }

        // If password doesn't match
        if (!user.getPassword().equals(userPassword)) {
            throw new RemoteException(RemoteError.ERR_DOLPHLAND_002_BAD_USER_PASSWORD);
        }

        // Get information from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Remove user for the others clients
        for (InfosClient infosOtherClient : ContextServer.getInfosClient()) {
            if (user.getId().equals(infosOtherClient.getUserId())) {
                // Remove user for the client
                infosOtherClient.setUser(null);
            }
        }

        // If user already connected
        if (user.isConnected()) {
            if (user.isOffline()) {
                // If user offline, dispatch event USER_REONLINE
                dispatchEvent(createRemoteEvent(RemoteEventType.USER_REONLINE, user.getId(), clientId));
            } else {
                // If user is not offline, dispatch event USER_REPLACEMENT
                dispatchEvent(createRemoteEvent(RemoteEventType.USER_REPLACEMENT, user.getId(), clientId));
            }
        }

        // Dispatch event USER_LOGIN
        dispatchEvent(createRemoteEvent(RemoteEventType.USER_LOGIN, user.getId(), clientId));

        // Connect the user
        user.setConnected(true);
        user.setOffline(false);

        // Set user for the client
        infosClient.setUser(user);

        // Dispatch event USERS_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.USERS_CHANGED));

        // Dispatch event APPLICATIONS_CHANGED to the client
        dispatchEvent(infosClient, createRemoteEvent(RemoteEventType.APPLICATIONS_CHANGED));

        // Dispatch event PARTIES_CHANGED to the client
        dispatchEvent(infosClient, createRemoteEvent(RemoteEventType.PARTIES_CHANGED));

        // For each party where user is a member
        for (InfosParty infosParty : ContextServer.getInfosParties()) {
            if (infosParty.getMembers().contains(user.getId())) {
                // Dispatch event PARTY_CHANGED
                dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, infosParty.getParty().getId()));
            }
        }

        return user;
    }



    /**
     * Logout a user
     * 
     * @param clientId
     *            Client id
     */
    public void logout(String clientId) {
        // Get information from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // If a connected user is attached by the client
        User user = ContextServer.getUser(infosClient.getUserId());
        if (user != null && user.isConnected()) {
            for (InfosParty infosParty : ContextServer.getInfosParties()) {
                // Leave all parties where user is a member
                if (infosParty.getMembers().contains(user.getId())) {
                    boParty.leaveParty(clientId, infosParty.getParty().getId());
                }
            }

            // Disconnect the user
            user.setConnected(false);
            user.setOffline(false);

            // Dispatch event USER_LOGOUT
            dispatchEvent(createRemoteEvent(RemoteEventType.USER_LOGOUT, user.getId(), clientId));

            // Unset user for the client
            infosClient.setUser(null);

            // Dispatch event USERS_CHANGED
            dispatchEvent(createRemoteEvent(RemoteEventType.USERS_CHANGED));
        }
    }



    /**
     * Get users
     * 
     * @return Users
     */
    public List<User> getUsers() {
        // Get users from context
        List<User> users = ContextServer.getUsers();

        // If no users in context, get users from bdd and set users in context
        if (users.isEmpty()) {
            for (User user : daoUser.getAllUsers()) {
                ContextServer.addUser(user);
            }
            users = ContextServer.getUsers();
        }

        return users;
    }



    /**
     * Offline a client
     * 
     * @param clientId
     *            Client id
     */
    public void offlineClient(String clientId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("offlineClient(): The client " + clientId + " is offline");
        }

        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Offline client
        infosClient.setOffline(true);

        // If an online user exists
        User user = ContextServer.getUser(infosClient.getUserId());
        if (user != null && !user.isOffline()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("offlineClient(): The user " + user.getId() + " is offline");
            }

            // Offline the user
            user.setOffline(true);

            // Dispatch event USER_OFFLINE
            dispatchEvent(createRemoteEvent(RemoteEventType.USER_OFFLINE, user.getId(), clientId));

            // Dispatch event USERS_CHANGED
            dispatchEvent(createRemoteEvent(RemoteEventType.USERS_CHANGED));

            // For each party where user is a member
            for (InfosParty infosParty : ContextServer.getInfosParties()) {
                if (infosParty.getMembers().contains(user.getId())) {
                    // Dispatch event PARTY_CHANGED
                    dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, infosParty.getParty().getId()));
                }
            }
        }
    }



    /**
     * Disconnect a client
     * 
     * @param clientId
     *            Client id
     */
    public void disconnectClient(String clientId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("disconnectClient(): The client " + clientId + " is disconnected");
        }

        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // If an connected user is attached by the client
        User user = ContextServer.getUser(infosClient.getUserId());
        if (user != null && user.isConnected()) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("disconnectClient(): The user " + user.getId() + " is disconnected");
            }

            // Leave all parties
            for (InfosParty infosParty : ContextServer.getInfosParties()) {
                boParty.leaveParty(clientId, infosParty.getParty().getId());
            }

            // Disconnect the user
            user.setConnected(false);
            user.setOffline(false);

            // Unset user for the client
            infosClient.setUser(null);

            // Dispatch event USER_DISCONNECTION
            dispatchEvent(createRemoteEvent(RemoteEventType.USER_DISCONNECTION, user.getId(), clientId));

            // Dispatch event USERS_CHANGED
            dispatchEvent(createRemoteEvent(RemoteEventType.USERS_CHANGED));
        }

        // Remove client
        ContextServer.removeClient(clientId);
    }



    /**
     * Send a message
     * 
     * @param clientId
     *            Client id
     * @param type
     *            Type
     * @param content
     *            Content
     * @param receiverUserId
     *            Receiver user id
     * @param partyId
     *            Party id
     */
    public void sendMessage(String clientId, RemoteMessageType type, String content, String receiverUserId, String partyId) {
        // Get information from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // If a connected user is attached by the client
        User user = ContextServer.getUser(infosClient.getUserId());
        if (user != null && user.isConnected()) {

            // Create message
            RemoteMessage message = new RemoteMessage();
            message.setType(type);
            message.setContent(content);
            message.setSenderUserId(user.getId());
            message.setReceiverUserId(receiverUserId);
            message.setPartyId(partyId);

            if (receiverUserId != null) {
                // Dispatch message to receiver
                dispatchMessage(ContextServer.getInfosClient(receiverUserId), message);
            } else if (partyId != null) {
                // Dispatch message to party's clients
                dispatchMessage(ContextServer.getInfosParty(partyId), message);
            } else {
                // Dispatch message to all clients
                dispatchMessage(message);
            }
        }
    }



    /**
     * Get applications
     * 
     * @param clientId
     *            Client id
     * @return Applications
     */
    public List<Application> getApplications(String clientId) {
        List<Application> applications = new ArrayList<Application>();

        // Get information from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Applications where a party is enabled
        List<ApplicationId> applicationsPartyEnabled = boParty.getApplicationsPartiesEnabled();

        // If a user is attached by the client
        User user = ContextServer.getUser(infosClient.getUserId());
        if (user != null) {
            for (ApplicationId applicationId : ApplicationId.values()) {
                Application application = new Application();
                application.setId(applicationId);
                application.setPartyEnabled(applicationsPartyEnabled.contains(applicationId));
                applications.add(application);
            }
        }

        return applications;
    }
}
