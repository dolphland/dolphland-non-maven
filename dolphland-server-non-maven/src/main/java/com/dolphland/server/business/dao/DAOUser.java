package com.dolphland.server.business.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.User;
import com.dolphland.server.util.date.DateUtil;
import com.dolphland.server.util.string.StrUtil;

/**
 * DAO for user
 * 
 * @author JayJay
 * 
 */
@Service
public class DAOUser extends AbstractDAO {

    private static final Logger LOG = Logger.getLogger(DAOUser.class);

    private final static String COLUMN_ID = "ID";
    private final static String COLUMN_PASSWORD = "PWD";
    private final static String COLUMN_FIRSTNAME = "PRENOM";
    private final static String COLUMN_LASTNAME = "NOM";
    private final static String COLUMN_BIRTHDAY = "BIRTHDAY";
    private final static String COLUMN_AVATAR = "AVATAR";
    private final static String COLUMN_BACKGROUND = "BACKGROUND";
    private final static String COLUMN_EMAIL = "EMAIL";

    private final static String[] ALL_COLUMNS = new String[] {
    COLUMN_ID,
    COLUMN_PASSWORD,
    COLUMN_FIRSTNAME,
    COLUMN_LASTNAME,
    COLUMN_BIRTHDAY,
    COLUMN_AVATAR,
    COLUMN_BACKGROUND,
    COLUMN_EMAIL
    };



    /**
     * Get all users
     * 
     * @return All users
     */
    public List<User> getAllUsers() {
        StringBuffer query = new StringBuffer();
        query.append("select " + StrUtil.formatList(ALL_COLUMNS));
        query.append(" from USER");
        query.append(" order by " + COLUMN_ID);
        if(LOG.isDebugEnabled()){
            LOG.debug("getAllUsers(): Execute query '" + query + "'");
        }
        return jdbcTemplate.query(query.toString(), new UserRowMapper());
    }



    /**
     * Get user from id
     * 
     * @return User
     */
    public User getUser(String id) {
        StringBuffer query = new StringBuffer();
        query.append("select " + StrUtil.formatList(ALL_COLUMNS));
        query.append(" from USER");
        query.append(" where " + COLUMN_ID + " = '" + id + "'");
        if(LOG.isDebugEnabled()){
            LOG.debug("getUser(): Execute query '" + query + "'");
        }
        List<User> users = jdbcTemplate.query(query.toString(), new UserRowMapper());
        return users == null || users.isEmpty() ? null : users.get(0);
    }

    /**
     * User row mapper
     * 
     * @author JayJay
     * 
     */
    private static class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int line) throws SQLException {
            UserResultSetExtractor extractor = new UserResultSetExtractor();
            return extractor.extractData(rs);
        }
    }

    /**
     * User results set extractor
     * 
     * @author JayJay
     * 
     */
    private static class UserResultSetExtractor implements ResultSetExtractor<User> {

        @Override
        public User extractData(ResultSet rs) throws SQLException {
            User user = new User();
            user.setId(rs.getString(COLUMN_ID));
            user.setPassword(rs.getString(COLUMN_PASSWORD));
            user.setFirstName(rs.getString(COLUMN_FIRSTNAME));
            user.setLastName(rs.getString(COLUMN_LASTNAME));
            user.setBirthday(DateUtil.toXmlDate(rs.getDate(COLUMN_BIRTHDAY)));
            user.setAvatar(rs.getString(COLUMN_AVATAR));
            user.setBackground(rs.getString(COLUMN_BACKGROUND));
            user.setEmail(rs.getString(COLUMN_EMAIL));
            return user;
        }
    }
}
