package com.dolphland.server.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.RemoteError;
import com.dolphland.core.ws.data.RemoteEventType;
import com.dolphland.server.business.dao.DAOScoreUser;
import com.dolphland.server.business.dao.DAOUser;
import com.dolphland.server.business.exception.RemoteException;
import com.dolphland.server.context.ContextServer;
import com.dolphland.server.util.exception.AppBusinessException;

/**
 * Business implementation for party
 * 
 * @author JayJay
 * 
 */
@Service
public class BOParty extends AbstractBOParty {

    private static final Logger LOG = Logger.getLogger(BOParty.class);

    private BOChess boChess;

    protected DAOScoreUser daoScoreUser;



    @Autowired
    public void setDaoScoreUser(DAOScoreUser daoScoreUser) {
        this.daoScoreUser = daoScoreUser;
    }



    @Autowired
    public void setBoChess(BOChess boChess) {
        this.boChess = boChess;
    }



    /**
     * Create party's informations for the client and application specified.
     * The party is ranked if ranked indicator is true
     * 
     * @param infosClient
     *            Client's informations
     * @param applicationId
     *            Application id
     * @param ranked
     *            True if party is ranked
     * 
     * @return Party's informations created
     */
    private InfosParty createInfosParty(InfosClient infosClient, ApplicationId applicationId, boolean ranked) {
        if (!getApplicationsPartiesEnabled().contains(applicationId)) {
            throw new AppBusinessException("The party's application is disabled !");
        }

        InfosParty result = null;
        switch (applicationId) {
            case CHESS :
                result = new InfosChess(boChess, infosClient.getUserId(), applicationId, ranked, boChess.createBoard());
                break;

            default :
                throw new AppBusinessException("The party's creation is not implemented !");
        }

        return result;
    }



    /**
     * Remove party
     * 
     * @param infosParty
     *            Party's informations
     */
    private void removeParty(InfosParty infosParty) {
        // Remove party
        ContextServer.removeParty(infosParty.getParty().getId());

        // Dispatch event PARTIES_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTIES_CHANGED));
    }



    /**
     * Get parties
     * 
     * @return Parties
     */
    public List<Party> getParties() {
        List<Party> result = new ArrayList<Party>();
        for (InfosParty infosParty : ContextServer.getInfosParties()) {
            result.add(infosParty.getParty());
        }
        return result;
    }



    /**
     * Create a party for the client, application specified.
     * The party is ranked if ranked indicator is true
     * 
     * @param clientId
     *            Client id
     * @param applicationId
     *            Application id
     * @param ranked
     *            True if party is ranked
     * 
     * @return Party created
     */
    public Party createParty(String clientId, ApplicationId applicationId, boolean ranked) {
        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Create party
        InfosParty infosParty = ContextServer.addParty(createInfosParty(infosClient, applicationId, ranked));

        // Dispatch event PARTIES_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTIES_CHANGED));

        // Join the party
        return joinParty(clientId, infosParty.getParty().getId());
    }



    /**
     * Finish a party from a client
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * @param winnersUsersIds
     *            Winners users ids
     * @param losersUsersIds
     *            Losers users ids
     * 
     * @return Party Finished
     */
    public Party finishParty(String clientId, String partyId, List<String> winnersUsersIds, List<String> losersUsersIds) {
        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Get informations from party
        InfosParty infosParty = getInfosParty(clientId, partyId);

        // Update score for winners
        for (String userId : winnersUsersIds) {
            if(LOG.isDebugEnabled()){
                LOG.debug("finishParty(): " + userId + " won a party for application " + infosParty.getParty().getApplicationId());
            }
            daoScoreUser.incrementScore(infosParty.getParty().getApplicationId(), userId, 1, 0);
        }

        // Update score for losers
        for (String userId : losersUsersIds) {
            if(LOG.isDebugEnabled()){
                LOG.debug("finishParty(): " + userId + " lost a party for application " + infosParty.getParty().getApplicationId());
            }
            daoScoreUser.incrementScore(infosParty.getParty().getApplicationId(), userId, 0, 1);
        }

        // Finish the party
        infosParty.finishParty();

        // Dispatch event PARTY_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, partyId));

        // Dispatch event PARTY_FINISHED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_FINISHED, infosParty.getParty().getId(), infosClient.getUserId()));

        // Dispatch event SCORES_CHANGED if winners and/or losers
        if (!winnersUsersIds.isEmpty() || !losersUsersIds.isEmpty()) {
            dispatchEvent(createRemoteEvent(RemoteEventType.SCORES_CHANGED));
        }

        return infosParty.getParty();
    }



    /**
     * Join a party for the client
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * 
     * @return Party Joined
     */
    public Party joinParty(String clientId, String partyId) {
        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Get informations from party
        InfosParty infosParty = getInfosParty(clientId, partyId);

        // If user is already a member, just dispatch event
        // PARTIES_CHANGED for the client to synchronize
        if (infosParty.getMembers().contains(infosClient.getUserId())) {
            dispatchEvent(infosClient, createRemoteEvent(RemoteEventType.PARTIES_CHANGED));
            dispatchEvent(infosClient, createRemoteEvent(RemoteEventType.PARTY_CHANGED, partyId));
            return infosParty.getParty();
        }

        if(LOG.isDebugEnabled()){
            LOG.debug("joinParty(): " + infosClient.getUserId() + " joins a party for application " + infosParty.getParty().getApplicationId());
        }

        // Trigger join party
        triggerJoinParty(infosClient, infosParty);

        // Add user to party
        infosParty.addMember(infosClient.getUserId());

        // Dispatch event PARTIES_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTIES_CHANGED));

        // Dispatch event PARTY_JOINED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_JOINED, partyId, infosClient.getUserId()));

        // Dispatch event PARTY_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, partyId));

        return infosParty.getParty();
    }



    /**
     * Leave a party for the client
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * 
     * @return Party Leaved
     */
    public Party leaveParty(String clientId, String partyId) {
        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Get informations from party
        InfosParty infosParty = getInfosParty(clientId, partyId);

        // If user is not a member, just dispatch event
        // PARTIES_CHANGED for the client to synchronize
        if (!infosParty.getMembers().contains(infosClient.getUserId())) {
            dispatchEvent(infosClient, createRemoteEvent(RemoteEventType.PARTIES_CHANGED));
            return infosParty.getParty();
        }

        if(LOG.isDebugEnabled()){
            LOG.debug("leaveParty(): " + infosClient.getUserId() + " leaves a party for application " + infosParty.getParty().getApplicationId());
        }

        // Dispatch event PARTY_LEFT
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_LEFT, infosParty.getParty().getId(), infosClient.getUserId()));

        // Trigger leave party
        triggerLeaveParty(infosClient, infosParty);

        // Remove user from party
        infosParty.removeMember(infosClient.getUserId());

        if (infosParty.getMembers().isEmpty()) {
            // If no more member, remove party
            removeParty(infosParty);
        } else {
            // Dispatch event PARTIES_CHANGED
            dispatchEvent(createRemoteEvent(RemoteEventType.PARTIES_CHANGED));

            // Dispatch event PARTY_CHANGED
            dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, partyId));
        }

        return infosParty.getParty();
    }



    /**
     * Abort a party for the client
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * 
     * @return Party Aborted
     */
    public Party abortParty(String clientId, String partyId) {
        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Get informations from party
        InfosParty infosParty = getInfosParty(clientId, partyId);

        // Do nothing if party is finished
        if (infosParty.getParty().isFinished()) {
            throw new RemoteException(RemoteError.ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY);
        }

        if(LOG.isDebugEnabled()){
            LOG.debug("abortParty(): " + infosClient.getUserId() + " aborts a party for application " + infosParty.getParty().getApplicationId());
        }

        // Trigger abort party
        triggerAbortParty(infosClient, infosParty);

        // Dispatch event PARTIES_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTIES_CHANGED));

        // Dispatch event PARTY_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, partyId));

        return infosParty.getParty();
    }



    /**
     * Get applications where parties are enabled
     * 
     * @return Applications where parties are enabled
     */
    public List<ApplicationId> getApplicationsPartiesEnabled() {
        List<ApplicationId> result = new ArrayList<ApplicationId>();
        result.add(ApplicationId.CHESS);
        // result.add(ApplicationId.BELOTE);
        // result.add(ApplicationId.DRAUGHT);
        return result;
    }



    @Override
    public void triggerJoinParty(InfosClient infosClient, InfosParty infosParty) {
        if (infosParty != null) {
            infosParty.getBoParty().triggerJoinParty(infosClient, infosParty);
        }
    }



    @Override
    public void triggerLeaveParty(InfosClient infosClient, InfosParty infosParty) {
        if (infosParty != null) {
            infosParty.getBoParty().triggerLeaveParty(infosClient, infosParty);
        }
    }



    @Override
    public void triggerAbortParty(InfosClient infosClient, InfosParty infosParty) {
        if (infosParty != null) {
            infosParty.getBoParty().triggerAbortParty(infosClient, infosParty);
        }
    }
}
