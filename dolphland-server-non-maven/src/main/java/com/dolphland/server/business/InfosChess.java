package com.dolphland.server.business;

import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.server.util.assertion.AssertUtil;

/**
 * Information for a party Chess
 * 
 * @author JayJay
 * 
 */
public class InfosChess extends InfosParty {

    // Board
    private ChessBoard board = null;



    /**
     * Constructor with party's business, user id, application id and ranked
     * information
     * The party is ranked if ranked indicator is true
     * 
     * @param boChess
     *            Party's business
     * @param userId
     *            User id
     * @param applicationId
     *            Application id
     * @param ranked
     *            True if party is ranked
     * @param board
     *            Board
     */
    public InfosChess(BOChess boChess, String userId, ApplicationId applicationId, boolean ranked, ChessBoard board) {
        super(boChess, userId, applicationId, ranked);
        AssertUtil.notNull(board, "board is null !");
        this.board = board;
    }



    /**
     * Get board
     * 
     * @return board
     */
    public ChessBoard getBoard() {
        return board;
    }
}
