package com.dolphland.server.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;
import com.dolphland.core.ws.data.ChessColumn;
import com.dolphland.core.ws.data.ChessMovement;
import com.dolphland.core.ws.data.ChessPiece;
import com.dolphland.core.ws.data.ChessPieceType;
import com.dolphland.core.ws.data.ChessPlayer;
import com.dolphland.core.ws.data.ChessPlayerState;
import com.dolphland.core.ws.data.ChessPosition;
import com.dolphland.core.ws.data.ChessRow;
import com.dolphland.core.ws.data.ChessSquare;
import com.dolphland.core.ws.data.RemoteError;
import com.dolphland.core.ws.data.RemoteEventType;
import com.dolphland.server.business.exception.RemoteException;
import com.dolphland.server.context.ContextServer;
import com.dolphland.server.util.string.StrUtil;

/**
 * Business implementation for chess
 * 
 * @author JayJay
 * 
 */
@Service
public class BOChess extends AbstractBOParty {

    private static final Logger LOG = Logger.getLogger(BOChess.class);

    private static final int MOVABLE_POSITIONS = 0;
    private static final int TAKABLE_POSITIONS = 1;

    private BOParty boParty;



    @Autowired
    public void setBoParty(BOParty boParty) {
        this.boParty = boParty;
    }



    @Override
    protected InfosChess getInfosParty(String clientId, String partyId) {
        return (InfosChess) super.getInfosParty(clientId, partyId);
    }



    @Override
    protected InfosChess getInfosParty(String clientId, String partyId, boolean owner) {
        return (InfosChess) super.getInfosParty(clientId, partyId, owner);
    }



    @Override
    public List<ApplicationId> getApplicationsPartiesEnabled() {
        List<ApplicationId> result = new ArrayList<ApplicationId>();
        result.add(ApplicationId.CHESS);
        return result;
    }



    /**
     * Create position from row and column
     * 
     * @param row
     *            Row
     * @param column
     *            Column
     * 
     * @return Position created
     */
    private ChessPosition createPosition(ChessRow row, ChessColumn column) {
        ChessPosition out = new ChessPosition();
        out.setRow(row);
        out.setColumn(column);
        return out;
    }



    /**
     * Create square from position and color
     * 
     * @param position
     *            Position
     * @param color
     *            Color
     * 
     * @return Square created
     */
    private ChessSquare createSquare(ChessPosition position, ChessColor color) {
        ChessSquare out = new ChessSquare();
        out.setPosition(position);
        out.setColor(color);
        return out;
    }



    /**
     * Create piece from its id, position, type and color
     * 
     * @param id
     *            Id
     * @param position
     *            Position
     * @param type
     *            Type
     * @param color
     *            Color
     * @return Piece created
     */
    private ChessPiece createPiece(String id, ChessPosition position, ChessPieceType type, ChessColor color) {
        ChessPiece out = new ChessPiece();
        out.setPieceId(id);
        out.setPosition(position);
        out.setType(type);
        out.setColor(color);
        out.setMoved(false);
        return out;
    }



    /**
     * Create player from the color specified
     * 
     * @param color
     *            Color
     * 
     * @return Player created
     */
    private ChessPlayer createPlayer(ChessColor color) {
        ChessPlayer out = new ChessPlayer();
        out.setColor(color);
        out.setState(ChessPlayerState.NORMAL);
        out.setUserId(null);
        return out;
    }



    /**
     * Return player if possible
     * 
     * @param infosChess
     *            Chess's informations
     * @param infosClient
     *            Client's informations
     * 
     * @return Player
     */
    private ChessPlayer getPlayer(InfosChess infosChess, InfosClient infosClient) {
        for (ChessPlayer player : infosChess.getBoard().getPlayers()) {
            if (infosClient.getUserId().equals(player.getUserId())) {
                return player;
            }
        }
        return null;
    }
    
    
    
    /**
     * Get versus color
     * 
     * @param color Color
     * 
     * @return Versus color
     */
    private ChessColor getVersusColor(ChessColor color){
        return color == ChessColor.WHITE ? ChessColor.BLACK : ChessColor.WHITE;
    }



    /**
     * Tell if the game is started
     * 
     * @param infosChess
     *            Chess's informations
     * 
     * @return True if the game is started
     */
    private boolean isGameStarted(InfosChess infosChess) {
        if (infosChess.getParty().isFinished()) {
            return false;
        }
        for (ChessPlayer player : infosChess.getBoard().getPlayers()) {
            if (StrUtil.isEmpty(player.getUserId())) {
                return false;
            }
        }
        return true;
    }



    /**
     * Get player identified by its color
     * 
     * @param board
     *            Board
     * @param color
     *            Player's color
     * 
     * @return Player identified by its color
     */
    private ChessPlayer getPlayer(ChessBoard board, ChessColor color) {
        for (ChessPlayer player : board.getPlayers()) {
            if (player.getColor() == color) {
                return player;
            }
        }
        return null;
    }



    /**
     * Get piece from board and position
     * 
     * @param board
     *            Board
     * @param position
     *            Position
     * 
     * @return Piece
     */
    private ChessPiece getPiece(ChessBoard board, ChessPosition position) {
        if (position == null) {
            return null;
        }
        for (ChessPiece piece : board.getPieces()) {
            if (compare(piece.getPosition(), position) == 0) {
                return piece;
            }
        }
        return null;
    }



    /**
     * Get piece from board and position
     * 
     * @param board
     *            Board
     * @param position
     *            Position
     * @param movementSourcePosition
     *            Movement's source position (all pieces in this position are
     *            considered as a shadow)
     * @param movementTargetPosition
     *            Movement's target position (all pieces in this position are
     *            considered as a ghost)
     * 
     * @return Piece
     */
    private ChessPiece getPiece(ChessBoard board, ChessPosition position, ChessPosition movementSourcePosition, ChessPosition movementTargetPosition) {
        // If target position, the piece is considered as a ghost
        if (position != null && movementTargetPosition != null && compare(position, movementTargetPosition) == 0) {
            return createPiece(StrUtil.EMPTY_STRING, movementTargetPosition, ChessPieceType.PAWN, board.getCurrentColor());
        }
        ChessPiece piece = getPiece(board, position);
        // If a piece is in source position, the piece is considered as a shadow
        if (piece != null && movementSourcePosition != null && compare(piece.getPosition(), movementSourcePosition) == 0) {
            return null;
        }
        return piece;
    }



    /**
     * Get square from board and position
     * 
     * @param board
     *            Board
     * @param position
     *            Position
     * 
     * @return Square
     */
    private ChessSquare getSquare(ChessBoard board, ChessPosition position) {
        for (ChessSquare square : board.getSquares()) {
            if (compare(square.getPosition(), position) == 0) {
                return square;
            }
        }
        return null;
    }



    /**
     * Get recursively squares between two positions
     * 
     * @param board
     *            Board
     * @param position1
     *            Position 1
     * @param position2
     *            Position 2
     * 
     * @return Squares
     */
    private List<ChessSquare> getSquares(ChessBoard board, ChessPosition position1, ChessPosition position2) {
        List<ChessSquare> squares = new ArrayList<ChessSquare>();
        ChessPosition position = null;

        // Add square for position 1
        squares.add(getSquare(board, position1));

        // Exit if position 1 = position 2
        if (compare(position1, position2) == 0) {
            return squares;
        }

        // Try to increment row and column (SE direction)
        position = getPosition(position1.getRow().ordinal() + 1, position1.getColumn().ordinal() + 1);
        if (position != null && position.getRow().compareTo(position2.getRow()) <= 0 && position.getColumn().compareTo(position2.getColumn()) <= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        // Try to decrement row and column (NW direction)
        position = getPosition(position1.getRow().ordinal() - 1, position1.getColumn().ordinal() - 1);
        if (position != null && position.getRow().compareTo(position2.getRow()) >= 0 && position.getColumn().compareTo(position2.getColumn()) >= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        // Try to increment row and decrement column (SW direction)
        position = getPosition(position1.getRow().ordinal() + 1, position1.getColumn().ordinal() - 1);
        if (position != null && position.getRow().compareTo(position2.getRow()) <= 0 && position.getColumn().compareTo(position2.getColumn()) >= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        // Try to decrement row and increment column (NE direction)
        position = getPosition(position1.getRow().ordinal() - 1, position1.getColumn().ordinal() + 1);
        if (position != null && position.getRow().compareTo(position2.getRow()) >= 0 && position.getColumn().compareTo(position2.getColumn()) <= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        // Try to increment row (S direction)
        position = getPosition(position1.getRow().ordinal() + 1, position1.getColumn().ordinal());
        if (position != null && position.getRow().compareTo(position2.getRow()) <= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        // Try to increment column (E direction)
        position = getPosition(position1.getRow().ordinal(), position1.getColumn().ordinal() + 1);
        if (position != null && position.getColumn().compareTo(position2.getColumn()) <= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        // Try to decrement row (N direction)
        position = getPosition(position1.getRow().ordinal() - 1, position1.getColumn().ordinal());
        if (position != null && position.getRow().compareTo(position2.getRow()) >= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        // Try to decrement column (W direction)
        position = getPosition(position1.getRow().ordinal(), position1.getColumn().ordinal() - 1);
        if (position != null && position.getColumn().compareTo(position2.getColumn()) >= 0) {
            squares.addAll(getSquares(board, position, position2));
            return squares;
        }

        return squares;
    }



    /**
     * Compare two positions and return -1 if position1 < position2, 1 if
     * position1 > position2 or 0 if position1 = position2
     * 
     * @param position1
     *            First position
     * @param position2
     *            Second position
     * 
     * @return -1, 1 or 0
     */
    private int compare(ChessPosition position1, ChessPosition position2) {
        if (position1 == null && position2 == null) {
            return 0;
        }
        if (position1 == null) {
            return -1;
        }
        if (position2 == null) {
            return 1;
        }
        int result = position1.getRow().compareTo(position2.getRow());
        if (result != 0) {
            return result;
        }
        return position1.getColumn().compareTo(position2.getColumn());
    }



    /**
     * Get position from row index and column index
     * 
     * @param rowIdx
     *            Row index
     * @param columnIdx
     *            Column index
     * @return
     */
    private ChessPosition getPosition(int rowIdx, int columnIdx) {
        if (rowIdx < 0 || ChessRow.values().length <= rowIdx) {
            return null;
        }
        if (columnIdx < 0 || ChessColumn.values().length <= columnIdx) {
            return null;
        }
        return createPosition(ChessRow.values()[rowIdx], ChessColumn.values()[columnIdx]);
    }



    /**
     * Get target position from a movement
     * 
     * @param movement
     *            Movement
     * 
     * @return Target position
     */
    private ChessPosition getTargetPosition(ChessMovement movement) {
        if (movement == null || movement.getSquares().size() <= 1) {
            return null;
        }
        return movement.getSquares().get(movement.getSquares().size() - 1).getPosition();
    }



    /**
     * Get source position from a movement
     * 
     * @param movement
     *            Movement
     * 
     * @return Source position
     */
    private ChessPosition getSourcePosition(ChessMovement movement) {
        if (movement == null || movement.getSquares().isEmpty()) {
            return null;
        }
        return movement.getSquares().get(0).getPosition();
    }



    /**
     * Precise if position is target by one or more movements
     * 
     * @param movements
     *            Movements
     * @param position
     *            Position
     * 
     * @return True if position is target by one or more movements
     */
    private boolean isTargetPosition(List<ChessMovement> movements, ChessPosition position) {
        if (position == null) {
            return false;
        }
        for (ChessMovement movement : movements) {
            if (compare(getTargetPosition(movement), position) == 0) {
                return true;
            }
        }
        return false;
    }



    /**
     * Test if movement is forbidden (if king is chess or mat after the
     * movement)
     * 
     * @param board
     *            Board
     * @param movement
     *            Movement
     * 
     * @return True if movement is forbidden
     */
    private boolean isForbiddenMovement(ChessBoard board, ChessMovement movement) {

        // Get movement's source position
        ChessPosition movementSourcePosition = getSourcePosition(movement);

        // Get movement's target position
        ChessPosition movementTargetPosition = getTargetPosition(movement);

        // Get player's king position and all movements of versus player's
        // pieces
        ChessPosition kingPosition = null;
        List<ChessMovement> versusTakableMovements = new ArrayList<ChessMovement>();
        for (ChessPiece piece : board.getPieces()) {
            if (piece.getColor() != board.getCurrentColor()) {
                // Ignore piece in target position
                if (compare(piece.getPosition(), movementTargetPosition) == 0) {
                    continue;
                }
                versusTakableMovements.addAll(getMovements(board, piece.getType(), piece.getColor(), piece.isMoved(), piece.getPosition().getRow(), piece.getPosition().getColumn(), TAKABLE_POSITIONS, movementSourcePosition, movementTargetPosition));
            } else if (piece.getType() == ChessPieceType.KING) {
                // If king is in source position
                if (compare(piece.getPosition(), movementSourcePosition) == 0) {
                    // King's position is target position
                    kingPosition = movementTargetPosition;
                } else {
                    kingPosition = piece.getPosition();
                }
            }
        }

        // Movement is forbidden if king can be chess or mat after the movement
        return isTargetPosition(versusTakableMovements, kingPosition);
    }



    /**
     * Get recursively movements about a type, a color, a row, a column and a
     * position's type
     * 
     * @param board
     *            Board
     * @param type
     *            Type
     * @param color
     *            Color
     * @param moved
     *            Indicator for piece already moved
     * @param row
     *            Row
     * @param column
     *            Column
     * @param typePositions
     *            Position's type (MOVABLE_POSITIONS for positions accepted by
     *            movement or TAKABLE_POSITIONS pour positions accepted by
     *            taken)
     * @param movementSourcePosition
     *            Movement's source position (all pieces in this position are
     *            considered as a shadow)
     * @param movementTargetPosition
     *            Movement's target position (all pieces in this position are
     *            considered as a ghost)
     * 
     * @return Movements
     */
    private List<ChessMovement> getMovements(ChessBoard board, ChessPieceType type, ChessColor color, boolean moved, ChessRow row, ChessColumn column, int typePositions, ChessPosition movementSourcePosition, ChessPosition movementTargetPosition) {
        List<ChessMovement> movements = new ArrayList<ChessMovement>();

        ChessPosition startPosition = getPosition(row.ordinal(), column.ordinal());
        ChessPiece piecePosition = null;
        ChessPosition position = null;
        ChessMovement movement = null;

        // Get movement about piece's type
        switch (type) {
            case PAWN :
                // If black piece
                if (ChessColor.BLACK == color) {
                    if (TAKABLE_POSITIONS != typePositions) {
                        // If simple movement enabled
                        position = getPosition(row.ordinal() + 1, column.ordinal());
                        if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);

                            // If double movement enabled
                            position = getPosition(row.ordinal() + 2, column.ordinal());
                            if (row == ChessRow.ROW_2 && position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                                movement = new ChessMovement();
                                movement.getSquares().addAll(getSquares(board, startPosition, position));
                                movements.add(movement);
                            }
                        }
                    }
                    // If take a SE piece is possible
                    position = getPosition(row.ordinal() + 1, column.ordinal() + 1);
                    if (TAKABLE_POSITIONS == typePositions) {
                        if (position != null) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }

                    // If take a SW piece is possible
                    position = getPosition(row.ordinal() + 1, column.ordinal() - 1);
                    if (TAKABLE_POSITIONS == typePositions) {
                        if (position != null) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }
                // If white piece
                else {
                    if (TAKABLE_POSITIONS != typePositions) {
                        // If simple movement enabled
                        position = getPosition(row.ordinal() - 1, column.ordinal());
                        if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);

                            // If double movement enabled
                            position = getPosition(row.ordinal() - 2, column.ordinal());
                            if (row == ChessRow.ROW_7 && position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                                movement = new ChessMovement();
                                movement.getSquares().addAll(getSquares(board, startPosition, position));
                                movements.add(movement);
                            }
                        }
                    }
                    // If take a NE piece is possible
                    position = getPosition(row.ordinal() - 1, column.ordinal() + 1);
                    if (TAKABLE_POSITIONS == typePositions) {
                        if (position != null) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }

                    // If take a NW piece is possible
                    position = getPosition(row.ordinal() - 1, column.ordinal() - 1);
                    if (TAKABLE_POSITIONS == typePositions) {
                        if (position != null) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }
                break;

            case BISHOP :
                // Add movements for NW direction
                position = getPosition(row.ordinal() - 1, column.ordinal() - 1);
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal() - 1, position.getColumn().ordinal() - 1);
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for NE direction
                position = getPosition(row.ordinal() - 1, column.ordinal() + 1);
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal() - 1, position.getColumn().ordinal() + 1);
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for SW direction
                position = getPosition(row.ordinal() + 1, column.ordinal() - 1);
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal() + 1, position.getColumn().ordinal() - 1);
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for SE direction
                position = getPosition(row.ordinal() + 1, column.ordinal() + 1);
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal() + 1, position.getColumn().ordinal() + 1);
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }
                break;

            case KNIGHT :
                // Add movements for NW direction
                position = getPosition(row.ordinal() - 1, column.ordinal() - 2);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                position = getPosition(row.ordinal() - 2, column.ordinal() - 1);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for NE direction
                position = getPosition(row.ordinal() - 1, column.ordinal() + 2);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                position = getPosition(row.ordinal() - 2, column.ordinal() + 1);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for SW direction
                position = getPosition(row.ordinal() + 1, column.ordinal() - 2);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                position = getPosition(row.ordinal() + 2, column.ordinal() - 1);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for SE direction
                position = getPosition(row.ordinal() + 1, column.ordinal() + 2);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                position = getPosition(row.ordinal() + 2, column.ordinal() + 1);
                if (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                } else if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }
                break;

            case ROOK :
                // Add movements for N direction
                position = getPosition(row.ordinal() - 1, column.ordinal());
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal() - 1, position.getColumn().ordinal());
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for S direction
                position = getPosition(row.ordinal() + 1, column.ordinal());
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal() + 1, position.getColumn().ordinal());
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for W direction
                position = getPosition(row.ordinal(), column.ordinal() - 1);
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal(), position.getColumn().ordinal() - 1);
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }

                // Add movements for E direction
                position = getPosition(row.ordinal(), column.ordinal() + 1);
                while (position != null && getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                    movement = new ChessMovement();
                    movement.getSquares().addAll(getSquares(board, startPosition, position));
                    movements.add(movement);
                    position = getPosition(position.getRow().ordinal(), position.getColumn().ordinal() + 1);
                }
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else {
                    piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null && piecePosition.getColor() != color) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                }
                break;

            case QUEEN :
                // Queen's movements is made by concatenation of rook's
                // movements and bishop's movements
                movements.addAll(getMovements(board, ChessPieceType.BISHOP, color, moved, row, column, typePositions, movementSourcePosition, movementTargetPosition));
                movements.addAll(getMovements(board, ChessPieceType.ROOK, color, moved, row, column, typePositions, movementSourcePosition, movementTargetPosition));
                break;

            case KING :
                // Get all movements of versus player's pieces
                List<ChessMovement> versusTakableMovements = new ArrayList<ChessMovement>();
                if (TAKABLE_POSITIONS != typePositions) {
                    for (ChessPiece piece : board.getPieces()) {
                        if (piece.getColor() != color) {
                            versusTakableMovements.addAll(getMovements(board, piece.getType(), piece.getColor(), piece.isMoved(), piece.getPosition().getRow(), piece.getPosition().getColumn(), TAKABLE_POSITIONS, movementSourcePosition, movementTargetPosition));
                        }
                    }
                }

                // Add movement for NW direction
                position = getPosition(row.ordinal() - 1, column.ordinal() - 1);
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // Add movement for N direction
                position = getPosition(row.ordinal() - 1, column.ordinal());
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // Add movement for NE direction
                position = getPosition(row.ordinal() - 1, column.ordinal() + 1);
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // Add movement for W direction
                position = getPosition(row.ordinal(), column.ordinal() - 1);
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // Add movement for E direction
                position = getPosition(row.ordinal(), column.ordinal() + 1);
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // Add movement for SW direction
                position = getPosition(row.ordinal() + 1, column.ordinal() - 1);
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // Add movement for S direction
                position = getPosition(row.ordinal() + 1, column.ordinal());
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // Add movement for SE direction
                position = getPosition(row.ordinal() + 1, column.ordinal() + 1);
                if (TAKABLE_POSITIONS == typePositions) {
                    if (position != null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    }
                } else if (position != null && !isTargetPosition(versusTakableMovements, position)) {
                    // If king can't be taken by a versus player's piece
                    if (getPiece(board, position, movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, position));
                        movements.add(movement);
                    } else {
                        piecePosition = getPiece(board, position, movementSourcePosition, movementTargetPosition);
                        if (piecePosition != null && piecePosition.getColor() != color) {
                            movement = new ChessMovement();
                            movement.getSquares().addAll(getSquares(board, startPosition, position));
                            movements.add(movement);
                        }
                    }
                }

                // If king is not moved
                if (TAKABLE_POSITIONS != typePositions && !moved) {
                    // Test if small roque is possible
                    piecePosition = getPiece(board, getPosition(row.ordinal(), column.ordinal() + 3), movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null &&
                        ChessPieceType.ROOK == piecePosition.getType() &&
                        color == piecePosition.getColor() &&
                        !piecePosition.isMoved() &&
                        !isTargetPosition(versusTakableMovements, getPosition(row.ordinal(), column.ordinal())) &&
                        !isTargetPosition(versusTakableMovements, getPosition(row.ordinal(), column.ordinal() + 1)) &&
                        getPiece(board, getPosition(row.ordinal(), column.ordinal() + 1), movementSourcePosition, movementTargetPosition) == null &&
                        !isTargetPosition(versusTakableMovements, getPosition(row.ordinal(), column.ordinal() + 2)) &&
                        getPiece(board, getPosition(row.ordinal(), column.ordinal() + 2), movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, getPosition(row.ordinal(), column.ordinal() + 2)));
                        movements.add(movement);
                    }

                    // Test if big roque is possible
                    piecePosition = getPiece(board, getPosition(row.ordinal(), column.ordinal() - 4), movementSourcePosition, movementTargetPosition);
                    if (piecePosition != null &&
                        ChessPieceType.ROOK == piecePosition.getType() &&
                        color == piecePosition.getColor() &&
                        !piecePosition.isMoved() &&
                        !isTargetPosition(versusTakableMovements, getPosition(row.ordinal(), column.ordinal())) &&
                        !isTargetPosition(versusTakableMovements, getPosition(row.ordinal(), column.ordinal() - 1)) &&
                        getPiece(board, getPosition(row.ordinal(), column.ordinal() - 1), movementSourcePosition, movementTargetPosition) == null &&
                        !isTargetPosition(versusTakableMovements, getPosition(row.ordinal(), column.ordinal() - 2)) &&
                        getPiece(board, getPosition(row.ordinal(), column.ordinal() - 2), movementSourcePosition, movementTargetPosition) == null &&
                        getPiece(board, getPosition(row.ordinal(), column.ordinal() - 3), movementSourcePosition, movementTargetPosition) == null) {
                        movement = new ChessMovement();
                        movement.getSquares().addAll(getSquares(board, startPosition, getPosition(row.ordinal(), column.ordinal() - 2)));
                        movements.add(movement);
                    }
                }
                break;
        }

        List<ChessMovement> authorizedMovements = new ArrayList<ChessMovement>();
        if (TAKABLE_POSITIONS == typePositions) {
            authorizedMovements.addAll(movements);
        } else {
            // Check movements to remove forbidden movements (if king is chess
            // or mat after the movement)
            for (ChessMovement movementToCheck : movements) {
                if (!isForbiddenMovement(board, movementToCheck)) {
                    authorizedMovements.add(movementToCheck);
                }
            }
        }

        return authorizedMovements;
    }



    /**
     * Create a board
     * 
     * @return Board created
     */
    public ChessBoard createBoard() {
        ChessBoard board = new ChessBoard();

        // Create squares
        ChessColor colorRow = ChessColor.WHITE;
        for (ChessRow row : ChessRow.values()) {
            ChessColor colorCol = colorRow;
            for (ChessColumn column : ChessColumn.values()) {
                // Add square
                board.getSquares().add(createSquare(createPosition(row, column), colorCol));

                // Change column's color
                colorCol = getVersusColor(colorCol);
            }
            // Change row's color
            colorRow = getVersusColor(colorRow);
        }

        // Create black pieces
        ChessColor color = ChessColor.BLACK;
        int idPiece = 1;
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_1), ChessPieceType.ROOK, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_2), ChessPieceType.KNIGHT, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_3), ChessPieceType.BISHOP, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_4), ChessPieceType.QUEEN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_5), ChessPieceType.KING, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_6), ChessPieceType.BISHOP, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_7), ChessPieceType.KNIGHT, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_1, ChessColumn.COLUMN_8), ChessPieceType.ROOK, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_1), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_2), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_3), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_4), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_5), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_6), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_7), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_2, ChessColumn.COLUMN_8), ChessPieceType.PAWN, color));

        // Create white pieces
        color = ChessColor.WHITE;
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_1), ChessPieceType.ROOK, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_2), ChessPieceType.KNIGHT, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_3), ChessPieceType.BISHOP, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_4), ChessPieceType.QUEEN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_5), ChessPieceType.KING, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_6), ChessPieceType.BISHOP, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_7), ChessPieceType.KNIGHT, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_8, ChessColumn.COLUMN_8), ChessPieceType.ROOK, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_1), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_2), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_3), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_4), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_5), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_6), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_7), ChessPieceType.PAWN, color));
        board.getPieces().add(createPiece("P" + (idPiece++), createPosition(ChessRow.ROW_7, ChessColumn.COLUMN_8), ChessPieceType.PAWN, color));

        // Add players
        board.getPlayers().add(createPlayer(ChessColor.WHITE));
        board.getPlayers().add(createPlayer(ChessColor.BLACK));

        // Set current color
        board.setCurrentColor(ChessColor.WHITE);

        return board;
    }



    /**
     * Update state for each player from board
     * 
     * @param board
     *            Board
     */
    private void updatePlayersStates(ChessBoard board) {

        // Get current player
        ChessPlayer currentPlayer = getPlayer(board, board.getCurrentColor());

        // Get player's king piece, player's movements and all movements of
        // versus player's pieces
        ChessPiece kingPiece = null;
        List<ChessMovement> versusTakableMovements = new ArrayList<ChessMovement>();
        List<ChessMovement> playerMovements = new ArrayList<ChessMovement>();
        for (ChessPiece piece : board.getPieces()) {
            if (piece.getColor() != board.getCurrentColor()) {
                versusTakableMovements.addAll(getMovements(board, piece.getType(), piece.getColor(), piece.isMoved(), piece.getPosition().getRow(), piece.getPosition().getColumn(), TAKABLE_POSITIONS, null, null));
            } else {
                playerMovements.addAll(getMovements(board, piece.getType(), piece.getColor(), piece.isMoved(), piece.getPosition().getRow(), piece.getPosition().getColumn(), MOVABLE_POSITIONS, null, null));
                if (piece.getType() == ChessPieceType.KING) {
                    kingPiece = piece;
                }
            }
        }

        // If king can be taken by versus player
        if (isTargetPosition(versusTakableMovements, kingPiece.getPosition())) {
            if (playerMovements.isEmpty()) {
                // If no movement is possible for player, its state is MAT
                currentPlayer.setState(ChessPlayerState.MAT);
            } else {
                // Else, its state is CHESS
                currentPlayer.setState(ChessPlayerState.CHESS);
            }
        } else if (playerMovements.isEmpty()) {
            // If no movement is possible for player, all players are PAT
            getPlayer(board, ChessColor.WHITE).setState(ChessPlayerState.PAT);
            getPlayer(board, ChessColor.BLACK).setState(ChessPlayerState.PAT);
        } else if (board.getPieces().size() == 2) {
            // If only two kings are in the board, all players are PAT
            getPlayer(board, ChessColor.WHITE).setState(ChessPlayerState.PAT);
            getPlayer(board, ChessColor.BLACK).setState(ChessPlayerState.PAT);
        } else {
            // Else, its state is NORMAL
            currentPlayer.setState(ChessPlayerState.NORMAL);
        }
    }



    /**
     * Get board for a client
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * 
     * @return Board
     */
    public ChessBoard getBoard(String clientId, String partyId) {
        // Get informations from party
        InfosChess infosChess = getInfosParty(clientId, partyId);

        return infosChess.getBoard();
    }



    /**
     * Called to set the player for party representing by the color
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * @param color
     *            Player's color to become
     * @return Board
     */
    public ChessBoard setPlayer(String clientId, String partyId, ChessColor color) {
        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Get informations from party
        InfosChess infosChess = getInfosParty(clientId, partyId);

        // Do nothing if party is finished
        if (infosChess.getParty().isFinished()) {
            throw new RemoteException(RemoteError.ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY);
        }

        // If party is started, players can't be changed
        if (isGameStarted(infosChess)) {
            throw new RemoteException(RemoteError.ERR_CHESS_002_PLAYERS_NO_CHANGE);
        }

        // Get player identified by its color
        ChessPlayer player = getPlayer(infosChess.getBoard(), color);

        if (infosClient.getUserId().equals(player.getUserId())) {
            if(LOG.isDebugEnabled()){
                LOG.debug("setPlayer(): " + infosClient.getUserId() + " is no more the " + player.getColor() + " player"); 
            }
            // If user is already the player, free the player
            player.setUserId(null);
        } else if (getPlayer(infosChess, infosClient) != null) {
            // If user is already a player
            throw new RemoteException(RemoteError.ERR_CHESS_001_USER_ALREADY_PLAYER);
        } else if (!StrUtil.isEmpty(player.getUserId())) {
            // If player is not free
            throw new RemoteException(RemoteError.ERR_CHESS_003_PLAYER_ALREADY_TAKEN);
        } else {
            if(LOG.isDebugEnabled()){
                LOG.debug("setPlayer(): " + infosClient.getUserId() + " become the " + player.getColor() + " player"); 
            }
            // Set user for the player
            player.setUserId(infosClient.getUserId());
        }

        // Dispatch event PARTY_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, partyId));

        return infosChess.getBoard();
    }



    /**
     * Get movements for a piece
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * @param piece
     *            Piece
     * 
     * @return Movements
     */
    public List<ChessMovement> getMovements(String clientId, String partyId, ChessPiece piece) {
        List<ChessMovement> movements = new ArrayList<ChessMovement>();

        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Get informations from party
        InfosChess infosChess = getInfosParty(clientId, partyId);

        // Do nothing if party is finished
        if (infosChess.getParty().isFinished()) {
            throw new RemoteException(RemoteError.ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY);
        }

        // If party is not started, no movement
        if (!isGameStarted(infosChess)) {
            return movements;
        }

        // If piece is null, no movement
        if (piece == null) {
            return movements;
        }

        // Get player identified by piece's color
        ChessPlayer player = getPlayer(infosChess.getBoard(), piece.getColor());

        // If user is not the player, no movement
        if (!infosClient.getUserId().equals(player.getUserId())) {
            return movements;
        }

        // Add all movements
        movements.addAll(getMovements(infosChess.getBoard(), piece.getType(), piece.getColor(), piece.isMoved(), piece.getPosition().getRow(), piece.getPosition().getColumn(), MOVABLE_POSITIONS, null, null));

        // If no movement found, add a movement just with piece's position
        if (movements.isEmpty()) {
            ChessMovement movement = new ChessMovement();
            movement.getSquares().add(getSquare(infosChess.getBoard(), piece.getPosition()));
            movements.add(movement);
        }

        return movements;
    }



    /**
     * Called to move a piece to a position
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     * @param piece
     *            Piece to move
     * @param position
     *            Position where piece is moved
     * @return Board
     */
    public ChessBoard movePiece(String clientId, String partyId, ChessPiece piece, ChessPosition position) {
        // Get informations from client
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Get informations from party
        InfosChess infosChess = getInfosParty(clientId, partyId);

        // Do nothing if party is finished
        if (infosChess.getParty().isFinished()) {
            throw new RemoteException(RemoteError.ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY);
        }

        // If party is not started, no movement
        if (!isGameStarted(infosChess)) {
            return infosChess.getBoard();
        }

        // If piece is null, no movement
        if (piece == null) {
            return infosChess.getBoard();
        }

        // Get player identified by piece's color
        ChessPlayer player = getPlayer(infosChess.getBoard(), piece.getColor());

        // If user is not the player, no movement
        if (!infosClient.getUserId().equals(player.getUserId())) {
            return infosChess.getBoard();
        }

        // If bad piece's color
        if (piece.getColor() != infosChess.getBoard().getCurrentColor()) {
            throw new RemoteException(RemoteError.ERR_CHESS_004_MOVEMENT_FORBIDDEN);
        }

        // Get piece's movement from movements
        ChessMovement pieceMovement = null;
        for (ChessMovement movement : getMovements(clientId, partyId, piece)) {
            if (compare(getTargetPosition(movement), position) == 0) {
                pieceMovement = movement;
                break;
            }
        }

        // If no movement is found
        if (pieceMovement == null) {
            throw new RemoteException(RemoteError.ERR_CHESS_004_MOVEMENT_FORBIDDEN);
        }

        // Remove target versus player's piece
        ChessPiece targetPiece = getPiece(infosChess.getBoard(), getTargetPosition(pieceMovement));
        if (targetPiece != null && targetPiece.getColor() != infosChess.getBoard().getCurrentColor()) {
            infosChess.getBoard().getPieces().remove(targetPiece);
            infosChess.getBoard().getDefeatedPieces().add(targetPiece);
        }

        if(LOG.isDebugEnabled()){
            LOG.debug("movePiece(): " + infosClient.getUserId() + " moves the piece " + piece); 
        }

        // Do movement
        getPiece(infosChess.getBoard(), piece.getPosition()).setPosition(position);

        // Store last movement
        infosChess.getBoard().setLastMovement(pieceMovement);

        if (ChessPieceType.KING == piece.getType() &&
            position.getColumn().ordinal() - piece.getPosition().getColumn().ordinal() == 2) {
            // If small roque
            ChessPosition rookPosition = getPosition(piece.getPosition().getRow().ordinal(), piece.getPosition().getColumn().ordinal() + 3);
            ChessPiece rookPiece = getPiece(infosChess.getBoard(), rookPosition);
            rookPiece.setPosition(getPosition(position.getRow().ordinal(), position.getColumn().ordinal() - 1));
            rookPiece.setMoved(true);
        } else if (ChessPieceType.KING == piece.getType() &&
            piece.getPosition().getColumn().ordinal() - position.getColumn().ordinal() == 2) {
            // If big roque
            ChessPosition rookPosition = getPosition(piece.getPosition().getRow().ordinal(), piece.getPosition().getColumn().ordinal() - 4);
            ChessPiece rookPiece = getPiece(infosChess.getBoard(), rookPosition);
            rookPiece.setPosition(getPosition(position.getRow().ordinal(), position.getColumn().ordinal() + 1));
            rookPiece.setMoved(true);
        }

        // Change current color
        infosChess.getBoard().setCurrentColor(getVersusColor(infosChess.getBoard().getCurrentColor()));

        // Update state for each player
        updatePlayersStates(infosChess.getBoard());

        // Trigger player state can be changed
        triggerPlayerStateChanged(clientId, partyId);

        // Dispatch event PARTY_CHANGED
        dispatchEvent(createRemoteEvent(RemoteEventType.PARTY_CHANGED, partyId));

        return infosChess.getBoard();
    }



    @Override
    public void triggerJoinParty(InfosClient infosClient, InfosParty infosParty) {
        // Do nothing
    }



    @Override
    public void triggerLeaveParty(InfosClient infosClient, InfosParty infosParty) {
        // Get chess informations from party
        InfosChess infosChess = (InfosChess) infosParty;

        // If the user who has left is a player
        if (getPlayer(infosChess, infosClient) != null && isGameStarted(infosChess)) {
            // The party is aborted
            boParty.abortParty(infosClient.getClientId(), infosParty.getParty().getId());
        }
    }



    @Override
    public void triggerAbortParty(InfosClient infosClient, InfosParty infosParty) {
        // Get chess informations from party
        InfosChess infosChess = (InfosChess) infosParty;

        // Get player
        ChessPlayer player = getPlayer(infosChess, infosClient);

        // If the user who has left is a player
        if (player != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("triggerAbortParty(): Player " + player.getUserId() + " aborts the party !");
            }

            // The player lost the game (MAT)
            player.setState(ChessPlayerState.MAT);

            // Trigger player state has changed
            triggerPlayerStateChanged(infosClient.getClientId(), infosParty.getParty().getId());
        }
    }



    /**
     * Called when a player's state has changed
     * 
     * @param clientId
     *            Client id
     * @param partyId
     *            Party id
     */
    private void triggerPlayerStateChanged(String clientId, String partyId) {
        // Get informations from party
        InfosChess infosChess = getInfosParty(clientId, partyId);

        // Determinate if party is finished and if there are winner and loser
        List<String> winnersUsersIds = new ArrayList<String>();
        List<String> losersUsersIds = new ArrayList<String>();
        boolean partyToFinish = false;

        for (ChessPlayer player : infosChess.getBoard().getPlayers()) {
            switch (player.getState()) {
                case PAT :
                    // If a player is PAT party is finished 
                    partyToFinish = true;
                    // If ranked party there are 2 winners
                    if(infosChess.getParty().isRanked()){
                        if(LOG.isDebugEnabled()){
                            LOG.debug("triggerPlayerStateChanged(): Player " + player.getUserId() + " (" + player.getColor() + ") is PAT !");
                        }
                        winnersUsersIds.add(player.getUserId());
                    }
                    break;

                case MAT :
                    // If a player is MAT party is finished
                    partyToFinish = true;
                    // If ranked party there are a winner and a loser
                    if(infosChess.getParty().isRanked()){
                        if(LOG.isDebugEnabled()){
                            LOG.debug("triggerPlayerStateChanged(): Player " + player.getUserId() + " (" + player.getColor() + ") is MAT !");
                        }
                        losersUsersIds.add(player.getUserId());
                        winnersUsersIds.add(getPlayer(infosChess.getBoard(), getVersusColor(player.getColor())).getUserId());
                    }
                    break;

                default :
                    break;
            }
        }

        if (partyToFinish) {
            boParty.finishParty(clientId, partyId, winnersUsersIds, losersUsersIds);
        }
    }

}
