package com.dolphland.server.business.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Abstract DAO
 * 
 * @author JayJay
 * 
 */
abstract class AbstractDAO {

    protected JdbcTemplate jdbcTemplate;



    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
