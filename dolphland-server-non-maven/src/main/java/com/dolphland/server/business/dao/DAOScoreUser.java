package com.dolphland.server.business.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Score;
import com.dolphland.server.business.BOChess;
import com.dolphland.server.util.string.StrUtil;

/**
 * DAO for user's score
 * 
 * @author JayJay
 * 
 */
@Service
public class DAOScoreUser extends AbstractDAO {

    private static final Logger LOG = Logger.getLogger(DAOScoreUser.class);

    private final static String COLUMN_IDAPP = "IDAPP";
    private final static String COLUMN_IDUSER = "IDUSER";
    private final static String COLUMN_WIN = "WIN";
    private final static String COLUMN_LOST = "LOST";

    private final static String[] ALL_COLUMNS = new String[] {
    COLUMN_IDAPP,
    COLUMN_IDUSER,
    COLUMN_WIN,
    COLUMN_LOST
    };



    /**
     * Get scores from application's id
     * 
     * @param appId
     *            Application's id
     * 
     * @return Scores
     */
    public List<Score> getScores(ApplicationId appId) {
        StringBuffer query = new StringBuffer();
        query.append("select " + StrUtil.formatList(ALL_COLUMNS));
        query.append(" from SCOREUSER");
        query.append(" where " + COLUMN_IDAPP + " = '" + appId + "'");
        if(LOG.isDebugEnabled()){
            LOG.debug("getScores(): Execute query '" + query + "'");
        }
        return jdbcTemplate.query(query.toString(), new ScoreRowMapper());
    }



    /**
     * Get score from application's id and user's id
     * 
     * @param appId
     *            Application's id
     * @param userId
     *            User's id
     * 
     * @return Score
     */
    public Score getScore(ApplicationId appId, String userId) {
        StringBuffer query = new StringBuffer();
        query.append("select " + StrUtil.formatList(ALL_COLUMNS));
        query.append(" from SCOREUSER");
        query.append(" where " + COLUMN_IDAPP + " = '" + appId + "'");
        query.append(" and " + COLUMN_IDUSER + " = '" + userId + "'");
        if(LOG.isDebugEnabled()){
            LOG.debug("getScore(): Execute query '" + query + "'");
        }
        List<Score> scores = jdbcTemplate.query(query.toString(), new ScoreRowMapper());
        return scores == null || scores.isEmpty() ? null : scores.get(0);
    }



    /**
     * Increment score from application's id and user's id
     * 
     * @param appId
     *            Application's id
     * @param userId
     *            User's id
     * @param winToIncrement
     *            Win to increment
     * @param lostToIncrement
     *            Lost to increment
     */
    public synchronized void incrementScore(ApplicationId appId, String userId, int winToIncrement, int lostToIncrement) {
        StringBuffer query = new StringBuffer();
        if (getScore(appId, userId) == null) {
            // If insert
            query.append("insert into SCOREUSER");
            query.append(" values ('" + appId + "', '" + userId + "', " + winToIncrement + ", " + lostToIncrement + ")");
        } else {
            // If update
            query.append("update SCOREUSER");
            query.append(" set " + COLUMN_WIN + " = " + COLUMN_WIN + " + " + winToIncrement);
            query.append(" , " + COLUMN_LOST + " = " + COLUMN_LOST + " + " + lostToIncrement);
            query.append(" where " + COLUMN_IDAPP + " = '" + appId + "'");
            query.append(" and " + COLUMN_IDUSER + " = '" + userId + "'");
        }
        if(LOG.isDebugEnabled()){
            LOG.debug("incrementScore(): Execute query '" + query + "'");
        }
        jdbcTemplate.update(query.toString());
    }

    /**
     * Score row mapper
     * 
     * @author JayJay
     * 
     */
    private static class ScoreRowMapper implements RowMapper<Score> {

        @Override
        public Score mapRow(ResultSet rs, int line) throws SQLException {
            ScoreResultSetExtractor extractor = new ScoreResultSetExtractor();
            return extractor.extractData(rs);
        }
    }

    /**
     * Score results set extractor
     * 
     * @author JayJay
     * 
     */
    private static class ScoreResultSetExtractor implements ResultSetExtractor<Score> {

        @Override
        public Score extractData(ResultSet rs) throws SQLException {
            Score score = new Score();
            score.setApplicationId(ApplicationId.fromValue(rs.getString(COLUMN_IDAPP)));
            score.setUserId(rs.getString(COLUMN_IDUSER));
            score.setWin(rs.getInt(COLUMN_WIN));
            score.setLost(rs.getInt(COLUMN_LOST));
            return score;
        }
    }
}
