package com.dolphland.server.business.exception;

import com.dolphland.core.ws.data.RemoteError;
import com.dolphland.server.util.exception.AppException;

/**
 * Remote exception
 * 
 * @author JayJay
 * 
 */
public class RemoteException extends AppException {

    private RemoteError remoteError;



    public RemoteException(RemoteError remoteError) {
        super(remoteError.toString());
        this.remoteError = remoteError;
    }



    public RemoteException(RemoteError remoteError, Throwable cause) {
        super(remoteError.toString(), cause);
    }



    public RemoteError getRemoteError() {
        return remoteError;
    }
}
