package com.dolphland.server.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.RemoteEventType;
import com.dolphland.server.business.dao.DAOScoreUser;
import com.dolphland.server.context.ContextServer;
import com.dolphland.server.util.exception.AppBusinessException;

/**
 * Abstract business for a party
 * 
 * @author JayJay
 * 
 */
public abstract class AbstractBOParty extends AbstractBO {



    /**
     * Get party's informations from a client and a party
     * 
     * @param clientId
     *            Client's id
     * @param partyId
     *            Party's id
     * @return Party's informations
     */
    protected InfosParty getInfosParty(String clientId, String partyId) {
        return getInfosParty(clientId, partyId, false);
    }



    /**
     * Get party's informations from a client and a party. The indicator owner
     * is true if the client is the party's owner
     * 
     * @param clientId
     *            Client's id
     * @param partyId
     *            Party's id
     * @param owner
     *            Indicator owner. True if the client is the party's owner
     * 
     * @return Party's informations
     */
    protected InfosParty getInfosParty(String clientId, String partyId, boolean owner) {
        InfosClient infosClient = null;
        try {
            // Get informations from client
            infosClient = ContextServer.getInfosClient(clientId);

            // Get informations from party
            InfosParty infosParty = ContextServer.getInfosParty(partyId);

            // If indicator owner is true whereas user is not the owner
            if (owner && !infosClient.getUserId().equals(infosParty.getParty().getOwnerUserId())) {
                throw new AppBusinessException("The party's owner is not good !");
            }

            // If party's application is not enabled
            if (!getApplicationsPartiesEnabled().contains(infosParty.getParty().getApplicationId())) {
                throw new AppBusinessException("The party's application is disabled !");
            }

            return infosParty;
        } catch (AppBusinessException exc) {
            if (infosClient != null) {
                // Dispatch event PARTIES_CHANGED for the client to synchronize
                // parties
                dispatchEvent(infosClient, createRemoteEvent(RemoteEventType.PARTIES_CHANGED));
            }
            throw exc;
        }
    }



    /**
     * Get applications where the party is enabled
     * 
     * @return Applications
     */
    public abstract List<ApplicationId> getApplicationsPartiesEnabled();



    /**
     * Trigger the party joined by the client
     * 
     * @param infosParty
     *            Party's informations
     * @param infosClient
     *            Client's informations
     */
    public abstract void triggerJoinParty(InfosClient infosClient, InfosParty infosParty);



    /**
     * Trigger the party left by the client
     * 
     * @param infosParty
     *            Party's informations
     * @param infosClient
     *            Client's informations
     */
    public abstract void triggerLeaveParty(InfosClient infosClient, InfosParty infosParty);



    /**
     * Trigger the party aborted by the client
     * 
     * @param infosParty
     *            Party's informations
     * @param infosClient
     *            Client's informations
     */
    public abstract void triggerAbortParty(InfosClient infosClient, InfosParty infosParty);
}
