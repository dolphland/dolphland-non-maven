/*
 * $Log: PriorityQueue.java,v $
 * Revision 1.3 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.2 2009/04/03 09:07:50 bvatan
 * - genericisation
 * Revision 1.1 2009/03/05 15:02:57 bvatan
 * - initial check in
 */

package com.dolphland.server.util.event;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

/* not thread safe */
/**
 * Impélénente une file avec gestion des priorités et des filtres
 */
public class PriorityQueue<T> {

    static final Logger log = Logger.getLogger(PriorityQueue.class);

    private List<Itm>[] items;

    private int size;

    private List<PriorityQueueFilter<T>> filters;

    private PriorityQueueFilter<T> allInOneFilter;

    private T next;



    public PriorityQueue(int maxPriority) {
        items = new ArrayList[maxPriority];
        filters = new ArrayList<PriorityQueueFilter<T>>();
        allInOneFilter = allinOneFilter();
        for (int i = 0; i < items.length; i++) {
            items[i] = new ArrayList<Itm>();
        }
    }



    public void addFilter(PriorityQueueFilter<T> f) {
        if (f == null) {
            return;
        }
        filters.add(f);
        allInOneFilter = allinOneFilter();
        updateItms();
    }



    public void removeFilter(PriorityQueueFilter<T> f) {
        if (f == null) {
            return;
        }
        filters.remove(f);
        allInOneFilter = allinOneFilter();
        updateItms();
    }



    private PriorityQueueFilter<T> allinOneFilter() {
        return new PriorityQueueFilter<T>() {

            public int filter(T item) {
                int filterAction = PriorityQueueFilter.ACCEPT;
                for (Iterator<PriorityQueueFilter<T>> it = filters.iterator(); it.hasNext();) {
                    PriorityQueueFilter<T> filter = it.next();
                    filterAction = Math.max(filterAction, filter.filter(item));
                }
                return filterAction;
            }
        };
    }



    private void updateItms() {
        for (int i = items.length - 1; i >= 0; i--) {
            Iterator<Itm> it = items[i].iterator();
            List<Itm> toRemove = new ArrayList<Itm>();
            while (it.hasNext()) {
                Itm itm = it.next().update();
                if (itm.discard()) {
                    toRemove.add(itm);
                }
            }
            items[i].removeAll(toRemove);
            size -= toRemove.size();
        }
        next = peek();
    }



    public boolean hasNext() {
        return next != null;
    }



    public T peek() {
        for (int i = items.length - 1; i >= 0; i--) {
            if (items[i].size() == 0) {
                continue;
            }
            T out = null;
            Iterator<Itm> it = items[i].iterator();
            out = null;
            while (it.hasNext()) {
                Itm itm = it.next();
                if (!itm.filtered()) {
                    out = itm.value;
                    break;
                }
            }
            if (out != null) {
                return out;
            }
        }
        return null;
    }



    public T pop() {
        for (int i = items.length - 1; i >= 0; i--) {
            if (items[i].size() == 0) {
                continue;
            }
            T out = null;
            Iterator<Itm> it = items[i].iterator();
            List<Itm> removes = new ArrayList<Itm>();
            out = null;
            while (it.hasNext()) {
                Itm itm = it.next();
                if (!itm.filtered()) {
                    out = itm.value;
                    removes.add(itm);
                    break;
                } else {
                    if (itm.discard()) {
                        removes.add(itm);
                    }
                }
            }
            size -= removes.size();
            items[i].removeAll(removes);
            if (out != null) {
                next = peek();
                return out;
            }
        }
        return null;
    }



    public void queue(T item, int priority) {
        if (item == null) {
            throw new NullPointerException("item is null !"); //$NON-NLS-1$
        }
        if (priority > items.length - 1) {
            priority = items.length - 1;
        } else if (priority < 0) {
            priority = 0;
        }
        Itm itm = new Itm(item);
        if (itm.discard()) {
            return;
        }
        size++;
        items[priority].add(itm);
        next = peek();
    }



    public int size() {
        return size;
    }



    public void clear() {
        for (int i = 0; i < items.length; i++) {
            items[i].clear();
        }
    }



    public Iterator<T> iterator() {
        return new FilteredIteratorImpl(new PriorityQueueFilter<T>() {

            public int filter(T item) {
                return ACCEPT;
            }
        });
    }



    public Iterator<T> filteredIterator() {
        return iterator(allInOneFilter);
    }



    public Iterator<T> iterator(PriorityQueueFilter<T> filter) {
        if (filter == null) {
            return iterator();
        } else {
            return new FilteredIteratorImpl(filter);
        }
    }

    // iterator filtré
    private class FilteredIteratorImpl implements Iterator<T> {

        private Iterator<Itm>[] iterators;

        private PriorityQueueFilter<T> filter;

        private T peek;



        public FilteredIteratorImpl(PriorityQueueFilter<T> filter) {
            this.filter = filter;
            iterators = new Iterator[items.length];
            for (int i = items.length - 1; i >= 0; i--) {
                iterators[i] = items[i].iterator();
            }
            peek = peek();
        }



        private T peek() {
            for (int i = items.length - 1; i >= 0; i--) {
                while (iterators[i].hasNext()) {
                    Itm itm = iterators[i].next();
                    if (filter.filter(itm.value) == PriorityQueueFilter.ACCEPT) {
                        return itm.value;
                    }
                }
            }
            return null;
        }



        public boolean hasNext() {
            return peek != null;
        }



        public T next() {
            if (peek == null) {
                throw new NoSuchElementException();
            }
            T out = peek;
            peek = peek();
            return out;
        }



        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    private class Itm {

        T value;

        int filterAction = -1;

        boolean discard;

        boolean filter;



        Itm(T val) {
            value = val;
            update();
        }



        Itm update() {
            filterAction = filter(value);
            filter = filterAction == PriorityQueueFilter.FILTER || filterAction == PriorityQueueFilter.DISCARD;
            discard = filterAction == PriorityQueueFilter.DISCARD;
            return this;
        }



        boolean filtered() {
            return filter;
        }



        boolean discard() {
            return discard;
        }



        private int filter(T item) {
            return allInOneFilter.filter(item);
        }
    }
}
