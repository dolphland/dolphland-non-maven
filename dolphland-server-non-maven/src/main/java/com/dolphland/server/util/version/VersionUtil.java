package com.dolphland.server.util.version;

import java.io.IOException;
import java.util.Properties;

import com.dolphland.core.ws.data.ApplicationId;

/**
 * Version util
 * 
 * @author JayJay
 * 
 */
public class VersionUtil {

    /**
     * Get dolphland's version
     * 
     * @return Dolphland's version
     */
    public final static String getVersion() {
        return getVersion("DOLPHLAND");
    }



    /**
     * Get application's version
     * 
     * @param applicationId
     *            Application
     * @return Application's version
     */
    public final static String getVersion(ApplicationId applicationId) {
        return getVersion(applicationId.toString());
    }



    /**
     * Get application's version
     * 
     * @param applicationId
     *            Application
     * @return Application's version
     */
    private final static String getVersion(String applicationId) {
        try {
            Properties prop = new Properties();
            prop.load(new VersionUtil().getClass().getResourceAsStream("/versions.properties"));
            return (String) prop.get(applicationId);
        } catch (IOException e) {
            return "";
        }
    }
}
