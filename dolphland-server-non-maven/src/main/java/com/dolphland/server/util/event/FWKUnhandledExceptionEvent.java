/*
 * $Log: FWKUnhandledExceptionEvent.java,v $
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/27 07:25:09 bvatan
 * - initital check in
 */
package com.dolphland.server.util.event;

/**
 * Ev�nement transportant une exception qui n'a pas �t� trait�e dans
 * l'application.
 * 
 * @author JayJay
 */
public class FWKUnhandledExceptionEvent extends Event {

    private Throwable unhandledException;



    public FWKUnhandledExceptionEvent(Throwable e) {
        super(FwkEvents.FWK_UNHANDLED_EXCEPTION_EVENT);
        this.unhandledException = e;
    }



    /** Retourne l'exception non g�r�e */
    public Throwable getUnhandledException() {
        return unhandledException;
    }

}
