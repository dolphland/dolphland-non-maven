/*
 * $Log: EventMulticaster.java,v $
 * Revision 1.13 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.12 2010/11/12 15:47:54 bvatan
 * - Ajout support des schedules de type crontab
 * Revision 1.11 2009/06/22 07:38:30 bvatan
 * - ajout services d'ajouts d'interceptors
 * Revision 1.10 2009/03/27 07:56:01 bvatan
 * - mise � niveau suivante passagedes id EventDestination en long
 * Revision 1.9 2009/03/24 13:53:01 bvatan
 * - gestion des schedules
 * Revision 1.8 2009/03/10 12:40:47 bvatan
 * - remplacemenet appel Event.getId() par Event.getDestination()
 * Revision 1.7 2009/03/05 15:02:43 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.6 2009/02/19 08:36:00 bvatan
 * - ajout postEventAndWait()
 * Revision 1.5 2009/01/29 15:09:34 bvatan
 * - instanciation singleton dans bloc initializer
 * Revision 1.4 2008/12/03 13:30:21 bvatan
 * - gestion des�v�nements barri�re
 * Revision 1.3 2007/05/07 14:54:16 bvatan
 * - Ajout et gestion du service postEventAndWait()
 * Revision 1.2 2007/04/20 10:49:26 bvatan
 * - Le Multicaster ne d�marre plus automatiquement
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.1 2006/12/04 13:26:40 bvatan
 * - initial check in
 */
package com.dolphland.server.util.event;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.log4j.Logger;

/**
 * Multicaster d'�v�nements.<br>
 * 
 * Cas d'utilisation commun :<br>
 * 
 * <pre>
 *  // D�finition d'une nouvelle destination
 *  public static final EventDestination EVENT_DESTINATION = new EventDestination(&quot;MY_DESTINATION&quot;);
 *  
 *  // R�f�rence du Multicasteur
 *  Event multicaster multicaster = EventMulticaster.getInstance();
 *  // D�marrage du multicasteur
 *  multicaster.start();
 *  
 *  // Abonnement d'un Subscriber
 *  multicaster.subscribe(new MySubscriber()).forEvents(EVENT_DESTINATION);
 *  
 *  // Emission d'un �v�nement vers la destination pr�c�dement cr��e
 *  Event evt = new Event(EVENT_DESTINATION);
 *  multicaster.postEvent(evt);
 *  
 *  // Emission d'un �v�nement vers une destination particuli�re
 *  EventDestination MY_DEST = ...;
 *  Event evt = new Event(EVENT_DESTINATION);
 *  // MY_DEST surcharge EVENT_DESTINATION qui est port�e par evt.
 *  multicaster.postEvent(evt, MY_DEST);
 *  
 *  // Arr�t du multicasteur
 *  multicaster.stop();
 *  
 *  --------------------------------------------------
 *  //Subscriber
 *  public class MySubscriber extends DefaultSubscriber {
 *  
 *  	public void eventReceived(Event evt) {
 *  		System.out.println(&quot;received event &quot;+evt);
 *  	}
 *  
 *  }
 * </pre>
 * 
 * @author JayJay
 */
public class EventMulticaster {

    private final static Logger log = Logger.getLogger(EventMulticaster.class);

    private static EventMulticaster singleton;

    private EventDispatcher eventDispatcher;

    private EventDispatchThread dispatchThread;

    static {
        singleton = new EventMulticaster();
    }



    private EventMulticaster() {
        eventDispatcher = new EventDispatcherImpl();
        dispatchThread = new EventDispatchThread(eventDispatcher);
    }



    public static EventMulticaster getInstance() {
        return singleton;
    }



    /**
     * D�marre le multicasteur
     */
    public void start() {
        dispatchThread.start();
    }



    /**
     * Arr�te le multicasteur.
     */
    public void stop() {
        dispatchThread.stop();
    }



    public void addEventInterceptor(EventInterceptor interceptor) {
        dispatchThread.addEventInterceptor(interceptor);
    }



    public void removeEventInterceptor(EventInterceptor interceptor) {
        dispatchThread.removeEventInterceptor(interceptor);
    }



    public EventRegisterer subscribe(EventSubscriber el) {
        return eventDispatcher.subscribe(el);
    }



    public EventRegisterer unsubscribe(EventSubscriber el) {
        return eventDispatcher.unsubscribe(el);
    }



    public void removeAllListener(EventSubscriber el) {
        eventDispatcher.unsubscribeFromAll(el);
    }



    public void postEvent(Event evt) {
        postEvent(evt, evt.getDestination());
    }



    /**
     * Ajoute une planification d'�v�nement. Un cas d'utilisation possible est
     * le suivant :<br>
     * 
     * <b>Exemple 1: Programmation de l'�mission d'un �v�nement pour �tre �mis
     * 20 fois par interval d'une seconde</b>
     * 
     * <pre>
     * 	Schedule sched = EventMulticaster.getInstance().schedule(new MyEvent().schedule().count(20).interval(1000));
     * 	// Fil du programme
     * 	...
     * 	// Destruction de la programmation
     * 	sched.destroy();
     * </pre>
     * 
     * <b>Exemple 2: Programmation de l'�mission d'un �v�nement pour �tre �mis
     * par interval de 2 secondes vers une destination particuli�re</b>
     * 
     * <pre>
     * 	// obtention de la destination de l'�v�nement � programmer
     * 	EventDestination destination = ...;
     * 	// programmation de l'�mission toutes les 2 secondes vers destination
     * 	Schedule sched = EventMulticaster.getInstance().schedule(new MyEvent().schedule(destination).interval(1000));
     * 	// Fil du programme
     * 	...
     * 	// destruction de la programmation
     * 	sched.destroy();
     * </pre>
     * 
     * @param sched
     *            La planification � ajouter (contient l'�v�nement)
     * @return Le Schedule ajout�.
     */
    public Schedule schedule(Schedule sched) {
        return dispatchThread.schedule(sched);
    }



    /**
     * Programme l'�mission d'un �v�nement en fonction d'une expression crontab
     * encapsul�e par le<br>
     * param�tre sp�cifi�.
     * 
     * @param sched
     *            Crit�res de d�clenchement de l'�v�nement.
     */
    void schedule(CrontabSchedule sched) {
        dispatchThread.schedule(sched);
    }



    /**
     * Poste l'�v�nement sp�cifi� vers la destination sp�cifi�e. La destination
     * sp�cifi�e surcharge la destination port�e par l'�v�nement.<br>
     * 
     * @param evt
     *            L'�v�nement � poster
     * @param dest
     *            La destination vers laquelle l'�v�nement sera �mis.
     */
    public void postEvent(Event evt, EventDestination dest) {
        if (dest == null) {
            throw new NullPointerException("dest is null !"); //$NON-NLS-1$
        }
        if (!dispatchThread.isRunning()) {
            log.info("postEvent(): EventMulticaster is not started !"); //$NON-NLS-1$
        }
        dispatchThread.postEvent(evt, dest);
    }



    public void postEventAndWait(Event evt) throws InterruptedException {
        postEventAndWait(evt, evt.getDestination());
    }



    public void postEventAndWait(Event evt, EventDestination dest) throws InterruptedException {
        if (dest == null) {
            throw new NullPointerException("dest is null !"); //$NON-NLS-1$
        }
        if (!dispatchThread.isRunning()) {
            log.info("postEventAndWait(): EventMulticaster is not started !"); //$NON-NLS-1$
        }
        dispatchThread.postEventAndWait(evt, dest);
    }



    /**
     * Bloque le thread courant jusqu'� r�ception de <u><b>tous</b></u> les
     * EventId sp�cifi�s
     * 
     * @param millis
     *            Le nombre de millisecondes d'attente maximum (z�ro = attendre
     *            ind�finiment)
     * @param evtIds
     *            La liste des EventId � attendre
     * @return La liste des Event re�us qui sont indentif�s par la liste des
     *         EventId attendus
     * @throws InterruptedException
     *             Si le thread est interrompu pendant l'attente
     */
    public List<Event> waitForAll(long millis, final EventDestination... evtIds) throws InterruptedException {
        List<Event> out = null;
        class Lock {

            List<Event> evts = new Vector<Event>();
        }
        final Lock lock = new Lock();
        EventSubscriber esb = new DefaultEventSubscriber() {

            private Map<Long, Boolean> received = new TreeMap<Long, Boolean>();

            private int countReceived = 0;
            {
                for (int i = 0; i < evtIds.length; i++) {
                    received.put(new Long(evtIds[i].id()), false);
                }
            }



            public void eventReceived(Event e) {
                Boolean alreadyReceived = received.get(e.getDestination().id());
                if (alreadyReceived != null) {
                    if (!alreadyReceived.booleanValue()) {
                        countReceived++;
                    }
                    received.put(new Long(e.getDestination().id()), true);
                    synchronized (lock) {
                        lock.evts.add(e);
                    }
                }
                if (countReceived == evtIds.length) {
                    synchronized (lock) {
                        lock.notify();
                    }
                }
            }
        };
        EventRegisterer er = subscribe(esb);
        for (int i = 0; i < evtIds.length; i++) {
            er.forEvents(evtIds[i]);
        }
        synchronized (lock) {
            lock.wait(millis);
            out = lock.evts;
        }
        unsubscribe(esb).forAllEvents();
        return out;
    }



    /**
     * M�me comportement que
     * <code>&lt;T&gt; postEventAndWait(Event, EventId, Class&lt;T&gt;, long)</code>
     * mais ne retourne jamais sans avoir re�u un �v�nement qui correspond �
     * EventId et � la classe attendue.<br>
     * Eviquivalent � <code>postEventAndWait(evt,eid,eclass,<b>0</b>)</code>
     */
    public <T extends Event> T postEventAndWait(Event evt, final EventDestination eid, final Class<T> eclass) throws InterruptedException {
        return postEventAndWait(evt, eid, eclass, 0);
    }



    /**
     * Poste un �v�nement et bloque le thread appelant jusqu'� r�ception d'un
     * �v�nement de type T et dont l'EventId est celui sp�cifi� en param�tre. La
     * classe de l'�v�nement attendu doit �tre une sous classes de T ou T elle
     * m�me.<br>
     * Le thread appelant est bloqu� dans la limite de millis millisecondes sauf
     * si millis vaut z�ro, dans ce cas le thread appelant bloque ind�finiment
     * jusqu'� r�ception de l'�v�nement d'id sp�cifi�.
     * 
     * @param <T>
     *            Le type de l'�v�nement attendu
     * @param evt
     *            L'�v�nement � poster
     * @param eid
     *            L'EventId de l'�v�nement � attendre
     * @param eclass
     *            La classe de l'�v�nement � attendre (T ou une sous class de T)
     * @param millis
     *            Le nombre de millisecodnes � attendre avant d'abandonner. Si
     *            z�ro, pas d'abandon.
     * @return Le premier �v�nement re�u dont l'id est celui attendu et dont le
     *         type est assignable � T.
     * @throws InterruptedException
     *             Si le thread appelant est interrompu pendant l'attente.
     */
    public <T extends Event> T postEventAndWait(Event evt, final EventDestination eid, final Class<T> eclass, long millis) throws InterruptedException {
        T out;
        class Lock {

            T waited;
        }
        final Lock lock = new Lock();
        EventSubscriber esb = new DefaultEventSubscriber() {

            public void eventReceived(Event e) {
                if (eclass.isAssignableFrom(e.getClass())) {
                    synchronized (lock) {
                        lock.waited = (T) e;
                        lock.notify();
                    }
                }
            }
        };
        subscribe(esb).forEvents(eid);
        postEvent(evt);
        try {
            synchronized (lock) {
                if (lock.waited == null) {
                    lock.wait(millis);
                }
                out = lock.waited;
            }
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
            throw ie;
        } finally {
            unsubscribe(esb).forAllEvents();
        }
        return out;
    }



    /**
     * Bloque le thread courant jusqu'� ce qu'au moins un des EventId sp�cifi�s
     * soit re�u
     * 
     * @param millis
     *            Le nombre de millisecondes d'attente maximum (z�ro = attendre
     *            ind�finiment)
     * @param evtIds
     *            La liste des EventIds � attendre
     * @return Le Event re�us, c'est � dire le premier re�u parmis ceux attendus
     * @throws InterruptedException
     */
    public Event waitForAny(long millis, final EventDestination... evtIds) throws InterruptedException {
        Event out = null;
        class Lock {

            Event evt = null;
        }
        final Lock lock = new Lock();
        EventRegisterer er = subscribe(new DefaultEventSubscriber() {

            public void eventReceived(Event e) {
                synchronized (lock) {
                    lock.evt = e;
                    lock.notify();
                }
            }
        });
        for (int i = 0; i < evtIds.length; i++) {
            er.forEvents(evtIds[i]);
        }
        synchronized (lock) {
            lock.wait(millis);
            out = lock.evt;
        }
        return out;
    }



    public Event waitForAny(final EventDestination... evtIds) throws InterruptedException {
        return waitForAny(0, evtIds);
    }



    public List<Event> waitForAll(EventDestination... evtIds) throws InterruptedException {
        return waitForAll(0, evtIds);
    }



    public Event waitFor(EventDestination evtid, long millis) throws InterruptedException {
        List<Event> evts = waitForAll(millis, evtid);
        if (evts.size() > 0) {
            return evts.get(0);
        }
        return null;
    }



    public Event waitFor(EventDestination evtid) throws InterruptedException {
        return waitFor(evtid, 0);
    }

}
