/*
 * $Log: ScheduleInfo.java,v $
 * Revision 1.2 2014/03/10 16:28:30 bvatan
 * - Internationalized
 * Revision 1.1 2009/02/03 11:35:20 bvatan
 * - initial check in
 */

package com.dolphland.server.util.task;

import java.util.Date;

import com.dolphland.server.util.event.Event;

/**
 * Structrure qui contient les informations n�cessaires pour effectuer une
 * demande de programmation d'�v�nement au <code>TaskManager</code>.<br>
 * <br>
 * Parmi les diff�rentes informations port�es apr cet objet on trouve :<br>
 * <ul>
 * <li>startDate : Date � partir de laquelle la programmation s'active</li>
 * <li>endDate : Date � partir de laquelle la programmation se d�sactive</li>
 * <li>repeatCount : Nombre d'activation de la programmation entre startDate et
 * endDate</li>
 * <li>repeatInterval : Interval de temps (en millisecondes) entre 2 activations
 * </li>
 * <li>event : Ev�nement post� � chaque activation de la programmation</li>
 * </ul>
 * <br>
 * startDate et endDate sont optionnels :<br>
 * <br>
 * <ul>
 * <li>Si startDate est null, c'est date de demande de programmation qui fait
 * fois, c'est � dire que kla programamtion est active immediatement.</li>
 * <li>Si endDate est null, la programmation reste valide ind�finiement (jusqu'�
 * ce que repeatCount soit atteind)</li>
 * <li>Si endDate est ant�rieur � startDate, la demande de programmation est non
 * valdie et sera ignor�e</li>
 * </ul>
 * 
 * @author JayJay
 */
public class ScheduleInfo {

    public static final int REPEAT_FOREVER = -1;

    private volatile Date startDate;

    private volatile Date endDate;

    private volatile int repeatCount;

    private volatile long repeatInterval;

    private volatile Event event;



    /**
     * Construit une demander de programamtion d'�v�nement pour le TaskManager.<br>
     * 
     * @param event
     *            L'�v�nement � programmer.
     */
    private ScheduleInfo(Event event) {
        if (event == null) {
            throw new NullPointerException("event is null !"); //$NON-NLS-1$
        }
        this.event = event;
    }



    /**
     * Retourne l'�v�nement � poster � chaque activation.
     */
    public Event getEvent() {
        return event;
    }



    public synchronized Date getStartDate() {
        return startDate;
    }



    /**
     * Date � partir de laquelle l'�v�nement commencera � �tre post�
     */
    public synchronized ScheduleInfo startDate(Date startDate) {
        if (startDate != null && endDate != null) {
            if (startDate.after(endDate)) {
                throw new IllegalArgumentException("startDate is later than endDate !"); //$NON-NLS-1$
            }
        }
        this.startDate = startDate;
        return this;
    }



    public synchronized Date getEndDate() {
        return endDate;
    }



    /**
     * Date � partir de laquelle l'�v�nement ne sera plus post�
     */
    public synchronized ScheduleInfo endDate(Date endDate) {
        if (startDate != null && endDate != null) {
            if (startDate.after(endDate)) {
                throw new IllegalArgumentException("startDate is later than endDate !"); //$NON-NLS-1$
            }
        }
        this.endDate = endDate;
        return this;
    }



    public int getRepeatCount() {
        return repeatCount;
    }



    /**
     * D�finit le nombre d'activations, c'est � dire le nombre de fois o�
     * l'�v�nement sera post�. Utiliser REPEAT_FOREVER pour obtenir une requ�te
     * r�currente � l'infini.<br>
     */
    public ScheduleInfo repeatCount(int repeatCount) {
        if (repeatCount < 0 && repeatCount != REPEAT_FOREVER) {
            throw new IllegalArgumentException("Use repeatCount > 0 or repeatCount == REPEAT_FOREVER !"); //$NON-NLS-1$
        }
        this.repeatCount = repeatCount;
        return this;
    }



    public ScheduleInfo repeatForever() {
        return repeatCount(REPEAT_FOREVER);
    }



    public long getRepeatInterval() {
        return repeatInterval;
    }



    /**
     * Minimum 10 ms.<br>
     * Plus les intervals sont petit, moins le syst�me est en mesure de la
     * honorer, en particulier les valeurs en dessous de 1000 millisecondes.
     */
    public ScheduleInfo repeatInterval(long repeatInterval) {
        if (repeatInterval < 10) {
            throw new IllegalArgumentException("Use a repeatInterval >= 250 ms !"); //$NON-NLS-1$
        }
        this.repeatInterval = repeatInterval;
        return this;
    }



    public static final ScheduleInfo forEvent(Event evt) {
        return new ScheduleInfo(evt);
    }

}
