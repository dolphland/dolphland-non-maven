/*
 * $Log: EventSubscriber.java,v $
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.1 2006/12/04 13:26:40 bvatan
 * - initial check in
 */
package com.dolphland.server.util.event;

/**
 * D�finit l'intefrace des subscriber d'�v�nements.<br>
 * 
 * Les impl�mentations sont notifi�es des �v�nements pour lesquels elles se sont
 * enregistr�es.
 * 
 * @author JayJay
 */
public interface EventSubscriber {

    /**
     * Invoqu� lorqu'un �v�nement est d�livr�
     * 
     * @param e
     *            L'�v�nement d�livr�
     */
    void eventReceived(Event e);

}
