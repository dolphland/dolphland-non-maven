package com.dolphland.server.util.event;

import java.util.Calendar;

import com.dolphland.server.util.exception.AppInfrastructureException;
import com.kenai.crontabparser.CronTabExpression;

/**
 * Encapsule les informations de planification de l'�mission d'un �v�nement par
 * une expression de type crontab (version UNIX)
 * 
 * @author JayJay
 */
public class CrontabSchedule {

    private CronTabExpression crontabExp;

    private EventDestination destination;

    private Event event;

    private volatile boolean garbage;

    private volatile Calendar lastFiredTime;



    /**
     * Construit un schedule � partir d'une expression crontab de type UNIX.
     * 
     * @param expression
     *            Expression crontab de type UNIX
     * @param e
     *            Ev�nement � poster
     */
    CrontabSchedule(String expression, Event e) {
        if (expression == null) {
            throw new NullPointerException("expression is null !"); //$NON-NLS-1$
        }
        if (e == null) {
            throw new NullPointerException("e is null !"); //$NON-NLS-1$
        }
        try {
            crontabExp = CronTabExpression.parse(expression);
        } catch (Exception ex) {
            throw new AppInfrastructureException("Unable to build schedule !", ex); //$NON-NLS-1$
        }
        this.event = e;
        this.destination = e.getDestination();
    }



    void setDestination(EventDestination destination) {
        this.destination = destination;
    }



    EventDestination getDestination() {
        return destination;
    }



    Event getEvent() {
        return event;
    }



    boolean isTriggerable() {
        // on ne d�clenche pas plus d'une fois par minute (secondes non g�r�es
        // par crontab parser)
        Calendar now = Calendar.getInstance();
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return crontabExp.matches(now) && (lastFiredTime == null || !lastFiredTime.equals(now));
    }



    public void destroy() {
        garbage = true;
    }



    public boolean isGarbage() {
        return garbage;
    }



    public synchronized void fired() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        lastFiredTime = cal;
    }



    public synchronized long getLastFiredTime() {
        return lastFiredTime.getTimeInMillis();
    }
}
