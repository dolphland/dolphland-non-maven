/*
 * $Log: EventRequest.java,v $
 * Revision 1.1 2009/02/03 11:35:19 bvatan
 * - initial check in
 */

package com.dolphland.server.util.task;

import com.dolphland.server.util.event.Event;

class EventRequest extends Request {

    private Event evt;



    EventRequest(Event evt) {
        this.evt = evt;
    }



    public Event getEvt() {
        return evt;
    }

}
