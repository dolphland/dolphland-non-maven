package com.dolphland.server.util.task;

import org.springframework.util.Assert;

class RunnableRequest extends Request {

    private Runnable job;



    RunnableRequest(Runnable job) {
        Assert.notNull(job, "null job provided !"); //$NON-NLS-1$
        this.job = job;
    }



    Runnable getJob() {
        return job;
    }

}
