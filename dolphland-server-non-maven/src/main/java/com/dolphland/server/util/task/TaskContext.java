/*
 * $Log: TaskContext.java,v $
 * Revision 1.2 2009/04/01 09:33:13 jscafi
 * - Résolution des warnings
 * Revision 1.1 2009/02/03 11:35:20 bvatan
 * - initial check in
 */

package com.dolphland.server.util.task;

public class TaskContext {

    // private volatile EventRegisterer eventRegisterer;

    // private volatile EventRegisterer eventDeregisterer;

    TaskContext() {

    }
    /*
     * void setEventRegistrerer(EventRegisterer registerer) {
     * this.eventRegisterer = registerer; }
     * void setEventDeregisterer(EventRegisterer eventDeregisterer) {
     * this.eventDeregisterer = eventDeregisterer; }
     * public EventRegisterer getEventRegisterer() { return eventRegisterer; }
     * public EventRegisterer getEventDeregisterer() { return eventDeregisterer;
     * }
     */
}
