/*
 * $Log: ScheduleRequest.java,v $
 * Revision 1.1 2009/02/03 11:35:20 bvatan
 * - initial check in
 */

package com.dolphland.server.util.task;

import com.dolphland.server.util.event.Event;

class ScheduleRequest extends Request {

    private volatile Event event;



    void setEvent(Event event) {
        this.event = event;
    }



    Event getEvent() {
        return event;
    }

}
