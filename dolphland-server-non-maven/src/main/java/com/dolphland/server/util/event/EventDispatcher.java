/*
 * $Log: EventDispatcher.java,v $
 * Revision 1.2 2009/03/05 15:00:22 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.1 2006/12/04 13:26:40 bvatan
 * - initial check in
 */
package com.dolphland.server.util.event;

/**
 * 
 * Un EventDispatcher est responsable de la d�livrance des �v�nements ainsi que
 * de l'enregistrement/d�senregistrement des subscribers souhaitant recevoir les
 * �v�nements.
 * 
 * @author JayJay
 */
interface EventDispatcher {

    /**
     * Cr�� un contexte d'enregistrement et retourne un EventRegister pour la
     * d�finition des �v�nements devant �tre transmis au subscriber sp�cifi�
     * 
     * @param s
     *            Le subscriber qui doit recevoir les �v�nements
     * 
     * @return Le EventRegister permettant de d�finir quels �v�nements sont �
     *         transmettre au subscriber
     */
    public EventRegisterer subscribe(EventSubscriber s);



    /**
     * Cr�� un contexte de d�senregistrement et retourne un EventRegister pour
     * la d�finition des �v�nements devant �tre d�tach�s du subscriber sp�cifi�
     * 
     * @param s
     *            Le subscriber qui ne doit plus recevoir les �v�nements
     * 
     * @return Le EventRegister permettant de d�finir quels �v�nements ne sont
     *         plus � transmettre au subscriber.
     */
    public EventRegisterer unsubscribe(EventSubscriber s);



    /**
     * D�sactive la distribution de tous les �v�nements pour le subscriber
     * sp�cifi�. Apr�s appel de ce service pour un subscriber, ce dernier ne
     * recevra plus aucun �v�nement.
     * 
     * @param s
     *            Le subscriber qui ne doit plus recevoir aucun �v�nement.
     */
    public void unsubscribeFromAll(EventSubscriber s);



    /**
     * Distribue l'�v�nement sp�cifi� � tous les subscriber enregistr�s pour le
     * recevoir.
     * 
     * @param evt
     *            L'�v�nement � distribuer
     * @param evtDest
     *            La destination de l'�v�nement
     */
    public void dispatchEvent(Event evt, EventDestination evtDest);

}
