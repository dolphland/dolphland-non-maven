package com.dolphland.server.util.assertion;

import com.dolphland.server.util.exception.AppInfrastructureException;
import com.dolphland.server.util.string.StrUtil;

/**
 * Assert util
 * 
 * @author JayJay
 * 
 */
public class AssertUtil {

    /**
     * Load an exception with a message if the object is null
     * 
     * @param in
     *            Object
     * @param message
     *            Message
     */
    public static void notNull(Object in, String message) {
        if (in == null) {
            throw new NullPointerException(message);
        }
    }



    /**
     * Load an exception with a message if the string is empty
     * 
     * @param in
     *            Object
     * @param message
     *            Message
     */
    public static void notEmpty(String in, String message) {
        if (StrUtil.isEmpty(in)) {
            throw new AppInfrastructureException(message);
        }
    }
}
