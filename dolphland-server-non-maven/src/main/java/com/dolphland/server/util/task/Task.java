/*
 * $Log: Task.java,v $
 * Revision 1.19 2014/03/10 16:28:30 bvatan
 * - Internationalized
 * Revision 1.18 2013/08/01 11:03:12 bvatan
 * - FWK-98: Make it possible to queue an Event or a Runnable directly to the
 * Task
 * Revision 1.17 2013/04/30 09:38:50 bvatan
 * - FWK-58: added support for ThreadDeath Error catch and log
 * Revision 1.16 2010/05/04 14:57:54 bvatan
 * - passage de deamon en volatile (pb sur aix)
 * Revision 1.15 2009/06/17 14:18:00 bvatan
 * - ajout du mode deamon
 * Revision 1.14 2009/04/17 13:34:55 bvatan
 * - ajotu du mode daemon
 * Revision 1.13 2009/04/17 08:03:57 bvatan
 * - correction du bug sur waitForDeath() qui provoquait l'attente infinie de
 * l'arr�t de la t�che
 * - les t�ches sont daemon
 * Revision 1.12 2009/04/03 09:15:20 bvatan
 * - ajout taskEventReceived()
 * Revision 1.11 2009/03/24 13:53:37 bvatan
 * - suppression du TaskManager
 * Revision 1.10 2009/03/10 12:42:05 bvatan
 * - Task fourni le service getMulticaster()
 * - Le TaskManager n'est plus obligatoire pour que la t�che puisse d�marrer
 * Revision 1.9 2009/03/06 12:50:30 bvatan
 * - ajout gestion des filtres d'�v�nements
 * Revision 1.8 2009/03/05 15:13:13 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.7 2009/03/05 15:06:33 bvatan
 * - Mise � niveau EventId -> EventDestination
 * - Refonte gestion des requ�tes avec PriorityQueue
 * Revision 1.6 2009/03/02 12:27:47 bvatan
 * - ajout indicateur busy
 * - ajout javadoc
 * Revision 1.5 2009/02/27 16:36:55 bvatan
 * - ajout service getMulticaster()
 * - affinement de la notification des requ�tes lorsqu'elles sont termin�es
 * Revision 1.4 2009/02/27 14:36:40 bvatan
 * - ajout traces de logs
 * Revision 1.3 2009/02/19 08:33:43 bvatan
 * - ajout waitForDeath()
 * Revision 1.2 2009/02/05 14:23:21 bvatan
 * - ne met plus en cache la r�f�rence du multicaster, utilise celui du manager.
 * Revision 1.1 2009/02/03 11:35:20 bvatan
 * - initial check in
 */

package com.dolphland.server.util.task;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.dolphland.server.util.event.DefaultEventSubscriber;
import com.dolphland.server.util.event.EDT;
import com.dolphland.server.util.event.Event;
import com.dolphland.server.util.event.EventDestination;
import com.dolphland.server.util.event.EventMulticaster;
import com.dolphland.server.util.event.EventRegisterer;
import com.dolphland.server.util.event.FwkUncaughtExceptionHandler;
import com.dolphland.server.util.event.PriorityQueue;
import com.dolphland.server.util.event.PriorityQueueFilter;
import com.dolphland.server.util.event.ReversePolymorhInvoker;

/**
 * D�finit une t�che asynchrone qui traite des demandes.
 * 
 * @author JayJay
 */
public class Task implements Comparable<Task> {

    private static final Logger log = Logger.getLogger(Task.class);

    /** indicateur de vie de la t�che */
    private volatile boolean shouldRun;

    /** thread de la t�che */
    private volatile Thread thread;

    /** runner, implements Runnable */
    private Runner runner = new Runner();

    /** nom de la t�che */
    private volatile String name;

    /** monitor de blocage pour les demandes, pause, sleep etc. */
    private Object monitor = new Object();

    /**
     * monitor pour l'activit� de la t�che (une t�che est active lorsqu'elle
     * traite des demandes)
     */
    private Object activeMonitor = new Object();

    /**
     * monitor pour le rechargement de la t�che.
     */
    private Object reloadMonitor = new Object();

    /**
     * monitor pour l'arr�t de la t�che
     */
    private Object stopMonitor = new Object();

    /**
     * indique si la t�che est compl�tement arr�t�e ou pas
     */
    private volatile boolean dead = false;

    /** indique une demande de mise en pause */
    private volatile boolean pause = false;

    /** indique que la t�che est active */
    private volatile boolean active = false;

    /** indique si une demande d'endormissement a �t� effectu�e */
    private volatile boolean sleep = false;

    /** indique si une demande d'arr�t a �t� effectu�e */
    private volatile boolean stop = false;

    /** indique si une demande de rechargement a �t� effectu�e */
    private volatile boolean reload = false;

    private volatile boolean busy = false;

    /** liste des demande � traiter */
    private PriorityQueue<Request> requests;

    /** liste des EventDestination auxquels la t�che est abonn�e */
    private Set<EventDestination> eventDestinations = Collections.synchronizedSortedSet(new TreeSet<EventDestination>());

    /**
     * r�ceptionne les �v�nements avant de les transformer en demande pour la
     * t�che
     */
    private EventWatcher eventWatcher;

    /** Identifiant de la t�che */
    private EventDestination taskDestination;

    /** Liste des filtres d'�v�nements appliqu�s sur la t�che */
    private HashMap<Class<? extends Event>, PriorityQueueFilter<Request>> eventFilters;

    /**
     * Indque si la t�che est un daemon
     */
    private volatile boolean daemon;



    /**
     * Construit une t�che avec le nom sp�cifi�. Le nom fourni doit identifier
     * la t�che de mani�re unique.
     * 
     * @param name
     *            Le nom de la t�che.
     */
    public Task(String name) {
        if (name == null) {
            throw new NullPointerException("name is null !"); //$NON-NLS-1$
        }
        eventFilters = new HashMap<Class<? extends Event>, PriorityQueueFilter<Request>>();
        requests = new PriorityQueue<Request>(Event.MAX_SYS_PRIORITY);
        eventWatcher = new EventWatcher();
        shouldRun = false;
        this.name = name;
        this.dead = true;
        this.daemon = true;
        this.taskDestination = new EventDestination();
    }



    /**
     * D�finit la t�che comme daemon. Une t�che deamon n'emp�che pas la JVM de
     * terminer si elle n'est pas arr�t�. Si la t�che est toujours vivante au
     * moment ou la JVM doit terminer, cette derni�re terminera sans attendre la
     * fin de la t�che.<br>
     * Lorsque la t�che n'est pas daemon (par d�faut), la JVM ne se termine pas
     * tant que la t�che est active.<br>
     * NB: L'appel � setDaemon() doit s'effectuer avant l'appel � start(), sinon
     * l'appel sera sans effet et la t�che sera d'office daemon.
     */
    public void setDaemon(boolean daemon) {
        this.daemon = daemon;
    }



    /**
     * Retourne le nom de la t�che
     */
    public final String getName() {
        return name;
    }



    /**
     * Retourne l'EventDestination que la t�che utilise pour recevoir les
     * �v�nements
     */
    public EventDestination getEventDestination() {
        return taskDestination;
    }



    /**
     * D�marre la t�che
     */
    public final void start() {
        synchronized (monitor) {
            if (thread != null) {
                return;
            }
            busy = false;
            stop = false;
            sleep = false;
            pause = false;
            reload = false;
            shouldRun = true;
            synchronized (activeMonitor) {
                active = true;
            }
            thread = new Thread(runner, getName());
            thread.setDaemon(daemon);
            thread.setUncaughtExceptionHandler(new FwkUncaughtExceptionHandler());
            thread.start();
        }
    }



    /**
     * Demande � la t�che de s'arr�ter. La t�che terminera les traitements en
     * cours et toutes les requ�tes en attente de traitement mais n'en prendra
     * plus de nouvelles<br>
     * <br>
     * Cette m�thode est non bloquante, la demande d'arr�t est effectu�e et la
     * m�thode retourne sans attendre l'arr�t de la t�che.
     */
    public final void stop() {
        stop(false);
    }



    /**
     * Demande � la t�che de s'arr�ter imm�diatement. La t�che purgera toutes
     * les demandes en attente et terminera au plus t�t.<br>
     * </br> Cette m�thode est non bloquante, la demande d'ar�t est effectu�e et
     * la m�thode retourne sans attendre l'arr�t de la t�che
     */
    public final void stopImmediate() {
        stop(true);
    }



    private final void stop(boolean immediate) {
        synchronized (monitor) {
            if (thread == null) {
                return;
            }
            pause = false;
            sleep = false;
            stop = true;
            reload = false;
            if (immediate) {
                if (log.isInfoEnabled()) {
                    log.info("stop(): Stop requested from [" + getName() + "], " + requests.size() + " unhandled requests"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
                // quitter au plus t�t, ne pas laisser la boucle poursuivre.
                shouldRun = false;
                // purger les demandes non trait�es
                requests.clear();
                // interrompre le thread, sortir des pause, sleep et compagnie
                thread.interrupt();
            } else {
                if (log.isInfoEnabled()) {
                    log.info("stop(): Stop requested for [" + getName() + "], processing " + requests.size() + " remaining requests."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
                // d�j� � true, juste pour insister. Laisser la boucle active et
                // attendre la fin des demandes. nextRequest() l�vera
                // InterruptedException une fois termin�.
                shouldRun = true;
                // notifier et quitter en terminant l'encours.
                monitor.notifyAll();
            }
        }
    }



    /**
     * Attend que la t�che soit compl�tement termin�e avant de retourner. Si la
     * t�che est d�j� arr�t�, la m�thdoe retourne imm�diatement.
     * 
     * @throws InterruptedException
     *             Si le thread appelant a �t� interrompu pendant l'attente.
     */
    public void waitForDeath() throws InterruptedException {
        waitForDeath(0);
    }



    /**
     * Attend que la t�che soit compl�tement arr�t�e dans un d�lais d'au moins
     * millis millisecondes.
     * 
     * @param millis
     *            Le nombre de millisecondes � attendre avant d'abandonner
     * @throws InterruptedException
     *             Si le thread appelant a �t� interrompu.
     */
    public void waitForDeath(long millis) throws InterruptedException {
        long waitTime = millis;
        synchronized (stopMonitor) {
            while (!dead && waitTime >= 0) {
                if (log.isDebugEnabled()) {
                    log.debug("waitForDeath(): Waiting for task [" + getName() + "] to die... (" + waitTime + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
                long start = System.currentTimeMillis();
                stopMonitor.wait(waitTime);
                waitTime -= System.currentTimeMillis() - start;
                if (waitTime <= 0) {
                    break;
                }
            }
        }
    }



    /**
     * Indique si la t�che est morte, c'est � dire si elle est compl�tement
     * arr�t�. Une t�che n'est pas compl�tement arr�t� lorsqu'elle est en train
     * de s'arr�ter. Des demandes restantes � tra�ter peuvent prolonger le temps
     * de vie de la t�che.
     * 
     * @return
     */
    public boolean isDead() {
        return dead;
    }



    /**
     * Met la t�che en pause. Provoque l'arr�t du d�pilement des requ�tes dans
     * la file d'attente de la t�che. Les �v�nements et requ�tes ne sont pas
     * perdus, continus d'�tre accept�s et restent dans la file d'attente de la
     * t�che, ils seront trait�s apr�s le prochain wakeup() ou stop().<br>
     * <br>
     * Cette m�thode bloque jusqu'� ce que la t�che soit effectivement mise en
     * pause, c'est � dire qu'elle n'est plus active. Note: il est de la
     * responsabilit� des sous classes d'effectuer un appel � doPause() dans les
     * longs traitements qu'elles effectuent. Sans appel � doPause(), le
     * traitement en cours se terminera avant que la t�che ne se mette en pause.
     */
    public final void pauseAndWait() throws InterruptedException {
        pause(true);
    }



    /**
     * Met la t�che en pause mais n'attend pas que la t�che soit effectivement
     * en pause pour retourner. Si la t�che effectue un traitement, il continue
     * et cette m�thode retourne imm�diatement. Voire
     * <code>pauseAndWait()</code>
     */
    public final void pause() {
        try {
            pause(false);
        } catch (InterruptedException ie) {
            // ignorer, n'est pas lev�e en mode non bloquant.
        }
    }



    private final void pause(boolean wait) throws InterruptedException {
        if (log.isDebugEnabled()) {
            log.debug("pause(): Pause requested for [" + getName() + "]"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        synchronized (monitor) {
            if (pause) {
                log.info("pause(): Task is already paused, request ignored."); //$NON-NLS-1$
                return;
            }
            pause = true;
            monitor.notify();
        }
        if (wait) {
            synchronized (activeMonitor) {
                while (active) {
                    activeMonitor.wait();
                }
            }
        }
    }



    /**
     * Suspend la t�che jusqu'au prochain wakeup() ou stop(). Les demandes en
     * cours non tra�t�s dans la file d'attente de la t�che ne sont pas perdues.
     * Elle seront trait�es au prochain wakeup() ou stop().<br>
     * Attention : si <code>stopImmediate()</code> est appel�e apr�s
     * <code>sleep()</code>, les demandes seront ignor�es. <br>
     * <br>
     * Cette m�thode est non bloquante, la demande de suspension est effectu�e
     * et la m�thode retourne sans attendre la suspension.
     */
    public final void sleep() {
        sleep(false);
    }



    /**
     * Purge les demandes en attente et suspend la t�che jusqu'au prochain
     * wakeup() ou stop(). Les demandes non tra�t�es dans la file d'attente de
     * la t�che au moment de la suspension seront purg�es et ignor�es.<br>
     * Pendant la suspension, la t�che n'acc�pte plus de demandes, elles sont
     * toutes ignor�es (donc ne seront jamais tra�t�es).<br>
     * Au prochain <code>wakeup()</code> la t�che reprend son traitement normal
     * et acc�pte de nouveau les demandes.<br>
     * <br>
     * <br>
     * Cette m�thode est non bloquante, la demande de suspension est effectu�e
     * et la m�thode retourne sans attendre la suspension.<br>
     * NB: <code>sleepImmediate()</code> suivi de <code>stop()</code> est
     * �quivalent � <code>stopImmediate()</code>
     */
    public final void sleepImmediate() {
        sleep(true);
    }



    private final void sleep(boolean immediate) {
        if (log.isDebugEnabled()) {
            log.debug("sleep(): Sleep requested for [" + getName() + "]"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        synchronized (monitor) {
            if (stop) {
                log.debug("sleep(): Task is stopping, sleep request ignored."); //$NON-NLS-1$
                return;
            }
            if (sleep) {
                log.debug("sleep(): Task is already sleeping, request ignored."); //$NON-NLS-1$
                return;
            }
            sleep = true;
            if (immediate) {
                requests.clear();
            }
        }
    }



    /**
     * Bloque la t�che jusqu'� ce qu'elle soit sortie du mode pause par un
     * wakeup().
     * 
     * @throws InterruptedException
     *             Si la t�che est interrompue (par <code>stop()</code> ou
     *             <code>stopImmediate()</code>) pendant sa pause.
     */
    protected final void doPause() throws InterruptedException {
        synchronized (monitor) {
            while (pause) {
                if (log.isInfoEnabled()) {
                    log.info("doPause(): Task [" + getName() + "] suspended... (" + requests.size() + " remaining requests"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
                synchronized (activeMonitor) {
                    active = false;
                    activeMonitor.notifyAll();
                }
                monitor.wait();
            }
            synchronized (activeMonitor) {
                active = true;
                activeMonitor.notifyAll();
            }
        }
    }



    /**
     * Abonne la t�che � un �v�nement. Une fois enregistr�e, la t�che recevra
     * les �v�nements dont l'EventId a �t� sp�cifi� par ce service.
     * 
     * @param eid
     *            L'eventId � enregistrer
     */
    protected void registerEvent(EventDestination eid) {
        if (eid == null) {
            return;
        }
        eventDestinations.add(eid);
    }



    /**
     * Appel� au lancement de la t�che, c'est dans ce service que la t�che doit
     * s'aobonner aux �v�nements qu'elle souhaite recevoir.
     * 
     * @param ctx
     *            Le contexte de la t�che
     */
    protected void init(TaskContext ctx) {
    }



    /**
     * Appel� juste avant que la t�che ne s'arr�te. Lorsque cet appel a eu lieu,
     * plus aucun �v�nement n'est transmi � la t�che.
     * 
     * @param ctx
     *            Le contexte de la t�che
     */
    protected void destroy(TaskContext ctx) {
    }



    /**
     * Indique si la t�che est endormie ou en attente de fin du traitement en
     * cours pour s'endormir
     */
    public final boolean isSleeping() {
        return sleep;
    }



    /**
     * Indique si la t�che est en pause ou en attente de fin du traitement en
     * cours pour se mettre en pause.
     */
    public final boolean isPausing() {
        return pause;
    }



    /**
     * Indique si la t�ch est en cours de rechargement.
     */
    public final boolean isReloading() {
        return reload;
    }



    /**
     * Indique si la t�che est en cours de traitement d'une demande. Attention,
     * cet indicateur est ind�pendant de l'indicateur de pause. Si la t�che est
     * en pause, il se peut qu'elle soit �galement busy (en pause au milieu d'un
     * traitement). De m�me, il se peut qu'elle ne soit pas busy, mais en pause
     * quand m�me, donc non r�ceptive.
     * 
     * @return true si la t�che est en occup�e, false sinon.
     */
    public boolean isBusy() {
        return busy;
    }



    /**
     * Recharge la t�che. R�initialise la t�che en d�clenchant
     * <code>destroy()</code> suivi de <code>init()</code> sur la t�che.<br>
     * <br>
     * La t�che termine d'abord le traitement en cours avant de se recharger. <br>
     * La t�che continue d'accepter les demandes pendant le rechargement, de ce
     * fait elle ne perd aucune demande effectu�e pendant le rechargement. La
     * t�che reprend le traitement des demandes apr�s avoir termin� le
     * rechargement.<br>
     * Cette m�thode bloque jusqu'� se que le rechargement soit termin�.
     */
    public final void reloadAndWait() throws InterruptedException {
        reload(true);
    }



    /**
     * Version non bloquante de <code>reloadAndWait()</code>. Cette m�thode
     * n'attend pas que la t�che ait termin� son rechargement.
     */
    public final void reload() {
        try {
            reload(false);
        } catch (InterruptedException ie) {
            // ignorer, n'est pas lev�e si wait==false;
        }
    }



    private final void reload(boolean wait) throws InterruptedException {
        synchronized (monitor) {
            if (stop) {
                log.info("reload(): Task is stoping, reload request ignored."); //$NON-NLS-1$
                return;
            }
            if (reload) {
                log.info("reload(): Task is already reloading, request ignored."); //$NON-NLS-1$
                return;
            }
            reload = true;
            requests.queue(new ReloadRequest(), Event.MAX_SYS_PRIORITY);
        }
        if (wait) {
            synchronized (reloadMonitor) {
                while (reload) {
                    reloadMonitor.wait();
                }
            }
        }
    }



    /**
     * R�veille la t�che si elle �tait en pause ou endormie.
     */
    public final void wakeup() {
        synchronized (monitor) {
            pause = false;
            sleep = false;
            if (log.isDebugEnabled()) {
                log.debug("wakeup(): Wakeup request for [" + getName() + "]"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            monitor.notify();
        }
    }



    /**
     * Effectue une demande � la t�che.<br>
     * Si la t�che est en cours d'arr�t, la demande sera ignor�e
     * silencieusement.<br>
     * Si la t�che est endormie, la demande sera ignor�e silencieusement.<br>
     * 
     * @param request
     *            La requ�te � traiter par la t�che
     */
    final void request(Request request) {
        if (request == null) {
            return;
        }
        synchronized (monitor) {
            // ne pas accepter de demande quand la t�che est endormie
            if (sleep)
                return;
            // ne pas accepter de demande quand la t�che est en cours d'arr�t ou
            // n'est pas d�marr�e
            if (!shouldRun)
                return;
            if (stop)
                return;
            requests.queue(request, request.getPriority());
            if (!pause) {
                // on notify rien si on est en pause, attendre wakeup() ou
                // stop().
                monitor.notify();
            }
        }
        try {
            taskEventReceived();
        } catch (Exception e) {
            log.error("request(): taskEventReceived() has thrown an exception !", e); //$NON-NLS-1$
        }
    }



    /**
     * R�cup�re la prochaine demande � traiter par la t�che. Attend si la t�che
     * est en pause ou endormie.
     * 
     * @throws InterruptedException
     *             Si la t�che est interrompue.
     */
    private final Request nextRequest() throws InterruptedException {
        synchronized (monitor) {
            Request next = null;
            while ((next = requests.pop()) == null || pause || sleep) {
                if (pause) {
                    if (log.isInfoEnabled()) {
                        log.info("nextRequest(): Task [" + getName() + "] paused..."); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                }
                if (sleep) {
                    if (log.isInfoEnabled()) {
                        log.info("nextRequest(): Task [" + getName() + "] suspended..."); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                }
                if (stop && next == null) {
                    // une demande d'arr�t a �t� effectu�e et il n'y a plus de
                    // demande en attente.
                    throw new InterruptedException("No more remaining request, stopping."); //$NON-NLS-1$
                }
                if (pause || sleep) {
                    active = false;
                    synchronized (activeMonitor) {
                        activeMonitor.notifyAll();
                    }
                }
                monitor.wait();
                if (stop) {
                    if (log.isInfoEnabled()) {
                        log.info("nextRequest(): Stopping in progress, processing " + requests.size() + " remaining requests..."); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                }
            }
            synchronized (activeMonitor) {
                active = true;
                activeMonitor.notifyAll();
            }
            log.debug("nextRequest(): get the next request to be processed"); //$NON-NLS-1$
            return next;
        }
    }



    /**
     * Retourne la liste des �v�nements en attente de traitement par la t�che.
     * Seuls les �v�nements comptatibles (c'est � dire des sous classes) avec le
     * type sp�cifi� sont retourn�s.
     * 
     * @param clazz
     *            La type parent des �v�nements � retourner.
     * @return Un ArrayList<Event> contenant les �v�nements trouv�s.
     */
    public final <T extends Event> List<T> getPendingEvents(Class<T> clazz) {
        List<T> out = new ArrayList<T>();
        synchronized (monitor) {
            for (Iterator<Request> it = requests.iterator(); it.hasNext();) {
                Request r = it.next();
                if (r instanceof EventRequest) {
                    EventRequest er = (EventRequest) r;
                    Event e = er.getEvt();
                    if (clazz.isAssignableFrom(e.getClass())) {
                        out.add((T) e);
                    }
                }
            }
        }
        return out;
    }



    /**
     * Applique un filtre sur les �v�nements en attente dans la file des
     * demandes de la t�che.<br>
     * Les �v�nements filtr�s ne sont pas supprim�s de la liste, il ne sont
     * simplements plus tansmis � la t�che jusqu'� ce que le filtre soit
     * d�sactiv�.<br>
     * Les autres �v�nements en attente dans la t�che qui ne sont pas filtr�s
     * seront trait�s comme si les �v�nements filtr�s n'avaient jamais exist�s.<br>
     * <br>
     * Apr�s d�sactivation du filtre, les �v�nements filtr�s r�apparaisent dans
     * la file de la t�che et dans l'ordre o� ils ont �t� ajout�s.
     * 
     * @param clazz
     *            Le type des �v�nements � filtrer. Attention, tous les sous
     *            types du type sp�cifi� seront filtr�s.
     */
    public final <T extends Event> void filterEvents(final Class<T> clazz) {
        synchronized (monitor) {
            if (eventFilters.get(clazz) == null) {
                PriorityQueueFilter<Request> filter = new PriorityQueueFilter<Request>() {

                    public int filter(Request item) {
                        if (item instanceof EventRequest) {
                            EventRequest er = (EventRequest) item;
                            Event e = er.getEvt();
                            if (clazz.isAssignableFrom(e.getClass())) {
                                return FILTER;
                            }
                        }
                        return ACCEPT;
                    }
                };
                if (log.isDebugEnabled()) {
                    log.debug("filterEvents(): Adding filter for " + clazz); //$NON-NLS-1$
                }
                eventFilters.put(clazz, filter);
                requests.addFilter(filter);
            }
        }
    }



    /**
     * Supprime le filtrage des �v�nements pour le type sp�cifi�.
     * 
     * @param clazz
     *            Le type des �v�nements � ne plus filtrer.
     */
    public final <T extends Event> void unfilterEvents(Class<T> clazz) {
        synchronized (monitor) {
            PriorityQueueFilter<Request> filter = eventFilters.remove(clazz);
            if (filter != null) {
                if (log.isDebugEnabled()) {
                    log.debug("unfilterEvents(): Removing filter for " + clazz); //$NON-NLS-1$
                }
                requests.removeFilter(filter);
            }
        }
    }



    /**
     * <p>
     * Execute the specified {@link Runnable} within the thread of this task.
     * The job will be queued in this task priority queue and executed according
     * to its specified priority.
     * </p>
     * <p>
     * This method can be useful to ensure that a job is executed within the
     * task thread especially when the tasl has just started. If some events are
     * posted shortly after the task has started they might be missed by the
     * task because is hasn't got the chance to register for this event
     * destination before it is posted. in such case it might be helpfull to
     * demand that such code is executed directly to the task.
     * </p>
     * 
     * @param job
     *            The {@link Runnable} to be executed within this task thread.
     *            Null not allowed
     * @param priority
     *            The execution priority of the specified job as defined by the
     *            {@link Event} class.<br>
     *            Example: {@link Event#DFT_PRIORITY} would set priority to
     *            default.
     */
    public void queue(Runnable job, int priority) {
        RunnableRequest rr = new RunnableRequest(job);
        // m�me syst�me de priorit� que pour les �v�nements (m�mes valeurs
        // de priorit�s)
        rr.setPriority(priority);
        request(rr);
    }



    /**
     * <p>
     * Handles the specified {@link Event} within the thread of this task like
     * if this event had been posted though the {@link EventMulticaster} The job
     * will be queued the specified in this task priority queue and handle it
     * according to its priority like if the event has been posted throuigh the
     * {@link EventMulticaster}.
     * </p>
     * <p>
     * This method can be useful to ensure that an event is handled by the task
     * event is the task is not completely started yet.<br>
     * When the task starts it may not handle all events that are posted since
     * the {@link Task#start()} call. Some event might be missed because the
     * task takes some time to start and register for these events.<br>
     * in such cases it might be usefull to ensure that event will actually end
     * in the task queue by invoking this service.
     * </p>
     * 
     * @param e
     *            The {@link Event} to be handled within this task thread.
     *            Null not allowed
     */
    public void queue(Event e) {
        EventRequest er = new EventRequest(e);
        // m�me syst�me de priorit� que pour les �v�nements (m�mes valeurs
        // de priorit�s)
        er.setPriority(e.getPriority());
        request(er);
    }

    /**
     * Capte les �v�nements auquels la t�che s'est abonn�e.
     */
    private final class EventWatcher extends DefaultEventSubscriber {

        @Override
        public void eventReceived(Event e) {
            queue(e);
        }
    }

    /**
     * Boucle principale de la t�che
     * 
     * @author JayJay
     */
    private final class Runner implements Runnable {

        public void run() {
            dead = false;
            TaskContext ctx = new TaskContext();
            try {
                if (log.isInfoEnabled()) {
                    log.info("run(): Task [" + getName() + "] started"); //$NON-NLS-1$ //$NON-NLS-2$
                }
                // abonnement de la t�che aux �v�nements
                EventRegisterer er = EDT.subscribe(eventWatcher);
                for (EventDestination edest : eventDestinations) {
                    er.forEvents(edest);
                }
                er.forEvents(taskDestination);
                // initialisation de la t�che
                try {
                    init(ctx);
                } catch (Exception e) {
                    log.error("run(): Task [" + getName() + "] raised an exception in init() !", e); //$NON-NLS-1$ //$NON-NLS-2$
                }
                ReversePolymorhInvoker invoker = new ReversePolymorhInvoker();
                while (shouldRun) {
                    Request request = null;
                    try {
                        request = nextRequest();
                        if (log.isDebugEnabled()) {
                            log.debug("run(): Task [" + getName() + "] handles new request"); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                        busy = true;
                        if (request instanceof EventRequest) {
                            Event evt = ((EventRequest) request).getEvt();
                            if (log.isDebugEnabled()) {
                                log.debug("run(): Task [" + getName() + "] handles EventRequest for event " + evt); //$NON-NLS-1$ //$NON-NLS-2$
                            }
                            invoker.invoke(Task.this, "handleEvent", evt.getClass(), evt); //$NON-NLS-1$
                            if (log.isDebugEnabled()) {
                                log.debug("run(): Task [" + getName() + "] EventRequest completed"); //$NON-NLS-1$ //$NON-NLS-2$
                            }
                        } else if (request instanceof ReloadRequest) {
                            if (log.isDebugEnabled()) {
                                log.debug("run(): Task [" + getName() + "] handles ReloadRequest"); //$NON-NLS-1$ //$NON-NLS-2$
                            }
                            try {
                                destroy(ctx);
                                init(ctx);
                            } catch (Exception e) {
                                log.error("run(): Task [" + getName() + "] raised an exception while reloading !", e); //$NON-NLS-1$ //$NON-NLS-2$
                                throw e;
                            } finally {
                                synchronized (monitor) {
                                    synchronized (reloadMonitor) {
                                        // fin du rechargement
                                        reload = false;
                                        reloadMonitor.notifyAll();
                                    }
                                }
                            }
                        } else if (request instanceof RunnableRequest) {
                            Runnable job = ((RunnableRequest) request).getJob();
                            job.run();
                        }
                        if (log.isDebugEnabled()) {
                            log.debug("run(): Task [" + getName() + "] handled request successfully"); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                        request.done();
                    } catch (InterruptedException ie) {
                        if (log.isInfoEnabled()) {
                            log.info("run(): Task [" + getName() + "] interrupted"); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                        shouldRun = false;
                    } catch (InvocationTargetException ite) {
                        Throwable cause = ite.getCause();
                        if (cause instanceof InterruptedException) {
                            if (log.isInfoEnabled()) {
                                log.info("run(): Task [" + getName() + "] interrupted"); //$NON-NLS-1$ //$NON-NLS-2$
                            }
                            shouldRun = false;
                        } else {
                            log.error("run(): Task [" + getName() + "] request failed !", ite); //$NON-NLS-1$ //$NON-NLS-2$
                            request.fail(cause);
                        }
                    } catch (Exception e) {
                        log.error("run(): Task [" + getName() + "] request failed !", e); //$NON-NLS-1$ //$NON-NLS-2$
                        request.fail(e);
                    } finally {
                        if (request != null) {
                            request.signal();
                        }
                        busy = false;
                    }
                }
            } finally {
                // destruction de la t�che
                try {
                    destroy(ctx);
                } catch (Exception e) {
                    log.error("run(): Task [" + getName() + "] raised an exception in destroy() !", e); //$NON-NLS-1$ //$NON-NLS-2$
                }
                // d�sabonnement de tous les �v�nements
                EDT.unsubscribe(eventWatcher).forAllEvents();
            }
            if (log.isInfoEnabled()) {
                log.info("run(): Task [" + getName() + "] stoped"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            thread = null;
            synchronized (stopMonitor) {
                dead = true;
                stopMonitor.notifyAll();
            }
        }
    }



    /**
     * Appel� en dernier ressort si l'�v�nement � traiter n'a pu �tre trait�
     * dans la t�che
     * 
     * @param e
     *            L'�v�nement � traiter
     * @throws InterruptedException
     *             si la t�che a �t� interrompue pendant le traitement de
     *             l'�v�nement
     */
    protected void handleEvent(Event e) throws InterruptedException {
        if (log.isInfoEnabled()) {
            log.info("handleEvent(): Event " + e + " has not been treated by the task !"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }



    /**
     * Appel� lorsque la t�che a re�ue un �v�nement � traiter. Cette m�thode
     * n'est appel�e depuis le thread de la t�che, le traitement qu'elle
     * contient doit �tre correctement synchronis�.<br>
     * Les impl�mentations ne devraient utiliser ce service que lorsqu'elles ont
     * absolument besoin d'�tre notifi�es d�s r�ception d'un �v�nement afin de
     * retester l'�tat de la t�che.
     */
    protected void taskEventReceived() {

    }



    @Override
    public final boolean equals(Object obj) {
        if (obj == null)
            return false;
        if (obj instanceof Task) {
            Task other = (Task) obj;
            return this.compareTo(other) == 0;
        }
        return false;
    }



    public final int compareTo(Task o) {
        if (o == null)
            return 1;
        Task other = (Task) o;
        return name.compareTo(other.name);
    }

}
