/*
 * $Log: EventInterceptorException.java,v $
 * Revision 1.1 2009/06/22 07:30:40 bvatan
 * - initial check in
 */

package com.dolphland.server.util.event;

import com.dolphland.server.util.exception.AppInfrastructureException;

public class EventInterceptorException extends AppInfrastructureException {

    public EventInterceptorException() {
        super();
    }



    public EventInterceptorException(String message, Throwable cause) {
        super(message, cause);
    }



    public EventInterceptorException(String message) {
        super(message);
    }



    public EventInterceptorException(Throwable cause) {
        super(cause);
    }

}
