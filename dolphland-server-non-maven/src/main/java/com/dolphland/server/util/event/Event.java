/*
 * $Log: Event.java,v $
 * Revision 1.6 2014/03/10 16:15:19 bvatan
 * - Internationalized
 * Revision 1.5 2009/03/24 13:52:47 bvatan
 * - mise � jour javadoc
 * - MAX_SYS_PRIORITY passe � 6 au lieu de 5
 * Revision 1.4 2009/03/05 15:00:05 bvatan
 * - mise � niveau EventId -> EventDestination
 * - mise � jour ojavadoc
 * Revision 1.3 2009/01/30 15:10:52 bvatan
 * - Serializable
 * Revision 1.2 2008/07/07 11:30:43 bvatan
 * - Ajout des �v�nements de UI (UIEvent)
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.3 2006/12/08 15:00:45 bvatan
 * - test les eventId n�gatifs dans le constructeur
 * Revision 1.2 2006/12/07 10:54:20 bvatan
 * - correction bug multithreading. Les threads postants un �v�nement via
 * postAndWait() pouvaient �tre notifi�s alors que l'�v�nement post� n'avait pas
 * encore �t� d�livr�.
 * Revision 1.1 2006/12/04 13:26:40 bvatan
 * - initial check in
 */
package com.dolphland.server.util.event;

import java.io.Serializable;

/**
 * Classe de base de tout �v�nement d�livr� par EventMulticaster.<br>
 * <br>
 * Un �v�nement est constitu� d'une destination par d�faut. Cette destination
 * qui constitue la destination � laquelle l'�v�nement sera distribu� si la
 * destination n'est pas sp�cifi�e au moment de l'envoi.<br>
 * <br>
 * <b>Exemple 1:</b><br>
 * 
 * <pre>
 * EventDestination DEFAULT_DEST = new EventDestination();
 * Event evt = new Event(DEFAULT_DEST);
 * EventMulticaster.getInstance().postEvent(evt);
 * </pre>
 * 
 * Dans l'exemple ci-dessus, l'�v�nement sera distribu� � la destination
 * DEFAULT_DEST qui est port�e par l'�v�nement.<br>
 * <br>
 * <b>Exemple 2:</b><br>
 * 
 * <pre>
 * EventDestination DEFAULT_DEST = new EventDestination();
 * EventDestination EXPLICIT_DEST = new EventDestination();
 * Event evt = new Event(DEFAULT_DEST);
 * EventMulticaster.getInstance().postEvent(evt, EXPLICIT_DEST);
 * </pre>
 * 
 * Dans l'exemple ci-dessus, l'�v�nement sera distribu� � la destination
 * EXPLICIT_DEST et non � DEFAULT_DEST. Cette derni�re sera simplement ignor�e. <br>
 * <br>
 * 
 * @author JayJay
 */
public class Event implements Serializable {

    /* priorit� des �v�nements applicatifs */
    public static final int MIN_PRIORITY = 0;

    public static final int DFT_PRIORITY = 2;

    public static final int MAX_PRIORITY = 4;

    /* priorit� des �v�nements syst�me : toujours plus prioritaires */
    public static final int MIN_SYS_PRIORITY = 5;

    public static final int MAX_SYS_PRIORITY = 6;

    /**
     * Type d'�v�nement standard, sera dispatch� par l'EventDispatchThread
     */
    static final int DEFAULT_EVENT_TYPE = 0;

    /**
     * type d'�v�nement graphique, sera dispatch� par l'EDT AWT/Swing
     * AwtEventQueue-x
     */
    static final int UI_EVENT_TYPE = 1;

    volatile EventDestination destination;

    volatile int eventType;

    volatile int priority;



    public Event(EventDestination destination) {
        if (destination == null) {
            throw new NullPointerException("destination cannot be null !"); //$NON-NLS-1$
        }
        this.destination = destination;
        this.eventType = DEFAULT_EVENT_TYPE;
        this.priority = DFT_PRIORITY;
    }



    /**
     * @deprecated Utiliser Event(EventDestination)
     */
    public Event(EventId evtId) {
        this((EventDestination) evtId);
    }



    /**
     * 
     * D�finit le type de l'�v�nement :<br>
     * <ul>
     * <li>DEFAULT_EVENT_TYPE : L'�v�nement sera dispatch� par
     * l'EventDispatchThread standard</li>
     * <li>UI_EVENT_TYPE : L'�v�nement sera dispatch� par l'EDT AWT/Swing
     * AwtEventQueue-x</li>
     * </ul>
     * 
     * @param eventType
     *            DEFAULT_EVENT_TYPE ou UI_EVENT_TYPE
     */
    protected void setEventType(int eventType) {
        this.eventType = eventType;
    }



    /**
     * @deprecated Utiliser getDestination() � la place
     */
    public EventDestination getId() {
        return destination;
    }



    /**
     * Retourne la destination port�e par l'�v�nement
     */
    public EventDestination getDestination() {
        return destination;
    }



    /**
     * D�finit la priorit� de l'�v�nement :<br>
     * <ul>
     * <li>0 : priorit� la plus faible (MIN_PRIORITY)
     * <li>4 : priorit� la plus haute (MAX_PRIORITY)
     * </ul>
     * <br>
     * Par d�faut, la priorit� est DFT_PRIORITY
     */
    public final void setPriority(int priority) {
        setPriorityImpl(priority);
    }



    synchronized void setPriorityImpl(int p) {
        if (p < MIN_PRIORITY) {
            this.priority = MIN_PRIORITY;
        } else if (p > MAX_PRIORITY) {
            this.priority = MAX_PRIORITY;
        } else {
            this.priority = p;
        }
    }



    /**
     * Retourne la priorit� de l'�v�nement.
     */
    public final int getPriority() {
        return this.priority;
    }



    public String toString() {
        return destination.toString();
    }

}
