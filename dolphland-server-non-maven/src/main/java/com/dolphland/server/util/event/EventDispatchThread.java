/*
 * $Log: EventDispatchThread.java,v $
 * Revision 1.13 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.12 2013/04/30 09:38:50 bvatan
 * - FWK-58: added support for ThreadDeath Error catch and log
 * Revision 1.11 2010/11/12 15:47:13 bvatan
 * - Ajout support des schedules de type crontab
 * Revision 1.10 2009/06/22 07:30:10 bvatan
 * - ajotu interception d'�v�nement
 * Revision 1.9 2009/03/30 09:34:32 jscafi
 * - Suppression des warnings inutiles
 * Revision 1.8 2009/03/24 13:53:01 bvatan
 * - gestion des schedules
 * Revision 1.7 2009/03/05 15:01:47 bvatan
 * - mise � niveau EventId -> EventDestination
 * - ajout service de diffusion postEvent(Event,EventDestination)
 * Revision 1.6 2008/12/03 14:41:45 bvatan
 * - le EventDispatchThread devient un deamon
 * Revision 1.5 2008/07/07 11:30:42 bvatan
 * - Ajout des �v�nements de UI (UIEvent)
 * Revision 1.4 2007/05/07 14:54:16 bvatan
 * - Ajout et gestion du service postEventAndWait()
 * Revision 1.3 2007/04/20 10:47:36 bvatan
 * - ajout d'un indicateur d'activit� : indique si le thread � pr�t � dispatcher
 * les �v�nements
 * Revision 1.2 2007/04/16 09:39:04 bvatan
 * - modification du format des traces de logs
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.2 2007/03/27 07:23:44 bvatan
 * - renommage thread EventDispatcherThread -> EventDispatchThread
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.3 2006/12/07 15:52:54 bvatan
 * - ajout documentation
 * - mise � null du thread sur stop().
 * Revision 1.2 2006/12/07 10:54:20 bvatan
 * - correction bug multithreading. Les threads postants un �v�nement via
 * postAndWait() pouvaient �tre notifi�s alors que l'�v�nement post� n'avait pas
 * encore �t� d�livr�.
 * Revision 1.1 2006/12/04 13:26:40 bvatan
 * - initial check in
 */
package com.dolphland.server.util.event;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

/**
 * Thread d'�x�cution de la distribution des �v�nements.<br>
 * <br>
 * Ce thread g�re la file des �v�nements qui lui sont post�s via les m�thodes
 * postEvent(Event) et postEventAndWait(Event). Le thread d�l�gue la
 * distribution de chaque �v�nement de la file au multicasteur fourni au
 * constructeur.<br>
 * <br>
 * 
 * @author JayJay
 */
class EventDispatchThread {

    private static final Logger log = Logger.getLogger(EventDispatchThread.class);

    /**
     * EventDispatcher responsable de la distribution des �v�nements
     */
    private EventDispatcher eventDispatcher;

    /**
     * Indicateur de vie du thread
     */
    private boolean shouldRun;

    /**
     * Indicateur d'activit� du dispatcheur
     */
    private volatile boolean running = false;

    /**
     * Thread d'ex�cution de la distribution des �v�nements.
     */
    private Thread thread;

    /**
     * Runnable � ex�cuter par le thread d'ex�cution de la distribution des
     * �v�nements
     */
    private Runner runner;

    /**
     * Liste des �v�nements � distribuer
     */
    private PriorityQueue<EvtInfo> events;

    /**
     * Scheduler interne
     */
    private Scheduler scheduler;

    private List<EventInterceptor> interceptors;



    /**
     * Cr�� le dispatcheur qui d�l�guera la distribution des �v�nements au
     * EventDispatcher sp�cifi�
     * 
     * @param eventDispatcher
     *            Le EventDispatcher en charge de la logique de distribution des
     *            �v�nements.
     */
    EventDispatchThread(EventDispatcher eventDispatcher) {
        if (eventDispatcher == null) {
            throw new NullPointerException("eventDispatcher cannot be null !"); //$NON-NLS-1$
        }
        this.eventDispatcher = eventDispatcher;
        this.interceptors = new ArrayList<EventInterceptor>();
        events = new PriorityQueue<EvtInfo>(Event.MAX_SYS_PRIORITY);
        runner = new Runner();
        scheduler = new Scheduler();
    }



    /**
     * D�marre le thread de distribution des �v�nements
     */
    synchronized void start() {
        if (thread != null) {
            return;
        }
        shouldRun = true;
        thread = new Thread(runner, "EventDispatchThread"); //$NON-NLS-1$
        thread.setDaemon(true);
        thread.setUncaughtExceptionHandler(new FwkUncaughtExceptionHandler());
        thread.start();
        scheduler.start();
    }



    /**
     * Arr�te le thread de distribution des �v�nements
     */
    synchronized void stop() {
        if (thread == null) {
            return;
        }
        shouldRun = false;
        thread.interrupt();
        thread = null;
        scheduler.stop();
    }



    /**
     * Indique si le DispatchThread est pr�t � dispatcher les �v�nements
     */
    public boolean isRunning() {
        return running;
    }



    Schedule schedule(Schedule sched) {
        if (sched == null) {
            throw new NullPointerException("sched is null !"); //$NON-NLS-1$
        }
        scheduler.addSchedule(sched);
        return sched;
    }



    void schedule(CrontabSchedule sched) {
        if (sched == null) {
            throw new NullPointerException("sched is null !"); //$NON-NLS-1$
        }
        scheduler.addSchedule(sched);
    }



    public void addEventInterceptor(EventInterceptor interceptor) {
        synchronized (interceptors) {
            interceptors.add(interceptor);
        }
    }



    public void removeEventInterceptor(EventInterceptor interceptor) {
        synchronized (interceptors) {
            interceptors.remove(interceptor);
        }
    }



    private boolean intercept(Event evt, EventDestination dest) {
        List<EventInterceptor> ics = null;
        synchronized (interceptors) {
            ics = new ArrayList<EventInterceptor>(interceptors);
        }
        for (EventInterceptor interceptor : ics) {
            if (!interceptor.processEvent(evt, dest)) {
                if (log.isInfoEnabled()) {
                    log.info("postEvent(): Event " + evt + ", destination=" + dest + " has been intercepted: not delivered."); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
                return false;
            }
        }
        return true;
    }



    /**
     * D�livre l'�v�nement aux listeners et retourne imm�diatement
     * 
     * @param evt
     *            L'�v�nement � d�livrer
     */
    void postEvent(Event evt) {
        postEvent(evt, evt.getDestination());
    }



    /**
     * D�livre l'�v�nement aux listeners de la destination sp�cifi�e et retourne
     * imm�diatement
     * 
     * @param evt
     *            L'�v�nement � d�livrer
     */
    void postEvent(Event evt, EventDestination dest) {
        if (dest == null) {
            throw new NullPointerException("destination is null!"); //$NON-NLS-1$
        }
        if (!intercept(evt, dest)) {
            return;
        }
        synchronized (events) {
            events.queue(new EvtInfo(null, evt, dest), evt.getPriority());
            events.notify();
        }
    }



    void postEventAndWait(Event evt) throws InterruptedException {
        postEvent(evt, evt.getDestination());
    }



    void postEventAndWait(Event evt, EventDestination dest) throws InterruptedException {
        if (!intercept(evt, dest)) {
            return;
        }
        EvtInfo evtInfo = new EvtInfo(new Object(), evt, dest);
        synchronized (events) {
            events.queue(evtInfo, evt.getPriority());
            events.notify();
        }
        synchronized (evtInfo.lock) {
            while (!evtInfo.notified) {
                evtInfo.lock.wait();
            }
        }
    }

    /**
     * Runnable contenant la logique de d�pilement des �v�nements de la file
     * d'�v�nements. G�re la d�l�gation de la d�livrance des �v�nements au
     * EventDispatcher.
     * 
     * @author JayJay
     */
    private class Runner implements Runnable {

        public void run() {
            log.info("run() : EventDispatchThread started."); //$NON-NLS-1$
            EvtInfo evtInfo = null;
            Event evt = null;
            EventDestination evtDest = null;
            Object evtLock = null;
            running = true;
            while (shouldRun) {
                try {
                    synchronized (events) {
                        while (events.size() == 0) {
                            log.debug("run() : EventDispatchThread waiting..."); //$NON-NLS-1$
                            events.wait();
                        }
                        evtInfo = (EvtInfo) events.pop();
                    }
                    evt = evtInfo.evt;
                    evtDest = evtInfo.dest;
                    evtLock = evtInfo.lock;
                } catch (InterruptedException ie) {
                    log.debug("run() : EventDispatchThread interrupted !"); //$NON-NLS-1$
                    Thread.currentThread().interrupt();
                    break;
                }
                log.debug("run() : EventDispatchThread dispatching..."); //$NON-NLS-1$
                // distribuer l'�v�nement aux subscribers
                switch (evt.eventType) {
                    case Event.DEFAULT_EVENT_TYPE :
                        eventDispatcher.dispatchEvent(evt, evtDest);
                        break;
                    case Event.UI_EVENT_TYPE :
                        final Event uiEvt = evt;
                        final EventDestination uiEvtDest = evtDest;
                        if (evtLock != null) {
                            try {
                                SwingUtilities.invokeAndWait(new Runnable() {

                                    public void run() {
                                        eventDispatcher.dispatchEvent(uiEvt, uiEvtDest);
                                    }
                                });
                            } catch (Exception e) {
                                // ignore
                            }
                        } else {
                            SwingUtilities.invokeLater(new Runnable() {

                                public void run() {
                                    eventDispatcher.dispatchEvent(uiEvt, uiEvtDest);
                                }
                            });
                        }
                        break;
                }
                log.debug("run() : EventDispatchThread dispatching done."); //$NON-NLS-1$
                if (evtLock != null) {
                    log.debug("run(): Notifying waiting poster..."); //$NON-NLS-1$
                    synchronized (evtLock) {
                        evtInfo.notified = true;
                        evtLock.notify();
                    }
                }
            }
            running = false;
            log.info("run() : EventDispatchThread stoped."); //$NON-NLS-1$
        }
    }

    private class EvtInfo {

        Object lock;

        Event evt;

        EventDestination dest;

        volatile boolean notified = false;



        EvtInfo(Object lock, Event evt, EventDestination dest) {
            this.lock = lock;
            this.evt = evt;
            this.dest = dest;
        }

    }
}
