/*
 * $Log: DefaultEventSubscriber.java,v $
 * Revision 1.3 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.2 2009/03/27 07:50:06 bvatan
 * - verrouillage de equals() et hashCode()
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - déplacement du multicaster vers fwk-common
 */
package com.dolphland.server.util.event;

import org.apache.log4j.Logger;

public class DefaultEventSubscriber implements EventSubscriber {

    private static final Logger log = Logger.getLogger(DefaultEventSubscriber.class);



    public void eventReceived(Event e) {
        log.error("eventReceived(): Unhandled event :" + e); //$NON-NLS-1$
        throw new FWKUnhandledEventException("Unhandled event !", e); //$NON-NLS-1$
    }



    @Override
    public final boolean equals(Object obj) {
        return super.equals(obj);
    }



    @Override
    public final int hashCode() {
        return super.hashCode();
    }
}
