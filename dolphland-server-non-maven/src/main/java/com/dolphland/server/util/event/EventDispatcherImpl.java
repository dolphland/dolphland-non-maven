/*
 * $Log: EventDispatcherImpl.java,v $
 * Revision 1.10 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.9 2009/03/30 09:34:32 jscafi
 * - Suppression des warnings inutiles
 * Revision 1.8 2009/03/30 08:51:00 bvatan
 * - suppression de identityHashCode pour identifier les subscribers
 * Revision 1.7 2009/03/11 17:24:35 bvatan
 * - correction bug dispatchEvent. Etait synchrnos� et pouvait engendrer des
 * deadlocks
 * Revision 1.6 2009/03/05 15:27:12 bvatan
 * - augmentation de l'API pour conserver une compatitibilit� descendante avec
 * les applications
 * Revision 1.5 2009/03/05 15:01:29 bvatan
 * - mise � niveau EventId -> EventDestination
 * - ajout service de diffusion postEvent(Event,EventDestination)
 * Revision 1.4 2009/02/27 12:49:34 bvatan
 * - utilisaition des System.identityHashcode() � la place de
 * EventSubscriber.hashCode()
 * Revision 1.3 2008/08/12 14:03:55 bvatan
 * - correction d'un bug sur le d�-enregistrement pour tous les �v�nement -> CCE
 * Revision 1.2 2008/04/18 14:38:57 bvatan
 * - correction bug CCE
 * Revision 1.1 2007/04/06 15:34:50 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.3 2007/03/27 07:26:22 bvatan
 * - reorg imports
 * Revision 1.2 2007/03/27 07:24:38 bvatan
 * - ajout du reverse polymorrph invoker pour l'invocation des eventReceived()
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.2 2006/12/08 15:01:15 bvatan
 * - utilisation du BTree pour le stockage/recherche des listeners
 * Revision 1.1 2006/12/04 13:26:40 bvatan
 * - initial check in
 */
package com.dolphland.server.util.event;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.apache.log4j.Logger;

/**
 * Impl�mentation de Multicaster.
 * 
 * Cet objet est responsablde l'enregistrement/d�senregistrement des listeners
 * et de la distribution des �v�nements.
 * 
 * @author JayJay
 */
public class EventDispatcherImpl implements EventDispatcher {

    private static final Logger log = Logger.getLogger(EventDispatcherImpl.class);

    /**
     * Listeners index�s par les destination d'�v�nements
     */
    private Map<EventDestination, Set<EventSubscriber>> eventListeners;

    /**
     * Destinations index�es par les listeners
     */
    private Map<EventSubscriber, Set<EventDestination>> listenerDestinations;

    /**
     * Listeners abonn�s � tous les �v�nements
     */
    private Map<EventSubscriber, EventSubscriber> universalListeners;

    /**
     * D�l�gu� charg� de l'invocation du service de notification de r�ception
     * d'un �v�nement sur un Subscriber
     */
    private ReversePolymorhInvoker subscriberInvoker;



    public EventDispatcherImpl() {
        eventListeners = new WeakHashMap<EventDestination, Set<EventSubscriber>>();
        listenerDestinations = new WeakHashMap<EventSubscriber, Set<EventDestination>>();
        universalListeners = new WeakHashMap<EventSubscriber, EventSubscriber>();
        subscriberInvoker = new ReversePolymorhInvoker();
    }



    public EventRegisterer subscribe(EventSubscriber el) {
        return new EventRegistererImpl(el);
    }



    public EventRegisterer unsubscribe(EventSubscriber el) {
        return new EventDeregistererImpl(el);
    }



    public synchronized void unsubscribeFromAll(EventSubscriber el) {
        // obtention de la liste des �v�nements du listener
        Integer evtLstrHash = new Integer(System.identityHashCode(el));
        Set<EventDestination> events = listenerDestinations.get(evtLstrHash);
        if (events == null) {
            return;
        }
        // pour chaque destination, retirer le listener
        for (Iterator<EventDestination> it = events.iterator(); it.hasNext();) {
            EventDestination destination = it.next();
            Set<EventSubscriber> listeners = getListeners(destination);
            if (listeners != null) {
                listeners.remove(el);
            }
        }
    }



    public void dispatchEvent(Event evt, EventDestination evtDest) {
        EventSubscriber[] ess = null;
        synchronized (this) {
            Set<EventSubscriber> listeners = getListeners(evtDest);
            if (listeners == null) {
                return;
            }
            ess = listeners.toArray(new EventSubscriber[listeners.size()]);
        }
        Class<?> evtClass = evt.getClass();
        for (EventSubscriber lstr : ess) {
            try {
                subscriberInvoker.invoke(lstr, "eventReceived", evtClass, evt); //$NON-NLS-1$
            } catch (InvocationTargetException e) {
                Throwable cause = e.getCause();
                log.error("Subscriber " + lstr + " has thrown an exception !", cause); //$NON-NLS-1$ //$NON-NLS-2$
            } catch (Exception e) {
                log.error("Unable to invoke service on handler ! Trying by standard interface...", e); //$NON-NLS-1$
                try {
                    lstr.eventReceived(evt);
                } catch (Exception ex) {
                    log.error("Subscriber " + lstr + " has thrown an exception !", ex); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        }
    }



    public synchronized Set<EventSubscriber> getListeners(EventDestination destination) {
        Set<EventSubscriber> out = eventListeners.get(destination);
        if (out == null || out.size() == 0) {
            // si pas de listener pour la destination, ajouter tous les
            // listeners universels pour cette destination
            out = new HashSet<EventSubscriber>(universalListeners.values());
            eventListeners.put(destination, out);
        }
        return out;
    }

    /**
     * Impl�mentation d'un contexte d'enregistrement des �v�nements pour un
     * listener donn�.
     * 
     * @author JayJay
     */
    private class EventRegistererImpl implements EventRegisterer {

        private EventSubscriber evtLstr;



        EventRegistererImpl(EventSubscriber l) {
            this.evtLstr = l;
        }



        public EventRegisterer forEvent(EventDestination evtId) {
            return forEvents(evtId);
        }



        public EventRegisterer forEvents(EventId evtId) {
            return forEvents((EventDestination) evtId);
        }



        public EventRegisterer forEvent(EventId evtChannel) {
            return forEvents((EventDestination) evtChannel);
        }



        public EventRegisterer forEvents(EventDestination evtDest) {
            synchronized (EventDispatcherImpl.this) {
                // enregistrement du listener dans la liste des listeners de
                // la destination
                Set<EventSubscriber> set = getListeners(evtDest);
                if (set == null) {
                    set = new LinkedHashSet<EventSubscriber>();
                    eventListeners.put(evtDest, set);
                }
                set.add(evtLstr);
                // ajout de la destination dans la liste des destinations du
                // listener
                // Integer evtLstrHash = new
                // Integer(System.identityHashCode(evtLstr));
                Set<EventDestination> destinations = listenerDestinations.get(evtLstr);
                if (destinations == null) {
                    destinations = new HashSet<EventDestination>();
                    listenerDestinations.put(evtLstr, destinations);
                }
                destinations.add(evtDest);
                set = null;
                destinations = null;
            }
            return this;
        }



        public void forAllEvents() {
            synchronized (EventDispatcherImpl.this) {
                // ajouter le listener aux listeners universels
                universalListeners.put(evtLstr, evtLstr);
                // pour chaque eventChannel connu, ajouter le listener
                Iterator<EventDestination> it = eventListeners.keySet().iterator();
                while (it.hasNext()) {
                    EventDestination evtDest = (EventDestination) it.next();
                    forEvents(evtDest);
                }
            }
        }



        public EventSubscriber getSubscriber() {
            return evtLstr;
        }
    }

    /**
     * Impl�mentation d'un contexte de d�senregistrement des �v�nements pour un
     * listener donn�.
     * 
     * @author JayJay
     */
    private class EventDeregistererImpl implements EventRegisterer {

        private EventSubscriber evtLstr;



        EventDeregistererImpl(EventSubscriber l) {
            this.evtLstr = l;
        }



        public EventRegisterer forEvent(EventDestination evtId) {
            return forEvents(evtId);
        }



        public EventRegisterer forEvents(EventId evtId) {
            return forEvents((EventDestination) evtId);
        }



        public EventRegisterer forEvents(EventDestination evtChannel) {
            synchronized (EventDispatcherImpl.this) {
                // suppression du listener dans la liste des listeners de
                // l'�v�nement
                Set<EventSubscriber> set = getListeners(evtChannel);
                if (set == null) {
                    return this;
                }
                set.remove(evtLstr);
                // suppression de l'�v�nement dans la liste des �v�nements du
                // listener
                // Integer evtLstrHash = new
                // Integer(System.identityHashCode(evtLstr));
                Set<EventDestination> events = listenerDestinations.get(evtLstr);
                if (events == null) {
                    return this;
                }
                events.remove(evtChannel);
            }
            return this;
        }



        public EventRegisterer forEvent(EventId evtId) {
            return forEvents((EventDestination) evtId);
        }



        public void forAllEvents() {
            synchronized (EventDispatcherImpl.this) {
                // liste des listes de listeners pour chaque �v�nement
                Collection<Set<EventSubscriber>> listenersList = eventListeners.values();
                // pour chaque liste de listener, retirer le listener
                for (Iterator<Set<EventSubscriber>> it = listenersList.iterator(); it.hasNext();) {
                    Set<EventSubscriber> listeners = it.next();
                    listeners.remove(evtLstr);
                }
                // supprimer la liste d'�v�nements du listener
                // listenerDestinations.remove(new
                // Integer(System.identityHashCode(evtLstr)));
                listenerDestinations.remove(evtLstr);
                // supprimer le listener de la liste des listeners universels
                universalListeners.remove(evtLstr);
            }
        }



        public EventSubscriber getSubscriber() {
            return evtLstr;
        }
    }
}
