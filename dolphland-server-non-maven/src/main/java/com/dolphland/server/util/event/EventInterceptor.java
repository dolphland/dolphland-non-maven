/*
 * $Log: EventInterceptor.java,v $
 * Revision 1.1 2009/06/22 07:30:40 bvatan
 * - initial check in
 */

package com.dolphland.server.util.event;

/**
 * Les <code>EventInterceptor</code> sont install�s apr�s leur �mission par les
 * composants clients mais avant leur transmission au
 * <code>EventDispatchThread</code><br>
 * <br>
 * <code>processEvent()</code> permet d'indiquer si l'�v�nement doit �tre
 * effectivement post�, dans ce cas <code>processEvent()</code> retourne true et
 * permet �galement d'�mettre une exception si l'�mission d'�v�nement dans le
 * context est une erreur.
 * 
 * @author JayJay
 */
public class EventInterceptor {

    /**
     * Traite l'�v�nement post� avant qu'il ne soit d�livr�.
     * 
     * @param evt
     *            Ev�nement post� par un composant
     * @param dest
     *            Destination de l'� v�nement
     * @return true si l'�v�nement peut �tre post�, false s'il doit �tre ignor�.
     * @throws EventInterceptorException
     *             Si l'�mission de cet �v�nement dans le context est une erreur
     */
    public boolean processEvent(Event evt, EventDestination dest) throws EventInterceptorException {
        return true;
    }

}
