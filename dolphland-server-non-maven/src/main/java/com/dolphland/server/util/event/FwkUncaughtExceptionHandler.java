package com.dolphland.server.util.event;

import java.lang.Thread.UncaughtExceptionHandler;
import java.text.MessageFormat;

import org.apache.log4j.Logger;

/**
 * <p>
 * Default exception handler that should be set on each thread spawn by the
 * framework
 * </p>
 * <p>
 * Setting this handler on each thread will make it possible to detect thread
 * death as well as unhadled exception within the thread
 * </p>
 * 
 * @author JayJay
 */
public class FwkUncaughtExceptionHandler implements UncaughtExceptionHandler {

    private static final Logger LOG = Logger.getLogger(FwkUncaughtExceptionHandler.class);



    public void uncaughtException(Thread t, Throwable e) {
        String message = MessageFormat.format("Thread [{0}] has been terminated after the following exception has not been thrown !", t.getName()); //$NON-NLS-1$
        LOG.error("uncaughtException(): " + message, e); //$NON-NLS-1$
    }

}
