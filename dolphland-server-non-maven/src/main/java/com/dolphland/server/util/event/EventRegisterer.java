/*
 * $Log: EventRegisterer.java,v $
 * Revision 1.3 2009/03/05 15:27:12 bvatan
 * - augmentation de l'API pour conserver une compatitibilit� descendante avec
 * les applications
 * Revision 1.2 2009/03/05 15:02:50 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.1 2007/04/06 15:34:50 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.1 2006/12/04 13:26:40 bvatan
 * - initial check in
 */
package com.dolphland.server.util.event;

/**
 * Repr�sente un enregisreur/d�senregistreur d'�v�nements.<br>
 * 
 * Les impl�mentations encapsulent un listener et l'enregistre pour chacune des
 * destinations d'�v�nement sp�cifi�es par le service
 * forEvents(EventDestination).<br>
 * <br>
 * Dans le cadre d'un d�senregistrement, les impl�mentations sont responsables
 * du d�tachement du listener encapsul� pour chacune des destination sp�cifi�es
 * par le service forEvents(EventDestination)<br>
 * Exemple :<br>
 * 
 * <pre>
 * EventDestination MY_DEST1 = new EventDestination();
 * EventDestination MY_DEST2 = new EventDestination();
 * EventMulticaster mc = EventMulticaster.getInstance();
 * mc.subscribe(mySubscriber).forEvents(MY_DEST1).forEvents(MY_DEST2);
 * </pre>
 * 
 * <br>
 * Dans l'exmple ci-dessus, <code>mySubscriber</code> recevra tous les
 * �v�nements � destination de MY_DEST1 et tous les �v�nements � destination de
 * MY_DEST2.
 * 
 * @author JayJay
 */
public interface EventRegisterer {

    /**
     * @deprecated Utiliser forEvents(EventDestination)
     */
    @Deprecated
    public EventRegisterer forEvent(EventId evtId);



    /**
     * @deprecated Utiliser forEvents(EventDestination)
     */
    @Deprecated
    public EventRegisterer forEvents(EventId evtId);



    /**
     * @deprecated Utiliser forEvents(EventDestination)
     */
    @Deprecated
    public EventRegisterer forEvent(EventDestination evtId);



    /**
     * Sp�cifie la destination pour lasuelle on veut recvoir ou ne plsu recevoir
     * les �v�nements.<br>
     * Dans le cas d'un enregistrement, l'abonn� recevra tous les �v�nements
     * envoy�s � la destination sp�cifi�e.<br>
     * Dans le cas d'un d�senregistrement, l'abonn� ne recevra plus aucun
     * �v�nement envoy� vers la destination sp�cifi�e
     * 
     * @param evtDestination
     *            La destination des �v�nements � enregistrer/d�senregistrer
     * @return Un EventRegister ayant les m�mes capacit�s
     *         d'enregistrement/d�senregistrement que this
     */
    public EventRegisterer forEvents(EventDestination evtDestination);



    /**
     * Sp�cifie que l'abonn� recevra tous les �v�nements de toutes les
     * destinations ou bien qu'il ne recevra plus aucun �v�nement pour aucune
     * destination.
     */
    public void forAllEvents();



    /**
     * Retourne le subscriber sous jacent.
     * 
     * @return Le EventSubscriber concern� par l'engistrement/d�senregistrement
     */
    public EventSubscriber getSubscriber();
}
