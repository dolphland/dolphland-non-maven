package com.dolphland.server.context;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.dolphland.core.ws.data.User;
import com.dolphland.server.business.InfosClient;
import com.dolphland.server.business.InfosParty;
import com.dolphland.server.util.exception.AppBusinessException;
import com.dolphland.server.util.string.StrUtil;

/**
 * Context for the server
 * 
 * @author JayJay
 * 
 */
public class ContextServer {

    private static ContextServer context;

    // Users
    private Map<String, User> usersById = new TreeMap<String, User>();

    // Informations for each client
    private Map<String, InfosClient> infosByClientId = new TreeMap<String, InfosClient>();

    // Informations for each party
    private Map<String, InfosParty> infosByPartyId = new TreeMap<String, InfosParty>();



    private synchronized static ContextServer getInstance() {
        if (context == null) {
            start();
        }
        return context;
    }



    /**
     * Start context
     */
    public static synchronized void start() {
        if (context == null) {
            context = new ContextServer();
        }
    }



    /**
     * Stop context
     */
    public static synchronized void stop() {
        context = null;
    }



    /**
     * Get users
     * 
     * @return Users
     */
    public static synchronized List<User> getUsers() {
        return new ArrayList<User>(getInstance().usersById.values());
    }



    /**
     * Get user from id
     * 
     * @param userId
     *            User id
     * 
     * @return User
     */
    public static synchronized User getUser(String userId) {
        return StrUtil.isEmpty(userId) ? null : getInstance().usersById.get(userId);
    }



    /**
     * Add user
     * 
     * @param user
     *            User
     */
    public static synchronized void addUser(User user) {
        getInstance().usersById.put(user.getId(), user);
    }



    /**
     * Get informations from all clients
     * 
     * @return Informations for each client
     */
    public static synchronized List<InfosClient> getInfosClient() {
        return new ArrayList<InfosClient>(getInstance().infosByClientId.values());
    }



    /**
     * Get informations from a client
     * 
     * @param clientId
     *            Client id
     * 
     * @return Informations for client searched
     */
    public static synchronized InfosClient getInfosClient(String clientId) {
        if (StrUtil.isEmpty(clientId)) {
            return null;
        }
        InfosClient infosClient = getInstance().infosByClientId.get(clientId);
        if (infosClient == null) {
            throw new AppBusinessException("No informations found for the client " + clientId + " !");
        }
        return infosClient;
    }



    /**
     * Add a client
     * 
     * @param infosClient
     *            Client's informations
     * 
     * @return Informations for client added
     */
    public static synchronized InfosClient addClient(InfosClient infosClient) {
        getInstance().infosByClientId.put(infosClient.getClientId(), infosClient);
        return infosClient;
    }



    /**
     * Remove a client
     * 
     * @param clientId
     *            Client id
     * 
     * @return Informations from client removed
     */
    public static synchronized InfosClient removeClient(String clientId) {
        return getInstance().infosByClientId.remove(clientId);
    }



    /**
     * Get informations from all parties
     * 
     * @return Informations for each party
     */
    public static synchronized List<InfosParty> getInfosParties() {
        return new ArrayList<InfosParty>(getInstance().infosByPartyId.values());
    }



    /**
     * Get informations from a party
     * 
     * @param partyId
     *            Party id
     * 
     * @return Informations for party searched
     */
    public static synchronized InfosParty getInfosParty(String partyId) {
        if (StrUtil.isEmpty(partyId)) {
            return null;
        }
        InfosParty infosParty = getInstance().infosByPartyId.get(partyId);
        if (infosParty == null || infosParty.getParty() == null) {
            throw new AppBusinessException("No informations found for the party " + partyId + " !");
        }
        return infosParty;
    }



    /**
     * Add a party
     * 
     * @param infosParty
     *            Party's informations
     * 
     * @return Informations for party addedo
     */
    public static synchronized InfosParty addParty(InfosParty infosParty) {
        getInstance().infosByPartyId.put(infosParty.getParty().getId(), infosParty);
        return infosParty;
    }



    /**
     * Remove a party
     * 
     * @param partyId
     *            Party id
     * 
     * @return Informations for party removed
     */
    public static synchronized InfosParty removeParty(String partyId) {
        return getInstance().infosByPartyId.remove(partyId);
    }
}
