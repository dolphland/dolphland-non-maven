package com.dolphland.server.context;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.dolphland.server.util.event.EDT;
import com.dolphland.server.util.version.VersionUtil;

/**
 * Web's context listener
 * 
 * @author JayJay
 * 
 */
public class ContextListener implements ServletContextListener {

    private static final Logger LOG = Logger.getLogger(ContextListener.class);



    /**
     * Called when context initialized
     * 
     * @param sce
     *            Servlet context
     */
    public void contextInitialized(ServletContextEvent sce) {
        LOG.info("Starting Dolphland server " + VersionUtil.getVersion() + "...");

        try {
            LOG.info("Starting Event Multicaster...");
            EDT.start();
        } catch (Exception exc) {
            LOG.error("Error during starting Event Multicaster", exc);
        }

        try {
            LOG.info("Starting context...");
            ContextServer.start();
        } catch (Exception exc) {
            LOG.error("Error during starting context", exc);
        }
    }



    /**
     * Called when context destroyed
     * 
     * @param sce
     *            Servlet context
     */
    public void contextDestroyed(ServletContextEvent sce) {
        LOG.info("Stopping Dolphland server " + VersionUtil.getVersion() + "...");

        try {
            LOG.info("Stopping context...");
            ContextServer.stop();
        } catch (Exception exc) {
            LOG.error("Error during stopping context", exc);
        }

        try {
            LOG.info("Stopping Event Multicaster...");
            EDT.stop();
        } catch (Exception exc) {
            LOG.error("Error during stopping Event Multicaster", exc);
        }
    }
}
