package com.dolphland.server.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessMovement;
import com.dolphland.core.ws.data.GetChessBoardRequest;
import com.dolphland.core.ws.data.GetChessBoardResponse;
import com.dolphland.core.ws.data.GetChessMovementsRequest;
import com.dolphland.core.ws.data.GetChessMovementsResponse;
import com.dolphland.core.ws.data.MoveChessPieceRequest;
import com.dolphland.core.ws.data.MoveChessPieceResponse;
import com.dolphland.core.ws.data.SetChessPlayerRequest;
import com.dolphland.core.ws.data.SetChessPlayerResponse;
import com.dolphland.server.business.BOChess;

/**
 * Service implementation for chess
 * 
 * @author JayJay
 * 
 */
@Service
public class SVChess {

    private static final Logger LOG = Logger.getLogger(SVChess.class);

    private BOChess boChess;



    @Autowired
    public void setBoChess(BOChess boChess) {
        this.boChess = boChess;
    }



    /**
     * Get board
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public GetChessBoardResponse getBoard(GetChessBoardRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'getBoard'...");
        }

        // Get board
        ChessBoard board = boChess.getBoard(request.getClientId(), request.getPartyId());

        // Construct response
        GetChessBoardResponse response = new GetChessBoardResponse();
        response.setPartyId(request.getPartyId());
        response.setBoard(board);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'getBoard'...");
        }
        return response;
    }



    /**
     * Set a player
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public SetChessPlayerResponse setPlayer(SetChessPlayerRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'setPlayer'...");
        }

        // Get board
        ChessBoard board = boChess.setPlayer(request.getClientId(), request.getPartyId(), request.getColor());

        // Construct response
        SetChessPlayerResponse response = new SetChessPlayerResponse();
        response.setPartyId(request.getPartyId());
        response.setBoard(board);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'setPlayer'...");
        }
        return response;
    }



    /**
     * Get movements for a piece
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public GetChessMovementsResponse getMovements(GetChessMovementsRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'getMovements'...");
        }

        // Get movements
        List<ChessMovement> movements = boChess.getMovements(request.getClientId(), request.getPartyId(), request.getPiece());

        // Construct response
        GetChessMovementsResponse response = new GetChessMovementsResponse();
        response.setPartyId(request.getPartyId());
        response.getMovements().addAll(movements);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'getMovements'...");
        }
        return response;
    }



    /**
     * Move a piece
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public MoveChessPieceResponse movePiece(MoveChessPieceRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'movePiece'...");
        }

        // Get board
        ChessBoard board = boChess.movePiece(request.getClientId(), request.getPartyId(), request.getPiece(), request.getPosition());

        // Construct response
        MoveChessPieceResponse response = new MoveChessPieceResponse();
        response.setPartyId(request.getPartyId());
        response.setBoard(board);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'movePiece'...");
        }
        return response;
    }
}
