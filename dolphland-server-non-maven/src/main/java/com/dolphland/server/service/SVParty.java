package com.dolphland.server.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.AbortPartyRequest;
import com.dolphland.core.ws.data.AbortPartyResponse;
import com.dolphland.core.ws.data.CreatePartyRequest;
import com.dolphland.core.ws.data.CreatePartyResponse;
import com.dolphland.core.ws.data.GetPartiesRequest;
import com.dolphland.core.ws.data.GetPartiesResponse;
import com.dolphland.core.ws.data.JoinPartyRequest;
import com.dolphland.core.ws.data.JoinPartyResponse;
import com.dolphland.core.ws.data.LeavePartyRequest;
import com.dolphland.core.ws.data.LeavePartyResponse;
import com.dolphland.core.ws.data.Party;
import com.dolphland.server.business.BOParty;

/**
 * Service implementation for party
 * 
 * @author JayJay
 * 
 */
@Service
public class SVParty {

    private static final Logger LOG = Logger.getLogger(SVParty.class);

    private BOParty boParty;



    @Autowired
    public void setBoParty(BOParty boParty) {
        this.boParty = boParty;
    }



    /**
     * Get parties
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public GetPartiesResponse getParties(GetPartiesRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'getParties'...");
        }

        // Get parties
        List<Party> parties = boParty.getParties();

        // Construct response
        GetPartiesResponse response = new GetPartiesResponse();
        response.getParties().addAll(parties);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'getParties'...");
        }
        return response;
    }



    /**
     * Create a party
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public CreatePartyResponse createParty(CreatePartyRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'createParty'...");
        }

        // Create party
        Party party = boParty.createParty(request.getClientId(), request.getApplicationId(), request.isRanked());

        // Construct response
        CreatePartyResponse response = new CreatePartyResponse();
        response.setParty(party);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'createParty'...");
        }
        return response;
    }



    /**
     * Join a party
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public JoinPartyResponse joinParty(JoinPartyRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'joinParty'...");
        }

        // Join party
        Party party = boParty.joinParty(request.getClientId(), request.getPartyId());

        // Construct response
        JoinPartyResponse response = new JoinPartyResponse();
        response.setParty(party);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'joinParty'...");
        }
        return response;
    }



    /**
     * Leave a party
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public LeavePartyResponse leaveParty(LeavePartyRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'leaveParty'...");
        }

        // Leave party
        Party party = boParty.leaveParty(request.getClientId(), request.getPartyId());

        // Construct response
        LeavePartyResponse response = new LeavePartyResponse();
        response.setParty(party);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'leaveParty'...");
        }
        return response;
    }



    /**
     * Abort a party
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public AbortPartyResponse abortParty(AbortPartyRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'abortParty'...");
        }

        // Leave party
        Party party = boParty.abortParty(request.getClientId(), request.getPartyId());

        // Construct response
        AbortPartyResponse response = new AbortPartyResponse();
        response.setParty(party);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'abortParty'...");
        }
        return response;
    }
}
