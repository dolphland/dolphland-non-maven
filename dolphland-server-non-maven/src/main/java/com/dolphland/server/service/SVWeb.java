package com.dolphland.server.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.GetWebContentRequest;
import com.dolphland.core.ws.data.GetWebContentResponse;
import com.dolphland.server.business.BOWeb;

/**
 * Service implementation for web
 * 
 * @author JayJay
 * 
 */
@Service
public class SVWeb {

    private static final Logger LOG = Logger.getLogger(SVWeb.class);

    private BOWeb boWeb;



    @Autowired
    public void setBoWeb(BOWeb boWeb) {
        this.boWeb = boWeb;
    }



    /**
     * Get content
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public GetWebContentResponse getContent(GetWebContentRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'getContent'...");
        }

        // Construct response
        GetWebContentResponse response = new GetWebContentResponse();
        response.getServerResponse().addAll(boWeb.getContent(request.getSocketId(), request.getBrowserRequest()));

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'getContent'...");
        }
        return response;
    }
}
