package com.dolphland.server.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.SendMessageRequest;
import com.dolphland.core.ws.data.SendMessageResponse;
import com.dolphland.server.business.BOUser;

/**
 * Service implementation for message
 * 
 * @author JayJay
 * 
 */
@Service
public class SVMessage {

    private static final Logger LOG = Logger.getLogger(SVMessage.class);

    private BOUser boUser;



    @Autowired
    public void setBoUser(BOUser boUser) {
        this.boUser = boUser;
    }



    /**
     * Send message
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public SendMessageResponse sendMessage(SendMessageRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'sendMessage'...");
        }

        // Send the message
        boUser.sendMessage(request.getClientId(), request.getType(), request.getContent(), request.getReceiverUserId(), request.getPartyId());

        // Construct response
        SendMessageResponse response = new SendMessageResponse();

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'sendMessage'...");
        }
        return response;
    }
}
