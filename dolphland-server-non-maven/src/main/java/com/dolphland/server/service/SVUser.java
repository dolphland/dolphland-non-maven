package com.dolphland.server.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.Application;
import com.dolphland.core.ws.data.GetApplicationsRequest;
import com.dolphland.core.ws.data.GetApplicationsResponse;
import com.dolphland.core.ws.data.GetUsersRequest;
import com.dolphland.core.ws.data.GetUsersResponse;
import com.dolphland.core.ws.data.LoginRequest;
import com.dolphland.core.ws.data.LoginResponse;
import com.dolphland.core.ws.data.LogoutRequest;
import com.dolphland.core.ws.data.LogoutResponse;
import com.dolphland.core.ws.data.User;
import com.dolphland.server.business.BOUser;

/**
 * Service implementation for user
 * 
 * @author JayJay
 * 
 */
@Service
public class SVUser {

    private static final Logger LOG = Logger.getLogger(SVUser.class);

    private BOUser boUser;



    @Autowired
    public void setBoUser(BOUser boUser) {
        this.boUser = boUser;
    }



    /**
     * Login user
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public LoginResponse login(LoginRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'login'...");
        }

        // Login user
        User user = boUser.login(request.getClientId(), request.getUserId(), request.getUserPassword());

        // Construct response
        LoginResponse response = new LoginResponse();
        response.setUser(user);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'login'...");
        }
        return response;
    }



    /**
     * Logout user
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public LogoutResponse logout(LogoutRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'logout'...");
        }

        // Logout user
        boUser.logout(request.getClientId());

        // Construct response
        LogoutResponse response = new LogoutResponse();

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'logout'...");
        }
        return response;
    }



    /**
     * Get users
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public GetUsersResponse getUsers(GetUsersRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'getUsers'...");
        }

        // Get users
        List<User> users = boUser.getUsers();

        // Construct response
        GetUsersResponse response = new GetUsersResponse();
        response.getUsers().addAll(users);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'getUsers'...");
        }
        return response;
    }



    /**
     * Get applications
     * 
     * @param request
     *            Request
     * 
     * @return Applications
     */
    public GetApplicationsResponse getApplications(GetApplicationsRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'getApplications'...");
        }

        // Get applications
        List<Application> applications = boUser.getApplications(request.getClientId());

        // Construct response
        GetApplicationsResponse response = new GetApplicationsResponse();
        response.getApplications().addAll(applications);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'getApplications'...");
        }
        return response;
    }
}
