package com.dolphland.server.service.web;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.dolphland.core.ws.data.AbortPartyRequest;
import com.dolphland.core.ws.data.AbortPartyResponse;
import com.dolphland.core.ws.data.CreatePartyRequest;
import com.dolphland.core.ws.data.CreatePartyResponse;
import com.dolphland.core.ws.data.GetApplicationsRequest;
import com.dolphland.core.ws.data.GetApplicationsResponse;
import com.dolphland.core.ws.data.GetChessBoardRequest;
import com.dolphland.core.ws.data.GetChessBoardResponse;
import com.dolphland.core.ws.data.GetChessMovementsRequest;
import com.dolphland.core.ws.data.GetChessMovementsResponse;
import com.dolphland.core.ws.data.GetPartiesRequest;
import com.dolphland.core.ws.data.GetPartiesResponse;
import com.dolphland.core.ws.data.GetUsersRequest;
import com.dolphland.core.ws.data.GetUsersResponse;
import com.dolphland.core.ws.data.GetWebContentRequest;
import com.dolphland.core.ws.data.GetWebContentResponse;
import com.dolphland.core.ws.data.HelloRequest;
import com.dolphland.core.ws.data.HelloResponse;
import com.dolphland.core.ws.data.JoinPartyRequest;
import com.dolphland.core.ws.data.JoinPartyResponse;
import com.dolphland.core.ws.data.LeavePartyRequest;
import com.dolphland.core.ws.data.LeavePartyResponse;
import com.dolphland.core.ws.data.LoginRequest;
import com.dolphland.core.ws.data.LoginResponse;
import com.dolphland.core.ws.data.LogoutRequest;
import com.dolphland.core.ws.data.LogoutResponse;
import com.dolphland.core.ws.data.MoveChessPieceRequest;
import com.dolphland.core.ws.data.MoveChessPieceResponse;
import com.dolphland.core.ws.data.RelayerRequest;
import com.dolphland.core.ws.data.RelayerResponse;
import com.dolphland.core.ws.data.RemoteError;
import com.dolphland.core.ws.data.SendMessageRequest;
import com.dolphland.core.ws.data.SendMessageResponse;
import com.dolphland.core.ws.data.SetChessPlayerRequest;
import com.dolphland.core.ws.data.SetChessPlayerResponse;
import com.dolphland.server.business.InfosClient;
import com.dolphland.server.business.exception.RemoteException;
import com.dolphland.server.context.ContextServer;
import com.dolphland.server.service.SVChess;
import com.dolphland.server.service.SVMessage;
import com.dolphland.server.service.SVParty;
import com.dolphland.server.service.SVRelayer;
import com.dolphland.server.service.SVUser;
import com.dolphland.server.service.SVWeb;

@Endpoint
public class EndpointApp {

    private static final Logger LOG = Logger.getLogger(EndpointApp.class);

    private static final String NAMESPACE_URI = "http://www.noussommestousdesdauphins.com/dolphland/ws/definitions";

    private SVRelayer serviceRelayer;

    private SVUser serviceUser;

    private SVMessage serviceMessage;

    private SVParty serviceParty;

    private SVWeb serviceWeb;

    private SVChess serviceChess;



    @Autowired
    public void setServiceRelayer(SVRelayer serviceRelayer) {
        this.serviceRelayer = serviceRelayer;
    }



    @Autowired
    public void setServiceUser(SVUser serviceUser) {
        this.serviceUser = serviceUser;
    }



    @Autowired
    public void setServiceMessage(SVMessage serviceMessage) {
        this.serviceMessage = serviceMessage;
    }



    @Autowired
    public void setServiceParty(SVParty serviceParty) {
        this.serviceParty = serviceParty;
    }



    @Autowired
    public void setServiceWeb(SVWeb serviceWeb) {
        this.serviceWeb = serviceWeb;
    }



    @Autowired
    public void setServiceChess(SVChess serviceChess) {
        this.serviceChess = serviceChess;
    }



    /**
     * Update last online date from a client
     * 
     * @param clientId
     *            Client id
     */
    private void updateLastOnlineDate(String clientId) {
        // Get informations client from context
        InfosClient infosClient = ContextServer.getInfosClient(clientId);

        // Update last online date
        if (infosClient != null) {
            infosClient.setLastOnlineDate(new Date());
        }
    }



    /**
     * Create a distant error from an exception
     * 
     * @param exception
     *            Exception
     * 
     * @return Remote error created
     */
    private RemoteError createRemoteError(Exception exception) {
        if (exception instanceof RemoteException) {
            RemoteException de = (RemoteException) exception;
            return de.getRemoteError();
        }
        return RemoteError.ERR_DOLPHLAND_000_UNEXPECTED_ERROR;
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "HelloRequest")
    @ResponsePayload
    public HelloResponse hello(@RequestPayload HelloRequest request) throws Exception {
        HelloResponse response = new HelloResponse();
        response.setResult("Hello " + request.getNom());
        return response;
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "RelayerRequest")
    @ResponsePayload
    public RelayerResponse relayer(@RequestPayload RelayerRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceRelayer.relayEvents(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'relayer' !", exc);
            RelayerResponse error = new RelayerResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "LoginRequest")
    @ResponsePayload
    public LoginResponse login(@RequestPayload LoginRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceUser.login(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'login' !", exc);
            LoginResponse error = new LoginResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "LogoutRequest")
    @ResponsePayload
    public LogoutResponse logout(@RequestPayload LogoutRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceUser.logout(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'logout' !", exc);
            LogoutResponse error = new LogoutResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetUsersRequest")
    @ResponsePayload
    public GetUsersResponse getUsers(@RequestPayload GetUsersRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceUser.getUsers(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'getUsers' !", exc);
            GetUsersResponse error = new GetUsersResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SendMessageRequest")
    @ResponsePayload
    public SendMessageResponse sendMessage(@RequestPayload SendMessageRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceMessage.sendMessage(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'sendMessage' !", exc);
            SendMessageResponse error = new SendMessageResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetApplicationsRequest")
    @ResponsePayload
    public GetApplicationsResponse getApplications(@RequestPayload GetApplicationsRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceUser.getApplications(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'getApplications' !", exc);
            GetApplicationsResponse error = new GetApplicationsResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetPartiesRequest")
    @ResponsePayload
    public GetPartiesResponse getParties(@RequestPayload GetPartiesRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceParty.getParties(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'getParties' !", exc);
            GetPartiesResponse error = new GetPartiesResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreatePartyRequest")
    @ResponsePayload
    public CreatePartyResponse createParty(@RequestPayload CreatePartyRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceParty.createParty(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'createParty' !", exc);
            CreatePartyResponse error = new CreatePartyResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "JoinPartyRequest")
    @ResponsePayload
    public JoinPartyResponse joinParty(@RequestPayload JoinPartyRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceParty.joinParty(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'joinParty' !", exc);
            JoinPartyResponse error = new JoinPartyResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "LeavePartyRequest")
    @ResponsePayload
    public LeavePartyResponse leaveParty(@RequestPayload LeavePartyRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceParty.leaveParty(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'leaveParty' !", exc);
            LeavePartyResponse error = new LeavePartyResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AbortPartyRequest")
    @ResponsePayload
    public AbortPartyResponse abortParty(@RequestPayload AbortPartyRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceParty.abortParty(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'abortParty' !", exc);
            AbortPartyResponse error = new AbortPartyResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetWebContentRequest")
    @ResponsePayload
    public GetWebContentResponse getWebContent(@RequestPayload GetWebContentRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceWeb.getContent(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'getWebContent' !", exc);
            GetWebContentResponse error = new GetWebContentResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetChessBoardRequest")
    @ResponsePayload
    public GetChessBoardResponse getChessBoard(@RequestPayload GetChessBoardRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceChess.getBoard(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'getChessBoard' !", exc);
            GetChessBoardResponse error = new GetChessBoardResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "SetChessPlayerRequest")
    @ResponsePayload
    public SetChessPlayerResponse setChessPlayer(@RequestPayload SetChessPlayerRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceChess.setPlayer(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'setChessPlayer' !", exc);
            SetChessPlayerResponse error = new SetChessPlayerResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetChessMovementsRequest")
    @ResponsePayload
    public GetChessMovementsResponse getChessMovements(@RequestPayload GetChessMovementsRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceChess.getMovements(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'getChessMovements' !", exc);
            GetChessMovementsResponse error = new GetChessMovementsResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }



    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "MoveChessPieceRequest")
    @ResponsePayload
    public MoveChessPieceResponse moveChessPiece(@RequestPayload MoveChessPieceRequest request) throws Exception {
        try {
            updateLastOnlineDate(request.getClientId());
            return serviceChess.movePiece(request);
        } catch (Exception exc) {
            LOG.error("Error during loading service 'moveChessPiece' !", exc);
            MoveChessPieceResponse error = new MoveChessPieceResponse();
            error.setError(createRemoteError(exc));
            return error;
        }
    }
}
