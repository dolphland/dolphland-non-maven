package com.dolphland.server.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.dolphland.core.ws.data.RelayerRequest;
import com.dolphland.core.ws.data.RelayerResponse;
import com.dolphland.core.ws.data.RemoteEvent;
import com.dolphland.core.ws.data.RemoteMessage;
import com.dolphland.server.business.InfosClient;
import com.dolphland.server.context.ContextServer;

/**
 * Service implementation for relayer
 * 
 * @author JayJay
 * 
 */
@Service
public class SVRelayer {

    private static final Logger LOG = Logger.getLogger(SVRelayer.class);



    /**
     * Relay events
     * 
     * @param request
     *            Request
     * @return Response
     */
    public RelayerResponse relayEvents(RelayerRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start of service 'relayEvents'...");
        }

        // Get infos client from context
        InfosClient infosClient = ContextServer.getInfosClient(request.getClientId());

        // If no infos client exists, add a new client
        if (ContextServer.getInfosClient(request.getClientId()) == null) {
            infosClient = ContextServer.addClient(new InfosClient());
        }

        // Update last online date
        infosClient.setLastOnlineDate(new Date());

        // Get and remove events
        List<RemoteEvent> events = infosClient.removeEvents();

        // Get and remove messages
        List<RemoteMessage> messages = infosClient.removeMessages();

        // Construct response
        RelayerResponse response = new RelayerResponse();
        response.setClientId(infosClient.getClientId());
        response.getEvents().addAll(events);
        response.getMessages().addAll(messages);

        if (LOG.isDebugEnabled()) {
            LOG.debug("End of service 'relayEvents'...");
        }

        return response;
    }
}
