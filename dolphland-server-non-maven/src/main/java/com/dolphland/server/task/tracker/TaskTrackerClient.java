package com.dolphland.server.task.tracker;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dolphland.server.business.BOUser;
import com.dolphland.server.business.InfosClient;
import com.dolphland.server.context.ContextServer;
import com.dolphland.server.util.event.EDT;
import com.dolphland.server.util.event.Event;
import com.dolphland.server.util.event.EventDestination;
import com.dolphland.server.util.event.Schedule;
import com.dolphland.server.util.task.Task;
import com.dolphland.server.util.task.TaskContext;

/**
 * Task client tracker
 * 
 * @author JayJay
 * 
 */
@Service
public class TaskTrackerClient extends Task {

    private static final Logger LOG = Logger.getLogger(TaskTrackerClient.class);

    // Time between two integrations
    private static final int SLEEPING_TIME_MS = 1000;

    // Limit where the client is considered as offline
    private static final int LIMIT_OFFLINE_TIME_MS = 10 * 1000;

    // Limit where the client is considered as disconnected
    private static final int LIMIT_DISCONNECTED_TIME_MS = 10 * 60 * 1000;

    private BOUser boUser;



    /**
     * Default constructor
     */
    public TaskTrackerClient() {
        super("TaskClientTracker");
        registerEvent(TaskClientEvent.EVENT_DESTINATION);
    }



    @Autowired
    public void setBoUser(BOUser boUser) {
        this.boUser = boUser;
    }



    @Override
    protected void init(TaskContext ctx) {
        super.init(ctx);

        EDT.schedule(Schedule.event(new TaskClientEvent()).destination(getEventDestination()).startDate(new Date().getTime() + SLEEPING_TIME_MS).interval(SLEEPING_TIME_MS));
    }



    @PostConstruct
    public void doStart() {
        start();
    }



    @PreDestroy
    public void doStop() {
        stop();
    }



    /**
     * Relay events
     * 
     * @param e
     *            Event
     * 
     * @throws InterruptedException
     *             When interruption
     */
    protected void handleEvent(TaskClientEvent e) throws InterruptedException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("handleEvent(): Start tracking client...");
        }

        // Get current time
        Date currentTime = new Date();

        for (InfosClient infosClient : ContextServer.getInfosClient()) {
            if (!infosClient.isOffline() && currentTime.getTime() - LIMIT_OFFLINE_TIME_MS > infosClient.getLastOnlineDate().getTime()) {
                boUser.offlineClient(infosClient.getClientId());
            } else if (infosClient.isOffline() && currentTime.getTime() - LIMIT_DISCONNECTED_TIME_MS > infosClient.getLastOnlineDate().getTime()) {
                boUser.disconnectClient(infosClient.getClientId());
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("handleEvent(): End of tracking client...");
        }
    }

    /**
     * Event called to track client
     * 
     * @author JayJay
     * 
     */
    private static class TaskClientEvent extends Event {

        private static final EventDestination EVENT_DESTINATION = new EventDestination();



        public TaskClientEvent() {
            super(EVENT_DESTINATION);
        }
    }
}
