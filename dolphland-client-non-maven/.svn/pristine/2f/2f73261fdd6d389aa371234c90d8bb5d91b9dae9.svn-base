package com.dolphland.client.util.application.components;

import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.WorkPane;
import com.dolphland.client.util.layout.GridBagConstraintsBuilder;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;

public class FwkWorkPane extends WorkPane {

	private DisabledWorkPaneManager disabledWorkPaneManager;

	/**
	 * Constructeur
	 */
	public FwkWorkPane(MasterFrame frame) {
		super(frame);
		
		// D�finit le manager de d�sactivation du workPane
		disabledWorkPaneManager = new DisabledWorkPaneManager();
		disabledWorkPaneManager.addWorkPane(this);
	}

	/**
	 * D�sactive le WorkPane en affichant un message
	 * @param message Message � afficher
	 */
	public void startDisabledMode(String message){
		disabledWorkPaneManager.startDisabledMode(message);
	}
	
	/**
	 * D�sactive le WorkPane en affichant un message tout en indiquant si la souris peut traverser ou non
	 * @param message Message � afficher
	 * @param mouseTraversable Indique si la souris peut traverser ou non
	 */
	public void startDisabledMode(String message, boolean mouseTraversable){
		disabledWorkPaneManager.startDisabledMode(message, mouseTraversable);
	}
	
	/**
	 * R�active le WorkPane
	 */
	public void endDisabledMode(){
		disabledWorkPaneManager.endDisabledMode();
	}
	
	/** 
	 * Par d�faut la fermeture de la zone de travail n'a aucune r�percussion sur l'application
	 * NB : Pour fermer l'application, passer par la m�thode close() de la MasterFrame => getMasterFrame().close();
	 */
	@Override
	public void close() {
		disabledWorkPaneManager.endDisabledMode();
	}
	
	/**
	 * Cr�ation d'un Builder de <code>GridBagConstraints</code>.
	 */
	protected GridBagConstraintsBuilder createGridBagConstraintsBuilder(){
		return new GridBagConstraintsBuilder();
	}
	
	/**
	 * Cr�ation d'un Builder de <code>TableLayout</code>.
	 */
	protected TableLayoutBuilder createTableLayoutBuilder(){
		return new TableLayoutBuilder();
	}
	
	/**
	 * Cr�ation d'un Builder de <code>TableLayoutConstraints</code>.
	 */
	protected TableLayoutConstraintsBuilder createTableLayoutConstraintsBuilder(){
		return new TableLayoutConstraintsBuilder();
	}
}