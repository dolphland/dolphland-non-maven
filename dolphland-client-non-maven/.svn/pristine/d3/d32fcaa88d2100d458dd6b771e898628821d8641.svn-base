package com.dolphland.client.util.logs;

import java.awt.Color;
import java.awt.Font;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.GlyphView;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.ParagraphView;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;

import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.introspection.IntrospectionUtil;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.transaction.SwingInvoker;

public class UIAppender {

    private static final Logger LOG = Logger.getLogger(UIAppender.class);

    private static final int MAX_TRACES = 1000;

    private static final String LOG_PATTERN = "%d{dd/MM/yy HH:mm:ss} %-5p [%-20t] %-30c{1} - %m%n"; //$NON-NLS-1$

    private static String uiAppenderName;

    private static Object uiAppender;

    private static LogsTextPane editor;

    private static DefaultStyledDocument editorDocument;

    private static int nbTraces;

    private static int firstLineEndPosition;

    private static SimpleAttributeSet debugAtts;
    private static SimpleAttributeSet infoAtts;
    private static SimpleAttributeSet warnAtts;
    private static SimpleAttributeSet errorAtts;



    /**
     * <p>
     * Dynamically installs the log components required for creating a log's
     * dialog
     * </p>
     */
    public static void install() {
        nbTraces = 0;
        firstLineEndPosition = 0;
        uiAppenderName = "_UIAPPENDER"; //$NON-NLS-1$
        Object rootLogger = LogUtil.getRootLogger();
        if (rootLogger == null) {
            return;
        }
        Object oldAppender = IntrospectionUtil.invoke(rootLogger, "getAppender", uiAppenderName); //$NON-NLS-1$
        if (oldAppender != null) {
            IntrospectionUtil.invoke(oldAppender, "close"); //$NON-NLS-1$
        }
        Method addAppender = null;
        try {
            // Implementation depending current log's system
            switch (LogUtil.getCurrentLogSystem()) {
                case LOGBACK :
                    uiAppender = new LogbackAppender();
                    addAppender = rootLogger.getClass().getMethod("addAppender", IntrospectionUtil.loadClass("ch.qos.logback.core.Appender")); //$NON-NLS-1$ //$NON-NLS-2$
                    addAppender.invoke(rootLogger, uiAppender);
                    break;

                case OTHER :
                default :
                    throw new AppBusinessException("Current log's system is not implemented !"); //$NON-NLS-1$
            }
        } catch (Exception ex) {
            LOG.error("install(): Unable to install logs for UI !", ex); //$NON-NLS-1$
        }
    }



    /**
     * <p>
     * Remove any previously installed ui's appender.
     * </p>
     */
    public static void uninstall() {
        try {
            Object rootLogger = null;
            // Implementation depending current log's system
            switch (LogUtil.getCurrentLogSystem()) {
                case LOGBACK :
                    IntrospectionUtil.invoke(uiAppender, "close"); //$NON-NLS-1$
                    rootLogger = LogUtil.getRootLogger();
                    IntrospectionUtil.invoke(rootLogger, "detachAppender", uiAppenderName); //$NON-NLS-1$
                    break;

                case OTHER :
                default :
                    throw new AppBusinessException("Current log's system is not implemented !"); //$NON-NLS-1$
            }
        } catch (Exception e) {
            // ignore
        }
    }



    /**
     * Get editor's pane
     * 
     * @return Editor's pane
     */
    public static LogsTextPane getEditor() {
        return editor;
    }



    private static void doAppend(final String msgContent, final int msgLevel, final int errorLevel, final int warningLevel, final int infoLevel) {
        if (editor != null) {
            new SwingInvoker() {

                public void run() {
                    Document doc = editorDocument;
                    try {
                        AttributeSet atts = null;
                        if (msgLevel >= errorLevel) {
                            atts = errorAtts;
                        } else if (msgLevel >= warningLevel) {
                            atts = warnAtts;
                        } else if (msgLevel >= infoLevel) {
                            atts = infoAtts;
                        } else {
                            atts = debugAtts;
                        }
                        // Remove last characters new line from text to insert
                        String textToInsert = msgContent;
                        while (textToInsert.length() > 0 && StrUtil.NEW_LINE.equals(textToInsert.substring(textToInsert.length() - 1))) {
                            textToInsert = textToInsert.substring(0, textToInsert.length() - 1);
                        }
                        // Insert text
                        doc.insertString(doc.getLength(), textToInsert, atts);
                        if (nbTraces >= MAX_TRACES) {
                            doc.remove(0, firstLineEndPosition);
                            firstLineEndPosition = textToInsert.length();
                        }
                        // If document is used by editor
                        if (doc.equals(editor.getDocument())) {
                            // Set caret's position to the end of document
                            editor.setCaretPosition(doc.getLength());
                        }
                    } catch (Exception e) {
                        LOG.error("BAD LOCATION !"); //$NON-NLS-1$
                    }
                    nbTraces++;
                }
            }.invokeLater();
        }
    }



    private static void doInit() {
        SimpleAttributeSet as = null;

        as = new SimpleAttributeSet();
        StyleConstants.setForeground(as, Color.RED);
        StyleConstants.setFontFamily(as, "Courier"); //$NON-NLS-1$
        StyleConstants.setFontSize(as, 12);
        StyleConstants.setBold(as, true);
        errorAtts = as;

        as = new SimpleAttributeSet();
        StyleConstants.setForeground(as, new Color(245, 103, 27));
        StyleConstants.setFontFamily(as, "Courier"); //$NON-NLS-1$
        StyleConstants.setFontSize(as, 12);
        StyleConstants.setBold(as, true);
        warnAtts = as;

        as = new SimpleAttributeSet();
        StyleConstants.setForeground(as, Color.BLUE);
        StyleConstants.setFontFamily(as, "Courier"); //$NON-NLS-1$
        StyleConstants.setFontSize(as, 12);
        infoAtts = as;

        as = new SimpleAttributeSet();
        StyleConstants.setForeground(as, Color.BLACK);
        StyleConstants.setFontFamily(as, "Courier"); //$NON-NLS-1$
        StyleConstants.setFontSize(as, 12);
        debugAtts = as;

        if (editor == null) {
            editorDocument = new DefaultStyledDocument();
            editor = new LogsTextPane();
            editor.setEditorKit(new WrapEditorKit());
            StyleContext sc = new StyleContext();
            Style defaultStyle = sc.getStyle(StyleContext.DEFAULT_STYLE);
            Style mainStyle = sc.addStyle("MainStyle", defaultStyle); //$NON-NLS-1$
            sc.getFont("Courier", Font.BOLD, 12); //$NON-NLS-1$
            StyleConstants.setFontFamily(mainStyle, "Courier"); //$NON-NLS-1$
            StyleConstants.setFontSize(mainStyle, 12);
            editor.setDocument(editorDocument);
            editor.setLogicalStyle(mainStyle);
        }
    }

    static class LogsTextPane extends JTextPane {

        public void pause() {
            // Clone editor's document to pause
            setDocument(cloneDocument(editorDocument));

            // Set caret's position to the end of document
            setCaretPosition(getDocument().getLength());
        }



        public void resume() {
            // If document is not used by editor
            if (!editorDocument.equals(editor.getDocument())) {
                // Set editor's document to resume
                setDocument(editorDocument);
            }

            // Set caret's position to the end of document
            setCaretPosition(getDocument().getLength());
        }



        private DefaultStyledDocument cloneDocument(DefaultStyledDocument source) {
            DefaultStyledDocument dest = new DefaultStyledDocument();
            try {
                List<DefaultStyledDocument.ElementSpec> specs = new ArrayList<DefaultStyledDocument.ElementSpec>();
                DefaultStyledDocument.ElementSpec spec = new DefaultStyledDocument.ElementSpec(new SimpleAttributeSet(), DefaultStyledDocument.ElementSpec.EndTagType);
                specs.add(spec);
                fillSpecs(source.getDefaultRootElement(), specs, false);
                spec = new DefaultStyledDocument.ElementSpec(new SimpleAttributeSet(), DefaultStyledDocument.ElementSpec.StartTagType);
                specs.add(spec);

                DefaultStyledDocument.ElementSpec[] arr = new DefaultStyledDocument.ElementSpec[specs.size()];
                specs.toArray(arr);
                insertSpecs(dest, dest.getLength(), arr);
                // Remove last character
                if (dest.getLength() > 0) {
                    dest.remove(dest.getLength() - 1, 1);
                }
            } catch (Exception ex) {
                LOG.error("cloneDocument(): Unable to clone document !", ex); //$NON-NLS-1$
            }
            return dest;
        }



        private void insertSpecs(DefaultStyledDocument doc, int offset, DefaultStyledDocument.ElementSpec[] specs) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
            // doc.insert(0, specs); method is protected
            Method m = DefaultStyledDocument.class.getDeclaredMethod("insert", new Class[] { int.class, DefaultStyledDocument.ElementSpec[].class });
            m.setAccessible(true);
            m.invoke(doc, new Object[] { offset, specs });
        }



        private void fillSpecs(Element elem, List<DefaultStyledDocument.ElementSpec> specs, boolean includeRoot) throws BadLocationException {
            DefaultStyledDocument.ElementSpec spec;
            if (elem.isLeaf()) {
                String str = elem.getDocument().getText(elem.getStartOffset(), elem.getEndOffset() - elem.getStartOffset());
                spec = new DefaultStyledDocument.ElementSpec(elem.getAttributes(), DefaultStyledDocument.ElementSpec.ContentType, str.toCharArray(), 0, str.length());
                specs.add(spec);
            }
            else {
                if (includeRoot) {
                    spec = new DefaultStyledDocument.ElementSpec(elem.getAttributes(), DefaultStyledDocument.ElementSpec.StartTagType);
                    specs.add(spec);
                }

                for (int i = 0; i < elem.getElementCount(); i++) {
                    fillSpecs(elem.getElement(i), specs, true);
                }

                if (includeRoot) {
                    spec = new DefaultStyledDocument.ElementSpec(elem.getAttributes(), DefaultStyledDocument.ElementSpec.EndTagType);
                    specs.add(spec);
                }
            }
        }
    }

    private static class WrapEditorKit extends StyledEditorKit {

        ViewFactory defaultFactory = new WrapColumnFactory();



        public ViewFactory getViewFactory() {
            return defaultFactory;
        }



        public MutableAttributeSet getInputAttributes() {
            return super.getInputAttributes();
        }
    }

    private static class WrapColumnFactory implements ViewFactory {

        public View create(Element elem) {
            String kind = elem.getName();
            if (kind != null) {
                if (kind.equals(AbstractDocument.ContentElementName)) {
                    return new WrapLabelView(elem);
                } else if (kind.equals(AbstractDocument.ParagraphElementName)) {
                    return new NoWrapParagraphView(elem);
                } else if (kind.equals(AbstractDocument.SectionElementName)) {
                    return new BoxView(elem, View.Y_AXIS);
                } else if (kind.equals(StyleConstants.ComponentElementName)) {
                    return new ComponentView(elem);
                } else if (kind.equals(StyleConstants.IconElementName)) {
                    return new IconView(elem);
                }
            }

            // default to text display
            return new LabelView(elem);
        }
    }

    private static class NoWrapParagraphView extends ParagraphView {

        public NoWrapParagraphView(Element elem) {
            super(elem);
        }



        public void layout(int width, int height) {
            super.layout(Short.MAX_VALUE, height);
        }



        public float getMinimumSpan(int axis) {
            return super.getPreferredSpan(axis);
        }
    }

    private static class WrapLabelView extends LabelView {

        public WrapLabelView(Element elem) {
            super(elem);
        }



        public int getBreakWeight(int axis, float pos, float len) {
            if (axis == View.X_AXIS) {
                checkPainter();
                int p0 = getStartOffset();
                int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
                if (p1 == p0) {
                    // can't even fit a single character
                    return View.BadBreakWeight;
                }
                try {
                    // if the view contains line break char return forced break
                    if (getDocument().getText(p0, p1 - p0).indexOf("\r") >= 0) { //$NON-NLS-1$
                        return View.ForcedBreakWeight;
                    }
                } catch (BadLocationException ex) {
                    // should never happen
                }
            }
            return super.getBreakWeight(axis, pos, len);
        }



        public View breakView(int axis, int p0, float pos, float len) {
            if (axis == View.X_AXIS) {
                checkPainter();
                int p1 = getGlyphPainter().getBoundedPosition(this, p0, pos, len);
                try {
                    // if the view contains line break char break the view
                    int index = getDocument().getText(p0, p1 - p0).indexOf("\r"); //$NON-NLS-1$
                    if (index >= 0) {
                        GlyphView v = (GlyphView) createFragment(p0, p0 + index + 1);
                        return v;
                    }
                } catch (BadLocationException ex) {
                    // should never happen
                }
            }
            return super.breakView(axis, p0, pos, len);
        }
    }

    private static class LogbackAppender extends ConsoleAppender<ILoggingEvent> {

        private LogbackAppender() {
            LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
            PatternLayoutEncoder ple = new PatternLayoutEncoder();
            ple.setPattern(LOG_PATTERN);
            ple.setContext(lc);
            ple.start();
            setName(uiAppenderName);
            setEncoder(ple);
            setContext(lc);
            start();
        }



        @Override
        protected void append(final ILoggingEvent event) {
            String msg = ((LayoutWrappingEncoder<ILoggingEvent>) getEncoder()).getLayout().doLayout(event);
            UIAppender.doAppend(msg, event.getLevel().toInt(), Level.ERROR_INT, Level.WARN_INT, Level.INFO_INT);
        }



        @Override
        public void start() {
            UIAppender.doInit();
            super.start();
        }
    }
}
