package com.dolphland.client.util.dialogs;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;

import org.apache.log4j.Logger;

import com.dolphland.client.util.action.KeyActionBinder;
import com.dolphland.client.util.event.CloseAllDialogsEvt;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EventSubscriber;

/**
 * <p>
 * JDialog extension that handles closing on the escape key strike.
 * </p>
 * <p>
 * This dialog is able to receive {@link CloseAllDialogsEvt} and closes itself
 * upon such event reception.
 * </p>
 * 
 * @author JayJay
 * @author J�r�my Scafi
 */
public class FwkDialog extends JDialog {

    private static final Logger LOG = Logger.getLogger(FwkDialog.class);

    private EventSubscriber closeDialogSubscriber;



    private void installKeyActionDisposer() {
        // Close dialog if Echap pressed
        KeyActionBinder.bindKey(this, KeyEvent.VK_ESCAPE, new Runnable() {

            public void run() {
                doCloseDialog(new FwkDialogClosingEvent(FwkDialogCloseEvent.USER_ESCAPE_EVENT));
            }
        });
    }



    private void installSubscriber() {
        this.addComponentListener(new ComponentAdapter() {

            @Override
            public void componentShown(ComponentEvent e) {
                EDT.unsubscribe(getCloseDialogSubscriber()).forAllEvents();
                EDT.subscribe(getCloseDialogSubscriber()).forEvents(CloseAllDialogsEvt.DEST);
            }



            @Override
            public void componentHidden(ComponentEvent e) {
                EDT.unsubscribe(getCloseDialogSubscriber()).forAllEvents();
            }
        });

        this.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                doCloseDialog(new FwkDialogClosingEvent(FwkDialogCloseEvent.WINDOW_CLOSING_EVENT));
            }

        });
    }



    public FwkDialog() throws HeadlessException {
        super();
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Dialog owner, boolean modal) throws HeadlessException {
        super(owner, modal);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Dialog owner, String title, boolean modal, GraphicsConfiguration gc) throws HeadlessException {
        super(owner, title, modal, gc);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Dialog owner, String title, boolean modal) throws HeadlessException {
        super(owner, title, modal);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Dialog owner, String title) throws HeadlessException {
        super(owner, title);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Dialog owner) throws HeadlessException {
        super(owner);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Frame owner, boolean modal) throws HeadlessException {
        super(owner, modal);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Frame owner, String title, boolean modal, GraphicsConfiguration gc) {
        super(owner, title, modal, gc);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Frame owner, String title, boolean modal) throws HeadlessException {
        super(owner, title, modal);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Frame owner, String title) throws HeadlessException {
        super(owner, title);
        installKeyActionDisposer();
        installSubscriber();
    }



    public FwkDialog(Frame owner) throws HeadlessException {
        super(owner);
        installKeyActionDisposer();
        installSubscriber();
    }



    /**
     * <p>
     * This method is called when a close request has been received for the
     * dialog that this class is bound to.
     * </p>
     * <p>
     * The specified {@link FwkDialogClosingEvent} parameter makes it possible
     * to veto the close operation if implementors invoke the
     * {@link FwkDialogClosingEvent#setVetoClosing(boolean)} method. Invoking
     * this method with a true parameter will prevent the dialog from closing.
     * </p>
     * <p>
     * By default, this method does nothing and thus, closing will occur. There
     * is 3 events that trigegrs dialog closing:<br>
     * <ol>
     * <li>The user has clicked on the top right cross button that closes the
     * dialog</li>
     * <li>The user has pressed the ESCAPE key</li>
     * <li>The dialog has received an {@link CloseAllDialogsEvt}</li>
     * </ol>
     * </p>
     * 
     * @param dce
     *            The event object attached to the dialog closing event that
     *            this method represents.
     */
    protected void dialogClosing(FwkDialogClosingEvent dce) {
    }



    /**
     * Get subscriber that close dialog when CloseAllDialogs event received
     * 
     * @return Subscriber that close dialog when CloseAllDialogs event received
     */
    private EventSubscriber getCloseDialogSubscriber() {
        if (closeDialogSubscriber == null) {
            closeDialogSubscriber = new CloseDialogSubscriber();
        }
        return closeDialogSubscriber;
    }



    private void doCloseDialog(FwkDialogClosingEvent dce) {
        try {
            dialogClosing(dce);
        } catch (Exception e) {
            if (LOG.isInfoEnabled()) {
                LOG.info("eventReceived(): dialogClosing() has raised an exception !", e); //$NON-NLS-1$
            }
        }
        if (!dce.isVetoClosing()) {
            EDT.unsubscribe(getCloseDialogSubscriber()).forAllEvents();
            super.dispose();
        }
    }



    @Override
    public void dispose() {
        doCloseDialog(new FwkDialogClosingEvent(FwkDialogCloseEvent.WINDOW_CLOSING_EVENT));
    }

    /**
     * Close dialog when CloseAllDialogs event received
     * 
     * @author JayJay
     * 
     */
    private class CloseDialogSubscriber extends DefaultEventSubscriber {

        @SuppressWarnings("unused")
        public void eventReceived(CloseAllDialogsEvt se) {
            doCloseDialog(new FwkDialogClosingEvent(FwkDialogCloseEvent.CLOSE_ALL_DIALOGS_EVENT));
        }
    }
}
