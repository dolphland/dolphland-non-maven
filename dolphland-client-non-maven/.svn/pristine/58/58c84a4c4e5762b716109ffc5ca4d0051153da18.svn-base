/*
 * $Log: ProductAction.java,v $
 * Revision 1.3 2013/04/15 08:09:48 bvatan
 * - D�placement des UIAction et classes associ�s dans
 * com.vallourec.ctiv.fwk.ui.application.actions
 * Revision 1.2 2010/09/14 13:24:05 bvatan
 * - correction bug trigger modifier
 * Revision 1.1 2009/02/04 13:38:39 jscafi
 * - Ajout de ProducTable permettant de g�rer des tables de produits
 * Revision 1.1 2009/01/06 14:17:04 bvatan
 * - ajotu gestion du blocage des barre par coul�e
 */

package com.dolphland.client.util.table;

import javax.swing.Icon;

import com.dolphland.client.util.action.UIAction;

/**
 * Classe bas�e sur le syst�me d'Actions de Swing permettant d'ex�cuter une
 * proc�dure (MVCTx) � partir d'un
 * ou plusieurs composants Swing provenant d'une table de produits.<br>
 * <br>
 * Typage:
 * <ul>
 * <li>T: R�sultat de l'action provenant de la m�thode <code>doExecute</code>
 * </ul>
 * <br>
 * Pr�conisations d'utilisation des actions (<code>UIAction</code> et d�riv�es): <br>
 * <ul>
 * <li>Cr�er les actions � l'int�rieur du sc�nario (classe interne) et non dans
 * une classe propre.<br>
 * Les vues sont cens�es ne manipuler que des <code>UIAction</code> fournies par
 * le sc�nario via un setter.
 * <li>Eviter dans la mesure du possible l'utilisation de variables dans les
 * actions � cause du multithreading
 * <li>D�clarer les �v�nements des actions en private package tout en suivant la
 * norme NomActionEvt
 * <li>Eviter dans la mesure du possible de faire des setEnabled � partir de la
 * vue.<br>
 * C'est au sc�nario de prendre cette d�cision et non � la vue.
 * </ul>
 * <br>
 * Exemple respectant les pr�conisations d'utilisation:<br>
 * <br>
 * <ol>
 * <li>Cr�ation de la classe �v�nement h�ritant de <code>UIActionEvt</code>, qui
 * contient tous les param�tres n�cessaires � l'action <br>
 * <code>
 * <pre>
 * class MonActionEvt extends UIActionEvt{
 * 	private String text;
 * 	public void setText(String text){
 * 		this.text = text;
 * 	}
 * 	public String getText(){
 * 		return text;
 * 	}
 * }
 * </pre>
 * </code>
 * 
 * <li>Dans la vue, d�claration puis installation de l'action via un setter
 * <code>
 * <pre>
 * UIAction&lt;MonActionEvt,?&gt; monAction ;
 *      
 * public void setMonAction(UIAction&lt;MonActionEvt,?&gt; monAction){
 * 	this.monAction = monAction;
 * 	monAction.install(btnMonAction); 
 * }
 * </pre>
 * </code>
 * 
 * <li>Dans la vue, ex�cution de l'action lors d'un �v�nement Swing avec pour
 * param�tre d'ex�cution l'�v�nement de l'action <code>
 * <pre>
 * JButton btnMonAction;
 *      
 * public JButton getBtnMonAction(){
 * 	if(btnMonAction == null){
 * 		btnMonAction = new JButton("Go Action");
 * 		btnMonAction.addActionListener(new ActionListener(){
 * 			public void actionPerformed(ActionEvent e) {
 * 				if(monAction != null){
 * 					MonActionEvt evt = new MonActionEvt();
 * 					evt.setText("Hello");
 * 					monAction.execute(evt);
 * 				}
 * 			}			
 * 		});
 * 	}
 * 	return btnMonAction;
 * }
 * </pre>
 * </code>
 * 
 * <li>Dans le sc�nario, cr�ation de la classe interne puis affectation de
 * l'action � la vue <code>
 * <pre> 
 * private class MonAction extends UIAction&lt;MonActionEvt,Boolean&gt;{
 * 	protected Boolean doExecute(MonActionEvt param) {			
 * 		log.info("Ex�cution de MonAction : " + param.getText());
 * 		return true;
 * 	}
 * }
 * 
 * view.setMonAction(new MonAction());
 * </pre>
 * </code>
 * </ol>
 * 
 * @author J�r�my Scafi
 * @author JayJay
 * 
 */
public class ProductAction<T> extends UIAction<ProductActionEvt, T> {

    /**
     * Constructeur sans param�tre
     */
    public ProductAction() {
        super();
        setTriggerModifier(TRIGGER_CONTEXT);
    }



    /**
     * Constructeur o� l'on sp�cifie le nom
     * 
     * @param name
     *            Nom de l'action
     */
    public ProductAction(String name) {
        super(name);
        setTriggerModifier(TRIGGER_CONTEXT);
    }



    /**
     * Constructeur o� l'on sp�cifie un nom et une ic�ne
     * 
     * @param name
     *            Nom de l'action
     * @param icon
     *            Ic�ne de l'action
     */
    public ProductAction(String name, Icon icon) {
        super(name, icon);
        setTriggerModifier(TRIGGER_CONTEXT);
    }

}
