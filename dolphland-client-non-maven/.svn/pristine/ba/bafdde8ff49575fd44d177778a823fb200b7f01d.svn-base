package com.dolphland.client.usecase.application.party;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.event.EventChatMessage;
import com.dolphland.client.event.EventPartyChanged;
import com.dolphland.client.event.EventPartyFinished;
import com.dolphland.client.service.SVFactory;
import com.dolphland.client.usecase.application.AbstractApplicationScenario;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EventSubscriber;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.transaction.MVCTx;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.RemoteMessageType;

/**
 * Scenario for a party
 * 
 * @author JayJay
 * 
 *         P : type of the party's representation
 */
public abstract class AbstractPartyScenario<P> extends AbstractApplicationScenario {

    private static final Logger LOG = Logger.getLogger(AbstractPartyScenario.class);

    private static final MessageFormater FMT = MessageFormater.getFormater(AbstractPartyScenario.class);

    // Party id
    private String partyId;

    // View
    private PartyView view;

    // Party's renderer
    private PartyRenderer<P> renderer;

    // Event subscriber
    private EventSubscriber eventSubscriber;



    /**
     * Constructor with application id and party id
     * 
     * @param applicationId
     *            Application id
     * @param partyId
     *            Party id
     */
    public AbstractPartyScenario(ApplicationId applicationId, String partyId) {
        super(applicationId);

        AssertUtil.notNull(partyId, "partyId is null !");
        this.partyId = partyId;

        // Create event subscriber
        eventSubscriber = new PartyEventSubscriber();

        // Create view
        view = new PartyView();

        // Set actions
        view.setActionChat(new ActionChat());

        // Set party's component
        view.setPartyComponent(getPartyRenderer().getPartyComponent());
    }



    @Override
    protected void doEnter(Object param) {
        super.doEnter(param);

        // Subscribe
        subscribe();

        getWorkPane().setView(view);

        try {
            doStartParty();
        } catch (Exception exc) {
            LOG.error("Error when starting party", exc);
        }
    }



    @Override
    protected void doLeave() {
        try {
            doStopParty();
        } catch (Exception exc) {
            LOG.error("Error when stopping party", exc);
        }

        unsubscribe();
    }



    protected void subscribe() {
        EDT.subscribe(eventSubscriber)
            .forEvents(EventChatMessage.DEST)
            .forEvents(EventPartyFinished.DEST)
            .forEvents(EventPartyChanged.DEST);
    }



    protected void unsubscribe() {
        EDT.unsubscribe(eventSubscriber).forAllEvents();
    }



    /**
     * Get party's renderer
     * 
     * @return Party's renderer
     */
    protected PartyRenderer<P> getPartyRenderer() {
        if (renderer == null) {
            renderer = createPartyRenderer();
            renderer.setAnimationsEnabled(true);
        }
        return renderer;
    }



    /**
     * Create party's renderer (to implement)
     * 
     * @return Party's renderer
     */
    protected abstract PartyRenderer<P> createPartyRenderer();



    /**
     * @return the party
     */
    protected Party getParty() {
        return ContextClient.getParty(partyId);
    }



    /**
     * Called when starting party
     */
    protected void doStartParty() {
        getWorkPane().setView(view);
    }



    /**
     * Called when stopping party
     */
    protected void doStopParty() {
    }



    /**
     * Called when aborting party
     */
    protected void doAbortParty() {
        new TransactionAbortParty().execute();
    }

    /**
     * Action to chat
     * 
     * @author JayJay
     * 
     */
    private class ActionChat extends UIAction<PartyActionEvt, Object> {

        /**
         * Default constructor
         */
        public ActionChat() {
            super(FMT.format("AbstractPartyScenario.RS_ACTION_CHAT")); //$NON-NLS-1$
        }



        @Override
        public boolean isEnabled(PartyActionEvt ctx) {
            clearDisabledCauses();
            if (StrUtil.isEmpty(ctx.getMessage())) {
                addDisabledCause(FMT.format("AbstractPartyScenario.RS_ERROR_NO_MESSAGE")); //$NON-NLS-1$
            }
            return getDisabledCauses().isEmpty() && super.isEnabled(ctx);
        }



        @Override
        protected boolean shouldExecute(PartyActionEvt param) {
            return super.shouldExecute(param) && isEnabled(param);
        }



        @Override
        protected void preExecute(PartyActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("AbstractPartyScenario.RS_WAITING_CHAT"));
        }



        @Override
        protected Object doExecute(PartyActionEvt param) {
            SVFactory.createSVMessage().sendMessage(RemoteMessageType.CHAT, param.getMessage(), getParty());
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
            view.initChatInput();
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Transaction to abort the party
     * 
     * @author jeremy.scafi
     */
    private class TransactionAbortParty extends MVCTx<Object, Object> {

        @Override
        protected boolean shouldExecute(Object param) {
            if (getParty() == null) {
                return false;
            }
            return !getParty().isFinished() && getWorkPane().showConfirmDialog(FMT.format("AbstractPartyScenario.RS_CONFIRM_ABORT_PARTY"));
        }



        @Override
        protected void preExecute(Object param) {
            getWorkPane().showInfoMessage(FMT.format("AbstractPartyScenario.RS_WAITING_ABORT_PARTY"));
        }



        @Override
        protected Object doExecute(Object param) {
            SVFactory.createSVParty().abortParty(getParty().getId());
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Event subscriber for party
     * 
     * @author JayJay
     * 
     */
    private class PartyEventSubscriber extends DefaultEventSubscriber {

        public void eventReceived(EventChatMessage e) {
            if (e.getParty() == null || !e.getParty().getId().equals(partyId)) {
                return;
            }
            getPartyRenderer().getUserComponent(e.getSender().getId()).showDialogBubble(e.getContent());
        }



        public void eventReceived(EventPartyFinished e) {
            if (e.getParty() == null || !e.getParty().getId().equals(partyId)) {
                return;
            }

            // Prevent that party is finished
            getWorkPane().showInfoDialog(FMT.format("AbstractPartyScenario.RS_FINISHED_PARTY"));
        }



        public void eventReceived(EventPartyChanged<P> e) {
            if (e.getParty() == null || !e.getParty().getId().equals(partyId)) {
                return;
            }

            // Update party's representation
            getPartyRenderer().updateParty(e.getParty(), e.getPartyRepresentation());

            // Set application loaded if party is represented
            setApplicationLoaded(true);
        }
    }
}
