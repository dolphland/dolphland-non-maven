package com.dolphland.client.context;

import java.util.ArrayList;
import java.util.List;

import com.dolphland.client.util.string.StrUtil;
import com.dolphland.core.ws.data.Application;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.User;

/**
 * Context for the client
 * 
 * @author JayJay
 * 
 */
public class ContextClient {

    private static ContextClient context;

    // Client id
    private String clientId = null;

    // User id
    private String userId = null;

    // Users
    private List<User> users = new ArrayList<User>();

    // Applications
    private List<Application> applications = new ArrayList<Application>();

    // Parties
    private List<Party> parties = new ArrayList<Party>();



    private synchronized static ContextClient getInstance() {
        if (context == null) {
            start();
        }
        return context;
    }



    /**
     * Start context
     */
    public static synchronized void start() {
        if (context == null) {
            context = new ContextClient();
        }
    }



    /**
     * Stop context
     */
    public static synchronized void stop() {
        context = null;
    }



    /**
     * @return the clientId
     */
    public static synchronized String getClientId() {
        return getInstance().clientId;
    }



    /**
     * @param clientId
     *            the clientId to set
     */
    public static synchronized void setClientId(String clientId) {
        getInstance().clientId = StrUtil.isEmpty(clientId) ? StrUtil.EMPTY_STRING : clientId;
    }



    /**
     * @return the userId
     */
    public static synchronized String getUserId() {
        return getInstance().userId;
    }



    /**
     * @param userId
     *            the userId to set
     */
    public static synchronized void setUserId(String userId) {
        getInstance().userId = StrUtil.isEmpty(userId) ? StrUtil.EMPTY_STRING : userId;
    }



    /**
     * Get users
     * 
     * @return Users
     */
    public static synchronized List<User> getUsers() {
        return new ArrayList<User>(getInstance().users);
    }



    /**
     * Get user from id
     * 
     * @param userId
     *            User id
     * 
     * @return Users
     */
    public static synchronized User getUser(String userId) {
        for (User user : getInstance().users) {
            if (user.getId().equals(userId)) {
                return user;
            }
        }
        return null;
    }



    /**
     * Set users
     * 
     * @param users
     *            Users
     */
    public static synchronized void setUsers(List<User> users) {
        getInstance().users = new ArrayList<User>(users);
    }



    /**
     * Get applications
     * 
     * @return Applications
     */
    public static synchronized List<Application> getApplications() {
        return new ArrayList<Application>(getInstance().applications);
    }



    /**
     * Get application from id
     * 
     * @param applicationId
     *            Application id
     * 
     * @return Application
     */
    public static synchronized Application getApplication(ApplicationId applicationId) {
        for (Application application : getInstance().applications) {
            if (application.getId().equals(applicationId)) {
                return application;
            }
        }
        return null;
    }



    /**
     * Set applications
     * 
     * @param applications
     *            Applications
     */
    public static synchronized void setApplications(List<Application> applications) {
        getInstance().applications = new ArrayList<Application>(applications);
    }



    /**
     * Get parties
     * 
     * @return Parties
     */
    public static synchronized List<Party> getParties() {
        return new ArrayList<Party>(getInstance().parties);
    }



    /**
     * Get party from id
     * 
     * @param partyId
     *            Party id
     * 
     * @return Party
     */
    public static synchronized Party getParty(String partyId) {
        for (Party party : getInstance().parties) {
            if (party.getId().equals(partyId)) {
                return party;
            }
        }
        return null;
    }



    /**
     * Set parties
     * 
     * @param parties
     *            Parties
     */
    public static synchronized void setParties(List<Party> parties) {
        getInstance().parties = new ArrayList<Party>(parties);
    }
}
