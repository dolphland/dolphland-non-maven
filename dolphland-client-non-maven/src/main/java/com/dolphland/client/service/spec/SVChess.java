package com.dolphland.client.service.spec;

import java.util.List;

import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;
import com.dolphland.core.ws.data.ChessMovement;
import com.dolphland.core.ws.data.ChessPiece;
import com.dolphland.core.ws.data.ChessPosition;

/**
 * Business for chess
 * 
 * @author JayJay
 * 
 */
public interface SVChess {

    /**
     * Get board for party
     * 
     * @param partyId
     *            Party id
     * 
     * @return Board
     */
    public ChessBoard getBoard(String partyId);



    /**
     * Called to set the player for party representing by the color
     * 
     * @param partyId
     *            Party id
     * @param color
     *            Player's color to become
     * 
     * @return Board
     */
    public ChessBoard setPlayer(String partyId, ChessColor color);



    /**
     * Get movements for a piece
     * 
     * @param partyId
     *            Party id
     * @param piece
     *            Piece
     * 
     * @return Movements
     */
    public List<ChessMovement> getMovements(String partyId, ChessPiece piece);



    /**
     * Move a piece to a position
     * 
     * @param partyId
     *            Party id
     * @param piece
     *            Piece
     * @param position
     *            Position
     * 
     * @return Board
     */
    public ChessBoard movePiece(String partyId, ChessPiece piece, ChessPosition position);
}
