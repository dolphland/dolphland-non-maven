package com.dolphland.client.service.spec;

import java.util.List;

import com.dolphland.core.ws.data.Application;
import com.dolphland.core.ws.data.User;

/**
 * Business for user
 * 
 * @author JayJay
 * 
 */
public interface SVUser {

    /**
     * Get users
     * 
     * @return Users
     */
    public List<User> getUsers();



    /**
     * Get applications for current user
     * 
     * @return Applications for current user
     */
    public List<Application> getApplications();



    /**
     * Login a user from id and password
     * 
     * @param userId
     *            User id
     * @param userPassword
     *            User password
     */
    public void login(String userId, String userPassword);



    /**
     * Logout the current user
     * 
     * @param onlyOneTry
     *            If true, only one try will be attempted 
     */
    public void logout(boolean onlyOneTry);
}
