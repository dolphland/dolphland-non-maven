package com.dolphland.client.service.web;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.ws.client.core.WebServiceTemplate;

import com.dolphland.client.util.assertion.AssertUtil;

/**
 * Invoker for web service
 * 
 * @author JayJay
 * 
 */
public class WebServiceInvoker {

    // Max number of simultaneous web service request
    private static final int MAX_NUMBER_SIMULTANEOUS_WEB_SERVICE_REQUEST = 5;

    // Create web service executor
    private static ExecutorService executor = Executors.newFixedThreadPool(MAX_NUMBER_SIMULTANEOUS_WEB_SERVICE_REQUEST);

    // Web service template
    private WebServiceTemplate wsTemplate;
    


    /**
     * Constructor with Web service template
     * 
     * @param wsTemplate
     *            Web service template
     */
    public WebServiceInvoker(WebServiceTemplate wsTemplate) {
        AssertUtil.notNull(wsTemplate, "wsTemplate is null !");
        this.wsTemplate = wsTemplate;
    }



    /**
     * Invoke web service from request
     * 
     * @param request
     *            Request
     * 
     * @return Response
     * 
     * @throws CancellationException
     *             - if the service's execution was cancelled
     * @throws ExecutionException
     *             - if the service's execution threw an exception
     * @throws InterruptedException
     *             - if the current service's execution was interrupted while
     *             waiting
     * @throws TimeoutException
     *             - if the wait timed out
     */
    public Object invoke(Object request) throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
        return invoke(request, 0);
    }



    /**
     * Invoke web service from request with timeout specified.
     * 
     * @param request
     *            Request
     * @param timeout
     *            Timeout Can be <= 0 if no wait timed out
     * 
     * @return Response
     * 
     * @throws CancellationException
     *             - if the service's execution was cancelled
     * @throws ExecutionException
     *             - if the service's execution threw an exception
     * @throws InterruptedException
     *             - if the current service's execution was interrupted while
     *             waiting
     * @throws TimeoutException
     *             - if the wait timed out
     */
    public Object invoke(Object request, int timeout) throws CancellationException, ExecutionException, InterruptedException, TimeoutException {
        Future<Object> future = executor.submit(new TaskWebServiceInvoker(request));
        if (timeout > 0) {
            return future.get(timeout, TimeUnit.MILLISECONDS);
        }
        return future.get();
    }

    /**
     * Task to invoke a web service
     * 
     * @author JayJay
     * 
     */
    private class TaskWebServiceInvoker implements Callable<Object> {

        private Object request;



        public TaskWebServiceInvoker(Object request) {
            this.request = request;
        }



        @Override
        public Object call() throws Exception {
            return wsTemplate.marshalSendAndReceive(request);
        }
    }
}
