package com.dolphland.client.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.event.EventApplicationsChanged;
import com.dolphland.client.event.EventChatMessage;
import com.dolphland.client.event.EventPartiesChanged;
import com.dolphland.client.event.EventPartyFinished;
import com.dolphland.client.event.EventPartyJoined;
import com.dolphland.client.event.EventPartyLeft;
import com.dolphland.client.event.EventUserDisconnection;
import com.dolphland.client.event.EventUserLogin;
import com.dolphland.client.event.EventUserLogout;
import com.dolphland.client.event.EventUserOffline;
import com.dolphland.client.event.EventUserReOnline;
import com.dolphland.client.event.EventUserReplacement;
import com.dolphland.client.event.EventUsersChanged;
import com.dolphland.client.service.spec.SVRelayer;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.introspection.IntrospectionUtil;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.RelayerRequest;
import com.dolphland.core.ws.data.RelayerResponse;
import com.dolphland.core.ws.data.RemoteEvent;
import com.dolphland.core.ws.data.RemoteMessage;
import com.dolphland.core.ws.data.User;

/**
 * Service implementation for relayer
 * 
 * @author JayJay
 * 
 */
class SVRelayerImpl extends AbstractService implements SVRelayer {

    private static final Logger LOG = Logger.getLogger(SVRelayerImpl.class);



    @Override
    public void relay() {
        // Construct request
        RelayerRequest request = new RelayerRequest();
        request.setClientId(ContextClient.getClientId());

        // Get response
        RelayerResponse response = (RelayerResponse) getRequester().request(request);

        // Refresh client id if changed
        if (!response.getClientId().equals(request.getClientId())) {
            ContextClient.setClientId(response.getClientId());
        }
        
        // Post messages
        postMessages(response);
        
        // post events
        postEvents(response);
    }
    
    
    
    /**
     * Post messages from response
     * 
     * @param response Response
     */
    private void postMessages(RelayerResponse response){
    	
    	// Get messages
        List<RemoteMessage> messages = response == null ? null : response.getMessages();
        
        // Post messages 
        if(messages != null){
            for (RemoteMessage message : messages) {
            	switch (message.getType()) {
					case CHAT:
						User sender = ContextClient.getUser(message.getSenderUserId());
						Party party = ContextClient.getParty(message.getPartyId());
	                    EDT.postEvent(new EventChatMessage(message.getContent(), sender, party));
						break;
	
					default:
						break;
				}
            }
        }
    }
    
    
    
    /**
     * Post events from response
     * 
     * @param response Response
     */
    private void postEvents(RelayerResponse response){

        // Get events
        List<RemoteEvent> events = response == null ? null : response.getEvents();

        // Relay events
        if (events != null) {
            for (RemoteEvent event : events) {
                if(LOG.isDebugEnabled()){
                    LOG.debug("Receipt " + IntrospectionUtil.toString(event));
                }
                switch (event.getType()) {
                	case USER_LOGIN :
                		User user = ContextClient.getUser(event.getSourceId());
                        EDT.postEvent(new EventUserLogin(user, event.getTargetId()));
                        break;
                        
                	case USER_LOGOUT :
                		user = ContextClient.getUser(event.getSourceId());
                        EDT.postEvent(new EventUserLogout(user, event.getTargetId()));
                        break;
                        
                	case USER_OFFLINE :
                		user = ContextClient.getUser(event.getSourceId());
                        EDT.postEvent(new EventUserOffline(user, event.getTargetId()));
                        break;
                        
                	case USER_REONLINE :
                		user = ContextClient.getUser(event.getSourceId());
                        EDT.postEvent(new EventUserReOnline(user, event.getTargetId()));
                        break;
                        
                	case USER_DISCONNECTION :
                		user = ContextClient.getUser(event.getSourceId());
                        EDT.postEvent(new EventUserDisconnection(user, event.getTargetId()));
                        break;
                        
                	case USER_REPLACEMENT :
                		user = ContextClient.getUser(event.getSourceId());
                        EDT.postEvent(new EventUserReplacement(user, event.getTargetId()));
                        break;
                        
                    case USERS_CHANGED :
                        try {
                            // Set all users
                            ContextClient.setUsers(SVFactory.createSVUser().getUsers());
                        } catch (Exception exc) {
                            LOG.error("Error when getting users", exc);
                        }
                        EDT.postEvent(new EventUsersChanged());
                        break;

                    case APPLICATIONS_CHANGED :
                        try {
                            // Set all applications
                            ContextClient.setApplications(SVFactory.createSVUser().getApplications());
                        } catch (Exception exc) {
                            LOG.error("Error when getting applications", exc);
                        }
                        EDT.postEvent(new EventApplicationsChanged());
                        break;

                    case PARTIES_CHANGED :
                        try {
                            // Set all parties
                            ContextClient.setParties(SVFactory.createSVParty().getParties());
                        } catch (Exception exc) {
                            LOG.error("Error when getting parties", exc);
                        }
                        EDT.postEvent(new EventPartiesChanged());
                        break;

                    case PARTY_JOINED :
                		Party party = ContextClient.getParty(event.getSourceId());
                		user = ContextClient.getUser(event.getTargetId());
                        EDT.postEvent(new EventPartyJoined(party, user));
                        break;

                    case PARTY_LEFT :
                		party = ContextClient.getParty(event.getSourceId());
                		user = ContextClient.getUser(event.getTargetId());
                        EDT.postEvent(new EventPartyLeft(party, user));
                        break;

                    case PARTY_FINISHED :
                        party = ContextClient.getParty(event.getSourceId());
                        EDT.postEvent(new EventPartyFinished(party));
                        break;

                    case PARTY_CHANGED :
                        try {
                            SVFactory.createSVParty().synchronizeParty(event.getSourceId());
                        } catch (Exception exc) {
                            LOG.error("Error when synchronizing party " + event.getSourceId(), exc);
                        }
                        break;

                    default :
                        break;
                }
            }
        }
    }
}
