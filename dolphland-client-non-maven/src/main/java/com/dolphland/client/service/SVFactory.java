package com.dolphland.client.service;

import com.dolphland.client.service.spec.SVChess;
import com.dolphland.client.service.spec.SVMessage;
import com.dolphland.client.service.spec.SVParty;
import com.dolphland.client.service.spec.SVRelayer;
import com.dolphland.client.service.spec.SVUser;
import com.dolphland.client.service.spec.SVWeb;

/**
 * Service factory
 * 
 * @author JayJay
 * 
 */
public class SVFactory {

    /**
     * Create a service for event
     * 
     * @return Service for event
     */
    public static SVRelayer createSVEvent() {
        return new SVRelayerImpl();
    }



    /**
     * Create a service for message
     * 
     * @return Service for message
     */
    public static SVMessage createSVMessage() {
        return new SVMessageImpl();
    }



    /**
     * Create a service for user
     * 
     * @return Service for user
     */
    public static SVUser createSVUser() {
        return new SVUserImpl();
    }



    /**
     * Create a service for party
     * 
     * @return Service for party
     */
    public static SVParty createSVParty() {
        return new SVPartyImpl();
    }



    /**
     * Create a service for web
     * 
     * @return Service for web
     */
    public static SVWeb createSVWeb() {
        return new SVWebImpl();
    }



    /**
     * Create a service for chess
     * 
     * @return Service for chess
     */
    public static SVChess createSVChess() {
        return new SVChessImpl();
    }
}
