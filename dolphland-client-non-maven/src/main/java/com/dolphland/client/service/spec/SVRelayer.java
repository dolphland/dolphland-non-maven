package com.dolphland.client.service.spec;

/**
 * Business for relayer
 * 
 * @author JayJay
 * 
 */
public interface SVRelayer {

    /**
     * Relay
     */
    public void relay();
}
