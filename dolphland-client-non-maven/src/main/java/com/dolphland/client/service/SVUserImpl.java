package com.dolphland.client.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.service.spec.SVUser;
import com.dolphland.client.util.introspection.IntrospectionUtil;
import com.dolphland.core.ws.data.Application;
import com.dolphland.core.ws.data.GetApplicationsRequest;
import com.dolphland.core.ws.data.GetApplicationsResponse;
import com.dolphland.core.ws.data.GetUsersRequest;
import com.dolphland.core.ws.data.GetUsersResponse;
import com.dolphland.core.ws.data.LoginRequest;
import com.dolphland.core.ws.data.LoginResponse;
import com.dolphland.core.ws.data.LogoutRequest;
import com.dolphland.core.ws.data.LogoutResponse;
import com.dolphland.core.ws.data.User;

/**
 * Service implementation for user
 * 
 * @author JayJay
 * 
 */
class SVUserImpl extends AbstractService implements SVUser {

    private static final Logger LOG = Logger.getLogger(SVUserImpl.class);



    @Override
    public List<User> getUsers() {
        // Construct request
        GetUsersRequest request = new GetUsersRequest();
        request.setClientId(ContextClient.getClientId());

        if (LOG.isDebugEnabled()) {
            LOG.debug("getUsers(): Send " + IntrospectionUtil.toString(request));
        }

        // Get all users
        GetUsersResponse response = (GetUsersResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getUsers(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getUsers();
    }



    @Override
    public List<Application> getApplications() {
        // Construct request
        GetApplicationsRequest request = new GetApplicationsRequest();
        request.setClientId(ContextClient.getClientId());

        if (LOG.isDebugEnabled()) {
            LOG.debug("getApplications(): Send " + IntrospectionUtil.toString(request));
        }

        // Get all applications
        GetApplicationsResponse response = (GetApplicationsResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getApplications(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getApplications();
    }



    @Override
    public void login(String userId, String userPassword) {
        // Construct request
        LoginRequest request = new LoginRequest();
        request.setClientId(ContextClient.getClientId());
        request.setUserId(userId);
        request.setUserPassword(userPassword);

        // Change user id
        ContextClient.setUserId(userId);

        if (LOG.isDebugEnabled()) {
            LOG.debug("login(): Send " + IntrospectionUtil.toString(request));
        }

        // Login
        LoginResponse response = (LoginResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("login(): Receipt " + IntrospectionUtil.toString(response));
        }
    }



    @Override
    public void logout(boolean onlyOneTry) {
        // Construct request
        LogoutRequest request = new LogoutRequest();
        request.setClientId(ContextClient.getClientId());

        // Change user id
        ContextClient.setUserId(null);

        if (LOG.isDebugEnabled()) {
            LOG.debug("logout(): Send " + IntrospectionUtil.toString(request));
        }

        // Logout
        LogoutResponse response = (LogoutResponse) getRequester().request(request, onlyOneTry);

        if (LOG.isDebugEnabled()) {
            LOG.debug("logout(): Receipt " + IntrospectionUtil.toString(response));
        }
    }
}
