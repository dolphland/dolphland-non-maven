package com.dolphland.client.service.spec;

import java.util.List;


/**
 * Business for web
 * 
 * @author JayJay
 * 
 */
public interface SVWeb {
    
    /**
     * Get server response from socket id and browser request 
     * 
     * @param socketId Socket id
     * @param browserRequest Browser request
     * 
     * @return Server response
     */
    public List<Integer> getContent(String socketId, List<Integer> browserRequest);
}
