package com.dolphland.client.service.web;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.core.ws.data.RemoteError;

/**
 * Requester for web service
 * 
 * @author JayJay
 * 
 */
public class WebServiceRequester {

    private static final Logger LOG = Logger.getLogger(WebServiceRequester.class);

    private static final MessageFormater FMT = MessageFormater.getFormater(WebServiceRequester.class);

    // Default request max tries
    private final static int DEFAULT_REQUEST_MAX_TRIES = 10;

    // Default request try delay (in milliseconds)
    private final static int DEFAULT_REQUEST_TRY_DELAY = 1000;

    // Default request timeout (in milliseconds)
    private final static int DEFAULT_REQUEST_TIMEOUT = 0;

    // Request max tries
    private int requestMaxTries = DEFAULT_REQUEST_MAX_TRIES;

    // Request try delay (in milliseconds)
    private int requestTryDelay = DEFAULT_REQUEST_TRY_DELAY;

    // Request timeout (in milliseconds)
    private int requestTimeout = DEFAULT_REQUEST_TIMEOUT;

    // Web service invoker
    private WebServiceInvoker wsInvoker;



    /**
     * Constructor with Web service invoker
     * 
     * @param wsInvoker
     *            Web service invoker
     */
    public WebServiceRequester(WebServiceInvoker wsInvoker) {
        AssertUtil.notNull(wsInvoker, "wsInvoker is null !");
        this.wsInvoker = wsInvoker;
    }



    /**
     * Get request's max tries
     * 
     * @return the requestMaxTries
     */
    public int getRequestMaxTries() {
        return requestMaxTries;
    }



    /**
     * Set request's max tries
     * 
     * @param requestMaxTries
     *            the requestMaxTries to set
     */
    public void setRequestMaxTries(int requestMaxTries) {
        this.requestMaxTries = requestMaxTries;
    }



    /**
     * Get request's try delay
     * 
     * @return the requestTryDelay
     */
    public int getRequestTryDelay() {
        return requestTryDelay;
    }



    /**
     * Set request's try delay
     * 
     * @param requestTryDelay
     *            the requestTryDelay to set
     */
    public void setRequestTryDelay(int requestTryDelay) {
        this.requestTryDelay = requestTryDelay;
    }



    /**
     * Get request's timeout
     * 
     * @return the requestTimeout
     */
    public int getRequestTimeout() {
        return requestTimeout;
    }



    /**
     * Set request's timeout
     * 
     * @param requestTimeout
     *            the requestTimeout to set
     */
    public void setRequestTimeout(int requestTimeout) {
        this.requestTimeout = requestTimeout;
    }



    /**
     * Create exception from an error
     * 
     * @param error
     *            Error
     * 
     * @return Exception created
     */
    private AppBusinessException createException(RemoteError error) {
        if (error == null) {
            return createException(RemoteError.ERR_DOLPHLAND_000_UNEXPECTED_ERROR);
        }
        String message = null;
        switch (error) {
            case ERR_DOLPHLAND_000_UNEXPECTED_ERROR :
                message = FMT.format("WebServiceRequester.ERR_DOLPHLAND_000_UNEXPECTED_ERROR");
                break;

            case ERR_DOLPHLAND_001_NOT_EXISTED_USER :
                message = FMT.format("WebServiceRequester.ERR_DOLPHLAND_001_NOT_EXISTED_USER");
                break;

            case ERR_DOLPHLAND_002_BAD_USER_PASSWORD :
                message = FMT.format("WebServiceRequester.ERR_DOLPHLAND_002_BAD_USER_PASSWORD");
                break;

            case ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY :
                message = FMT.format("WebServiceRequester.ERR_PARTY_001_ACTION_FORBIDDEN_FINISHED_PARTY");
                break;

            case ERR_CHESS_001_USER_ALREADY_PLAYER :
                message = FMT.format("WebServiceRequester.ERR_CHESS_001_USER_ALREADY_PLAYER");
                break;

            case ERR_CHESS_002_PLAYERS_NO_CHANGE :
                message = FMT.format("WebServiceRequester.ERR_CHESS_002_PLAYERS_NO_CHANGE");
                break;

            case ERR_CHESS_003_PLAYER_ALREADY_TAKEN :
                message = FMT.format("WebServiceRequester.ERR_CHESS_003_PLAYER_ALREADY_TAKEN");
                break;
                
            case ERR_CHESS_004_MOVEMENT_FORBIDDEN :
                message = FMT.format("WebServiceRequester.ERR_CHESS_004_MOVEMENT_FORBIDDEN");
                break;
                
            default :
                break;
        }
        return new AppBusinessException(message);
    }



    /**
     * Request web service from the request then return the response
     * 
     * @param request
     *            Request
     * 
     * @return Response
     */
    public Object request(Object request) {
        return request(request, false);
    }



    /**
     * Request web service from the request then return the response
     * 
     * @param request
     *            Request
     * @param onlyOneTry
     *            If true, only one try will be attempted 
     * 
     * @return Response
     */
    public Object request(Object request, boolean onlyOneTry) {
        boolean failed = true;
        int nbAttempts = 0;
        Object response = null;
        AppInfrastructureException lastException = null;
        while (failed && (nbAttempts < requestMaxTries)) {
            nbAttempts++;
            try {
                // Invoke web service and get response from request
                response = wsInvoker.invoke(request, requestTimeout);

                // Request successfully executed
                failed = false;
            } catch (TimeoutException toe) {
                LOG.error("request(): Could not send request, no response from server !", toe);
                // rethrow an exception with a customized message
                lastException = new AppInfrastructureException("Could not send request, no response from server !", toe);
                if(onlyOneTry){
                    break;
                }
            } catch (CancellationException exc) {
                LOG.error("request(): Request has been cancelled", exc);
                lastException = new AppInfrastructureException("Request has been cancelled", exc);
                // process has been cancelled, do no keep trying to request
                break;
            } catch (InterruptedException exc) {
                LOG.error("request(): Request has been interrupted", exc);
                lastException = new AppInfrastructureException("Request has been interrupted", exc);
                // process has been interrupted, do no keep trying to request
                break;
            } catch (ExecutionException exc) {
                LOG.error("request(): Request has been interrupted with the error " + exc.getMessage(), exc);
                lastException = new AppInfrastructureException("Connection has been interrupted with an error", exc);
                // process has been interrupted with an error, do no keep trying
                // to connect
                break;
            }
            if (failed) {
                // Try to request to server up to max request times do not wait
                // after the last attempt
                if (nbAttempts < requestMaxTries) {
                    LOG.error("Request attempt " + nbAttempts + "/" + requestMaxTries + " failed, next attempt in " + (requestTryDelay / 1000f) + " second(s)");
                    try {
                        Thread.sleep(requestTryDelay);
                    } catch (InterruptedException e) {
                        // Propagate thread interruption
                        throw new AppBusinessException("Interrupted exception", e);
                    }
                }
            }
        }
        if (failed) {
            throw lastException;
        }

        // Get error from response
        RemoteError error = null;
        try {
            error = (RemoteError) response.getClass().getMethod("getError", new Class[] {}).invoke(response, new Object[] {});
        } catch (Exception exc) {
            throw new AppInfrastructureException("Error when getting error from response", exc);
        }
        if (error != null) {
            throw createException(error);
        }
        return response;
    }
}
