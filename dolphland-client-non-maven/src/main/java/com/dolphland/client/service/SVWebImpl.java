package com.dolphland.client.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.service.spec.SVWeb;
import com.dolphland.client.util.introspection.IntrospectionUtil;
import com.dolphland.core.ws.data.GetWebContentRequest;
import com.dolphland.core.ws.data.GetWebContentResponse;

/**
 * Service implementation for web
 * 
 * @author JayJay
 * 
 */
class SVWebImpl extends AbstractService implements SVWeb {

    private static final Logger LOG = Logger.getLogger(SVWebImpl.class);



    @Override
    public List<Integer> getContent(String socketId, List<Integer> browserRequest) {
        // Construct request
        GetWebContentRequest request = new GetWebContentRequest();
        request.setClientId(ContextClient.getClientId());
        request.setSocketId(socketId);
        request.getBrowserRequest().addAll(browserRequest);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getContent(): Send " + IntrospectionUtil.toString(request));
        }

        // Get content
        GetWebContentResponse response = (GetWebContentResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getContent(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getServerResponse();
    }
}
