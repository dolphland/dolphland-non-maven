package com.dolphland.client.service;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.service.spec.SVMessage;
import com.dolphland.client.util.introspection.IntrospectionUtil;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.RemoteMessageType;
import com.dolphland.core.ws.data.SendMessageRequest;
import com.dolphland.core.ws.data.SendMessageResponse;
import com.dolphland.core.ws.data.User;

/**
 * Service implementation for message
 * 
 * @author JayJay
 * 
 */
class SVMessageImpl extends AbstractService implements SVMessage {

    private static final Logger LOG = Logger.getLogger(SVMessageImpl.class);



    @Override
    public void sendMessage(RemoteMessageType type, String content) {
        sendMessage(type, content, null, null);
    }



    @Override
    public void sendMessage(RemoteMessageType type, String content, User receiver) {
        sendMessage(type, content, receiver, null);
    }



    @Override
    public void sendMessage(RemoteMessageType type, String content, Party party) {
        sendMessage(type, content, null, party);
    }



    @Override
    public void sendMessage(RemoteMessageType type, String content, User receiver, Party party) {
        SendMessageRequest request = new SendMessageRequest();
        request.setClientId(ContextClient.getClientId());
        request.setType(type);
        request.setContent(content);
        request.setReceiverUserId(receiver == null ? null : receiver.getId());
        request.setPartyId(party == null ? null : party.getId());

        if (LOG.isDebugEnabled()) {
            LOG.debug("sendMessage(): Send " + IntrospectionUtil.toString(request));
        }

        // Send message
        SendMessageResponse response = (SendMessageResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("sendMessage(): Receipt " + IntrospectionUtil.toString(response));
        }
    }
}
