package com.dolphland.client.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.event.EventPartyChanged;
import com.dolphland.client.service.spec.SVParty;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.introspection.IntrospectionUtil;
import com.dolphland.core.ws.data.AbortPartyRequest;
import com.dolphland.core.ws.data.AbortPartyResponse;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.CreatePartyRequest;
import com.dolphland.core.ws.data.CreatePartyResponse;
import com.dolphland.core.ws.data.GetPartiesRequest;
import com.dolphland.core.ws.data.GetPartiesResponse;
import com.dolphland.core.ws.data.JoinPartyRequest;
import com.dolphland.core.ws.data.JoinPartyResponse;
import com.dolphland.core.ws.data.LeavePartyRequest;
import com.dolphland.core.ws.data.LeavePartyResponse;
import com.dolphland.core.ws.data.Party;

/**
 * Service implementation for party
 * 
 * @author JayJay
 * 
 */
class SVPartyImpl extends AbstractService implements SVParty {

    private static final Logger LOG = Logger.getLogger(SVPartyImpl.class);



    @Override
    public List<Party> getParties() {
        // Construct request
        GetPartiesRequest request = new GetPartiesRequest();
        request.setClientId(ContextClient.getClientId());

        if (LOG.isDebugEnabled()) {
            LOG.debug("getParties(): Send " + IntrospectionUtil.toString(request));
        }

        // Get all parties
        GetPartiesResponse response = (GetPartiesResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getParties(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getParties();
    }



    @Override
    public Party createParty(ApplicationId applicationId, boolean ranked) {
        // Construct request
        CreatePartyRequest request = new CreatePartyRequest();
        request.setClientId(ContextClient.getClientId());
        request.setApplicationId(applicationId);
        request.setRanked(ranked);

        if (LOG.isDebugEnabled()) {
            LOG.debug("createParty(): Send " + IntrospectionUtil.toString(request));
        }

        // Create party
        CreatePartyResponse response = (CreatePartyResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("createParty(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getParty();
    }



    @Override
    public Party joinParty(String partyId) {
        // Construct request
        JoinPartyRequest request = new JoinPartyRequest();
        request.setClientId(ContextClient.getClientId());
        request.setPartyId(partyId);

        if (LOG.isDebugEnabled()) {
            LOG.debug("joinParty(): Send " + IntrospectionUtil.toString(request));
        }

        // Create party
        JoinPartyResponse response = (JoinPartyResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("joinParty(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getParty();
    }



    @Override
    public Party leaveParty(String partyId) {
        // Construct request
        LeavePartyRequest request = new LeavePartyRequest();
        request.setClientId(ContextClient.getClientId());
        request.setPartyId(partyId);

        if (LOG.isDebugEnabled()) {
            LOG.debug("leaveParty(): Send " + IntrospectionUtil.toString(request));
        }

        // Create party
        LeavePartyResponse response = (LeavePartyResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("leaveParty(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getParty();
    }



    @Override
    public Party abortParty(String partyId) {
        // Construct request
        AbortPartyRequest request = new AbortPartyRequest();
        request.setClientId(ContextClient.getClientId());
        request.setPartyId(partyId);

        if (LOG.isDebugEnabled()) {
            LOG.debug("abortParty(): Send " + IntrospectionUtil.toString(request));
        }

        // Create party
        AbortPartyResponse response = (AbortPartyResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("abortParty(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getParty();
    }



    @Override
    public void synchronizeParty(String partyId) {
        // Get party
        Party party = ContextClient.getParty(partyId);

        // If party doesn't exist
        if (party == null) {
            throw new AppBusinessException("Party doesn't exist !");
        }

        // Synchronize party
        switch (party.getApplicationId()) {
            case CHESS :
                ChessBoard board = SVFactory.createSVChess().getBoard(partyId);
                EDT.postEvent(new EventPartyChanged<ChessBoard>(party, board));
                break;

            default :
                throw new AppBusinessException("Party's synchronisation not implemented !");
        }
    }
}
