package com.dolphland.client.service.spec;

import com.dolphland.core.ws.data.RemoteMessageType;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.User;

/**
 * Business for message
 * 
 * @author JayJay
 * 
 */
public interface SVMessage {



    /**
     * Send a message from its type and content
     * 
     * @param type
     *            Type
     * @param content
     *            Content
     */
	public void sendMessage(RemoteMessageType type, String content);



    /**
     * Send a message from its type, content and receiver
     * 
     * @param type
     *            Type
     * @param content
     *            Content
     * @param receiver
     *            Receiver 
     */
	public void sendMessage(RemoteMessageType type, String content, User receiver);



    /**
     * Send a message from its type, content and party
     * 
     * @param type
     *            Type
     * @param content
     *            Content
     * @param party
     *            Party
     */
	public void sendMessage(RemoteMessageType type, String content, Party party);



    /**
     * Send a message from its type, content, receiver and party
     * 
     * @param type
     *            Type
     * @param content
     *            Content
     * @param receiver
     *            Receiver
     * @param party
     *            Party
     */
	public void sendMessage(RemoteMessageType type, String content, User receiver, Party party);
}
