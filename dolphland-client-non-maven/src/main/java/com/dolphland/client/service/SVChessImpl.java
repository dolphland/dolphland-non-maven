package com.dolphland.client.service;

import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.service.spec.SVChess;
import com.dolphland.client.util.introspection.IntrospectionUtil;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;
import com.dolphland.core.ws.data.ChessMovement;
import com.dolphland.core.ws.data.ChessPiece;
import com.dolphland.core.ws.data.ChessPosition;
import com.dolphland.core.ws.data.GetChessBoardRequest;
import com.dolphland.core.ws.data.GetChessBoardResponse;
import com.dolphland.core.ws.data.GetChessMovementsRequest;
import com.dolphland.core.ws.data.GetChessMovementsResponse;
import com.dolphland.core.ws.data.MoveChessPieceRequest;
import com.dolphland.core.ws.data.MoveChessPieceResponse;
import com.dolphland.core.ws.data.SetChessPlayerRequest;
import com.dolphland.core.ws.data.SetChessPlayerResponse;

/**
 * Service implementation for chess
 * 
 * @author JayJay
 * 
 */
class SVChessImpl extends AbstractService implements SVChess {

    private static final Logger LOG = Logger.getLogger(SVChessImpl.class);



    @Override
    public ChessBoard getBoard(String partyId) {
        // Construct request
        GetChessBoardRequest request = new GetChessBoardRequest();
        request.setClientId(ContextClient.getClientId());
        request.setPartyId(partyId);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getBoard(): Send " + IntrospectionUtil.toString(request));
        }

        // Get board
        GetChessBoardResponse response = (GetChessBoardResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getBoard(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getBoard();
    }



    @Override
    public ChessBoard setPlayer(String partyId, ChessColor color) {
        // Construct request
        SetChessPlayerRequest request = new SetChessPlayerRequest();
        request.setClientId(ContextClient.getClientId());
        request.setPartyId(partyId);
        request.setColor(color);

        if (LOG.isDebugEnabled()) {
            LOG.debug("setPlayer(): Send " + IntrospectionUtil.toString(request));
        }

        // Get board
        SetChessPlayerResponse response = (SetChessPlayerResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("setPlayer(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getBoard();
    }



    @Override
    public List<ChessMovement> getMovements(String partyId, ChessPiece piece) {
        // Construct request
        GetChessMovementsRequest request = new GetChessMovementsRequest();
        request.setClientId(ContextClient.getClientId());
        request.setPartyId(partyId);
        request.setPiece(piece);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getMovements(): Send " + IntrospectionUtil.toString(request));
        }

        // Get movements
        GetChessMovementsResponse response = (GetChessMovementsResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("getMovements(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getMovements();
    }



    @Override
    public ChessBoard movePiece(String partyId, ChessPiece piece, ChessPosition position) {
        // Construct request
        MoveChessPieceRequest request = new MoveChessPieceRequest();
        request.setClientId(ContextClient.getClientId());
        request.setPartyId(partyId);
        request.setPiece(piece);
        request.setPosition(position);

        if (LOG.isDebugEnabled()) {
            LOG.debug("movePiece(): Send " + IntrospectionUtil.toString(request));
        }

        // Get board
        MoveChessPieceResponse response = (MoveChessPieceResponse) getRequester().request(request);

        if (LOG.isDebugEnabled()) {
            LOG.debug("movePiece(): Receipt " + IntrospectionUtil.toString(response));
        }

        return response.getBoard();
    }
}
