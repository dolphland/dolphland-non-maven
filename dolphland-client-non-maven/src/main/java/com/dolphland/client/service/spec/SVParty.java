package com.dolphland.client.service.spec;

import java.util.List;

import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;

/**
 * Business for party
 * 
 * @author JayJay
 * 
 */
public interface SVParty {

    /**
     * Get parties for current user
     * 
     * @return Get parties for current user
     */
    public List<Party> getParties();



    /**
     * Create a party for an application
     * The party is ranked if ranked indicator is true
     * 
     * @param applicationId
     *            Application id
     * @param ranked
     *            True if party is ranked
     * 
     * @return Party created
     */
    public Party createParty(ApplicationId applicationId, boolean ranked);



    /**
     * Join the party
     * 
     * @param partyId
     *            Party id
     * 
     * @return party joined
     */
    public Party joinParty(String partyId);



    /**
     * Leave the party
     * 
     * @param partyId
     *            Party id
     * 
     * @return party leaved
     */
    public Party leaveParty(String partyId);



    /**
     * Abort the party
     * 
     * @param partyId
     *            Party id
     * 
     * @return party aborted
     */
    public Party abortParty(String partyId);



    /**
     * Synchronize the party
     * 
     * @param partyId
     *            Party id
     */
    public void synchronizeParty(String partyId);
}
