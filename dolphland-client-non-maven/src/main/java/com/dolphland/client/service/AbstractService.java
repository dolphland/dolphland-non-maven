package com.dolphland.client.service;

import com.dolphland.client.service.web.WebServiceRequester;
import com.dolphland.client.util.conf.FwkServiceLocator;

/**
 * Abstract service
 * 
 * @author JayJay
 * 
 */
public class AbstractService {

    /**
     * Get requester for web service
     * 
     * @return Requester for web service
     */
    protected WebServiceRequester getRequester() {
        return (WebServiceRequester) FwkServiceLocator.getInstance().getRequester();
    }
}
