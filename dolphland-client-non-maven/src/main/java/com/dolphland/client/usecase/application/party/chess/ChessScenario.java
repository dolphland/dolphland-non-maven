package com.dolphland.client.usecase.application.party.chess;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingUtilities;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.service.SVFactory;
import com.dolphland.client.usecase.application.party.AbstractPartyScenario;
import com.dolphland.client.usecase.application.party.PartyRenderer;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.transaction.MVCTx;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;
import com.dolphland.core.ws.data.ChessMovement;
import com.dolphland.core.ws.data.ChessPiece;
import com.dolphland.core.ws.data.ChessPlayer;
import com.dolphland.core.ws.data.ChessPlayerState;

/**
 * Scenario for chess
 * 
 * @author JayJay
 * 
 */
public class ChessScenario extends AbstractPartyScenario<ChessBoard> {

    private static final MessageFormater FMT = MessageFormater.getFormater(ChessScenario.class);




    /**
     * Constructor with application id and party id
     * 
     * @param applicationId
     *            Application id
     * @param partyId
     *            Party id
     */
    public ChessScenario(ApplicationId applicationId, String partyId) {
        super(applicationId, partyId);
    }



    @Override
    protected PartyRenderer<ChessBoard> createPartyRenderer() {
        ChessRenderer2D renderer = new ChessRenderer2D();
        renderer.setActionSelectPlayer(new ActionSelectPlayer());
        renderer.setActionSelectSquare(new ActionSelectSquare());
        return renderer;
    }



    @Override
    protected ChessRenderer2D getPartyRenderer() {
        return (ChessRenderer2D) super.getPartyRenderer();
    }

    /**
     * Action when selecting a player
     * 
     * @author JayJay
     * 
     */
    private class ActionSelectPlayer extends UIAction<ChessActionEvt, Object> {

        @Override
        protected Object doExecute(ChessActionEvt param) {
            if (ChessHelper.getPlayer(param.getBoard(), ChessColor.WHITE).getUserId() != null &&
                ChessHelper.getPlayer(param.getBoard(), ChessColor.BLACK).getUserId() != null &&
                ContextClient.getUserId().equals(ChessHelper.getPlayer(param.getBoard(), param.getColorPlayer()).getUserId())) {
                // If current user selects its own player whereas all players
                // are fixed, the user decides to abort the party
                doAbortParty();
            } else if (ChessHelper.getPlayer(param.getBoard(), param.getColorPlayer()).getUserId() != null &&
                !ContextClient.getUserId().equals(ChessHelper.getPlayer(param.getBoard(), param.getColorPlayer()).getUserId())) {
                // If current user doesn't select its own player, it's a view's
                // modification
                SwingUtilities.invokeLater(new Runnable() {

                    @Override
                    public void run() {
                        getWorkPane().showInfoDialog("Changement de vue...");
                    }
                });
            } else {
                // Else, the player become the current user's player
                new TransactionBecomePlayer().execute(param);
            }
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
            // Update is made with event receiving
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }
    }

    /**
     * Action when selecting a square
     * 
     * @author JayJay
     * 
     */
    private class ActionSelectSquare extends UIAction<ChessActionEvt, Object> {

        // Current piece to move
        private ChessPiece currentPieceToMove;

        @Override
        protected boolean shouldExecute(ChessActionEvt param) {
            // If all players aren't fixed, no move is authorized
            if (ChessHelper.getPlayer(param.getBoard(), ChessColor.WHITE).getUserId() == null ||
                ChessHelper.getPlayer(param.getBoard(), ChessColor.BLACK).getUserId() == null) {
                return false;
            }

            // If current user isn't current player, no move is authorized
            if (!ContextClient.getUserId().equals(ChessHelper.getPlayer(param.getBoard(), param.getBoard().getCurrentColor()).getUserId())) {
                return false;
            }

            // If one of player is in state MAT or PAT, no move is authorized
            for (ChessPlayer player : param.getBoard().getPlayers()) {
                if (ChessPlayerState.MAT == player.getState() || ChessPlayerState.PAT == player.getState()) {
                    return false;
                }
            }
            
            // If no piece to move or to get movements
            if (currentPieceToMove == null && param.getPiece() == null) {
                return false;
            }

            // If piece to get movement is not the current color
            if (currentPieceToMove == null && !param.getPiece().getColor().equals(param.getBoard().getCurrentColor())) {
                return false;
            }

            return super.shouldExecute(param) && isEnabled(param);
        }



        @Override
        protected Object doExecute(ChessActionEvt param) {

            if (param.getPiece() != null && param.getPiece().getColor().equals(param.getBoard().getCurrentColor())) {
                // Get movements from piece to move
                currentPieceToMove = param.getPiece();
                new TransactionGetMovements().execute(param);
            }
            else {
                // Move a piece
                param.setPiece(currentPieceToMove);
                currentPieceToMove = null;
                new TransactionMovePiece().execute(param);
            }
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
            // Update is made with event receiving
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }
    }

    /**
     * Transaction to become a player
     * 
     * @author JayJay
     * 
     */
    private class TransactionBecomePlayer extends MVCTx<ChessActionEvt, Object> {

        @Override
        protected void preExecute(ChessActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("ChessScenario.RS_WAITING_BECOME_PLAYER", ChessHelper.toString(param.getColorPlayer())));
        }



        @Override
        protected Object doExecute(ChessActionEvt param) {
            SVFactory.createSVChess().setPlayer(getParty().getId(), param.getColorPlayer());
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
            // Update is made with event receiving
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Transaction to get movements for a piece
     * 
     * @author JayJay
     * 
     */
    private class TransactionGetMovements extends MVCTx<ChessActionEvt, List<ChessMovement>> {

        @Override
        protected void preExecute(ChessActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("ChessScenario.RS_WAITING_GET_MOVEMENTS", ChessHelper.toString(param.getPiece())));
        }



        @Override
        protected List<ChessMovement> doExecute(ChessActionEvt param) {
            return SVFactory.createSVChess().getMovements(getParty().getId(), param.getPiece());
        }



        @Override
        protected void updateViewSuccess(List<ChessMovement> data) {
            getPartyRenderer().blinkMovements(data);
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Transaction to move a piece
     * 
     * @author JayJay
     * 
     */
    private class TransactionMovePiece extends MVCTx<ChessActionEvt, Object> {

        @Override
        protected void preExecute(ChessActionEvt param) {
            // Clear movement
            getPartyRenderer().blinkMovements(new ArrayList<ChessMovement>());

            getWorkPane().showInfoMessage(FMT.format("ChessScenario.RS_WAITING_MOVE_PIECE", ChessHelper.toString(param.getPiece())));
        }



        @Override
        protected Object doExecute(ChessActionEvt param) {
            SVFactory.createSVChess().movePiece(getParty().getId(), param.getPiece(), param.getPosition());
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
            // Update is made with event receiving
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }
}
