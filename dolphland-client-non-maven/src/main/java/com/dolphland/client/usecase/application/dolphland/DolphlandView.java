package com.dolphland.client.usecase.application.dolphland;

import info.clearthought.layout.TableLayout;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.dolphland.client.usecase.application.ApplicationComponent;
import com.dolphland.client.usecase.application.party.PartyComponent;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.application.components.FwkView;
import com.dolphland.client.util.layout.TableLayoutBuilder;

/**
 * View for dolphland
 * 
 * @author JayJay
 * 
 */
public class DolphlandView extends FwkView<DolphlandActionEvt> {

    // UI components
    private FwkPanel panelBackground;
    private FwkPanel panelApplications;

    // Applications
    private List<ApplicationComponent> applications;

    // Parties
    private List<PartyComponent> parties;

    // Selected application
    private ApplicationComponent selectedApplication;

    // Selected party
    private PartyComponent selectedParty;

    // Application's mouse listener
    private ApplicationMouseListener applicationMouseListener;

    // Party's mouse listener
    private PartyMouseListener partyMouseListener;



    /**
     * Default constructor
     */
    public DolphlandView() {
        applications = new ArrayList<ApplicationComponent>();
        parties = new ArrayList<PartyComponent>();

        // Application's mouse listener
        applicationMouseListener = new ApplicationMouseListener();

        // Party's mouse listener
        partyMouseListener = new PartyMouseListener();

        // Initialize view
        initialize();

        // Update view when view resized
        addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                updateView();
            }
        });
    }



    private void initialize() {
        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        setLayout(layoutBuilder.get());

        add(getPanelBackground(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .width(TableLayout.FILL)
            .height(TableLayout.FILL)
            .get()));
    }



    private FwkPanel getPanelBackground() {
        if (panelBackground == null) {
            panelBackground = new FwkPanel();

            TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

            panelBackground.setLayout(layoutBuilder.get());

            panelBackground.add(getPanelApplications(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .leftInsets(DEFAULT_GAP)
                .width(TableLayout.FILL)
                .rightInsets(DEFAULT_GAP)
                .topInsets(DEFAULT_GAP)
                .height(TableLayout.FILL)
                .bottomInsets(DEFAULT_GAP)
                .get()));
        }

        return panelBackground;
    }



    private FwkPanel getPanelApplications() {
        if (panelApplications == null) {
            panelApplications = new FwkPanel();
            panelApplications.setOpaque(false);
        }
        return panelApplications;
    }



    private List<PartyComponent> getParties(ApplicationComponent application) {
        List<PartyComponent> out = new ArrayList<PartyComponent>();
        for (PartyComponent party : parties) {
            if (party.getApplicationId().equals(application.getApplicationId())) {
                out.add(party);
            }
        }
        return out;
    }



    private void updateView() {

        // Set applications
        getPanelApplications().removeAll();

        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        getPanelApplications().setLayout(layoutBuilder.get());

        int row = 0;
        int col = 0;
        int totalWidth = 0;

        for (ApplicationComponent application : applications) {

            // Set application's mouse listener
            application.removeMouseListener(applicationMouseListener);
            application.addMouseListener(applicationMouseListener);

            if (totalWidth + DEFAULT_GAP + application.getWidth() > getPanelApplications().getWidth()) {
                row++;
                col = 0;
                totalWidth = 0;
            }

            // Add application
            getPanelApplications().add(application, layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .row(row)
                .column(col)
                .height(application.getPreferredSize().getHeight())
                .leftInsets(DEFAULT_GAP)
                .width(application.getPreferredSize().getWidth())
                .get()));

            // Get application's parties
            List<PartyComponent> applicationParties = getParties(application);

            // Set counter in application from parties
            application.setCounter(applicationParties.size());

            if (application.equals(selectedApplication)) {
                // If application is selected
                application.setSelected(true);

                // Add parties components for each selected application's party
                int rowParty = 0;
                for (PartyComponent party : parties) {
                    if (!party.getApplicationId().equals(application.getApplicationId())) {
                        continue;
                    }

                    // Set party's mouse listener
                    party.removeMouseListener(partyMouseListener);
                    party.addMouseListener(partyMouseListener);

                    getPanelApplications().add(party, layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                        .row(row + (rowParty++) + 1)
                        .column(col)
                        .height(application.getPreferredSize().getHeight())
                        .get()));

                    // If selected party, set selected
                    if (party.equals(selectedParty)) {
                        party.setSelected(true);
                    }
                    // Else, set not selected
                    else {
                        party.setSelected(false);
                    }
                }

            } else {
                // If application is not selected
                application.setSelected(false);
            }

            totalWidth += DEFAULT_GAP + application.getWidth();
            col++;
        }

        validate();
        repaint();
    }



    @Override
    public DolphlandActionEvt createUIActionEvt() {
        DolphlandActionEvt evt = new DolphlandActionEvt();
        return evt;
    }



    /**
     * Set applications
     * 
     * @param applications
     *            Applications
     */
    public void setApplications(List<ApplicationComponent> applications) {
        this.applications = applications;
        updateView();
    }



    /**
     * Set parties
     * 
     * @param parties
     *            Parties
     */
    public void setParties(List<PartyComponent> parties) {
        this.parties = parties;
        updateView();
    }



    /**
     * Set background image
     * 
     * @param image
     *            Image
     */
    public void setBackgroundImage(BufferedImage image) {
        getPanelBackground().setImage(image);
    }

    /**
     * Application mouse listener
     * 
     * @author jayjay
     * 
     */
    private class ApplicationMouseListener extends MouseAdapter {

        @Override
        public void mouseEntered(MouseEvent e) {
            selectedApplication = (ApplicationComponent) e.getSource();
            updateView();
        }



        @Override
        public void mouseExited(MouseEvent e) {
            // Do nothing if application contains parties
            if (!getParties(selectedApplication).isEmpty()) {
                return;
            }
            selectedApplication = null;
            updateView();
        }
    }

    /**
     * Party mouse listener
     * 
     * @author jayjay
     * 
     */
    private class PartyMouseListener extends MouseAdapter {

        @Override
        public void mouseEntered(MouseEvent e) {
            selectedParty = (PartyComponent) e.getSource();
            updateView();
        }



        @Override
        public void mouseExited(MouseEvent e) {
            selectedParty = null;
            updateView();
        }
    }
}
