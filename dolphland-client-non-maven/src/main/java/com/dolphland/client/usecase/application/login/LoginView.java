package com.dolphland.client.usecase.application.login;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.dolphland.client.AppConstants;
import com.dolphland.client.context.ContextClient;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.application.components.FwkLabel;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.application.components.FwkView;
import com.dolphland.client.util.image.ImageLabel;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.input.PasswordTextField;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.user.UserComponent;
import com.dolphland.core.ws.data.User;

/**
 * View for login
 * 
 * @author JayJay
 * 
 */
public class LoginView extends FwkView<LoginActionEvt> {

    // UI components
    private FwkPanel panelBackground;
    private FwkPanel panelUsers;
    private FwkLabel lblUser;
    private PasswordTextField jtfPassword;
    private ImageLabel btnLogin;
    private Map<String, UserComponent> panelsByUser;

    // Selected user id
    private String selectedUserId = null;



    /**
     * Default constructor
     */
    public LoginView() {
        panelsByUser = new TreeMap<String, UserComponent>();
        addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                updatePanelUsers();
            }
        });
        initialize();
    }



    private void initialize() {
        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        setLayout(layoutBuilder.get());

        add(getPanelBackground(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .height(TableLayout.FILL)
            .width(TableLayout.FILL)
            .get()));
    }



    private FwkPanel createPanelImage() {
        FwkPanel out = new FwkPanel();
        out.setOpaque(false);
        out.setImage(AppConstants.PATH_IMAGE_BACKGROUND_LOGIN);
        return out;
    }



    private FwkPanel getPanelBackground() {
        if (panelBackground == null) {
            panelBackground = new FwkPanel();
            panelBackground.setOpaque(false);

            TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

            panelBackground.setLayout(layoutBuilder.get());

            panelBackground.add(createPanelImage(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .row(0)
                .width(TableLayout.FILL)
                .topInsets(DEFAULT_GAP)
                .height(TableLayout.FILL)
                .get()));

            panelBackground.add(getPanelUsers(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .row(1)
                .topInsets(DEFAULT_GAP)
                .height(TableLayout.PREFERRED)
                .get()));

            panelBackground.add(createPanelLogin(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .row(2)
                .topInsets(DEFAULT_GAP)
                .height(TableLayout.PREFERRED)
                .get()));

            panelBackground.add(createPanelActions(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .row(3)
                .topInsets(DEFAULT_GAP)
                .height(TableLayout.PREFERRED)
                .bottomInsets(DEFAULT_GAP)
                .get()));
        }

        return panelBackground;
    }



    private FwkPanel getPanelUsers() {
        if (panelUsers == null) {
            panelUsers = new FwkPanel();
            panelUsers.setOpaque(false);
        }
        return panelUsers;
    }



    private FwkPanel createPanelLogin() {
        FwkPanel out = new FwkPanel();
        out.setOpaque(false);

        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        out.setLayout(layoutBuilder.get());

        out.add(getLblUser(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .row(0)
            .height(TableLayout.PREFERRED)
            .leftInsets(1f / 2f)
            .width(TableLayout.PREFERRED)
            .rightInsets(1f / 2f)
            .get()));

        out.add(getJtfPassword(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .row(1)
            .topInsets(DEFAULT_GAP)
            .height(TableLayout.PREFERRED)
            .get()));

        return out;
    }



    private FwkPanel createPanelActions() {
        FwkPanel out = new FwkPanel();
        out.setOpaque(false);

        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        out.setLayout(layoutBuilder.get());

        out.add(getBtnLogin(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .leftInsets(1f / 2f)
            .width(TableLayout.PREFERRED)
            .rightInsets(1f / 2f)
            .height(TableLayout.PREFERRED)
            .get()));

        return out;
    }



    private FwkLabel getLblUser() {
        if (lblUser == null) {
            lblUser = new FwkLabel("Empty");
            lblUser.setForeground(Color.WHITE);
            lblUser.setHorizontalAlignment(SwingConstants.CENTER);
            lblUser.setVisible(false);
        }
        return lblUser;
    }



    private PasswordTextField getJtfPassword() {
        if (jtfPassword == null) {
            jtfPassword = new PasswordTextField(AppConstants.WIDTH_PASSWORD);
            jtfPassword.setColumns(jtfPassword.getNbChar());
            jtfPassword.setVisible(false);
        }
        return jtfPassword;
    }



    private ImageLabel getBtnLogin() {
        if (btnLogin == null) {
            btnLogin = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_LOGIN), ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_LOGIN_HOVERED));
        }
        return btnLogin;
    }



    /**
     * Update panel users
     */
    private void updatePanelUsers() {
        // Reinit panel users
        getPanelUsers().removeAll();

        if (!panelsByUser.isEmpty()) {
            TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();
            getPanelUsers().setLayout(layoutBuilder.get());

            int row = 0;
            int col = 0;
            int colCount = 0;
            int rowCount = 0;

            // Create an avatar for each user
            for (String userId : panelsByUser.keySet()) {
                UserComponent panelUser = panelsByUser.get(userId);
                TableLayoutConstraintsBuilder avatarConstraintsBuilder = createTableLayoutConstraintsBuilder();

                // If first user, we determinate how many rows and columns
                if (row == 0 && col == 0) {
                    colCount = Math.max(1, getWidth() / (int) (panelUser.getPreferredSize().getWidth() + (2 * DEFAULT_GAP)));
                    if (colCount > 1 && (panelsByUser.size() / colCount >= 2 || (panelsByUser.size() / colCount >= 1 && panelsByUser.size() % colCount > 0))) {
                        colCount--;
                    }
                    rowCount = panelsByUser.size() / colCount;
                    if (rowCount >= 1 && panelsByUser.size() % colCount > 0) {
                        rowCount++;
                    }
                }

                // Si changement de ligne
                if (col > colCount) {
                    row++;
                    col = 0;
                }

                // Si premier avatar de la ligne
                if (col == 0) {
                    avatarConstraintsBuilder
                        .leftInsets(1f / 2f);
                }
                // Sinon
                else {
                    avatarConstraintsBuilder
                        .leftInsets(DEFAULT_GAP);
                }

                // Si dernier avatar de la ligne
                if (col == colCount) {
                    avatarConstraintsBuilder
                        .rightInsets(1f / 2f);
                }

                getPanelUsers().add(panelUser, layoutBuilder.addComponent(avatarConstraintsBuilder
                    .row(row)
                    .column(col)
                    .height(TableLayout.PREFERRED)
                    .width(TableLayout.PREFERRED)
                    .get()));

                col++;
            }
        }

        validate();

        setSelectedUser(ContextClient.getUser(selectedUserId));
    }



    @Override
    public LoginActionEvt createUIActionEvt() {
        LoginActionEvt evt = new LoginActionEvt();
        evt.setUserId(selectedUserId);
        evt.setUserPassword(getJtfPassword().stringValue());
        return evt;
    }



    /**
     * Set users
     * 
     * @param users
     *            Users
     */
    public void setUsers(List<User> users) {
        for (final User user : users) {
            // Create a panel user
            UserComponent panelUser = new UserComponent(user);

            // Add a listener when click on the panel user
            panelUser.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (user.getId().equals(selectedUserId)) {
                        setSelectedUser(null);
                    } else {
                        setSelectedUser(user);
                    }
                }
            });

            // Add panel for the user
            panelsByUser.put(user.getId(), panelUser);
        }

        // Update panel users
        updatePanelUsers();
    }



    /**
     * Get user component
     * 
     * @param user
     *            User
     * 
     * @return User component
     */
    public Component getUserComponent(User user) {
        if (user == null) {
            return null;
        }
        return panelsByUser.get(user.getId());
    }



    /**
     * Set action login
     * 
     * @param actionLogin
     *            Action login
     */
    public void setActionLogin(UIAction<LoginActionEvt, ?> actionLogin) {
        bind(actionLogin, getBtnLogin());
        bind(actionLogin, getJtfPassword());
    }



    /**
     * Initialize caret position
     */
    public void initCaretPosition() {
        if (selectedUserId != null) {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    getJtfPassword().requestFocus();
                }
            });
        }
    }



    /**
     * Show desktop's background
     */
    public void showDesktopBackground() {
        getPanelBackground().setOpaque(true);
        getPanelBackground().setImage(AppConstants.PATH_IMAGE_BACKGROUND_DESKTOP, true);
    }



    /**
     * Set selected user from a user
     * 
     * @param user
     *            User selected
     */
    public void setSelectedUser(User user) {
        this.selectedUserId = user == null ? null : user.getId();
        for (String userId : panelsByUser.keySet()) {
            panelsByUser.get(userId).setSelected(userId.equals(selectedUserId));
        }

        getLblUser().setVisible(user != null);
        getJtfPassword().setVisible(user != null);
        getBtnLogin().setVisible(user != null);

        if (selectedUserId != null) {
            getLblUser().setText(user.getFirstName() + " " + user.getLastName());
            getJtfPassword().setText(StrUtil.EMPTY_STRING);
        }

        initCaretPosition();
    }
}
