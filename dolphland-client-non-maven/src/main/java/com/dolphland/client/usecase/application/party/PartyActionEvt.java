package com.dolphland.client.usecase.application.party;

import com.dolphland.client.util.action.UIActionEvt;

/**
 * Action context for party
 * 
 * @author JayJay
 * 
 */
public class PartyActionEvt extends UIActionEvt {

    private String message;



    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }



    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
