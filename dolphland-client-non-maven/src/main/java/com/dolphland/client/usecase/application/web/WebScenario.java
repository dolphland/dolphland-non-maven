package com.dolphland.client.usecase.application.web;

import java.io.IOException;

import com.dolphland.client.usecase.application.AbstractApplicationScenario;
import com.dolphland.core.ws.data.ApplicationId;

/**
 * Scenario for web
 * 
 * @author JayJay
 * 
 */
public class WebScenario extends AbstractApplicationScenario {

    // View
    private WebView view;



    /**
     * Constructor with application's id
     * 
     * @param applicationId
     *            Application's id
     */
    public WebScenario(ApplicationId applicationId) {
        super(applicationId);
        view = new WebView();
    }



    @Override
    protected void doEnter(Object param) {
        getWorkPane().setView(view);

        try {
            Runtime.getRuntime().exec("C:\\Documents and Settings\\JayJay\\Local Settings\\Application Data\\Google\\Chrome\\Application\\chrome.exe http://www.google.fr/ --proxy-server=localhost:" + 2000);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
