package com.dolphland.client.usecase.desktop;

import java.awt.Component;

import com.dolphland.client.usecase.application.AbstractApplicationScenario;
import com.dolphland.client.usecase.application.ApplicationAdapter;
import com.dolphland.client.util.flip.FlipApplicationAdapter;

/**
 * {@link FlipApplicationAdapter} used by desktop
 * 
 * @author jeremy.scafi
 * 
 */
class DesktopFlipApplication extends FlipApplicationAdapter {

    // Instance of application's scenario
    private AbstractApplicationScenario scenario;

    // Application
    private ApplicationAdapter application;



    /**
     * Constructor with application id, component and scenario
     * 
     * @param applicationId
     *            Application's id
     * @param application
     *            Application
     * @param applicationComponent
     *            Application's component
     * @param applicationScenario
     *            Application's scenario
     */
    public DesktopFlipApplication(String applicationId, ApplicationAdapter application, Component applicationComponent, AbstractApplicationScenario applicationScenario) {
        super(applicationId, applicationComponent, applicationScenario);
        this.application = application;
        this.scenario = applicationScenario;
    }



    /**
     * @return the scenario
     */
    public AbstractApplicationScenario getScenario() {
        return scenario;
    }



    /**
     * @return the application
     */
    public ApplicationAdapter getApplication() {
        return application;
    }

}
