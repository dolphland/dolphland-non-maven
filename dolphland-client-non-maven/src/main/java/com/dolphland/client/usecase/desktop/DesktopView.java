package com.dolphland.client.usecase.desktop;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;

import com.dolphland.client.AppConstants;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.application.components.FwkView;
import com.dolphland.client.util.flip.FlipApplicationAdapter;
import com.dolphland.client.util.flip.FlipPanel;
import com.dolphland.client.util.image.ImageLabel;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.layout.TableLayoutBuilder;

/**
 * View for desktop
 * 
 * @author JayJay
 * 
 */
public class DesktopView extends FwkView<DesktopActionEvt> {

    private final static int HEADER_HEIGHT = 50;
    private final static Color HEADER_BACKGROUND = Color.BLACK;

    // UI components
    private ImageLabel flipButton;
    private ImageLabel logoutButton;
    private FwkPanel headerPanel;
    private FlipPanel desktopPanel;



    /**
     * Default constructor
     */
    public DesktopView() {
        initialize();
    }



    private void initialize() {
        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        setLayout(layoutBuilder.get());

        add(getHeaderPanel(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .row(0)
            .width(TableLayout.FILL)
            .height(HEADER_HEIGHT)
            .get()));

        add(getDesktopPanel(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .row(1)
            .height(TableLayout.FILL)
            .get()));
    }



    private FwkPanel getHeaderPanel() {
        if (headerPanel == null) {
            headerPanel = new FwkPanel();
            headerPanel.setBackground(HEADER_BACKGROUND);

            TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

            headerPanel.setLayout(layoutBuilder.get());

            headerPanel.add(getFlipButton(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .column(0)
                .topInsets(1f / 2f)
                .height(TableLayout.PREFERRED)
                .bottomInsets(1f / 2f)
                .leftInsets(1f / 2f)
                .width(TableLayout.PREFERRED)
                .get()));

            headerPanel.add(getLogoutButton(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .column(1)
                .leftInsets(1f / 2f)
                .width(TableLayout.PREFERRED)
                .rightInsets(DEFAULT_GAP)
                .get()));
        }
        return headerPanel;
    }



    private FlipPanel getDesktopPanel() {
        if (desktopPanel == null) {
            desktopPanel = new FlipPanel();
            desktopPanel.setImage(AppConstants.PATH_IMAGE_BACKGROUND_DESKTOP, true);
        }
        return desktopPanel;
    }



    private ImageLabel getLogoutButton() {
        if (logoutButton == null) {
            logoutButton = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_LOGOUT), ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_LOGOUT_HOVERED));
        }
        return logoutButton;
    }



    private ImageLabel getFlipButton() {
        if (flipButton == null) {
            flipButton = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_FLIP), ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_FLIP_HOVERED));
        }
        return flipButton;
    }



    @Override
    public DesktopActionEvt createUIActionEvt() {
        DesktopActionEvt evt = new DesktopActionEvt();
        evt.setApplications(getDesktopPanel().getApplications());
        evt.setMaximizedApplicationId(getDesktopPanel().getMaximizedApplicationId());
        return evt;
    }



    /**
     * Get applications count
     */
    public int getApplicationsCount() {
        return getDesktopPanel().getApplications().size();
    }



    /**
     * Add an application to desktop
     * 
     * @param application
     *            Application
     */
    public void addApplication(FlipApplicationAdapter application) {
        getDesktopPanel().addApplication(application);
    }



    /**
     * Remove an application from desktop
     * 
     * @param applicationId
     *            Application's id
     */
    public void removeApplication(String applicationId) {
        getDesktopPanel().removeApplication(applicationId);
    }



    /**
     * Set logout's action
     * 
     * @param logoutAction
     *            Flip's action
     */
    public void setLogoutAction(UIAction<DesktopActionEvt, ?> logoutAction) {
        bind(logoutAction, getLogoutButton());
    }



    /**
     * Set flip's action
     * 
     * @param flipAction
     *            Flip's action
     */
    public void setFlipAction(UIAction<DesktopActionEvt, ?> flipAction) {
        bind(flipAction, getFlipButton());
    }



    /**
     * Flip applications
     */
    public void flipApplications() {
        getDesktopPanel().flipApplications();
    }



    /**
     * Get logout component
     * 
     * @return Logout component
     */
    public Component getLogoutComponent() {
        return getLogoutButton();
    }
}
