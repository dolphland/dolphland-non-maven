package com.dolphland.client.usecase.application.party;

import javax.swing.JComponent;

import com.dolphland.client.util.user.UserComponent;
import com.dolphland.core.ws.data.Party;

/**
 * Interface to represent a party's renderer
 * 
 * @author JayJay
 * 
 *         P : type of the party's representation
 */
public interface PartyRenderer<P> {

    /**
     * Get party's component
     * 
     * @return Party's compoennt
     */
    public JComponent getPartyComponent();



    /**
     * Get user's component for a user's id
     * 
     * @param userId
     *            User's id
     * 
     * @return User's component
     */
    public UserComponent getUserComponent(String userId);



    /**
     * Set if animations are enabled or disabled
     * 
     * @param animationsEnabled
     *            True if animations are enabled
     */
    public void setAnimationsEnabled(boolean animationsEnabled);



    /**
     * Update the party's representation
     * 
     * @param party
     *            Party
     * @param partyRepresentation
     *            Party's representation
     */
    public void updateParty(Party party, P partyRepresentation);
}
