package com.dolphland.client.usecase.application.party;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.dolphland.client.AppConstants;
import com.dolphland.client.usecase.application.ApplicationActionEvt;
import com.dolphland.client.usecase.application.ApplicationAdapter;
import com.dolphland.client.util.animator.AnimatedComponent;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.image.ImageLabel;
import com.dolphland.client.util.image.ImagePanel;
import com.dolphland.client.util.image.ImagePosition;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;

/**
 * Party component
 * 
 * @author JayJay
 * 
 */
public class PartyComponent extends AnimatedComponent {

    private static final MessageFormater FMT = MessageFormater.getFormater(PartyComponent.class);

    // Selection's color
    private final static Color COLOR_SELECTION = Color.RED;

    // Application id
    private ApplicationId applicationId;
    
    // Image panel
    private ImagePanel imagePanel;
    
    // Party
    private Party party;



    /**
     * Constructor with application specified
     * 
     * @param application
     *            Application
     */
    public PartyComponent(final ApplicationAdapter application) {
        super(application.getParty().getId());
        this.applicationId = application.getApplicationId();
        setImage(application.getActionOpen().getIcon(), false);
        setTypeBorder(TYPE_BORDER_ROUNDED_BORDER);
        setSelectedEnabled(true);
        setSelectedBackground(COLOR_SELECTION);
        setToolTipText(application.getActionOpen().getDescription());
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                ApplicationActionEvt evt = new ApplicationActionEvt();
                evt.setLinkComponent(PartyComponent.this);
                application.getActionOpen().execute(evt);
            }
        });
        setLayout(new BorderLayout());
        add(BorderLayout.CENTER, getImagePanel());
        setParty(application.getParty());
    }



    private ImagePanel getImagePanel() {
        if (imagePanel == null) {
            imagePanel = new ImagePanel(4, 4, 10, 10);
        }
        return imagePanel;
    }
    
    
    
    private ImageLabel createImageLabel() {
        ImageLabel out = null;
        if (party != null) {
            if (party.isRanked()) {
                if (party.isFinished()) {
                    out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_STAR_RED));
                    out.setToolTipText(FMT.format("PartyComponent.RS_RANKED_PARTY", FMT.format("PartyComponent.RS_FINISHED_PARTY")));
                }else if(party.isPaused()){
                    out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_STAR_ORANGE));
                    out.setToolTipText(FMT.format("PartyComponent.RS_RANKED_PARTY", FMT.format("PartyComponent.RS_PAUSED_PARTY")));                    
                }else{
                    out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_STAR_GREEN));
                    out.setToolTipText(FMT.format("PartyComponent.RS_RANKED_PARTY", FMT.format("PartyComponent.RS_STARTED_PARTY")));                    
                }
            } else {
                if (party.isFinished()) {
                    out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_BUTTON_RED));
                    out.setToolTipText(FMT.format("PartyComponent.RS_FINISHED_PARTY"));
                }else if(party.isPaused()){
                    out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_BUTTON_ORANGE));
                    out.setToolTipText(FMT.format("PartyComponent.RS_PAUSED_PARTY"));                    
                }else{
                    out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_BUTTON_GREEN));
                    out.setToolTipText(FMT.format("PartyComponent.RS_STARTED_PARTY"));                    
                }
            }
        }
        return out;
    }



    /**
     * @return the applicationId
     */
    public ApplicationId getApplicationId() {
        return applicationId;
    }



    /**
     * Set party
     * 
     * @param party
     *            Party
     */
    public void setParty(Party party) {
        this.party = party;
        getImagePanel().setImage(ImagePosition.BOTTOM_RIGHT, createImageLabel());
    }
}
