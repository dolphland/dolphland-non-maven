package com.dolphland.client.usecase.application.party.chess;

import com.dolphland.client.usecase.application.party.PartyRenderer;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;

interface ChessRenderer extends PartyRenderer<ChessBoard> {

    /**
     * Set action when select a square
     * 
     * @param actionSelectSquare
     *            Action when select a square
     */
    public void setActionSelectSquare(UIAction<ChessActionEvt, ?> actionSelectSquare);



    /**
     * Set action when select a player
     * 
     * @param actionSelectPlayer
     *            Action when select a player
     */
    public void setActionSelectPlayer(UIAction<ChessActionEvt, ?> actionSelectPlayer);



    /**
     * Get visitor's color
     * 
     * @return Visitor's color
     */
    public ChessColor getColorVisitor();



    /**
     * Set visitor's color
     * 
     * @param color
     *            Visitor's color
     */
    public void setColorVisitor(ChessColor color);
}
