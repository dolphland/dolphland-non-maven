package com.dolphland.client.usecase.application;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.dolphland.client.AppConstants;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.image.ImageLabel;
import com.dolphland.client.util.image.ImagePanel;
import com.dolphland.client.util.image.ImagePosition;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.core.ws.data.ApplicationId;

/**
 * Application component
 * 
 * @author JayJay
 * 
 */
public class ApplicationComponent extends FwkPanel {

    private static final MessageFormater FMT = MessageFormater.getFormater(ApplicationComponent.class);

    // Default background color
    private final static Color DEFAULT_BACKGROUND_COLOR = Color.WHITE;

    // Selection's color
    private final static Color COLOR_SELECTION = Color.RED;

    // Application id
    private ApplicationId applicationId;
    
    // Image panel
    private ImagePanel imagePanel;
    
    // Counter
    private int counter;



    /**
     * Constructor with application specified
     * 
     * @param application
     *            Application
     */
    public ApplicationComponent(final ApplicationAdapter application) {
        AssertUtil.notNull(application, "application is null !");
        this.applicationId = application.getApplicationId();
        setBackground(DEFAULT_BACKGROUND_COLOR);
        setImage(application.getActionOpen().getIcon(), false);
        setTypeBorder(TYPE_BORDER_ROUNDED_BORDER);
        setSelectedEnabled(true);
        setSelectedBackground(COLOR_SELECTION);
        setToolTipText(application.getActionOpen().getDescription());
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                ApplicationActionEvt evt = new ApplicationActionEvt();
                application.getActionOpen().execute(evt);
            }
        });
        setLayout(new BorderLayout());
        add(BorderLayout.CENTER, getImagePanel());
    }



    private ImagePanel getImagePanel() {
        if (imagePanel == null) {
            imagePanel = new ImagePanel(4, 4, 10, 10);
        }
        return imagePanel;
    }
    
    
    
    private ImageLabel createImageLabel() {
        ImageLabel out = null;
        if (counter > 0) {
            out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_PLUS_BLACK));
            out.setToolTipText(FMT.format("ApplicationComponent.RS_CURRENT_PARTIES", counter));
        }
        return out;
    }



    /**
     * @return the applicationId
     */
    public ApplicationId getApplicationId() {
        return applicationId;
    }



    /**
     * @param counter
     *            the counter to set
     */
    public void setCounter(int counter) {
        this.counter = counter;
        getImagePanel().setImage(ImagePosition.BOTTOM_RIGHT, createImageLabel());
    }
}
