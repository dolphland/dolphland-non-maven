package com.dolphland.client.usecase.application.party.chess;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvtFactory;
import com.dolphland.client.util.animator.AnimatedComponent;
import com.dolphland.client.util.animator.AnimatedPanel;
import com.dolphland.client.util.animator.Animation;
import com.dolphland.client.util.animator.AnimationParameter;
import com.dolphland.client.util.animator.DefaultAnimationParameter;
import com.dolphland.client.util.animator.SequenceAnimations;
import com.dolphland.client.util.application.components.FwkLabel;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.user.UserComponent;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;
import com.dolphland.core.ws.data.ChessColumn;
import com.dolphland.core.ws.data.ChessMovement;
import com.dolphland.core.ws.data.ChessPiece;
import com.dolphland.core.ws.data.ChessPieceType;
import com.dolphland.core.ws.data.ChessPlayer;
import com.dolphland.core.ws.data.ChessPlayerState;
import com.dolphland.core.ws.data.ChessPosition;
import com.dolphland.core.ws.data.ChessRow;
import com.dolphland.core.ws.data.ChessSquare;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.User;

/**
 * Party's renderer for chess
 * 
 * @author JayJay
 * 
 */
public class ChessRenderer2D extends AnimatedPanel implements ChessRenderer, UIActionEvtFactory<ChessActionEvt> {

    // Panel player size
    private final static int WIDTH_PANEL_PLAYER = UserComponent.DEFAULT_WIDTH;
    private final static int HEIGHT_PANEL_PLAYER = UserComponent.DEFAULT_HEIGHT;

    // Border's size
    private final static int BORDER_SIZE = 3;
    
    // Square size
    private final static int SIZE_SQUARE = 64;

    // Defeated pieces size
    private final static int SIZE_DEFEATED_PIECES = 130;

    // Position labels size
    private final static int SIZE_POSITION_LABELS = 20;

    // Piece's images url
    private final static String URL_2D_PIECE_PAWN_WHITE = "/images/chess2d/piece_pb_64_64.png";
    private final static String URL_2D_PIECE_PAWN_BLACK = "/images/chess2d/piece_pn_64_64.png";
    private final static String URL_2D_PIECE_BISHOP_WHITE = "/images/chess2d/piece_fb_64_64.png";
    private final static String URL_2D_PIECE_BISHOP_BLACK = "/images/chess2d/piece_fn_64_64.png";
    private final static String URL_2D_PIECE_KNIGHT_WHITE = "/images/chess2d/piece_cb_64_64.png";
    private final static String URL_2D_PIECE_KNIGHT_BLACK = "/images/chess2d/piece_cn_64_64.png";
    private final static String URL_2D_PIECE_ROOK_WHITE = "/images/chess2d/piece_tb_64_64.png";
    private final static String URL_2D_PIECE_ROOK_BLACK = "/images/chess2d/piece_tn_64_64.png";
    private final static String URL_2D_PIECE_QUEEN_WHITE = "/images/chess2d/piece_qb_64_64.png";
    private final static String URL_2D_PIECE_QUEEN_BLACK = "/images/chess2d/piece_qn_64_64.png";
    private final static String URL_2D_PIECE_KING_WHITE = "/images/chess2d/piece_rb_64_64.png";
    private final static String URL_2D_PIECE_KING_BLACK = "/images/chess2d/piece_rn_64_64.png";

    // Liste des url des ressources Images des cases
    private final static String URL_2D_SQUARE_WHITE = "/images/chess2d/square_b_01_64_64.png";
    private final static String URL_2D_SQUARE_BLACK = "/images/chess2d/square_n_01_64_64.png";

    // Indicator for animations enabled
    private boolean animationsEnabled;

    // Party
    private Party party;

    // Board
    private ChessBoard board;

    // Animation's parameter by id
    private Map<String, ChessAnimationParameter> animationsParameterById;

    // Visitor color
    private ChessColor colorVisitor = ChessColor.WHITE;

    // Actions
    private UIAction<ChessActionEvt, ?> actionSelectSquare;
    private UIAction<ChessActionEvt, ?> actionSelectPlayer;
    
    // Blinked movements
    private List<ChessMovement> blinkedMovements;



    /**
     * Default constructor
     */
    public ChessRenderer2D() {
        blinkedMovements = new ArrayList<ChessMovement>();
        animationsParameterById = new TreeMap<String, ChessRenderer2D.ChessAnimationParameter>();
        setOpaque(false);
    }


    
    @Override
    public ChessActionEvt createUIActionEvt() {
        ChessActionEvt evt = new ChessActionEvt();
        evt.setBoard(board);
        return evt;
    }



    @Override
    public JComponent getPartyComponent() {
        return this;
    }



    @Override
    public UserComponent getUserComponent(String userId) {
        return (UserComponent) getChessAnimatedComponent(new ChessAnimationParameter(ContextClient.getUser(userId), 0, 0));
    }



    @Override
    public void setAnimationsEnabled(boolean animationsEnabled) {
        this.animationsEnabled = animationsEnabled;
    }



    @Override
    public void setActionSelectSquare(UIAction<ChessActionEvt, ?> actionSelectSquare) {
        this.actionSelectSquare = actionSelectSquare;
    }



    @Override
    public void setActionSelectPlayer(UIAction<ChessActionEvt, ?> actionSelectPlayer) {
        this.actionSelectPlayer = actionSelectPlayer;
    }



    @Override
    public ChessColor getColorVisitor() {
        return colorVisitor;
    }



    @Override
    public void setColorVisitor(ChessColor color) {
        this.colorVisitor = color;
    }



    @Override
    public void updateParty(Party party, ChessBoard partyRepresentation) {
        this.party = party;
        this.board = partyRepresentation;

        List<SequenceAnimations> sequencesAnimations = getSequencesAnimations();

        // Stop animations
        stopAnimate();
        
        // Remove all animations
        removeAllAnimations();
        
        // Add each animation
        for (SequenceAnimations sequenceAnimations : sequencesAnimations) {
            addSequenceAnimations(sequenceAnimations);
        }
        
        // Start animations
        startAnimate();
        
        // Update blinking
        updateBlinking();
    }


    /**
     * Blink movements
     * 
     * @param movements Movements to blink
     */
    public void blinkMovements(List<ChessMovement> movements){
        this.blinkedMovements = movements;
        updateBlinking();
    }
    
    
    private synchronized void updateBlinking(){
        Color blinkColor = null;
        ChessAnimationParameter animationParameter = null;
        ChessPiece piece;
        AnimatedComponent animatedComponent = null;

        // For each squares
        for(ChessSquare square : board.getSquares()){
            piece = ChessHelper.getPiece(board, square.getPosition());
            animationParameter = new ChessAnimationParameter(square);
            animatedComponent = getChessAnimatedComponent(animationParameter);
            
            // Reinitialize blink's color
            blinkColor = null;

            // If not the versus user
            if (ChessHelper.getPlayer(board, ChessHelper.getVersusColor(board.getCurrentColor())).getUserId() == null || !ContextClient.getUserId().equals(ChessHelper.getPlayer(board, ChessHelper.getVersusColor(board.getCurrentColor())).getUserId())) {
                // If last movement
                if(board.getLastMovement() != null){
                    // If square is a source of a target movement's position
                    if(ChessHelper.compare(ChessHelper.getSourcePosition(board.getLastMovement()), square.getPosition()) == 0 || ChessHelper.compare(ChessHelper.getTargetPosition(board.getLastMovement()), square.getPosition()) == 0){
                        blinkColor = ChessColor.WHITE == board.getCurrentColor() ? Color.BLACK : Color.WHITE;
                    }
                }
            }

            if (piece != null) {
                // If piece is the king
                if (ChessPieceType.KING == piece.getType()) {
                    ChessPlayer player = ChessHelper.getPlayer(board, piece.getColor());
                    // If player is not in normal state
                    if (player != null && ChessPlayerState.NORMAL != player.getState()) {
                        // King's blinking color about player'state
                        switch (player.getState()) {
                            case CHESS :
                                blinkColor = Color.ORANGE;
                                break;

                            case MAT :
                                blinkColor = Color.RED;
                                break;

                            case PAT :
                                blinkColor = Color.GREEN;
                                break;
                        }
                    }
                }
            }

            // For each blinked movement
            for(ChessMovement movement : blinkedMovements){
                // If square is a source of a target movement's position
                if(ChessHelper.compare(ChessHelper.getSourcePosition(movement), square.getPosition()) == 0 || ChessHelper.compare(ChessHelper.getTargetPosition(movement), square.getPosition()) == 0){
                    blinkColor = ChessColor.WHITE == board.getCurrentColor() ? Color.WHITE : Color.BLACK;
                }
            }

            if (blinkColor != null) {
                // If blinking enabled
                animatedComponent.setBorder(new LineBorder(blinkColor, BORDER_SIZE));
            }else{
                // If blinking disabled
                animatedComponent.setBorder(null);
            }
        }
    }


    /**
     * Get animation's parameter from its identifier
     * 
     * @param id Identifier
     * 
     * @return Animation's parameter
     */
    private ChessAnimationParameter getAnimationParameter(String id){
        return animationsParameterById.get(id);
    }
    
        
    /**
     * Put animation's parameter with its identifier
     * 
     * @param id Identifier
     * @param parameter Animation's parameter
     * 
     * @return Animation's parameter
     */
    private ChessAnimationParameter putAnimationParameter(String id, ChessAnimationParameter parameter){
        animationsParameterById.put(id, parameter);
        return parameter;
    }
    
    

    /**
     * Get animations sequences
     */
    private List<SequenceAnimations> getSequencesAnimations() {
        List<SequenceAnimations> sequencesAnimations = new ArrayList<SequenceAnimations>();
        ChessAnimationParameter animationParameter = null;
        AnimatedComponent animatedComponent = null;
        Animation animation = null;

        // Remove animated components
        for (AnimatedComponent comp : getAllAnimatedComponents()) {
            animationParameter = getAnimationParameter(comp.getId());
            switch (animationParameter.getTypeAnimatedComponent()) {
                case POSITION_LABEl :
                    removeAnimatedComponent(comp.getId());
                    break;

                case USER :
                    // Remove user if no party's member
                    if(!party.getMembersUsersIds().contains(animationParameter.user.getId())){
                        removeAnimatedComponent(comp.getId());
                    }
                    break;
                    
                default :
                    break;
            }
        }

        // Show sequence with squares, players and static piece
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        sequencesAnimations.add(sequenceAnimations);

        // Show players
        ChessColor color = ChessColor.WHITE;
        animationParameter = new ChessAnimationParameter(ChessHelper.getPlayer(board, color));
        animatedComponent = getChessAnimatedComponent(animationParameter);
        animation = new Animation(animatedComponent, animationParameter);
        sequenceAnimations.addAnimation(animation);
        color = ChessColor.BLACK;
        animationParameter = new ChessAnimationParameter(ChessHelper.getPlayer(board, color));
        animatedComponent = getChessAnimatedComponent(animationParameter);
        animation = new Animation(animatedComponent, animationParameter);
        sequenceAnimations.addAnimation(animation);

        // Show square and positions labels
        for (ChessSquare square : board.getSquares()) {
            // Show square
            animationParameter = new ChessAnimationParameter(square);
            animatedComponent = getChessAnimatedComponent(animationParameter);
            animation = new Animation(animatedComponent, animationParameter);
            sequenceAnimations.addAnimation(animation);

            // Show row labels
            if (getColumn(animationParameter.position).ordinal() == 0) {
                animationParameter = new ChessAnimationParameter(animationParameter.position, false);
                animatedComponent = getChessAnimatedComponent(animationParameter);
                animation = new Animation(animatedComponent, animationParameter);
                sequenceAnimations.addAnimation(animation);
            }

            // Show column labels
            if (getRow(animationParameter.position).ordinal() == 0) {
                animationParameter = new ChessAnimationParameter(animationParameter.position, true);
                animatedComponent = getChessAnimatedComponent(animationParameter);
                animation = new Animation(animatedComponent, animationParameter);
                sequenceAnimations.addAnimation(animation);
            }
        }

        // Show pieces static and store positions from pieces moved
        List<ChessPosition> positionsPiecesMoved = new ArrayList<ChessPosition>();
        for (ChessPiece piece : board.getPieces()) {
            ChessPosition position = piece.getPosition();
            animationParameter = new ChessAnimationParameter(position, piece);
            animatedComponent = getChessAnimatedComponent(animationParameter);
            // If position has changed, show all steps and all movements
            if (ChessHelper.compare(getAnimationParameter(animatedComponent.getId()).position, position) != 0) {
                positionsPiecesMoved.add(position);
                continue;
            }
            animation = new Animation(animatedComponent, animationParameter);
            animationParameter.setLayerPolicy(ChessAnimationParameter.LAYER_FOREGROUND);
            sequenceAnimations.addAnimation(animation);
        }

        // Show sequence with pieces moved if necessary
        if (!positionsPiecesMoved.isEmpty()) {
            sequenceAnimations = new SequenceAnimations();
            sequencesAnimations.add(sequenceAnimations);
            for (ChessPosition position : positionsPiecesMoved) {
                ChessPiece piece = ChessHelper.getPiece(board, position);
                animationParameter = new ChessAnimationParameter(position, piece);
                animatedComponent = getChessAnimatedComponent(animationParameter);
                animationParameter.setStepsPolicy(null);
                animationParameter.setLayerPolicy(ChessAnimationParameter.LAYER_FOREGROUND);
                // If piece's type has changed
                if (animationParameter.typePiece != getAnimationParameter(animatedComponent.getId()).typePiece) {
                    animatedComponent.setBackImage(getImage(animationParameter.piece));
                    animationParameter.setBack(true);
                }
                animation = new Animation(animatedComponent, animationParameter);
                sequenceAnimations.addAnimation(animation);
            }
        }

        // Show sequence with defeated pieces
        sequenceAnimations = new SequenceAnimations();
        sequencesAnimations.add(sequenceAnimations);
        int indWhiteDefeatedPiece = 0;
        int indBlackDefeatedPiece = 0;
        for (ChessPiece piece : board.getDefeatedPieces()) {
            if (piece.getColor() == ChessColor.BLACK) {
                animationParameter = new ChessAnimationParameter(piece, indBlackDefeatedPiece++);
            } else {
                animationParameter = new ChessAnimationParameter(piece, indWhiteDefeatedPiece++);
            }
            animatedComponent = getChessAnimatedComponent(animationParameter);
            animation = new Animation(animatedComponent, animationParameter);
            animationParameter.setLayerPolicy(ChessAnimationParameter.LAYER_FOREGROUND);
            sequenceAnimations.addAnimation(animation);
        }
        
        // Show players and list visitors
        List<String> visitors = new ArrayList<String>();
        for (String userId : party.getMembersUsersIds()) {
            ChessPlayer player = ChessHelper.getPlayer(board, userId);
            if(player == null){
                visitors.add(userId);
            }else{
                // If member is a player
                animationParameter = new ChessAnimationParameter(ContextClient.getUser(userId), ChessHelper.getPlayer(board, userId));
            }
            animatedComponent = getChessAnimatedComponent(animationParameter);
            animation = new Animation(animatedComponent, animationParameter);
            sequenceAnimations.addAnimation(animation);
        }

        // Show visitors
        for (int indVisitor = 0 ; indVisitor < visitors.size() ; indVisitor++) {
            animationParameter = new ChessAnimationParameter(ContextClient.getUser(visitors.get(indVisitor)), indVisitor, visitors.size());
            animatedComponent = getChessAnimatedComponent(animationParameter);
            animation = new Animation(animatedComponent, animationParameter);
            sequenceAnimations.addAnimation(animation);
        }

        return sequencesAnimations;
    }



    /**
     * Get animated component for animation parameter specified (create it if
     * not existed)
     * 
     * @param animationParameter
     *            Animation parameter
     * 
     * @return Animated component
     */
    private AnimatedComponent getChessAnimatedComponent(final ChessAnimationParameter animationParameter) {
        AnimatedComponent result = getAnimatedComponent(animationParameter.getId());
        if (result == null) {
            switch (animationParameter.getTypeAnimatedComponent()) {
                case POSITION_LABEl :
                    result = createAnimatedPositionLabel(animationParameter);
                    break;

                case SQUARE :
                    result = createAnimatedSquare(animationParameter);
                    break;

                case PLAYER :
                    result = createAnimatedPlayer(animationParameter);
                    break;

                case USER :
                    result = createAnimatedUser(animationParameter);
                    break;

                case PIECE :
                case DEFEATED_PIECE :
                    result = createAnimatedPiece(animationParameter);
                    break;

                default :
                    break;
            }
            if (result != null) {
                // Put animation parameter
                putAnimationParameter(animationParameter.getId(), animationParameter);

                // Add animated component to board
                addAnimatedComponent(result);
            }
        }

        // Update animated component from parameter
        switch (animationParameter.getTypeAnimatedComponent()) {
            case SQUARE :
                final ChessPiece piece = ChessHelper.getPiece(board, animationParameter.position);
                for(MouseListener listener : Arrays.asList(result.getMouseListeners())){
                    result.removeMouseListener(listener);
                }
                result.addMouseListener(new MouseAdapter() {

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ChessActionEvt evt = createUIActionEvt();
                        evt.setPosition(animationParameter.position);
                        evt.setPiece(piece);
                        if (actionSelectSquare != null) {
                            actionSelectSquare.execute(evt);
                        }
                    }
                });
                break;

            case USER :
                UserComponent userComponent = ((UserComponent)result);
                userComponent.setUser(animationParameter.user);
                userComponent.setSelected(animationParameter.color == board.getCurrentColor() && animationParameter.player.getUserId() != null);
                break;

            default :
                break;
        }

        return result;
    }



    /**
     * Create animated position label for animation parameter specified
     * 
     * @param animationParameter
     *            Animation parameter
     * 
     * @return Animated position label
     */
    private AnimatedComponent createAnimatedPositionLabel(ChessAnimationParameter animationParameter) {
        AnimatedComponent animatedPositionLabel = new AnimatedComponent(animationParameter.getId());

        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        animatedPositionLabel.setLayout(layoutBuilder.get());

        JLabel lblPosition = null;
        if (animationParameter.horizontal) {
            lblPosition = getLabelColumn(animationParameter.position);
        } else {
            lblPosition = getLabelRow(animationParameter.position);
        }

        animatedPositionLabel.add(lblPosition, layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .height(TableLayout.FILL)
            .width(TableLayout.FILL)
            .get()));

        if (animationParameter.horizontal) {
            animatedPositionLabel.setSize(SIZE_SQUARE, SIZE_POSITION_LABELS);
        } else {
            animatedPositionLabel.setSize(SIZE_POSITION_LABELS, SIZE_SQUARE);
        }
        return animatedPositionLabel;
    }



    /**
     * Create animated square for animation parameter specified
     * 
     * @param animationParameter
     *            Animation parameter
     * 
     * @return Animated square
     */
    private AnimatedComponent createAnimatedSquare(final ChessAnimationParameter animationParameter) {
        String imageUrl = null;
        if (ChessColor.BLACK == animationParameter.square.getColor()) {
            imageUrl = URL_2D_SQUARE_BLACK;
        } else {
            imageUrl = URL_2D_SQUARE_WHITE;
        }

        return new AnimatedComponent(animationParameter.getId(), ImageUtil.getImage(imageUrl));
    }



    /**
     * Create animated player for animation parameter specified
     * 
     * @param animationParameter
     *            Animation parameter
     * 
     * @return Animated player
     */
    private AnimatedComponent createAnimatedPlayer(final ChessAnimationParameter animationParameter) {
        AnimatedComponent animatedPlayer = new AnimatedComponent(animationParameter.getId());

        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        animatedPlayer.setLayout(layoutBuilder.get());

        FwkPanel panelPlayer = null;
        panelPlayer = new FwkPanel();
        panelPlayer.setTypeBorder(FwkPanel.TYPE_BORDER_ROUNDED_BORDER);
        panelPlayer.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                ChessActionEvt evt = createUIActionEvt();
                evt.setColorPlayer(animationParameter.color);
                if (actionSelectPlayer != null) {
                    actionSelectPlayer.execute(evt);
                }
            }
        });

        animatedPlayer.add(panelPlayer, layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .height(TableLayout.FILL)
            .width(TableLayout.FILL)
            .get()));

        animatedPlayer.setSize(WIDTH_PANEL_PLAYER, HEIGHT_PANEL_PLAYER);
        return animatedPlayer;
    }



    /**
     * Create animated user for animation parameter specified
     * 
     * @param animationParameter
     *            Animation parameter
     * 
     * @return Animated player
     */
    private AnimatedComponent createAnimatedUser(final ChessAnimationParameter animationParameter) {
        AnimatedComponent animatedUser = new UserComponent(animationParameter.user);
        animatedUser.setSize(WIDTH_PANEL_PLAYER, HEIGHT_PANEL_PLAYER);
        return animatedUser;
    }



    /**
     * Create animated piece for animation parameter specified
     * 
     * @param animationParameter
     *            Animation parameter
     * 
     * @return Animated piece
     */
    private AnimatedComponent createAnimatedPiece(final ChessAnimationParameter animationParameter) {
        return new AnimatedComponent(animationParameter.getId(), getImage(animationParameter.piece));
    }



    /**
     * Get image from piece specified
     * 
     * @param piece
     *            Piece
     * 
     * @return Image
     */
    private BufferedImage getImage(ChessPiece piece) {
        String imageUrl = null;

        // Affichage en fonction du type et de la couleur
        switch (piece.getType()) {
            case PAWN :
                if (ChessColor.BLACK == piece.getColor()) {
                    imageUrl = URL_2D_PIECE_PAWN_BLACK;
                } else {
                    imageUrl = URL_2D_PIECE_PAWN_WHITE;
                }
                break;

            case BISHOP :
                if (ChessColor.BLACK == piece.getColor()) {
                    imageUrl = URL_2D_PIECE_BISHOP_BLACK;
                } else {
                    imageUrl = URL_2D_PIECE_BISHOP_WHITE;
                }
                break;

            case KNIGHT :
                if (ChessColor.BLACK == piece.getColor()) {
                    imageUrl = URL_2D_PIECE_KNIGHT_BLACK;
                } else {
                    imageUrl = URL_2D_PIECE_KNIGHT_WHITE;
                }
                break;

            case ROOK :
                if (ChessColor.BLACK == piece.getColor()) {
                    imageUrl = URL_2D_PIECE_ROOK_BLACK;
                } else {
                    imageUrl = URL_2D_PIECE_ROOK_WHITE;
                }
                break;

            case QUEEN :
                if (ChessColor.BLACK == piece.getColor()) {
                    imageUrl = URL_2D_PIECE_QUEEN_BLACK;
                } else {
                    imageUrl = URL_2D_PIECE_QUEEN_WHITE;
                }
                break;

            case KING :
                if (ChessColor.BLACK == piece.getColor()) {
                    imageUrl = URL_2D_PIECE_KING_BLACK;
                } else {
                    imageUrl = URL_2D_PIECE_KING_WHITE;
                }
                break;
        }

        return ImageUtil.getImage(imageUrl);
    }



    /**
     * Return the view color for current user
     * 
     * @return View color for current user
     */
    private ChessColor getViewColor() {
        if (ChessHelper.getPlayer(board, ChessColor.WHITE).getUserId() != null && ChessHelper.getPlayer(board, ChessColor.WHITE).getUserId().equals(ContextClient.getUserId())) {
            return ChessColor.WHITE;
        }

        if (ChessHelper.getPlayer(board, ChessColor.BLACK).getUserId() != null && ChessHelper.getPlayer(board, ChessColor.BLACK).getUserId().equals(ContextClient.getUserId())) {
            return ChessColor.BLACK;
        }

        return colorVisitor;
    }



    /**
     * Return position's row about current user
     * 
     * @param position
     *            Position
     * 
     * @return Position's row
     */
    private ChessRow getRow(ChessPosition position) {
        switch (getViewColor()) {
            case NONE :
                return ChessRow.values()[ChessColumn.values().length - 1 - position.getColumn().ordinal()];

            case BLACK :
                return ChessRow.values()[ChessRow.values().length - 1 - position.getRow().ordinal()];

            case WHITE :
                return position.getRow();
        }
        return null;
    }



    /**
     * Return position's column about current user
     * 
     * @param position
     *            Position
     * 
     * @return Position's column
     */
    private ChessColumn getColumn(ChessPosition position) {
        switch (getViewColor()) {
            case NONE :
                return ChessColumn.values()[position.getRow().ordinal()];

            case BLACK :
                return ChessColumn.values()[ChessColumn.values().length - 1 - position.getColumn().ordinal()];

            case WHITE :
                return position.getColumn();
        }
        return null;
    }



    /**
     * Return label's row from position about current user
     * 
     * @param position
     *            Position
     * @return Label's row
     */
    private FwkLabel getLabelRow(ChessPosition position) {
        String lbl = null;
        switch (getViewColor()) {
            case NONE :
                char c = 'A';
                c += position.getColumn().ordinal();
                lbl = new Character(c).toString();
                break;

            case BLACK :
            case WHITE :
                lbl = new Integer(position.getRow().ordinal() + 1).toString();
                break;
        }
        FwkLabel out = new FwkLabel(lbl);
        out.setHorizontalAlignment(SwingConstants.CENTER);
        out.setVerticalAlignment(SwingConstants.CENTER);
        return out;
    }



    /**
     * Return label's column from position about current user
     * 
     * @param position
     *            Position
     * @return Label's column
     */
    private FwkLabel getLabelColumn(ChessPosition position) {
        String lbl = null;
        switch (getViewColor()) {
            case NONE :
                lbl = new Integer(position.getRow().ordinal() + 1).toString();
                break;

            case BLACK :
            case WHITE :
                char c = 'A';
                c += position.getColumn().ordinal();
                lbl = new Character(c).toString();
                break;
        }
        FwkLabel out = new FwkLabel(lbl);
        out.setHorizontalAlignment(SwingConstants.CENTER);
        out.setVerticalAlignment(SwingConstants.CENTER);
        return out;
    }

    /**
     * Animation parameter for chess
     * 
     * @author JayJay
     * 
     */
    private class ChessAnimationParameter extends DefaultAnimationParameter {

        private TypeAnimatedComponent typeAnimatedComponent;
        private User user;
        private ChessPosition position;
        private ChessSquare square;
        private ChessPlayer player;
        private ChessPiece piece;
        private int indPiece;
        private ChessPieceType typePiece;
        private boolean horizontal;
        private ChessColor color;
        private int indVisitor;
        private int visitorsCount;



        /**
         * Constructor with position and horizontal indicator specified
         * 
         * @param position
         *            Position
         * @param horizontal
         *            If horizontal
         */
        public ChessAnimationParameter(ChessPosition position, boolean horizontal) {
            this.position = position;
            this.horizontal = horizontal;
            this.typeAnimatedComponent = TypeAnimatedComponent.POSITION_LABEl;
            setStepsPolicy(ONLY_ONE_STEP);
            refreshAnimationParameters();
        }



        /**
         * Constructor with square specified
         * 
         * @param square
         *            Square
         */
        public ChessAnimationParameter(ChessSquare square) {
            this.position = square.getPosition();
            this.square = square;
            this.typeAnimatedComponent = TypeAnimatedComponent.SQUARE;
            refreshAnimationParameters();
        }



        /**
         * Constructor with position and piece specified
         * 
         * @param position
         *            Position
         * @param piece
         *            Piece
         */
        public ChessAnimationParameter(ChessPosition position, ChessPiece piece) {
            this.position = position;
            this.piece = piece;
            this.typePiece = piece.getType();
            this.typeAnimatedComponent = TypeAnimatedComponent.PIECE;
            refreshAnimationParameters();
        }



        /**
         * Constructor with piece and indice specified
         * 
         * @param piece
         *            Piece
         * @param indPiece
         *            Indice piece
         */
        public ChessAnimationParameter(ChessPiece piece, int indPiece) {
            this.piece = piece;
            this.indPiece = indPiece;
            this.typeAnimatedComponent = TypeAnimatedComponent.DEFEATED_PIECE;
            refreshAnimationParameters();
        }



        /**
         * Constructor with player specified
         * 
         * @param player
         *            Player
         */
        public ChessAnimationParameter(ChessPlayer player) {
            this.player = player;
            this.color = player.getColor();
            this.typeAnimatedComponent = TypeAnimatedComponent.PLAYER;
            setStepsPolicy(ONLY_ONE_STEP);
            refreshAnimationParameters();
        }



        /**
         * Constructor with user and its player 
         * 
         * @param user
         *            User
         * @param player
         *            Player
         */
        public ChessAnimationParameter(User user, ChessPlayer player) {
            this.user = user;
            this.player = player;
            this.color = player.getColor();
            this.typeAnimatedComponent = TypeAnimatedComponent.USER;
            refreshAnimationParameters();
        }



        /**
         * Constructor with user, its visitor's indice and visitors count
         * 
         * @param user
         *            User
         * @param indVisitor
         *            Visitor's indice
         * @param visitorsCount
         *            Visitors count
         */
        public ChessAnimationParameter(User user, int indVisitor, int visitorsCount) {
            this.user = user;
            this.indVisitor = indVisitor;
            this.visitorsCount = visitorsCount;
            this.typeAnimatedComponent = TypeAnimatedComponent.USER;
            refreshAnimationParameters();
        }



        @Override
        public void fireAnimationPanelSizeHasChanged() {
            refreshAnimationParameters();
        }



        @Override
        public Integer getStepsPolicy() {
            if (!animationsEnabled) {
                return ONLY_ONE_STEP;
            }
            return super.getStepsPolicy();
        }
        
        
        
        @Override
        public Integer getLayerPolicy() {
            if(typeAnimatedComponent == TypeAnimatedComponent.USER){
                return AnimationParameter.LAYER_FOREGROUND;
            }
            return super.getLayerPolicy();
        }

        
        
        @Override
        public Long getAnimationDurationMs() {
            return super.getAnimationDurationMs();
        }


        /**
         * Get type animated component
         * 
         * @return Type animated component
         */
        public TypeAnimatedComponent getTypeAnimatedComponent() {
            return typeAnimatedComponent;
        }



        /**
         * Refresh animation parameters
         */
        private void refreshAnimationParameters() {
            if(typeAnimatedComponent == TypeAnimatedComponent.USER && player == null){
                setX(getCurrentX());
            }else{
                int x = (int) ((double) (ChessRenderer2D.this.getWidth() - getCurrentWidth()) / 2.0d);
                if (x < 0) {
                    x = 0;
                }
                x += getCurrentX();
                setX(x);
            }

            if(typeAnimatedComponent == TypeAnimatedComponent.USER && player == null){
                setY(getCurrentY());
            }else{
                int y = (int) ((double) (ChessRenderer2D.this.getHeight() - getCurrentHeight()) / 2.0d);
                if (y < 0) {
                    y = 0;
                }
                y += getCurrentY();
                setY(y);
            }
        }



        /**
         * Get current width
         * 
         * @return Current width
         */
        private int getCurrentWidth() {
            int width = 0;
            if (getViewColor() == ChessColor.NONE) {
                width += WIDTH_PANEL_PLAYER + DEFAULT_GAP;
            } else {
                width += SIZE_DEFEATED_PIECES + DEFAULT_GAP;
            }
            width += SIZE_POSITION_LABELS;
            width += SIZE_SQUARE * 8 + SIZE_POSITION_LABELS;
            if (getViewColor() == ChessColor.NONE) {
                width += WIDTH_PANEL_PLAYER + DEFAULT_GAP;
            } else {
                width += SIZE_DEFEATED_PIECES + DEFAULT_GAP;
            }
            return width;
        }



        /**
         * Get current height
         * 
         * @return Current height
         */
        private int getCurrentHeight() {
            int height = 0;
            if (getViewColor() != ChessColor.NONE) {
                height += HEIGHT_PANEL_PLAYER + DEFAULT_GAP;
            } else {
                height += SIZE_DEFEATED_PIECES + DEFAULT_GAP;
            }
            height += SIZE_POSITION_LABELS;
            height += SIZE_SQUARE * 8 + SIZE_POSITION_LABELS;
            if (getViewColor() != ChessColor.NONE) {
                height += HEIGHT_PANEL_PLAYER + DEFAULT_GAP;
            } else {
                height += SIZE_DEFEATED_PIECES + DEFAULT_GAP;
            }
            return height;
        }



        /**
         * Get current x
         * 
         * @return Current x
         */
        private int getCurrentX() {
            int x = 0;
            if(typeAnimatedComponent == TypeAnimatedComponent.USER && player == null){
                return WIDTH_PANEL_PLAYER;
            }
            if (getViewColor() == ChessColor.NONE) {
                if (typeAnimatedComponent == TypeAnimatedComponent.PLAYER && color == ChessColor.BLACK) {
                    return x;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.USER && color == ChessColor.BLACK) {
                    return x;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE && piece.getColor() == ChessColor.WHITE) {
                    x += (indPiece / 2) * SIZE_SQUARE;
                    return x;
                }
                x += WIDTH_PANEL_PLAYER + DEFAULT_GAP;
            } else {
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE && piece.getColor() != getViewColor()) {
                    if (indPiece % 2 == 0) {
                        x += SIZE_SQUARE;
                    }
                    return x;
                }
                x += SIZE_DEFEATED_PIECES + DEFAULT_GAP;
            }

            if (typeAnimatedComponent == TypeAnimatedComponent.POSITION_LABEl) {
                if (horizontal) {
                    return x + SIZE_POSITION_LABELS + getColumn(position).ordinal() * SIZE_SQUARE;
                } else {
                    return x;
                }
            }

            x += SIZE_POSITION_LABELS;

            if (typeAnimatedComponent == TypeAnimatedComponent.SQUARE || typeAnimatedComponent == TypeAnimatedComponent.PIECE) {
                return x + getColumn(position).ordinal() * SIZE_SQUARE;
            }

            x += SIZE_SQUARE * 8 + SIZE_POSITION_LABELS;

            if (getViewColor() == ChessColor.NONE) {
                if (typeAnimatedComponent == TypeAnimatedComponent.PLAYER) {
                    x += DEFAULT_GAP;
                    return x;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.USER) {
                    x += DEFAULT_GAP;
                    return x;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE) {
                    x += DEFAULT_GAP - (indPiece / 2) * SIZE_SQUARE;
                    return x;
                }
            } else {
                if (typeAnimatedComponent == TypeAnimatedComponent.PLAYER) {
                    return (getCurrentWidth() - WIDTH_PANEL_PLAYER) / 2;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.USER) {
                    return (getCurrentWidth() - WIDTH_PANEL_PLAYER) / 2;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE) {
                    x = getCurrentWidth() - SIZE_SQUARE;
                    if (indPiece % 2 == 0) {
                        x -= SIZE_SQUARE;
                    }
                    return x;
                }
            }

            return 0;
        }



        /**
         * Get current y
         * 
         * @return Current y
         */
        private int getCurrentY() {
            int y = 0;
            if(typeAnimatedComponent == TypeAnimatedComponent.USER && player == null){
                return ((ChessRenderer2D.this.getHeight() - (HEIGHT_PANEL_PLAYER * visitorsCount)) / (visitorsCount + 1)) * (indVisitor + 1);
            }
            if (getViewColor() != ChessColor.NONE) {
                if (typeAnimatedComponent == TypeAnimatedComponent.PLAYER && color != getViewColor()) {
                    return y;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.USER && color != getViewColor()) {
                    return y;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE && piece.getColor() == getViewColor()) {
                    y += (indPiece / 2) * SIZE_SQUARE;
                    return y;
                }
                y += HEIGHT_PANEL_PLAYER + DEFAULT_GAP;
            } else {
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE && piece.getColor() == ChessColor.BLACK) {
                    if (indPiece % 2 == 0) {
                        y += SIZE_SQUARE;
                    }
                    return y;
                }
                y += SIZE_DEFEATED_PIECES + DEFAULT_GAP;
            }

            if (typeAnimatedComponent == TypeAnimatedComponent.POSITION_LABEl) {
                if (horizontal) {
                    return y;
                } else {
                    return y + SIZE_POSITION_LABELS + getRow(position).ordinal() * SIZE_SQUARE;
                }
            }

            y += SIZE_POSITION_LABELS;

            if (typeAnimatedComponent == TypeAnimatedComponent.SQUARE || typeAnimatedComponent == TypeAnimatedComponent.PIECE) {
                return y + getRow(position).ordinal() * SIZE_SQUARE;
            }

            y += SIZE_SQUARE * 8 + SIZE_POSITION_LABELS;

            if (getViewColor() == ChessColor.NONE) {
                if (typeAnimatedComponent == TypeAnimatedComponent.PLAYER) {
                    return (getCurrentHeight() - HEIGHT_PANEL_PLAYER) / 2;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.USER) {
                    return (getCurrentHeight() - HEIGHT_PANEL_PLAYER) / 2;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE) {
                    y = getCurrentHeight() - SIZE_SQUARE;
                    if (indPiece % 2 == 0) {
                        y -= SIZE_SQUARE;
                    }
                    return y;
                }
            } else {
                if (typeAnimatedComponent == TypeAnimatedComponent.PLAYER) {
                    y += DEFAULT_GAP;
                    return y;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.USER) {
                    y += DEFAULT_GAP;
                    return y;
                }
                if (typeAnimatedComponent == TypeAnimatedComponent.DEFEATED_PIECE) {
                    y += DEFAULT_GAP - (indPiece / 2) * SIZE_SQUARE;
                    return y;
                }
            }

            return 0;
        }



        /**
         * Get id
         * 
         * @return Id
         */
        public String getId() {
            switch (typeAnimatedComponent) {
                case POSITION_LABEl :
                    return ChessHelper.toString(position) + horizontal;

                case SQUARE :
                    return ChessHelper.toString(position) + ChessHelper.toString(square);

                case PLAYER :
                    return ChessHelper.toString(player);

                case USER :
                    return ChessHelper.toString(user);

                case DEFEATED_PIECE :
                case PIECE :
                    return ChessHelper.toString(piece) + piece.getPieceId();

                default :
                    break;
            }
            return null;
        }
    }

    /**
     * Type animated component
     * 
     * @author JayJay
     * 
     */
    private static enum TypeAnimatedComponent {
        USER,
        POSITION_LABEl,
        SQUARE,
        PLAYER,
        PIECE,
        DEFEATED_PIECE
    }
}