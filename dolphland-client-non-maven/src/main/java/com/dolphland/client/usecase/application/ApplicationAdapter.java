package com.dolphland.client.usecase.application;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.version.VersionUtil;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;

/**
 * Application's adapter
 * 
 * @author JayJay
 */
public class ApplicationAdapter implements Cloneable {

    private static final MessageFormater FMT = MessageFormater.getFormater(ApplicationAdapter.class);

    // Application's id
    private ApplicationId applicationId;

    // Application's name
    private String applicationName;

    // Application's title
    private String applicationTitle;

    // Task's icon
    private String iconTaskRessource;

    // Application's icon
    private String iconAppRessource;

    // Class of application's scenario
    private Class<? extends AbstractApplicationScenario> classScenario;

    // Tell if application is unique
    private boolean applicationUnique;

    // Tell if application is an auto application (not loadable by the user)
    private boolean applicationAuto;

    // Action to open an application
    private UIAction<ApplicationActionEvt, ?> actionOpen;

    // Party
    private Party party;



    /**
     * Constructor with application's id
     * 
     * @param applicationId
     *            Application's id
     */
    public ApplicationAdapter(ApplicationId applicationId) {
        AssertUtil.notNull(applicationId, "applicationId is null !");
        this.applicationId = applicationId;
        this.applicationName = FMT.format("ApplicationAdapter.RS_APP_" + applicationId.toString());
        this.applicationTitle = FMT.format("ApplicationAdapter.RS_TITLE_APPLICATION", getApplicationName(), getApplicationId(), VersionUtil.getVersion(getApplicationId()));
    }



    public String getIconTaskRessource() {
        return iconTaskRessource;
    }



    public void setIconTaskRessource(String iconTaskRessource) {
        this.iconTaskRessource = iconTaskRessource;
    }



    public String getIconAppRessource() {
        return iconAppRessource;
    }



    public void setIconAppRessource(String iconAppRessource) {
        this.iconAppRessource = iconAppRessource;
    }



    public Class<? extends AbstractApplicationScenario> getClassScenario() {
        return classScenario;
    }



    public void setClassScenario(Class<? extends AbstractApplicationScenario> classScenario) {
        this.classScenario = classScenario;
    }



    public ApplicationId getApplicationId() {
        return applicationId;
    }



    public String getApplicationName() {
        return applicationName;
    }



    public String getApplicationTitle() {
        return applicationTitle;
    }



    public UIAction<ApplicationActionEvt, ?> getActionOpen() {
        return actionOpen;
    }



    public void setActionOpen(UIAction<ApplicationActionEvt, ?> actionOpen) {
        this.actionOpen = actionOpen;
    }



    public boolean isApplicationUnique() {
        return applicationUnique;
    }



    public void setApplicationUnique(boolean applicationUnique) {
        this.applicationUnique = applicationUnique;
    }



    public boolean isApplicationAuto() {
        return applicationAuto;
    }



    public void setApplicationAuto(boolean applicationAuto) {
        this.applicationAuto = applicationAuto;
    }



    public Party getParty() {
        return party;
    }



    public void setParty(Party party) {
        this.party = party;
    }

}
