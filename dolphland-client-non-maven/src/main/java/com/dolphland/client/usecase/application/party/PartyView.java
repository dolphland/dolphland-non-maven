package com.dolphland.client.usecase.application.party;

import info.clearthought.layout.TableLayout;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import com.dolphland.client.AppConstants;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.application.components.FwkView;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.input.CharacterTextField;
import com.dolphland.client.util.layout.TableLayoutBuilder;

/**
 * Scenario for a party
 * 
 * @author JayJay
 * 
 */
public class PartyView extends FwkView<PartyActionEvt> {

    private static final MessageFormater FMT = MessageFormater.getFormater(PartyView.class);

    // UI components
    private FwkPanel panelBackground;
    private FwkPanel panelParty;
    private CharacterTextField jtfChatInput;



    public PartyView() {
        initialize();
    }



    private void initialize() {
        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        setLayout(layoutBuilder.get());

        add(getPanelBackground(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .width(TableLayout.FILL)
            .height(TableLayout.FILL)
            .get()));
    }



    private FwkPanel getPanelBackground() {
        if (panelBackground == null) {
            panelBackground = new FwkPanel();
            panelBackground.setImage(AppConstants.PATH_IMAGE_BACKGROUND_PARTY);

            TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

            panelBackground.setLayout(layoutBuilder.get());

            panelBackground.add(getPanelParty(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .row(0)
                .leftInsets(DEFAULT_GAP)
                .width(TableLayout.FILL)
                .rightInsets(DEFAULT_GAP)
                .topInsets(DEFAULT_GAP)
                .height(TableLayout.FILL)
                .get()));

            panelBackground.add(createPanelChatAndHide(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
                .row(1)
                .topInsets(DEFAULT_GAP)
                .height(TableLayout.PREFERRED)
                .bottomInsets(DEFAULT_GAP)
                .get()));
        }
        return panelBackground;
    }



    private FwkPanel getPanelParty() {
        if (panelParty == null) {
            panelParty = new FwkPanel();
            panelParty.setOpaque(false);
        }
        return panelParty;
    }



    private CharacterTextField getJtfChatInput() {
        if (jtfChatInput == null) {
            jtfChatInput = new CharacterTextField(100);
            jtfChatInput.setColumns(jtfChatInput.getNbChar());
        }
        return jtfChatInput;
    }



    private FwkPanel createPanelChatAndHide() {
        FwkPanel out = new FwkPanel();
        out.setOpaque(false);

        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        out.setLayout(layoutBuilder.get());

        out.add(getJtfChatInput(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .column(0)
            .height(TableLayout.PREFERRED)
            .width(TableLayout.FILL)
            .get()));

        return out;
    }



    @Override
    public PartyActionEvt createUIActionEvt() {
        PartyActionEvt evt = new PartyActionEvt();
        evt.setMessage(getJtfChatInput().stringValue());
        return evt;
    }



    /**
     * Set party's component
     * 
     * @param partyComponent
     *            Party's component
     */
    public void setPartyComponent(JComponent partyComponent) {
        getPanelParty().removeAll();

        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();

        getPanelParty().setLayout(layoutBuilder.get());

        getPanelParty().add(partyComponent, layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .height(TableLayout.FILL)
            .width(TableLayout.FILL)
            .get()));

        getPanelParty().validate();
        getPanelParty().repaint();
    }



    /**
     * Set action chat
     * 
     * @param actionChat
     *            Action chat
     */
    public void setActionChat(UIAction<PartyActionEvt, ?> actionChat) {
        bind(actionChat, getJtfChatInput());
    }



    /**
     * Initialize chat input
     */
    public void initChatInput() {
        getJtfChatInput().setValue(FMT.format("PartyView.RS_DEFAULT_CHAT_TEXT"));

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                getJtfChatInput().selectAll();
            }
        });
    }
}
