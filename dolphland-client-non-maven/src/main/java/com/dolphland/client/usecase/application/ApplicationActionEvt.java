package com.dolphland.client.usecase.application;

import java.awt.Component;

import com.dolphland.client.util.action.UIActionEvt;

/**
 * Action context for a task
 * 
 * @author JayJay
 * 
 */
public class ApplicationActionEvt extends UIActionEvt {

    private Component linkComponent;



    /**
     * @return the linkComponent
     */
    public Component getLinkComponent() {
        return linkComponent;
    }



    /**
     * @param linkComponent
     *            the linkComponent to set
     */
    public void setLinkComponent(Component linkComponent) {
        this.linkComponent = linkComponent;
    }

}
