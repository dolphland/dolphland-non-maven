package com.dolphland.client.usecase.desktop;

import java.awt.Component;
import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;

import org.apache.log4j.Logger;

import com.dolphland.client.AppConstants;
import com.dolphland.client.context.ContextClient;
import com.dolphland.client.event.EventApplicationsChanged;
import com.dolphland.client.event.EventPartiesChanged;
import com.dolphland.client.event.EventPartyJoined;
import com.dolphland.client.event.EventPartyLeft;
import com.dolphland.client.event.EventUserLogin;
import com.dolphland.client.event.EventUserLogout;
import com.dolphland.client.event.EventUserReplacement;
import com.dolphland.client.service.SVFactory;
import com.dolphland.client.usecase.application.AbstractApplicationScenario;
import com.dolphland.client.usecase.application.ApplicationActionEvt;
import com.dolphland.client.usecase.application.ApplicationAdapter;
import com.dolphland.client.usecase.application.coach.CoachScenario;
import com.dolphland.client.usecase.application.dolphland.DolphlandScenario;
import com.dolphland.client.usecase.application.login.LoginScenario;
import com.dolphland.client.usecase.application.party.AbstractPartyScenario;
import com.dolphland.client.usecase.application.party.chess.ChessScenario;
import com.dolphland.client.usecase.application.web.WebScenario;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.animator.AnimationActionEvt;
import com.dolphland.client.util.application.components.FwkScenario;
import com.dolphland.client.util.application.components.FwkWorkPane;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EventSubscriber;
import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.flip.FlipApplicationAdapter;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.transaction.MVCTx;
import com.dolphland.client.util.url.UrlUtil;
import com.dolphland.core.ws.data.Application;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.User;

/**
 * Scenario for desktop
 * 
 * @author JayJay
 * 
 */
public class DesktopScenario extends FwkScenario {

    private static final MessageFormater FMT = MessageFormater.getFormater(DesktopScenario.class);

    private static final Logger LOG = Logger.getLogger(DesktopScenario.class);

    // View
    private DesktopView view;

    // Dolphland's scenario
    private DolphlandScenario dolphlandScenario;

    // Login's scenario
    private LoginScenario loginScenario;

    // Dolphland's application
    private ApplicationAdapter dolphlandApplication;

    // Login's application
    private ApplicationAdapter loginApplication;

    // Applications grouped by flip application id
    private Map<String, DesktopFlipApplication> applicationsByFlipApplicationId;

    // Event subscriber
    private EventSubscriber eventSubscriber;



    /**
     * Default constructor
     */
    public DesktopScenario() {
        applicationsByFlipApplicationId = new LinkedHashMap<String, DesktopFlipApplication>();

        // Create view
        view = new DesktopView();

        // Set actions
        view.setLogoutAction(new LogoutAction());
        view.setFlipAction(new FlipAction());

        // Create dolphland's scenario
        dolphlandScenario = new DolphlandScenario();

        // Create login's scenario
        loginScenario = new LoginScenario();

        // Create dolphland's application
        dolphlandApplication = createApplicationAdapter(ApplicationId.DOLPHLAND);

        // Create login's application
        loginApplication = createApplicationAdapter(ApplicationId.LOGIN);

        // Create event subscriber
        eventSubscriber = new DesktopEventSubscriber();

        // Subscribe
        subscribe();
    }



    @Override
    protected void doLeave() {
        unsubscribe();
    }



    private void subscribe() {
        EDT.subscribe(eventSubscriber)
            .forEvents(EventApplicationsChanged.DEST)
            .forEvents(EventPartiesChanged.DEST)
            .forEvents(EventUserLogin.DEST)
            .forEvents(EventUserLogout.DEST)
            .forEvents(EventUserReplacement.DEST)
            .forEvents(EventPartyJoined.DEST)
            .forEvents(EventPartyLeft.DEST);
    }



    private void unsubscribe() {
        EDT.unsubscribe(eventSubscriber).forAllEvents();
    }



    @Override
    protected void doEnter(Object param) {
        // Set view
        getWorkPane().setView(view);

        // Add application Login
        addApplication(loginApplication, loginScenario, null, null);
    }



    /**
     * Add an application to desktop with an attach point
     * 
     * @param application
     *            Application
     * @param scenario
     *            Scenario
     * @param inAttachPoint
     *            In attach point
     * @param outAttachPoint
     *            Out attach point
     * 
     * @return True if successful adding application
     */
    private boolean addApplication(ApplicationAdapter application, AbstractApplicationScenario scenario, Point inAttachPoint, Point outAttachPoint) {
        if (scenario != null && getWorkPane() != null) {
            // Create work pane
            FwkWorkPane applicationWorkPane = new FwkWorkPane(getWorkPane().getMasterFrame());

            // Create flip application
            DesktopFlipApplication flipApplication = createFlipApplicationAdapter(createFlipApplicationId(application), application, scenario, applicationWorkPane, inAttachPoint, outAttachPoint);

            // Add application to flip application
            applicationsByFlipApplicationId.put(flipApplication.getApplicationId(), flipApplication);

            // Set work pane
            scenario.setWorkPane(applicationWorkPane);

            // Enter in scenario
            scenario.enter();

            // Add application to desktop
            view.addApplication(flipApplication);

            return true;
        }
        return false;
    }



    /**
     * Remove an application from its flip application id
     * 
     * @param flipApplicationId
     *            Flip application id
     * 
     * @return True if successful removing application
     */
    private boolean removeApplication(String flipApplicationId) {
        DesktopFlipApplication application = applicationsByFlipApplicationId.get(flipApplicationId);
        if (application != null && application.getScenario() != null) {
            // Remove application from flip application
            applicationsByFlipApplicationId.remove(flipApplicationId);

            // Remove application
            view.removeApplication(flipApplicationId);

            // Leave scenario
            application.getScenario().leave();

            return true;
        }
        return false;
    }



    /**
     * Create adapter from application's id
     * 
     * @param applicationId
     *            Application's id
     * 
     * @return Adapter
     */
    private ApplicationAdapter createApplicationAdapter(ApplicationId applicationId) {
        ApplicationAdapter adapter = null;
        switch (applicationId) {
            case DOLPHLAND :
                adapter = new ApplicationAdapter(applicationId);
                adapter.setApplicationAuto(true);
                adapter.setApplicationUnique(true);
                adapter.setClassScenario(DolphlandScenario.class);
                break;

            case LOGIN :
                adapter = new ApplicationAdapter(applicationId);
                adapter.setApplicationAuto(true);
                adapter.setApplicationUnique(true);
                adapter.setClassScenario(LoginScenario.class);
                break;

            case LOGOUT :
                adapter = new ApplicationAdapter(applicationId);
                adapter.setApplicationAuto(true);
                adapter.setApplicationUnique(true);
                // Logout application is not implemented for this client in a
                // scenario but only with a button
                break;

            // case CONFIG :
            // adapter = new ApplicationAdapter(applicationId);
            // adapter.setActionOpen(new ActionOpenApplication(adapter));
            // adapter.setIconTaskRessource(AppConstants.PATH_IMAGE_ICON_CONFIG_BIG);
            // adapter.setIconAppRessource(AppConstants.PATH_IMAGE_ICON_CONFIG_SMALL);
            // adapter.setClassScenario(ConfigScenario.class);
            // break;

            case CHESS :
                adapter = new ApplicationAdapter(applicationId);
                adapter.setActionOpen(new ActionCreateParty(adapter));
                adapter.setIconTaskRessource(AppConstants.PATH_IMAGE_ICON_CHESS_BIG);
                adapter.setIconAppRessource(AppConstants.PATH_IMAGE_ICON_CHESS_SMALL);
                adapter.setClassScenario(ChessScenario.class);
                break;

            //case DRAUGHT :
                //adapter = new ApplicationAdapter(applicationId);
                //adapter.setActionOpen(new ActionCreateParty(adapter));
                //adapter.setIconTaskRessource(AppConstants.PATH_IMAGE_ICON_DRAUGHT_BIG);
                //adapter.setIconAppRessource(AppConstants.PATH_IMAGE_ICON_DRAUGHT_SMALL);
                // adapter.setClassScenario(DraughtScenario.class);
                //break;

            //case BELOTE :
                //adapter = new ApplicationAdapter(applicationId);
                //adapter.setActionOpen(new ActionCreateParty(adapter));
                //adapter.setIconTaskRessource(AppConstants.PATH_IMAGE_ICON_BELOTE_BIG);
                //adapter.setIconAppRessource(AppConstants.PATH_IMAGE_ICON_BELOTE_SMALL);
                // adapter.setClassScenario(BeloteScenario.class);
                //break;

            //case COACH :
                //adapter = new ApplicationAdapter(applicationId);
                //adapter.setActionOpen(new ActionOpenApplication(adapter));
                //adapter.setIconTaskRessource(AppConstants.PATH_IMAGE_ICON_COACH_BIG);
                //adapter.setIconAppRessource(AppConstants.PATH_IMAGE_ICON_COACH_SMALL);
                //adapter.setClassScenario(CoachScenario.class);
                //break;

            //case EXPLORER :
                //adapter = new ApplicationAdapter(applicationId);
                //adapter.setActionOpen(new ActionOpenApplication(adapter));
                //adapter.setIconTaskRessource(AppConstants.PATH_IMAGE_ICON_EXPLORER_BIG);
                //adapter.setIconAppRessource(AppConstants.PATH_IMAGE_ICON_EXPLORER_SMALL);
                // adapter.setClassScenario(ExplorerScenario.class);
                //break;

            //case WEB :
                //adapter = new ApplicationAdapter(applicationId);
                //adapter.setActionOpen(new ActionOpenApplication(adapter));
                //adapter.setIconTaskRessource(AppConstants.PATH_IMAGE_ICON_WEB_BIG);
                //adapter.setIconAppRessource(AppConstants.PATH_IMAGE_ICON_WEB_SMALL);
                //adapter.setClassScenario(WebScenario.class);
                //break;

            default :
                throw new AppBusinessException(FMT.format("DesktopScenario.RS_APPLICATION_NOT_IMPLEMENTED", applicationId));
        }
        return adapter;
    }



    /**
     * Create adapter from a party
     * 
     * @param party
     *            party
     * 
     * @return Adapter
     */
    private ApplicationAdapter createPartyAdapter(Party party) {
        ApplicationAdapter adapter = createApplicationAdapter(party.getApplicationId());
        adapter.setParty(party);
        adapter.setActionOpen(new ActionOpenParty(adapter));
        return adapter;
    }



    /**
     * Create an attach point from a component
     * 
     * @param component
     *            Component
     * 
     * @return Attach point
     */
    private Point createAttachPoint(Component component) {
        if (component == null) {
            return null;
        }
        // The attach point is the center of the component
        int x = component.getX() + component.getWidth() / 2;
        int y = component.getY() + component.getHeight() / 2;
        Component parent = component.getParent();
        while (parent != null && parent != view) {
            x += parent.getX();
            y += parent.getY();
            parent = parent.getParent();
        }
        return new Point(x, y);
    }



    /**
     * Create an attach point for left screen
     * 
     * @return Attach point
     */
    private Point createLeftScreenAttachPoint() {
        return new Point(0, view.getHeight() / 2);
    }



    /**
     * Create flip application
     * 
     * @param flipApplicationId
     *            Flip application id
     * @param application
     *            Application
     * @param applicationComponent
     *            Application's component
     * @param inAttachPoint
     *            In attach point
     * @param outAttachPoint
     *            Out attach point
     * 
     * @return Flip application
     */
    private DesktopFlipApplication createFlipApplicationAdapter(String flipApplicationId, ApplicationAdapter application, AbstractApplicationScenario scenario, Component applicationComponent, Point inAttachPoint, Point outAttachPoint) {
        // Create adapter
        DesktopFlipApplication adapter = new DesktopFlipApplication(flipApplicationId, application, applicationComponent, scenario);

        // Set attach points for animation show
        adapter.getFlipAnimationShow().setInAttachPoint(inAttachPoint);
        adapter.getFlipAnimationShow().setOutAttachPoint(outAttachPoint);

        // Add flip's animations actions depending application id
        switch (application.getApplicationId()) {
            case DOLPHLAND :
                adapter.getFlipAnimationShow().getActionsAfter().add(new ActionReplaceApplication(ApplicationId.LOGIN));
                break;

            case LOGIN :
                adapter.getFlipAnimationShow().getActionsAfter().add(new ActionReplaceApplication(ApplicationId.DOLPHLAND));
                adapter.getFlipAnimationShow().getActionsAfter().add(new ActionInitCaretPositionLogin());
                break;

            case LOGOUT :
                break;

            //case CONFIG :
                //break;

            case CHESS :
                adapter.getFlipAnimationHide().getActionsAfter().add(new ActionLeaveParty(application));
                break;

            //case DRAUGHT :
                //break;

            //case BELOTE :
                //break;

            //case COACH :
                //break;

            //case EXPLORER :
                //break;

            //case WEB :
                //break;

            default :
                throw new AppBusinessException(FMT.format("DesktopScenario.RS_APPLICATION_NOT_IMPLEMENTED", application.getApplicationId()));
        }

        // Update actions states before any animation
        adapter.getFlipAnimationHide().getActionsBefore().add(new UpdateActionsStates());
        adapter.getFlipAnimationMaximize().getActionsBefore().add(new UpdateActionsStates());
        adapter.getFlipAnimationMinimize().getActionsBefore().add(new UpdateActionsStates());
        adapter.getFlipAnimationShow().getActionsBefore().add(new UpdateActionsStates());

        // Update actions states after any animation
        adapter.getFlipAnimationHide().getActionsAfter().add(new UpdateActionsStates());
        adapter.getFlipAnimationMaximize().getActionsAfter().add(new UpdateActionsStates());
        adapter.getFlipAnimationMinimize().getActionsAfter().add(new UpdateActionsStates());
        adapter.getFlipAnimationShow().getActionsAfter().add(new UpdateActionsStates());

        return adapter;
    }



    /**
     * Create flip application's id from an application
     * 
     * @param application
     *            Application
     * 
     * @return Flip application's id
     */
    private String createFlipApplicationId(ApplicationAdapter application) {
        StringBuilder id = new StringBuilder();
        id.append(application.getApplicationId().toString());
        if (application.getParty() != null) {
            id.append(application.getParty().getId());
        }
        return id.toString();
    }

    /**
     * Action to logout
     * 
     * @author JayJay
     * 
     */
    private class LogoutAction extends UIAction<DesktopActionEvt, DesktopActionEvt> {

        @Override
        public Icon getIcon() {
            return ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_LOGOUT);
        }



        @Override
        public String getDescription() {
            return FMT.format("DesktopScenario.RS_LOGOUT_ACTION_DESCRIPTION");
        }



        @Override
        public boolean isVisible(DesktopActionEvt ctx) {
            // Logout not visible if no maximized application
            if (StrUtil.isEmpty(ctx.getMaximizedApplicationId())) {
                return false;
            }

            // Logout visible if not login application
            for (FlipApplicationAdapter application : ctx.getApplications()) {
                if (application.getApplicationId().equals(ApplicationId.LOGIN.toString())) {
                    return false;
                }
            }
            return true;
        }



        @Override
        protected boolean shouldExecute(DesktopActionEvt param) {
            return getWorkPane().showConfirmDialog(FMT.format("DesktopScenario.RS_LOGOUT_ACTION_CONFIRM"));
        }



        @Override
        protected void preExecute(DesktopActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("DesktopScenario.RS_LOGOUT_ACTION_WAITING"));
        }



        @Override
        protected DesktopActionEvt doExecute(DesktopActionEvt param) {
            SVFactory.createSVUser().logout(false);
            return param;
        }



        @Override
        protected void updateViewSuccess(DesktopActionEvt data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Action to flip
     * 
     * @author JayJay
     * 
     */
    private class FlipAction extends UIAction<DesktopActionEvt, DesktopActionEvt> {

        @Override
        public String getDescription() {
            return FMT.format("DesktopScenario.RS_FLIP_ACTION_DESCRIPTION");
        }



        @Override
        public boolean isVisible(DesktopActionEvt ctx) {
            // Flip not visible if no maximized application
            if (StrUtil.isEmpty(ctx.getMaximizedApplicationId())) {
                return false;
            }

            // Flip visible if more one application and not login application
            for (FlipApplicationAdapter application : ctx.getApplications()) {
                if (application.getApplicationId().equals(ApplicationId.LOGIN.toString())) {
                    return false;
                }
            }
            return ctx.getApplications().size() > 1;
        }



        @Override
        protected void preExecute(DesktopActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("DesktopScenario.RS_FLIP_ACTION_WAITING"));
        }



        @Override
        protected DesktopActionEvt doExecute(DesktopActionEvt param) {
            return param;
        }



        @Override
        protected void updateViewSuccess(DesktopActionEvt data) {
            view.flipApplications();
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Action to open an application
     * 
     * @author JayJay
     * 
     */
    private class ActionOpenApplication extends UIAction<ApplicationActionEvt, Object> {

        private ApplicationAdapter application;



        /**
         * Constructor with application
         * 
         * @param application
         *            Application
         */
        public ActionOpenApplication(ApplicationAdapter application) {
            AssertUtil.notNull(application, "application is null !");
            this.application = application;
        }



        @Override
        public Icon getIcon() {
            return ImageUtil.getImageIcon(application.getIconTaskRessource());
        }



        @Override
        public String getDescription() {
            return application.getApplicationName();
        }



        @Override
        protected boolean shouldExecute(ApplicationActionEvt param) {
            if (application.getClassScenario() == null) {
                getWorkPane().showWarningDialog(FMT.format("DesktopScenario.RS_APPLICATION_NOT_IMPLEMENTED", application.getApplicationName()));
                return false;
            }
            return super.shouldExecute(param);
        }



        @Override
        protected void preExecute(ApplicationActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("DesktopScenario.RS_WAITING_OPEN_APPLICATION", application.getApplicationName()));
        }



        @Override
        protected Object doExecute(ApplicationActionEvt param) {
            try {
                // Create scenario
                AbstractApplicationScenario scenario = (AbstractApplicationScenario) application.getClassScenario().getConstructor(ApplicationId.class).newInstance(application.getApplicationId());

                // Add application
                addApplication(application, scenario, createAttachPoint(param.getLinkComponent()), createLeftScreenAttachPoint());
            } catch (Exception exc) {
                LOG.error(FMT.format("DesktopScenario.RS_ERROR_APPLICATION", application.getApplicationName(), exc.getMessage()), exc); //$NON-NLS-1$
                throw new AppBusinessException(FMT.format("DesktopScenario.RS_ERROR_APPLICATION", application.getApplicationName(), exc.getMessage())); //$NON-NLS-1$
            }
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Action to create a party
     * 
     * @author JayJay
     * 
     */
    private class ActionCreateParty extends UIAction<ApplicationActionEvt, Object> {

        private ApplicationAdapter application;
        private boolean ranked;



        /**
         * Constructor with application
         * 
         * @param application
         *            Application
         */
        public ActionCreateParty(ApplicationAdapter application) {
            AssertUtil.notNull(application, "application is null !");
            this.application = application;
        }



        @Override
        public Icon getIcon() {
            return ImageUtil.getImageIcon(application.getIconTaskRessource());
        }



        @Override
        public String getDescription() {
            return application.getApplicationName();
        }



        @Override
        protected boolean shouldExecute(ApplicationActionEvt param) {
            if (application.getClassScenario() == null) {
                getWorkPane().showWarningDialog(FMT.format("DesktopScenario.RS_APPLICATION_NOT_IMPLEMENTED", application.getApplicationName()));
                return false;
            }
            ranked = getWorkPane().showConfirmDialog(FMT.format("DesktopScenario.RS_CONFIRM_RANKED"));
            return super.shouldExecute(param);
        }



        @Override
        protected void preExecute(ApplicationActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("DesktopScenario.RS_WAITING_OPEN_APPLICATION", application.getApplicationName()));
        }



        @Override
        protected Object doExecute(ApplicationActionEvt param) {
            // Create party
            Party party = SVFactory.createSVParty().createParty(application.getApplicationId(), ranked);

            // Open party
            new ActionOpenParty(createPartyAdapter(party)).execute(param);

            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Action to open a party
     * 
     * @author JayJay
     * 
     */
    private class ActionOpenParty extends UIAction<ApplicationActionEvt, Object> {

        private ApplicationAdapter application;
        private User owner;



        /**
         * Constructor with application
         * 
         * @param application
         *            Application
         */
        public ActionOpenParty(ApplicationAdapter application) {
            AssertUtil.notNull(application, "application is null !");
            this.application = application;
            this.owner = ContextClient.getUser(application.getParty().getOwnerUserId());
        }



        @Override
        public Icon getIcon() {
            return ImageUtil.getImageIcon(ImageUtil.getImage(UrlUtil.getUrl(AppConstants.URL_IMAGE_AVATAR + owner.getAvatar())));
        }



        @Override
        public String getDescription() {
            return FMT.format("DesktopScenario.RS_DESCRIPTION_PARTY", owner.getFirstName(), owner.getLastName());
        }



        @Override
        protected boolean shouldExecute(ApplicationActionEvt param) {
            if (application.getClassScenario() == null) {
                getWorkPane().showWarningDialog(FMT.format("DesktopScenario.RS_APPLICATION_NOT_IMPLEMENTED", application.getApplicationName()));
                return false;
            }
            return super.shouldExecute(param);
        }



        @Override
        protected void preExecute(ApplicationActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("DesktopScenario.RS_WAITING_OPEN_APPLICATION", application.getApplicationName()));
        }



        @Override
        protected Object doExecute(ApplicationActionEvt param) {
            // Join party
            SVFactory.createSVParty().joinParty(application.getParty().getId());

            try {
                // Create scenario
                AbstractApplicationScenario scenario = (AbstractPartyScenario<?>) application.getClassScenario().getConstructor(ApplicationId.class, String.class).newInstance(application.getApplicationId(), application.getParty().getId());

                // Add application
                addApplication(application, scenario, createAttachPoint(param.getLinkComponent()), createLeftScreenAttachPoint());
            } catch (Exception exc) {
                LOG.error(FMT.format("DesktopScenario.RS_ERROR_APPLICATION", application.getApplicationName(), exc.getMessage()), exc); //$NON-NLS-1$
                throw new AppBusinessException((FMT.format("DesktopScenario.RS_ERROR_APPLICATION", application.getApplicationName(), exc.getMessage()))); //$NON-NLS-1$
            }

            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Action to leave a party
     * 
     * @author JayJay
     * 
     */
    private class ActionLeaveParty extends UIAction<AnimationActionEvt, Object> {

        private ApplicationAdapter application;



        /**
         * Constructor with application
         * 
         * @param application
         *            Application
         */
        public ActionLeaveParty(ApplicationAdapter application) {
            AssertUtil.notNull(application, "application is null !");
            this.application = application;
        }



        @Override
        protected void preExecute(AnimationActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("DesktopScenario.RS_WAITING_LEAVE_PARTY"));
        }



        @Override
        protected Object doExecute(AnimationActionEvt param) {
            SVFactory.createSVParty().leaveParty(application.getParty().getId());
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
            // Update is made with event receiving
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Action to update actions states
     * 
     * @author p-jeremy.scafi
     * 
     */
    private class UpdateActionsStates extends UIAction<AnimationActionEvt, Object> {

        @Override
        protected void updateViewSuccess(Object data) {
            // Update actions states
            view.updateActionsStates();
        }
    }

    /**
     * Action to replace an application
     * 
     * @author JayJay
     * 
     */
    private class ActionReplaceApplication extends UIAction<AnimationActionEvt, Object> {

        private ApplicationId applicationId;



        /**
         * Constructor with application's id
         * 
         * @param applicationId
         *            Application's id
         */
        public ActionReplaceApplication(ApplicationId applicationId) {
            AssertUtil.notNull(applicationId, "applicationId is null !");
            this.applicationId = applicationId;
        }



        @Override
        protected Object doExecute(AnimationActionEvt param) {
            removeApplication(applicationId.toString());

            // Remove all applications that are not auto
            for (String flipApplicationId : new ArrayList<String>(applicationsByFlipApplicationId.keySet())) {
                ApplicationAdapter application = applicationsByFlipApplicationId.get(flipApplicationId).getApplication();
                if (application.isApplicationAuto()) {
                    continue;
                }
                removeApplication(flipApplicationId);
            }

            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }
    }

    /**
     * Action to init caret position for login
     * 
     * @author JayJay
     * 
     */
    private class ActionInitCaretPositionLogin extends UIAction<AnimationActionEvt, Object> {

        @Override
        protected void updateViewSuccess(Object data) {
            loginScenario.initCaretPosition();
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }
    }

    /**
     * Transaction to refresh applications
     * 
     * @author JayJay
     * 
     */
    private class RefreshApplicationsTx extends MVCTx<Object, List<ApplicationAdapter>> {

        @Override
        protected List<ApplicationAdapter> doExecute(Object param) {
            List<ApplicationAdapter> result = new ArrayList<ApplicationAdapter>();
            for (Application application : ContextClient.getApplications()) {
                ApplicationAdapter adapter = createApplicationAdapter(application.getId());
                // If application is loadable by the user
                if (!adapter.isApplicationAuto()) {
                    result.add(adapter);
                }
            }
            return result;
        }



        @Override
        protected void updateViewSuccess(List<ApplicationAdapter> data) {
            // Set applications
            dolphlandScenario.setApplications(data);
        }



        @Override
        protected void updateViewError(Exception exc) {
            getWorkPane().showErrorDialog(exc.getMessage());
        }
    }

    /**
     * Transaction to refresh parties
     * 
     * @author JayJay
     * 
     */
    private class RefreshPartiesTx extends MVCTx<Object, List<ApplicationAdapter>> {

        @Override
        protected List<ApplicationAdapter> doExecute(Object param) {
            List<ApplicationAdapter> result = new ArrayList<ApplicationAdapter>();
            for (Party party : ContextClient.getParties()) {
                result.add(createPartyAdapter(party));
            }
            return result;
        }



        @Override
        protected void updateViewSuccess(List<ApplicationAdapter> data) {
            dolphlandScenario.setParties(data);
        }



        @Override
        protected void updateViewError(Exception exc) {
            getWorkPane().showErrorDialog(exc.getMessage());
        }
    }

    /**
     * Event subscriber for desktop
     * 
     * @author JayJay
     * 
     */
    public class DesktopEventSubscriber extends DefaultEventSubscriber {

        public void eventReceived(EventUserLogin e) {
            // If current user login
            if (e.getClientId().equals(ContextClient.getClientId())) {
                // Replace application login by application Dolphland
                addApplication(dolphlandApplication, dolphlandScenario, createAttachPoint(loginScenario.getUserComponent(e.getUser())), createAttachPoint(view.getLogoutComponent()));
            }
        }



        public void eventReceived(EventUserLogout e) {
            // If current user logout
            if (e.getClientId().equals(ContextClient.getClientId())) {
                // Select user logout
                loginScenario.setSelectedUser(e.getUser());
                // Replace application dolphland by application login
                addApplication(loginApplication, loginScenario, createAttachPoint(view.getLogoutComponent()), createAttachPoint(loginScenario.getUserComponent(e.getUser())));
            }
        }



        public void eventReceived(EventUserReplacement e) {
            if (e.getClientId().equals(ContextClient.getClientId())) {
                // If the current client, replace application login by
                // application Dolphland and prevent that client replaces
                addApplication(dolphlandApplication, dolphlandScenario, createAttachPoint(loginScenario.getUserComponent(e.getUser())), createAttachPoint(view.getLogoutComponent()));
                getWorkPane().showWarningDialog(FMT.format("DesktopScenario.RS_CLIENT_REPLACES"));
            } else if (e.getUser().getId().equals(ContextClient.getUserId())) {
                // Select user replaced
                loginScenario.setSelectedUser(e.getUser());
                // If not the current client, replace application dolphland by
                // application login and prevent that client is replaced
                addApplication(loginApplication, loginScenario, createAttachPoint(view.getLogoutComponent()), createAttachPoint(loginScenario.getUserComponent(e.getUser())));
                getWorkPane().showWarningDialog(FMT.format("DesktopScenario.RS_CLIENT_REPLACED"));
            }
        }



        public void eventReceived(EventApplicationsChanged e) {
            // Refresh applications
            new RefreshApplicationsTx().execute();
        }



        public void eventReceived(EventPartiesChanged e) {
            // Refresh parties
            new RefreshPartiesTx().execute();
        }



        public void eventReceived(EventPartyJoined e) {
            // If current user has joined the party
            if (e.getUser().getId().equals(ContextClient.getUserId())) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("User " + e.getUser().getId() + " has joined party " + e.getParty().getId());
                }
            }
        }



        public void eventReceived(EventPartyLeft e) {
            // If current user has left the party
            if (e.getUser().getId().equals(ContextClient.getUserId())) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("User " + e.getUser().getId() + " has left party " + e.getParty().getId());
                }
            }
        }
    }
}
