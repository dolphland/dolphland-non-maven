package com.dolphland.client.usecase.application.coach;

import com.dolphland.client.util.application.components.FwkView;

/**
 * View for coach's goals
 * 
 * @author JayJay
 * 
 */
public class CoachGoalsView extends FwkView<CoachActionEvt> {

    // UI Components
}
