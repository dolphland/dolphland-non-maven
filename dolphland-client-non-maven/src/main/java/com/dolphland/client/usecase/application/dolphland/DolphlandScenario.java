package com.dolphland.client.usecase.application.dolphland;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.dolphland.client.AppConstants;
import com.dolphland.client.context.ContextClient;
import com.dolphland.client.usecase.application.AbstractApplicationScenario;
import com.dolphland.client.usecase.application.ApplicationAdapter;
import com.dolphland.client.usecase.application.ApplicationComponent;
import com.dolphland.client.usecase.application.party.PartyComponent;
import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.transaction.MVCTx;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.User;

/**
 * Application's scenario for Dolphland
 * 
 * @author JayJay
 * 
 */
public class DolphlandScenario extends AbstractApplicationScenario {

    // View
    private DolphlandView view;



    /**
     * Default constructor
     */
    public DolphlandScenario() {
        super(ApplicationId.DOLPHLAND);

        // Create view
        view = new DolphlandView();
    }



    @Override
    protected void doEnter(Object param) {
        getWorkPane().setView(view);

        // Subscribe
        subscribe();

        // Refresh background
        new RefreshBackgroundTx().execute();
    }



    @Override
    protected void doLeave() {
        unsubscribe();
    }



    private void subscribe() {
    }



    private void unsubscribe() {
    }



    /**
     * Set applications
     * 
     * @param applications
     *            Applications
     */
    public void setApplications(List<ApplicationAdapter> applications) {
        // Set applications components
        List<ApplicationComponent> applicationsComponents = new ArrayList<ApplicationComponent>();
        for (ApplicationAdapter application : applications) {
            applicationsComponents.add(new ApplicationComponent(application));
        }
        view.setApplications(applicationsComponents);

        // Set application loaded if applications are set and not empty
        setApplicationLoaded(!applications.isEmpty());
    }



    /**
     * Set parties
     * 
     * @param parties
     *            Parties
     */
    public void setParties(List<ApplicationAdapter> parties) {
        // Set parties components
        List<PartyComponent> partiesComponents = new ArrayList<PartyComponent>();
        for (ApplicationAdapter party : parties) {
            partiesComponents.add(new PartyComponent(party));
        }
        view.setParties(partiesComponents);
    }

    /**
     * Transaction to refresh background
     * 
     * @author JayJay
     * 
     */
    private class RefreshBackgroundTx extends MVCTx<Object, URL> {

        @Override
        protected URL doExecute(Object param) {
            User currentUser = ContextClient.getUser(ContextClient.getUserId());
            URL url = null;
            try {
                url = new URL(AppConstants.URL_IMAGE_BACKGROUND + currentUser.getBackground());
            } catch (MalformedURLException exc) {
                throw new AppBusinessException("Error when getting background for current user", exc);
            }
            return url;
        }



        @Override
        protected void updateViewSuccess(URL data) {
            view.setBackgroundImage(ImageUtil.getImage(data));
        }



        @Override
        protected void updateViewError(Exception exc) {
            getWorkPane().showErrorDialog(exc.getMessage());
        }
    }
}
