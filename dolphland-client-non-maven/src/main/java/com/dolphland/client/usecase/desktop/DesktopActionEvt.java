package com.dolphland.client.usecase.desktop;

import java.util.List;

import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.flip.FlipApplicationAdapter;

/**
 * Action context for desktop
 * 
 * @author JayJay
 * 
 */
public class DesktopActionEvt extends UIActionEvt {

    private List<FlipApplicationAdapter> applications;
    private String maximizedApplicationId;



    public List<FlipApplicationAdapter> getApplications() {
        return applications;
    }



    public void setApplications(List<FlipApplicationAdapter> applications) {
        this.applications = applications;
    }



    public String getMaximizedApplicationId() {
        return maximizedApplicationId;
    }



    public void setMaximizedApplicationId(String maximizedApplicationId) {
        this.maximizedApplicationId = maximizedApplicationId;
    }
}
