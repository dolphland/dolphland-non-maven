package com.dolphland.client.usecase.application.login;

import java.awt.Component;
import java.util.List;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.event.EventUsersChanged;
import com.dolphland.client.service.SVFactory;
import com.dolphland.client.usecase.application.AbstractApplicationScenario;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EventSubscriber;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.transaction.MVCTx;
import com.dolphland.core.ws.data.ApplicationId;
import com.dolphland.core.ws.data.User;

/**
 * Scenario for login
 * 
 * @author JayJay
 * 
 */
public class LoginScenario extends AbstractApplicationScenario {

    private static final MessageFormater FMT = MessageFormater.getFormater(LoginScenario.class);

    // View
    private LoginView view;

    // Event subscriber
    private EventSubscriber eventSubscriber;



    /**
     * Default constructor
     */
    public LoginScenario() {
        super(ApplicationId.LOGIN);

        view = new LoginView();

        // Set actions
        view.setActionLogin(new ActionLogin());

        // Create event subscriber
        eventSubscriber = new LoginEventSubscriber();
    }



    @Override
    protected void doEnter(Object param) {
        // Force work pane and view opaque
        getWorkPane().setOpaque(false);
        view.setOpaque(false);

        getWorkPane().setView(view);

        // Subscribe
        subscribe();

        // Refresh users
        new RefreshUsersTx().execute();
    }



    @Override
    protected void doLeave() {
        unsubscribe();
    }



    private void subscribe() {
        EDT.subscribe(eventSubscriber)
            .forEvents(EventUsersChanged.DEST);
    }



    private void unsubscribe() {
        EDT.unsubscribe(eventSubscriber).forAllEvents();
    }



    /**
     * Show desktop's background
     */
    public void showDesktopBackground() {
        // Show desktop's background
        view.showDesktopBackground();
    }



    /**
     * Get user component
     * 
     * @param user
     *            User
     * 
     * @return User component
     */
    public Component getUserComponent(User user) {
        return view.getUserComponent(user);
    }



    /**
     * Set selected user
     * 
     * @param user
     *            User
     */
    public void setSelectedUser(User user) {
        view.setSelectedUser(user);
    }



    /**
     * Initialize caret position
     */
    public void initCaretPosition() {
        view.initCaretPosition();
    }

    /**
     * Transaction to refresh users
     * 
     * @author JayJay
     * 
     */
    private class RefreshUsersTx extends MVCTx<LoginActionEvt, List<User>> {

        @Override
        protected List<User> doExecute(LoginActionEvt param) {
            // Try to get users from context
            List<User> users = ContextClient.getUsers();

            // If no user, get users
            if (users.isEmpty()) {
                ContextClient.setUsers(SVFactory.createSVUser().getUsers());
                users = ContextClient.getUsers();
            }

            return users;
        }



        @Override
        protected void updateViewSuccess(List<User> data) {
            view.setUsers(data);

            // Set application loaded if users are set and not empty
            setApplicationLoaded(!data.isEmpty());
        }



        @Override
        protected void updateViewError(Exception exc) {
            getWorkPane().showErrorDialog(exc.getMessage());
        }
    }

    /**
     * Action to login
     * 
     * @author JayJay
     * 
     */
    private class ActionLogin extends UIAction<LoginActionEvt, Object> {

        @Override
        public boolean isVisible() {
            return isVisible(view.createUIActionEvt());
        }



        @Override
        public boolean isVisible(LoginActionEvt ctx) {
            return super.isVisible(ctx) && ctx.getUserId() != null;
        }



        @Override
        public boolean isEnabled(LoginActionEvt ctx) {
            clearDisabledCauses();
            if (StrUtil.isEmpty(ctx.getUserId())) {
                addDisabledCause(FMT.format("LoginScenario.RS_ERROR_NO_USER_ID")); //$NON-NLS-1$
            }
            if (StrUtil.isEmpty(ctx.getUserPassword())) {
                addDisabledCause(FMT.format("LoginScenario.RS_ERROR_NO_USER_PASSWORD")); //$NON-NLS-1$
            }
            return getDisabledCauses().isEmpty() && super.isEnabled(ctx);
        }



        @Override
        protected void preExecute(LoginActionEvt param) {
            getWorkPane().showInfoMessage(FMT.format("LoginScenario.RS_WAITING_LOGIN"));
        }



        @Override
        protected Object doExecute(LoginActionEvt param) {
            SVFactory.createSVUser().login(param.getUserId(), param.getUserPassword());
            return null;
        }



        @Override
        protected void updateViewSuccess(Object data) {
        }



        @Override
        protected void updateViewError(Exception ex) {
            getWorkPane().showErrorDialog(ex.getMessage());
        }



        @Override
        protected void postExecute() {
            getWorkPane().clearMessages();
        }
    }

    /**
     * Event subscriber for login
     * 
     * @author JayJay
     * 
     */
    public class LoginEventSubscriber extends DefaultEventSubscriber {

        public void eventReceived(EventUsersChanged e) {
            // Refresh users
            new RefreshUsersTx().execute();
        }
    }
}
