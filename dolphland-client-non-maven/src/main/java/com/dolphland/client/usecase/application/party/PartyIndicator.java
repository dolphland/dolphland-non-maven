package com.dolphland.client.usecase.application.party;

/**
 * Party's indicator
 * 
 * @author jeremy.scafi
 *
 */
public enum PartyIndicator {

    RANKED,
    PAUSED,
    STARTED;
}