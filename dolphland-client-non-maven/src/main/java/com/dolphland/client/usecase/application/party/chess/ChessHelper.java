package com.dolphland.client.usecase.application.party.chess;

import java.util.Iterator;

import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;
import com.dolphland.core.ws.data.ChessMovement;
import com.dolphland.core.ws.data.ChessPiece;
import com.dolphland.core.ws.data.ChessPlayer;
import com.dolphland.core.ws.data.ChessPosition;
import com.dolphland.core.ws.data.ChessSquare;
import com.dolphland.core.ws.data.User;

/**
 * Chess's helper
 * 
 * @author JayJay
 * 
 */
class ChessHelper {

    private static final MessageFormater FMT = MessageFormater.getFormater(ChessHelper.class);



    /**
     * Get piece from board and position
     * 
     * @param board
     *            Board
     * @param position
     *            Position
     * 
     * @return Piece
     */
    final static ChessPiece getPiece(ChessBoard board, ChessPosition position) {
        for (ChessPiece piece : board.getPieces()) {
            if (compare(piece.getPosition(), position) == 0) {
                return piece;
            }
        }
        return null;
    }



    /**
     * Get player from board and color
     * 
     * @param board
     *            Board
     * @param color
     *            Color
     * 
     * @return Player
     */
    final static ChessPlayer getPlayer(ChessBoard board, ChessColor color) {
        for (ChessPlayer player : board.getPlayers()) {
            if (player.getColor() == color) {
                return player;
            }
        }
        return null;
    }



    /**
     * Return user's player
     * 
     * @param board
     *            Board
     * @param userId
     *            User's id
     * 
     * @return User's player
     */
    final static ChessPlayer getPlayer(ChessBoard board, String userId) {
        for (ChessPlayer player : board.getPlayers()) {
            if (userId.equals(player.getUserId())) {
                return player;
            }
        }
        return null;
    }
    
    
    
    /**
     * Get versus color
     * 
     * @param color Color
     * 
     * @return Versus color
     */
    final static ChessColor getVersusColor(ChessColor color){
        return color == ChessColor.WHITE ? ChessColor.BLACK : ChessColor.WHITE;
    }



    /**
     * Get target position from a movement
     * 
     * @param movement
     *            Movement
     * 
     * @return Target position
     */
    final static ChessPosition getTargetPosition(ChessMovement movement) {
        if (movement == null || movement.getSquares().size() <= 1) {
            return null;
        }
        return movement.getSquares().get(movement.getSquares().size() - 1).getPosition();
    }



    /**
     * Get source position from a movement
     * 
     * @param movement
     *            Movement
     * 
     * @return Source position
     */
    final static ChessPosition getSourcePosition(ChessMovement movement) {
        if (movement == null || movement.getSquares().isEmpty()) {
            return null;
        }
        return movement.getSquares().get(0).getPosition();
    }



    /**
     * Compare two positions and return -1 if position1 < position2, 1 if
     * position1 > position2 or 0 if position1 = position2
     * 
     * @param position1
     *            First position
     * @param position2
     *            Second position
     * 
     * @return -1, 1 or 0
     */
    final static int compare(ChessPosition position1, ChessPosition position2) {
        int result = position1.getRow().compareTo(position2.getRow());
        if (result != 0) {
            return result;
        }
        return position1.getColumn().compareTo(position2.getColumn());
    }



    /**
     * Get String representation of the color
     * 
     * @param color
     *            Color
     * 
     * @return String representation
     */
    final static String toString(ChessColor color) {
        String result = null;
        switch (color) {
            case WHITE :
                result = FMT.format("ChessHelper.RS_COLOR_WHITE");
                break;

            case BLACK :
                result = FMT.format("ChessHelper.RS_COLOR_BLACK");
                break;

            default :
                result = FMT.format("ChessHelper.RS_UNDEFINED");
                break;
        }
        return result;
    }



    /**
     * Return formatted value from {@link ChessPosition}
     * 
     * @param position
     *            Position
     * 
     * @return Formatted value
     */
    final static String toString(ChessPosition position) {
        char c = 'A';
        c += position.getColumn().ordinal();
        return "(" + c + ", " + (position.getRow().ordinal() + 1) + ")";
    }



    /**
     * Return formatted value from {@link ChessSquare}
     * 
     * @param square
     *            Square
     * 
     * @return Formatted value
     */
    final static String toString(ChessSquare square) {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(toString(square.getColor()));
        strBuilder.append(" : ");
        strBuilder.append(toString(square.getPosition()));
        return strBuilder.toString();
    }



    /**
     * Return formatted value from {@link ChessMovement}
     * 
     * @param movement
     *            Movement
     * 
     * @return Formatted value
     */
    final static String toString(ChessMovement movement) {
        StringBuilder strBuilder = new StringBuilder();
        Iterator<ChessSquare> itSquares = movement.getSquares().iterator();
        while (itSquares.hasNext()) {
            ChessSquare square = itSquares.next();
            strBuilder.append(toString(square));
            if (itSquares.hasNext()) {
                strBuilder.append(" => ");
            }
        }
        return strBuilder.toString();
    }



    /**
     * Return formatted value from {@link ChessPiece}
     * 
     * @param piece
     *            Piece
     * 
     * @return Formatted value
     */
    final static String toString(ChessPiece piece) {
        String result = "";
        switch (piece.getType()) {
            case KING :
                result += "R";
                break;

            case QUEEN :
                result += "Q";
                break;

            case ROOK :
                result += "T";
                break;

            case KNIGHT :
                result += "C";
                break;

            case BISHOP :
                result += "F";
                break;

            case PAWN :
                result += "P";
                break;
        }
        result += (piece.getColor() == ChessColor.WHITE) ? "W" : "B";
        return result;
    }



    /**
     * Return formatted value from {@link ChessPlayer}
     * 
     * @param player
     *            Player
     * 
     * @return Formatted value
     */
    final static String toString(ChessPlayer player) {
        return (player.getColor() == ChessColor.WHITE) ? "PW" : "PB";
    }



    /**
     * Return formatted value from {@link User}
     * 
     * @param user
     *            User
     * 
     * @return Formatted value
     */
    final static String toString(User user) {
        return user.getId();
    }
}
