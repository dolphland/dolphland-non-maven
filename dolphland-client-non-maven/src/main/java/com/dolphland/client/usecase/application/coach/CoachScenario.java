package com.dolphland.client.usecase.application.coach;

import com.dolphland.client.usecase.application.AbstractApplicationScenario;
import com.dolphland.core.ws.data.ApplicationId;

/**
 * Application's scenario for coach
 * 
 * @author JayJay
 * 
 */
public class CoachScenario extends AbstractApplicationScenario {

    // View
    private CoachGoalsView view;



    /**
     * Constructor with application's id
     * 
     * @param applicationId
     *            Application's id
     */
    public CoachScenario(ApplicationId applicationId) {
        super(applicationId);
        view = new CoachGoalsView();
    }



    @Override
    protected void doEnter(Object param) {
        getWorkPane().setView(view);
    }
}
