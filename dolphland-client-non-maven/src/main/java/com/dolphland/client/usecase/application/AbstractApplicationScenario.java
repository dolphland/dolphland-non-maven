package com.dolphland.client.usecase.application;

import com.dolphland.client.util.application.components.FwkScenario;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.flip.FlipApplicationLoader;
import com.dolphland.core.ws.data.ApplicationId;

/**
 * Scenario for an application
 * 
 * @author JayJay
 * 
 */
public abstract class AbstractApplicationScenario extends FwkScenario implements FlipApplicationLoader {

    private ApplicationId applicationId;
    private boolean applicationLoaded;



    /**
     * Constructor with application's id
     * 
     * @param applicationId
     *            Application's id
     */
    public AbstractApplicationScenario(ApplicationId applicationId) {
        AssertUtil.notNull(applicationId, "ApplicationId is null !");
        this.applicationId = applicationId;
    }



    @Override
    protected void doEnter(Object param) {
    }



    @Override
    protected void doLeave() {
        // Application is no more loaded if application is end
        setApplicationLoaded(false);
    }



    /**
     * @return the application's id
     */
    public ApplicationId getApplicationId() {
        return applicationId;
    }



    @Override
    public boolean isApplicationLoaded() {
        return applicationLoaded;
    }



    /**
     * Set if application is loaded
     * 
     * @param applicationLoaded
     *            True if application is loaded
     */
    protected void setApplicationLoaded(boolean applicationLoaded) {
        this.applicationLoaded = applicationLoaded;
    }
}
