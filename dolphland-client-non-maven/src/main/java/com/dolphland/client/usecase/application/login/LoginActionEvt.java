package com.dolphland.client.usecase.application.login;

import com.dolphland.client.util.action.UIActionEvt;

/**
 * Action context for Login
 * 
 * @author JayJay
 * 
 */
public class LoginActionEvt extends UIActionEvt {

    private String userId;
    private String userPassword;



    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }



    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }



    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }



    /**
     * @param userPassword
     *            the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

}
