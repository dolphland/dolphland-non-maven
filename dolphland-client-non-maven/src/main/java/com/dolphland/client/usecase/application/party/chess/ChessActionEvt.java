package com.dolphland.client.usecase.application.party.chess;

import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.core.ws.data.ChessBoard;
import com.dolphland.core.ws.data.ChessColor;
import com.dolphland.core.ws.data.ChessPiece;
import com.dolphland.core.ws.data.ChessPosition;

/**
 * Action for chess
 * 
 * @author p-Jeremy.Scafi
 * 
 */
public class ChessActionEvt extends UIActionEvt {

    private ChessBoard board;
    private ChessPiece piece;
    private ChessPosition position;
    private ChessColor colorPlayer;



    /**
     * @return the board
     */
    public ChessBoard getBoard() {
        return board;
    }



    /**
     * @param board
     *            the board to set
     */
    public void setBoard(ChessBoard board) {
        this.board = board;
    }



    /**
     * @return the piece
     */
    public ChessPiece getPiece() {
        return piece;
    }



    /**
     * @param piece
     *            the piece to set
     */
    public void setPiece(ChessPiece piece) {
        this.piece = piece;
    }



    /**
     * @return the position
     */
    public ChessPosition getPosition() {
        return position;
    }



    /**
     * @param position
     *            the position to set
     */
    public void setPosition(ChessPosition position) {
        this.position = position;
    }



    /**
     * @return the colorPlayer
     */
    public ChessColor getColorPlayer() {
        return colorPlayer;
    }



    /**
     * @param colorPlayer
     *            the colorPlayer to set
     */
    public void setColorPlayer(ChessColor colorPlayer) {
        this.colorPlayer = colorPlayer;
    }

}
