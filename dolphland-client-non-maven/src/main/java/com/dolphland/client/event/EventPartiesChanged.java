package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;

/**
 * Event for parties changed
 * 
 * @author JayJay
 * 
 */
public class EventPartiesChanged extends UIEvent {

    public final static EventDestination DEST = new EventDestination();



    /**
     * Constructor
     */
    public EventPartiesChanged() {
    	super(DEST);
    }
}
