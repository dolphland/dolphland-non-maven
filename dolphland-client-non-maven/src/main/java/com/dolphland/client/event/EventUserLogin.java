package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;
import com.dolphland.core.ws.data.User;

/**
 * Event for user login
 * 
 * @author JayJay
 * 
 */
public class EventUserLogin extends UIEvent {

    public final static EventDestination DEST = new EventDestination();
    
    private User user;
    
    private String clientId;



    /**
     * Constructor with user and client id
     * 
     * @param user User
     * @param clientId Client id
     */
    public EventUserLogin(User user, String clientId) {
        super(DEST);
        this.user = user;
        this.clientId = clientId;
    }
    
    
    
    /**
     * Get user
     * 
     * @return User
     */
    public User getUser() {
        return user;
    }
    
    
    
    /**
     * Get client id
     * 
     * @return Client id
     */
    public String getClientId() {
        return clientId;
    }
}
