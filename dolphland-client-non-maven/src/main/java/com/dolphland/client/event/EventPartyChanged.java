package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;
import com.dolphland.core.ws.data.Party;

/**
 * Event for party changed
 * 
 * @author JayJay
 * 
 *         P : type of the party's representation
 */
public class EventPartyChanged<P> extends UIEvent {

    public final static EventDestination DEST = new EventDestination();

    private Party party;

    private P partyRepresentation;



    /**
     * Constructor
     */
    public EventPartyChanged(Party party, P partyRepresentation) {
        super(DEST);
        this.party = party;
        this.partyRepresentation = partyRepresentation;
    }



    /**
     * @return the party
     */
    public Party getParty() {
        return party;
    }



    /**
     * @return the partyRepresentation
     */
    public P getPartyRepresentation() {
        return partyRepresentation;
    }

}
