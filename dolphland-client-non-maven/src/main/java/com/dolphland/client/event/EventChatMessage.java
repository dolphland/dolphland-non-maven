package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.User;

/**
 * Event for party's chat message
 * 
 * @author JayJay
 * 
 */
public class EventChatMessage extends UIEvent {

    public final static EventDestination DEST = new EventDestination();

    // Content
    private String content;
    
    // Sender
    private User sender;
    
    // Party
    private Party party;



    /**
     * Constructor with content and sender
     * 
     * @param content
     *            Content
     * @param sender
     *            Sender
     */
    public EventChatMessage(String content, User sender) {
    	this(content, sender, null);
    }



    /**
     * Constructor with content, sender and party
     * 
     * @param content
     *            Content
     * @param sender
     *            Sender
     * @param party
     *            Party
     */
    public EventChatMessage(String content, User sender, Party party) {
    	super(DEST);
        this.content = content;
        this.sender = sender;
        this.party = party;
    }



	/**
	 * Get content
	 * 
	 * @return Content
	 */
	public String getContent() {
		return content;
	}    
	
	
	
	/**
	 * Get sender
	 * 
	 * @return Sender
	 */
	public User getSender() {
		return sender;
	}
	
	
	
	/**
	 * Get party
	 * 
	 * @return Party
	 */
	public Party getParty() {
		return party;
	}
}
