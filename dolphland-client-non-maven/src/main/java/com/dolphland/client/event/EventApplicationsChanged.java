package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;

/**
 * Event for applications changed
 * 
 * @author JayJay
 * 
 */
public class EventApplicationsChanged extends UIEvent {

    public final static EventDestination DEST = new EventDestination();



    /**
     * Constructor
     */
    public EventApplicationsChanged() {
    	super(DEST);
    }
}
