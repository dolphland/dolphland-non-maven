package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;
import com.dolphland.core.ws.data.Party;
import com.dolphland.core.ws.data.User;

/**
 * Event for party joined
 * 
 * @author JayJay
 * 
 */
public class EventPartyJoined extends UIEvent {

    public final static EventDestination DEST = new EventDestination();
    
    private Party party;
    
    private User user;



    /**
     * Constructor with party and user
     * 
     * @param party Party
     * @param user User
     */
    public EventPartyJoined(Party party, User user) {
    	super(DEST);
    	this.party = party;
    	this.user = user;
    }
    
    
    
    /**
     * Get party
     * 
     * @return Party
     */
    public Party getParty() {
		return party;
	}
    
    
    
    /**
     * Get user
     * 
     * @return User
     */
    public User getUser() {
		return user;
	}
}
