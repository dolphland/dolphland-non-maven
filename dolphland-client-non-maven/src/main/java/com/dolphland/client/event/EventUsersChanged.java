package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;

/**
 * Event for users changed
 * 
 * @author JayJay
 * 
 */
public class EventUsersChanged extends UIEvent {

    public final static EventDestination DEST = new EventDestination();



    /**
     * Constructor
     */
    public EventUsersChanged() {
    	super(DEST);
    }
}
