package com.dolphland.client.event;

import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.UIEvent;
import com.dolphland.core.ws.data.Party;

/**
 * Event for party finished
 * 
 * @author JayJay
 * 
 */
public class EventPartyFinished extends UIEvent {

    public final static EventDestination DEST = new EventDestination();
    
    private Party party;



    /**
     * Constructor with party
     * 
     * @param party Party
     */
    public EventPartyFinished(Party party) {
    	super(DEST);
    	this.party = party;
    }
    
    
    
    /**
     * Get party
     * 
     * @return Party
     */
    public Party getParty() {
		return party;
	}
}
