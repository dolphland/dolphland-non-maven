package com.dolphland.client.main;

import org.apache.log4j.Logger;

import com.dolphland.client.context.ContextClient;
import com.dolphland.client.service.SVFactory;
import com.dolphland.client.task.proxy.TaskProxyManager;
import com.dolphland.client.task.relayer.TaskRequesterRelayer;
import com.dolphland.client.usecase.desktop.DesktopScenario;
import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.startup.UIApplication;
import com.dolphland.client.util.application.startup.UIApplicationContext;
import com.dolphland.client.util.conf.FwkBoot;
import com.dolphland.client.util.conf.FwkBootStep;
import com.dolphland.client.util.event.CloseAppRequestEvt;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.FwkEvents;
import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.i18n.MessageFormater;

/**
 * Application Loader
 * 
 * @author JayJay
 * 
 */
public class App extends UIApplication {

    private static final MessageFormater FMT = MessageFormater.getFormater(App.class);

    private static final Logger LOG = Logger.getLogger(App.class);
    
    private TaskProxyManager taskProxyLoader;
    
    private TaskRequesterRelayer taskRelayer;



    /**
     * Default constructor
     */
    public App() {
        EDT.subscribe(new AppEventWatcher()).forEvents(FwkEvents.CLOSE_APP_REQUEST_EVT);
    }



    @Override
    public MasterFrame createMasterFrame(UIApplicationContext ctx) {
        return new AppFrame();
    }



    @Override
    public void init(UIApplicationContext ctx) {
        LOG.info("Starting Dolphland client...");
        
        String label = null;

        label = FMT.format("App.RS_START_PROXY_MANAGER"); //$NON-NLS-1$
        ctx.addBootStep(new FwkBootStep(label) { //$NON-NLS-1$

            @Override
            protected void doExecute(FwkBoot booter) {
                taskProxyLoader = new TaskProxyManager();
                taskProxyLoader.start();
            }
        });

        label = FMT.format("App.RS_START_CONTEXT"); //$NON-NLS-1$
        ctx.addBootStep(new FwkBootStep(label) { //$NON-NLS-1$

            @Override
            protected void doExecute(FwkBoot booter) {
                ContextClient.start();
            }
        });

        label = FMT.format("App.RS_START_REQUESTER_RELAYER"); //$NON-NLS-1$
        ctx.addBootStep(new FwkBootStep(label) { //$NON-NLS-1$

            @Override
            protected void doExecute(FwkBoot booter) {
                taskRelayer = new TaskRequesterRelayer();
                taskRelayer.start();
            }
        });
    }



    @Override
    public void startup(UIApplicationContext ctx) {
        DesktopScenario scenario = new DesktopScenario();
        scenario.setWorkPane(ctx.getMasterFrame().getWorkPane());
        scenario.enter();
    }
    
    private class AppEventWatcher extends DefaultEventSubscriber {

        @SuppressWarnings("unused")
        public void eventReceived(CloseAppRequestEvt e) {
            LOG.info("Stopping Dolphland client...");

            try {
                // Logout if necessary
                SVFactory.createSVUser().logout(true);
            } catch (Exception exc) {
                LOG.error("Error during logout", exc);
            }
            
            try {
                LOG.info(FMT.format("App.RS_STOP_REQUESTER_RELAYER"));
                taskRelayer.stop();
            } catch (Exception exc) {
                throw new AppInfrastructureException("Error during stopping requester relayer", exc);
            }

            try {
                LOG.info(FMT.format("App.RS_STOP_CONTEXT"));
                ContextClient.stop();
            } catch (Exception exc) {
                throw new AppInfrastructureException("Error during stopping context", exc);
            }
            
            try {
                LOG.info(FMT.format("App.RS_STOP_PROXY_MANAGER"));
                taskProxyLoader.stop();
            } catch (Exception exc) {
                throw new AppInfrastructureException("Error during stopping proxy loader", exc);
            }
        }
    }
}