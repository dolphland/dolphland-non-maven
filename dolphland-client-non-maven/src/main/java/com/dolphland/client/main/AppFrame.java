package com.dolphland.client.main;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import com.dolphland.client.AppConstants;
import com.dolphland.client.util.application.components.FwkMasterFrame;
import com.dolphland.client.util.application.components.MessageBox;
import com.dolphland.client.util.conf.CEnvironement;
import com.dolphland.client.util.conf.FwkServiceLocator;
import com.dolphland.client.util.event.CloseAppRequestEvt;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.version.VersionUtil;

public class AppFrame extends FwkMasterFrame {

    private static final MessageFormater FMT = MessageFormater.getFormater(AppFrame.class);

    private TrayIcon trayIcon;



    public AppFrame() {
        // Fixe le titre
        setTitle("Dolphland");

        // Fixe l'ic�ne
        setIconImage(ImageUtil.getImage(AppConstants.PATH_IMAGE_ICON_DOLPHLAND));

        // Cr�ation du menu de la notification
        PopupMenu trayMenu = new PopupMenu();
        MenuItem menuShow = new MenuItem("Afficher");
        menuShow.addActionListener(new ShowListener());
        trayMenu.add(menuShow);
        MenuItem menuExit = new MenuItem("Quitter");
        menuExit.addActionListener(new ExitListener());
        trayMenu.add(menuExit);

        // Cr�ation de la notification
        trayIcon = new TrayIcon(getIconImage(), getTitle(), trayMenu);
        trayIcon.addActionListener(new ShowListener());

        // Redimensionnement automatique de l'image
        trayIcon.setImageAutoSize(true);

        // Gestion de l'ic�ne de notification
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowIconified(WindowEvent e) {
                try {
                    // Si la notification est autoris�e
                    if (SystemTray.isSupported()) {
                        // Ajout de la notification
                        SystemTray.getSystemTray().add(trayIcon);

                        // Cache l'application
                        setVisible(false);
                    }
                } catch (AWTException e1) {
                    throw new AppInfrastructureException("Erreur d'ajout de la notification");
                }
            }
        });
    }
    
    
    
    @Override
    protected MessageBox createMessageBox() {
        MessageBox out = new MessageBox(this, ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_DOLPHLAND));
        return out;
    }



    @Override
    protected String getVersion() {
        return VersionUtil.getVersion();
    }



    @Override
    protected void doInit(String[] args) {
        CEnvironement env = FwkServiceLocator.getInstance().getEnvironment();
        addInfoAPropos(FMT.format("AppFrame.RS_ENV"), env.getId()); //$NON-NLS-1$
    }

    private class ShowListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            setVisible(true);
            setExtendedState(JFrame.NORMAL);
            SystemTray.getSystemTray().remove(trayIcon);
        }
    }

    private class ExitListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            SystemTray.getSystemTray().remove(trayIcon);
            CloseAppRequestEvt evt = new CloseAppRequestEvt();
            EDT.postEvent(evt);
        }
    }
}
