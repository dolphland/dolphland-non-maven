package com.dolphland.client;

import com.dolphland.client.util.conf.FwkServiceLocator;

/**
 * Constants used by client
 * 
 * @author JayJay
 * 
 */
public class AppConstants {

    // Paths for background images used by application
    public final static String PATH_IMAGE_BACKGROUND_LOGIN = "/images/background/background_login.png";
    public final static String PATH_IMAGE_BACKGROUND_DESKTOP = "/images/background/background_desktop.png";
    public final static String PATH_IMAGE_BACKGROUND_PARTY = "/images/background/background_gray_232_164.png";

    // Paths for icons
    public final static String PATH_IMAGE_ICON_LOGIN = "/images/icons/icon_login.png";
    public final static String PATH_IMAGE_ICON_LOGIN_HOVERED = "/images/icons/icon_login_hovered.png";
    public final static String PATH_IMAGE_ICON_LOGOUT = "/images/icons/icon_logout.png";
    public final static String PATH_IMAGE_ICON_LOGOUT_HOVERED = "/images/icons/icon_logout_hovered.png";
    public final static String PATH_IMAGE_ICON_FLIP = "/images/icons/icon_flip.png";
    public final static String PATH_IMAGE_ICON_FLIP_HOVERED = "/images/icons/icon_flip_hovered.png";
    public final static String PATH_IMAGE_ICON_DOLPHLAND = "/images/icons/icon_dolphland_50_50.png";
    public final static String PATH_IMAGE_ICON_CONFIG_BIG = "/images/icons/icon_config_72_72.png";
    public final static String PATH_IMAGE_ICON_CONFIG_SMALL = "/images/icons/icon_config_20_20.png";
    public final static String PATH_IMAGE_ICON_CHESS_BIG = "/images/icons/icon_chess_72_72.png";
    public final static String PATH_IMAGE_ICON_CHESS_SMALL = "/images/icons/icon_chess_20_20.png";
    public final static String PATH_IMAGE_ICON_DRAUGHT_BIG = "/images/icons/icon_draught_72_72.png";
    public final static String PATH_IMAGE_ICON_DRAUGHT_SMALL = "/images/icons/icon_draught_20_20.png";
    public final static String PATH_IMAGE_ICON_BELOTE_BIG = "/images/icons/icon_belote_72_72.png";
    public final static String PATH_IMAGE_ICON_BELOTE_SMALL = "/images/icons/icon_belote_20_20.png";
    public final static String PATH_IMAGE_ICON_COACH_BIG = "/images/icons/icon_coach_72_72.png";
    public final static String PATH_IMAGE_ICON_COACH_SMALL = "/images/icons/icon_coach_20_20.png";
    public final static String PATH_IMAGE_ICON_WEB_BIG = "/images/icons/icon_web_72_72.png";
    public final static String PATH_IMAGE_ICON_WEB_SMALL = "/images/icons/icon_web_20_20.png";
    public final static String PATH_IMAGE_ICON_DELETE = "/images/icons/icon_delete_17_17.png";
    public final static String PATH_IMAGE_ICON_EXPLORER_BIG = "/images/icons/icon_explorer_72_72.png";
    public final static String PATH_IMAGE_ICON_EXPLORER_SMALL = "/images/icons/icon_explorer_20_20.png";
    public final static String PATH_IMAGE_ICON_BUTTON_RED = "/images/icons/icon_button_red_15_15.png";
    public final static String PATH_IMAGE_ICON_BUTTON_ORANGE = "/images/icons/icon_button_orange_15_15.png";
    public final static String PATH_IMAGE_ICON_BUTTON_GREEN = "/images/icons/icon_button_green_15_15.png";
    public final static String PATH_IMAGE_ICON_STAR_RED = "/images/icons/icon_star_red_15_15.png";
    public final static String PATH_IMAGE_ICON_STAR_ORANGE = "/images/icons/icon_star_orange_15_15.png";
    public final static String PATH_IMAGE_ICON_STAR_GREEN = "/images/icons/icon_star_green_15_15.png";
    public final static String PATH_IMAGE_ICON_PLUS_BLACK = "/images/icons/icon_plus_black_15_15.png";

    // Paths for desktop's images
    public final static String PATH_IMAGE_DESKTOP_OFFLINE_USER = "/images/desktop/desktop_offline_user_128_128.png";

    // Web's host
    public final static String WEB_HOST = FwkServiceLocator.getInstance().getEnvironment().getString("web.host");

    // Web's port
    public final static int WEB_PORT = FwkServiceLocator.getInstance().getEnvironment().getInt("web.port");

    // Url for images
    public final static String URL_IMAGE_AVATAR = "http://" + WEB_HOST + ":" + WEB_PORT + "/dolphland2/images/avatar/";
    public final static String URL_IMAGE_BACKGROUND = "http://" + WEB_HOST + ":" + WEB_PORT + "/dolphland2/images/background/";

    // Input width
    public final static int WIDTH_PASSWORD = 20;
    public final static int WIDTH_EMAIL = 50;
}
