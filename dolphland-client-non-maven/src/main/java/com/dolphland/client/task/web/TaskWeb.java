package com.dolphland.client.task.web;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.service.SVFactory;
import com.dolphland.client.task.proxy.TaskProxyManager;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.task.Task;
import com.dolphland.client.util.task.TaskContext;

/**
 * Task web
 * 
 * @author JayJay
 * 
 */
public class TaskWeb extends Task {

    private static final Logger LOG = Logger.getLogger(TaskWeb.class);

    private static final int PORT = 2000;

    // For auto-numbering thread
    private static int threadInitNumber;
    private static synchronized int nextThreadNum() {
        return threadInitNumber++;
    }
    
    // Web server socket
    private ServerSocket webServerSocket = null;



    /**
     * Default constructor
     */
    public TaskWeb() {
        super("TaskWeb");
    }



    @Override
    protected void init(TaskContext ctx) {
        super.init(ctx);
        try {
            webServerSocket = new ServerSocket(PORT);
        } catch (IOException exc) {
            LOG.error("Error when starting server socket", exc);
        }
        loopGetContent();
    }



    @Override
    protected void destroy(TaskContext ctx) {
        super.destroy(ctx);
        try {
            webServerSocket.close();
        } catch (IOException exc) {
            LOG.error("Error when stopping server socket", exc);
        }
    }
    
    
    private void loopGetContent(){
        while (!webServerSocket.isClosed()) {
            try {
                Socket webCustomerSocket = webServerSocket.accept();
                new ThreadGetContent(webCustomerSocket).start();
            } catch (Exception exc) {
                if(!webServerSocket.isClosed()){
                    LOG.error("Error when starting thread to get content", exc);
                }
            }
        }
    }
    
    public static void main(String[] args) {
        EDT.start();
        new TaskProxyManager().start();
        new TaskWeb().start();
        
        try {
            Runtime.getRuntime().exec("C:\\Documents and Settings\\JayJay\\Local Settings\\Application Data\\Google\\Chrome\\Application\\chrome.exe http://www.google.fr/ --proxy-server=localhost:" + PORT); 
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    


    /**
     * Convert ascii to code
     * 
     * @param car
     *            Ascii
     * @return Code
     */
    private int asciiToCode(char car) {
        byte[] codes = new String("" + car).getBytes();
        return ((int) codes[0]) & 0x000000FF;
    }



    /**
     * Convert code to ascii
     * 
     * @param code
     *            Code
     * @return Ascii
     */
    private String codeToAscii(int code) {
        byte b = (byte) (code & 0x000000FF);
        byte[] codes = new byte[] { b };
        return new String(codes);
    }

    /**
     * Thread that get content
     * 
     * @author JayJay
     * 
     */
    private class ThreadGetContent extends Thread {
        
        private final static int MAX_TRY = 5;
        private final static long TIMEOUT_MS = 10000;
        private final static long SLEEP_MS = 100;
        private final static String PARAMS_LIST_NAME = "params";
        private final static String PARAMS_LIST_DELIMITER = ";";
        private final static String PARAM_DELIMITER = "?";
        private final static String PARAM_EQUALS = "=";
        private final static String PARAM_ENCODING = "Accept-Encoding:";
        private final static String ENCRYPTION_KEY = "JayJayIsBack59";
        private final static String BYTE_DELIMITER = "|";
        private final static String REQUEST_LINE_DELIMITER_1 = "\r";
        private final static String REQUEST_LINE_DELIMITER_2 = "\n";
        private final static String REQUEST_DELIMITER = " ";

        private Socket webCustomerSocket;
        private InputStream customerIn;
        private OutputStream customerOut;
        private InputStream serverIn;
        private OutputStream serverOut;



        /**
         * Constructor with web customer socket
         * 
         * @param webCustomerSocket
         *            Web socket
         */
        public ThreadGetContent(Socket webCustomerSocket) {
            super("ThreadGetContent" + nextThreadNum());
            this.webCustomerSocket = webCustomerSocket;
        }
        
        private InputStream getCustomerInputStream() throws IOException{
            return webCustomerSocket.getInputStream();
        }

        private OutputStream getCustomerOutputStream() throws IOException{
            return new BufferedOutputStream(webCustomerSocket.getOutputStream());
        }

        private InputStream getServerInputStream() throws IOException{
            return new ServiceWebInputStream();
        }

        private OutputStream getServerOutputStream() throws IOException{
            return new BufferedOutputStream(new ServiceWebOutputStream((((ServiceWebInputStream)serverIn)).serverResponse));
        }

        
        @Override
        public void run() {
            long time0 = new Date().getTime();
            long time1 = new Date().getTime();

            try {
                int r0 = -1;
                int r1 = -1;
                int ch = -1;
                int i = -1;
                customerIn = getCustomerInputStream();
                serverIn = getServerInputStream();
                serverOut = getServerOutputStream();
                while (r0 != 0 || r1 != 0 || (time1 - time0) <= TIMEOUT_MS) {
                    String customerRequest = new String();
                    while ((r0 = customerIn.available()) > 0) {
                        LOG.debug("<<<" + r0 + " bytes du client");
                        for (i = 0; i < r0; i++) {
                            ch = customerIn.read();
                            if (ch != -1) {
                                serverOut.write(ch);
                                customerRequest += codeToAscii(ch);
                            } else {
                                LOG.debug("Flux du client ferm�");
                            }
                        }
                        time0 = new Date().getTime();
                        serverOut.flush();
                    }
                    if(!StrUtil.isEmpty(customerRequest)){
                        LOG.debug("Requ�te client :\n" + customerRequest);
                    }
                    String serverRequest = new String();
                    //String trace = new String();
                    String previousCharacter = "";
                    while ((r1 = serverIn.available()) > 0) {
                        List<Integer> serverResponseBytes= new ArrayList<Integer>();
                        LOG.debug(">>>" + r1 + " bytes du serveur");
                        for (i = 0; i < r1; i++) {
                            ch = serverIn.read();
                            if (ch != -1) {
                                String currentCharacter = codeToAscii(ch);
                                /*if(REQUEST_LINE_DELIMITER_2.equals(currentCharacter) && !REQUEST_LINE_DELIMITER_1.equals(previousCharacter)){
                                    continue;
                                }*/
                                //trace += new Integer(ch).toString()+";";
                                serverResponseBytes.add(ch);
                                //customerOut.write(ch);
                                previousCharacter = currentCharacter;
                                serverRequest += codeToAscii(ch);
                            } else {
                                LOG.debug("Flux du serveur ferm�");
                            }
                        }
                        time0 = new Date().getTime();
                        customerOut = getCustomerOutputStream();
                        for(int aByte : serverResponseBytes){
                            try{
                                customerOut.write(aByte);
                            }catch(Exception exc){
                                System.out.println("\n\n" + aByte + "\n\n");
                            }
                        }
                        customerOut.flush();
                    }
                    if(!StrUtil.isEmpty(serverRequest)){
                        LOG.debug("R�ponse serveur :\n" + serverRequest);
                    }
                    
                    if (r0 == 0 && r1 == 0) {
                        time1 = new Date().getTime();
                        sleep(SLEEP_MS);
                    }
                }                
            } catch (Exception exc) {
                LOG.error("Erreur de traitement de la requ�te Web : " + exc.getMessage(), exc);
            } finally {
                try {
                    if (customerIn != null) {
                        customerIn.close();
                    }
                } catch (Exception exc) {
                    LOG.error("Erreur de fermeture du flux d'entr�e client", exc);
                }

                try {
                    if (customerOut != null) {
                        customerOut.close();
                    }
                } catch (Exception exc) {
                    LOG.error("Erreur de fermeture du flux de sortie client", exc);
                }

                try {
                    if (serverIn != null) {
                        serverIn.close();
                    }
                } catch (Exception exc) {
                    LOG.error("Erreur de fermeture du flux d'entr�e serveur", exc);
                }

                try {
                    if (serverOut != null) {
                        serverOut.close();
                    }
                } catch (Exception exc) {
                    LOG.error("Erreur de fermeture du flux de sortie serveur", exc);
                }

                try {
                    if (webCustomerSocket != null) {
                        webCustomerSocket.close();
                    }
                } catch (Exception exc) {
                    LOG.error("Erreur de fermeture de la socket Web client", exc);
                }

                try {
                    if (webServerSocket != null) {
                        webServerSocket.close();
                    }
                } catch (Exception exc) {
                    LOG.error("Erreur de fermeture de la socket Web serveur", exc);
                }

                try {
                    time1 = new Date().getTime();
                    System.out.println("Fin de traitement de la socket apr�s " + (time1 - time0) + " ms");
                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }

            }
        }
    }

    
    
    private class ServiceWebInputStream extends InputStream {
        
        private List<Integer> serverResponse;
        
        
        public ServiceWebInputStream() {
            serverResponse = new ArrayList<Integer>();
        }
        
        @Override
        public int read() throws IOException {
            if(serverResponse.isEmpty()){
                return 0;
            }
            return serverResponse.remove(0);
        }
        
        @Override
        public int available() throws IOException {
            if(serverResponse.isEmpty()){
                return 0;
            }
            return 1;
        }
        
        @Override
        public void close() throws IOException {
            super.close();
        }
    }
        
    
    
    private class ServiceWebOutputStream extends OutputStream {

        private List<Integer> customerRequest = new ArrayList<Integer>();
        private List<Integer> serverResponse;
        private String socketId = new Long(new Date().getTime()).toString();
        
        public ServiceWebOutputStream(List<Integer> serverResponse) {
            this.serverResponse = serverResponse;
        }
        
        @Override
        public void write(int b) throws IOException {
            customerRequest.add(b);
        }

        @Override
        public void flush() throws IOException {
            super.flush();
            
            if(!customerRequest.isEmpty()){
                serverResponse.addAll(SVFactory.createSVWeb().getContent(socketId, customerRequest));
            }
        }
    }
}
