package com.dolphland.client.task.relayer;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import org.apache.log4j.Logger;

import com.dolphland.client.service.SVFactory;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.Event;
import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.Schedule;
import com.dolphland.client.util.stream.StreamUtil;
import com.dolphland.client.util.task.Task;
import com.dolphland.client.util.task.TaskContext;

/**
 * Task relayer requester
 * 
 * @author JayJay
 * 
 */
public class TaskRequesterRelayer extends Task {

    private static final Logger LOG = Logger.getLogger(TaskRequesterRelayer.class);

    // Time between two integrations
    private static final int SLEEPING_TIME_MS = 1000;



    /**
     * Default constructor
     */
    public TaskRequesterRelayer() {
        super("TaskRequesterRelayer");
        registerEvent(RelayerEvent.EVENT_DESTINATION);
    }



    @Override
    protected void init(TaskContext ctx) {
        super.init(ctx);

        EDT.schedule(Schedule.event(new RelayerEvent()).destination(getEventDestination()).startDate(new Date().getTime() + SLEEPING_TIME_MS));
    }



    /**
     * Relay events
     * 
     * @param e
     *            Event
     * 
     * @throws InterruptedException
     *             When interruption
     */
    protected void handleEvent(RelayerEvent e) throws InterruptedException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("handleEvent(): Start relaying event...");
        }

        ObjectOutputStream out = null;
        ObjectInputStream in = null;

        try {
            // Relay
            SVFactory.createSVEvent().relay();
        } catch (Exception exc) {
            LOG.error("Error request relayer", exc);
        } finally {
            StreamUtil.safeClose(in);
            StreamUtil.safeClose(out);
        }

        EDT.schedule(Schedule.event(new RelayerEvent()).destination(getEventDestination()).startDate(new Date().getTime() + SLEEPING_TIME_MS));

        if (LOG.isDebugEnabled()) {
            LOG.debug("handleEvent(): End of relaying event...");
        }
    }

    /**
     * Event called to relay events
     * 
     * @author JayJay
     * 
     */
    private static class RelayerEvent extends Event {

        private static final EventDestination EVENT_DESTINATION = new EventDestination();



        public RelayerEvent() {
            super(EVENT_DESTINATION);
        }
    }
}
