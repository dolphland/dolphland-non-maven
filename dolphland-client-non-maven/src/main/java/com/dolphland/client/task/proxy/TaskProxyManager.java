package com.dolphland.client.task.proxy;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.util.task.Task;
import com.dolphland.client.util.task.TaskContext;

/**
 * Task to load proxy
 * 
 * @author JayJay
 * 
 */
public class TaskProxyManager extends Task {

    private static final Logger LOG = Logger.getLogger(TaskProxyManager.class);

    // Proxy ids
    private final static int ID_NO_PROXY = 0;
    private final static int ID_PROXY_VALLOUREC = 1;

    // Proxy id argument
    private final static String ARG_PROXY_ID = "-Dhttp.default.proxy.id=";



    /**
     * Default constructor
     */
    public TaskProxyManager() {
        super("TaskProxyLoader");
    }



    @Override
    protected void init(TaskContext ctx) {
        super.init(ctx);

        try {
            Integer proxyId = null;

            RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
            List<String> args = runtimeMxBean.getInputArguments();
            for (String arg : args) {
                if (arg.startsWith(ARG_PROXY_ID)) {
                    proxyId = Integer.parseInt(arg.substring(ARG_PROXY_ID.length()));
                    break;
                }
            }
            if (proxyId != null) {
                switch (proxyId) {
                    case ID_PROXY_VALLOUREC :
                        System.setProperty("http.proxySet", "true");
                        System.setProperty("http.proxyHost", "10.234.89.75");
                        System.setProperty("http.proxyPort", "8822");
                        Authenticator authenticator = new Authenticator() {

                            public PasswordAuthentication getPasswordAuthentication() {
                                return (new PasswordAuthentication("jeremy.scafi", "89lp05".toCharArray()));
                            }
                        };
                        Authenticator.setDefault(authenticator);
                        break;

                    case ID_NO_PROXY:
                    default :
                        // Do nothing
                        break;
                }
            }
        } catch (Exception exc) {
            LOG.error("Error when loading proxy", exc);
        }
    }
}
