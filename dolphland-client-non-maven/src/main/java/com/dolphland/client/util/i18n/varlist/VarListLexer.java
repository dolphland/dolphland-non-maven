// $ANTLR 3.3 Nov 30, 2010 12:45:30
// D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g 2012-08-02 16:11:00

package com.dolphland.client.util.i18n.varlist;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.DFA;
import org.antlr.runtime.EarlyExitException;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.MismatchedSetException;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;


public class VarListLexer extends Lexer {

    public static final int EOF = -1;
    public static final int T__28 = 28;
    public static final int T__29 = 29;
    public static final int INT = 4;
    public static final int STRING = 5;
    public static final int FLOAT = 6;
    public static final int CHAR = 7;
    public static final int DATE = 8;
    public static final int NULL = 9;
    public static final int DecimalLiteral = 10;
    public static final int Identifier = 11;
    public static final int FloatingPointLiteral = 12;
    public static final int CharLiteral = 13;
    public static final int StringLiteral = 14;
    public static final int CAR = 15;
    public static final int DIGIT = 16;
    public static final int HexDigit = 17;
    public static final int Exponent = 18;
    public static final int FloatTypeSuffix = 19;
    public static final int EscapeSequence = 20;
    public static final int UnicodeEscape = 21;
    public static final int Letter = 22;
    public static final int JavaIDDigit = 23;
    public static final int WS = 24;
    public static final int NL = 25;
    public static final int COMMENT = 26;
    public static final int LINE_COMMENT = 27;



    // delegates
    // delegators

    public VarListLexer() {
        ;
    }



    public VarListLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }



    public VarListLexer(CharStream input, RecognizerSharedState state) {
        super(input, state);

    }



    public String getGrammarFileName() {
        return "D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g";
    }



    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:11:5: (
            // 'int' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:11:7:
            // 'int'
            {
                match("int");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "INT"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:12:8: (
            // 'string' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:12:10:
            // 'string'
            {
                match("string");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "STRING"

    // $ANTLR start "FLOAT"
    public final void mFLOAT() throws RecognitionException {
        try {
            int _type = FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:13:7: (
            // 'float' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:13:9:
            // 'float'
            {
                match("float");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "FLOAT"

    // $ANTLR start "CHAR"
    public final void mCHAR() throws RecognitionException {
        try {
            int _type = CHAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:14:6: (
            // 'char' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:14:8:
            // 'char'
            {
                match("char");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "CHAR"

    // $ANTLR start "DATE"
    public final void mDATE() throws RecognitionException {
        try {
            int _type = DATE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:15:6: (
            // 'date' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:15:8:
            // 'date'
            {
                match("date");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "DATE"

    // $ANTLR start "NULL"
    public final void mNULL() throws RecognitionException {
        try {
            int _type = NULL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:16:6: (
            // 'null' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:16:8:
            // 'null'
            {
                match("null");

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "NULL"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:17:7: (
            // ',' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:17:9: ','
            {
                match(',');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:18:7: (
            // '=' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:18:9: '='
            {
                match('=');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "T__29"

    // $ANTLR start "DecimalLiteral"
    public final void mDecimalLiteral() throws RecognitionException {
        try {
            int _type = DecimalLiteral;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:16: (
            // ( '-' )? ( '0' | '1' .. '9' ( '0' .. '9' )* ) )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:18: (
            // '-' )? ( '0' | '1' .. '9' ( '0' .. '9' )* )
            {
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:18:
                // ( '-' )?
                int alt1 = 2;
                int LA1_0 = input.LA(1);

                if ((LA1_0 == '-')) {
                    alt1 = 1;
                }
                switch (alt1) {
                    case 1 :
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:18:
                    // '-'
                    {
                        match('-');

                    }
                        break;

                }

                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:22:
                // ( '0' | '1' .. '9' ( '0' .. '9' )* )
                int alt3 = 2;
                int LA3_0 = input.LA(1);

                if ((LA3_0 == '0')) {
                    alt3 = 1;
                }
                else if (((LA3_0 >= '1' && LA3_0 <= '9'))) {
                    alt3 = 2;
                }
                else {
                    NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                    throw nvae;
                }
                switch (alt3) {
                    case 1 :
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:23:
                    // '0'
                    {
                        match('0');

                    }
                        break;
                    case 2 :
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:29:
                    // '1' .. '9' ( '0' .. '9' )*
                    {
                        matchRange('1', '9');
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:38:
                        // ( '0' .. '9' )*
                        loop2: do {
                            int alt2 = 2;
                            int LA2_0 = input.LA(1);

                            if (((LA2_0 >= '0' && LA2_0 <= '9'))) {
                                alt2 = 1;
                            }

                            switch (alt2) {
                                case 1 :
                                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:127:38:
                                // '0' .. '9'
                                {
                                    matchRange('0', '9');

                                }
                                    break;

                                default :
                                    break loop2;
                            }
                        } while (true);

                    }
                        break;

                }

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "DecimalLiteral"

    // $ANTLR start "Identifier"
    public final void mIdentifier() throws RecognitionException {
        try {
            int _type = Identifier;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:129:11: (
            // ( CAR ) ( CAR | DIGIT )* )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:129:13: (
            // CAR ) ( CAR | DIGIT )*
            {
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:129:13:
                // ( CAR )
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:129:14:
                // CAR
                {
                    mCAR();

                }

                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:129:18:
                // ( CAR | DIGIT )*
                loop4: do {
                    int alt4 = 2;
                    int LA4_0 = input.LA(1);

                    if (((LA4_0 >= '0' && LA4_0 <= '9') || (LA4_0 >= 'A' && LA4_0 <= 'Z') || LA4_0 == '_' || (LA4_0 >= 'a' && LA4_0 <= 'z'))) {
                        alt4 = 1;
                    }

                    switch (alt4) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:
                        {
                            if ((input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z')) {
                                input.consume();

                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }

                        }
                            break;

                        default :
                            break loop4;
                    }
                } while (true);

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "Identifier"

    // $ANTLR start "CAR"
    public final void mCAR() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:132:5: ( (
            // 'a' .. 'z' | 'A' .. 'Z' | '_' ) )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:132:7: (
            // 'a' .. 'z' | 'A' .. 'Z' | '_' )
            {
                if ((input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z')) {
                    input.consume();

                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

            }

        } finally {
        }
    }



    // $ANTLR end "CAR"

    // $ANTLR start "DIGIT"
    public final void mDIGIT() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:135:7: (
            // '0' .. '9' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:135:9: '0'
            // .. '9'
            {
                matchRange('0', '9');

            }

        } finally {
        }
    }



    // $ANTLR end "DIGIT"

    // $ANTLR start "HexDigit"
    public final void mHexDigit() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:138:10: (
            // ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:138:12: (
            // '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )
            {
                if ((input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'F') || (input.LA(1) >= 'a' && input.LA(1) <= 'f')) {
                    input.consume();

                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

            }

        } finally {
        }
    }



    // $ANTLR end "HexDigit"

    // $ANTLR start "FloatingPointLiteral"
    public final void mFloatingPointLiteral() throws RecognitionException {
        try {
            int _type = FloatingPointLiteral;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:5: ( (
            // '-' )? ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( Exponent )? | ( '-'
            // )? '.' ( '0' .. '9' )+ ( Exponent )? | ( '-' )? ( '0' .. '9' )+
            // Exponent | ( '-' )? ( '0' .. '9' )+ )
            int alt16 = 4;
            alt16 = dfa16.predict(input);
            switch (alt16) {
                case 1 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:9:
                // ( '-' )? ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( Exponent )?
                {
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:9:
                    // ( '-' )?
                    int alt5 = 2;
                    int LA5_0 = input.LA(1);

                    if ((LA5_0 == '-')) {
                        alt5 = 1;
                    }
                    switch (alt5) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:9:
                        // '-'
                        {
                            match('-');

                        }
                            break;

                    }

                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:14:
                    // ( '0' .. '9' )+
                    int cnt6 = 0;
                    loop6: do {
                        int alt6 = 2;
                        int LA6_0 = input.LA(1);

                        if (((LA6_0 >= '0' && LA6_0 <= '9'))) {
                            alt6 = 1;
                        }

                        switch (alt6) {
                            case 1 :
                            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:15:
                            // '0' .. '9'
                            {
                                matchRange('0', '9');

                            }
                                break;

                            default :
                                if (cnt6 >= 1)
                                    break loop6;
                                EarlyExitException eee =
                                new EarlyExitException(6, input);
                                throw eee;
                        }
                        cnt6++;
                    } while (true);

                    match('.');
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:30:
                    // ( '0' .. '9' )*
                    loop7: do {
                        int alt7 = 2;
                        int LA7_0 = input.LA(1);

                        if (((LA7_0 >= '0' && LA7_0 <= '9'))) {
                            alt7 = 1;
                        }

                        switch (alt7) {
                            case 1 :
                            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:31:
                            // '0' .. '9'
                            {
                                matchRange('0', '9');

                            }
                                break;

                            default :
                                break loop7;
                        }
                    } while (true);

                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:42:
                    // ( Exponent )?
                    int alt8 = 2;
                    int LA8_0 = input.LA(1);

                    if ((LA8_0 == 'E' || LA8_0 == 'e')) {
                        alt8 = 1;
                    }
                    switch (alt8) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:141:42:
                        // Exponent
                        {
                            mExponent();

                        }
                            break;

                    }

                }
                    break;
                case 2 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:142:9:
                // ( '-' )? '.' ( '0' .. '9' )+ ( Exponent )?
                {
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:142:9:
                    // ( '-' )?
                    int alt9 = 2;
                    int LA9_0 = input.LA(1);

                    if ((LA9_0 == '-')) {
                        alt9 = 1;
                    }
                    switch (alt9) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:142:9:
                        // '-'
                        {
                            match('-');

                        }
                            break;

                    }

                    match('.');
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:142:18:
                    // ( '0' .. '9' )+
                    int cnt10 = 0;
                    loop10: do {
                        int alt10 = 2;
                        int LA10_0 = input.LA(1);

                        if (((LA10_0 >= '0' && LA10_0 <= '9'))) {
                            alt10 = 1;
                        }

                        switch (alt10) {
                            case 1 :
                            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:142:19:
                            // '0' .. '9'
                            {
                                matchRange('0', '9');

                            }
                                break;

                            default :
                                if (cnt10 >= 1)
                                    break loop10;
                                EarlyExitException eee =
                                new EarlyExitException(10, input);
                                throw eee;
                        }
                        cnt10++;
                    } while (true);

                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:142:30:
                    // ( Exponent )?
                    int alt11 = 2;
                    int LA11_0 = input.LA(1);

                    if ((LA11_0 == 'E' || LA11_0 == 'e')) {
                        alt11 = 1;
                    }
                    switch (alt11) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:142:30:
                        // Exponent
                        {
                            mExponent();

                        }
                            break;

                    }

                }
                    break;
                case 3 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:143:9:
                // ( '-' )? ( '0' .. '9' )+ Exponent
                {
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:143:9:
                    // ( '-' )?
                    int alt12 = 2;
                    int LA12_0 = input.LA(1);

                    if ((LA12_0 == '-')) {
                        alt12 = 1;
                    }
                    switch (alt12) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:143:9:
                        // '-'
                        {
                            match('-');

                        }
                            break;

                    }

                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:143:14:
                    // ( '0' .. '9' )+
                    int cnt13 = 0;
                    loop13: do {
                        int alt13 = 2;
                        int LA13_0 = input.LA(1);

                        if (((LA13_0 >= '0' && LA13_0 <= '9'))) {
                            alt13 = 1;
                        }

                        switch (alt13) {
                            case 1 :
                            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:143:15:
                            // '0' .. '9'
                            {
                                matchRange('0', '9');

                            }
                                break;

                            default :
                                if (cnt13 >= 1)
                                    break loop13;
                                EarlyExitException eee =
                                new EarlyExitException(13, input);
                                throw eee;
                        }
                        cnt13++;
                    } while (true);

                    mExponent();

                }
                    break;
                case 4 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:144:9:
                // ( '-' )? ( '0' .. '9' )+
                {
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:144:9:
                    // ( '-' )?
                    int alt14 = 2;
                    int LA14_0 = input.LA(1);

                    if ((LA14_0 == '-')) {
                        alt14 = 1;
                    }
                    switch (alt14) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:144:9:
                        // '-'
                        {
                            match('-');

                        }
                            break;

                    }

                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:144:14:
                    // ( '0' .. '9' )+
                    int cnt15 = 0;
                    loop15: do {
                        int alt15 = 2;
                        int LA15_0 = input.LA(1);

                        if (((LA15_0 >= '0' && LA15_0 <= '9'))) {
                            alt15 = 1;
                        }

                        switch (alt15) {
                            case 1 :
                            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:144:15:
                            // '0' .. '9'
                            {
                                matchRange('0', '9');

                            }
                                break;

                            default :
                                if (cnt15 >= 1)
                                    break loop15;
                                EarlyExitException eee =
                                new EarlyExitException(15, input);
                                throw eee;
                        }
                        cnt15++;
                    } while (true);

                }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "FloatingPointLiteral"

    // $ANTLR start "Exponent"
    public final void mExponent() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:148:10: (
            // ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+ )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:148:12: (
            // 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+
            {
                if (input.LA(1) == 'E' || input.LA(1) == 'e') {
                    input.consume();

                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:148:22:
                // ( '+' | '-' )?
                int alt17 = 2;
                int LA17_0 = input.LA(1);

                if ((LA17_0 == '+' || LA17_0 == '-')) {
                    alt17 = 1;
                }
                switch (alt17) {
                    case 1 :
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:
                    {
                        if (input.LA(1) == '+' || input.LA(1) == '-') {
                            input.consume();

                        }
                        else {
                            MismatchedSetException mse = new MismatchedSetException(null, input);
                            recover(mse);
                            throw mse;
                        }

                    }
                        break;

                }

                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:148:33:
                // ( '0' .. '9' )+
                int cnt18 = 0;
                loop18: do {
                    int alt18 = 2;
                    int LA18_0 = input.LA(1);

                    if (((LA18_0 >= '0' && LA18_0 <= '9'))) {
                        alt18 = 1;
                    }

                    switch (alt18) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:148:34:
                        // '0' .. '9'
                        {
                            matchRange('0', '9');

                        }
                            break;

                        default :
                            if (cnt18 >= 1)
                                break loop18;
                            EarlyExitException eee =
                            new EarlyExitException(18, input);
                            throw eee;
                    }
                    cnt18++;
                } while (true);

            }

        } finally {
        }
    }



    // $ANTLR end "Exponent"

    // $ANTLR start "FloatTypeSuffix"
    public final void mFloatTypeSuffix() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:151:17: (
            // ( 'f' | 'F' | 'd' | 'D' ) )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:151:19: (
            // 'f' | 'F' | 'd' | 'D' )
            {
                if (input.LA(1) == 'D' || input.LA(1) == 'F' || input.LA(1) == 'd' || input.LA(1) == 'f') {
                    input.consume();

                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

            }

        } finally {
        }
    }



    // $ANTLR end "FloatTypeSuffix"

    // $ANTLR start "StringLiteral"
    public final void mStringLiteral() throws RecognitionException {
        try {
            int _type = StringLiteral;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:154:2: (
            // '\"' ( EscapeSequence | ~ ( '\\\\' | '\"' ) )* '\"' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:154:5:
            // '\"' ( EscapeSequence | ~ ( '\\\\' | '\"' ) )* '\"'
            {
                match('\"');
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:154:9:
                // ( EscapeSequence | ~ ( '\\\\' | '\"' ) )*
                loop19: do {
                    int alt19 = 3;
                    int LA19_0 = input.LA(1);

                    if ((LA19_0 == '\\')) {
                        alt19 = 1;
                    }
                    else if (((LA19_0 >= '\u0000' && LA19_0 <= '!') || (LA19_0 >= '#' && LA19_0 <= '[') || (LA19_0 >= ']' && LA19_0 <= '\uFFFF'))) {
                        alt19 = 2;
                    }

                    switch (alt19) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:154:11:
                        // EscapeSequence
                        {
                            mEscapeSequence();

                        }
                            break;
                        case 2 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:154:28:
                        // ~ ( '\\\\' | '\"' )
                        {
                            if ((input.LA(1) >= '\u0000' && input.LA(1) <= '!') || (input.LA(1) >= '#' && input.LA(1) <= '[') || (input.LA(1) >= ']' && input.LA(1) <= '\uFFFF')) {
                                input.consume();

                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }

                        }
                            break;

                        default :
                            break loop19;
                    }
                } while (true);

                match('\"');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "StringLiteral"

    // $ANTLR start "CharLiteral"
    public final void mCharLiteral() throws RecognitionException {
        try {
            int _type = CharLiteral;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:158:2: (
            // '\\'' ( EscapeSequence | ~ ( '\\'' | '\\\\' | '\\r' | '\\n' ) )
            // '\\'' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:158:6:
            // '\\'' ( EscapeSequence | ~ ( '\\'' | '\\\\' | '\\r' | '\\n' ) )
            // '\\''
            {
                match('\'');
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:158:11:
                // ( EscapeSequence | ~ ( '\\'' | '\\\\' | '\\r' | '\\n' ) )
                int alt20 = 2;
                int LA20_0 = input.LA(1);

                if ((LA20_0 == '\\')) {
                    alt20 = 1;
                }
                else if (((LA20_0 >= '\u0000' && LA20_0 <= '\t') || (LA20_0 >= '\u000B' && LA20_0 <= '\f') || (LA20_0 >= '\u000E' && LA20_0 <= '&') || (LA20_0 >= '(' && LA20_0 <= '[') || (LA20_0 >= ']' && LA20_0 <= '\uFFFF'))) {
                    alt20 = 2;
                }
                else {
                    NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                    throw nvae;
                }
                switch (alt20) {
                    case 1 :
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:158:13:
                    // EscapeSequence
                    {
                        mEscapeSequence();

                    }
                        break;
                    case 2 :
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:158:30:
                    // ~ ( '\\'' | '\\\\' | '\\r' | '\\n' )
                    {
                        if ((input.LA(1) >= '\u0000' && input.LA(1) <= '\t') || (input.LA(1) >= '\u000B' && input.LA(1) <= '\f') || (input.LA(1) >= '\u000E' && input.LA(1) <= '&') || (input.LA(1) >= '(' && input.LA(1) <= '[') || (input.LA(1) >= ']' && input.LA(1) <= '\uFFFF')) {
                            input.consume();

                        }
                        else {
                            MismatchedSetException mse = new MismatchedSetException(null, input);
                            recover(mse);
                            throw mse;
                        }

                    }
                        break;

                }

                match('\'');

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "CharLiteral"

    // $ANTLR start "EscapeSequence"
    public final void mEscapeSequence() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:163:5: (
            // '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
            // | UnicodeEscape )
            int alt21 = 2;
            int LA21_0 = input.LA(1);

            if ((LA21_0 == '\\')) {
                int LA21_1 = input.LA(2);

                if ((LA21_1 == '\"' || LA21_1 == '\'' || LA21_1 == '\\' || LA21_1 == 'b' || LA21_1 == 'f' || LA21_1 == 'n' || LA21_1 == 'r' || LA21_1 == 't')) {
                    alt21 = 1;
                }
                else if ((LA21_1 == 'u')) {
                    alt21 = 2;
                }
                else {
                    NoViableAltException nvae =
                    new NoViableAltException("", 21, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:163:9:
                // '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' |
                // '\\\\' )
                {
                    match('\\');
                    if (input.LA(1) == '\"' || input.LA(1) == '\'' || input.LA(1) == '\\' || input.LA(1) == 'b' || input.LA(1) == 'f' || input.LA(1) == 'n' || input.LA(1) == 'r' || input.LA(1) == 't') {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null, input);
                        recover(mse);
                        throw mse;
                    }

                }
                    break;
                case 2 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:164:9:
                // UnicodeEscape
                {
                    mUnicodeEscape();

                }
                    break;

            }
        } finally {
        }
    }



    // $ANTLR end "EscapeSequence"

    // $ANTLR start "UnicodeEscape"
    public final void mUnicodeEscape() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:169:5: (
            // '\\\\' 'u' HexDigit HexDigit HexDigit HexDigit )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:169:9:
            // '\\\\' 'u' HexDigit HexDigit HexDigit HexDigit
            {
                match('\\');
                match('u');
                mHexDigit();
                mHexDigit();
                mHexDigit();
                mHexDigit();

            }

        } finally {
        }
    }



    // $ANTLR end "UnicodeEscape"

    // $ANTLR start "Letter"
    public final void mLetter() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:174:5: (
            // '\\u0024' | '\\u0041' .. '\\u005a' | '\\u005f' | '\\u0061' ..
            // '\\u007a' | '\\u00c0' .. '\\u00d6' | '\\u00d8' .. '\\u00f6' |
            // '\\u00f8' .. '\\u00ff' | '\\u0100' .. '\\u1fff' | '\\u3040' ..
            // '\\u318f' | '\\u3300' .. '\\u337f' | '\\u3400' .. '\\u3d2d' |
            // '\\u4e00' .. '\\u9fff' | '\\uf900' .. '\\ufaff' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:
            {
                if (input.LA(1) == '$' || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z') || (input.LA(1) >= '\u00C0' && input.LA(1) <= '\u00D6') || (input.LA(1) >= '\u00D8' && input.LA(1) <= '\u00F6') || (input.LA(1) >= '\u00F8' && input.LA(1) <= '\u1FFF') || (input.LA(1) >= '\u3040' && input.LA(1) <= '\u318F') || (input.LA(1) >= '\u3300' && input.LA(1) <= '\u337F') || (input.LA(1) >= '\u3400' && input.LA(1) <= '\u3D2D') || (input.LA(1) >= '\u4E00' && input.LA(1) <= '\u9FFF') || (input.LA(1) >= '\uF900' && input.LA(1) <= '\uFAFF')) {
                    input.consume();

                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

            }

        } finally {
        }
    }



    // $ANTLR end "Letter"

    // $ANTLR start "JavaIDDigit"
    public final void mJavaIDDigit() throws RecognitionException {
        try {
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:191:5: (
            // '\\u0030' .. '\\u0039' | '\\u0660' .. '\\u0669' | '\\u06f0' ..
            // '\\u06f9' | '\\u0966' .. '\\u096f' | '\\u09e6' .. '\\u09ef' |
            // '\\u0a66' .. '\\u0a6f' | '\\u0ae6' .. '\\u0aef' | '\\u0b66' ..
            // '\\u0b6f' | '\\u0be7' .. '\\u0bef' | '\\u0c66' .. '\\u0c6f' |
            // '\\u0ce6' .. '\\u0cef' | '\\u0d66' .. '\\u0d6f' | '\\u0e50' ..
            // '\\u0e59' | '\\u0ed0' .. '\\u0ed9' | '\\u1040' .. '\\u1049' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:
            {
                if ((input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= '\u0660' && input.LA(1) <= '\u0669') || (input.LA(1) >= '\u06F0' && input.LA(1) <= '\u06F9') || (input.LA(1) >= '\u0966' && input.LA(1) <= '\u096F') || (input.LA(1) >= '\u09E6' && input.LA(1) <= '\u09EF') || (input.LA(1) >= '\u0A66' && input.LA(1) <= '\u0A6F') || (input.LA(1) >= '\u0AE6' && input.LA(1) <= '\u0AEF') || (input.LA(1) >= '\u0B66' && input.LA(1) <= '\u0B6F') || (input.LA(1) >= '\u0BE7' && input.LA(1) <= '\u0BEF') || (input.LA(1) >= '\u0C66' && input.LA(1) <= '\u0C6F') || (input.LA(1) >= '\u0CE6' && input.LA(1) <= '\u0CEF') || (input.LA(1) >= '\u0D66' && input.LA(1) <= '\u0D6F') || (input.LA(1) >= '\u0E50' && input.LA(1) <= '\u0E59') || (input.LA(1) >= '\u0ED0' && input.LA(1) <= '\u0ED9') || (input.LA(1) >= '\u1040' && input.LA(1) <= '\u1049')) {
                    input.consume();

                }
                else {
                    MismatchedSetException mse = new MismatchedSetException(null, input);
                    recover(mse);
                    throw mse;
                }

            }

        } finally {
        }
    }



    // $ANTLR end "JavaIDDigit"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:208:5: ( (
            // ' ' | '\\r' | '\\t' | '\ ' )+ )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:208:7: (
            // ' ' | '\\r' | '\\t' | '\ ' )+
            {
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:208:7:
                // ( ' ' | '\\r' | '\\t' | '\ ' )+
                int cnt22 = 0;
                loop22: do {
                    int alt22 = 2;
                    int LA22_0 = input.LA(1);

                    if ((LA22_0 == '\t' || (LA22_0 >= '\f' && LA22_0 <= '\r') || LA22_0 == ' ')) {
                        alt22 = 1;
                    }

                    switch (alt22) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:
                        {
                            if (input.LA(1) == '\t' || (input.LA(1) >= '\f' && input.LA(1) <= '\r') || input.LA(1) == ' ') {
                                input.consume();

                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }

                        }
                            break;

                        default :
                            if (cnt22 >= 1)
                                break loop22;
                            EarlyExitException eee =
                            new EarlyExitException(22, input);
                            throw eee;
                    }
                    cnt22++;
                } while (true);

                _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "WS"

    // $ANTLR start "NL"
    public final void mNL() throws RecognitionException {
        try {
            int _type = NL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:211:5: ( (
            // ( '\\r' )? '\\n' ) )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:211:7: ( (
            // '\\r' )? '\\n' )
            {
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:211:7:
                // ( ( '\\r' )? '\\n' )
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:211:8:
                // ( '\\r' )? '\\n'
                {
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:211:8:
                    // ( '\\r' )?
                    int alt23 = 2;
                    int LA23_0 = input.LA(1);

                    if ((LA23_0 == '\r')) {
                        alt23 = 1;
                    }
                    switch (alt23) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:211:8:
                        // '\\r'
                        {
                            match('\r');

                        }
                            break;

                    }

                    match('\n');

                }

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "NL"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:214:5: (
            // '/*' ( options {greedy=false; } : . )* '*/' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:214:9:
            // '/*' ( options {greedy=false; } : . )* '*/'
            {
                match("/*");

                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:214:14:
                // ( options {greedy=false; } : . )*
                loop24: do {
                    int alt24 = 2;
                    int LA24_0 = input.LA(1);

                    if ((LA24_0 == '*')) {
                        int LA24_1 = input.LA(2);

                        if ((LA24_1 == '/')) {
                            alt24 = 2;
                        }
                        else if (((LA24_1 >= '\u0000' && LA24_1 <= '.') || (LA24_1 >= '0' && LA24_1 <= '\uFFFF'))) {
                            alt24 = 1;
                        }

                    }
                    else if (((LA24_0 >= '\u0000' && LA24_0 <= ')') || (LA24_0 >= '+' && LA24_0 <= '\uFFFF'))) {
                        alt24 = 1;
                    }

                    switch (alt24) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:214:42:
                        // .
                        {
                            matchAny();

                        }
                            break;

                        default :
                            break loop24;
                    }
                } while (true);

                match("*/");

                _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "COMMENT"

    // $ANTLR start "LINE_COMMENT"
    public final void mLINE_COMMENT() throws RecognitionException {
        try {
            int _type = LINE_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:218:5: (
            // '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:218:7:
            // '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
            {
                match("//");

                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:218:12:
                // (~ ( '\\n' | '\\r' ) )*
                loop25: do {
                    int alt25 = 2;
                    int LA25_0 = input.LA(1);

                    if (((LA25_0 >= '\u0000' && LA25_0 <= '\t') || (LA25_0 >= '\u000B' && LA25_0 <= '\f') || (LA25_0 >= '\u000E' && LA25_0 <= '\uFFFF'))) {
                        alt25 = 1;
                    }

                    switch (alt25) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:218:12:
                        // ~ ( '\\n' | '\\r' )
                        {
                            if ((input.LA(1) >= '\u0000' && input.LA(1) <= '\t') || (input.LA(1) >= '\u000B' && input.LA(1) <= '\f') || (input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF')) {
                                input.consume();

                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null, input);
                                recover(mse);
                                throw mse;
                            }

                        }
                            break;

                        default :
                            break loop25;
                    }
                } while (true);

                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:218:26:
                // ( '\\r' )?
                int alt26 = 2;
                int LA26_0 = input.LA(1);

                if ((LA26_0 == '\r')) {
                    alt26 = 1;
                }
                switch (alt26) {
                    case 1 :
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:218:26:
                    // '\\r'
                    {
                        match('\r');

                    }
                        break;

                }

                match('\n');
                _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        } finally {
        }
    }



    // $ANTLR end "LINE_COMMENT"

    public void mTokens() throws RecognitionException {
        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:8: ( INT |
        // STRING | FLOAT | CHAR | DATE | NULL | T__28 | T__29 | DecimalLiteral
        // | Identifier | FloatingPointLiteral | StringLiteral | CharLiteral |
        // WS | NL | COMMENT | LINE_COMMENT )
        int alt27 = 17;
        alt27 = dfa27.predict(input);
        switch (alt27) {
            case 1 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:10: INT
            {
                mINT();

            }
                break;
            case 2 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:14:
            // STRING
            {
                mSTRING();

            }
                break;
            case 3 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:21:
            // FLOAT
            {
                mFLOAT();

            }
                break;
            case 4 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:27: CHAR
            {
                mCHAR();

            }
                break;
            case 5 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:32: DATE
            {
                mDATE();

            }
                break;
            case 6 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:37: NULL
            {
                mNULL();

            }
                break;
            case 7 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:42:
            // T__28
            {
                mT__28();

            }
                break;
            case 8 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:48:
            // T__29
            {
                mT__29();

            }
                break;
            case 9 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:54:
            // DecimalLiteral
            {
                mDecimalLiteral();

            }
                break;
            case 10 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:69:
            // Identifier
            {
                mIdentifier();

            }
                break;
            case 11 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:80:
            // FloatingPointLiteral
            {
                mFloatingPointLiteral();

            }
                break;
            case 12 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:101:
            // StringLiteral
            {
                mStringLiteral();

            }
                break;
            case 13 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:115:
            // CharLiteral
            {
                mCharLiteral();

            }
                break;
            case 14 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:127: WS
            {
                mWS();

            }
                break;
            case 15 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:130: NL
            {
                mNL();

            }
                break;
            case 16 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:133:
            // COMMENT
            {
                mCOMMENT();

            }
                break;
            case 17 :
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:1:141:
            // LINE_COMMENT
            {
                mLINE_COMMENT();

            }
                break;

        }

    }

    protected DFA16 dfa16 = new DFA16(this);
    protected DFA27 dfa27 = new DFA27(this);
    static final String DFA16_eotS =
    "\2\uffff\1\6\4\uffff";
    static final String DFA16_eofS =
    "\7\uffff";
    static final String DFA16_minS =
    "\1\55\2\56\4\uffff";
    static final String DFA16_maxS =
    "\2\71\1\145\4\uffff";
    static final String DFA16_acceptS =
    "\3\uffff\1\2\1\1\1\3\1\4";
    static final String DFA16_specialS =
    "\7\uffff}>";
    static final String[] DFA16_transitionS = {
    "\1\1\1\3\1\uffff\12\2",
    "\1\3\1\uffff\12\2",
    "\1\4\1\uffff\12\2\13\uffff\1\5\37\uffff\1\5",
    "",
    "",
    "",
    ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }



        public String getDescription() {
            return "140:1: FloatingPointLiteral : ( ( '-' )? ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( Exponent )? | ( '-' )? '.' ( '0' .. '9' )+ ( Exponent )? | ( '-' )? ( '0' .. '9' )+ Exponent | ( '-' )? ( '0' .. '9' )+ );";
        }
    }

    static final String DFA27_eotS =
    "\1\uffff\6\14\3\uffff\2\32\4\uffff\1\21\3\uffff\6\14\1\uffff\1" +
    "\32\2\uffff\1\44\5\14\1\uffff\2\14\1\54\1\55\1\56\1\14\1\60\3\uffff" +
    "\1\61\2\uffff";
    static final String DFA27_eofS =
    "\62\uffff";
    static final String DFA27_minS =
    "\1\11\1\156\1\164\1\154\1\150\1\141\1\165\2\uffff\3\56\4\uffff" +
    "\1\12\2\uffff\1\52\1\164\1\162\1\157\1\141\1\164\1\154\1\uffff\1" +
    "\56\2\uffff\1\60\1\151\1\141\1\162\1\145\1\154\1\uffff\1\156\1\164" +
    "\3\60\1\147\1\60\3\uffff\1\60\2\uffff";
    static final String DFA27_maxS =
    "\1\172\1\156\1\164\1\154\1\150\1\141\1\165\2\uffff\1\71\2\145\4" +
    "\uffff\1\12\2\uffff\1\57\1\164\1\162\1\157\1\141\1\164\1\154\1\uffff" +
    "\1\145\2\uffff\1\172\1\151\1\141\1\162\1\145\1\154\1\uffff\1\156" +
    "\1\164\3\172\1\147\1\172\3\uffff\1\172\2\uffff";
    static final String DFA27_acceptS =
    "\7\uffff\1\7\1\10\3\uffff\1\12\1\13\1\14\1\15\1\uffff\1\16\1\17" +
    "\7\uffff\1\11\1\uffff\1\20\1\21\6\uffff\1\1\7\uffff\1\4\1\5\1\6" +
    "\1\uffff\1\3\1\2";
    static final String DFA27_specialS =
    "\62\uffff}>";
    static final String[] DFA27_transitionS = {
    "\1\21\1\22\1\uffff\1\21\1\20\22\uffff\1\21\1\uffff\1\16\4\uffff" +
    "\1\17\4\uffff\1\7\1\11\1\15\1\23\1\12\11\13\3\uffff\1\10\3\uffff" +
    "\32\14\4\uffff\1\14\1\uffff\2\14\1\4\1\5\1\14\1\3\2\14\1\1\4" +
    "\14\1\6\4\14\1\2\7\14",
    "\1\24",
    "\1\25",
    "\1\26",
    "\1\27",
    "\1\30",
    "\1\31",
    "",
    "",
    "\1\15\1\uffff\1\12\11\13",
    "\1\15\1\uffff\12\15\13\uffff\1\15\37\uffff\1\15",
    "\1\15\1\uffff\12\33\13\uffff\1\15\37\uffff\1\15",
    "",
    "",
    "",
    "",
    "\1\22",
    "",
    "",
    "\1\34\4\uffff\1\35",
    "\1\36",
    "\1\37",
    "\1\40",
    "\1\41",
    "\1\42",
    "\1\43",
    "",
    "\1\15\1\uffff\12\33\13\uffff\1\15\37\uffff\1\15",
    "",
    "",
    "\12\14\7\uffff\32\14\4\uffff\1\14\1\uffff\32\14",
    "\1\45",
    "\1\46",
    "\1\47",
    "\1\50",
    "\1\51",
    "",
    "\1\52",
    "\1\53",
    "\12\14\7\uffff\32\14\4\uffff\1\14\1\uffff\32\14",
    "\12\14\7\uffff\32\14\4\uffff\1\14\1\uffff\32\14",
    "\12\14\7\uffff\32\14\4\uffff\1\14\1\uffff\32\14",
    "\1\57",
    "\12\14\7\uffff\32\14\4\uffff\1\14\1\uffff\32\14",
    "",
    "",
    "",
    "\12\14\7\uffff\32\14\4\uffff\1\14\1\uffff\32\14",
    "",
    ""
    };

    static final short[] DFA27_eot = DFA.unpackEncodedString(DFA27_eotS);
    static final short[] DFA27_eof = DFA.unpackEncodedString(DFA27_eofS);
    static final char[] DFA27_min = DFA.unpackEncodedStringToUnsignedChars(DFA27_minS);
    static final char[] DFA27_max = DFA.unpackEncodedStringToUnsignedChars(DFA27_maxS);
    static final short[] DFA27_accept = DFA.unpackEncodedString(DFA27_acceptS);
    static final short[] DFA27_special = DFA.unpackEncodedString(DFA27_specialS);
    static final short[][] DFA27_transition;

    static {
        int numStates = DFA27_transitionS.length;
        DFA27_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA27_transition[i] = DFA.unpackEncodedString(DFA27_transitionS[i]);
        }
    }

    class DFA27 extends DFA {

        public DFA27(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 27;
            this.eot = DFA27_eot;
            this.eof = DFA27_eof;
            this.min = DFA27_min;
            this.max = DFA27_max;
            this.accept = DFA27_accept;
            this.special = DFA27_special;
            this.transition = DFA27_transition;
        }



        public String getDescription() {
            return "1:1: Tokens : ( INT | STRING | FLOAT | CHAR | DATE | NULL | T__28 | T__29 | DecimalLiteral | Identifier | FloatingPointLiteral | StringLiteral | CharLiteral | WS | NL | COMMENT | LINE_COMMENT );";
        }
    }

}