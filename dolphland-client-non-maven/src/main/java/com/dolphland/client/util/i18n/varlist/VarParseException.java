package com.dolphland.client.util.i18n.varlist;

import com.dolphland.client.util.exception.AppInfrastructureException;


/**
 * <p>
 * Raised by {@link VarParser} when some parsing error has occured
 * </p>
 * 
 * @author JayJay
 */
public class VarParseException extends AppInfrastructureException {

	public VarParseException() {
		super();
	}

	public VarParseException(String pattern, Object... args) {
		super(pattern, args);
	}

	public VarParseException(String pattern, Throwable cause, Object... args) {
		super(pattern, cause, args);
	}

	public VarParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public VarParseException(String message) {
		super(message);
	}

	public VarParseException(Throwable cause) {
		super(cause);
	}

}
