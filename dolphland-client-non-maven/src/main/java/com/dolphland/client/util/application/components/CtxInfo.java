package com.dolphland.client.util.application.components;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;

import com.dolphland.client.util.string.StrUtil;

/**
 * Composant Swing affichant une bulle d'information contenant du texte ou du
 * HTML. La bulle se d�finit par :<br>
 * <li>Un point d�signant l'emplacement de la bulle : bodyPoint
 * <li>Une taille de corps pour la bulle : bodySize
 * <li>Un point d'attache qui indique ce que la bulle doit d�signer � l'�cran :
 * attachPoint<br>
 * Exemple d'utilisation :<br>
 * <code>
 * 	VisionCtxInfo tip = new VisionCtxInfo();<br>
 * 	tip.setBodyPoint(100,100);<br>
 * 	tip.setBodySize(180,80);<br>
 * 	tip.setAttachPoint(120,50);<br>
 * 	tip.validate();<br>
 * </code>
 * 
 * @author JayJay
 */
public class CtxInfo extends JComponent {

	/** Queue de la bulle en haut du corps */
	private static final int ATTACH_TOP = 1;

	/** Queue de la bulle � gauche du corps */
	private static final int ATTACH_LEFT = 2;

	/** Queue de la bulle en bas du corps */
	private static final int ATTACH_BOTTOM = 3;

	/** Queue de la bulle � droite du coprs */
	private static final int ATTACH_RIGHT = 4;

	/** Point d'attache de la bulle */
	private Point attachPoint;

	/** Point origine du corps de la bulle */
	private Point bodyPoint;

	/** Taille du corps de la bulle */
	private Dimension bodySize;

	/** Cot� du corps de la bulle auquel est rattach� la queue de la bulle */
	private int attachSide;

	/** Taille du rayon des cercles utilis�s pour arrondir les coins */
	private int cornerRadius = 10;

	/** Taille de la queue de la bulle � sa base */
	private int gapSize = 15;

	/** Dession de la bulle */
	private GeneralPath tipShape;

	/**
     * Indique si le point d'attache est valde. Le point d'attache est invalide
     * s'il est situ� en dehors du corps
     */
	private boolean validAttachPoint;

	/**
     * Matrice de convolution pour le floutage de l'ombre
     * 
     */
	private float[] blurKernel;

	private ConvolveOp blurOp;

	/**
     * Texte � afficher
     */
	private String text;

	/**
     * Vue HTML du texte � afficher
     */
	private View htmlView;

	/**
     * Image de l'ombre de la bulle
     */
	private BufferedImage shadow;
	
	private float alpha;

	public CtxInfo() {
		attachPoint = new Point();
		bodyPoint = new Point();
		setSize(1, 1);
		float v = 1f / 55f;
		blurKernel = new float[]{v, v, v, v, v, v, v, v, v};
		blurOp = new ConvolveOp(new Kernel(3, 3, blurKernel));
		setBackground(new Color(255, 255, 230));
		text = StrUtil.EMPTY_STRING;
		alpha = 1;
	}

	public void setAlpha(float alpha) {
		if(alpha>=0) {
			if(alpha <=1) {
				this.alpha = alpha;
			} else {
				this.alpha = 1;
			}
		} else {
			this.alpha = 0; 
		}
		repaint();
	}
	
	public float getAlpha() {
		return alpha;
	}
	
	public void setAttachPoint(Point attachPoint) {
		if(attachPoint == null) {
			throw new NullPointerException("attachePoint is null !"); //$NON-NLS-1$
		}
		this.attachPoint = attachPoint;
		invalidate();
	}

	public void setAttachPoint(int x, int y) {
		this.attachPoint = new Point(x, y);
	}

	public void setBodyPoint(int x, int y) {
		this.bodyPoint = new Point(x, y);
	}

	public void setBodyPoint(Point p) {
		this.bodyPoint = new Point(p);
		invalidate();
	}

	public void setBodySize(int w, int h) {
		setBodySize(new Dimension(w, h));
	}

	public void setBodySize(Dimension size) {
		bodySize = new Dimension(size);
		invalidate();
	}

	public Dimension getBodySize() {
		return new Dimension(bodySize);
	}

	public int getBodyWidth() {
		return bodySize.width;
	}

	public int getBodyHeight() {
		return bodySize.height;
	}

	public void setText(String txt) {
		if(txt == null) {
			txt = StrUtil.EMPTY_STRING;
		}
		this.text = txt;
		if(BasicHTML.isHTMLString(txt)) {
			htmlView = BasicHTML.createHTMLView(this, txt);
		} else {

			// Fixe la couleur de texte en fonction du Foreground du composant s'il est renseign�
			Color textColor = Color.BLACK;
			if(getForeground() != null)	{
				textColor = getForeground();
			}
			
			String codeHexaColor = (Integer.toHexString(textColor.getRGB())).substring(2);
			txt = "<html><font color=#" + codeHexaColor + ">" + txt.replaceAll("\n", "<br>") + "</font></html>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-5$ //$NON-NLS-3$ //$NON-NLS-4$
			htmlView = BasicHTML.createHTMLView(this, txt);
		}
		updateHtmlView();
	}

	private void updateHtmlView() {
		// d�termination de la taille de la bulle
		float width = htmlView.getPreferredSpan(View.X_AXIS);
		float height = htmlView.getPreferredSpan(View.Y_AXIS);
		Border boder = getBorder();
		Insets margin = new Insets(0, 0, 0, 0);
		if(boder != null) {
			margin = getBorder().getBorderInsets(this);
		}
		if(text == null || text.length() == 0) {
			setBodySize((int)width + cornerRadius * 2 + margin.left + margin.right, (int)height + cornerRadius + margin.top + margin.bottom);
		} else {
			setBodySize((int)width + cornerRadius + margin.left + margin.right, (int)height + cornerRadius + margin.top + margin.bottom);
		}
	}

	public String getText() {
		return text;
	}

	/**
     * Calcul les coordonn�es et la taille du composant en fonction du point
     * d'attache, du point origine du corps de la bulle et de sa taille
     */
	private void pack() {
		Point ap = new Point(attachPoint);
		Point bp = new Point(bodyPoint);

		if(htmlView == null) {
			updateHtmlView();
		}

		Dimension bs = bodySize;
		int x = bodyPoint.x;
		int y = bodyPoint.y;
		int w = bodySize.width;
		int h = bodySize.height;
		boolean attachInside = ((ap.x >= x) && (ap.y >= y) && (ap.x < x + w) && (ap.y < y + h));
		if(!attachInside) {
			if(ap.x < x) {
				w += x - ap.x;
				x = ap.x;
			}
			if(ap.y < y) {
				h += y - ap.y;
				y = ap.y;
			}
			if(ap.x > x + w) {
				w += ap.x - (x + w);
			}
			if(ap.y > y + h) {
				h += ap.y - (y + h);
			}
			validAttachPoint = true;
		} else {
			// point d'attache � l'int�rieur du body => ne pas afficher la queue
			// de la bulle
			validAttachPoint = false;
		}
		w += 5;
		h += 5;
		if((ap.x < bp.x) && (ap.y < bp.y)) {
			// CADRANT TOP_LEFT
			double dx = Math.abs(bp.x - ap.x);
			double dy = Math.abs(bp.y - ap.y);
			double d = Math.sqrt(dx * dx + dy * dy);
			double cosAlpha = dy / d;
			if(cosAlpha < Math.sin(3 * Math.PI / 4d)) {
				attachSide = ATTACH_LEFT;
			} else {
				attachSide = ATTACH_TOP;
			}
		} else if((ap.x > bp.x + bs.width) && (ap.y < bp.y)) {
			// CADRANT TOP_RIGHT
			double dx = Math.abs(bp.x + bs.width - ap.x);
			double dy = Math.abs(bp.y - ap.y);
			double d = Math.sqrt(dx * dx + dy * dy);
			double sinAlpha = dy / d;
			if(sinAlpha < Math.sin(3 * Math.PI / 4d)) {
				attachSide = ATTACH_RIGHT;
			} else {
				attachSide = ATTACH_TOP;
			}
		} else if((ap.x > bp.x + bs.width) && (ap.y > bp.y + bs.height)) {
			// CADRANT BOTTOM_RIGHT
			double dx = Math.abs(bp.x + bs.width - ap.x);
			double dy = Math.abs(bp.y + bs.height - ap.y);
			double d = Math.sqrt(dx * dx + dy * dy);
			double cosAlpha = dy / d;
			if(-cosAlpha < Math.sin(7 * Math.PI / 4d)) {
				attachSide = ATTACH_BOTTOM;
			} else {
				attachSide = ATTACH_RIGHT;
			}
		} else if((ap.x < bp.x) && (ap.y > bp.y + bs.height)) {
			// CADRANT BOTTOM_LEFT
			double dx = Math.abs(bp.x - ap.x);
			double dy = Math.abs(bp.y - ap.y);
			double d = Math.sqrt(dx * dx + dy * dy);
			double sinAlpha = dy / d;
			if(sinAlpha - 1 < Math.sin(5 * Math.PI / 4d)) {
				attachSide = ATTACH_BOTTOM;
			} else {
				attachSide = ATTACH_LEFT;
			}
		} else if(ap.y < bp.y) {
			// TOP
			attachSide = ATTACH_TOP;
		} else if(ap.x < bp.x) {
			// LEFT
			attachSide = ATTACH_LEFT;
		} else if(ap.y > bp.y + bs.height) {
			// BOTTOM
			attachSide = ATTACH_BOTTOM;
		} else if(ap.x > bp.x + bs.width) {
			// RIGHT
			attachSide = ATTACH_RIGHT;
		}

		ap.translate(-x, -y);
		bp.translate(-x, -y);
		int cr = cornerRadius;

		Shape topLeftCorner = new Arc2D.Float(bp.x, bp.y, cr * 2, cr * 2, 90f, 90f, Arc2D.OPEN);
		Shape topRightCorner = new Arc2D.Float(bp.x + bs.width - cr * 2 - 1, bp.y, cr * 2, cr * 2, 0f, 90f, Arc2D.OPEN);
		Shape bottomRightCorner = new Arc2D.Float(bp.x + bs.width - cr * 2 - 1, bp.y + bs.height - cr * 2 - 1, cr * 2, cr * 2, 270f, 90f, Arc2D.OPEN);
		Shape bottomLeftCorner = new Arc2D.Float(bp.x, bp.y + bs.height - cr * 2 - 1, cr * 2, cr * 2, 180f, 90f, Arc2D.OPEN);
		tipShape = new GeneralPath();
		switch(attachSide) {
			default:
				// si attachSide n'a pas �t� positionn�, forcer l'entr�e dans un
				// des cas
				// du switch pour dessiner le tip ne pas dessiner la queue de la
				// bulle
				validAttachPoint = false;
			case ATTACH_TOP: {
				int gapX;
				Point topLeft = bp;
				Point topRight = new Point(bp.x + bs.width - 1, bp.y);
				int gs = (int)Math.min(gapSize, topLeft.distance(topRight) - cr * 2);
				if(topLeft.distance(ap) < topRight.distance(ap)) {
					// GAP LEFT
					gapX = bp.x + cr + gs / 2;
				} else {
					// GAP RIGHT
					gapX = bp.x + bs.width - 1 - cr - gs / 2;
				}
				tipShape.moveTo(bp.x + bs.width - cr - 1, bp.y);
				if(gs > 0 && validAttachPoint) {
					tipShape.lineTo(gapX + gs / 2, bp.y);
					tipShape.lineTo(ap.x, ap.y);
					tipShape.lineTo(gapX - gs / 2, bp.y);
				}
				tipShape.lineTo(bp.x + cr, bp.y);
				tipShape.append(topLeftCorner, true);
				tipShape.lineTo(bp.x, bp.y + bs.height - cr - 1);
				tipShape.append(bottomLeftCorner, true);
				tipShape.lineTo(bp.x + bs.width - cr - 1, bp.y + bs.height - 1);
				tipShape.append(bottomRightCorner, true);
				tipShape.lineTo(bp.x + bs.width - 1, bp.y + cr);
				tipShape.append(topRightCorner, true);
				break;
			}
			case ATTACH_LEFT: {
				int gapY;
				Point topLeft = bp;
				Point bottomLeft = new Point(bp.x, bp.y + bs.height - 1);
				int gs = (int)Math.min(gapSize, topLeft.distance(bottomLeft) - cr * 2);
				if(topLeft.distance(ap) < bottomLeft.distance(ap)) {
					// GAP TOP
					gapY = bp.y + cr + gs / 2;
				} else {
					// GAP BOTTOM
					gapY = bp.y + bs.height - 1 - cr - gs / 2;
				}
				tipShape.moveTo(bp.x, bp.y + cr);
				if(gs > 0 && validAttachPoint) {
					tipShape.lineTo(bp.x, gapY - gs / 2);
					tipShape.lineTo(ap.x, ap.y);
					tipShape.lineTo(bp.x, gapY + gs / 2);
				}
				tipShape.lineTo(bp.x, bp.y + bs.height - cr - 1);
				tipShape.append(bottomLeftCorner, true);
				tipShape.lineTo(bp.x + bs.width - cr - 1, bp.y + bs.height - 1);
				tipShape.append(bottomRightCorner, true);
				tipShape.lineTo(bp.x + bs.width - 1, bp.y + cr);
				tipShape.append(topRightCorner, true);
				tipShape.lineTo(bp.x + cr, bp.y);
				tipShape.append(topLeftCorner, true);
				break;
			}
			case ATTACH_BOTTOM: {
				int gapX;
				Point bottomLeft = new Point(bp.x, bp.y + bs.height - 1);
				Point bottomRight = new Point(bp.x + bs.width - 1, bp.y + bs.height - 1);
				int gs = (int)Math.min(gapSize, bottomLeft.distance(bottomRight) - cr * 2);
				if(bottomLeft.distance(ap) < bottomRight.distance(ap)) {
					// GAP LEFT
					gapX = bp.x + cr + gs / 2;
				} else {
					// GAP RIGHT
					gapX = bp.x + bs.width - 1 - cr - gs / 2;
				}
				tipShape.moveTo(bp.x, bp.y + cr);
				tipShape.lineTo(bp.x, bp.y + bs.height - cr - 1);
				tipShape.append(bottomLeftCorner, true);
				if(gapSize > 0 && validAttachPoint) {
					tipShape.lineTo(gapX - gs / 2, bp.y + bs.height - 1);
					tipShape.lineTo(ap.x, ap.y);
					tipShape.lineTo(gapX + gs / 2, bp.y + bs.height - 1);
				}
				tipShape.lineTo(bp.x + bs.width - cr - 1, bp.y + bs.height - 1);
				tipShape.append(bottomRightCorner, true);
				tipShape.lineTo(bp.x + bs.width - 1, bp.y + cr - 1);
				tipShape.append(topRightCorner, true);
				tipShape.lineTo(bp.x + cr, bp.y);
				tipShape.append(topLeftCorner, true);
				break;
			}
			case ATTACH_RIGHT: {
				int gapY;
				Point bottomRight = new Point(bp.x + bs.width - 1, bp.y + bs.height - 1);
				Point topRight = new Point(bp.x + bs.width - 1, bp.y);
				int gs = (int)Math.min(gapSize, bottomRight.distance(topRight) - cr * 2);
				if(bottomRight.distance(ap) < topRight.distance(ap)) {
					// GAP BOTTOM
					gapY = bp.y + bs.height - 1 - cr - gs / 2;
				} else {
					// GAP TOP
					gapY = bp.y + cr + gs / 2;
				}
				tipShape.moveTo(bp.x, bp.y + cr);
				tipShape.lineTo(bp.x, bp.y + bs.height - cr - 1);
				tipShape.append(bottomLeftCorner, true);
				tipShape.lineTo(bp.x + bs.width - cr - 1, bp.y + bs.height - 1);
				tipShape.append(bottomRightCorner, true);
				if(gs > 0 && validAttachPoint) {
					tipShape.lineTo(bp.x + bs.width - 1, gapY + gs / 2);
					tipShape.lineTo(ap.x, ap.y);
					tipShape.lineTo(bp.x + bs.width - 1, gapY - gs / 2);
				}
				tipShape.lineTo(bp.x + bs.width - 1, bp.y + cr);
				tipShape.append(topRightCorner, true);
				tipShape.lineTo(bp.x + cr, bp.y);
				tipShape.append(topLeftCorner, true);
				break;
			}
		}
		shadow = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g3 = shadow.createGraphics();
		g3.setPaint(Color.LIGHT_GRAY);
		g3.fill(tipShape);
		setBounds(x, y, w, h);
	}

	public void validate() {
		pack();
	}

	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		if(tipShape != null) {
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
			g2.setComposite(ac);
			if(shadow != null) {
				g2.drawImage(shadow, blurOp, 5, 5);
			}
			g2.setPaint(getBackground());
			g2.fill(tipShape);
			g2.setPaint(Color.BLACK);
			g2.draw(tipShape);
			if(htmlView != null) {
				Point bp = new Point(bodyPoint);
				bp.translate(-getX() + cornerRadius / 2, -getY() + cornerRadius / 2);
				htmlView.paint(g, new Rectangle(bp, bodySize));
			}
		}
	}

}
