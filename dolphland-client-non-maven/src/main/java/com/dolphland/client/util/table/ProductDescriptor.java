/*
 * $Log: ProductDescriptor.java,v $
 * Revision 1.4 2014/04/15 14:51:47 bvatan
 * - FWK-143: Added support for row sorting when a double click is performed on
 * a ProtuctTable column header
 * Revision 1.3 2010/07/05 09:49:51 bvatan
 * - ajout service permettant de supprimer toues les propri�t�s du descripteur
 * Revision 1.2 2009/08/07 12:16:03 bvatan
 * - suppression Map cannonique non utilis�e
 * Revision 1.1 2009/02/04 13:38:39 jscafi
 * - Ajout de ProducTable permettant de g�rer des tables de produits
 * Revision 1.1 2009/01/06 14:17:05 bvatan
 * - ajotu gestion du blocage des barre par coul�e
 */

package com.dolphland.client.util.table;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe d�finissant les propri�t�s de produit permettant de d�finir un produit
 * 
 * @author jeremy.scafi
 * @author JayJay
 * 
 */
public class ProductDescriptor {

    private List<ProductProperty> propertyList;



    public ProductDescriptor() {
        propertyList = new ArrayList<ProductProperty>();
    }



    /**
     * Ajoute une propri�t� de produit � la description du produit
     * 
     * @param pp
     *            Propri�t� de produit
     */
    public void addProperty(ProductProperty pp) {
        propertyList.add(pp);
    }



    /**
     * Supprime toutes les prori�t�s du descripteur.
     */
    public void clearProperties() {
        propertyList.clear();
    }



    /**
     * Renvoie la liste des propri�t�s de produit permettant de d�finir un
     * produit
     */
    public List<ProductProperty> getProperties() {
        return propertyList;
    }



    public ProductProperty getPropertyAt(int propertyIndex) {
        if (propertyIndex < 0 || propertyIndex >= propertyList.size()) {
            throw new IndexOutOfBoundsException("propertyIndex out of range (" + propertyIndex + ") ! Maximum is " + (propertyList.size() - 1) + ", minimum is 0");
        }
        return propertyList.get(propertyIndex);
    }
}
