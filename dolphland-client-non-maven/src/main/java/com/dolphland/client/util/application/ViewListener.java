package com.dolphland.client.util.application;

/**
 * 
 * @author jeremy.scafi
 * 
 */
public class ViewListener implements Messenger {

    private Scenario scenario;



    public ViewListener(Scenario scenario) {
        this.scenario = scenario;
    }



    public void showInfoDialog(String msg) {
        if (scenario != null && scenario.getWorkPane() != null)
            scenario.getWorkPane().showInfoDialog(msg);
    }



    public void showWarningDialog(String msg) {
        if (scenario != null && scenario.getWorkPane() != null)
            scenario.getWorkPane().showWarningDialog(msg);
    }



    public void showErrorDialog(String msg) {
        if (scenario != null && scenario.getWorkPane() != null)
            scenario.getWorkPane().showErrorDialog(msg);
    }



    public boolean showConfirmDialog(String msg) {
        if (scenario != null && scenario.getWorkPane() != null)
            return scenario.getWorkPane().showConfirmDialog(msg);
        return false;
    }



    public String showInputDialog(String msg) {
        if (scenario != null && scenario.getWorkPane() != null)
            return scenario.getWorkPane().showInputDialog(msg);
        return null;
    }



    public String showInputDialog(String msg, String initialInputValue) {
        if (scenario != null && scenario.getWorkPane() != null)
            return scenario.getWorkPane().showInputDialog(msg, initialInputValue);
        return null;
    }



    public Object showInputDialog(String msg, Object[] elements) {
        if (scenario != null && scenario.getWorkPane() != null)
            return scenario.getWorkPane().showInputDialog(msg, elements);
        return null;
    }



    public String showPasswordDialog(String msg) {
        if (scenario != null && scenario.getWorkPane() != null)
            return scenario.getWorkPane().showPasswordDialog(msg);
        return null;
    }



    public String showPasswordDialog(String msg, String initialPasswordValue) {
        if (scenario != null && scenario.getWorkPane() != null)
            return scenario.getWorkPane().showPasswordDialog(msg, initialPasswordValue);
        return null;
    }



    public void clearMessages() {
        if (scenario != null && scenario.getWorkPane() != null)
            scenario.getWorkPane().clearMessages();
    }



    public void showInfoMessage(String message) {
        if (scenario != null && scenario.getWorkPane() != null)
            scenario.getWorkPane().showInfoMessage(message);
    }



    public void showWarningMessage(String message) {
        if (scenario != null && scenario.getWorkPane() != null)
            scenario.getWorkPane().showWarningMessage(message);
    }



    public void showErrorMessage(String message) {
        if (scenario != null && scenario.getWorkPane() != null)
            scenario.getWorkPane().showErrorMessage(message);
    }

}
