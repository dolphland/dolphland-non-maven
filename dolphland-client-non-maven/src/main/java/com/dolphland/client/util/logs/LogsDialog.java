package com.dolphland.client.util.logs;

import info.clearthought.layout.TableLayout;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.dialogs.DialogUtil;
import com.dolphland.client.util.event.CloseAllDialogsEvt;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.Event;
import com.dolphland.client.util.event.FwkEvents;
import com.dolphland.client.util.event.RegisterKeyEvent;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.transaction.SwingInvoker;

public class LogsDialog extends JFrame {

    /** Default gap value that should separate components (in pixels) */
    private static final int DEFAULT_GAP = 10;

    private static final MessageFormater fmt = MessageFormater.getFormater(LogsDialog.class);

    // Logs dialog singleton
    private static LogsDialog instance;

    // Editor pane
    private JEditorPane editorPane;

    // Scroll pane used bu editor pane
    private JScrollPane jspEditorPane;

    // Action to hide or show logs dialog
    private Runnable hideShowAction;

    // Button to pause logs
    private JButton btnPause;

    // Button to resume logs
    private JButton btnResume;

    // Button to clear logs
    private JButton btnClear;

    // Button to copy logs
    private JButton btnCopy;

    // Button to close logs dialog
    private JButton btnClose;



    /**
     * Private constructor used by singleton
     */
    private LogsDialog() {
        // Set content pane
        setContentPane(createContentPane());

        // Add listener to hide logs dialog when window closing
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                setVisible(false);
            }
        });

        // Create action to hide or show logs dialog
        hideShowAction = new HideShowLogsAction();
    }



    private FwkPanel createContentPane() {
        FwkPanel out = new FwkPanel();
        out.setTypeBorder(FwkPanel.TYPE_NO_BORDER);

        TableLayoutBuilder layoutBuider = new TableLayoutBuilder();

        out.setLayout(layoutBuider.get());

        out.add(getJspEditorPane(), layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
            .row(0)
            .width(TableLayout.FILL)
            .height(TableLayout.FILL)
            .get()));

        out.add(createButtonPane(), layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
            .row(1)
            .height(TableLayout.PREFERRED)
            .get()));

        return out;
    }



    private FwkPanel createButtonPane() {
        FwkPanel out = new FwkPanel();
        out.setTypeBorder(FwkPanel.TYPE_NO_BORDER);

        TableLayoutBuilder layoutBuider = new TableLayoutBuilder();

        out.setLayout(layoutBuider.get());

        out.add(getBtnPause(), layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
            .column(0)
            .topInsets(DEFAULT_GAP)
            .height(TableLayout.PREFERRED)
            .bottomInsets(DEFAULT_GAP)
            .leftInsets(1f / 5f)
            .width(TableLayout.PREFERRED)
            .get()));

        out.add(getBtnResume(), layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
            .column(0)
            .get()));

        out.add(getBtnClear(), layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
            .column(1)
            .leftInsets(1f / 5f)
            .width(TableLayout.PREFERRED)
            .get()));

        out.add(getBtnCopy(), layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
            .column(2)
            .leftInsets(1f / 5f)
            .width(TableLayout.PREFERRED)
            .get()));

        out.add(getBtnClose(), layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
            .column(3)
            .leftInsets(1f / 5f)
            .width(TableLayout.PREFERRED)
            .rightInsets(1f / 5f)
            .get()));

        return out;
    }



    private JScrollPane getJspEditorPane() {
        if (jspEditorPane == null) {
            jspEditorPane = new JScrollPane(getEditorPane());
        }
        return jspEditorPane;
    }



    private JEditorPane getEditorPane() {
        if (editorPane == null) {
            editorPane = UIAppender.getEditor();
        }
        return editorPane;
    }



    /**
     * Initialize focus to get focus on editor pane
     */
    private void initFocus() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                UIAppender.getEditor().requestFocus();
            }
        });
    }



    private JButton getBtnPause() {
        if (btnPause == null) {
            btnPause = new JButton(fmt.format("LogsDialog.RS_ACTION_PAUSE"));
            // Pause editor pane's content after click on button
            btnPause.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    pause();
                }
            });
        }
        return btnPause;
    }



    private JButton getBtnResume() {
        if (btnResume == null) {
            btnResume = new JButton(fmt.format("LogsDialog.RS_ACTION_RESUME"));
            btnResume.setVisible(false);
            // Resume editor pane's content after click on button
            btnResume.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    resume();
                }
            });
        }
        return btnResume;
    }



    private JButton getBtnClear() {
        if (btnClear == null) {
            btnClear = new JButton(fmt.format("LogsDialog.RS_ACTION_CLEAR"));
            // Clear editor pane's content after click on button
            btnClear.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // Clear text on editor pane
                    UIAppender.getEditor().setText(StrUtil.EMPTY_STRING);

                    // Initialize focus to get focus on editor pane
                    initFocus();
                }
            });
        }
        return btnClear;
    }



    private JButton getBtnCopy() {
        if (btnCopy == null) {
            btnCopy = new JButton(fmt.format("LogsDialog.RS_ACTION_COPY"));
            // Copy in clip board editor pane's content after click on button
            btnCopy.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    // If text is selected, copy selected text
                    String textToCopy = UIAppender.getEditor().getSelectedText();
                    // Else, copy all content
                    if (StrUtil.isEmpty(textToCopy)) {
                        textToCopy = UIAppender.getEditor().getText();
                    }

                    // Copy text in clip board
                    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(textToCopy), null);

                    // Initialize focus to get focus on editor pane
                    initFocus();
                }
            });
        }
        return btnCopy;
    }



    private JButton getBtnClose() {
        if (btnClose == null) {
            btnClose = new JButton(fmt.format("LogsDialog.RS_ACTION_CLOSE"));
            // Hide logs dialog after click on button
            btnClose.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    hideShowAction.run();
                }
            });
        }
        return btnClose;
    }



    private void pause() {
        // Pause editor pane
        UIAppender.getEditor().pause();

        // Hide button pause
        getBtnPause().setVisible(false);

        // Show button resume
        getBtnResume().setVisible(true);

        // Initialize focus to get focus on editor pane
        initFocus();
    }



    private void resume() {
        // Resume editor pane
        UIAppender.getEditor().resume();

        // Scroll to the bottom
        getJspEditorPane().getVerticalScrollBar().setValue(getJspEditorPane().getVerticalScrollBar().getMaximum());

        // Hide button resume
        getBtnResume().setVisible(false);

        // Show button pause
        getBtnPause().setVisible(true);

        // Initialize focus to get focus on editor pane
        initFocus();
    }



    /**
     * Get logs dialog singleton
     * 
     * @return Logs dialog singleton
     */
    public static LogsDialog getInstance() {
        synchronized (LogsDialog.class) {
            if (instance == null) {
                instance = new LogsDialog();
            }
            return instance;
        }
    }



    /**
     * Called to install logs dialog
     */
    public void install() {
        // When CTRL + F12, hide or show logs dialog
        EDT.postEvent(RegisterKeyEvent.forAction(hideShowAction).keyCode(KeyEvent.VK_F12).ctrlDown());
        EDT.subscribe(new LogsRequestWatcher()).forEvent(FwkEvents.LOGS_REQUEST_EVT);
    }

    /**
     * Action to hide or show logs dialog
     * 
     * @author jeremy.scafi
     * 
     */
    private class HideShowLogsAction implements Runnable {

        public void run() {
            JFrame frame = getInstance();
            if (frame.isVisible()) {
                // Hide logs dialog
                frame.setVisible(false);
            } else {
                // Close all dialogs to see logs dialog
                EDT.postEvent(new CloseAllDialogsEvt());

                // Maximize logs dialog
                frame.setSize(DialogUtil.getScreenSize());
                frame.setLocation(0, 0);

                // Set logs dialog always on top
                frame.setAlwaysOnTop(true);

                // Set logs dialog visibility
                frame.setVisible(true);

                // Restore frame if minimized
                frame.setState(JFrame.NORMAL);

                // Resume
                resume();
            }
        }
    }

    /**
     * Event subscriber to watch when hide or show logs dialog
     * 
     * @author jeremy.scafi
     * 
     */
    private class LogsRequestWatcher extends DefaultEventSubscriber {

        public void eventReceived(Event e) {
            new SwingInvoker() {

                public void run() {
                    hideShowAction.run();
                }
            }.invokeLater();
        }
    }
}
