package com.dolphland.client.util.table;

/**
 * <p>
 * Listener that is notified by {@link ProductTable} instances
 * </p>
 * 
 * @author JayJay
 */
public class ProductTableListener {

    /**
     * <p>
     * Innvoked when the {@link ProductTable} columns have been resized
     * </p>
     * <p>
     * Columns sizes can be accessedwithin the provided event. </a>
     * 
     * @param pte
     *            The event that contains {@link ProductTable} relevant
     *            information
     */
    public void columnsResized(ProductTableEvent pte) {

    }

}
