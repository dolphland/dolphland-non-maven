package com.dolphland.client.util.exception;

/**
 * Exception for infrastructure
 * 
 * @author JayJay
 * 
 */
public class AppInfrastructureException extends AppException {

    public AppInfrastructureException() {
        super();
    }



    public AppInfrastructureException(String pattern, Object... args) {
        super(pattern, args);
    }



    public AppInfrastructureException(String pattern, Throwable cause, Object... args) {
        super(pattern, cause, args);
    }



    public AppInfrastructureException(String message, Throwable cause) {
        super(message, cause);
    }



    public AppInfrastructureException(String message) {
        super(message);
    }



    public AppInfrastructureException(Throwable cause) {
        super(cause);
    }

}
