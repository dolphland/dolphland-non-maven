/*
 * $Log: SwingInvoker.java,v $
 * Revision 1.2 2014/03/18 14:10:00 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * - externalized translatable strings
 * Revision 1.1 2008/11/14 15:07:54 bvatan
 * - initial check in
 */

package com.dolphland.client.util.transaction;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * Classe utilitaire qui simplifie l'utilisation de
 * <code>SwingUtilities.invokeLater()</code> et
 * <code>SwingUtilities.invokeAndWait()</code>.<br>
 * Les exception sont g�r�es par d�faut et enroul�es dans des
 * <code>VisionInfrastructureException</code> non check�es.
 * 
 * @author JayJay
 */
public abstract class SwingInvoker implements Runnable {

    private static final Logger log = Logger.getLogger(SwingInvoker.class);



    /**
     * Helper pour SwingUtilities.invokeAndWait()
     */
    public void invokeAndWait() {
        try {
            if (SwingUtilities.isEventDispatchThread()) {
                run();
            } else {
                SwingUtilities.invokeAndWait(this);
            }
        } catch (Exception e) {
            log.error("invokeAndWait(): Invoking SwingInvoker has failed", e); //$NON-NLS-1$
            throw new AppInfrastructureException("Invoking SwingInvoker has failed", e); //$NON-NLS-1$
        }
    }



    /**
     * Helper pour SwingUtilities.invokeLater()
     */
    public void invokeLater() {
        try {
            SwingUtilities.invokeLater(this);
        } catch (Exception e) {
            log.error("invokeLater(): L'invocation du SwingInvoker � �chou�", e); //$NON-NLS-1$
            throw new AppInfrastructureException("Invoking SwingInvoker has failed", e); //$NON-NLS-1$
        }
    }

}
