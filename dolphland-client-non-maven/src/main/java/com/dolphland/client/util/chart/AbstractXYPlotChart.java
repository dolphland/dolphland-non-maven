package com.dolphland.client.util.chart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Shape;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.general.SeriesDataset;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

/**
 * Cette classe permet de repr�senter un chart de type XYPlot avec une l�gende
 * am�lior�e.
 * 
 * La principale am�lioration de cette l�gende est de pouvoir d�cocher certaines
 * s�ries pour ne pas les voir appara�tre sur le chart.
 * 
 * La repr�sentation du Chart se fait dans le JComponent componentChart que l'on
 * peut obtenir par getChartComponent()
 * 
 * Possibilit� d'y d�finir :
 * - des entit�s de donn�es. Une entit� de donn�e est une s�rie identifi�e par
 * un Comparable, constitu�e de valeurs (abscisse, ordonn�e)
 * - des markers horizontaux. Un marker horizontal permet de distinguer une zone
 * du chart comprise dans un intervale d'abscisses ou une valeur d'abscisses. La
 * distinction se fait par du texte et/ou par une couleur
 * - des markers verticaux. Un marker vertical permet de distinguer une zone du
 * chart comprise dans un intervale d'ordonn�es ou une valeur d'ordonn�e. La
 * distinction se fait par du texte et/ou par une couleur
 * - des valeurs fixes. S�rie de donn�es ne contenant qu'une seule valeur. On
 * utilise des valeurs fixes pour repr�senter un seuil.
 * 
 * @author jeremy.scafi
 */
abstract class AbstractXYPlotChart extends AbstractChart {

    // Alignements du texte
    public final static int ALIGN_TEXT_CENTER = 0;
    public final static int ALIGN_TEXT_CENTER_LEFT = 1;
    public final static int ALIGN_TEXT_CENTER_RIGHT = 2;
    public final static int ALIGN_TEXT_TOP = 3;
    public final static int ALIGN_TEXT_TOP_LEFT = 4;
    public final static int ALIGN_TEXT_TOP_RIGHT = 5;
    public final static int ALIGN_TEXT_BOTTOM = 6;
    public final static int ALIGN_TEXT_BOTTOM_LEFT = 7;
    public final static int ALIGN_TEXT_BOTTOM_RIGHT = 8;

    public final static Color NO_COLOR_MARKER = new Color(0, 0, 0, 0);
    private final static Font FONT_MARKER = new Font("SansSerif", Font.ITALIC + Font.BOLD, 10); //$NON-NLS-1$

    // Liste des valeurs fixes
    private List listFixValues;

    // Liste des marker de domaine
    private List listHorizontalMarkers;
    private List listVerticalMarkers;

    private Double minAbs, maxAbs;
    private Double minOrd, maxOrd;

    // Indique si le premier auto pack sur l'axe des X et des Y a �t� r�alis�
    protected boolean firstAutoPackAxeXRealise;
    protected boolean firstAutoPackAxeYRealise;

    // DataSet du chart
    private XYSeriesCollection dataset;



    /**
     * Constructeur
     */
    public AbstractXYPlotChart() {
        this(new JPanel(), new ChartConstraints());
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AbstractXYPlotChart(ChartConstraints chartConstraints) {
        this(new JPanel(), chartConstraints);
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AbstractXYPlotChart(JPanel componentChart) {
        this(componentChart, new ChartConstraints());
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param componentChart
     *            : Composant devant repr�senter le Chart
     * @param chartConstraints
     *            : Contraintes � appliquer au Chart
     */
    public AbstractXYPlotChart(JPanel componentChart, ChartConstraints chartConstraints) {
        super(componentChart, chartConstraints);
        listFixValues = new ArrayList();
        listHorizontalMarkers = new ArrayList();
        listVerticalMarkers = new ArrayList();
    }



    /** Fournit le DataSet du chart */
    protected XYDataset getDataSet() {
        if (dataset == null) {
            dataset = new XYSeriesCollection();
        }
        return dataset;
    }



    /** Initialise le DataSet du chart */
    protected void initDataSet() {
        dataset.removeAllSeries();
    }



    /** Renvoie la s�rie d'indicie indSeries du chart */
    protected XYSeries getSeries(int indSeries) {
        return dataset.getSeries(indSeries);
    }



    /** Ajoute une s�rie au chart */
    protected void addSeries(XYSeries series) {
        dataset.addSeries(series);
    }



    /**
     * Initialise le chart
     */
    public void initChart() {
        // Indique que le premier auto pack sur l'axe des Y n'a pas �t� r�alis�
        firstAutoPackAxeYRealise = false;
        firstAutoPackAxeXRealise = false;

        // On rend visible toutes les s�ries
        for (ChartData chartData : getAllChartData()) {
            chartData.setVisible(true);
        }

        // Enl�ve toutes les s�ries
        initDataSet();

        // Enl�ve les valeurs fixes
        listFixValues = new ArrayList();

        // R�cup�re les donn�es
        XYPlot plot = getChart().getXYPlot();

        // Enl�ve les markers horizontaux
        if (listHorizontalMarkers != null) {
            Iterator itMarkers = listHorizontalMarkers.iterator();
            while (itMarkers.hasNext()) {
                plot.removeDomainMarker((Marker) itMarkers.next());
            }
        }
        listHorizontalMarkers = new ArrayList();

        // Enl�ve les markers verticaux
        if (listVerticalMarkers != null) {
            Iterator itMarkers = listVerticalMarkers.iterator();
            while (itMarkers.hasNext()) {
                plot.removeRangeMarker((Marker) itMarkers.next());
            }
        }
        listVerticalMarkers = new ArrayList();

        // Initialise l'axe des abscisses et l'axe des ordonn�es
        minAbs = null;
        maxAbs = null;
        minOrd = null;
        maxOrd = null;

        super.initChart();
    }



    /**
     * Renvoie la liste des items de la l�gende
     */
    protected LegendItem[] getLegendItems() {
        XYPlot plot = getChart().getXYPlot();
        XYItemRenderer renderer = plot.getRenderer();

        LegendItem[] legendItems = new LegendItem[renderer.getLegendItems().getItemCount()];
        for (int i = 0; i < legendItems.length; i++) {
            legendItems[i] = renderer.getLegendItems().get(i);
        }

        return legendItems;
    }



    /**
     * Cr�e l'impl�mentation du listener permettant de modifier les contraintes
     * du graphe en temps r�el
     */
    protected ChartConstraintsListener createChartConstraintsListenerImpl() {
        return new XYPlotChartConstraintsListenerImpl();
    }



    /**
     * Ajoute un marker horizontal, identifi� par un titre, une valeur, une
     * couleur et un alignement du titre
     */
    public void addHorizontalMarker(String markerTitle, double valMarker, Color colorMarker, int alignementTexte) {
        Marker marker = new ValueMarker(valMarker);
        marker.setPaint(colorMarker);
        marker.setLabelFont(FONT_MARKER);
        marker.setLabel(markerTitle);

        applyMarkerAlignementTexte(marker, alignementTexte);

        // Ajoute le marker de domaine
        XYPlot plot = getChart().getXYPlot();
        plot.addDomainMarker(marker);

        listHorizontalMarkers.add(marker);
    }



    /**
     * Ajoute un marker horizontal, identifi� par un titre, un intervalle, une
     * couleur et un alignement du titre
     */
    public void addHorizontalMarker(String markerTitle, double intervMinMarker, double intervMaxMarker, Color colorMarker, int alignementTexte) {
        Marker marker = new IntervalMarker(intervMinMarker, intervMaxMarker, colorMarker, new BasicStroke(2.0f), null, null, 1.0f);
        marker.setLabelFont(FONT_MARKER);
        marker.setLabel(markerTitle);

        applyMarkerAlignementTexte(marker, alignementTexte);

        // Ajoute le marker de domaine
        XYPlot plot = getChart().getXYPlot();
        plot.addDomainMarker(marker);

        listHorizontalMarkers.add(marker);
    }



    /**
     * Ajoute un marker vertical, identifi� par un titre, une valeur, une
     * couleur et un alignement du titre
     */
    public void addVerticalMarker(String markerTitle, double valMarker, Color colorMarker, int alignementTexte) {
        Marker marker = new ValueMarker(valMarker);
        marker.setPaint(colorMarker);
        marker.setLabelFont(FONT_MARKER);
        marker.setLabel(markerTitle);

        applyMarkerAlignementTexte(marker, alignementTexte);

        // Ajoute le marker de domaine
        XYPlot plot = getChart().getXYPlot();
        plot.addRangeMarker(marker);

        listVerticalMarkers.add(marker);
    }



    /**
     * Ajoute un marker vertical, identifi� par un titre, un intervalle, une
     * couleur et un alignement du titre
     */
    public void addVerticalMarker(String markerTitle, double intervMinMarker, double intervMaxMarker, Color colorMarker, int alignementTexte) {
        Marker marker = new IntervalMarker(intervMinMarker, intervMaxMarker, colorMarker, new BasicStroke(2.0f), null, null, 1.0f);
        marker.setLabelFont(FONT_MARKER);
        marker.setLabel(markerTitle);

        applyMarkerAlignementTexte(marker, alignementTexte);

        // Ajoute le marker de domaine
        XYPlot plot = getChart().getXYPlot();
        plot.addRangeMarker(marker);

        listVerticalMarkers.add(marker);
    }



    /** Applique au Marker l'alignement de texte */
    private void applyMarkerAlignementTexte(Marker marker, int alignementTexte) {

        switch (alignementTexte) {
            case ALIGN_TEXT_BOTTOM :
                marker.setLabelAnchor(RectangleAnchor.BOTTOM);
                marker.setLabelTextAnchor(TextAnchor.BOTTOM_CENTER);
                break;

            case ALIGN_TEXT_BOTTOM_LEFT :
                marker.setLabelAnchor(RectangleAnchor.BOTTOM_LEFT);
                marker.setLabelTextAnchor(TextAnchor.BOTTOM_LEFT);
                break;

            case ALIGN_TEXT_BOTTOM_RIGHT :
                marker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
                marker.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);
                break;

            case ALIGN_TEXT_TOP :
                marker.setLabelAnchor(RectangleAnchor.TOP);
                marker.setLabelTextAnchor(TextAnchor.TOP_CENTER);
                break;

            case ALIGN_TEXT_TOP_LEFT :
                marker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
                marker.setLabelTextAnchor(TextAnchor.TOP_LEFT);
                break;

            case ALIGN_TEXT_TOP_RIGHT :
                marker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
                marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
                break;

            case ALIGN_TEXT_CENTER :
                marker.setLabelAnchor(RectangleAnchor.CENTER);
                marker.setLabelTextAnchor(TextAnchor.CENTER);
                break;

            case ALIGN_TEXT_CENTER_LEFT :
                marker.setLabelAnchor(RectangleAnchor.LEFT);
                marker.setLabelTextAnchor(TextAnchor.CENTER_LEFT);
                break;

            case ALIGN_TEXT_CENTER_RIGHT :
                marker.setLabelAnchor(RectangleAnchor.RIGHT);
                marker.setLabelTextAnchor(TextAnchor.CENTER_RIGHT);
                break;

            default :
                marker.setLabelAnchor(RectangleAnchor.BOTTOM);
                marker.setLabelTextAnchor(TextAnchor.BOTTOM_CENTER);
                break;
        }
    }



    /**
     * Ajoute une valeur fixe pour l'entit� de donn�es identifi�e par
     * idChartData
     */
    public void addFixData(Comparable idChartData, double ord) {
        listFixValues.add(new FixValue(idChartData, ord));

        // On r�cup�re l'entit� des donn�es identifi�e par idChartData
        XYChartData chartData = (XYChartData) getChartData(idChartData);

        // Si elle n'existe pas, on la cr�e et on l'ajoute
        if (chartData == null) {
            chartData = new XYChartData(idChartData);
        }

        // Stockage des identifiants des s�ries d�j� cr��es (pour �viter une
        // erreur de modif concurrentielle)
        List idSeriesCrees = new ArrayList();
        for (int i = 0; i < getDataSet().getSeriesCount(); i++) {
            if (!idChartData.equals(getDataSet().getSeriesKey(i))) {
                idSeriesCrees.add(getSeries(i).getKey());
            }
        }

        // Si range abscisse impos�, on ajoute la valeur fixe � l'abscisse min
        // et l'abscisse max
        if (getChartConstraints().getAxeXRangeMin() != null) {
            chartData.addValue(getChartConstraints().getAxeXRangeMin(), new Double(ord));
        }

        if (getChartConstraints().getAxeXRangeMax() != null) {
            chartData.addValue(getChartConstraints().getAxeXRangeMax(), new Double(ord));
        }

        // Parcourt des identifiants des s�ries d�j� cr��es
        Iterator itIdSeriesCrees = idSeriesCrees.iterator();
        while (itIdSeriesCrees.hasNext()) {
            String idSeries = (String) itIdSeriesCrees.next();

            if (getChartData(idSeries) != null) {
                // On ajoute � chaque abscisse de la s�rie la valeur fixe
                Iterator itItems = ((XYChartData) getChartData(idSeries)).getXYSeries().getItems().iterator();

                while (itItems.hasNext()) {
                    XYDataItem item = (XYDataItem) itItems.next();
                    chartData.addValue(new Double(item.getXValue()), new Double(ord));
                }
            }
        }

        // Si au moins une valeur fixe
        if (listFixValues.size() > 0) {
            XYPlot plot = getChart().getXYPlot();
            // Si contrainte de pas sur axe y non renseign�, on r�active l'auto
            // tick
            if (getChartConstraints().getAxeYPas() == null && plot.getRangeAxis() instanceof NumberAxis) {
                ((NumberAxis) plot.getRangeAxis()).setAutoTickUnitSelection(true);
            }
        }

        putChartData(chartData);
    }



    /** Ajoute une valeur pour l'entit� de donn�es identifi�e par idChartData */
    public void addData(Comparable idChartData, double abs, double ord) {
        // On r�cup�re l'entit� des donn�es identifi�e par idChartData
        XYChartData chartData = (XYChartData) getChartData(idChartData);

        // Si elle n'existe pas, on la cr�e et on l'ajoute
        if (chartData == null) {
            chartData = new XYChartData(idChartData);
        }

        // On y ajoute les donn�es pass�es en param�tre
        chartData.addValue(new Double(abs), new Double(ord));

        // Ajoute la valeur au chart
        addData(chartData, abs, ord);
    }



    /** Ajoute une valeur pour le chartData */
    protected void addData(ChartData chartData, double abs, double ord) {
        XYPlot plot = getChart().getXYPlot();

        // Fixe l'axe des abscisses entre le min et max des ordonn�es si non
        // fix�
        if (!getChartConstraints().isAxeXRangeAutoPackable() && (getChartConstraints().getAxeXRangeMin() == null || getChartConstraints().getAxeXRangeMax() == null)) {
            if (minAbs == null || minAbs.doubleValue() >= abs)
                minAbs = new Double(abs);
            if (maxAbs == null || maxAbs.doubleValue() <= abs)
                maxAbs = new Double(abs);
            // Si au moins deux abscisses, on fixe le range
            if (maxAbs.doubleValue() != minAbs.doubleValue()) {
                plot.getDomainAxis().setRange(minAbs.doubleValue(), maxAbs.doubleValue());
                // Si contrainte de pas sur axe x non renseign�, on r�active
                // l'auto tick
                if (getChartConstraints().getAxeXPas() == null && plot.getDomainAxis() instanceof NumberAxis) {
                    ((NumberAxis) plot.getDomainAxis()).setAutoTickUnitSelection(true);
                }
            }
            // Si premi�re abscisse, on force l'autotick � 1 si pas de
            // contraintes de pas
            else {
                // Si contrainte de pas sur axe x non renseign�, on fixe le tick
                // � 1
                if (getChartConstraints().getAxeXPas() == null && plot.getDomainAxis() instanceof NumberAxis) {
                    ((NumberAxis) plot.getDomainAxis()).setTickUnit(new NumberTickUnit(1));
                }
            }
        }

        // Fixe l'axe des ordonn�es entre le min et max des ordonn�es si non
        // fix�
        if (!getChartConstraints().isAxeYRangeAutoPackable() && (getChartConstraints().getAxeYRangeMin() == null || getChartConstraints().getAxeYRangeMax() == null)) {
            if (minOrd == null || minOrd.doubleValue() >= ord)
                minOrd = new Double(ord);
            if (maxOrd == null || maxOrd.doubleValue() <= ord)
                maxOrd = new Double(ord);
            // Si au moins deux ordonn�es
            if (maxOrd.doubleValue() != minOrd.doubleValue()) {
                plot.getRangeAxis().setRangeWithMargins(minOrd.doubleValue(), maxOrd.doubleValue());
                // Si contrainte de pas sur axe y non renseign�, on r�active
                // l'auto tick
                if (getChartConstraints().getAxeYPas() == null && plot.getRangeAxis() instanceof NumberAxis) {
                    ((NumberAxis) plot.getRangeAxis()).setAutoTickUnitSelection(true);
                }
            }
            // Si premi�re ordonn�e, on force l'autotick � 1 si pas de
            // contraintes de pas
            else {
                // Si contrainte de pas sur axe y non renseign�, on fixe le tick
                // � 1
                if (getChartConstraints().getAxeYPas() == null && plot.getRangeAxis() instanceof NumberAxis) {
                    ((NumberAxis) plot.getRangeAxis()).setTickUnit(new NumberTickUnit(1));
                }
            }
        }

        // Parcourt des valeurs fixes pour les ajouter � l'abscisse abs
        Iterator itFixValues = listFixValues.iterator();
        while (itFixValues.hasNext()) {
            FixValue fixValue = (FixValue) itFixValues.next();

            XYChartData fixValueChartData = (XYChartData) getChartData(fixValue.idFixValue);
            fixValueChartData.addValue(new Double(abs), new Double(fixValue.value));
        }

        // Si au moins une valeur fixe
        if (listFixValues.size() > 0) {
            // Si contrainte de pas sur axe y non renseign�, on r�active l'auto
            // tick
            if (getChartConstraints().getAxeYPas() == null && plot.getRangeAxis() instanceof NumberAxis) {
                ((NumberAxis) plot.getRangeAxis()).setAutoTickUnitSelection(true);
            }
        }

        putChartData(chartData);
    }

    /**
     * Entit� de donn�es propre � un LineChart
     * 
     * @author jeremy.scafi
     */
    private class XYChartData extends ChartData {

        private XYSeries series;
        private List listAbs;
        private List listOrd;
        private int indChartData;



        public XYChartData(Comparable idChartData) {
            super(idChartData, getChartDataCount());
            series = new XYSeries(idChartData);
            this.listAbs = new ArrayList();
            this.listOrd = new ArrayList();
            this.indChartData = getChartDataCount();
        }



        public XYSeries getXYSeries() {
            return series;
        }



        public void addValue(Number abs, Number ord) {
            // Si premi�re valeur, on ajoute la s�rie de donn�es
            if (listAbs.size() == 0) {
                addSeries(series);
            }
            // Si l'abscisse existe d�j�, on met � jour
            if (listAbs.contains(abs)) {
                listOrd.set(listAbs.indexOf(abs), ord);
                series.update(abs, ord);
            }
            // Si l'abscisse n'existe pas, on cr�e
            else {
                listAbs.add(abs);
                listOrd.add(ord);
                series.add(abs, ord);
            }

            // Si le range de l'axe des X est auto packable
            if (getChartConstraints().isAxeXRangeAutoPackable()) {
                ValueAxis rangeX = getChart().getXYPlot().getDomainAxis();

                // Si range jamais initialis� par l'auto pack, on le d�limite
                // sur l'abscisse
                if (!firstAutoPackAxeXRealise) {
                    rangeX.setRange(abs.doubleValue() - getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue(), abs.doubleValue() + getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue());
                    firstAutoPackAxeXRealise = true;
                }
                // Sinon, on fait en sorte que l'abscisse soit dans le range
                else {
                    rangeX.setRange(Math.min(rangeX.getLowerBound(), abs.doubleValue() - getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue()), Math.max(rangeX.getUpperBound(), abs.doubleValue() + getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue()));
                }
            }

            // Si le range de l'axe des Y est auto packable
            if (getChartConstraints().isAxeYRangeAutoPackable()) {
                ValueAxis rangeY = getChart().getXYPlot().getRangeAxis();

                // Si range jamais initialis� par l'auto pack, on le d�limite
                // sur l'ordonn�e
                if (!firstAutoPackAxeYRealise) {
                    rangeY.setRange(ord.doubleValue() - getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue(), ord.doubleValue() + getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue());
                    firstAutoPackAxeYRealise = true;
                }
                // Sinon, on fait en sorte que l'ordonn�e soit dans le range
                else {
                    rangeY.setRange(Math.min(rangeY.getLowerBound(), ord.doubleValue() - getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue()), Math.max(rangeY.getUpperBound(), ord.doubleValue() + getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue()));
                }
            }
        }



        protected void hideAllValues() {
            // Cache toutes les valeurs
            XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
            renderer.setSeriesVisible(indChartData, false);
        }



        protected void showAllValues() {
            // Affiche toutes les valeurs
            XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
            renderer.setSeriesVisible(indChartData, true);
        }
    }

    /**
     * Impl�mentation du listener permettant de modifier les contraintes du
     * graphe en temps r�el
     * 
     * @author jeremy.scafi
     */
    private class XYPlotChartConstraintsListenerImpl extends DefaultChartConstraintsListenerImpl {

        /** Fixe le titre de l'axe X du chart */
        @Override
        public void doChangeAxeXTitle() {
            getChart().getXYPlot().getDomainAxis().setLabel(getChartConstraints().getAxeXTitle());
            super.doChangeAxeXTitle();
        }



        /** Fixe le titre de l'axe Y du chart */
        @Override
        public void doChangeAxeYTitle() {
            getChart().getXYPlot().getRangeAxis().setLabel(getChartConstraints().getAxeYTitle());
            super.doChangeAxeYTitle();
        }



        /** Fixe la visibilit� de l'axe X du chart */
        @Override
        public void doChangeAxeXVisible() {
            getChart().getXYPlot().getDomainAxis().setTickLabelsVisible(getChartConstraints().isAxeXVisible());
            getChart().getXYPlot().getDomainAxis().setTickMarksVisible(getChartConstraints().isAxeXVisible());
            super.doChangeAxeXVisible();
        }



        /** Fixe la visibilit� de l'axe Y du chart */
        @Override
        public void doChangeAxeYVisible() {
            getChart().getXYPlot().getRangeAxis().setTickLabelsVisible(getChartConstraints().isAxeYVisible());
            getChart().getXYPlot().getRangeAxis().setTickMarksVisible(getChartConstraints().isAxeYVisible());
            super.doChangeAxeYVisible();
        }



        /** Indique le min et le max du range de l'axe X */
        @Override
        public void doChangeAxeXRange() {
            // Fixe le range de l'axe X si d�finit dans les contraintes
            if (getChartConstraints().getAxeXRangeMin() != null && getChartConstraints().getAxeXRangeMax() != null)
                getChart().getXYPlot().getDomainAxis().setRange(getChartConstraints().getAxeXRangeMin().doubleValue(), getChartConstraints().getAxeXRangeMax().doubleValue());
            else
                getChart().getXYPlot().getDomainAxis().setAutoRange(true);
            super.doChangeAxeXRange();
        }



        /** Indique le min et le max du range de l'axe Y */
        @Override
        public void doChangeAxeYRange() {
            // Fixe le range de l'axe Y si d�finit dans les contraintes
            if (getChartConstraints().getAxeYRangeMin() != null && getChartConstraints().getAxeYRangeMax() != null)
                getChart().getXYPlot().getRangeAxis().setRange(getChartConstraints().getAxeYRangeMin().doubleValue(), getChartConstraints().getAxeYRangeMax().doubleValue());
            else
                getChart().getXYPlot().getRangeAxis().setAutoRange(true);
            super.doChangeAxeYRange();
        }



        /** Fixe le pas entre chaque valeur de l'axe des abscisses */
        @Override
        public void doChangeAxeXPas() {
            XYPlot plot = getChart().getXYPlot();
            if (getChartConstraints().getAxeXPas() != null) {
                if (plot.getDomainAxis() instanceof NumberAxis) {
                    ((NumberAxis) plot.getDomainAxis()).setTickUnit(new NumberTickUnit(getChartConstraints().getAxeXPas().doubleValue()));
                }
            }
            super.doChangeAxeXPas();
        }



        /** Fixe le pas entre chaque valeur de l'axe des ordonn�es */
        @Override
        public void doChangeAxeYPas() {
            XYPlot plot = getChart().getXYPlot();
            // Fixe le pas de l'axe des ordonn�es si indiqu�
            if (getChartConstraints().getAxeYPas() != null) {
                if (plot.getRangeAxis() instanceof NumberAxis) {
                    ((NumberAxis) plot.getRangeAxis()).setTickUnit(new NumberTickUnit(getChartConstraints().getAxeYPas().doubleValue()));
                }
            }
            super.doChangeAxeYPas();
        }



        /** Fixe les couleurs des donn�es du chart */
        @Override
        public void doChangeColorsChartData() {
            XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
            if (getChartConstraints().getColorsChartData() != null) {
                for (int i = 0; i < getChartConstraints().getColorsChartData().length; i++) {
                    renderer.setSeriesPaint(i, getChartConstraints().getColorsChartData()[i]);
                }
            }
            super.doChangeColorsChartData();
        }



        /** Fixe les formes des donn�es du chart */
        @Override
        public void doChangeShapesChartData() {
            XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
            if (getChartConstraints().getShapesChartData() != null) {
                for (int i = 0; i < getChartConstraints().getShapesChartData().length; i++) {
                    renderer.setSeriesShape(i, createShape(getChartConstraints().getShapesChartData()[i]));
                }
            }
            super.doChangeColorsChartData();
        }
    }

    private class FixValue {

        Comparable idFixValue;
        double value;



        public FixValue(Comparable idFixValue, double value) {
            this.idFixValue = idFixValue;
            this.value = value;
        }
    }

    /**
     * Renderer pour g�rer la s�lection d'une aire dans le graphe
     * 
     * @author JayJay
     * 
     */
    protected class AreaSelectionRenderer extends XYAreaRenderer implements ChartSelection {

        private Integer selectedSeries = null;
        private Integer selectedData = null;
        private SeriesDataset dataSet = null;



        public AreaSelectionRenderer(SeriesDataset dataSet) {
            super(AREA_AND_SHAPES);
            this.dataSet = dataSet;
        }



        public boolean isSelectionEnabled(ChartEntity entity) {
            if (entity instanceof XYItemEntity) {
                return ((XYItemEntity) entity).getDataset().equals(dataSet);
            }
            return false;
        }



        public boolean isSelectionActive() {
            return selectedSeries != null && selectedData != null;
        }



        public Integer getSelectedSeries() {
            return selectedSeries;
        }



        public Integer getSelectedData() {
            return selectedData;
        }



        public void setSelectedSeries(Integer indSeries) {
            this.selectedSeries = indSeries;
        }



        public void setSelectedData(Integer indData) {
            this.selectedData = indData;
        }



        public void clearSelection() {
            this.selectedSeries = null;
            this.selectedData = null;
        }



        private boolean isSelected(int series, int item) {
            if (!isSelectionActive()) {
                return false;
            }
            return series == selectedSeries && item == selectedData;
        }



        @Override
        public Paint getItemPaint(int row, int column) {
            if (isSelected(row, column)) {
                return getChartConstraints().getSelectionColor();
            }
            return super.getItemPaint(row, column);
        }



        @Override
        public Shape getItemShape(int row, int column) {
            if (isSelected(row, column)) {
                return createShape(ChartShape.CIRCLE);
            }
            return createShape(ChartShape.DOT);
        }



        @Override
        public boolean getItemVisible(int series, int item) {
            if (isSelected(series, item)) {
                return true;
            }
            return super.getItemVisible(series, item);
        }
    }

    /**
     * Renderer pour g�rer la s�lection d'une ligne ou d'un point dans le graphe
     * 
     * @author JayJay
     * 
     */
    protected class LineAndShapeSelectionRenderer extends XYLineAndShapeRenderer implements ChartSelection {

        private Integer selectedSeries = null;
        private Integer selectedData = null;
        private SeriesDataset dataSet = null;



        public LineAndShapeSelectionRenderer(SeriesDataset dataSet, boolean lines, boolean shapes) {
            super(lines, shapes);
            this.dataSet = dataSet;
        }



        public boolean isSelectionEnabled(ChartEntity entity) {
            if (entity instanceof XYItemEntity) {
                return ((XYItemEntity) entity).getDataset().equals(dataSet);
            }
            return false;
        }



        public boolean isSelectionActive() {
            return selectedSeries != null && selectedData != null;
        }



        public Integer getSelectedSeries() {
            return selectedSeries;
        }



        public Integer getSelectedData() {
            return selectedData;
        }



        public void setSelectedSeries(Integer indSeries) {
            this.selectedSeries = indSeries;
        }



        public void setSelectedData(Integer indData) {
            this.selectedData = indData;
        }



        public void clearSelection() {
            this.selectedSeries = null;
            this.selectedData = null;
        }



        private boolean isSelected(int series, int item) {
            if (!isSelectionActive()) {
                return false;
            }
            return series == selectedSeries && item == selectedData;
        }



        @Override
        public boolean getItemShapeFilled(int series, int item) {
            if (isSelected(series, item)) {
                return false;
            }
            return super.getItemShapeFilled(series, item);
        }



        @Override
        public Paint getItemPaint(int row, int column) {
            if (isSelected(row, column)) {
                return getChartConstraints().getSelectionColor();
            }
            return super.getItemPaint(row, column);
        }



        @Override
        public Shape getItemShape(int series, int item) {
            Shape shape = super.getItemShapeVisible(series, item) ? super.getItemShape(series, item) : null;
            if (shape == null && isSelected(series, item)) {
                return createShape(ChartShape.CIRCLE);
            }
            return shape;
        }



        @Override
        public boolean getItemShapeVisible(int series, int item) {
            if (isSelected(series, item)) {
                return true;
            }
            return super.getItemShapeVisible(series, item);
        }
    }
}
