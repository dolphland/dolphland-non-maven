package com.dolphland.client.util.chart;

import org.jfree.chart.entity.ChartEntity;

/**
 * Interface to select one data from series in a chart
 * 
 * @author JayJay
 * 
 */
interface ChartSelection {

    /**
     * Tell if selection is enabled for this entity
     * 
     * @param entity
     *            Entity
     * @return True if selection is enabled for this entity
     */
    public boolean isSelectionEnabled(ChartEntity entity);



    /**
     * Tell if selection is active
     * 
     * @return True if selection is active
     */
    public boolean isSelectionActive();



    /**
     * Get selected series
     * 
     * @return Indice for series selected
     */
    public Integer getSelectedSeries();



    /**
     * Get selected data
     * 
     * @return Indice for data selected
     */
    public Integer getSelectedData();



    /**
     * Set selected series
     * 
     * @param indSeries
     *            Indice for series
     */
    public void setSelectedSeries(Integer indSeries);



    /**
     * Set selected data
     * 
     * @param indData
     *            Indice for data
     */
    public void setSelectedData(Integer indData);



    /**
     * Clear selection
     */
    public void clearSelection();
}
