package com.dolphland.client.util.flip;

/**
 * Interface use to know when an application is loaded
 * 
 * @author p-jeremy.scafi
 * 
 */
public interface FlipApplicationLoader {

    /**
     * Tell if application is loaded or not
     * 
     * @return True if application is loaded
     */
    public boolean isApplicationLoaded();
}
