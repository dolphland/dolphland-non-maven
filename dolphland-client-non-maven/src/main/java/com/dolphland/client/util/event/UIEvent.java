/*
 * $Log: UIEvent.java,v $
 * Revision 1.3 2009/03/05 15:40:15 bvatan
 * - ajout constructeur prenant un EventId pour compatibilit� descendante
 * Revision 1.2 2009/03/05 15:13:04 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.1 2008/07/07 11:30:43 bvatan
 * - Ajout des �v�nements de UI (UIEvent)
 */

package com.dolphland.client.util.event;

public class UIEvent extends Event {

    public UIEvent(EventId evtId) {
        this((EventDestination) evtId);
    }



    public UIEvent(EventDestination evtId) {
        super(evtId);
        setEventType(UI_EVENT_TYPE);
    }

}
