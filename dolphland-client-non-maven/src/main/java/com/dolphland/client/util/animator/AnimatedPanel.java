package com.dolphland.client.util.animator;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.image.ImageUtil;

/**
 * Panel with animated components
 * 
 * @author JayJay
 */
public class AnimatedPanel extends FwkPanel {

    // Animator
    private AnimatorTask animator;

    // Sequences animations
    private List<SequenceAnimations> sequencesAnimations;

    // Animated components
    private Map<String, AnimatedComponent> animatedComponents;

    // Animate started
    private boolean animationInProgress;



    /**
     * Default constructor
     */
    public AnimatedPanel() {
        sequencesAnimations = new ArrayList<SequenceAnimations>();
        animatedComponents = new LinkedHashMap<String, AnimatedComponent>();
        setLayout(null);
    }



    /**
     * Get animated components count
     * 
     * @return Animated components count
     */
    public int getAnimatedComponentsCount() {
        return animatedComponents.size();
    }



    /**
     * Get all animated components
     * 
     * @return Animated components
     */
    public List<AnimatedComponent> getAllAnimatedComponents() {
        return new ArrayList<AnimatedComponent>(animatedComponents.values());
    }



    /**
     * Get all animated components ids
     * 
     * @return Animated components ids
     */
    public List<String> getAllAnimatedComponentsIds() {
        return new ArrayList<String>(animatedComponents.keySet());
    }



    /**
     * Get animated component for id specified
     * 
     * @param id
     *            Animated component id
     * 
     * @return Animated component
     */
    public AnimatedComponent getAnimatedComponent(String id) {
        return animatedComponents.get(id);
    }



    /**
     * Add animated component for id specified
     * 
     * @param id
     *            Animated component id
     * @param animatedComponent
     *            Animated component
     */
    public void addAnimatedComponent(AnimatedComponent animatedComponent) {
        AssertUtil.notNull(animatedComponent, "animatedComponent is null !");
        add(animatedComponent);
        animatedComponents.put(animatedComponent.getId(), animatedComponent);
    }



    /**
     * Remove animated component for id specified
     * 
     * @param id
     *            Animated component id
     */
    public void removeAnimatedComponent(String id) {
        AnimatedComponent animatedComponent = animatedComponents.remove(id);
        if (animatedComponent != null) {
            remove(animatedComponent);
        }
    }



    /**
     * Remove all animated components
     */
    public void removeAllAnimatedComponents() {
        for (String id : new ArrayList<String>(animatedComponents.keySet())) {
            removeAnimatedComponent(id);
        }
    }



    /**
     * Add an animation
     * 
     * @param animation
     *            Animation
     */
    public void addAnimation(Animation animation) {
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        sequenceAnimations.addAnimation(animation);
        sequencesAnimations.add(sequenceAnimations);
    }



    /**
     * Add animations specified
     * 
     * @param animations
     */
    public void addSequenceAnimations(SequenceAnimations sequenceAnimations) {
        sequencesAnimations.add(sequenceAnimations);
    }



    /**
     * Remove all animations
     */
    public void removeAllAnimations() {
        sequencesAnimations.clear();
    }



    /**
     * Start animate
     */
    public void startAnimate() {
        if (isAnimationInProgress()) {
            stopAnimate();
        }
        if (!isAnimationInProgress() && animator == null) {
            animationInProgress = true;
            animator = new AnimatorTask(this, sequencesAnimations);
            animator.start();
        }
    }



    /**
     * Stop animate
     */
    public void stopAnimate() {
        if (isAnimationInProgress() && animator != null) {
            animationInProgress = false;
            animator.interrupt();
            try {
                animator.join();
            } catch (InterruptedException e) {
            }
            animator = null;
        }
    }



    /**
     * Indicator for animation in progress
     * 
     * @return True if animation in progress
     */
    public boolean isAnimationInProgress() {
        return animationInProgress;
    }



    public static void main(String[] args) {
        // Start EDT
        EDT.start();

        // Create animated panel
        AnimatedPanel animatedPanel = new AnimatedPanel();
        animatedPanel.setBackground(Color.RED);

        // Create and show frame, if frame is closed, stop animation
        JFrame frame = new JFrame();
        frame.setSize(640, 480);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(animatedPanel);
        frame.setVisible(true);

        // Add animated component
        BufferedImage frontImage = ImageUtil.getImage("/images/cards/card_ace_hearts.gif");
        BufferedImage backImage = ImageUtil.getImage("/images/cards/card_back.png");
        AnimatedComponent component = new AnimatedComponent("1", frontImage, backImage);
        animatedPanel.addAnimatedComponent(component);

        // Add first animations sequence
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        DefaultAnimationParameter animationParameter = new DefaultAnimationParameter();
        animationParameter.setX(100);
        animationParameter.setY(200);
        animationParameter.setRatioWidth(2.0d);
        animationParameter.setRatioHeight(2.0d);
        animationParameter.setDegreRotation(-180);
        animationParameter.getActionsBeforeAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("D�but animation " + param.getAnimatedComponentId() + "\t(Temps : " + new Date().getTime() + ")");
            }
        });
        animationParameter.getActionsAfterAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("Fin animation " + param.getAnimatedComponentId() + "\t\t(Temps : " + new Date().getTime() + ")");
            }
        });
        Animation animation = new Animation(component, animationParameter);
        sequenceAnimations.addAnimation(animation);
        animatedPanel.addSequenceAnimations(sequenceAnimations);

        // Add second animations sequence
        sequenceAnimations = new SequenceAnimations(5000);
        animationParameter = new DefaultAnimationParameter();
        animationParameter.setBack(true);
        animationParameter.setDegreRotation(1080);
        animationParameter.setAnimationDurationMs(5000l);
        animationParameter.getActionsBeforeAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("D�but animation " + param.getAnimatedComponentId() + "\t(Temps : " + new Date().getTime() + ")");
            }
        });
        animationParameter.getActionsAfterAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("Fin animation " + param.getAnimatedComponentId() + "\t\t(Temps : " + new Date().getTime() + ")");
            }
        });
        animation = new Animation(component, animationParameter);
        sequenceAnimations.addAnimation(animation);
        animatedPanel.addSequenceAnimations(sequenceAnimations);

        animatedPanel.startAnimate();
    }
}
