package com.dolphland.client.util.animator;

import java.util.ArrayList;
import java.util.List;

/**
 * Sequence animations
 * 
 * @author JayJay
 * 
 */
public class SequenceAnimations {

    private List<Animation> animations;
    private long waitTimeMs;



    /**
     * Default constructor
     */
    public SequenceAnimations() {
        this(0l);
    }



    /**
     * Constructor with wait time in ms specified
     * 
     * @param waitTimeMs
     *            Wait time in ms
     */
    public SequenceAnimations(long waitTimeMs) {
        this.animations = new ArrayList<Animation>();
        this.waitTimeMs = waitTimeMs;
    }



    /**
     * @return the component
     */
    public void addAnimation(Animation animation) {
        animations.add(animation);
    }



    /**
     * Remove animation specified
     * 
     * @param animation
     *            Animation
     */
    public void removeAnimation(Animation animation) {
        animations.remove(animation);
    }



    /**
     * Remove all animations
     */
    public void removeAllAnimations() {
        animations.clear();
    }



    /**
     * Get all animations
     * 
     * @return All animations
     */
    public List<Animation> getAnimations() {
        return new ArrayList<Animation>(animations);
    }



    /**
     * Get wait time in ms
     * 
     * @return Wait time in ms
     */
    public long getWaitTimeMs() {
        return waitTimeMs;
    }
}
