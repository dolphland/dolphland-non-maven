/*
 * $Log: ProductTable.java,v $
 * Revision 1.37 2014/04/15 14:51:47 bvatan
 * - FWK-143: Added support for row sorting when a double click is performed on
 * a ProtuctTable column header
 * Revision 1.36 2014/03/18 15:17:44 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * - externalized translatable strings
 * Revision 1.35 2013/11/25 13:32:26 bvatan
 * - FWK-122: fixed bug that placed tooltip out of screen when set on the last
 * right column
 * Revision 1.34 2013/09/20 09:48:34 bvatan
 * - FWK-54: Reworked to become an UIActionEvtFactory and does now comply with
 * ActionsContext API.
 * - getViewEvent() and createProductActionEvent() are now deprecated,
 * createUIActionEvt() is now the preferred usage
 * Revision 1.33 2013/08/30 13:51:17 bvatan
 * - added printTable method, left private for now as not stable
 * Revision 1.32 2013/08/27 13:23:21 bvatan
 * - FWK-54: remove all references to ProductAction, all replaced by
 * UIAction<ProductActionevt, ?>
 * - Fixed bug that did not handle double-clic properly
 * Revision 1.31 2013/08/27 11:57:01 bvatan
 * - FWK54
 * Revision 1.30 2013/07/03 10:47:59 bvatan
 * - FWK-91: added selectAll() service
 * Revision 1.29 2013/07/03 10:19:22 bvatan
 * - FWK-91: make it possible to select a list of ProductAdapter in the table.
 * When calling setProducts(), the current selection in now preserved.
 * Revision 1.28 2013/07/02 14:25:03 bvatan
 * - FWK-85: line selection fix (slaout)
 * - FWK-90: added support for column resize event (slaout, bvatan)
 * Revision 1.27 2013/07/02 12:46:29 bvatan
 * - FWK85 - fix by Sebastien Laout: ligne selection fix
 * Revision 1.26 2013/04/15 08:09:48 bvatan
 * - D�placement des UIAction et classes associ�s dans
 * com.vallourec.ctiv.fwk.ui.application.actions
 * Revision 1.25 2012/05/02 15:11:22 bvatan
 * - Ajout possibilit� de notifier l'ensemble des actions pour mise � jour
 * de
 * l'�tat visible ou enabled.
 * Revision 1.24 2012/04/19 08:13:25 bvatan
 * - Correction ArrayIndexOutOfBoundsException
 * Revision 1.23 2011/09/26 10:40:16 bvatan
 * - correction gestion de l'affichage des tooltips
 * Revision 1.22 2011/09/09 13:46:49 bvatan
 * - uniformisation des UIActions sur les composants synoptiques
 * Revision 1.21 2010/12/02 08:37:39 bvatan
 * - ajout possibilit� d'ajouter une bulle de description sur une colonne
 * et/ou
 * sur une cellule
 * Revision 1.20 2010/10/04 12:17:37 bvatan
 * - correction d'un bug graphique sur le menu contextuel lorsque la loste de la
 * table changeait
 * Revision 1.19 2010/09/28 08:41:05 bvatan
 * - correction bug
 * Revision 1.18 2010/09/27 09:31:32 bvatan
 * - correction bug sur actions
 * Revision 1.17 2010/09/27 09:24:10 bvatan
 * - correction bug sur actions
 * Revision 1.16 2010/09/24 07:04:37 bvatan
 * - Am�lioration de la s�lection/d�s�lection des lignes pour toujours
 * �tre
 * assur� d'avoir en s�lectionn� la ligne o� la souris est positionn�e
 * lors d'un
 * double clic ou d'un clic droit
 * - Am�lioration de l'affichage du menu contextuel pour mettre en �vidence
 * les
 * menus pouvant se lancer par double clic.
 * Revision 1.15 2010/09/17 11:32:23 bvatan
 * - gestion de la s�lection/d�s�lection des ligne par clic simple (et non
 * ctrl+clic)
 * Revision 1.14 2010/09/16 13:45:55 bvatan
 * - correction bug concernant le isVisible() et isEnabled() des ProductAction
 * Revision 1.12 2010/09/15 06:51:59 bvatan
 * - gestion correcte des UIAction.isEnabled() et isVisible()
 * Revision 1.11 2010/09/08 07:35:07 bvatan
 * - ajout m�thode setSelectedProduct()
 * Revision 1.10 2010/07/29 09:51:27 bvatan
 * - gestion des ProductActionConstraints permettant d'associer des contrainte
 * de positionnement (ou autre) sur les ProductAction
 * Revision 1.9 2010/07/20 11:51:49 bvatan
 * - gestion de l'affichage des ProductAction sur plusieurs lignes/colonnes
 * Revision 1.8 2010/07/20 07:21:44 bvatan
 * - ajout setter setGroupActionCount()
 * Revision 1.7 2010/06/09 09:47:46 bvatan
 * - Ajout possibilit� de positionner les ProductAction sur les 4 c�t�s de
 * de la
 * table
 * Revision 1.6 2009/12/01 15:17:35 bvatan
 * - Fixe le background du JScrollPane en m�me temps que celui de la
 * ProductTable
 * Revision 1.5 2009/10/20 07:32:08 bvatan
 * - ajout getProducts()
 * - ajout setEnabled()
 * Revision 1.4 2009/08/07 12:16:29 bvatan
 * - ajout possibilit�e de grouper des colonnes dans l'ent�te
 * Revision 1.3 2009/03/16 12:07:21 jscafi
 * - Correction bug table
 * Revision 1.2 2009/03/09 08:03:56 jscafi
 * - Refonte des encours synoptique pour utiliser le TableLayout, qui permet de
 * ne pas avoir de bugs d'affichage
 * Revision 1.1 2009/02/04 13:38:39 jscafi
 * - Ajout de ProducTable permettant de g�rer des tables de produits
 * Revision 1.1 2009/01/06 14:17:05 bvatan
 * - ajotu gestion du blocage des barre par coul�e
 */

package com.dolphland.client.util.table;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Chromaticity;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTable.PrintMode;
import javax.swing.SwingUtilities;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

import com.dolphland.client.util.action.ActionsContext;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.action.UIActionEvtFactory;
import com.dolphland.client.util.action.UIActionUtil;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.Schedule;
import com.dolphland.client.util.event.UIHideCtxInfoEvt;
import com.dolphland.client.util.event.UIShowAbsoluteCtxInfoEvt;
import com.dolphland.client.util.event.UIShowCtxInfoEvt;
import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.table.ProductTableRowComparatorContext.SortNature;

/**
 * Classe impl�mentant une table de produits
 * 
 * @author jeremy.scafi
 * @author JayJay
 * 
 */
public class ProductTable extends JPanel implements UIActionEvtFactory<ProductActionEvt> {

    private static final MessageFormater FMT = MessageFormater.getFormater(ProductTable.class);

    private static final Logger log = Logger.getLogger(ProductTable.class);

    public final static Color[] BACKGROUND_ROWS_COLORS = { new Color(230, 230, 230), Color.WHITE };

    private JPanel contentPanel;

    private JPanel topActionsPanel;
    private JPanel bottomActionsPanel;
    private JPanel leftActionsPanel;
    private JPanel rightActionsPanel;

    private ProductTableModel productsTableModel;

    private ProductDescriptor productDescriptor;

    private ProductTableContentManager propertyTableContentManager;

    private ProductTableColumnModel productsTableColumnModel;

    private List<UIAction<ProductActionEvt, ?>> actions;

    private ResizeableExtendedTable productsTable;

    private ClickSelectionInverter clickSelectionInverter;

    private JScrollPane productTableScroller;

    private int groupActionCount = Integer.MAX_VALUE;

    private JPopupMenu popup;

    private Schedule hideDescriptionSchedule;

    private ProductTableListener productTableListener;

    public List<ProductAdapter> currentSelection;

    private ProductsTableSelectionModel productsTableSelectionModel;

    private ActionsContext actionsContext;



    /**
     * Constructeur avec pour seul param�tre le descripteur de produits
     * 
     * @param pDescriptor
     *            Descripteur de produits
     */
    public ProductTable(ProductDescriptor pDescriptor) {
        this(pDescriptor, new DefaultProductTableContentManager());
    }



    /**
     * Constructeur avec le descripteur de produits et le manager de la table
     * des produits
     * 
     * @param pDescriptor
     *            Descripteur de produits
     * @param pTableContentManager
     *            Manager de la table des produits
     */
    public ProductTable(ProductDescriptor pDescriptor, ProductTableContentManager pTableContentManager) {
        if (pDescriptor == null) {
            throw new NullPointerException("Please specify a not null ProductDescriptor !"); //$NON-NLS-1$
        }
        if (pTableContentManager == null) {
            throw new NullPointerException("Please specify a not null ProductTableContentManager !"); //$NON-NLS-1$
        }
        this.productDescriptor = pDescriptor;
        this.propertyTableContentManager = pTableContentManager;
        this.actions = new ArrayList<UIAction<ProductActionEvt, ?>>();
        this.popup = new JPopupMenu();
        this.actionsContext = new ActionsContext(this);
        this.propertyTableContentManager.setTableComponent(this);
        this.propertyTableContentManager.setActionEvtFactory(this);
        this.propertyTableContentManager.setActionsContext(actionsContext);
        setLayout(new BorderLayout());
        contentPanel = createContentPanel();
        topActionsPanel = createActionsPanel();
        bottomActionsPanel = createActionsPanel();
        leftActionsPanel = createActionsPanel();
        rightActionsPanel = createActionsPanel();

        add(contentPanel, BorderLayout.CENTER);
        add(topActionsPanel, BorderLayout.NORTH);
        add(bottomActionsPanel, BorderLayout.SOUTH);
        add(leftActionsPanel, BorderLayout.WEST);
        add(rightActionsPanel, BorderLayout.EAST);
    }



    /**
     * <p>
     * Registers the listener that will be notified of this table events
     * </p>
     * <p>
     * If null then no event will be triggered.
     * </p>
     * 
     * @param productTableListener
     *            The listener instance to be registered on that
     *            {@link ProductTable}
     */
    public void setProductTableListener(ProductTableListener productTableListener) {
        this.productTableListener = productTableListener;
    }



    /**
     * Fixe la liste des produits de la table des produits
     * 
     * @param products
     *            Liste des produits
     */
    public void setProducts(List<ProductAdapter> products) {
        if (products == null) {
            products = new ArrayList<ProductAdapter>();
        }
        popup.setVisible(false);
        List<ProductTableRow> rows = new ArrayList<ProductTableRow>();
        int idx = 0;
        for (ProductAdapter pa : products) {
            ProductTableRow ptr = new ProductTableRow(idx++);
            ptr.setProduct(pa);
            rows.add(ptr);
        }
        // disable selection listener to avoid clearing current selection
        getProductTableSelectionModel().setSelectionListenerEnabled(false);
        getProductsTableModel().setProducts(rows);
        setSelectedProducts(currentSelection);
        // re-enabled selection listener
        getProductTableSelectionModel().setSelectionListenerEnabled(true);
    }



    /**
     * Fixe le produit s�lectionn� de la table des produits
     * 
     * @param product
     *            Produit � s�lectionner
     */
    public void setSelectedProduct(ProductAdapter product) {
        ProductAdapter selectedProduct = null;
        for (ProductAdapter pa : getProducts()) {
            if (product != null && pa.getId().equals(product.getId())) {
                selectedProduct = pa;
                break;
            }
        }

        if (selectedProduct != null) {
            int indSelectedProduct = getProducts().indexOf(selectedProduct);
            productsTable.setRowSelectionInterval(indSelectedProduct, indSelectedProduct);
        } else {
            productsTable.clearSelection();
        }
    }



    /**
     * <p>
     * Returns the list of currently selected {@link ProductAdapter} in the
     * table.
     * </p>
     * 
     * @return A List that contains the selected {@link ProductAdapter} in the
     *         table.
     */
    public List<ProductAdapter> getSelectedProducts() {
        return createUIActionEvt().getSelectedProducts();
    }



    /**
     * <p>
     * Create a Map that indexes the specified {@link ProductAdapter} list by
     * their ids
     * </p>
     * 
     * @param products
     *            The product list to be indexed.
     * @return The products passed as parameter indexed by their
     *         {@link ProductAdapter#getId()} values.
     */
    private Map<String, ProductAdapter> createIndex(List<ProductAdapter> products) {
        Map<String, ProductAdapter> out = new HashMap<String, ProductAdapter>();
        for (ProductAdapter pa : products) {
            out.put(pa.getId(), pa);
        }
        return out;
    }



    /**
     * <p>
     * Set the list of products that should be selected in the table
     * </p>
     * <p>
     * If some specific product does not exist in the table, it will be ignored.
     * </p>
     * <p>
     * Products in the table that have the same identifier (
     * {@link ProductAdapter#getId()}) as the specified products will be
     * selected.
     * </p>
     * 
     * @param selection
     *            The list of products to be selected
     */
    public void setSelectedProducts(List<ProductAdapter> selection) {
        productsTable.clearSelection();
        if (selection == null || selection.size() == 0) {
            // if selection is null or empty, just clear current selection
            return;
        } else {
            // find products to be selected in the table
            Map<String, ProductAdapter> selectionIndex = createIndex(selection);
            List<Integer> selectedIndexes = new ArrayList<Integer>();
            for (ProductTableRow row : getProductsTableModel().getRows()) {
                ProductAdapter p = selectionIndex.get(row.getProduct().getId());
                // is this row product in selection ?
                if (p != null) {
                    // if so, product should be selected
                    selectedIndexes.add(row.getIndex());
                }
            }

            // create selection with selected indexes
            getProductTableSelectionModel().setSelectionListenerEnabled(false);
            int firstIndex = Integer.MAX_VALUE;
            int lastIndex = Integer.MIN_VALUE;
            for (Integer idx : selectedIndexes) {
                productsTable.addRowSelectionInterval(idx, idx);
                firstIndex = Math.min(firstIndex, idx);
                lastIndex = Math.max(lastIndex, idx);
            }
            getProductTableSelectionModel().setSelectionListenerEnabled(true);
            getProductTableSelectionModel().fireSelectionChanged(firstIndex, lastIndex);
        }
    }



    /**
     * <p>
     * Select all the products in the table
     * </p>
     */
    public void selectAll() {
        setSelectedProducts(getProducts());
    }



    /**
     * Fournit la liste des produits de la table des produits
     * 
     * @return Liste des produits
     */
    public List<ProductAdapter> getProducts() {
        List<ProductAdapter> products = new ArrayList<ProductAdapter>();
        for (ProductTableRow ptr : getProductsTableModel().getRows()) {
            products.add(ptr.getProduct());
        }
        return products;
    }



    /**
     * <p>
     * Returns the current view event.
     * </p>
     * 
     * @return A {@link ProductActionEvt} that contains all that table view
     *         state.
     * 
     * @deprecated Use createUIActionEvt
     */
    public ProductActionEvt getViewEvent() {
        return createUIActionEvt();
    }



    /**
     * Fixe �galement la couleur de fond du viewport et du scrollpane
     * contenant
     * le tableau de produits
     */
    @Override
    public void setBackground(Color bg) {
        super.setBackground(bg);
        if (productTableScroller != null) {
            productTableScroller.setBackground(bg);
            productTableScroller.getViewport().setBackground(bg);
        }
    }



    /**
     * Fixe le nombre d'actions � regrouper sur un bloc d'affichage. Un bloc
     * d'affichage
     * peut �tre une ligne ou une colonne suivant le mode d'affichage choisi
     * pour les actions.<br>
     * Si le bloc est une ligne, groupActionCount est le nombre d'actions par
     * ligne, si le bloc
     * est une colonne, groupActionCount est le nombre d'actions par colonne.
     * 
     * @param groupActionCount
     *            Le nombre d'actions par bloc.
     */
    public void setGroupActionCount(int groupActionCount) {
        this.groupActionCount = groupActionCount;
    }



    /**
     * Ajoute une action produit
     * Triggers possibles de l'action :
     * - TRIGGER_CONTEXT : pour que l'action se lance via un menu contextuel sur
     * une ligne
     * - TRIGGER_CLICK : pour que l'action se lance via le changement de ligne
     * - TRIGGER_DBCLICK : pour que l'action se lance via le double click sur
     * une ligne
     * - TRIGGER_OPTION : pour que l'action se lance via un bouton
     * 
     * @param pa
     *            Action produit � ajouter
     * 
     * @deprecated Use bind in {@link VisionView} or {@link ActionsContext}
     */
    public void addProductAction(final UIAction<ProductActionEvt, ?> pa) {
        if (pa == null) {
            throw new NullPointerException("Please specify a not null ProductAction !"); //$NON-NLS-1$
        }
        actions.add(pa);
        ProductActionEvt event = createUIActionEvt();
        // Si l'action est de trigger option, on cr�e un bouton pour l'action
        if (pa.isTriggerEnabled(UIAction.TRIGGER_OPTION)) {
            JButton button = new JButton();
            button.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    try {
                        ProductActionEvt pae = createUIActionEvt();
                        pa.execute(pae);
                    } catch (Exception ex) {
                        log.error("actionPerformed(): ProductAction has thrown an exception !", ex); //$NON-NLS-1$
                    }
                }
            });
            UIActionUtil.installAction(pa, event, button);

            GridBagConstraints gc = new GridBagConstraints();

            switch (propertyTableContentManager.getProductActionConstraints(pa).getPositionTriggerOption()) {
            // Si position haute
                case ProductActionConstraints.POSITION_TOP :
                    gc.gridx = topActionsPanel.getComponentCount() % groupActionCount;
                    gc.gridy = (int) Math.floor((double) topActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    topActionsPanel.add(button, gc);
                    break;

                // Si position basse
                case ProductActionConstraints.POSITION_BOTTOM :
                    gc.gridx = bottomActionsPanel.getComponentCount() % groupActionCount;
                    gc.gridy = (int) Math.floor((double) bottomActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    bottomActionsPanel.add(button, gc);
                    break;

                // Si position gauche
                case ProductActionConstraints.POSITION_LEFT :
                    gc.gridx = (int) Math.floor((double) leftActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.gridy = leftActionsPanel.getComponentCount() % groupActionCount;
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    leftActionsPanel.add(button, gc);
                    break;

                // Si position droite
                case ProductActionConstraints.POSITION_RIGHT :
                    gc.gridx = (int) Math.floor((double) rightActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.gridy = rightActionsPanel.getComponentCount() % groupActionCount;
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    rightActionsPanel.add(button, gc);
                    break;
            }
        }
        // si trigger option
        if (pa.isTriggerEnabled(UIAction.TRIGGER_OPTION)) {
            UIActionUtil.updateActionEnabled(pa, event);
            UIActionUtil.updateActionVisible(pa, event);
        }
    }



    /**
     * Set actions context
     * 
     * @param actionsContext
     *            Actions context
     */
    public void setActionsContext(ActionsContext actionsContext) {
        this.actionsContext = actionsContext;
        this.propertyTableContentManager.setActionsContext(actionsContext);
    }



    /**
     * Bind action specified for the table
     * 
     * @param action
     *            Action
     */
    public <P extends UIActionEvt> void bind(UIAction<P, ?> action) {
        if (actionsContext == null) {
            throw new NullPointerException("Please set ActionsContext !"); //$NON-NLS-1$
        }
        if (action == null) {
            throw new NullPointerException("Please specify a ProductAction not null !"); //$NON-NLS-1$
        }

        // Add action
        actions.add((UIAction<ProductActionEvt, ?>) action);

        // If action is trigger option, create and bind button
        if (action.isTriggerEnabled(UIAction.TRIGGER_OPTION)) {
            JButton button = new JButton();

            GridBagConstraints gc = new GridBagConstraints();

            switch (propertyTableContentManager.getProductActionConstraints((UIAction<ProductActionEvt, ?>) action).getPositionTriggerOption()) {
            // Si position haute
                case ProductActionConstraints.POSITION_TOP :
                    gc.gridx = topActionsPanel.getComponentCount() % groupActionCount;
                    gc.gridy = (int) Math.floor((double) topActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    topActionsPanel.add(button, gc);
                    break;

                // Si position basse
                case ProductActionConstraints.POSITION_BOTTOM :
                    gc.gridx = bottomActionsPanel.getComponentCount() % groupActionCount;
                    gc.gridy = (int) Math.floor((double) bottomActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    bottomActionsPanel.add(button, gc);
                    break;

                // Si position gauche
                case ProductActionConstraints.POSITION_LEFT :
                    gc.gridx = (int) Math.floor((double) leftActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.gridy = leftActionsPanel.getComponentCount() % groupActionCount;
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    leftActionsPanel.add(button, gc);
                    break;

                // Si position droite
                case ProductActionConstraints.POSITION_RIGHT :
                    gc.gridx = (int) Math.floor((double) rightActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.gridy = rightActionsPanel.getComponentCount() % groupActionCount;
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    rightActionsPanel.add(button, gc);
                    break;
            }

            // Bind button for action
            actionsContext.bind(action, button);
        }
    }



    /**
     * Unbind action specified for the table
     * 
     * @param action
     *            Action
     */
    public <P extends UIActionEvt> void unbind(UIAction<P, ?> action) {
        if (actionsContext != null && action != null) {
            // Remove action
            actions.remove((UIAction<ProductActionEvt, ?>) action);

            // Unbind action
            actionsContext.unbind((UIAction<UIActionEvt, ?>) action);
        }
    }



    /**
     * Fournit la liste des actions produit de la table
     * 
     * @return Liste des actions produit de la table
     */
    public List<UIAction<ProductActionEvt, ?>> getProductActions() {
        return new ArrayList<UIAction<ProductActionEvt, ?>>(actions);
    }



    /**
     * Mise � jour de la visibilit� des actions produit en fonction du
     * contexte
     * de la table
     */
    public void updateProductActionsVisible() {
        UIActionUtil.updateActionsVisible(actions, this);
    }



    /**
     * Mise � jour de l'activation des actions produit en fonction du contexte
     * de la table
     */
    public void updateProductActionsEnabled() {
        UIActionUtil.updateActionsEnabled(actions, this);
    }



    /**
     * Si le panel est activ�/d�sactiv�, la table l'est aussi
     */
    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        productsTable.setEnabled(enabled);
        // Activation/D�sactivation des actions produit en fonction du
        // contexte
        // de la table
        for (UIAction<ProductActionEvt, ?> pa : actions) {
            pa.setEnabled(enabled);
        }
        updateProductActionsEnabled();
    }



    /**
     * Efface la s�lection des produits de la table des produits
     */
    public void clearSelection() {
        getProductsTable().clearSelection();
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre
     *            group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre
     *            group�es
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin) {
        getProductsTable().addColumnGroup(idColumnGroup, titleColumnGroup, indicesColumnTableDeb, indicesColumnTableFin);
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre
     *            group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre
     *            group�es
     * @param columnGroupRenderer
     *            Renderer du groupe de colonnes
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin, TableCellRenderer columnGroupRenderer) {
        getProductsTable().addColumnGroup(idColumnGroup, titleColumnGroup, indicesColumnTableDeb, indicesColumnTableFin, columnGroupRenderer);
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre
     *            group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre
     *            group�es
     * @param idColumnGroupParent
     *            Indique l'identifiant du groupe de colonnes devant contenir le
     *            groupe de colonnes
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin, String idColumnGroupParent) {
        getProductsTable().addColumnGroup(idColumnGroup, titleColumnGroup, indicesColumnTableDeb, indicesColumnTableFin, idColumnGroupParent);
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre
     *            group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre
     *            group�es
     * @param idColumnGroupParent
     *            Indique l'identifiant du groupe de colonnes devant contenir le
     *            groupe de colonnes
     * @param columnGroupRenderer
     *            Renderer du groupe de colonnes
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin, String idColumnGroupParent, TableCellRenderer columnGroupRenderer) {
        getProductsTable().addColumnGroup(idColumnGroup, titleColumnGroup, indicesColumnTableDeb, indicesColumnTableFin, idColumnGroupParent, columnGroupRenderer);
    }



    /**
     * Fixe le titre du groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     */
    public void setColumnGroupTitle(String idColumnGroup, String titleColumnGroup) {
        getProductsTable().setColumnGroupTitle(idColumnGroup, titleColumnGroup);
    }



    /**
     * Create product action event
     * 
     * @return Product action event
     * 
     * @deprecated Use createUIActionEvt
     */
    public ProductActionEvt createProductActionEvent() {
        return createUIActionEvt();
    }



    /**
     * Create product action event
     * 
     * @return Product action event
     */
    public ProductActionEvt createUIActionEvt() {
        ProductActionEvt out = null;
        List<ProductAdapter> allProducts = new ArrayList<ProductAdapter>();
        List<ProductAdapter> selectedProducts = new ArrayList<ProductAdapter>();
        List<ProductTableRow> allRows = null;
        List<ProductTableRow> selectedRows = new ArrayList<ProductTableRow>();
        int[] rows = getProductsTable().getSelectedRows();
        ProductTableModel model = getProductsTableModel();
        allRows = model.getRows();
        for (ProductTableRow r : allRows) {
            allProducts.add(r.getProduct());
        }
        for (int i = 0; i < rows.length; i++) {
            ProductTableRow ptr = model.getRowAt(rows[i]);
            ProductAdapter pa = ptr.getProduct();
            selectedProducts.add(pa);
            selectedRows.add(ptr);
        }
        ProductTableRow firstRowBeforeSelected = null;
        ProductTableRow firstRowAfterSelected = null;
        if (selectedRows.size() > 0) {
            ProductTableRow firstSelectedRow = selectedRows.get(0);
            if (firstSelectedRow.getIndex() > 0) {
                firstRowBeforeSelected = allRows.get(firstSelectedRow.getIndex() - 1);
            }
            if (firstSelectedRow.getIndex() < allRows.size() - 1) {
                firstRowAfterSelected = allRows.get(firstSelectedRow.getIndex() + 1);
            }
        }
        out = new ProductActionEvt();
        out.setProductTable(this);
        out.setAllRows(allRows);
        out.setSelectedRows(selectedRows);
        out.setAllProducts(allProducts);
        out.setSelectedProducts(selectedProducts);
        out.setFirstRowBeforeSelected(firstRowBeforeSelected);
        out.setFirstRowAfterSelected(firstRowAfterSelected);
        return out;
    }



    private JPanel createContentPanel() {
        JPanel out = new JPanel();
        out.setLayout(new GridBagLayout());
        GridBagConstraints gc00 = new GridBagConstraints();
        gc00.gridx = 0;
        gc00.gridy = 0;
        gc00.weightx = 1;
        gc00.weighty = 1;
        gc00.anchor = GridBagConstraints.CENTER;
        gc00.fill = GridBagConstraints.BOTH;
        out.add(getProductsTableScroller(), gc00);
        return out;
    }



    private JScrollPane getProductsTableScroller() {
        if (productTableScroller == null) {
            productTableScroller = new JScrollPane();
            productTableScroller.setViewportView(getProductsTable());
            productTableScroller.getViewport().setBackground(getBackground());
            productTableScroller.setBackground(getBackground());
        }
        return productTableScroller;
    }



    private ProductTableModel getProductsTableModel() {
        if (productsTableModel == null) {
            productsTableModel = new ProductTableModel(productDescriptor, propertyTableContentManager);
        }
        return productsTableModel;
    }



    private ProductTableColumnModel getProductsTableColumnModel() {
        if (productsTableColumnModel == null) {
            productsTableColumnModel = new ProductTableColumnModel(productDescriptor, propertyTableContentManager);
        }
        return productsTableColumnModel;
    }



    private ExtendedTable getProductsTable() {
        if (productsTable == null) {
            productsTable = new ResizeableExtendedTable();
            productsTable.setSelectionModel(getProductTableSelectionModel());
            productsTable.setModel(getProductsTableModel());
            productsTable.setColumnModel(getProductsTableColumnModel());
            productsTable.setFont(propertyTableContentManager.getProductTableDefaultFont());
            productsTable.setOpaque(propertyTableContentManager.isProductTableOpaque());
            productsTable.setSelectionMode(propertyTableContentManager.getProductTableSelectionMode());
            productsTable.setShowGrid(propertyTableContentManager.isProductTableShowGrid());
            productsTable.setShowHorizontalLines(propertyTableContentManager.isProductTableShowHorizontalLines());
            productsTable.setRowHeight(propertyTableContentManager.getProductTableRowHeight());
            productsTable.setBorder(propertyTableContentManager.getProductTableBorder());
            productsTable.setCellSelectionEnabled(propertyTableContentManager.isProductTableSelectionEnabled());
            productsTable.setRowSelectionAllowed(propertyTableContentManager.isProductTableRowSelectionAllowed());
            productsTable.getTableHeader().setReorderingAllowed(propertyTableContentManager.isProductTableReorderingAllowed());
            ProductsTableMouseHandler mouseHandler = new ProductsTableMouseHandler();
            productsTable.addMouseListener(mouseHandler);
            productsTable.addMouseMotionListener(mouseHandler);
            productsTable.getTableHeader().addMouseListener(new ProductsTableHeaderMouseListener());
            productsTable.getTableHeader().addMouseMotionListener(new ProductsTableHeaderMouseMotionListener());
            productsTable.setResizeableTableListener(new ResizeableTableListener() {

                @Override
                public void columnsResized(ResizeableTableEvent e) {
                    if (productTableListener != null) {
                        final ProductTableEvent pte = new ProductTableEvent(ProductTable.this, getColumnWidths());
                        SwingUtilities.invokeLater(new Runnable() {

                            public void run() {
                                try {
                                    productTableListener.columnsResized(pte);
                                } catch (Exception ex) {
                                    log.error("getProductsTable(): ProductTableListener.columnsResized() has failed !", ex); //$NON-NLS-1$
                                }
                            }
                        });
                    }
                }
            });
            // Keep a reference on the ClickSelectionInverter, so later on, we
            // will be able to call setEnabled(boolean) on the inverter if the
            // ProductTable gets a new method setInvertSelectionOnClick(boolean)
            clickSelectionInverter = new ClickSelectionInverter();
            clickSelectionInverter.bind(productsTable);

            Enumeration<TableColumn> enumTC = productsTable.getColumnModel().getColumns();
            while (enumTC.hasMoreElements()) {
                enumTC.nextElement().getCellEditor().addCellEditorListener(new CellEditorListener() {

                    @Override
                    public void editingStopped(ChangeEvent e) {
                        clearSelection();
                    }



                    @Override
                    public void editingCanceled(ChangeEvent e) {
                        clearSelection();
                    }
                });
            }
        }
        return productsTable;
    }



    private JPanel createActionsPanel() {
        JPanel out = new JPanel();
        out.setLayout(new GridBagLayout());
        return out;
    }



    /**
     * Affiche la description en fonction de la propri�t� de produit et du
     * point
     * d'attache
     * 
     * @param pp
     *            Propri�t� de produit
     * @param attachPoint
     *            Point d'attach
     */
    private void showDescription(ProductProperty pp, Point attachPoint) {
        showDescription(null, pp, attachPoint);
    }



    /**
     * Affiche la description en fonction du produit, de la propri�t� de
     * produit
     * et du point d'attache
     * 
     * @param pa
     *            Produit
     * @param pp
     *            Propri�t� de produit
     * @param attachPoint
     *            Point d'attache
     */
    private void showDescription(ProductAdapter pa, ProductProperty pp, Point attachPoint) {
        // Si la propri�t� de produit ou le produit sont diff�rents de
        // ceux de
        // la description affich�e
        hideDescription();

        // R�cup�ration de la description et du composant
        String description = null;
        JComponent component = null;
        if (pp != null && pa != null) {
            description = propertyTableContentManager.getCellTooltip(pp, pa);
            component = getProductsTable();
        } else if (pp != null) {
            description = propertyTableContentManager.getProductPropertyTooltip(pp);
            component = getProductsTable().getTableHeader();
        }

        if (!StrUtil.isEmpty(description)) {
            UIShowAbsoluteCtxInfoEvt evt = new UIShowAbsoluteCtxInfoEvt(component, description, attachPoint, UIShowCtxInfoEvt.AUTO);
            EDT.postEvent(evt);
        }
    }



    /**
     * Cache la description d�j� affich�e
     */
    private void hideDescription() {
        if (hideDescriptionSchedule != null) {
            hideDescriptionSchedule.destroy();
        }
        EDT.postEvent(new UIHideCtxInfoEvt(getProductsTable()));
        EDT.postEvent(new UIHideCtxInfoEvt(getProductsTable().getTableHeader()));
    }



    /**
     * <p>
     * Return the sizes of this table's columns. Each column size is reachable
     * at the index of its corresponding column.
     * </p>
     * 
     * @return The sizes of all the columns that this table contains.
     */
    public int[] getColumnWidths() {
        JTableHeader header = productsTable.getTableHeader();
        TableColumnModel columnModel = header.getColumnModel();

        int columnCount = columnModel.getColumnCount();
        int[] columnSizes = new int[columnCount];

        for (int i = 0; i < columnCount; i++) {
            TableColumn column = columnModel.getColumn(i);
            columnSizes[i] = column.getWidth();
        }

        return columnSizes;
    }

    /**
     * Let the user clicks on a cell or row to invert selection (without
     * maintaining Ctrl).
     * Call {@code bind(JTable)} to start listening for clicks on a table, and
     * {@code unbind()} to stop listening.
     * 
     * @author p-Sebastien.Laout
     */
    private final static class ClickSelectionInverter extends MouseMotionAdapter implements MouseListener {

        private static final int NONE = -1;

        // The table we bind to
        private JTable productTable;

        // true if the mouse is currently pressed
        private boolean clicked;

        // the row number the mouse was over just before clicking,
        // or NONE if it was not on a row or the row it was on was
        // already deselected (so we do not need to unselect it)
        private int firstClickedRow = NONE;

        private int firstClickedColumn = NONE;



        /*
         * Despite very unintuitive names, ProductTable allows only three
         * selection models:
         * A. Allow no selection at all
         * propertyTableContentManager.isProductTableRowSelectionAllowed() is
         * passed to table.setRowSelectionAllowed()
         * => false for no selection at all
         * => true to use model B or C below
         * B. Select cells
         * propertyTableContentManager.isProductTableSelectionEnabled() is
         * passed to table.setCellSelectionEnabled()
         * => true for cell selection
         * C. Select entire rows
         * propertyTableContentManager.isProductTableSelectionEnabled() is
         * passed to table.setCellSelectionEnabled()
         * => false for entire row selection
         */
        private boolean isSelectionAllowed() {
            return productTable.getRowSelectionAllowed();
        }



        private boolean isRowSelectionMode() {
            return !productTable.getCellSelectionEnabled();
        }



        /**
         * Start listening for clicks on the {@code table}.
         * A ClickSelectionInverter instance can only listen one table at a
         * time.
         * Calling {@code bind(JTable)} several times will unbind previous
         * tables
         * and keep listening only on the last binded table.
         * 
         * @param table
         *            the table to invert selection on single-click on a row
         */
        public void bind(JTable table) {
            if (productTable != null) {
                unbind();
            }

            productTable = table;
            productTable.addMouseMotionListener(this);
            productTable.addMouseListener(this);
        }



        /**
         * Do not listen to the binded table clicks anymore, if there was a
         * binded table.
         */
        public void unbind() {
            if (productTable != null) {
                productTable.removeMouseMotionListener(this);
                productTable.removeMouseListener(this);
                productTable = null;
            }
        }



        @Override
        public void mouseMoved(MouseEvent e) {
            /**
             * How does the ClickSelectionInverter works?
             * - We detect a click and if it involved only one row, we deselect
             * the clicked row if it was selected before the click.
             * - To detect how many rows a click involves, we need to catch
             * mousePressed and
             * mouseReleased events: the click involved a single row if the
             * mouse was over
             * the same row when the mouse was pressed and released.
             * - But when clicking a cell, it is first selected and then our
             * mousePressed()
             * method is called: so we cannot determine if the row was selected
             * or not
             * prior to the click.
             * - To remedy to that problem, we monitor mouseMoved too: before a
             * cell is
             * clicked, a mouseMoved over that cell is first fired: we then
             * remember the
             * state of the cell just before it is clicked.
             * 
             * To summarize:
             * 1. mouseMoved() is called: we remember the row number in
             * firstClickedRow if
             * - the mouse is not pressed,
             * - the mouse is over a row,
             * - and that row is selected.
             * If not, we reset the firstClickedRow variable.
             * 2. mousePressed() is called: we set the variable clicked to true,
             * so that
             * mouseMoved() will not override firstClickedRow when moving the
             * mouse while
             * it is pressed.
             * 3. mouseReleased() is called: if the mouse is still on the row
             * number we
             * remembered, it is because it was selected before the click and we
             * know the
             * mouse is still over the same row: we call clearSelection() to
             * invert the
             * selection.
             */

            if (!clicked && e.getClickCount() == 1 && SwingUtilities.isLeftMouseButton(e)) {
                firstClickedRow = NONE;
                firstClickedColumn = NONE;
                if (productTable != null && isSelectionAllowed()) {
                    int clickedRow = productTable.rowAtPoint(e.getPoint());
                    int clickedColumn = productTable.columnAtPoint(e.getPoint());
                    if (productTable.isEnabled() && clickedRow >= 0 && productTable.isRowSelected(clickedRow) && productTable.getSelectedRowCount() == 1) {
                        // If in rows selection mode, do not check if the mouse
                        // did not changed column
                        // If in cells selection mode, check if the mouse did
                        // not changed column
                        if (isRowSelectionMode() || (clickedColumn >= 0 && productTable.isColumnSelected(clickedColumn) && productTable.getSelectedColumnCount() == 1)) {
                            firstClickedRow = clickedRow;
                            firstClickedColumn = clickedColumn;
                        }
                    }
                }
            }
        }



        public void mousePressed(MouseEvent e) {
            if (e.getClickCount() == 1 && SwingUtilities.isLeftMouseButton(e)) {
                clicked = true;
            }
        }



        public void mouseReleased(MouseEvent e) {
            if (e.getClickCount() == 1 && SwingUtilities.isLeftMouseButton(e)) {
                // The mouse was over a selected row before clicking on it?
                if (productTable != null && isSelectionAllowed() && firstClickedRow >= 0) {
                    int clickedRow = productTable.rowAtPoint(e.getPoint());
                    int clickedColumn = productTable.columnAtPoint(e.getPoint());
                    // We clicked the row without dragging to span several rows?
                    if (clickedRow == firstClickedRow) {
                        // If in rows selection mode, do not check if the mouse
                        // did
                        // not changed column
                        // If in cells selection mode, check if the mouse did
                        // not
                        // changed column
                        if (isRowSelectionMode() || (clickedColumn == firstClickedColumn)) {
                            // Unselect the only selected row (or the only
                            // selected
                            // cell)
                            productTable.clearSelection();
                        }
                    }
                }

                // If the user click again without moving the mouse, it must
                // continue to work
                firstClickedRow = NONE;
                firstClickedColumn = NONE;
                clicked = false;
                mouseMoved(e);
            }
        }



        public void mouseExited(MouseEvent e) {
            // Nothing to do
        }



        public void mouseEntered(MouseEvent e) {
            // Nothing to do
        }



        public void mouseClicked(MouseEvent e) {
            // Nothing to do
        }
    }



    private ProductsTableSelectionModel getProductTableSelectionModel() {
        if (productsTableSelectionModel == null) {
            productsTableSelectionModel = new ProductsTableSelectionModel();
        }
        return productsTableSelectionModel;
    }

    private class ProductsTableSelectionModel extends DefaultListSelectionModel {

        ProductsTableSelectionListener selectionListener = new ProductsTableSelectionListener();



        public ProductsTableSelectionModel() {
            addListSelectionListener(selectionListener);
        }



        public void setSelectionListenerEnabled(boolean enabled) {
            removeListSelectionListener(selectionListener);
            if (enabled) {
                addListSelectionListener(selectionListener);
            }
        }



        public void fireSelectionChanged(int firstIndex, int lastIndex) {
            super.fireValueChanged(firstIndex, lastIndex);
        }
    }

    private class ProductsTableSelectionListener implements ListSelectionListener {

        public void valueChanged(ListSelectionEvent e) {
            if (e.getValueIsAdjusting()) {
                return;
            }
            ProductActionEvt pae = createUIActionEvt();
            ProductTable.this.currentSelection = pae.getSelectedProducts();
            UIActionUtil.executeActions(actions, ProductTable.this, UIAction.TRIGGER_CLICK);
        }
    }

    private class ProductsTableMouseHandler extends MouseInputAdapter {

        public void mouseClicked(MouseEvent e) {
            // Si double clic sur une ligne
            if (e.getClickCount() == 2) {
                // On ne r�alise des actions que si un �l�ment est
                // s�lectionn�
                if (getProductsTable().getSelectedRow() >= 0) {
                    UIActionUtil.executeActions(actions, ProductTable.this, UIAction.TRIGGER_DBCLICK);
                }
            }
        }



        public void mousePressed(MouseEvent e) {
            // Si double clic ou clic droit sur une ligne
            if (e.getClickCount() == 2 || SwingUtilities.isRightMouseButton(e)) {
                // Si ligne du double clic ou du clic droit non
                // s�lectionn�e,
                // s�lection de la ligne o� le double clic ou le clic droit
                // est
                // r�alis�
                int clickedRow = productsTable.rowAtPoint(e.getPoint());
                if (getProductsTable().isEnabled() && clickedRow >= 0 && !productsTable.isRowSelected(clickedRow)) {
                    productsTable.setRowSelectionInterval(clickedRow, clickedRow);
                }
            }
        }



        public void mouseReleased(MouseEvent e) {
            // Si clic pour afficher un popup menu
            if (e.isPopupTrigger()) {
                // On n'affiche le popup que si un �l�ment est
                // s�lectionn�
                if (getProductsTable().getSelectedRow() >= 0) {
                    // Parcourt des actions de trigger CONTEXT
                    popup.setVisible(false);
                    popup.removeAll();
                    if (actionsContext != null) {
                        UIActionUtil.updatePopupMenuFromActions(actions, actionsContext, popup);
                    } else {
                        UIActionUtil.updatePopupMenuFromActions(actions, new ActionsContext(ProductTable.this), popup);
                    }
                    if (popup.getComponentCount() > 0) {
                        popup.show(getProductsTable(), e.getX(), e.getY());
                    }
                }
            }
        }



        @Override
        public void mouseExited(MouseEvent e) {
            // Lorsque la souris quitte la table on cache la description
            // courante
            hideDescription();
        }



        @Override
        public void mouseMoved(MouseEvent e) {
            int cursorRow = productsTable.rowAtPoint(e.getPoint());
            int cursorColumn = productsTable.columnAtPoint(e.getPoint());

            if (cursorRow >= 0 && cursorColumn >= 0) {
                ProductAdapter pa = getProductsTableModel().getRowAt(cursorRow).getProduct();
                ProductProperty pp = productDescriptor.getProperties().get(cursorColumn);

                // Affiche si besoin la description de la cellule
                showDescription(pa, pp, e.getPoint());
            }
        }
    }

    private class ProductsTableHeaderMouseListener extends MouseAdapter {

        private ProductProperty lastSortedColumnProperty;
        private SortNature[] sortNatures = { SortNature.ASCENT, SortNature.DESCENT };
        private int nextSortNatureIndex = 0;



        @Override
        public void mouseExited(MouseEvent e) {
            // Lorsque la souris quitte l'ent�te de table on cache la
            // description courante
            hideDescription();
        }



        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getClickCount() == 2) {
                int col = getProductsTable().columnAtPoint(e.getPoint());
                if (col < 0) {
                    return;
                }
                ProductProperty pp = productDescriptor.getPropertyAt(col);
                if (lastSortedColumnProperty != pp) {
                    nextSortNatureIndex = 0;
                }
                SortNature sortNature = sortNatures[nextSortNatureIndex++ % sortNatures.length];
                ProductTableRowComparatorContext ctx = new ProductTableRowComparatorContext(Arrays.asList(pp), sortNature);
                getProductsTableModel().sortRows(propertyTableContentManager.createRowComparator(ctx));
                lastSortedColumnProperty = pp;
            }
        }
    }

    private class ProductsTableHeaderMouseMotionListener extends MouseMotionAdapter {

        @Override
        public void mouseMoved(MouseEvent e) {
            int cursorColumn = productsTable.columnAtPoint(e.getPoint());

            ProductProperty pp = productDescriptor.getProperties().get(cursorColumn);

            // Affiche si besoin la description de la colonne
            showDescription(pp, e.getPoint());
        }
    }



    /**
     * <p>
     * Prints the underlying {@link JTable} with its headers and columns.
     * </p>
     */
    private void printTable(PrintMode printMode) {
        PrintRequestAttributeSet set = new HashPrintRequestAttributeSet();
        set.add(Chromaticity.MONOCHROME);
        try {
            getProductsTable().print(PrintMode.FIT_WIDTH, null, null, true, set, true);
        } catch (Exception e) {
            throw new AppBusinessException(FMT.format("ProductTable.RS_PRINT_REQUEST_FAILURE"), e);
        }
    }

}
