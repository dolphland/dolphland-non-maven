package com.dolphland.client.util.table;

import java.awt.Color;
import java.awt.Component;
import java.util.EventObject;
import java.util.Iterator;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

class ProductTableColumnModel extends DefaultTableColumnModel {

    private ProductDescriptor productDescriptor;

    private ProductTableContentManager propertyFormater;



    public ProductTableColumnModel(ProductDescriptor pDescriptor, ProductTableContentManager pFormater) {
        super();
        if (pDescriptor == null) {
            throw new NullPointerException("Please specify a not null ProductDescriptor !"); //$NON-NLS-1$
        }
        if (pFormater == null) {
            throw new NullPointerException("Please specify a not null PropertyFormater !"); //$NON-NLS-1$
        }
        this.productDescriptor = pDescriptor;
        this.propertyFormater = pFormater;
        List<ProductProperty> properties = productDescriptor.getProperties();
        TableColumn tc = null;
        HeaderRenderer hr = new HeaderRenderer();
        CellRenderer cr = new CellRenderer();
        Iterator<ProductProperty> it = properties.iterator();
        int column = 0;
        while (it.hasNext()) {
            ProductProperty pp = it.next();
            tc = new TableColumn(column);
            tc.setResizable(true);
            tc.setHeaderValue(pp.getHeader());
            tc.setCellRenderer(cr);
            tc.setCellEditor(new ProductTableCellEditor(column));
            tc.setHeaderRenderer(hr);
            tc.setWidth(propertyFormater.getProductPropertyWidth(pp));
            tc.setMinWidth(propertyFormater.getProductPropertyMinWidth(pp));
            tc.setMaxWidth(propertyFormater.getProductPropertyMaxWidth(pp));
            tc.setPreferredWidth(propertyFormater.getProductPropertyPreferredWidth(pp));
            addColumn(tc);
            column++;
        }
    }

    private class ProductTableCellEditor implements TableCellEditor {

        TableCellEditor cellEditor;



        public ProductTableCellEditor(int column) {
            ProductProperty pp = productDescriptor.getProperties().get(column);
            cellEditor = propertyFormater.getTableCellEditor(pp, column);
        }



        public Object getCellEditorValue() {
            return cellEditor.getCellEditorValue();
        }



        public boolean isCellEditable(EventObject anEvent) {
            return cellEditor.isCellEditable(anEvent);
        }



        public boolean shouldSelectCell(EventObject anEvent) {
            return cellEditor.shouldSelectCell(anEvent);
        }



        public boolean stopCellEditing() {
            return cellEditor.stopCellEditing();
        }



        public void cancelCellEditing() {
            cellEditor.cancelCellEditing();
        }



        public void addCellEditorListener(CellEditorListener l) {
            cellEditor.addCellEditorListener(l);
        }



        public void removeCellEditorListener(CellEditorListener l) {
            cellEditor.removeCellEditorListener(l);
        }



        public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
            return cellEditor.getTableCellEditorComponent(table, value, isSelected, row, column);
        }
    }

    private class CellRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JLabel out = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            out.setHorizontalAlignment(JLabel.CENTER);
            out.setForeground(Color.BLACK);
            if (!isSelected) {
                out.setBackground(ProductTable.BACKGROUND_ROWS_COLORS[row % 2]);
            } else {
                out.setBackground(table.getSelectionBackground());
            }
            ProductProperty pp = productDescriptor.getProperties().get(column);
            ProductAdapter pa = ((ProductTableModel) table.getModel()).getRowAt(row).getProduct();
            return propertyFormater.renderProduct(out, pp, pa, isSelected, hasFocus, row, column);
        }
    }

    private class HeaderRenderer extends DefaultGroupableTableHeaderRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component out = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            ProductProperty pp = productDescriptor.getProperties().get(column);
            return propertyFormater.renderProductProperty(out, pp, isSelected, hasFocus, row, column);
        }
    }
}
