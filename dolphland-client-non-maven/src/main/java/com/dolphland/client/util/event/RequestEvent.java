/*
 * $Log: RequestEvent.java,v $
 * Revision 1.1 2009/03/05 15:02:57 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

/**
 * D�finit un �v�nement auquel il est possible de r�pondre sp�cifiquement.<br>
 * Le principe est le suivant :<br>
 * <ul>
 * <li>Un �m�tteur effectue une demande en envoyant un RequestEvent vers une
 * destination.
 * <li>L'�v�nement est transmis � tous les subscriber de la destination.
 * <li>Le premier subscriber qui r�pond transmet sa r�ponse � l'�metteur.
 * <li>Les �ventuelles r�ponses d'autre subscribers ne seront pas intercept�s
 * par l'emtteur.
 * </ul>
 * 
 * @author JayJay
 */
public class RequestEvent extends Event {

    private EventDestination source;



    public RequestEvent(EventDestination destination) {
        super(destination);
        source = new EventDestination();
    }



    public <T extends Event> T request(Class<T> expected) throws InterruptedException {
        return EventMulticaster.getInstance().postEventAndWait(this, source, expected);
    }



    public <T extends Event> T request(Class<T> expected, long maxWait) throws InterruptedException {
        return EventMulticaster.getInstance().postEventAndWait(this, source, expected, maxWait);
    }



    public <T extends Event> T request(long maxWait) throws InterruptedException, ClassCastException {
        return (T) EventMulticaster.getInstance().postEventAndWait(this, source, Event.class, maxWait);
    }



    public <T extends Event> T request() throws InterruptedException, ClassCastException {
        return (T) EventMulticaster.getInstance().postEventAndWait(this, source, Event.class);
    }



    public void respond(Event e) {
        EventMulticaster.getInstance().postEvent(e, source);
    }
}
