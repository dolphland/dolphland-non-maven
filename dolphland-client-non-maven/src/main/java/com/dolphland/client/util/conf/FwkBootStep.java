/*
 * $Log: FwkBootStep.java,v $
 * Revision 1.5 2014/03/10 16:10:46 bvatan
 * - Internationalized
 * Revision 1.4 2012/07/24 09:30:19 bvatan
 * - added javadoc
 * Revision 1.3 2009/04/01 09:33:13 jscafi
 * - Résolution des warnings
 * Revision 1.2 2008/12/03 13:29:14 bvatan
 * - ne catch plus les exceptions dnas execute()
 * Revision 1.1 2008/11/27 16:31:09 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

/**
 * <p>
 * Defines a custom step to be part of the application startup process.
 * </p>
 * <p>
 * Startup (or boot) process is executed by {@link FwkBoot}, extra/custom boot
 * steps can be added to the core boot steps byt adding instances of this class
 * the {@link FwkBoot}
 * </p>
 * 
 * @author JayJay
 */
public class FwkBootStep {

    private String name;

    private BootContext bootContext;



    /**
     * <p>
     * Builds a new boot step whose label is specified in parameter
     * </p>
     * 
     * @param name
     *            The label name of the boot step. This label may appear
     *            somewhere on the GUI for example.
     */
    public FwkBootStep(String name) {
        if (name == null) {
            throw new NullPointerException("name is null"); //$NON-NLS-1$
        }
        this.name = name;
    }



    /**
     * <p>
     * Get this boot step label name
     * </p>
     * 
     * @return this boot step label name
     */
    public String getName() {
        return name;
    }



    /**
     * <p>
     * Set the boot context reference to be used by this boot step. Must be set
     * <b>before</b> that <b>execute()</b> is called
     * </p>
     * 
     * @param ctx
     *            The context reference containing bott information
     */
    final void setBootContext(BootContext ctx) {
        this.bootContext = ctx;
    }



    /**
     * <p>
     * Get the boot context associated to this boot step.
     * </p>
     * 
     * @return The boot context associated to this boot step
     */
    protected final BootContext getBootContext() {
        return bootContext;
    }



    /**
     * <p>
     * Execute this boot step in the boot context set on this instance.
     * </p>
     * 
     * @param booter
     *            The booter refrence that is executing this boot step.
     */
    final void execute(FwkBoot booter) {
        doExecute(booter);
    }



    /**
     * <p>
     * Boot step code logic to be overriden by subclasses. Does not nothing by
     * default.
     * </p>
     * 
     * @param booter
     *            The booter instance that is executing this boot step.
     */
    protected void doExecute(FwkBoot booter) {

    }

}
