package com.dolphland.client.util.flip;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionUtil;
import com.dolphland.client.util.animator.AnimatedComponent;
import com.dolphland.client.util.animator.AnimatedPanel;
import com.dolphland.client.util.animator.AnimationActionEvt;
import com.dolphland.client.util.application.components.FwkLabel;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;

/**
 * Class to represent a flip's panel for applications
 * 
 * @author JayJay
 * 
 */
public class FlipPanel extends FwkPanel {

    // For auto-numbering thread
    private static int threadInitNumber;



    private static synchronized int nextThreadNum() {
        return threadInitNumber++;
    }

    // Flip's animatedPanel
    private AnimatedPanel flipAnimatedPanel;

    // Flip's animator
    private FlipAnimator flipAnimator;

    // Flip's handler
    private FlipHandler flipHandler;

    // Maximized application
    private String maximizedApplicationId;

    // Applications grouped by id
    private Map<String, FlipApplicationAdapter> applicationsById;

    // Layout builder
    private TableLayoutBuilder layoutBuilder;



    /**
     * Default constructor
     */
    public FlipPanel() {
        applicationsById = new LinkedHashMap<String, FlipApplicationAdapter>();

        // Create animated panel
        flipAnimatedPanel = new AnimatedPanel();
        flipAnimatedPanel.setOpaque(false);

        // Add animated panel
        addComponent(flipAnimatedPanel);

        // Create animator
        flipAnimator = new FlipAnimator(flipAnimatedPanel);

        // Create handler
        flipHandler = new FlipHandler(this, flipAnimatedPanel, flipAnimator);

        // Affect handler to flip's animated panel
        flipAnimatedPanel.addMouseListener(flipHandler);
        flipAnimatedPanel.addMouseMotionListener(flipHandler);

        // Resize component to show if resize panel
        addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                // Resize all components
                for (Component component : getComponents()) {
                    component.setSize(getSize());
                }
            }
        });
    }



    /**
     * Add an application to flip's panel
     * 
     * @param application
     *            Application
     */
    public void addApplication(FlipApplicationAdapter application) {
        AssertUtil.notNull(application, "application is null !");

        // Application's component is not visible before showing animation
        application.getApplicationComponent().setVisible(false);

        // Show application after animation application show or maximize
        application.getFlipAnimationShow().getActionsAfter().add(0, new ShowApplicationAction());
        application.getFlipAnimationMaximize().getActionsAfter().add(0, new ShowApplicationAction());

        // Remove application after animation application hide
        application.getFlipAnimationHide().getActionsAfter().add(0, new RemoveApplicationAction());

        // Add application's component
        applicationsById.put(application.getApplicationId(), application);

        // Thread to show application when the application is loaded and sized
        ShowApplicationThread showApplicationThread = new ShowApplicationThread(application);
        showApplicationThread.start();

        // Add application's component
        addComponent(application.getApplicationComponent());
    }



    /**
     * Remove an application from flip's panel
     * 
     * @param applicationId
     *            Application's id
     */
    public void removeApplication(String applicationId) {
        FlipApplicationAdapter application = applicationsById.remove(applicationId);
        if (application != null) {
            applicationsById.remove(application.getApplicationId());

            // Remove application's component
            remove(application.getApplicationComponent());

            // Remove animated component
            flipAnimatedPanel.removeAnimatedComponent(application.getApplicationId());

            // If the removed application is the maximized application, show
            // first application if possible
            if (applicationId.equals(maximizedApplicationId) && !applicationsById.isEmpty()) {
                AnimationActionEvt evt = new AnimationActionEvt();
                evt.setAnimatedComponentId(applicationsById.keySet().iterator().next());
                new ShowApplicationAction().execute(evt);
            }
        }
    }



    /**
     * Get application
     * 
     * @param applicationId
     * 
     * @return Application
     */
    public FlipApplicationAdapter getApplication(String applicationId) {
        return applicationsById.get(applicationId);
    }



    /**
     * Get all applications
     * 
     * @return Applications
     */
    public List<FlipApplicationAdapter> getApplications() {
        return new ArrayList<FlipApplicationAdapter>(applicationsById.values());
    }



    /**
     * Get the maximized application's id
     * 
     * @return Maximized application's id
     */
    public String getMaximizedApplicationId() {
        return maximizedApplicationId;
    }



    /**
     * Remove all applications from flip's panel
     */
    public void removeAllApplications() {
        for (String applicationId : new ArrayList<String>(applicationsById.keySet())) {
            removeApplication(applicationId);
        }
    }



    /**
     * Flip applications
     */
    public void flipApplications() {

        // If maximized application
        if (maximizedApplicationId != null) {
            // Stop animation
            flipAnimatedPanel.stopAnimate();

            // Get application
            FlipApplicationAdapter application = applicationsById.get(maximizedApplicationId);

            // Create the application's snapshot
            BufferedImage applicationImage = ImageUtil.getImage(application.getApplicationComponent());

            // Get animated component
            AnimatedComponent animatedComponent = flipAnimatedPanel.getAnimatedComponent(maximizedApplicationId);

            // Set application image
            animatedComponent.setFrontImage(applicationImage);

            // If minimize is disabled, do nothing
            boolean minimizeDisabled = false;
            if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationMinimize().getActionsBefore(), flipAnimatedPanel.getAnimatedComponent(maximizedApplicationId))) {
                minimizeDisabled = true;
            }
            if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationMinimize().getActionsAfter(), flipAnimatedPanel.getAnimatedComponent(maximizedApplicationId))) {
                minimizeDisabled = true;
            }
            // else Show the application with an animation
            if (!minimizeDisabled) {
                application.getFlipAnimationMinimize().setSelectedApplicationId(maximizedApplicationId);
                flipAnimator.executeAnimations(application.getFlipAnimationMinimize());
            }

            // Unset maximized application
            maximizedApplicationId = null;

            // Show animated panel
            setShowedComponent(flipAnimatedPanel);
        }
    }



    /**
     * Get layout builder
     * 
     * @return Layout builder
     */
    private TableLayoutBuilder getLayoutBuilder() {
        if (layoutBuilder == null) {
            layoutBuilder = createTableLayoutBuilder();
            setLayout(layoutBuilder.get());
        }
        return layoutBuilder;
    }



    /**
     * Add component to flip panel
     * 
     * @param component
     *            Component to add
     */
    private void addComponent(Component component) {
        add(component, getLayoutBuilder().addComponent(createTableLayoutConstraintsBuilder()
            .height(TableLayout.FILL)
            .width(TableLayout.FILL)
            .get()));

        validate();
    }



    /**
     * Set the showed component
     * 
     * @param showedComponent
     *            Showed component
     */
    private void setShowedComponent(Component showedComponent) {
        for (Component component : getComponents()) {
            component.setVisible(component.equals(showedComponent));
        }
    }

    /**
     * Action to show application
     * 
     * @author p-jeremy.scafi
     * 
     */
    private class ShowApplicationAction extends UIAction<AnimationActionEvt, Component> {

        @Override
        protected Component doExecute(AnimationActionEvt param) {
            maximizedApplicationId = param.getAnimatedComponentId();
            return applicationsById.get(maximizedApplicationId).getApplicationComponent();
        }



        @Override
        protected void updateViewSuccess(Component data) {
            setShowedComponent(data);
        }
    }

    /**
     * Remove application
     * 
     * @author p-jeremy.scafi
     * 
     */
    private class RemoveApplicationAction extends UIAction<AnimationActionEvt, String> {

        @Override
        protected String doExecute(AnimationActionEvt param) {
            String previousApplicationId = null;
            for (String applicationId : applicationsById.keySet()) {
                if (applicationId.equals(param.getAnimatedComponentId())) {
                    break;
                }
                previousApplicationId = applicationId;
            }
            removeApplication(param.getAnimatedComponentId());
            return previousApplicationId;
        }



        @Override
        protected void updateViewSuccess(String data) {
            // Center to previous application
            flipAnimator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
                .selectedApplicationId(data)
                .get());
        }
    }

    /**
     * Thread to show application when the application is loaded and resized
     * 
     * @author p-jeremy.scafi
     * 
     */
    private class ShowApplicationThread extends Thread {

        private FlipApplicationAdapter application;



        /**
         * Default constructor with application
         * 
         * @param application
         *            Application
         */
        public ShowApplicationThread(FlipApplicationAdapter application) {
            super("ShowApplicationThread" + nextThreadNum());
            this.application = application;
        }



        @Override
        public void run() {
            // Do nothing while application is not loaded and sized
            while (!application.getApplicationLoader().isApplicationLoaded() || application.getApplicationComponent().getWidth() <= 0 || application.getApplicationComponent().getHeight() <= 0) {
                try {
                    sleep(10);
                } catch (InterruptedException e) {
                    interrupt();
                }
            }

            // Wait a short time to be sure that application is loaded
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                interrupt();
            }

            // Create the application's snapshot
            BufferedImage applicationImage = ImageUtil.getImage(application.getApplicationComponent());

            // Add application's component to animated component
            AnimatedComponent animatedComponent = new AnimatedComponent(application.getApplicationId(), applicationImage);

            // Initially, application's component is not visible. To do that,
            // force its size to 0
            animatedComponent.setSize(0, 0);

            // Add animated component
            flipAnimatedPanel.addAnimatedComponent(animatedComponent);

            // Set animated panel to showed component
            setShowedComponent(flipAnimatedPanel);

            // If show is disabled, do nothing
            if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationShow().getActionsBefore(), flipAnimatedPanel.getAnimatedComponent(application.getApplicationId()))) {
                return;
            }
            if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationShow().getActionsAfter(), flipAnimatedPanel.getAnimatedComponent(application.getApplicationId()))) {
                return;
            }

            // Show the application with an animation
            flipAnimator.executeAnimations(application.getFlipAnimationShow());
        }
    }



    public static void main(String[] args) {
        // Start EDT
        EDT.start();

        final FlipPanel flipPanel = new FlipPanel();
        flipPanel.setBackground(Color.RED);

        JFrame frame = new JFrame();
        frame.setSize(640, 480);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(flipPanel);
        frame.setVisible(true);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        Color[] colorsApplications = new Color[] { Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN, Color.ORANGE, Color.PINK };

        for (int i = 0; i < colorsApplications.length; i++) {
            FwkPanel applicationPanel = new FwkPanel();
            applicationPanel.setBackground(colorsApplications[i]);
            applicationPanel.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    flipPanel.flipApplications();
                }
            });
            FwkLabel lblTest = new FwkLabel("TEST");
            lblTest.setForeground(Color.RED);
            TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
            applicationPanel.setLayout(layoutBuilder.get());
            applicationPanel.add(lblTest, layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
                .leftInsets(1f / 2f)
                .width(TableLayout.PREFERRED)
                .rightInsets(1f / 2f)
                .topInsets(1f / 2f)
                .height(TableLayout.PREFERRED)
                .bottomInsets(1f / 2f)
                .get()));

            flipPanel.addApplication(new FlipApplicationAdapter(new Integer(i).toString(), applicationPanel, new FlipApplicationLoader() {

                @Override
                public boolean isApplicationLoaded() {
                    return true;
                }
            }));

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
