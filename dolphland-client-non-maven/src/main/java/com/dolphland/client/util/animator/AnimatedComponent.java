package com.dolphland.client.util.animator;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

import org.apache.log4j.Logger;

import com.dolphland.client.util.action.UIActionEvtFactory;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.assertion.AssertUtil;

/**
 * Component can be animated
 * 
 * @author JayJay
 */
public class AnimatedComponent extends FwkPanel implements UIActionEvtFactory<AnimationActionEvt> {

    private static final Logger LOG = Logger.getLogger(AnimatedComponent.class);

    private String id;
    private BufferedImage frontImage;
    private BufferedImage backImage;
    private AffineTransform imageTransform;
    private Rectangle bounds;
    private Rectangle boundsWithoutRotation;
    private Rectangle boundsWithoutTransformations;
    private AffineTransform transform;
    private int degreeRotation;
    private boolean back;



    /**
     * Default constructor
     * 
     * @param id
     *            Id
     */
    public AnimatedComponent(String id) {
        this(id, null, null);
    }



    /**
     * Constructor with front image specified
     * 
     * @param id
     *            Id
     * @param frontImage
     *            Front image
     */
    public AnimatedComponent(String id, BufferedImage frontImage) {
        this(id, frontImage, null);
    }



    /**
     * Constructor with front image and back image specified
     * 
     * @param id
     *            Id
     * @param frontImage
     *            Front image
     * @param backImage
     *            Back image
     */
    public AnimatedComponent(String id, BufferedImage frontImage, BufferedImage backImage) {
        AssertUtil.notEmpty(id, "id is null !");
        setOpaque(false);
        this.id = id;
        this.frontImage = frontImage;
        this.backImage = backImage;
        this.imageTransform = new AffineTransform();
        this.transform = new AffineTransform();
        applyTransformations(0, 0, 0, 0, 0, false);
        refreshComponent();
    }



    /**
     * @return the id
     */
    public String getId() {
        return id;
    }



    /**
     * @return the frontImage
     */
    public BufferedImage getFrontImage() {
        return frontImage;
    }



    /**
     * @param frontImage
     *            the frontImage to set
     */
    public void setFrontImage(BufferedImage frontImage) {
        this.frontImage = frontImage;
    }



    /**
     * @return the backImage
     */
    public BufferedImage getBackImage() {
        return backImage;
    }



    /**
     * @param backImage
     *            the backImage to set
     */
    public void setBackImage(BufferedImage backImage) {
        this.backImage = backImage;
    }



    /**
     * @return the degreRotation
     */
    public int getDegreRotation() {
        return degreeRotation;
    }



    /**
     * @return the back
     */
    public boolean isBack() {
        return back;
    }



    /**
     * Apply step animation's transformations
     * 
     * @param stepAnimation
     *            Step's animation
     */
    void applyStepAnimationTransformations(StepAnimationAdapter stepAnimation) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Start apply transformations for step: stepsDone=" + stepAnimation.getStepsDone() + ", stepsToDo=" + stepAnimation.getStepsToDo() + ", deltaX=" + stepAnimation.getDeltaXStep() + ", deltaY=" + stepAnimation.getDeltaYStep() + ", deltaWidth=" + stepAnimation.getDeltaWidthStep() + ", deltaHeight=" + stepAnimation.getDeltaHeightStep() + ", deltaDegree=" + stepAnimation.getDeltaDegreeStep() + ", back=" + stepAnimation.isBackStep());
        }
        applyTransformations(stepAnimation.getDeltaDegreeStep(), stepAnimation.getDeltaXStep(), stepAnimation.getDeltaYStep(), stepAnimation.getDeltaWidthStep(), stepAnimation.getDeltaHeightStep(), stepAnimation.isBackStep());
    }



    /**
     * Apply transformations : rotation with delta degree or translation with
     * delta x and delta y or delta width and delta height or back state
     * 
     * @param deltaDegree
     *            Delta degree
     * @param deltaX
     *            Delta x
     * @param deltaY
     *            Delta y
     * @param deltaWidth
     *            Delta width
     * @param deltaHeight
     *            Delta height
     * @param back
     *            Indicator for back state
     */
    private void applyTransformations(int deltaDegree, int deltaX, int deltaY, int deltaWidth, int deltaHeight, boolean back) {

        if (frontImage == null && deltaDegree != 0) {
            LOG.warn("Rotation is not implemented for a panel ! Degre rotation is forced to 0");
            deltaDegree = 0;
        }

        transform = new AffineTransform();

        // Apply back state
        this.back = back;

        // Apply translation
        if (LOG.isDebugEnabled()) {
            LOG.debug("bounds : " + getBounds());
            LOG.debug("boundsWithoutRotation : " + getBoundsWithoutRotation());
            LOG.debug("boundsWithoutTransformations : " + getBoundsWithoutTransformations());
            LOG.debug("Apply translation");
        }

        getBounds().x += deltaX;
        getBounds().y += deltaY;
        getBoundsWithoutRotation().x += deltaX;
        getBoundsWithoutRotation().y += deltaY;

        // Apply resize
        if (LOG.isDebugEnabled()) {
            LOG.debug("bounds : " + getBounds());
            LOG.debug("boundsWithoutRotation : " + getBoundsWithoutRotation());
            LOG.debug("boundsWithoutTransformations : " + getBoundsWithoutTransformations());
            LOG.debug("Apply resize");
        }

        getBoundsWithoutRotation().width += deltaWidth;
        getBoundsWithoutRotation().height += deltaHeight;
        getBounds().width = getBoundsWithoutRotation().width;
        getBounds().height = getBoundsWithoutRotation().height;
        double scaleX = getBoundsWithoutRotation().width <= deltaWidth ? 1.0d : ((double) getBoundsWithoutRotation().width) / ((double) getBoundsWithoutTransformations().width);
        double scaleY = getBoundsWithoutRotation().height <= deltaHeight ? 1.0d : ((double) getBoundsWithoutRotation().height) / ((double) getBoundsWithoutTransformations().height);
        transform.scale(scaleX, scaleY);

        // Apply rotation
        if (LOG.isDebugEnabled()) {
            LOG.debug("bounds : " + getBounds());
            LOG.debug("boundsWithoutRotation : " + getBoundsWithoutRotation());
            LOG.debug("boundsWithoutTransformations : " + getBoundsWithoutTransformations());
            LOG.debug("Apply rotation");
        }

        this.degreeRotation += deltaDegree;
        int limitedDegreRotation = getDegreRotation() % 360;
        if (limitedDegreRotation < 0) {
            limitedDegreRotation = 360 + limitedDegreRotation;
        }
        int angle = limitedDegreRotation;
        int wTmp = 0;
        int wImg = getBounds().width;
        int hImg = getBounds().height;
        while (angle > 90) {
            angle -= 90;
            wTmp = wImg;
            wImg = hImg;
            hImg = wTmp;
        }
        int wPanel = (int) (((double) hImg * Math.cos(Math.toRadians(90 - angle))) + ((double) wImg * Math.cos(Math.toRadians(angle))));
        int hPanel = (int) (((double) wImg * Math.cos(Math.toRadians(90 - angle))) + ((double) hImg * Math.cos(Math.toRadians(angle))));
        transform = new AffineTransform();
        if (limitedDegreRotation >= 0 && limitedDegreRotation <= 90) {
            transform.translate(((double) hImg * Math.cos(Math.toRadians(90 - angle))), 0);
        } else if (limitedDegreRotation >= 90 && limitedDegreRotation <= 180) {
            transform.translate(wPanel, ((double) wImg * Math.cos(Math.toRadians(90 - angle))));
        } else if (limitedDegreRotation >= 180 && limitedDegreRotation <= 270) {
            transform.translate(wPanel - ((double) hImg * Math.cos(Math.toRadians(90 - angle))), hPanel);
        } else {
            transform.translate(0, hPanel - ((double) wImg * Math.cos(Math.toRadians(90 - angle))));
        }
        transform.rotate(Math.toRadians(limitedDegreRotation));
        getBounds().width = wPanel;
        getBounds().height = hPanel;

        transform.scale(scaleX, scaleY);

        if (LOG.isDebugEnabled()) {
            LOG.debug("bounds : " + getBounds());
            LOG.debug("boundsWithoutRotation : " + getBoundsWithoutRotation());
            LOG.debug("boundsWithoutTransformations : " + getBoundsWithoutTransformations());
            LOG.debug("End apply transformations");
        }
    }



    /**
     * Refresh component
     */
    void refreshComponent() {
        setBounds(new Rectangle(getBounds().x, getBounds().y, getBounds().width, getBounds().height));
        this.imageTransform = transform;
        validate();
    }



    /**
     * Get bounds
     * 
     * @return Bounds
     */
    public Rectangle getBounds() {
        if (bounds == null || (bounds.width <= 0 && bounds.height <= 0)) {
            if (frontImage != null) {
                bounds = new Rectangle(0, 0, frontImage.getWidth(), frontImage.getHeight());
            } else {
                bounds = new Rectangle(super.getBounds());
            }
        }
        return bounds;
    }



    /**
     * Get bounds without rotation
     * 
     * @return Bounds without rotation
     */
    public Rectangle getBoundsWithoutRotation() {
        if (boundsWithoutRotation == null || (boundsWithoutRotation.width <= 0 && boundsWithoutRotation.height <= 0)) {
            boundsWithoutRotation = new Rectangle(getBounds());
        }
        return boundsWithoutRotation;
    }



    /**
     * Get bounds without transformations
     * 
     * @return Bounds without transformations
     */
    public Rectangle getBoundsWithoutTransformations() {
        if (boundsWithoutTransformations == null || (boundsWithoutTransformations.width <= 0 && boundsWithoutTransformations.height <= 0)) {
            boundsWithoutTransformations = new Rectangle(getBounds());
        }
        return boundsWithoutTransformations;
    }



    @Override
    public AnimationActionEvt createUIActionEvt() {
        AnimationActionEvt evt = new AnimationActionEvt();
        evt.setAnimatedComponentId(getId());
        return evt;
    }



    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (isBack() && backImage != null) {
            Graphics2D g2 = (Graphics2D) g;
            g2.drawImage(backImage, imageTransform, this);
        } else if (frontImage != null) {
            Graphics2D g2 = (Graphics2D) g;
            g2.drawImage(frontImage, imageTransform, this);
        }
    }
}
