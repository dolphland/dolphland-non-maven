/*
 $Log: CtxInfoManager.java,v $
 Revision 1.20  2014/03/18 13:15:32  bvatan
 - marked all non-nls strings
 - translated french messages into english
 - externalized translatable strings

 Revision 1.19  2009/08/25 07:01:16  bvatan
 - reorg imports

 Revision 1.18  2009/08/07 09:07:21  bvatan
 - les �v�nements de fadein fadeout sont en MAX_SYS_PRIORITY

 Revision 1.17  2009/08/06 12:55:10  bvatan
 - Gestion de l'affichage/masque progressif des bulles d'infos

 Revision 1.16  2009/04/24 15:08:09  bvatan
 - ajout possibilit� de fixer le body point dasn les infos bulles en position absolue

 Revision 1.15  2009/04/23 09:18:38  jscafi
 - Correction bug de log d'attach point non renseign�
 - Correction bug de tooltip non visible dans table d'en cours synoptique

 Revision 1.14  2009/03/10 12:22:53  bvatan
 - passage de traces en debug, info trop verbeux.

 Revision 1.13  2008/11/13 10:07:06  jscafi
 - Diminution des warnings

 Revision 1.12  2008/10/09 09:07:00  jscafi
 - Prise en compte du positionnement du body pour les bulles � positionnement absolu
 - Ajout possibilit� de changer la couleur de fond et d'�crit d'une bulle
 - Correction sur les �v�nements des bulles o� il y avait inversion dans les identifiants d'�venement show et hide
 - Restriction private package pass� en public pour le CtxInfoManager

 Revision 1.11  2008/10/06 07:38:12  jscafi
 - Refactoring de codes

 Revision 1.10  2008/10/03 09:17:07  bvatan
 - suppression de getRootPaneContainer() inutile remplac� par JCompoenet.getRootPane()

 Revision 1.9  2008/09/29 14:38:05  bvatan
 - passe en package private

 Revision 1.8.2.1  2008/09/29 12:48:05  jscafi
 - Am�liorations sur les charts
 - Renommage de m�thodes sur listener de synoptique

 Revision 1.8  2008/09/15 13:52:02  bvatan
 - formatage code
 - modification trace de logs sur r�ception des �v�nements show/hide

 Revision 1.7  2008/09/04 08:58:32  jscafi
 - Ajout de la position automatique de l'info bulle

 Revision 1.6  2008/09/03 09:40:52  jscafi
 - Ajout du placement automatique du contexte info en fonction de la position du composant

 Revision 1.5  2008/09/02 14:03:11  jscafi
 - Ajout de la position automatique de l'info bulle

 Revision 1.4  2008/07/30 09:02:16  bvatan
 - afficnage de la strat�gie de l'algorithme de postionnement
 - ajout javadoc

 Revision 1.3  2008/07/29 08:45:46  bvatan
 - gestion du positionnement des infos bulle

 Revision 1.2  2008/07/23 14:27:27  bvatan
 - merge avec PRODUCT_PAINTING
 - correction du syst�me de coordonn�es (int,float->double)
 - autoalignement des produits avec gestion du mode autoAdjustable
 - fixation de la taille des produits de mani�re uniforme sur tous les composant via SynoptiqueConf
 - corrections bugs sur la taille des fl�ches signaux

 Revision 1.1.2.1  2008/07/21 17:09:09  bvatan
 - merge avec head
 - passage du syst�me de coordonn�e entier vers double

 */

package com.dolphland.client.util.application.components;

import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.dolphland.client.util.event.AppEvents;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.Schedule;
import com.dolphland.client.util.event.UIEvent;
import com.dolphland.client.util.event.UIHideCtxInfoEvt;
import com.dolphland.client.util.event.UIShowAbsoluteCtxInfoEvt;
import com.dolphland.client.util.event.UIShowCtxInfoEvt;

/**
 * Gestionnaire charg� de l'affichage des VisionCtxInfo (bulles d'infos
 * contextuelles)
 * 
 * @author JayJay
 */
public class CtxInfoManager {

	private static final Logger log = Logger.getLogger(CtxInfoManager.class);

	private final EventDestination DEST = new EventDestination();
	
	/**
	 * Contraintes des tip index�es par les compsoant auqueles les tips sont
	 * rattach�s. Donne un tipConstraints pour un composant donn�
	 */
	private Map compCstr;

	/**
	 * Contraintes des tip index�es par le tips aux m�me. Donne un
	 * TipConstraints pour un tip donn�
	 */
	private Map tipCstr;

	/**
	 * Liste des ComponentWatcher index�s par leurs JLayeredPane
	 */
	private Map layerPaneListeners;

	public CtxInfoManager() {
		this.compCstr = new HashMap();
		this.tipCstr = new HashMap();
		this.layerPaneListeners = new HashMap();
		subscribe();
	}

	private void subscribe() {
		EDT.subscribe(new EventWatcher())
			.forEvent(AppEvents.UI_SHOW_CTX_INFO_EVT)
			.forEvent(AppEvents.UI_HIDE_CTX_INFO_EVT)
			.forEvents(DEST);
	}

	private class EventWatcher extends DefaultEventSubscriber {

		public void eventReceived(final UIShowAbsoluteCtxInfoEvt se) {
			JComponent comp = se.getComponent();
			JRootPane rootPane = comp.getRootPane();
			if(rootPane == null) {
				log.error("eventReceived(UIShowAbsoluteCtxInfoEvt): " + comp + " has no RootPaneContainer ancestor"); //$NON-NLS-1$ //$NON-NLS-2$
				return;
			}
			if(log.isDebugEnabled()) {
				log.debug("eventReceived(UIShowAbsoluteCtxInfoEvt): Displaying CtxInfo for " + comp); //$NON-NLS-1$
			}
			JLayeredPane layerPane = rootPane.getLayeredPane();
			float alpha = 0f;
			TipConstraints cstr = getComponentConstraints(comp);
			if(cstr != null) {
				cstr.abortFadeOut();
				alpha = cstr.tip.getAlpha();
				deleteTip(cstr);
			}
			CtxInfo tip = new CtxInfo();
			if(se.getBackground() != null) {
				tip.setBackground(se.getBackground());
			}
			if(se.getForeground() != null) {
				tip.setForeground(se.getForeground());
			}
			tip.setText(se.getText());
			Point ap = SwingUtilities.convertPoint(comp, se.getAttachPoint(), layerPane);

			Point bp = null;
			if(se.isBodyPointAbsolute()) {
				bp = se.getBodyPoint();
			} else {
				switch(se.getBodyPosition()) {
					case UIShowAbsoluteCtxInfoEvt.BOTTOM:
						bp = new Point(ap.x - (int)((double)tip.getBodyWidth() / 2.0d), ap.y + 20);
						break;

					case UIShowAbsoluteCtxInfoEvt.BOTTOM_LEFT:
						bp = new Point(ap.x + 25 - tip.getBodyWidth(), ap.y + 20);
						break;

					case UIShowAbsoluteCtxInfoEvt.BOTTTOM_RIGHT:
						bp = new Point(ap.x - 25, ap.y + 20);
						break;

					case UIShowAbsoluteCtxInfoEvt.LEFT:
						bp = new Point(ap.x - 25 - tip.getBodyWidth(), ap.y - (int)((double)tip.getBodyHeight() / 2.0d));
						break;

					case UIShowAbsoluteCtxInfoEvt.RIGHT:
						bp = new Point(ap.x + 25, ap.y - (int)((double)tip.getBodyHeight() / 2.0d));
						break;

					case UIShowAbsoluteCtxInfoEvt.TOP:
						bp = new Point(ap.x - (int)((double)tip.getBodyWidth() / 2.0d), ap.y - 20 - tip.getBodyHeight());
						break;

					case UIShowAbsoluteCtxInfoEvt.TOP_RIGHT:
						bp = new Point(ap.x - 25, ap.y - 20 - tip.getBodyHeight());
						break;

					case UIShowAbsoluteCtxInfoEvt.TOP_LEFT:
					default:
						bp = new Point(ap.x + 25 - tip.getBodyWidth(), ap.y - 20 - tip.getBodyHeight());
						break;
				}
			}
			cstr = new TipConstraints(comp, rootPane, tip);
			indexConstraints(cstr);
			tip.setBodyPoint(bp);
			tip.setAttachPoint(ap);
			tip.validate();
			tip.setAlpha(alpha);
			layerPane.add(tip, JLayeredPane.PALETTE_LAYER);
			if(se.shouldFadeIn()) {
				cstr.startFadeIn();
			} else {
				tip.setAlpha(1);
			}
			if(se.getLifeTime() > 0) {
				UIHideCtxInfoEvt hideTipEvt = new UIHideCtxInfoEvt(cstr.comp);
				hideTipEvt.setFadeOut(se.shouldFadeOut());
				Schedule sched = Schedule.event(hideTipEvt).startDate(System.currentTimeMillis() + se.getLifeTime());
				EDT.schedule(sched);
			}
		}

		public void eventReceived(final UIShowCtxInfoEvt se) {
			JComponent comp = se.getComponent();
			JRootPane rootPane = comp.getRootPane();
			if(rootPane == null) {
				log.error("eventReceived(UIShowCtxInfoEvt): " + comp + " has no RootPaneContainer ancestor !"); //$NON-NLS-1$ //$NON-NLS-2$
				return;
			}
			if(log.isDebugEnabled()) {
				log.debug("eventReceived(UIShowCtxInfoEvt): Displaying CtxInfo for " + comp); //$NON-NLS-1$
			}
			TipConstraints cstr = getComponentConstraints(comp);
			float alpha = 0;
			if(cstr != null) {
				cstr.abortFadeOut();
				alpha = cstr.tip.getAlpha();
				deleteTip(cstr);
			}
			JLayeredPane layerPane = rootPane.getLayeredPane();
			if(getComponentConstraints(comp) != null)
				return;
			CtxInfo tip = new CtxInfo();
			if(se.getBackground() != null) {
				tip.setBackground(se.getBackground());
			}
			if(se.getForeground() != null) {
				tip.setForeground(se.getForeground());
			}
			tip.setText(se.getText());
			int bodyPointStrategy = adjustBodyStrategy(layerPane, comp, se.getBodyPointStrategy());
			adjustPlacement(layerPane, comp, bodyPointStrategy, se.getAttachPointStrategy(), tip);
			cstr = new TipConstraints(comp, rootPane, tip, bodyPointStrategy, se.getAttachPointStrategy());
			indexConstraints(cstr);
			registerListener(layerPane);
			tip.validate();
			tip.setAlpha(alpha);
			layerPane.add(tip, JLayeredPane.PALETTE_LAYER);
			if(se.shouldFadeIn()) {
				cstr.startFadeIn();
			} else {
				tip.setAlpha(1);
			}
			if(se.getLifeTime() > 0) {
				UIHideCtxInfoEvt hideTipEvt = new UIHideCtxInfoEvt(cstr.comp);
				hideTipEvt.setFadeOut(se.shouldFadeOut());
				Schedule sched = Schedule.event(hideTipEvt).startDate(System.currentTimeMillis() + se.getLifeTime());
				EDT.schedule(sched);
			}
		}

		public void eventReceived(final UIHideCtxInfoEvt he) {
			TipConstraints cstrs = getComponentConstraints(he.getComponent());
			if(cstrs == null) {
				return;
			}
			cstrs.abortFadeIn();
			if(he.shouldFadeOut()) {
				cstrs.startFadeOut();
			} else {
				deleteTip(cstrs);
			}
		}

		public void eventReceived(UIFadeInEvent e) {
			TipConstraints cstrs = e.getTipConstraints();
			CtxInfo tip = cstrs.tip;
			float alpha = tip.getAlpha();
			if(alpha < 1f) {
				tip.setAlpha(alpha+0.2f);
			} else {
				e.getSchedule().destroy();
			}
		}
		
		public void eventReceived(UIFadeOutEvent e) {
			TipConstraints cstrs = e.getTipConstraints();
			CtxInfo tip = cstrs.tip;
			float alpha = tip.getAlpha();
			if(alpha > 0f) {
				tip.setAlpha(alpha-0.2f);
			} else {
				e.getSchedule().destroy();
				deleteTip(cstrs);
			}
		}
	}

	private void deleteTip(TipConstraints cstrs) {
        JRootPane rootPane = cstrs.rootPane;
		if(rootPane == null) {
			log.error("eventReceived(UIHideCtxInfoEvt): " + cstrs.comp + " has no RootPaneContainer ancestor !"); //$NON-NLS-1$ //$NON-NLS-2$
			return;
		}
		if(log.isDebugEnabled()) {
			log.debug("eventReceived(UIHideCtxInfoEvt): Hiding CtxInfo for " + cstrs.comp); //$NON-NLS-1$
		}
		JLayeredPane layerPane = rootPane.getLayeredPane();
		deregisterListener(layerPane);
		TipConstraints cstr = removeComponentConstraints(cstrs.comp);
		if(cstr != null) {
			CtxInfo tip = cstr.tip;
			layerPane.remove(tip);
			layerPane.repaint(tip.getBounds());
		}
	}

	/**
	 * Indexe les contraintes par composant r�f�renc� et par tip
	 * 
	 * @param cstr
	 *            Les contraitnes � indexer
	 */
	private void indexConstraints(TipConstraints cstr) {
		compCstr.put(cstr.comp, cstr);
		tipCstr.put(cstr.tip, cstr);
	}

	/**
	 * Retourne les contraintes du tip associ� au composant sp�cifi�
	 * 
	 * @param comp
	 *            Le composant associ� au tip
	 * @return Les contraintes du tip rattach� au composant sp�cifi�
	 */
	private TipConstraints getComponentConstraints(JComponent comp) {
		return (TipConstraints)compCstr.get(comp);
	}

	/**
	 * Retourne les contraintes du tip sp�cifi�
	 * 
	 * @param tip
	 *            Le VisionCtxInfo dont on veut obtenir les contraintes
	 *            d'affichage
	 * @return Les contraintes d'affichage du tip sp�cifi�
	 */
	private TipConstraints getTipConstraints(CtxInfo tip) {
		return (TipConstraints)tipCstr.get(tip);
	}

	/**
	 * D�sindexe le tip associ� au composant sp�cifi�
	 * 
	 * @param comp
	 *            Le composant dotn on veut d�sindexer le tip qui lui est
	 *            associ�
	 * @return Les contraintes qui �taitent associ�es au tip du composant
	 *         sp�cifi�
	 */
	private TipConstraints removeComponentConstraints(JComponent comp) {
		TipConstraints out = (TipConstraints)compCstr.remove(comp);
		if(out != null)
			tipCstr.remove(out.tip);
		return out;
	}

	/**
	 * Enregistre un ComponentListener sur le JLyaeredPane sp�cifi�. Si le
	 * JLayeredPane a d�j� un ComponentListener retourne ce listener, sinon
	 * enregistre un nouveau ComponentListener.
	 * 
	 * @param layerPane
	 *            Le JLayeredPane � enregistrer
	 * @return Le ComponentListener enregistr� sur le JLayeredPane
	 */
	private ComponentListener registerListener(JLayeredPane layerPane) {
		ComponentListener cl = (ComponentListener)layerPaneListeners.get(layerPane);
		if(cl == null) {
			cl = new ComponentWatcher(layerPane);
			layerPaneListeners.put(layerPane, cl);
			layerPane.addComponentListener(cl);
		}
		return cl;
	}

	/**
	 * Suprrimer l'enregustrement du ComponenetListsner sur le JLayeredPane
	 * sp�cifi� uniquement s'il n'y plus de VisionCtxInfo dans ce JlayeredPane.
	 * 
	 * @param layerPane
	 *            Le JLayeredPane � d�senregistrer
	 * @return Le ComponentListener qui est ou �tait enregistr� sur le
	 *         JLayeredPane
	 */
	private ComponentListener deregisterListener(JLayeredPane layerPane) {
		ComponentListener out = (ComponentListener)layerPaneListeners.remove(layerPane);
		if(out != null) {
			Component[] comps = layerPane.getComponents();
			for(int i = 0; i < comps.length; i++) {
				if(comps[i] instanceof CtxInfo) {
					// ne pas desenregistrer car il reste des tips dans le
					// layerPane
					return out;
				}
			}
			layerPane.removeComponentListener(out);
		}
		return out;
	}

	/**
	 * Ajuste le positionnement du VisionCtxInfo en fonction du positionement
	 * demand� et de la place disponible pour ce positionnement.
	 * 
	 * @param layerPane
	 *            Le JLayeredPane dans lequele s'affiche le VisionCtxInfo
	 * @param comp
	 *            Le composant r�f�rence auquel le VisionCtxInfo se r�f�re
	 * @param originalBodyPointStrategy
	 *            La stragt�gy de positionnement du corps du VisionCtxInfo par
	 *            rapport � comp
	 * @param originalAttachPointStrategy
	 *            Le strat�gy de positionnement du point d'attache du
	 *            VisionCtxInfo par rapport � comp
	 * @param tip
	 *            Le VisionCtxInfo � positionner
	 */
	private void adjustPlacement(JLayeredPane layerPane, JComponent comp, int originalBodyPointStrategy, int originalAttachPointStrategy, CtxInfo tip) {
		Point bp = getBodyPoint(layerPane, comp, originalBodyPointStrategy, tip);
		Point ap = null;
		if(originalAttachPointStrategy == UIShowCtxInfoEvt.AUTO) {
			// attacher du m�me c�t� que le body par rapport au composant
			ap = getAttachPoint(layerPane, comp, originalBodyPointStrategy, tip);
		} else {
			// respecter la strat�gie fix�e par l'�v�nement
			ap = getAttachPoint(layerPane, comp, originalAttachPointStrategy, tip);
		}
		if(bp.x < 0)
			bp.x = 0;
		if(bp.y < 0)
			bp.y = 0;
		if(bp.x + tip.getBodyWidth() > layerPane.getWidth())
			bp.x = layerPane.getWidth() - tip.getBodyWidth();
		if(bp.y + tip.getBodyHeight() > layerPane.getHeight())
			bp.y = layerPane.getHeight() - tip.getBodyHeight();
		Point newBP = new Point(bp);
		Point newAP = new Point(ap);
		Point loc = SwingUtilities.convertPoint(comp.getParent(), comp.getLocation(), layerPane);
		Rectangle cBounds = new Rectangle(loc, comp.getSize());
		// si collision avec le composant il faut d�caler l'affichage de la
		// bulle
		if(cBounds.intersects(new Rectangle(bp, tip.getBodySize()))) {
			int th = tip.getBodyHeight();
			int tw = tip.getBodyWidth();
			int spaceLeft = loc.x;
			int spaceRight = layerPane.getWidth() - (loc.x + comp.getWidth());
			int spaceTop = loc.y;
			int spaceBottom = layerPane.getHeight() - (loc.y + comp.getHeight());
			// afficher l� o� il y a de la place et d�placer le point d'attache
			// du m�me c�t�
			// que le body par rapport au composant
			if(spaceTop >= th) {
				newBP = getBodyPoint(layerPane, comp, UIShowCtxInfoEvt.TOP, tip);
				newAP = getAttachPoint(layerPane, comp, UIShowCtxInfoEvt.TOP, tip);
			} else if(spaceBottom >= th) {
				newBP = getBodyPoint(layerPane, comp, UIShowCtxInfoEvt.BOTTOM, tip);
				newAP = getAttachPoint(layerPane, comp, UIShowCtxInfoEvt.BOTTOM, tip);
			} else if(spaceLeft >= tw) {
				newBP = getBodyPoint(layerPane, comp, UIShowCtxInfoEvt.LEFT, tip);
				newAP = getAttachPoint(layerPane, comp, UIShowCtxInfoEvt.LEFT, tip);
			} else if(spaceRight >= tw) {
				newBP = getBodyPoint(layerPane, comp, UIShowCtxInfoEvt.RIGHT, tip);
				newAP = getAttachPoint(layerPane, comp, UIShowCtxInfoEvt.RIGHT, tip);
			}
		}
		tip.setBodyPoint(newBP);
		tip.setAttachPoint(newAP);
	}

	/**
	 * Retourne la position du cors du VisionCtxInfo dans le JLayeredPane en
	 * fonction de la strat�gy de positionnement sp�cifi�
	 * 
	 * @param layerPane
	 *            Le JLayeredPane dans lequel s'affiche le VisionCtxInfo
	 * @param c
	 *            Le composant auquel le VisionCtx se r�f�re
	 * @param bodyPointStrategy
	 *            La strat�gy de positionnement du corps par rapport � comp
	 * @param tip
	 *            Le VisionCtxInfo � positionner
	 * @return La position du corps du visionCtxInfo sp�cifi� dans le
	 *         JLayeredPane sp�cifi�
	 */
	private Point getBodyPoint(JLayeredPane layerPane, JComponent c, int bodyPointStrategy, CtxInfo tip) {
		Point out = new Point();
		Point loc = SwingUtilities.convertPoint(c.getParent(), c.getLocation(), layerPane);
		switch(bodyPointStrategy) {
			case UIShowCtxInfoEvt.LEFT:
				out.x = loc.x - tip.getBodyWidth() - 15;
				out.y = loc.y - 15;
				break;
			case UIShowCtxInfoEvt.TOP_LEFT:
				out.x = loc.x - tip.getBodyWidth() - 15;
				out.y = loc.y - tip.getBodyHeight() - 15;
				break;
			case UIShowCtxInfoEvt.TOP:
				out.x = loc.x - 15;
				out.y = loc.y - tip.getBodyHeight() - 15;
				break;
			case UIShowCtxInfoEvt.TOP_RIGHT:
				out.x = loc.x + c.getWidth() + 15;
				out.y = loc.y - tip.getBodyHeight() - 15;
				break;
			case UIShowCtxInfoEvt.RIGHT:
				out.x = loc.x + c.getWidth() + 15;
				out.y = loc.y - 15;
				break;
			case UIShowCtxInfoEvt.BOTTOM_RIGHT:
				out.x = loc.x + c.getWidth() + 15;
				out.y = loc.y + c.getHeight() + 15;
				break;
			case UIShowCtxInfoEvt.BOTTOM:
				out.x = loc.x - 15;
				out.y = loc.y + c.getHeight() + 15;
				break;
			case UIShowCtxInfoEvt.BOTTOM_LEFT:
				out.x = loc.x - tip.getBodyWidth() - 15;
				out.y = loc.y + c.getHeight() + 15;
				break;
			default:
				log.error("getBodyPoint(): Unknown bodyPointStrategy !"); //$NON-NLS-1$
				return null;
		}
		return out;
	}

	/**
	 * Retourne la position du point d'attache du VisionCtxInfo dans le
	 * JLayeredPane en fonction de la strat�gy de positionnement sp�cifi�
	 * 
	 * @param layerPane
	 *            Le JLayeredPane dans lequel s'affiche le VisionCtxInfo
	 * @param c
	 *            Le composant auquel le VisionCtx se r�f�re
	 * @param bodyPointStrategy
	 *            La strat�gy de positionnement du point d'attache par rapport �
	 *            comp
	 * @param tip
	 *            Le CtxInfo � positionner
	 * @return La position du point d'attache du visionCtxInfo sp�cifi� dans le
	 *         JLayeredPane sp�cifi�
	 */
	private Point getAttachPoint(JLayeredPane layerPane, JComponent c, int attachStrategy, CtxInfo tip) {
		Point out = new Point();
		Point loc = SwingUtilities.convertPoint(c.getParent(), c.getLocation(), layerPane);
		switch(attachStrategy) {
			case UIShowCtxInfoEvt.LEFT:
				out.x = loc.x;
				out.y = loc.y + 10;
				break;
			case UIShowCtxInfoEvt.TOP_LEFT:
				out.x = loc.x;
				out.y = loc.y;
				break;
			case UIShowCtxInfoEvt.TOP:
				out.x = loc.x + 15;
				out.y = loc.y;
				break;
			case UIShowCtxInfoEvt.TOP_RIGHT:
				out.x = loc.x + c.getWidth();
				out.y = loc.y;
				break;
			case UIShowCtxInfoEvt.RIGHT:
				out.x = loc.x + c.getWidth();
				out.y = loc.y + 10;
				break;
			case UIShowCtxInfoEvt.BOTTOM_RIGHT:
				out.x = loc.x + c.getWidth();
				out.y = loc.y + c.getHeight();
				break;
			case UIShowCtxInfoEvt.BOTTOM:
				out.x = loc.x + 15;
				out.y = loc.y + c.getHeight();
				break;
			case UIShowCtxInfoEvt.BOTTOM_LEFT:
				out.x = loc.x;
				out.y = loc.y + c.getHeight();
				break;
			default:
				log.error("getAttachPoint(): Unknown attachPointStrategy !"); //$NON-NLS-1$
				return null;
		}
		return out;
	}

	/**
	 * Ajuste la strat�gie du body en fournissant dans le cas AUTO la strat�gie
	 * en fonction de la position composant par rapport au layerPane
	 */
	private int adjustBodyStrategy(JLayeredPane layerPane, JComponent c, int bodyStrategy) {

		if(bodyStrategy != UIShowCtxInfoEvt.AUTO) {
			return bodyStrategy;
		} else {
			Point loc = SwingUtilities.convertPoint(c.getParent(), c.getLocation(), layerPane);

			int width = layerPane.getWidth();
			int height = layerPane.getHeight();
			int x = loc.x;
			int y = loc.y;

			// Si composant positionn� en haut � gauche du layerPane
			if(y < height / 2 && x < width / 2) {
				return UIShowCtxInfoEvt.BOTTOM_RIGHT;
			}
			// Si composant positionn� en haut � droite du layerPane
			else if(y < height / 2 && x >= width / 2) {
				return UIShowCtxInfoEvt.BOTTOM_LEFT;
			}
			// Si composant positionn� en bas � gauche du layerPane
			else if(y >= height / 2 && x < width / 2) {
				return UIShowCtxInfoEvt.TOP_RIGHT;
			}

			// Si aucun des cas, on affiche en haut � gauche
			return UIShowCtxInfoEvt.TOP_LEFT;
		}
	}

	/**
	 * ComponentListener du JLayeredPane qui contient le VisionCtxInfo. Les
	 * VisionCtxInfo sont repositionn�s si le JLayered est redimensionn�.<br>
	 * Les VisionCtxInfo dont la position est d�finie de mani�re absolue ne sont
	 * pas repositionn�s
	 * 
	 * @author JayJay
	 */
	private class ComponentWatcher extends ComponentAdapter {
		private JLayeredPane layerPane;

		private ComponentWatcher(JLayeredPane layerPane) {
			this.layerPane = layerPane;
		}

		public void componentResized(ComponentEvent e) {
			Component[] comps = layerPane.getComponents();
			for(int i = 0; i < comps.length; i++) {
				// v�rifier qu'il n'existe plus de CtxInfo dans le
				// JLayeredPane
				if(comps[i] instanceof CtxInfo) {
					CtxInfo tip = (CtxInfo)comps[i];
					TipConstraints cstr = getTipConstraints(tip);
					if((cstr != null) && !cstr.absolute) {
						// repositionner le VisionCtxInfo
						adjustPlacement(layerPane, cstr.comp, cstr.bodyPointStrategy, cstr.attachPointStrategy, tip);
						tip.validate();
					}
				}
			}
		}
	}

	/**
	 * Contraintes d'affichage d'un CtxInfo
	 * 
	 * @author JayJay
	 */
	private class TipConstraints {
		private static final long fadeDelay = 50;
		private int bodyPointStrategy;
		private int attachPointStrategy;
		private CtxInfo tip;
		private JComponent comp;
        private JRootPane rootPane;
		private boolean absolute;
		private Schedule fadeInSchedule;
		private Schedule fadeOutSchedule;

		TipConstraints(JComponent comp, JRootPane rootPane, CtxInfo tip, int bodyPointStrategy, int attachPointStrategy) {
			if(tip == null) {
				throw new NullPointerException("tip is null !"); //$NON-NLS-1$
			}
			if(comp == null) {
				throw new NullPointerException("comp is null !"); //$NON-NLS-1$
			}
            if(rootPane == null) {
                throw new NullPointerException("rootPane is null !"); //$NON-NLS-1$
            }
			this.tip = tip;
			this.comp = comp;
            this.rootPane = rootPane;
			this.bodyPointStrategy = bodyPointStrategy;
			this.attachPointStrategy = attachPointStrategy;
			this.absolute = false;
		}

		TipConstraints(JComponent comp, JRootPane rootPane, CtxInfo tip) {
			if(tip == null) {
				throw new NullPointerException("tip is null !"); //$NON-NLS-1$
			}
			if(comp == null) {
				throw new NullPointerException("comp is null !"); //$NON-NLS-1$
			}
			this.tip = tip;
			this.comp = comp;
            this.rootPane = rootPane;
			this.absolute = true;
		}

		public void startFadeIn() {
			UIFadeInEvent fadeInEvt = new UIFadeInEvent(this);
			Schedule sched = Schedule.event(fadeInEvt).interval(fadeDelay);
			fadeInEvt.setSchedule(sched);
			EDT.schedule(sched);
			this.fadeInSchedule = sched;
		}
		
		public void abortFadeIn() {
			if(fadeInSchedule != null) {
				fadeInSchedule.destroy();
				fadeInSchedule = null;
			}
		}
		
		public void startFadeOut() {
			UIFadeOutEvent fadeOutEvt = new UIFadeOutEvent(this);
			Schedule sched = Schedule.event(fadeOutEvt).interval(fadeDelay);
			fadeOutEvt.setSchedule(sched);
			EDT.schedule(sched);
			this.fadeOutSchedule = sched;
		}
		
		public void abortFadeOut() {
			if(fadeOutSchedule != null) {
				fadeOutSchedule.destroy();
				fadeOutSchedule = null;
			}
		}
		
	}

	private class UIFadeInEvent extends UIEvent {
		private TipConstraints tipConstraints;
		private Schedule schedule;
		public UIFadeInEvent(TipConstraints cstrs) {
			super(DEST);
			this.tipConstraints = cstrs;
			setPriority(MAX_SYS_PRIORITY);
		}
		public void setSchedule(Schedule sched) {
			this.schedule = sched;
		}
		public Schedule getSchedule() {
			return schedule;
		}
		public TipConstraints getTipConstraints() {
			return tipConstraints;
		}
	}
	
	private class UIFadeOutEvent extends UIEvent {
		private TipConstraints tipConstraints;
		private Schedule schedule;
		public UIFadeOutEvent(TipConstraints cstrs) {
			super(DEST);
			this.tipConstraints = cstrs;
			setPriority(MAX_SYS_PRIORITY);
		}
		public void setSchedule(Schedule sched) {
			this.schedule = sched;
		}
		public Schedule getSchedule() {
			return schedule;
		}
		public TipConstraints getTipConstraints() {
			return tipConstraints;
		}
	}
	
}
