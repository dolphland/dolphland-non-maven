package com.dolphland.client.util.table;

import java.util.List;

import com.dolphland.client.util.action.UIActionEvt;

/**
 * Classe Ev�nement permettant de fournir les param�tres n�cessaires �
 * l'ex�cution d'une <code>ProductAction</code>
 * 
 * @author J�r�my Scafi
 * @author JayJay
 * 
 */
public class ProductActionEvt extends UIActionEvt {

    private volatile ProductTable productTable;
    private volatile List<ProductTableRow> allRows;
    private volatile List<ProductTableRow> selectedRows;
    private volatile List<ProductAdapter> allProducts;
    private volatile List<ProductAdapter> selectedProducts;
    private volatile ProductTableRow firstRowBeforeSelected;
    private volatile ProductTableRow firstRowAfterSelected;



    public ProductTable getProductTable() {
        return productTable;
    }



    public void setProductTable(ProductTable productTable) {
        this.productTable = productTable;
    }



    public List<ProductTableRow> getAllRows() {
        return allRows;
    }



    public void setAllRows(List<ProductTableRow> allRows) {
        this.allRows = allRows;
    }



    public List<ProductTableRow> getSelectedRows() {
        return selectedRows;
    }



    public void setSelectedRows(List<ProductTableRow> selectedRows) {
        this.selectedRows = selectedRows;
    }



    public List<ProductAdapter> getAllProducts() {
        return allProducts;
    }



    public void setAllProducts(List<ProductAdapter> allProducts) {
        this.allProducts = allProducts;
    }



    public List<ProductAdapter> getSelectedProducts() {
        return selectedProducts;
    }



    public void setSelectedProducts(List<ProductAdapter> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }



    public ProductTableRow getFirstRowBeforeSelected() {
        return firstRowBeforeSelected;
    }



    public void setFirstRowBeforeSelected(ProductTableRow firstRowBeforeSelected) {
        this.firstRowBeforeSelected = firstRowBeforeSelected;
    }



    public ProductTableRow getFirstRowAfterSelected() {
        return firstRowAfterSelected;
    }



    public void setFirstRowAfterSelected(ProductTableRow firstRowAfterSelected) {
        this.firstRowAfterSelected = firstRowAfterSelected;
    }

}
