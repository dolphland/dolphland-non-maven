/*
 * $Log: FwkServiceLocator.java,v $
 * Revision 1.10 2014/03/10 16:10:46 bvatan
 * - Internationalized
 * Revision 1.9 2013/03/25 15:05:27 bvatan
 * - FWK-46: services.xml file is not stored on disk anymore
 * Revision 1.8 2012/07/23 12:47:07 bvatan
 * - genericised getBean() return type (autocast)
 * - added method to get beans byt hteir type
 * Revision 1.7 2011/09/08 15:07:16 bvatan
 * - correction bug chargement du fichier de service Spring pour permettre
 * l'import de fichier de resources � partir du fichier de service SPring
 * Revision 1.6 2009/06/25 11:11:47 bvatan
 * - ajout exception dans trace de log sur echec d'obtention d'un bean du
 * conetxt spring
 * Revision 1.5 2009/03/12 07:27:08 bvatan
 * - r�duction de la contention
 * Revision 1.4 2009/01/28 13:34:56 bvatan
 * - ajout propri�t� environement
 * Revision 1.3 2008/12/19 14:09:56 bvatan
 * - interception des exceptions dans getBean() lorsque le bean demand� n'existe
 * pas. Ajout d'une trace de log info � la place de l'exception
 * Revision 1.2 2008/11/28 10:06:10 bvatan
 * - ajotu service d'aide � l'obtention des services standards
 * Revision 1.1 2008/11/27 07:38:34 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.io.Resource;

/**
 * ServiceLocator qui permet de charger et d'obtenir les services d�clar�s dans
 * SERVICES_XML.<br>
 * 
 * @author JayJay
 */
public class FwkServiceLocator {

    private static final Logger log = Logger.getLogger(FwkServiceLocator.class);

    private static FwkServiceLocator instance;

    private GenericApplicationContext ctx;

    private volatile CEnvironement environment;

    static {
        instance = new FwkServiceLocator();
    }



    private FwkServiceLocator() {
    }



    public static FwkServiceLocator getInstance() {
        return instance;
    }



    /**
     * Charge les services d�clar�s dans SERVICES_XML et substitue les variables
     * par leurs valeurs.
     * 
     * @param properties
     *            Table des valeurs des variables index�es par leurs noms.
     */
    public synchronized void loadServices(Resource r) {
        loadServices(r, null);
    }



    /**
     * <p>
     * Load declared bean definitions in the specified {@link Resource} and
     * merge the specified properties with it.
     * </p>
     * 
     * @param r
     *            The resource pointing to the bean defintions to be loaded.
     * @param properties
     *            The properties to be replaced in the bean defintion resource.
     *            If null then assume no properties are to be replaced
     */
    public synchronized void loadServices(Resource r, Map<String, String> properties) {
        if (ctx != null) {
            log.info("loadServices(): Services loading has been ignored, loadServices() has already been called !"); //$NON-NLS-1$
            return;
        }
        ctx = new GenericApplicationContext();
        // register a property placeholder configurer in any case
        BeanDefinition propertyPlaceholderBeanDefinition = BeanDefinitionBuilder.genericBeanDefinition(PropertySourcesPlaceholderConfigurer.class).getBeanDefinition();
        ctx.registerBeanDefinition("propertySourcePlaceholderConfigurer", propertyPlaceholderBeanDefinition); //$NON-NLS-1$
        // if properties are specified then add a MapPropertySource
        if (properties != null) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            for (Entry<String, String> entry : properties.entrySet()) {
                map.put(entry.getKey(), entry.getValue());
            }
            MapPropertySource out = new MapPropertySource("applicationProperties", map); //$NON-NLS-1$
            ctx.getEnvironment().getPropertySources().addFirst(out);
        }
        // register bean defintion from the specified Resource
        XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(ctx);
        xmlReader.setValidationMode(XmlBeanDefinitionReader.VALIDATION_XSD);
        xmlReader.loadBeanDefinitions(r);
        ctx.refresh();
    }



    public Object getRelayer() {
        return getBean("relayer"); //$NON-NLS-1$
    }



    public Object getRequester() {
        return getBean("requester"); //$NON-NLS-1$
    }



    public Object getConnector() {
        return getBean("connector"); //$NON-NLS-1$
    }



    void setEnvironment(CEnvironement env) {
        if (env == null) {
            throw new NullPointerException("env is null !"); //$NON-NLS-1$
        }
        this.environment = env;
    }



    public CEnvironement getEnvironment() {
        return environment;
    }



    /**
     * Retourne le service dont l'id sp�cifi� est d�clar� dans SERVICES_XML
     * 
     * @param beanId
     *            L'id du service
     * @return Le service idnetifi� par l'id sp�cifi�
     */
    @SuppressWarnings("unchecked")//$NON-NLS-1$
    public synchronized <T> T getBean(String beanId) {
        if (ctx == null) {
            throw new IllegalStateException("loadServices() has not been called !"); //$NON-NLS-1$
        }
        try {
            return (T) ctx.getBean(beanId);
        } catch (Exception e) {
            if (log.isInfoEnabled()) {
                log.info("getBean(): Unable to get bean [" + beanId + "] from Spring context.", e); //$NON-NLS-1$ //$NON-NLS-2$
            }
            return null;
        }
    }



    /**
     * <p>
     * Get all the context beans whose class is specified.
     * </p>
     * 
     * @see BeanFactoryUtils#beansOfTypeIncludingAncestors(org.springframework.beans.factory.ListableBeanFactory,
     *      Class)
     * @param beanClass
     *            The class of the beans to be returned
     * @return A map that index returned beans by their names
     */
    public synchronized <T> Map<String, T> getBeans(Class<T> beanClass) {
        if (ctx == null) {
            throw new IllegalStateException("loadServices() has not been called !"); //$NON-NLS-1$
        }
        try {
            return BeanFactoryUtils.beansOfTypeIncludingAncestors(ctx, beanClass);
        } catch (Exception e) {
            if (log.isInfoEnabled()) {
                log.info("getBean(): Could not get bean of class [" + beanClass.getName() + "] from spring context.", e); //$NON-NLS-1$ //$NON-NLS-2$
            }
            return null;
        }
    }

}
