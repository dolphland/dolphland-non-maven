package com.dolphland.client.util.animator;

import com.dolphland.client.util.assertion.AssertUtil;

/**
 * Animator's adapter
 * 
 * @author jayjay
 * 
 */
class StepAnimationAdapter {

    private Animation animation;
    private int stepsToDo;
    private int stepsDone;



    /**
     * Constructor with animation and steps to do
     * 
     * @param animation
     *            Animation
     * @param stepsToDo
     *            Steps to do
     */
    public StepAnimationAdapter(Animation animation, int stepsToDo) {
        AssertUtil.notNull(animation, "animation is null !");
        this.animation = animation;
        this.stepsToDo = stepsToDo;
    }



    /**
     * Round number
     * 
     * @param num
     *            Number
     * 
     * @return Number rounded
     */
    private int round(double num) {
        int result = (int) Math.floor(num);
        if (((int) Math.floor(num + 0.5d)) > result) {
            result++;
        }
        return result;
    }



    /**
     * Get remaining delta x
     * 
     * @return Remaining delta x
     */
    public int getRemainingDeltaX() {
        return (animation.getParameter().getX() != null ? animation.getParameter().getX() - animation.getComponent().getBoundsWithoutRotation().x : 0);
    }



    /**
     * Get remaining delta y
     * 
     * @return Remaining delta y
     */
    public int getRemainingDeltaY() {
        return (animation.getParameter().getY() != null ? animation.getParameter().getY() - animation.getComponent().getBoundsWithoutRotation().y : 0);
    }



    /**
     * Get remaining delta width
     * 
     * @return Remaining delta width
     */
    public int getRemainingDeltaWidth() {
        int deltaWidth = (animation.getParameter().getWidth() != null ? animation.getParameter().getWidth() - animation.getComponent().getBoundsWithoutRotation().width : 0);
        if (deltaWidth == 0) {
            deltaWidth = (animation.getParameter().getRatioWidth() != null ? (int) (((double) animation.getComponent().getBoundsWithoutTransformations().width) * animation.getParameter().getRatioWidth()) - animation.getComponent().getBoundsWithoutRotation().width : 0);
        }
        return deltaWidth;
    }



    /**
     * Get remaining delta height
     * 
     * @return Remaining delta height
     */
    public int getRemainingDeltaHeight() {
        int deltaHeight = (animation.getParameter().getHeight() != null ? animation.getParameter().getHeight() - animation.getComponent().getBoundsWithoutRotation().height : 0);
        if (deltaHeight == 0) {
            deltaHeight = (animation.getParameter().getRatioHeight() != null ? (int) (((double) animation.getComponent().getBoundsWithoutTransformations().height) * animation.getParameter().getRatioHeight()) - animation.getComponent().getBoundsWithoutRotation().height : 0);
        }
        return deltaHeight;
    }



    /**
     * Get remaining delta degree
     * 
     * @return Remaining delta degree
     */
    public int getRemainingDeltaDegree() {
        return (animation.getParameter().getDegreRotation() != null ? animation.getParameter().getDegreRotation() - animation.getComponent().getDegreRotation() : 0);
    }



    /**
     * Get animation
     * 
     * @return
     */
    public Animation getAnimation() {
        return animation;
    }



    /**
     * Get delta x
     * 
     * @return Delta x
     */
    public int getDeltaXStep() {
        return stepsToDo == stepsDone + 1 ? getRemainingDeltaX() : round((double) getRemainingDeltaX() / (double) (stepsToDo - stepsDone));
    }



    /**
     * Get delta y
     * 
     * @return Delta y
     */
    public int getDeltaYStep() {
        return stepsToDo == stepsDone + 1 ? getRemainingDeltaY() : round((double) getRemainingDeltaY() / (double) (stepsToDo - stepsDone));
    }



    /**
     * Get delta width
     * 
     * @return Delta width
     */
    public int getDeltaWidthStep() {
        return stepsToDo == stepsDone + 1 ? getRemainingDeltaWidth() : round((double) getRemainingDeltaWidth() / (double) (stepsToDo - stepsDone));
    }



    /**
     * Get delta height
     * 
     * @return Delta height
     */
    public int getDeltaHeightStep() {
        return stepsToDo == stepsDone + 1 ? getRemainingDeltaHeight() : round((double) getRemainingDeltaHeight() / (double) (stepsToDo - stepsDone));
    }



    /**
     * Get delta degree
     * 
     * @return Delta degree
     */
    public int getDeltaDegreeStep() {
        return stepsToDo == stepsDone + 1 ? getRemainingDeltaDegree() : round((double) getRemainingDeltaDegree() / (double) (stepsToDo - stepsDone));
    }



    /**
     * Get back indicator
     * 
     * @return Back indicator
     */
    public boolean isBackStep() {
        return animation.getParameter().isBack() != null ? animation.getParameter().isBack() : animation.getComponent().isBack();
    }



    /**
     * Get background indicator
     * 
     * @return Background indicator
     */
    public boolean isBackgroundLayerStep() {
        return animation.getParameter().getLayerPolicy() == null ? false : animation.getParameter().getLayerPolicy() == AnimationParameter.LAYER_BACKGROUND;
    }



    /**
     * Get foreground indicator
     * 
     * @return Foreground indicator
     */
    public boolean isForegroundLayerStep() {
        return animation.getParameter().getLayerPolicy() == null ? false : animation.getParameter().getLayerPolicy() == AnimationParameter.LAYER_FOREGROUND;
    }



    /**
     * Get steps done
     * 
     * @return Steps done
     */
    public int getStepsDone() {
        return stepsDone;
    }



    /**
     * Get steps to do
     * 
     * @return Steps to do
     */
    public int getStepsToDo() {
        return stepsToDo;
    }



    /**
     * Increment step
     */
    public void incrementStep() {
        stepsDone++;
    }
}
