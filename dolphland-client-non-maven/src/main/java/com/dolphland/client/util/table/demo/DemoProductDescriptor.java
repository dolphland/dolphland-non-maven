package com.dolphland.client.util.table.demo;

import com.dolphland.client.util.table.ProductDescriptor;
import com.dolphland.client.util.table.ProductProperty;


public class DemoProductDescriptor extends ProductDescriptor{

    public static ProductProperty PROP_NO = new ProductProperty("number", "N)");
    public static ProductProperty PROP_TEST = new ProductProperty("test", "Test");
    public static ProductProperty PROP_ACTION_1 = new ProductProperty("action1", "Action 1", ProductProperty.ACTION_TYPE);
    public static ProductProperty PROP_ACTION_2 = new ProductProperty("action2", "Action 2", ProductProperty.ACTION_TYPE);
    
    
    public DemoProductDescriptor() {
        addProperty(PROP_NO);
        addProperty(PROP_TEST);
        addProperty(PROP_ACTION_1);
        addProperty(PROP_ACTION_2);
    }

}
