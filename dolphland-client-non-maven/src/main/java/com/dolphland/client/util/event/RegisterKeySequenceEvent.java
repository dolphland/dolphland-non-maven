package com.dolphland.client.util.event;

import com.dolphland.client.util.exception.AppException;

/**
 * Lorsque cet �v�nement est post�, l'�v�nement sp�cifi� dans son constructeur
 * sera post� d�s lors que <br>
 * la s�quence de touches sp�cifi�e sera d�tect�e dans le d�lai sp�cifi�.<br>
 * 
 * 
 * @author JayJay
 */
public final class RegisterKeySequenceEvent extends UIEvent {

    public static final int DEFAULT_DELAY = 5;

    static final EventDestination DEST = new EventDestination();

    private int[] keySequence;

    private Event event;

    private EventDestination eventDestination;

    private int delay;



    /**
     * Conditionne l'�mission de l'�v�nement sp�cifi� � la d�tection de la
     * frappe de la s�quence de touches<br>
     * sp�cifi�e. La frappe des touches doit se faire dans le d�lai sp�cifi�.<br>
     * Si les condition sont remplies (frappe de la bonne s�quence de touche
     * dans le d�lai), l'�v�nement<br>
     * sp�cifi� sera �mis vers la destination s�pcifi�e.
     * 
     * @param keySequence
     *            S�quence de touche � d�tecter (KeyEvent.VK_****)
     * @param evt
     *            Ev�nement � �mettre quand la s�quence de touche a �t� d�tecter
     *            dans le d�lai.
     * @param destination
     *            Destination de l'�v�nement � �mettre.
     * @param delay
     *            D�lai en seconde pourla frappe de la s�quence de touches.
     */
    public RegisterKeySequenceEvent(int[] keySequence, Event evt, EventDestination destination, int delay) {
        super(DEST);
        if (keySequence == null) {
            throw new NullPointerException("keySequence is null !"); //$NON-NLS-1$
        }
        if (evt == null) {
            throw new NullPointerException("evt is null !"); //$NON-NLS-1$
        }
        if (destination == null) {
            destination = evt.getDestination();
        }
        if (delay < 0) {
            throw new AppException("delay < 0 !"); //$NON-NLS-1$
        }
        this.keySequence = keySequence;
        this.event = evt;
        this.eventDestination = destination;
        this.delay = delay;
    }



    public RegisterKeySequenceEvent(int[] keySequence, Event evt) {
        this(keySequence, evt, evt.getDestination(), DEFAULT_DELAY);
    }



    public RegisterKeySequenceEvent(int[] keySequence, Event evt, int delay) {
        this(keySequence, evt, evt.getDestination(), delay);
    }



    public int[] getKeySequence() {
        return keySequence;
    }



    public Event getEvent() {
        return event;
    }



    public EventDestination getEventDestination() {
        return eventDestination;
    }



    public int getDelay() {
        return delay;
    }

}
