package com.dolphland.client.util.concurrent;

import java.util.LinkedList;

import org.apache.log4j.Logger;

/**
 * Classe d'ex�cution de taches asyncnhrones.
 * 
 * 
 * @author JayJay
 */
public class ThreadWorker {

    private static final Logger log = Logger.getLogger(ThreadWorker.class);

    private LinkedList tasks;

    private Worker worker;

    private Thread thread;



    public ThreadWorker(String name) {
        worker = new Worker();
        thread = new Thread(worker, name);
        tasks = new LinkedList();
    }



    public void postpone(Runnable task) {
        synchronized (tasks) {
            tasks.addFirst(task);
            tasks.notify();
        }
    }



    public synchronized void stop() {
        if (!worker.shouldRun) {
            return;
        }
        worker.shouldRun = false;
        thread.interrupt();
    }



    public synchronized void start() {
        if (worker.shouldRun) {
            return;
        }
        worker.shouldRun = true;
        thread.start();
    }

    private class Worker implements Runnable {

        private boolean shouldRun;



        public void run() {
            log.info("WorkerThread started."); //$NON-NLS-1$
            while (shouldRun) {
                try {
                    synchronized (tasks) {
                        while (tasks.size() < 1) {
                            tasks.wait();
                        }
                        Runnable task = (Runnable) tasks.removeLast();
                        task.run();
                    }
                } catch (InterruptedException ie) {
                    shouldRun = false;
                }
            }
            log.info("WorkerThread stopped."); //$NON-NLS-1$
        }
    }
}
