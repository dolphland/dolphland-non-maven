package com.dolphland.client.util.i18n.varlist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.util.Assert;

import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * <p>
 * Contains the list of variables that have been parsed by {@link VarParser}
 * </p>
 * 
 * @author JayJay
 */
public class Variables {

    private static final Variable[] EMPTY_VAR_ARRAY = new Variable[0];

    // variables list
    private List<Variable> variables;

    // variables indexed by their names
    private Map<String, List<Variable>> varMap;

    // lock indicator, when true this instance cannot be modified
    private boolean locked;

    // the extracted variable names (cached)
    private String[] variableNames;



    /**
     * <p>
     * Builds a new instance with the specified list of variables
     * </p>
     * <p>
     * The instance is immediately locked and connot be modified anymore.
     * </p>
     * 
     * @param variables
     *            The variable list that this instance should hold
     */
    public Variables(List<Variable> variables) {
        Assert.notNull(variables, "variables cannot be null"); //$NON-NLS-1$
        this.variables = new ArrayList<Variable>();
        this.varMap = new TreeMap<String, List<Variable>>();
        for (Variable var : variables) {
            add(var);
        }
        lock();
    }



    /**
     * <p>
     * Build a new instance that is not locked, variables can later be added
     * with the <code>add</code> method until the <code>lock()</code> is called
     * </p>
     */
    Variables() {
        this.variables = new ArrayList<Variable>();
        this.varMap = new TreeMap<String, List<Variable>>();
    }



    /**
     * Lock this instance so that it cannot be modified anymore.
     */
    void lock() {
        this.locked = true;
        this.variables = Collections.unmodifiableList(variables);
    }



    /**
     * Add a variable. If the instance has been locked and exception will be
     * thrown.
     * 
     * @param var
     *            The variabel to be added
     */
    void add(Variable var) {
        if (locked) {
            throw new IllegalStateException("Cannot add variable anymore to this instance: instance is locked"); //$NON-NLS-1$
        }
        // ignore null variables
        if (var == null) {
            return;
        }
        this.variables.add(var);
        List<Variable> vars = this.varMap.get(var.getName());
        if (vars == null) {
            vars = new ArrayList<Variable>();
            this.varMap.put(var.getName(), vars);
        }
        vars.add(var);
    }



    /**
     * <p>
     * Get the list of variable names in their order of appearance.
     * </p>
     * 
     * @return An array of <code>String</code> that holds the variable names
     */
    public String[] getVariableNames() {
        if (locked) {
            if (this.variableNames == null) {
                this.variableNames = extractVariableNames();
            }
            return this.variableNames;
        } else {
            return extractVariableNames();
        }
    }



    /**
     * Get the list of variable names in their order of appearance.
     * 
     * @return An array of <code>String</code> that holds the variable names in
     *         the right order of appearance
     */
    private String[] extractVariableNames() {
        LinkedHashSet<String> set = new LinkedHashSet<String>();
        for (Variable v : variables) {
            set.add(v.getName());
        }
        return set.toArray(new String[set.size()]);
    }



    /**
     * <p>
     * Return the variable list with order of appearance preserved.
     * </p>
     * 
     * @return
     */
    public List<Variable> getVariables() {
        return variables;
    }



    /**
     * <p>
     * Get the number of variables hold in this instance
     * </p>
     */
    public int getVariablesCount() {
        return this.variables.size();
    }



    /**
     * <p>
     * Returns the variable at the specified index.
     * </p>
     * <p>
     * If no variable exist at the specified index then return null
     * </p>
     * <p>
     * Note: No excpetion if thrown byt this method, if the index is aout of
     * range then null is returned.
     * </p>
     * 
     * @param idx
     *            The index of the variable to be returned
     * @return the variable at the specified index (zero based) or null if no
     *         variable exist at this index.
     */
    public Variable getVariable(int idx) {
        if (idx < 0 || idx > this.variables.size() - 1) {
            return null;
        }
        return this.variables.get(idx);
    }



    /**
     * <p>
     * Returns the first variable whose name if specified.
     * </p>
     * <p>
     * If several variables exists for the specified name then only the first
     * occurance is returned.
     * </p>
     * 
     * @param name
     *            The name of the variable to get
     * @return The name of the first appearing variable whose name is specified.
     *         Return null if no variable whith this name exist.
     */
    public Variable getVariable(String name) {
        List<Variable> vars = this.varMap.get(name);
        if (vars == null) {
            return null;
        }
        return vars.get(0);
    }



    /**
     * <p>
     * Return all the variables with the specified name.
     * </p>
     * <p>
     * The returned array holds the variables for the specified name in their
     * order of appearance.
     * </p>
     * 
     * @param name
     *            The name of the variable(s) to be returned.
     * @return An array of the <code>Variable</code> for the specified name in
     *         their order of appearance.
     */
    public Variable[] getVariables(String name) {
        List<Variable> vars = this.varMap.get(name);
        if (vars == null) {
            return EMPTY_VAR_ARRAY;
        }
        return vars.toArray(new Variable[vars.size()]);
    }



    /**
     * <p>
     * Get variables values as an array of objects.
     * </p>
     * <p>
     * The returned array holds variables values (and values only) between
     * specified indexes.
     * </p>
     * 
     * @param fromIndex
     *            The index of the first (inclusive) value to be returned
     * @param toIndex
     *            The index of the last (exclusive) value. When negative assumed
     *            to be <code>variableCount()</code>, i.e. all variables at
     *            indexes following fromIndex.
     * @return An array that contains String, BigDecimal and Date instances.
     *         Null reference may exist for null valued variables.
     * @throws AppInfrastructureException
     *             If the specified indexes are out of range. Valid range is
     *             <code>[0-getVariablesCount()]</code>
     */
    public Object[] getValues(int fromIndex, int toIndex) {
        if (fromIndex >= 0 && fromIndex <= this.variables.size()) {
            Object[] out = null;
            List<Variable> vars = null;
            if (toIndex < 0) {
                vars = this.variables.subList(fromIndex, this.variables.size());
            } else {
                vars = this.variables.subList(fromIndex, this.variables.size());
            }
            out = new Object[vars.size()];
            int k = 0;
            for (Variable var : vars) {
                out[k++] = var.getValue();
            }
            return out;
        } else {
            throw new AppInfrastructureException("fromIndex out of bounds %s not within 0-%s", fromIndex, this.variables.size()); //$NON-NLS-1$
        }
    }



    /**
     * <p>
     * Return all the variables values (and values only) from (inclusive) and
     * after the specified index
     * </p>
     * <p>
     * All indexes previous to the specified index are ignored.
     * </p>
     * 
     * @param fromIndex
     *            The index of the first value to be returned
     * @return An array of objects that contains String, BigDecimal and Date
     *         instances. Null references may exist if some variables have null
     *         values
     * @throws AppInfrastructureException
     *             If the specified index is out of range. Valid range is
     *             <code>[0-getVariablesCount()]</code>
     */
    public Object[] getValues(int fromIndex) {
        return getValues(fromIndex, -1);
    }



    /**
     * <p>
     * Return the value of the first variable whose name is specified.
     * </p>
     * <p>
     * Note that a variable with a name may appear sevral times, in this case
     * this is the first occurence that will be returned.
     * </p>
     * 
     * @param varName
     *            The name of the variable whose value is to be returned
     * @return The value of the first variable whose name is specified, null if
     *         no variable with this name exist or if the variable's value is
     *         actually null.
     */
    public Object getValue(String varName) {
        Variable v = getVariable(varName);
        return v == null ? null : v.getValue();
    }



    /**
     * <p>
     * Return all variables <b><u>values</u></b> indexed by their names.
     * </p>
     * <p>
     * If several variables share the same name then the value corresponding to
     * the name will be a list.
     * </p>
     * 
     * @return A Map where keys are variable names and values are variables
     *         values (or list of values when sevral variables share the same
     *         name)
     */
    public Map<String, Object> getValueMap() {
        Map<String, Object> out = new HashMap<String, Object>();
        for (Variable v : this.variables) {
            List<Variable> vars = this.varMap.get(v.getName());
            if (vars.size() > 1) {
                if (out.get(v.getName()) != null) {
                    // variable name already added
                    continue;
                }
                List<Object> values = new ArrayList<Object>();
                for (Variable v1 : vars) {
                    values.add(v1.getValue());
                }
                out.put(v.getName(), values);
            } else {
                out.put(v.getName(), v.getValue());
            }
        }
        return out;
    }



    /**
     * <p>
     * Return variables indexed by their names.
     * </p>
     * <p>
     * When several variables share the name, the map will contain a list of
     * {@link Variable} for that name.
     * </p>
     * 
     * @return A map that index {@link Variable}s by their names
     */
    public Map<String, Object> getVariableMap() {
        return new HashMap<String, Object>(this.varMap);
    }



    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        String[] varNames = getVariableNames();
        for (String vName : varNames) {
            out.append(vName).append("=["); //$NON-NLS-1$
            Variable[] vars = getVariables(vName);
            for (int k = 0; k < vars.length; k++) {
                Variable var = vars[k];
                out.append(var);
                if (k < vars.length - 1) {
                    out.append("; "); //$NON-NLS-1$
                }
            }
            out.append("]\n"); //$NON-NLS-1$
        }
        return out.toString();
    }
}
