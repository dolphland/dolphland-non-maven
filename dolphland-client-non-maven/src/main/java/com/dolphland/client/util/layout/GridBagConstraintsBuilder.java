package com.dolphland.client.util.layout;

import java.awt.GridBagConstraints;
import java.awt.Insets;

/**
 * Cette classe repr�sente un Builder permettant de construire un
 * <code>GridBagConstraints</code>.
 * La classe <code>GridBagConstraints</code> sp�cifie les contraintes pour un
 * composant utilisant comme layout
 * la classe <code>GridBagLayout</code>.
 * 
 * @author jeremy.scafi
 */
public class GridBagConstraintsBuilder {

    GridBagConstraints gc;



    /**
     * Constructeur sans param�tre
     */
    public GridBagConstraintsBuilder() {
        gc = new GridBagConstraints();
    }



    /**
     * Constructeur avec un mod�le de <code>GridBagConstraints</code> en
     * param�tre.
     * 
     * @param gcModele
     *            Mod�le de <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder(GridBagConstraints gcModele) {
        gc = new GridBagConstraints(
            gcModele.gridx,
            gcModele.gridy,
            gcModele.gridwidth,
            gcModele.gridheight,
            gcModele.weightx,
            gcModele.weighty,
            gcModele.anchor,
            gcModele.fill,
            gcModele.insets,
            gcModele.ipadx,
            gcModele.ipady);
    }



    /**
     * Cette m�thode fixe le champ gridx du <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie la position horizontale de la zone d'affichage du
     * composant, o� la premi�re
     * cellule dans une ligne est <code>gridx=0</code>.
     * La valeur <code>RELATIVE</code> sp�cifie que le composant est plac�
     * imm�diatement apr�s le composant
     * qui a �t� ajout� au conteneur juste avant celui ci.
     * <p>
     * La valeur par d�faut est <code>RELATIVE</code>. <code>gridx</code> ne
     * doit pas �tre n�gatif.
     * 
     * @param x
     *            Valeur du champ gridx du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder x(int x) {
        gc.gridx = x;
        return this;
    }



    /**
     * Cette m�thode fixe le champ gridy du <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie la position verticale de la zone d'affichage du
     * composant, o� la premi�re
     * cellule dans une colonne est <code>gridy=0</code>.
     * La valeur <code>RELATIVE</code> sp�cifie que le composant est plac�
     * imm�diatement apr�s le composant
     * qui a �t� ajout� au conteneur juste avant celui ci.
     * <p>
     * La valeur par d�faut est <code>RELATIVE</code>. <code>gridy</code> ne
     * doit pas �tre n�gatif.
     * 
     * @param y
     *            Valeur du champ gridy du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder y(int y) {
        gc.gridy = y;
        return this;
    }



    /**
     * Cette m�thode fixe le champ gridwidth du <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie le nombre de cellules contenues dans une ligne
     * devant composer la zone d'affichage du composant.
     * <p>
     * Utiliser <code>REMAINDER</code> permet de sp�cifier que la zone
     * d'affichage du composant doit �tre entre <code>gridx</code> et la
     * derni�re cellule de la ligne. Utiliser <code>RELATIVE</code> permet de
     * sp�cifier que la zone d'affichage du composant doit �tre entre
     * <code>gridx</code> et la prochaine zone d'affichage de cette ligne.
     * <p>
     * <code>gridwidth</code> ne doit pas �tre n�gatif et sa valeur par d�faut
     * est 1.
     * 
     * @param width
     *            Valeur du champ gridwidth du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder width(int width) {
        gc.gridwidth = width;
        return this;
    }



    /**
     * Cette m�thode fixe le champ gridheight du <code>GridBagConstraints</code>
     * . <br>
     * Ce champ sp�cifie le nombre de cellules contenues dans une colonne
     * devant composer la zone d'affichage du composant.
     * <p>
     * Utiliser <code>REMAINDER</code> permet de sp�cifier que la zone
     * d'affichage du composant doit �tre entre <code>gridy</code> et la
     * derni�re cellule de la colonne. Utiliser <code>RELATIVE</code> permet de
     * sp�cifier que la zone d'affichage du composant doit �tre entre
     * <code>gridy</code> et la prochaine zone d'affichage de cette colonne.
     * <p>
     * <code>gridheight</code> ne doit pas �tre n�gatif et sa valeur par d�faut
     * est 1.
     * 
     * @param height
     *            Valeur du champ gridheight du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder height(int height) {
        gc.gridheight = height;
        return this;
    }



    /**
     * Cette m�thode fixe la valeur left du champ insets du
     * <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie l'espacement externe du composant, ce qui repr�sente
     * l'espace entre le composant
     * et les bordures de sa zone d'affichage.
     * <p>
     * La valeur par d�faut est <code>new Insets(0, 0, 0, 0)</code>.
     * 
     * @param leftInsets
     *            Valeur left du champ insets du <code>GridBagConstraints</code>
     *            .
     */
    public GridBagConstraintsBuilder leftInsets(int leftInsets) {
        gc.insets = new Insets(gc.insets != null ? gc.insets.top : 0, leftInsets, gc.insets != null ? gc.insets.bottom : 0, gc.insets != null ? gc.insets.right : 0);
        return this;
    }



    /**
     * Cette m�thode fixe la valeur right du champ insets du
     * <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie l'espacement externe du composant, ce qui repr�sente
     * l'espace entre le composant
     * et les bordures de sa zone d'affichage.
     * <p>
     * La valeur par d�faut est <code>new Insets(0, 0, 0, 0)</code>.
     * 
     * @param rightInsets
     *            Valeur right du champ insets du
     *            <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder rightInsets(int rightInsets) {
        gc.insets = new Insets(gc.insets != null ? gc.insets.top : 0, gc.insets != null ? gc.insets.left : 0, gc.insets != null ? gc.insets.bottom : 0, rightInsets);
        return this;
    }



    /**
     * Cette m�thode fixe la valeur top du champ insets du
     * <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie l'espacement externe du composant, ce qui repr�sente
     * l'espace entre le composant
     * et les bordures de sa zone d'affichage.
     * <p>
     * La valeur par d�faut est <code>new Insets(0, 0, 0, 0)</code>.
     * 
     * @param topInsets
     *            Valeur top du champ insets du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder topInsets(int topInsets) {
        gc.insets = new Insets(topInsets, gc.insets != null ? gc.insets.left : 0, gc.insets != null ? gc.insets.bottom : 0, gc.insets != null ? gc.insets.right : 0);
        return this;
    }



    /**
     * Cette m�thode fixe la valeur bottom du champ insets du
     * <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie l'espacement externe du composant, ce qui repr�sente
     * l'espace entre le composant
     * et les bordures de sa zone d'affichage.
     * <p>
     * La valeur par d�faut est <code>new Insets(0, 0, 0, 0)</code>.
     * 
     * @param bottomInsets
     *            Valeur bottom du champ insets du
     *            <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder bottomInsets(int bottomInsets) {
        gc.insets = new Insets(gc.insets != null ? gc.insets.top : 0, gc.insets != null ? gc.insets.left : 0, bottomInsets, gc.insets != null ? gc.insets.right : 0);
        return this;
    }



    /**
     * Cette m�thode fixe le champ weightx du <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie comment distribuer l'espace horizontal restant.
     * <p>
     * Le manager du layout calcule le poids de la colonne pour �tre au maximum
     * <code>weightx</code> de tous les composants dans une colonne. Si l'espace
     * utilis� est horizontalement plus petit que la zone n�cessaire, l'espace
     * restant est distribu� � chaque colonne proportionnellement � leur poids.
     * Une colonne qui a un poids � 0 ne re�oit pas d'espace restant.
     * <p>
     * Si tous les poids sont � 0, tout l'espace restant appara�t entre les
     * grilles de cellule et les bordures gauche et droite.
     * <p>
     * La valeur par d�faut de ce champ est <code>0</code>. <code>weightx</code>
     * ne doit pas �tre n�gatif.
     * 
     * @param weightx
     *            Valeur du champ weightx du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder weightx(double weightx) {
        gc.weightx = weightx;
        return this;
    }



    /**
     * Cette m�thode fixe le champ weighty du <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie comment distribuer l'espace vertical restant.
     * <p>
     * Le manager du layout calcule le poids de la ligne pour �tre au maximum
     * <code>weighty</code> de tous les composants dans une ligne. Si l'espace
     * utilis� est verticalement plus petit que la zone n�cessaire, l'espace
     * restant est distribu� � chaque ligne proportionnellement � leur poids.
     * Une ligne qui a un poids � 0 ne re�oit pas d'espace restant.
     * <p>
     * Si tous les poids sont � 0, tout l'espace restant appara�t entre les
     * grilles de cellule et les bordures haut et bas.
     * <p>
     * La valeur par d�faut de ce champ est <code>0</code>. <code>weighty</code>
     * ne doit pas �tre n�gatif.
     * 
     * @param weighty
     *            Valeur du champ weighty du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder weighty(double weighty) {
        gc.weighty = weighty;
        return this;
    }



    /**
     * Cette m�thode fixe le champ ipadx du <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie l'espacement interne du composant, ce qui correspond �
     * l'espacement � ajouter
     * � la largeur minimale du composant. La largeur minimale du composant est
     * alors au moins �gale � sa largeur
     * minimale + <code>ipadx</code> pixels.
     * <p>
     * La valeur par d�faut est <code>0</code>.
     * 
     * @param ipadx
     *            Valeur du champ ipadx du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder ipadx(int ipadx) {
        gc.ipadx = ipadx;
        return this;
    }



    /**
     * Cette m�thode fixe le champ ipady du <code>GridBagConstraints</code>. <br>
     * Ce champ sp�cifie l'espacement interne du composant, ce qui correspond �
     * l'espacement � ajouter
     * � la hauteur minimale du composant. La hauteur minimale du composant est
     * alors au moins �gale � sa hauteur
     * minimale + <code>ipady</code> pixels.
     * <p>
     * La valeur par d�faut est <code>0</code>.
     * 
     * @param ipady
     *            Valeur du champ ipady du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder ipady(int ipady) {
        gc.ipady = ipady;
        return this;
    }



    /**
     * Cette m�thode fixe le champ anchor du <code>GridBagConstraints</code>. <br>
     * Ce champ est utilis� quand le composant est plus petit que sa zone
     * d'affichage.
     * Ceci d�termine o� placer le composant par rapport � sa zone d'affichage
     * (correspond � l'attribut anchor).
     * <p>
     * Il existe deux types de valeurs possibles : relatives et absolues. Les
     * valeurs relatives sont interpr�t�es relativement � l'orientation du
     * composant par rapport � son conteneur alors que les valeurs absolues ne
     * le sont pas. Les valeurs absolues sont:
     * <code>GridBagConstraints.CENTER</code>,
     * <code>GridBagConstraints.NORTH</code>,
     * <code>GridBagConstraints.NORTHEAST</code>,
     * <code>GridBagConstraints.EAST</code>,
     * <code>GridBagConstraints.SOUTHEAST</code>,
     * <code>GridBagConstraints.SOUTH</code>,
     * <code>GridBagConstraints.SOUTHWEST</code>,
     * <code>GridBagConstraints.WEST</code> et
     * <code>GridBagConstraints.NORTHWEST</code>. Les valeurs relatives sont:
     * <code>GridBagConstraints.PAGE_START</code>,
     * <code>GridBagConstraints.PAGE_END</code>,
     * <code>GridBagConstraints.LINE_START</code>,
     * <code>GridBagConstraints.LINE_END</code>,
     * <code>GridBagConstraints.FIRST_LINE_START</code>,
     * <code>GridBagConstraints.FIRST_LINE_END</code>,
     * <code>GridBagConstraints.LAST_LINE_START</code> et
     * <code>GridBagConstraints.LAST_LINE_END</code>. La valeur par d�faut est
     * <code>GridBagConstraints.CENTER</code>.
     * 
     * @param anchor
     *            Valeur du champ anchor du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder anchor(int anchor) {
        gc.anchor = anchor;
        return this;
    }



    /**
     * Cette m�thode fixe le champ fill du <code>GridBagConstraints</code>. <br>
     * Ce champ est utilis� quand la zone d'affichage du composant est plus
     * grande que la taille requise
     * par le composant. Ceci d�termine le redimensionnement du composant �
     * r�aliser (correspond � l'attribut fill).
     * <p>
     * Les valeurs suivantes sont valides:
     * <p>
     * <ul>
     * <li>
     * <code>GridBagConstraints.NONE</code>: Ne redimensionne pas le composant.
     * <li>
     * <code>GridBagConstraints.HORIZONTAL</code>: Redimensionne la largeur du
     * composant pour l'afficher dans toute la partie horizontale de sa zone
     * d'affichage mais ne redimensionne pas sa hauteur.
     * <li>
     * <code>GridBagConstraints.VERTICAL</code>: Redimensionne la hauteur du
     * composant pour l'afficher dans toute la partie verticale de sa zone
     * d'affichage mais ne redimensionne pas sa largeur.
     * <li>
     * <code>GridBagConstraints.BOTH</code>: Redimensionne le composant pour
     * l'afficher dans la totalit� de sa zone d'affichage.
     * </ul>
     * <p>
     * La valeur par d�faut est <code>GridBagConstraints.NONE</code>.
     * 
     * @param fill
     *            Valeur du champ fill du <code>GridBagConstraints</code>.
     */
    public GridBagConstraintsBuilder fill(int fill) {
        gc.fill = fill;
        return this;
    }



    /**
     * Renvoie le <code>GridBagConstraints</code> construit
     * 
     * @return <code>GridBagConstraints</code> construit
     */
    public GridBagConstraints get() {
        return gc;
    }
}
