package com.dolphland.client.util.table.demo;

import com.dolphland.client.main.AppFrame;
import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.startup.UIApplication;
import com.dolphland.client.util.application.startup.UIApplicationContext;


public class DemoTable extends UIApplication {

    /**
     * Default constructor
     */
    public DemoTable() {
    }



    @Override
    public MasterFrame createMasterFrame(UIApplicationContext ctx) {
        return new DemoMasterFrame();
    }



    @Override
    public void init(UIApplicationContext ctx) {
    }



    @Override
    public void startup(UIApplicationContext ctx) {
        DemoScenario scenario = new DemoScenario();
        scenario.setWorkPane(ctx.getMasterFrame().getWorkPane());
        scenario.enter();
    }
}