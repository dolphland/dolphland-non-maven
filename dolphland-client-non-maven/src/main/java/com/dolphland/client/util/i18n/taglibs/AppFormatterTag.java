package com.dolphland.client.util.i18n.taglibs;

import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.dolphland.client.util.i18n.MessageFormater;

public class AppFormatterTag extends TagSupport {

    private String bundle = null;
    private String locale = null;



    @Override
    public int doStartTag() throws JspException {
        return super.doStartTag();
    }



    @Override
    public int doEndTag() throws JspException {

        MessageFormater fmt = null;
        
        if (getLocale() == null) {
            fmt = MessageFormater.getFormater(getBundle());
        } else {
            fmt = MessageFormater.getFormater(getBundle(), new Locale(getLocale()));
        }

        pageContext.setAttribute(AppFormatterConstants.FORMATTER, fmt);

        return super.doEndTag();
    }



    /**
     * @return the bundle
     */
    public String getBundle() {
        return bundle;
    }



    /**
     * @param bundle
     *            the bundle to set
     */
    public void setBundle(String bundle) {
        this.bundle = bundle;
    }



    /**
     * @return the locale
     */
    public String getLocale() {
        return locale;
    }



    /**
     * @param locale
     *            the locale to set
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

}
