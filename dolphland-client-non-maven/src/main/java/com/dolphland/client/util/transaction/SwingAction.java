package com.dolphland.client.util.transaction;

/**
 * Repr�sente une action a ex�cuter dans un context Swing.<br>
 * <br>
 * Un {@link SwingAction} est destin� � �tre ex�cut� par un
 * {@link SwingActionExecutor}.<br>
 * Le processus d'invocation d'un {@link SwingAction} est le suivant :<br>
 * <ul>
 * <li>Invocation de <b>shouldExecute()</b> <u>par le thread Swing</u> : si
 * <b>shouldExecute()</b> retourne false l'action s'arr�te, les �tapes suivantes
 * ne sont pas effectu�es</li>
 * <li>Invocation de <b>preExecute()</b> <u>par le thread Swing</u></li>
 * <li>Invocation de <b>doExecute()</b> <u>par le thread du
 * {@link SwingActionExecutor}</u></li>
 * <ul>
 * <li>Si doExecute() n'a pas lev� d'exception : invocation de
 * <b>updateViewSuccess()</b> <u>par le thread Swing</u></li>
 * <li>Si doExecute() a lev� une exception : invocation de
 * <b>updateViewError()</b><u>par le thread Swing</u></li>
 * </ul>
 * <li>Invocation de <b>postExecute()</b> <u>par le thread Swing</u></li> </ul>
 * 
 * @param <P>
 *            Le type du param�tre que prend l'action
 * @param <T>
 *            Le type de donn�e produite par doExecute()
 * 
 * @author JayJay
 */
public abstract class SwingAction<P, T> {

    /**
     * Service appell� juste avant preExecute(). Ce service retourne false si
     * l'ex�cution de la transaction doit �tre annul�e. Si la transaction doit
     * continuer � s'ex�cuter normalement, ce service doit retourner true.<br>
     * Si ce service retourne false, les services preExecute(), doExecute(),
     * updateViewSuccess(), updateViewError() et postExecute() ne seront pas
     * appel�s.
     * 
     * @param param
     *            Le param�tre transmis � execute()
     * @return true si l'action doit s'ex�cuter, false sinon.
     */
    protected abstract boolean shouldExecute(P param);



    /**
     * Service appell� juste avant doExecute() dans le fil du thread AWT
     */
    protected abstract void preExecute(P param);



    /**
     * Service appell� juste apr�s updateViewLater() updateViewSuccess() et
     * updateViewError() dans le fil du thread AWT.
     */
    protected abstract void postExecute();



    /**
     * M�thode d'ex�cution de la transaction, contient les traitements qui
     * d�marrent la transaction.
     * 
     * @param param
     *            Param�tre � transmettre � doExecute()
     */
    protected abstract T doExecute(P param);



    /**
     * M�thode appell�e dans le cas o� la transaction s'est d�roul�e sans
     * erreur.
     * 
     * @param data
     *            Objet retourn� par doExecute()
     */
    protected abstract void updateViewSuccess(T data);



    /**
     * Methode appell�e dans le cas o� une exception a �t� lev�e par
     * doExecute(). L'exception lev�e est pass�e en param�tre.
     * 
     * @param ex
     *            Exception lev�e par doExecute()
     */
    protected abstract void updateViewError(Exception ex);

}
