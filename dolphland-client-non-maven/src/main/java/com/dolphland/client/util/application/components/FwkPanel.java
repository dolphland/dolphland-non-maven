package com.dolphland.client.util.application.components;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.layout.GridBagConstraintsBuilder;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;
import com.dolphland.client.util.string.StrUtil;

/**
 * Classe d�finissant un panel Vision pouvant avoir un titre
 * 
 * Il est possible de d�finir :
 * - le type de bordure (DEFAULT_TYPE_BORDER pour utiliser la bordure par d�faut)
 * - la font du titre (DEFAULT_TITLE_FONT pour utiliser la valeur par d�faut en fonction du type de bordure)
 * - la couleur du titre (DEFAULT_TITLE_COLOR pour utiliser la valeur par d�faut en fonction du type de bordure)
 * - la position du titre via TitledBorder (DEFAULT_TITLE_POSITION pour utiliser la valeur par d�faut en fonction du type de bordure)
 * - la justification du titre via TitledBorder (DEFAULT_TITLE_JUSTIFICATION pour utiliser la valeur par d�faut en fonction du type de bordure)
 * - la politique d'�criture du titre (DEFAULT_WRITING_POLICY pour utiliser la valeur par d�faut en fonction du type de bordure)
 * - l'image � afficher dans le panel 
 * - la posibilit� de s�lectionner le panel
 * 
 * @author jeremy.scafi
 */
public class FwkPanel extends JPanel implements Scrollable{

    /** Default background color */
    private final static Color DEFAULT_BACKGROUND_COLOR = Color.BLACK;

    /** Default gap value that should separate components (in pexels) */
    protected static final int DEFAULT_GAP = 10;
    
	/** Liste des types de bordures pr�d�finies pour le panel */
	public final static int TYPE_NO_BORDER = 0;
	public final static int TYPE_BORDER_LINE_BORDER = 1;
	public final static int TYPE_BORDER_STYLED_BORDER = 2;
	public final static int TYPE_BORDER_BOLD_LEFT = 3;
	public final static int TYPE_BORDER_ROUNDED_BORDER = 4;
		
	/** Liste des politiques d'�criture du titre du panel */
	public final static int WRITING_NO_POLICY = 0;
	public final static int WRITING_POLICY_LOWER_CASE = 1;
	public final static int WRITING_POLICY_UPPER_CASE = 2;
	public final static int WRITING_POLICY_LOWER_CASE_WITH_SPACES = 3;
	public final static int WRITING_POLICY_UPPER_CASE_WITH_SPACES = 4;
	
	/** Constantes fixant les valeurs par d�faut en fonction du type de bordure */
	public final static Font DEFAULT_TITLE_FONT = null;
	public final static Color DEFAULT_TITLE_COLOR = null;
	public final static int DEFAULT_TITLE_POSITION = -1;
	public final static int DEFAULT_TITLE_JUSTIFICATION = -1;
	public final static int DEFAULT_WRITING_POLICY = -1;
	public final static int DEFAULT_TYPE_BORDER = TYPE_NO_BORDER;

	/** Constantes fixant les rotations possibles pour les images */
	public final static int IMAGE_ROTATION_0_DEGRE = 0;
	public final static int IMAGE_ROTATION_90_DEGRE = 1;
    public final static int IMAGE_ROTATION_180_DEGRE = 2;
	public final static int IMAGE_ROTATION_270_DEGRE = 3;
	
	// Variables
	private String title;
	private Font titleFont;
	private Color titleColor;
	private int titlePosition;
	private int titleJustification;
	private int titleWritingPolicy;
	private int typeBorder;
	
	// Valeurs par d�faut en fonction du type de bordure
	private Font defaultTitleFont;
	private Color defaultTitleColor;
	private int defaultTitlePosition;
	private int defaultTitleJustification;
	private int defaultTitleWritingPolicy;
	
	// Variables utilis�es pour rendre le panel arrondi
	private int strokeSize = 1;
    private Color shadowColor = Color.black;
    private boolean shady = true;
    private boolean highQuality = true;
    private Dimension arcs = new Dimension(20, 20);
    private int shadowGap = 5;
    private int shadowOffset = 4;
    private int shadowAlpha = 150;

    // Variables utilis�es pour afficher une image
	private BufferedImage image;
	private int imageLeftInset;
	private int imageRightInset;
	private int imageTopInset;
	private int imageBottomInset;
	private boolean mosaicEnabled;
	
	// Variables utilis�es pour la s�lection
	private boolean selected;
	private boolean selectedEnabled;
	private Color selectedForeground = null;
	private Color selectedBackground = null;
	
	// Variables de sauvegarde de valeurs  
	private boolean safeOpaque = isOpaque();
	private Color safeBackground = getBackground();
	private Color safeForeground = getForeground();
	
	
	/**
	 * Constructeur sans param�tre
	 */
	public FwkPanel(){
		this(null);
	}

	/**
	 * Constructeur avec le titre du panel en param�tre
	 */
	public FwkPanel(String title){
		super();
		setBackground(DEFAULT_BACKGROUND_COLOR);
		setTitle(title);
		setTitlePosition(DEFAULT_TITLE_POSITION);
		setTitleJustification(DEFAULT_TITLE_JUSTIFICATION);
		setTitleWritingPolicy(DEFAULT_WRITING_POLICY);
		setTypeBorder(DEFAULT_TYPE_BORDER);
	}
	
	/**
	 * R�d�finition de la m�thode getPreferredSize() pour prendre en compte dans la largeur pr�f�r�e du panel la largeur pr�f�r�e de la bordure titre d'un panel
	 */
	@Override
	public Dimension getPreferredSize() {
		if(image != null){
		    int width = image.getWidth()+imageLeftInset+imageRightInset;
		    int height = image.getHeight()+imageTopInset+imageBottomInset;
			return new Dimension(width, height);
		}else{
	        Dimension size = super.getPreferredSize();
			int width = size.width;
			int height = size.height;
			Border border = getBorder();
			if(border != null && !StrUtil.isEmpty(getTitle()) && border instanceof TitledBorder){
				TitledBorder titledBorder = ((TitledBorder)border);
				width = Math.max(width, titledBorder.getMinimumSize(this).width);
			}
			return new Dimension(width, height);
		}
	}
	
	/**
	 * Cr�ation d'un Builder de <code>GridBagConstraints</code>.
	 */
	protected GridBagConstraintsBuilder createGridBagConstraintsBuilder(){
		return new GridBagConstraintsBuilder();
	}
	
	/**
	 * Cr�ation d'un Builder de <code>TableLayout</code>.
	 */
	protected TableLayoutBuilder createTableLayoutBuilder(){
		return new TableLayoutBuilder();
	}
	
	/**
	 * Cr�ation d'un Builder de <code>TableLayoutConstraints</code>.
	 */
	protected TableLayoutConstraintsBuilder createTableLayoutConstraintsBuilder(){
		return new TableLayoutConstraintsBuilder();
	}
	
	/**
	 * Fixe le type de bordure du panel 
	 */
	public void setTypeBorder(int typeBorder){
		this.typeBorder = typeBorder;
		
		updateBorder();
	}
	
	/**
	 * Fixe le type de bordure du panel 
	 */
	private void updateBorder(){
		
		if(selected){
			super.setBackground(selectedBackground!=null?selectedBackground:safeBackground);
			super.setForeground(selectedForeground!=null?selectedForeground:safeForeground);
		}else{
			super.setBackground(safeBackground);
			super.setForeground(safeForeground);
		}

		// Fixe les valeurs par d�faut en fonction du type de bordure
		defaultTitleFont = new Font("Dialog", Font.BOLD, 18);
		defaultTitleColor = getForeground();
		defaultTitleJustification = TitledBorder.CENTER;
		defaultTitleWritingPolicy = WRITING_POLICY_UPPER_CASE_WITH_SPACES;

		switch(typeBorder) {
			case TYPE_BORDER_STYLED_BORDER:
			case TYPE_BORDER_BOLD_LEFT:
				defaultTitlePosition = TitledBorder.BELOW_TOP;
				break;

			default:
				defaultTitlePosition = TitledBorder.TOP;
				break;
		}
		
		// Fixe la bordure du titre, l'opacit� et les marges image en fonction du type de bordure
		TitledBorder titledBorder = new TitledBorder("");
		switch(getTypeBorder()) {
			case TYPE_NO_BORDER:
				super.setOpaque(safeOpaque);
				titledBorder = null;
				if(!StrUtil.isEmpty(getTitle())){
					titledBorder = new TitledBorder(new LineBorder(getForeground(), 0), getTitle());
				}
				imageLeftInset = 0;
				imageRightInset = 0;
				imageTopInset = 0;
				imageBottomInset = 0;
				break;

			case TYPE_BORDER_LINE_BORDER:
				super.setOpaque(safeOpaque);
				titledBorder = new TitledBorder(new LineBorder(getForeground(), 1), getTitle());
				imageLeftInset = 1;
				imageRightInset = 1;
				imageTopInset = 1;
				imageBottomInset = 1;
				break;

			case TYPE_BORDER_STYLED_BORDER:
				super.setOpaque(safeOpaque);
				StyledLineBorder styledBorder = new StyledLineBorder(getForeground(), 2);
				if(StrUtil.isEmpty(getTitle())){
					styledBorder.setOffsetX1(0);
					styledBorder.setOffsetY1(0);
					styledBorder.setOffsetX2(0);
					styledBorder.setOffsetY2(0);
					titledBorder = new TitledBorder(styledBorder);
					imageLeftInset = styledBorder.getThickness()+3;
					imageRightInset = styledBorder.getThickness()+3;
					imageTopInset = styledBorder.getThickness()+3;
					imageBottomInset = styledBorder.getThickness()+3;
				} else {					
					styledBorder.setOffsetX1(-2);
					styledBorder.setOffsetY1(-2);
					styledBorder.setOffsetX2(-4);
					styledBorder.setOffsetY2(-4);
					imageLeftInset = 5;
					imageRightInset = 5;
					imageTopInset = 5;
					imageBottomInset = 5;
				}
				titledBorder = new TitledBorder(styledBorder);
				break;

			case TYPE_BORDER_BOLD_LEFT:
				super.setOpaque(safeOpaque);
				titledBorder = new TitledBorder(BorderFactory.createMatteBorder(1, 5, 1, 1, getForeground()));
				imageLeftInset = 2;
				imageRightInset = 0;
				imageTopInset = 0;
				imageBottomInset = 0;
				break;
				
			case TYPE_BORDER_ROUNDED_BORDER:
				super.setOpaque(false);
				titledBorder = null;
				if(!StrUtil.isEmpty(getTitle())){
					titledBorder = new TitledBorder(new LineBorder(getForeground(), 0), getTitle());
				}				imageLeftInset = shadowOffset;
				imageRightInset = shadowGap + shadowOffset;
				imageTopInset = shadowOffset;
				imageBottomInset = shadowGap + shadowOffset;
				break;
				
			default:
				super.setOpaque(safeOpaque);
				titledBorder = null;
				imageLeftInset = 0;
				imageRightInset = 0;
				imageTopInset = 0;
				imageBottomInset = 0;
				break;
		}
		
		if(titledBorder != null){
			// Fixe le titre en fonction de la politique d'�criture
			if(StrUtil.isEmpty(title)){
				titledBorder.setTitle(null);
			} else {
				String titleStyled = "";
				switch(getTitleWritingPolicy()) {
					case WRITING_POLICY_UPPER_CASE_WITH_SPACES:
						for(int i = 0 ; title != null && i < title.length() ; i++) {
							titleStyled += " " + title.substring(i, i+1).toUpperCase();
						}
						break;

					case WRITING_POLICY_LOWER_CASE_WITH_SPACES:
						for(int i = 0 ; title != null && i < title.length() ; i++) {
							titleStyled += " " + title.substring(i, i+1).toLowerCase();
						}
						break;

					case WRITING_POLICY_UPPER_CASE:
						for(int i = 0 ; title != null && i < title.length() ; i++) {
							titleStyled += title.substring(i, i+1).toUpperCase();
						}
						break;

					case WRITING_POLICY_LOWER_CASE:
						for(int i = 0 ; title != null && i < title.length() ; i++) {
							titleStyled += title.substring(i, i+1).toLowerCase();
						}
						break;

					default:
						titleStyled = title;
						break;
				}
				titledBorder.setTitle(titleStyled);
			}
			
			// Fixe la font du titre
			titledBorder.setTitleFont(getTitleFont());

			// Fixe la couleur du titre
			titledBorder.setTitleColor(getTitleColor());

			// Fixe la position du titre
			titledBorder.setTitlePosition(getTitlePosition());

			// Fixe la justification du titre
			titledBorder.setTitleJustification(getTitleJustification());
		}

		// Fixe la bordure
		setBorder(titledBorder);
	}
	
	/** 
	 * Fixe le titre du Panel 
	 */
	public void setTitle(String title){
		this.title = title;

		updateBorder();
	}
	
	/** 
	 * Fixe la font du titre du Panel 
	 */
	public void setTitleFont(Font titleFont){	
		this.titleFont = titleFont;

		updateBorder();
	}
	
	/** 
	 * Fixe la couleur du titre du Panel 
	 */
	public void setTitleColor(Color titleColor){	
		this.titleColor = titleColor;

		updateBorder();
	}
	
	/** 
	 * Fixe la position du titre du Panel 
	 */
	public void setTitlePosition(int titlePosition){	
		this.titlePosition = titlePosition;

		updateBorder();
	}
	
	/** 
	 * Fixe la justification du titre du Panel 
	 */
	public void setTitleJustification(int titleJustification){	
		this.titleJustification = titleJustification;

		updateBorder();
	}
	
	/** 
	 * Fixe la politique d'�criture du titre du Panel 
	 */
	public void setTitleWritingPolicy(int titleWritingPolicy){	
		this.titleWritingPolicy = titleWritingPolicy;

		updateBorder();
	}
	
	/**
	 * Renvoie le titre du panel
	 */
	public String getTitle(){
		return title;
	}	
	
	/**
	 * Renvoie le type de bordure du panel 
	 */
	public int getTypeBorder(){
		return typeBorder;
	}
	
	/** 
	 * Renvoie la font du titre du Panel 
	 */
	public Font getTitleFont(){	
		return titleFont!=DEFAULT_TITLE_FONT?titleFont:defaultTitleFont;
	}
	
	/** 
	 * Renvoie la couleur du titre du Panel 
	 */
	public Color getTitleColor(){	
		return titleColor!=DEFAULT_TITLE_COLOR?titleColor:defaultTitleColor;
	}
	
	/** 
	 * Renvoie la position du titre du Panel 
	 */
	public int getTitlePosition(){	
		return titlePosition!=DEFAULT_TITLE_POSITION?titlePosition:defaultTitlePosition;
	}
	
	/** 
	 * Renvoie la justification du titre du Panel 
	 */
	public int getTitleJustification(){	
		return titleJustification!=DEFAULT_TITLE_JUSTIFICATION?titleJustification:defaultTitleJustification;
	}
	
	/** 
	 * Renvoie la politique d'�criture du titre du Panel 
	 */
	public int getTitleWritingPolicy(){	
		return titleWritingPolicy!=DEFAULT_WRITING_POLICY?titleWritingPolicy:defaultTitleWritingPolicy;
	}
	
	/** M�thodes pour le scrolling */
	public Dimension getPreferredScrollableViewportSize() {
		return getPreferredSize();
	}

	public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 10;
	}

	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	public boolean getScrollableTracksViewportWidth() {
		return false;
	}

	public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
		return 10;
	}
	
    public BufferedImage getImage() {
		return image;
	}

    public void setImage(String resourcePath){
    	setImage(resourcePath==null?null:ImageUtil.getImage(resourcePath));
    }

    public void setImage(String resourcePath, int rotation){
        setImage(resourcePath==null?null:ImageUtil.getImage(resourcePath), rotation);
    }

    public void setImage(String resourcePath, boolean mosaicEnabled){
    	setImage(resourcePath==null?null:ImageUtil.getImage(resourcePath), mosaicEnabled);
    }

    public void setImage(String resourcePath, boolean mosaicEnabled, int rotation){
        setImage(resourcePath==null?null:ImageUtil.getImage(resourcePath), mosaicEnabled, rotation);
    }

    public void setImage(Icon icon) {
        setImage(icon, false);
	}

    public void setImage(Icon icon, int rotation) {
        setImage(icon, false, rotation);
    }

    public void setImage(Icon icon, boolean mosaicEnabled) {
        BufferedImage image = new BufferedImage( icon.getIconWidth() , icon.getIconHeight() , BufferedImage.TYPE_INT_RGB );
        icon.paintIcon(null, image.getGraphics() , 0 , 0 );
        setImage(image, mosaicEnabled);
	}

    public void setImage(Icon icon, boolean mosaicEnabled, int rotation) {
        BufferedImage image = new BufferedImage( icon.getIconWidth() , icon.getIconHeight() , BufferedImage.TYPE_INT_RGB );
        icon.paintIcon(null, image.getGraphics() , 0 , 0 );
        setImage(image, mosaicEnabled, rotation);
    }

    public void setImage(BufferedImage image) {
        setImage(image, false);
    }

    public void setImage(BufferedImage image, int rotation) {
        setImage(image, false, rotation);
    }

    public void setImage(BufferedImage image, boolean mosaicEnabled) {
        setImage(image, mosaicEnabled, 0);
    }
    
    public void setImage(BufferedImage image, boolean mosaicEnabled, int rotation) {
        switch (rotation) {
            case IMAGE_ROTATION_90_DEGRE :
                this.image = ImageUtil.rotateImage(image, 90, this);
                break;

            case IMAGE_ROTATION_180_DEGRE :
                this.image = ImageUtil.rotateImage(image, 180, this);
                break;

            case IMAGE_ROTATION_270_DEGRE :
                this.image = ImageUtil.rotateImage(image, 270, this);
                break;

            default :
                this.image = image;
                break;
        }
        this.mosaicEnabled = mosaicEnabled;
        
        updateBorder();
    }
    
    public void rotateImage(int rotation){
        this.image = ImageUtil.rotateImage(image, rotation, this);
        updateBorder();
    }

    @Override
    public void setOpaque(boolean isOpaque) {
    	super.setOpaque(isOpaque);
    	this.safeOpaque = isOpaque;
    }
    
    @Override
    public void setBackground(Color bg) {
    	super.setBackground(bg);
    	this.safeBackground = bg;
    }
    
    @Override
    public void setForeground(Color fg) {
    	super.setForeground(fg);
    	this.safeForeground = fg;
    }
    
    public Color getSelectedBackground() {
		return selectedBackground;
	}
    
    public void setSelectedBackground(Color selectedBackground) {
		this.selectedBackground = selectedBackground;
	}
    
    public Color getSelectedForeground() {
		return selectedForeground;
	}
    
    public void setSelectedForeground(Color selectedForeground) {
		this.selectedForeground = selectedForeground;
	}
    
    /**
     * Fixe la s�lection du panel
     * @param selected
     */
    public void setSelected(boolean selected) {
    	if(isSelectedEnabled()){
    		this.selected = selected;
    		updateBorder();
    	}
	}
    
    /**
     * Indique si le panel est s�lectionn�
     * @return
     */
    public boolean isSelected() {
		return selected;
	}
    
    /**
     * Permet de d�finir le panel peut �tre s�lectionn�
     * @param selectedEnabled
     */
    public void setSelectedEnabled(boolean selectedEnabled) {
		this.selectedEnabled = selectedEnabled;
		if(!selectedEnabled){
			this.selected = false;
		}
	}
    
    /**
     * Indique si le panel peut �tre s�lectionn�
     * @return
     */
    public boolean isSelectedEnabled() {
		return selectedEnabled;
	}
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if(getTypeBorder() == TYPE_BORDER_ROUNDED_BORDER){
            int width = getWidth();
            int height = getHeight();
            int shadowGap = this.shadowGap;
            Color shadowColorA = new Color(
            		shadowColor.getRed(), 
            		shadowColor.getGreen(), 
            		shadowColor.getBlue(), 
            		shadowAlpha);
            Graphics2D graphics = (Graphics2D) g;

            //Sets antialiasing if HQ.
            if (highQuality) {
                graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
    			RenderingHints.VALUE_ANTIALIAS_ON);
            }

            //Draws shadow borders if any.
            if (shady) {
                graphics.setColor(shadowColorA);
                graphics.fillRoundRect(
                        shadowOffset,// X position
                        shadowOffset,// Y position
                        width - strokeSize - shadowOffset, // width
                        height - strokeSize - shadowOffset, // height
                        arcs.width, arcs.height);// arc Dimension
            } else {
                shadowGap = 1;
            }

            //Draws the rounded opaque panel with borders.
            graphics.setColor(getBackground());
            graphics.fillRoundRect(0, 0, width - shadowGap, 
    		height - shadowGap, arcs.width, arcs.height);
            graphics.setColor(getForeground());
            graphics.setStroke(new BasicStroke(strokeSize));
            graphics.drawRoundRect(0, 0, width - shadowGap, 
    		height - shadowGap, arcs.width, arcs.height);

            //Sets strokes to default, is better.
            graphics.setStroke(new BasicStroke());
        }
        
        if(image != null){
        	Graphics2D g2 = (Graphics2D)g;
    		int w = getWidth()-imageLeftInset-imageRightInset;
    		int h = getHeight()-imageTopInset-imageBottomInset;
    		
    		if(mosaicEnabled){
        		int wI = image.getWidth();
        		int hI = image.getHeight();
        		for(int x = 0 ; (wI * x) < w ; x++){
        			for(int y = 0 ; (hI * y) < h ; y++){
        				g2.drawImage(image, imageLeftInset + (wI * x), imageTopInset + (hI * y), this);
        			}
        		}
    		}else{
    		    int imageLeftPosition = imageLeftInset;
    		    int imageTopPosition = imageTopInset;
    		    int imageWidth = image.getWidth();
                int imageHeight = image.getHeight();
                double ratioWidth = (double) (imageWidth) / ((double) w);
                double ratioHeight = (double) (imageHeight) / ((double) h);
                double ratioWidthHeight = (double) (imageWidth) / ((double) imageHeight);
                if(ratioWidth < ratioHeight){
                    imageWidth = (int)((double) h * ratioWidthHeight);
                    imageHeight = h;
                    imageLeftPosition += (int)((double)(w - imageWidth) / 2.0d);
                }else{
                    imageWidth = w;
                    imageHeight = (int)((double) w / ratioWidthHeight);
                    imageTopPosition += (int)((double)(h - imageHeight) / 2.0d);
                }
                g2.drawImage(image, imageLeftPosition, imageTopPosition, imageWidth, imageHeight, this);
    		}
        }
    }
}
