package com.dolphland.client.util.action;

import java.awt.Component;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

/**
 * <p>
 * Binds a key stroke to an action.
 * </p>
 * <p>
 * Only one action is allowed per modifiers/key code combination
 * </p>
 * <p>
 * Examples:
 * </p>
 * 
 * <pre>
 * // When Enter key is pressed anywhere in the this component, the OK button is
 * // clicked, triggering the validation of the window
 * ComponentKeyMapper.bindKey(this, KeyEvent.VK_ENTER, getOkButton());
 * 
 * // When F1 key is pressed anywhere in the window, the help action is triggered
 * ComponentKeyMapper.bindKey(this, KeyEvent.VK_F1, new Runnable() {
 * 
 *     public void run() {
 *         System.out.println(&quot;HELP REQUESTED!&quot;);
 *     }
 * });
 * 
 * // When Alt+Shift+F1 key is pressed anywhere in the window, the custom action is
 * // triggered
 * ComponentKeyMapper.bindKey(this, KeyEvent.ALT_MASK | KeyEvent.SHIFT_MASK, KeyEvent.VK_F1, new Runnable() {
 * 
 *     public void run() {
 *         System.out.println(&quot;Alt+Shift+F1 PRESSED!&quot;);
 *     }
 * });
 * </pre>
 * 
 * @author p-Sebastien.Laout
 * 
 */
public class KeyActionBinder {

    private static final Logger LOG = Logger.getLogger(KeyActionBinder.class);



    /**
     * Trigger a click on <code>button</code> when the key <code>keyCode</code>
     * is pressed anywhere in the <code>component</code>.
     * 
     * @param component
     *            the component where to watch for the key press
     * @param keyCode
     *            any {@link KeyEvent}.VK_* code
     * @param button
     *            the button on which to simulate a short click when the key is
     *            pressed
     */
    public static void bindKey(Component component, int keyCode, AbstractButton button) {
        bindKey(component, 0, keyCode, button);
    }



    /**
     * Invoke the specified <code>action</code> when the key
     * <code>keyCode</code> is pressed anywhere in the window
     * <code>component</code>.
     * 
     * @param component
     *            The component where to watch for the key press
     * @param keyCode
     *            Any {@link KeyEvent}.VK_* code
     * @param action
     *            The action on which to call the run() method when the key
     *            is pressed
     */
    public static void bindKey(Component component, int keyCode, Runnable action) {
        bindKey(component, 0, keyCode, action);
    }



    /**
     * Trigger a click on <code>button</code> when the key <code>keyCode</code>
     * is pressed with modifiers <code>modifiers</code> anywhere in the
     * <code>component</code>.
     * 
     * @param component
     *            the component where to watch for the key press
     * @param keyModifiers
     *            a bit-mask value of {@link KeyEvent#CTRL_MASK},
     *            {@link KeyEvent#ALT_MASK}, {@link KeyEvent#SHIFT_MASK},
     *            {@link KeyEvent#META_MASK}; 0 means any or no
     *            modifier are pressed to trigger the button click
     * @param keyCode
     *            any {@link KeyEvent}.VK_* code
     * @param button
     *            the button on which to simulate a short click when the key is
     *            pressed
     */
    static void bindKey(Component component, int keyModifiers, int keyCode, AbstractButton button) {
        AbstractKeyDispatcher dispatcher = new ButtonKeyDispatcher(component, keyModifiers, keyCode, button);
        component.addHierarchyListener(new VisibilityHierarchyListener(component, dispatcher));
    }



    /**
     * Invoke the specified <code>action</code> when the key
     * <code>keyCode</code> is pressed with modifiers <code>modifiers</code>
     * anywhere in the <code>component</code>.
     * 
     * @param component
     *            the component where to watch for the key press
     * @param keyModifiers
     *            a bit-mask value of {@link KeyEvent#CTRL_MASK},
     *            {@link KeyEvent#ALT_MASK}, {@link KeyEvent#SHIFT_MASK},
     *            {@link KeyEvent#META_MASK}; 0 means any or no
     *            modifier are pressed to trigger the button click
     * @param keyCode
     *            any {@link KeyEvent}.VK_* code
     * @param job
     *            The action on which to call the run() method when the key
     *            is pressed
     */
    public static void bindKey(Component component, int keyModifiers, int keyCode, Runnable job) {
        AbstractKeyDispatcher dispatcher = new ActionKeyDispatcher(component, keyModifiers, keyCode, job);
        HierarchyListener[] hlsts = component.getHierarchyListeners();
        for (int i = 0; i < hlsts.length; i++) {
            HierarchyListener hl = hlsts[i];
            if (hl instanceof VisibilityHierarchyListener) {
                VisibilityHierarchyListener vhl = (VisibilityHierarchyListener) hl;
                if (vhl.getKeyModifiers() == keyModifiers && vhl.getKeyCode() == keyCode) {
                    component.removeHierarchyListener(vhl);
                    break;
                }
            }
        }
        component.addHierarchyListener(new VisibilityHierarchyListener(component, dispatcher));
    }

    /**
     * <p>
     * Watch for a component's own visibility events (when it is show on screen
     * or hidden from screen) and attach a KeyEventDispatcher when showing the
     * component, and detach it when hiding the component.
     * </p>
     * <p>
     * We absolutely need to detach it because if a window attaches a
     * HierarchyListener, then is closed, created again and shown, the new
     * HierarchyListener will not be attached and the old one will be triggered
     * but pointing to the old deleted window. Then, since we detach the
     * listener on hiding, we attach it on showing.
     * </p>
     * 
     * @author p-Sebastien.Laout
     */
    private static final class VisibilityHierarchyListener implements HierarchyListener {

        private final Component component;

        private final AbstractKeyDispatcher dispatcher;



        private VisibilityHierarchyListener(Component component, AbstractKeyDispatcher dispatcher) {
            this.component = component;
            this.dispatcher = dispatcher;
        }



        int getKeyCode() {
            return dispatcher.getKeyCode();
        }



        int getKeyModifiers() {
            return dispatcher.getKeyModifiers();
        }



        public void hierarchyChanged(HierarchyEvent e) {
            // We receive events for the whole hierarchy of the component.
            // We are only interested in the parent component events...
            if (e.getComponent() == component) {
                // ... And to be more specific, we only need to know when the
                // component is made visible or is made hidden
                if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                    KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
                    // When the component is made visible, attach the key
                    // dispatcher, and detach it on hidding
                    if (component.isShowing()) {
                        manager.addKeyEventDispatcher(dispatcher);
                    } else {
                        manager.removeKeyEventDispatcher(dispatcher);
                    }
                }
            }
        }
    }

    /**
     * An abstract KeyEventDispatcher that triggers the abstract method
     * onKeySequenceDetected() when a key is pressed
     * (with the specified modifiers, or without taking account of the modifiers
     * if it is 0).
     * 
     * @author p-Sebastien.Laout
     */
    private abstract static class AbstractKeyDispatcher implements KeyEventDispatcher {

        private final Component component;

        private final int keyModifiers;

        private final int keyCode;



        public AbstractKeyDispatcher(Component component, int keyModifiers, int keyCode) {
            this.component = component;
            this.keyModifiers = keyModifiers;
            this.keyCode = keyCode;
        }



        public int getKeyCode() {
            return keyCode;
        }



        public int getKeyModifiers() {
            return keyModifiers;
        }



        public boolean dispatchKeyEvent(KeyEvent e) {
            // The user pressed the watched key?...
            if (e.getID() == KeyEvent.KEY_PRESSED && e.getKeyCode() == keyCode) {
                // ... And it has the watched modifiers, if we watched specific
                // ones?
                if (keyModifiers == 0 || keyModifiers == e.getModifiers()) {
                    // One window can watch for key Enter, for instance, and
                    // then call another sub-window that also watch for the
                    // Enter key.
                    // Both windows are visible, so their watchers are active.
                    // But only the window currently focused need to catch the
                    // key press: the other inactive window must not catch the
                    // event
                    if (areInSameWindow(component, e.getComponent())) {
                        onKeySequenceDetected();
                        // return true;
                    }
                }
            }
            return false;
        }



        private static boolean areInSameWindow(Component component1, Component component2) {
            Component root1 = SwingUtilities.getRoot(component1);
            Component root2 = SwingUtilities.getRoot(component2);
            return (root1 == root2);
        }



        protected abstract void onKeySequenceDetected();
    }

    /**
     * A KeyEventDispatcher that shortly presses a button when a key is pressed
     * (with the specified modifiers, or without taking account of the modifiers
     * if it is 0).
     * 
     * @author p-Sebastien.Laout
     */
    private static class ButtonKeyDispatcher extends AbstractKeyDispatcher {

        private static final int SIMULATED_CLICK_DURATION = 100/* ms */;

        private final AbstractButton button;



        public ButtonKeyDispatcher(Component component, int keyModifiers, int keyCode, AbstractButton button) {
            super(component, keyModifiers, keyCode);
            this.button = button;
        }



        @Override
        protected void onKeySequenceDetected() {
            button.doClick(SIMULATED_CLICK_DURATION);
        }
    }

    /**
     * A KeyEventDispatcher that runs the provided {Runnable#run} when a key is
     * pressed (with the specified modifiers, or without taking account of the
     * modifiers if it is 0).
     * 
     * @author p-Sebastien.Laout
     */
    private static class ActionKeyDispatcher extends AbstractKeyDispatcher {

        private final Runnable job;



        public ActionKeyDispatcher(Component component, int keyModifiers, int keyCode, Runnable job) {
            super(component, keyModifiers, keyCode);
            this.job = job;
        }



        @Override
        protected void onKeySequenceDetected() {
            try {
                job.run();
            } catch (Exception e) {
                LOG.error("onKeySequenceDetected(): Runnable.run() has failed !", e); //$NON-NLS-1$
            }
        }
    }
}
