package com.dolphland.client.util.animator;

import java.util.List;

import com.dolphland.client.util.action.UIAction;

/**
 * Parameter for action
 * 
 * @author JayJay
 * 
 */
public interface AnimationParameter {

    // Layer policy
    public final static int LAYER_FOREGROUND = 0;
    public final static int LAYER_BACKGROUND = 1;

    // Steps policy
    public final static int ALL_STEPS = 0;
    public final static int ONLY_ONE_STEP = 1;



    /**
     * @return the x
     */
    public Integer getX();



    /**
     * @return the y
     */
    public Integer getY();



    /**
     * @return the width
     */
    public Integer getWidth();



    /**
     * @return the height
     */
    public Integer getHeight();



    /**
     * @return the ratio width
     */
    public Double getRatioWidth();



    /**
     * @return the ratio height
     */
    public Double getRatioHeight();



    /**
     * @return the degreRotation
     */
    public Integer getDegreRotation();



    /**
     * @return the back
     */
    public Boolean isBack();



    /**
     * @return the layer policy
     */
    public Integer getLayerPolicy();



    /**
     * @return the steps policy
     */
    public Integer getStepsPolicy();



    /**
     * @return the animation's duration in milliseconds
     */
    public Long getAnimationDurationMs();



    /**
     * Called when animation panel has changed
     */
    public void fireAnimationPanelSizeHasChanged();



    /**
     * Get action to execute before animation
     */
    public List<UIAction<AnimationActionEvt, ?>> getActionsBeforeAnimation();



    /**
     * Get action to execute after animation
     */
    public List<UIAction<AnimationActionEvt, ?>> getActionsAfterAnimation();

}
