package com.dolphland.client.util.i18n;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.dolphland.client.util.string.StrUtil;

/**
 * <p>
 * Servlet filter meant to be used in a web context so as to automatically set
 * the thread context Locale that is used by {@link MessageFormater} to
 * determine the Locale to be used for message formatting.
 * </p>
 * 
 * @author Julien Delrue
 * @author JayJay
 */
public class MessageFormaterServletFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(MessageFormaterServletFilter.class);

    private static final String LANG_PARAMETER_NAME_PARAMETER = "langParameterName"; //$NON-NLS-1$

    private static final String DEFAULT_LANG_PARAMETER_NAME = "lang"; //$NON-NLS-1$

    private String langParameterName = null;



    public void init(FilterConfig config) throws ServletException {
        String langParameterName = config.getInitParameter(LANG_PARAMETER_NAME_PARAMETER);
        if (StrUtil.isEmpty(langParameterName)) {
            this.langParameterName = DEFAULT_LANG_PARAMETER_NAME;
        } else {
            this.langParameterName = langParameterName;
        }
        LOG.info("init() : Language parameter name set to <" + langParameterName + ">"); //$NON-NLS-1$ //$NON-NLS-2$
    }



    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        try {
            Locale localeToSet = null;
            String paramLangVal = req.getParameter(langParameterName);
            if (paramLangVal != null) {
                // trying to format a locale with the parameter value
                localeToSet = new Locale(paramLangVal);
                LOG.debug("doFilter() : Requested locale <" + localeToSet.toString() + "> from URL parameter <" + langParameterName + "> (" + paramLangVal + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            } else {
                // trying to get the locale via HTTP header
                HttpServletRequest request = (HttpServletRequest) req;
                localeToSet = request.getLocale();
                LOG.debug("doFilter() : No <" + langParameterName + "> found in request, got locale from HTTP header < " + localeToSet + ">"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            }

            // this should NEVER occur but we prefer to fuck the flies...
            if (localeToSet == null) {
                localeToSet = Locale.getDefault();
            }

            LOG.debug("doFilter() : Setting MessageFormater context locale to <" + localeToSet.toString() + ">"); //$NON-NLS-1$ //$NON-NLS-2$
            MessageFormater.setThreadLocale(localeToSet);

            chain.doFilter(req, res);
        } finally {
            LOG.debug("doFilter() : Clearing locale"); //$NON-NLS-1$
            MessageFormater.clearThreadLocale();
        }
    }



    public void destroy() {
        MessageFormater.clearThreadLocale();
    }

}
