/*
 * $Log: EventDestination.java,v $
 * Revision 1.4 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.3 2009/03/27 07:52:36 bvatan
 * - id passe en long
 * Revision 1.2 2009/03/06 12:49:34 bvatan
 * - gestion du hashCode unique et qui identifie la destination dans la JVM
 * Revision 1.1 2009/03/05 15:00:12 bvatan
 * - initial check in
 * Revision 1.5 2009/02/27 16:35:47 bvatan
 * - l'id n'est plus utilis�
 * Revision 1.4 2009/02/19 08:35:14 bvatan
 * - javadoc javadoc
 * Revision 1.3 2009/01/30 15:10:52 bvatan
 * - Serializable
 * Revision 1.2 2007/08/29 10:24:45 bvatan
 * - le contructeur devient public pour permettre de sp�cifier un id (int) pour
 * pallier au manque de fiabilit� de la m�thode hashCode() de String
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.2 2007/03/08 09:40:01 bvatan
 * - les Eventid ne sont plus identifi�s que par un String
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 */
package com.dolphland.client.util.event;

import java.io.Serializable;

import com.dolphland.client.util.string.StrUtil;

/**
 * Repr�sente une destination d'�v�nements.
 * 
 * @author JayJay
 */
public class EventDestination extends Object implements Comparable<EventDestination>, Serializable {

    private static int idSeq;



    private static final long nextId() {
        synchronized (EventDestination.class) {
            return idSeq++;
        }
    }

    private long id;

    private String representation;



    /**
     * Construit un cannal avec un nom construit en fonction du hashCode de
     * l'objet
     */
    public EventDestination() {
        this(null);
    }



    /**
     * Construit un cannal avec un nom unique
     */
    public EventDestination(String rep) {
        this.id = nextId();
        if (!StrUtil.isEmpty(rep)) {
            this.representation = rep;
        } else {
            this.representation = "EVDST_" + id; //$NON-NLS-1$
        }
    }



    public long id() {
        return id;
    }



    public String getRepresentation() {
        return representation;
    }



    public final int hashCode() {
        return super.hashCode();
    }



    public boolean equals(Object obj) {
        return compareTo((EventDestination) obj) == 0;
    }



    public int compareTo(EventDestination o) {
        EventDestination ed = (EventDestination) o;
        return ed.id < id ? -1 : ed.id > id ? 1 : 0;
    }



    public String toString() {
        return representation;
    }
}
