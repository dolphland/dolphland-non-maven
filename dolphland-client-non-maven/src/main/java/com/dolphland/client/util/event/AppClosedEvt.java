package com.dolphland.client.util.event;

/**
 * Ev�nement �mis par le framework lorsque qu'aucun v�to n'a �t� mis � la
 * fermetur de l'application.<br>
 * L'application sera ferm�e d�finitivement une fois que les abonn�s auront re�u
 * l'�v�nement.<br>
 * Sur r�ception de cet �v�nements, les abonn�s doivent d�finitivement lib�rer
 * les ressources qu'ils d�tiennent.
 * 
 * @author JayJay
 * 
 */
public final class AppClosedEvt extends Event {

    public AppClosedEvt() {
        super(FwkEvents.APP_CLOSED_EVT);
    }

}
