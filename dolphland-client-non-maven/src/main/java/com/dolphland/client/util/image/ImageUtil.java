package com.dolphland.client.util.image;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

/**
 * Image util
 * 
 * @author JayJay
 * 
 */
public class ImageUtil {

    private final static Logger log = Logger.getLogger(ImageUtil.class);



    /**
     * Fournit l'image icon � partir de la resource image
     * 
     * @param resource
     *            Resource image
     * @return Image icon
     */
    public final static ImageIcon getImageIcon(String resource) {
        return new ImageIcon(ImageUtil.class.getResource(resource));
    }



    /**
     * Fournit l'image icon � partir de la resource image dans une certaine
     * dimension
     * 
     * @param resource
     *            Resource image
     * @param width
     *            Largeur
     * @param height
     *            Hauteur
     * @return Image icon
     */
    public final static ImageIcon getImageIcon(String resource, int width, int height) {
        ImageIcon image = getImageIcon(resource);
        Image img = image.getImage();
        Image newimg = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(newimg);
    }



    /**
     * Fournit l'image icon � partir de l'image
     * 
     * @param image
     *            Image
     * @return Image icon
     */
    public final static ImageIcon getImageIcon(BufferedImage image) {
        return new ImageIcon(image);
    }



    /**
     * Fournit l'image icon � partir de l'image dans une certaine dimension
     * 
     * @param image
     *            Image
     * @param width
     *            Largeur
     * @param height
     *            Hauteur
     * @return Image icon
     */
    public final static ImageIcon getImageIcon(BufferedImage image, int width, int height) {
        Image img = getImageIcon(image).getImage();
        Image newimg = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(newimg);
    }



    public final static BufferedImage getImage(String resourcePath) {
        try {
            return ImageIO.read(new ImageUtil().getClass().getResource(resourcePath));
        } catch (Exception e) {
            log.error("Erreur survenue lors de la r�cup�ration de l'image '" + resourcePath + "'", e);
            return null;
        }
    }



    public final static BufferedImage getImage(URL url) {
        try {
            return read(url);
        } catch (Exception e) {
            log.error("Erreur survenue lors de la r�cup�ration de l'image '" + url.getFile() + "'", e);
            return null;
        }
    }



    public final static BufferedImage getImage(Component component) {
        BufferedImage image = new BufferedImage(component.getWidth(), component.getHeight(), BufferedImage.TYPE_INT_ARGB);
        component.paint(image.getGraphics());
        return image;
    }



    private final static BufferedImage read(URL input) throws IOException {
        if (input == null) {
            throw new IllegalArgumentException("input == null!");
        }

        InputStream istream = null;
        try {
            istream = input.openConnection().getInputStream();
        } catch (IOException e) {
            throw new IIOException("Can't get input stream from URL!", e);
        }
        ImageInputStream stream = ImageIO.createImageInputStream(istream);
        BufferedImage bi;
        try {
            bi = ImageIO.read(stream);
            if (bi == null) {
                stream.close();
            }
        } finally {
            istream.close();
        }
        return bi;
    }



    public final static BufferedImage rotateImage(BufferedImage img, double angleRotation, ImageObserver observer) {
        AffineTransform identity = new AffineTransform();
        int width = img.getHeight();
        int height = img.getWidth();
        BufferedImage imageRotate = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D g2d = (Graphics2D) imageRotate.getGraphics();
        AffineTransform trans = new AffineTransform();
        trans.setTransform(identity);
        trans.translate(width, 0);
        trans.rotate(Math.toRadians(angleRotation));
        g2d.drawImage(img, trans, observer);
        return imageRotate;
    }
}
