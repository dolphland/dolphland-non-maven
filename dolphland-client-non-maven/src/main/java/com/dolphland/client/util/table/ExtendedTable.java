package com.dolphland.client.util.table;

import java.util.HashMap;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.i18n.MessageFormater;

/**
 * JTable ExtendedTable ajoutant � la notion de JTable :
 * - Cr�ation d'ent�tes de colonnes group�es
 * - Redimensionnement automatique des ent�tes de colonne en fonction de leur
 * contenu
 * 
 * @author jeremy.scafi
 */
public class ExtendedTable extends JTable {

    private static final MessageFormater FMT = MessageFormater.getFormater(ExtendedTable.class);
    private HashMap columnGroups;



    public ExtendedTable() {
        super();
        // Si ent�te de groupage de colonnes d�fini
        if (getColumnModel() != null) {
            setTableHeader(new GroupableTableHeader(getColumnModel()));
        }
        // Si aucun ent�te
        else {
            setTableHeader(new GroupableTableHeader());
        }
    }



    @Override
    public void setColumnModel(TableColumnModel columnModel) {
        super.setColumnModel(columnModel);
        setTableHeader(new GroupableTableHeader(getColumnModel()));
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre group�es
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin) {
        addColumnGroup(idColumnGroup, titleColumnGroup, indicesColumnTableDeb, indicesColumnTableFin, null, null);
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre group�es
     * @param columnGroupRenderer
     *            Renderer du groupe de colonnes
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin, TableCellRenderer columnGroupRenderer) {
        addColumnGroup(idColumnGroup, titleColumnGroup, indicesColumnTableDeb, indicesColumnTableFin, null, columnGroupRenderer);
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre group�es
     * @param idColumnGroupParent
     *            Indique l'identifiant du groupe de colonnes devant contenir le
     *            groupe de colonnes
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin, String idColumnGroupParent) {
        addColumnGroup(idColumnGroup, titleColumnGroup, indicesColumnTableDeb, indicesColumnTableFin, idColumnGroupParent, null);
    }



    /**
     * Ajoute un groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     * @param indiceColumnTableDeb
     *            Indice de d�but des colonnes de la JTable devant �tre group�es
     * @param indiceColumnTableFin
     *            Indice de fin des colonnes de la JTable devant �tre group�es
     * @param idColumnGroupParent
     *            Indique l'identifiant du groupe de colonnes devant contenir le
     *            groupe de colonnes
     * @param columnGroupRenderer
     *            Renderer du groupe de colonnes
     */
    public void addColumnGroup(String idColumnGroup, String titleColumnGroup, int indicesColumnTableDeb, int indicesColumnTableFin, String idColumnGroupParent, TableCellRenderer columnGroupRenderer) {
        GroupableTableHeader header = (GroupableTableHeader) this.getTableHeader();

        // Ajout du groupe de colonnes
        TableColumnModel cm = getColumnModel();
        ColumnGroup cg = new ColumnGroup(titleColumnGroup);
        for (int ind = Math.min(indicesColumnTableDeb, indicesColumnTableFin); ind <= Math.max(indicesColumnTableDeb, indicesColumnTableFin); ind++) {
            if (ind >= cm.getColumnCount()) {
                throw new AppInfrastructureException("The table column index exceeds the table columns capacity [%s>=%s]!", ind, cm.getColumnCount());
            }

            cg.add(cm.getColumn(ind));
        }
        if (idColumnGroupParent != null) {
            ColumnGroup cGroupParent = getColumnGroup(idColumnGroupParent);
            if (cGroupParent == null) {
                throw new AppInfrastructureException("The parent column group %s doesn''t exist in the table !", idColumnGroupParent);
            }
            cGroupParent.add(cg);
        }
        if (columnGroupRenderer != null)
            cg.setHeaderRenderer(columnGroupRenderer);
        putColumnGroup(idColumnGroup, cg);
        header.addColumnGroup(cg);
        header.revalidate();
    }



    /**
     * Fixe le titre du groupe de colonnes
     * 
     * @param idColumnGroup
     *            Identifiant du groupe de colonnes
     * @param titleColumnGroup
     *            Titre du groupe de colonnes
     */
    public void setColumnGroupTitle(String idColumnGroup, String titleColumnGroup) {
        ColumnGroup cg = getColumnGroup(idColumnGroup);
        if (cg != null) {
            cg.setText(titleColumnGroup);
        }
    }



    private void putColumnGroup(String idColumnGroup, ColumnGroup cg) {
        if (columnGroups == null) {
            columnGroups = new HashMap();
        }
        if (columnGroups.containsKey(idColumnGroup)) {
            throw new AppInfrastructureException("The column group %s already exist", idColumnGroup);
        }
        columnGroups.put(idColumnGroup, cg);
    }



    private ColumnGroup getColumnGroup(String idColumnGroup) {
        if (columnGroups != null) {
            return (ColumnGroup) columnGroups.get(idColumnGroup);
        }
        return null;
    }
}
