package com.dolphland.client.util.transaction;

/**
 * Un {@link SwingActionExecutor} est responsable de l'ex�cution d'une ou
 * plusieurs {@link SwingAction}</code>.<br>
 * <br>
 * {@link SwingActionExecutor} doit �tre d�marr� avant de pouvoir traiter les
 * {@link SwingAction} qui lui sont transmis.<br>
 * <br>
 * <u>Exemple d'utilisation:</u><br>
 * 
 * <pre>
 * // Obtention d'un SwingActionExecutor
 * SwingActionExecutor executor = ...;
 * // Obtention d'un SwingAction � ex�cuter
 * SwingAction action = ...;
 * // Transmission de l'action � l'ex�cutor (param�tre null pour l'action)
 * executor.add(action, null);
 * </pre>
 * 
 * Au pr�alable, l'ex�cutor doit �tre d�marr� sinon l'action ne sera pas
 * ex�cut�e :
 * 
 * <pre>
 * SwingActionExecutor executor = ...;
 * executor.start();
 * </pre>
 * 
 * @author JayJay
 */
public interface SwingActionExecutor {

    /**
     * D�marre l'ex�cutor. A partir de l�, les actions en attente commencent �
     * �tre ex�cut�es.
     */
    public abstract void start();



    /**
     * Arr�te l'ex�cutor. Les actions cessent d'�tre ex�cut�es.
     */
    public abstract void stop();



    /**
     * Ajoute une action � ex�cuter. Si l'action prend un param�tre il peut �tre
     * transmis en second argument, sinon null est permis.
     * 
     * @param <P>
     *            Type du param�tre de l'action
     * @param <T>
     *            Type de la valeur produite par l'action (doExecute(), c.f.
     *            {@link SwingAction}
     * @param sa
     *            Le {@link SwingAction} � ex�cuter
     * @param param
     *            Le param�tre � transmetre � l'action lors de l'ex�cution, null
     *            si aucun
     */
    public abstract <P, T> void add(SwingAction<P, T> sa, P param);

}
