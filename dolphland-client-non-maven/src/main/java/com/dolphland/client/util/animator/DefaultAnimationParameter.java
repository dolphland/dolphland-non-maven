package com.dolphland.client.util.animator;

import java.util.ArrayList;
import java.util.List;

import com.dolphland.client.util.action.UIAction;

/**
 * Get parameters for animation
 * 
 * @author JayJay
 * 
 */
public class DefaultAnimationParameter implements AnimationParameter {

    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;
    private Double ratioWidth;
    private Double ratioHeight;
    private Integer degreRotation;
    private Boolean back;
    private Integer layerPolicy;
    private Integer stepsPolicy;
    private Long animationDurationMs;
    private List<UIAction<AnimationActionEvt, ?>> actionsBeforeAnimation;
    private List<UIAction<AnimationActionEvt, ?>> actionsAfterAnimation;



    /**
     * Default constructor
     */
    public DefaultAnimationParameter() {
        actionsBeforeAnimation = new ArrayList<UIAction<AnimationActionEvt, ?>>();
        actionsAfterAnimation = new ArrayList<UIAction<AnimationActionEvt, ?>>();
    }



    /**
     * @return the x
     */
    public Integer getX() {
        return x;
    }



    /**
     * @param x
     *            the x to set
     */
    public void setX(Integer x) {
        this.x = x;
    }



    /**
     * @return the y
     */
    public Integer getY() {
        return y;
    }



    /**
     * @param y
     *            the y to set
     */
    public void setY(Integer y) {
        this.y = y;
    }



    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }



    /**
     * @param width
     *            the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }



    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }



    /**
     * @param height
     *            the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }



    /**
     * @return the ratioWidth
     */
    public Double getRatioWidth() {
        return ratioWidth;
    }



    /**
     * @param ratioWidth
     *            the ratioWidth to set
     */
    public void setRatioWidth(Double ratioWidth) {
        this.ratioWidth = ratioWidth;
    }



    /**
     * @return the ratioHeight
     */
    public Double getRatioHeight() {
        return ratioHeight;
    }



    /**
     * @param ratioHeight
     *            the ratioHeight to set
     */
    public void setRatioHeight(Double ratioHeight) {
        this.ratioHeight = ratioHeight;
    }



    /**
     * @return the degreRotation
     */
    public Integer getDegreRotation() {
        return degreRotation;
    }



    /**
     * @param degreRotation
     *            the degreRotation to set
     */
    public void setDegreRotation(Integer degreRotation) {
        this.degreRotation = degreRotation;
    }



    /**
     * @return the back
     */
    public Boolean isBack() {
        return back;
    }



    /**
     * @param back
     *            the back to set
     */
    public void setBack(Boolean back) {
        this.back = back;
    }



    /**
     * @return the layerPolicy
     */
    public Integer getLayerPolicy() {
        return layerPolicy;
    }



    /**
     * @param layerPolicy
     *            the layerPolicy to set
     */
    public void setLayerPolicy(Integer layerPolicy) {
        this.layerPolicy = layerPolicy;
    }



    /**
     * @return the stepsPolicy
     */
    public Integer getStepsPolicy() {
        return stepsPolicy;
    }



    /**
     * @param stepsPolicy
     *            the stepsPolicy to set
     */
    public void setStepsPolicy(Integer stepsPolicy) {
        this.stepsPolicy = stepsPolicy;
    }



    /**
     * @return the animationDurationMs
     */
    public Long getAnimationDurationMs() {
        return animationDurationMs;
    }



    /**
     * @param animationDurationMs
     *            the animationDurationMs to set
     */
    public void setAnimationDurationMs(Long animationDurationMs) {
        this.animationDurationMs = animationDurationMs;
    }



    @Override
    public void fireAnimationPanelSizeHasChanged() {
        // Do nothing
    }



    /**
     * @return the actionsBeforeAnimation
     */
    public List<UIAction<AnimationActionEvt, ?>> getActionsBeforeAnimation() {
        return actionsBeforeAnimation;
    }



    /**
     * @return the actionsAfterAnimation
     */
    public List<UIAction<AnimationActionEvt, ?>> getActionsAfterAnimation() {
        return actionsAfterAnimation;
    }
}
