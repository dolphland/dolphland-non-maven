// $ANTLR 3.3 Nov 30, 2010 12:45:30
// D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g 2012-08-02 16:11:00

package com.dolphland.client.util.i18n.varlist;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.BitSet;
import org.antlr.runtime.DFA;
import org.antlr.runtime.IntStream;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;

public class VarListParser extends Parser {

    public static final String[] tokenNames = new String[] {
    "<invalid>", "<EOR>", "<DOWN>", "<UP>", "INT", "STRING", "FLOAT", "CHAR", "DATE", "NULL", "DecimalLiteral", "Identifier", "FloatingPointLiteral", "CharLiteral", "StringLiteral", "CAR", "DIGIT", "HexDigit", "Exponent", "FloatTypeSuffix", "EscapeSequence", "UnicodeEscape", "Letter", "JavaIDDigit", "WS", "NL", "COMMENT", "LINE_COMMENT", "','", "'='"
    };
    public static final int EOF = -1;
    public static final int T__28 = 28;
    public static final int T__29 = 29;
    public static final int INT = 4;
    public static final int STRING = 5;
    public static final int FLOAT = 6;
    public static final int CHAR = 7;
    public static final int DATE = 8;
    public static final int NULL = 9;
    public static final int DecimalLiteral = 10;
    public static final int Identifier = 11;
    public static final int FloatingPointLiteral = 12;
    public static final int CharLiteral = 13;
    public static final int StringLiteral = 14;
    public static final int CAR = 15;
    public static final int DIGIT = 16;
    public static final int HexDigit = 17;
    public static final int Exponent = 18;
    public static final int FloatTypeSuffix = 19;
    public static final int EscapeSequence = 20;
    public static final int UnicodeEscape = 21;
    public static final int Letter = 22;
    public static final int JavaIDDigit = 23;
    public static final int WS = 24;
    public static final int NL = 25;
    public static final int COMMENT = 26;
    public static final int LINE_COMMENT = 27;



    // delegates
    // delegators

    public VarListParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }



    public VarListParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.ruleMemo = new HashMap[16 + 1];

    }



    public String[] getTokenNames() {
        return VarListParser.tokenNames;
    }



    public String getGrammarFileName() {
        return "D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g";
    }

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");



    public Object recoverFromMismatchedSet(IntStream input, RecognitionException e, BitSet follow) throws RecognitionException {
        throw e;
    }



    public String getErrorMessage(RecognitionException re, String[] arg1) {
        return super.getErrorMessage(re, arg1);
    }



    protected Object recoverFromMismatchedToken(IntStream is, int token, BitSet bs) throws RecognitionException {
        throw new RuntimeException("Mismatched token " + getTokenNames()[token] + ".");
    }



    public void setDateFormatPattern(String dateFormat) {
        this.dateFormat = new SimpleDateFormat(dateFormat);
    }



    public void setDateFormat(SimpleDateFormat dateFormat) {
        if (dateFormat == null) {
            throw new NullPointerException("dateFormat is null !");
        }
        this.dateFormat = dateFormat;
    }



    private String normalize(String raw) {
        raw = raw.substring(1, raw.length() - 1);
        raw = raw.replaceAll("\\\\\"", "\"");
        raw = raw.replaceAll("\\\\n", "\n");
        raw = raw.replaceAll("\\\\t", "\t");
        raw = raw.replaceAll("\\\\r", "\r");
        return raw;
    }



    private Date parseDate(String input, String varName) {
        try {
            return dateFormat.parse(input);
        } catch (Exception e) {
            throw new VarParseException("Could not parse date " + input + " for variable " + varName + " with format " + dateFormat.toPattern(), e);
        }
    }



    // $ANTLR start "variables"
    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:80:1: variables
    // returns [Variables vars] : v= variable ( ',' v= variable )* ;
    public final Variables variables() throws RecognitionException {
        Variables vars = null;
        int variables_StartIndex = input.index();
        Variable v = null;

        vars = new Variables();

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 1)) {
                return vars;
            }
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:84:2: (v=
            // variable ( ',' v= variable )* )
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:84:4: v=
            // variable ( ',' v= variable )*
            {
                pushFollow(FOLLOW_variable_in_variables113);
                v = variable();

                state._fsp--;
                if (state.failed)
                    return vars;
                if (state.backtracking == 0) {
                    vars.add(v);
                }
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:84:38:
                // ( ',' v= variable )*
                loop1: do {
                    int alt1 = 2;
                    int LA1_0 = input.LA(1);

                    if ((LA1_0 == 28)) {
                        alt1 = 1;
                    }

                    switch (alt1) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:84:39:
                        // ',' v= variable
                        {
                            match(input, 28, FOLLOW_28_in_variables118);
                            if (state.failed)
                                return vars;
                            pushFollow(FOLLOW_variable_in_variables122);
                            v = variable();

                            state._fsp--;
                            if (state.failed)
                                return vars;
                            if (state.backtracking == 0) {
                                vars.add(v);
                            }

                        }
                            break;

                        default :
                            break loop1;
                    }
                } while (true);

            }

        }

        catch (RecognitionException e) {
            throw new RuntimeException(getErrorMessage(e, tokenNames), e);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 1, variables_StartIndex);
            }
        }
        return vars;
    }



    // $ANTLR end "variables"

    // $ANTLR start "variable"
    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:87:1: variable
    // returns [Variable var] : ( ( STRING )? sname= DecimalLiteral '=' s1=
    // string | ( STRING )? sname= Identifier '=' s1= string | DATE dname=
    // DecimalLiteral '=' s2= string | DATE dname= Identifier '=' s2= string |
    // INT iname= DecimalLiteral '=' dl= DecimalLiteral | INT iname= Identifier
    // '=' dl= DecimalLiteral | FLOAT fname= DecimalLiteral '=' fpl=
    // FloatingPointLiteral | FLOAT fname= Identifier '=' fpl=
    // FloatingPointLiteral | CHAR cname= DecimalLiteral '=' car= CharLiteral |
    // CHAR cname= Identifier '=' car= CharLiteral );
    public final Variable variable() throws RecognitionException {
        Variable var = null;
        int variable_StartIndex = input.index();
        Token sname = null;
        Token dname = null;
        Token iname = null;
        Token dl = null;
        Token fname = null;
        Token fpl = null;
        Token cname = null;
        Token car = null;
        String s1 = null;

        String s2 = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 2)) {
                return var;
            }
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:88:2: ( (
            // STRING )? sname= DecimalLiteral '=' s1= string | ( STRING )?
            // sname= Identifier '=' s1= string | DATE dname= DecimalLiteral '='
            // s2= string | DATE dname= Identifier '=' s2= string | INT iname=
            // DecimalLiteral '=' dl= DecimalLiteral | INT iname= Identifier '='
            // dl= DecimalLiteral | FLOAT fname= DecimalLiteral '=' fpl=
            // FloatingPointLiteral | FLOAT fname= Identifier '=' fpl=
            // FloatingPointLiteral | CHAR cname= DecimalLiteral '=' car=
            // CharLiteral | CHAR cname= Identifier '=' car= CharLiteral )
            int alt4 = 10;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:88:4:
                // ( STRING )? sname= DecimalLiteral '=' s1= string
                {
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:88:4:
                    // ( STRING )?
                    int alt2 = 2;
                    int LA2_0 = input.LA(1);

                    if ((LA2_0 == STRING)) {
                        alt2 = 1;
                    }
                    switch (alt2) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:0:0:
                        // STRING
                        {
                            match(input, STRING, FOLLOW_STRING_in_variable141);
                            if (state.failed)
                                return var;

                        }
                            break;

                    }

                    sname = (Token) match(input, DecimalLiteral, FOLLOW_DecimalLiteral_in_variable146);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable148);
                    if (state.failed)
                        return var;
                    pushFollow(FOLLOW_string_in_variable152);
                    s1 = string();

                    state._fsp--;
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.STRING, (sname != null ? sname.getText() : null), s1);

                    }

                }
                    break;
                case 2 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:91:4:
                // ( STRING )? sname= Identifier '=' s1= string
                {
                    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:91:4:
                    // ( STRING )?
                    int alt3 = 2;
                    int LA3_0 = input.LA(1);

                    if ((LA3_0 == STRING)) {
                        alt3 = 1;
                    }
                    switch (alt3) {
                        case 1 :
                        // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:0:0:
                        // STRING
                        {
                            match(input, STRING, FOLLOW_STRING_in_variable159);
                            if (state.failed)
                                return var;

                        }
                            break;

                    }

                    sname = (Token) match(input, Identifier, FOLLOW_Identifier_in_variable164);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable166);
                    if (state.failed)
                        return var;
                    pushFollow(FOLLOW_string_in_variable170);
                    s1 = string();

                    state._fsp--;
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.STRING, (sname != null ? sname.getText() : null), s1);

                    }

                }
                    break;
                case 3 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:94:4:
                // DATE dname= DecimalLiteral '=' s2= string
                {
                    match(input, DATE, FOLLOW_DATE_in_variable177);
                    if (state.failed)
                        return var;
                    dname = (Token) match(input, DecimalLiteral, FOLLOW_DecimalLiteral_in_variable181);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable183);
                    if (state.failed)
                        return var;
                    pushFollow(FOLLOW_string_in_variable187);
                    s2 = string();

                    state._fsp--;
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        Date date = parseDate(s2, (dname != null ? dname.getText() : null));
                        var = new Variable(VarType.DATE, (dname != null ? dname.getText() : null), date);

                    }

                }
                    break;
                case 4 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:98:4:
                // DATE dname= Identifier '=' s2= string
                {
                    match(input, DATE, FOLLOW_DATE_in_variable194);
                    if (state.failed)
                        return var;
                    dname = (Token) match(input, Identifier, FOLLOW_Identifier_in_variable198);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable200);
                    if (state.failed)
                        return var;
                    pushFollow(FOLLOW_string_in_variable204);
                    s2 = string();

                    state._fsp--;
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        Date date = parseDate(s2, (dname != null ? dname.getText() : null));
                        var = new Variable(VarType.DATE, (dname != null ? dname.getText() : null), date);

                    }

                }
                    break;
                case 5 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:102:4:
                // INT iname= DecimalLiteral '=' dl= DecimalLiteral
                {
                    match(input, INT, FOLLOW_INT_in_variable211);
                    if (state.failed)
                        return var;
                    iname = (Token) match(input, DecimalLiteral, FOLLOW_DecimalLiteral_in_variable215);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable217);
                    if (state.failed)
                        return var;
                    dl = (Token) match(input, DecimalLiteral, FOLLOW_DecimalLiteral_in_variable221);
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.INTEGER, (iname != null ? iname.getText() : null), new BigDecimal((dl != null ? dl.getText() : null)));

                    }

                }
                    break;
                case 6 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:105:4:
                // INT iname= Identifier '=' dl= DecimalLiteral
                {
                    match(input, INT, FOLLOW_INT_in_variable228);
                    if (state.failed)
                        return var;
                    iname = (Token) match(input, Identifier, FOLLOW_Identifier_in_variable232);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable234);
                    if (state.failed)
                        return var;
                    dl = (Token) match(input, DecimalLiteral, FOLLOW_DecimalLiteral_in_variable238);
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.INTEGER, (iname != null ? iname.getText() : null), new BigDecimal((dl != null ? dl.getText() : null)));

                    }

                }
                    break;
                case 7 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:108:4:
                // FLOAT fname= DecimalLiteral '=' fpl= FloatingPointLiteral
                {
                    match(input, FLOAT, FOLLOW_FLOAT_in_variable245);
                    if (state.failed)
                        return var;
                    fname = (Token) match(input, DecimalLiteral, FOLLOW_DecimalLiteral_in_variable249);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable251);
                    if (state.failed)
                        return var;
                    fpl = (Token) match(input, FloatingPointLiteral, FOLLOW_FloatingPointLiteral_in_variable255);
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.FLOAT, (fname != null ? fname.getText() : null), new BigDecimal((fpl != null ? fpl.getText() : null)));

                    }

                }
                    break;
                case 8 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:111:4:
                // FLOAT fname= Identifier '=' fpl= FloatingPointLiteral
                {
                    match(input, FLOAT, FOLLOW_FLOAT_in_variable262);
                    if (state.failed)
                        return var;
                    fname = (Token) match(input, Identifier, FOLLOW_Identifier_in_variable266);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable268);
                    if (state.failed)
                        return var;
                    fpl = (Token) match(input, FloatingPointLiteral, FOLLOW_FloatingPointLiteral_in_variable272);
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.FLOAT, (fname != null ? fname.getText() : null), new BigDecimal((fpl != null ? fpl.getText() : null)));

                    }

                }
                    break;
                case 9 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:114:4:
                // CHAR cname= DecimalLiteral '=' car= CharLiteral
                {
                    match(input, CHAR, FOLLOW_CHAR_in_variable279);
                    if (state.failed)
                        return var;
                    cname = (Token) match(input, DecimalLiteral, FOLLOW_DecimalLiteral_in_variable283);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable285);
                    if (state.failed)
                        return var;
                    car = (Token) match(input, CharLiteral, FOLLOW_CharLiteral_in_variable289);
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.CHAR, (cname != null ? cname.getText() : null), normalize((car != null ? car.getText() : null)));

                    }

                }
                    break;
                case 10 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:117:4:
                // CHAR cname= Identifier '=' car= CharLiteral
                {
                    match(input, CHAR, FOLLOW_CHAR_in_variable296);
                    if (state.failed)
                        return var;
                    cname = (Token) match(input, Identifier, FOLLOW_Identifier_in_variable300);
                    if (state.failed)
                        return var;
                    match(input, 29, FOLLOW_29_in_variable302);
                    if (state.failed)
                        return var;
                    car = (Token) match(input, CharLiteral, FOLLOW_CharLiteral_in_variable306);
                    if (state.failed)
                        return var;
                    if (state.backtracking == 0) {

                        var = new Variable(VarType.CHAR, (cname != null ? cname.getText() : null), normalize((car != null ? car.getText() : null)));

                    }

                }
                    break;

            }
        }

        catch (RecognitionException e) {
            throw new RuntimeException(getErrorMessage(e, tokenNames), e);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 2, variable_StartIndex);
            }
        }
        return var;
    }



    // $ANTLR end "variable"

    // $ANTLR start "string"
    // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:122:1: string
    // returns [String value] : (str= StringLiteral | NULL );
    public final String string() throws RecognitionException {
        String value = null;
        int string_StartIndex = input.index();
        Token str = null;

        try {
            if (state.backtracking > 0 && alreadyParsedRule(input, 3)) {
                return value;
            }
            // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:123:2:
            // (str= StringLiteral | NULL )
            int alt5 = 2;
            int LA5_0 = input.LA(1);

            if ((LA5_0 == StringLiteral)) {
                alt5 = 1;
            }
            else if ((LA5_0 == NULL)) {
                alt5 = 2;
            }
            else {
                if (state.backtracking > 0) {
                    state.failed = true;
                    return value;
                }
                NoViableAltException nvae =
                new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:123:4:
                // str= StringLiteral
                {
                    str = (Token) match(input, StringLiteral, FOLLOW_StringLiteral_in_string324);
                    if (state.failed)
                        return value;
                    if (state.backtracking == 0) {
                        value = normalize((str != null ? str.getText() : null));
                    }

                }
                    break;
                case 2 :
                // D:\\dev\\workspaces\\workspace4\\fwk-common\\VarList.g:124:4:
                // NULL
                {
                    match(input, NULL, FOLLOW_NULL_in_string331);
                    if (state.failed)
                        return value;
                    if (state.backtracking == 0) {
                        value = null;
                    }

                }
                    break;

            }
        }

        catch (RecognitionException e) {
            throw new RuntimeException(getErrorMessage(e, tokenNames), e);
        } finally {
            if (state.backtracking > 0) {
                memoize(input, 3, string_StartIndex);
            }
        }
        return value;
    }

    // $ANTLR end "string"

    // Delegated rules

    protected DFA4 dfa4 = new DFA4(this);
    static final String DFA4_eotS =
    "\20\uffff";
    static final String DFA4_eofS =
    "\20\uffff";
    static final String DFA4_minS =
    "\1\4\1\12\2\uffff\4\12\10\uffff";
    static final String DFA4_maxS =
    "\2\13\2\uffff\4\13\10\uffff";
    static final String DFA4_acceptS =
    "\2\uffff\1\1\1\2\4\uffff\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12";
    static final String DFA4_specialS =
    "\20\uffff}>";
    static final String[] DFA4_transitionS = {
    "\1\5\1\1\1\6\1\7\1\4\1\uffff\1\2\1\3",
    "\1\2\1\3",
    "",
    "",
    "\1\10\1\11",
    "\1\12\1\13",
    "\1\14\1\15",
    "\1\16\1\17",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i = 0; i < numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }



        public String getDescription() {
            return "87:1: variable returns [Variable var] : ( ( STRING )? sname= DecimalLiteral '=' s1= string | ( STRING )? sname= Identifier '=' s1= string | DATE dname= DecimalLiteral '=' s2= string | DATE dname= Identifier '=' s2= string | INT iname= DecimalLiteral '=' dl= DecimalLiteral | INT iname= Identifier '=' dl= DecimalLiteral | FLOAT fname= DecimalLiteral '=' fpl= FloatingPointLiteral | FLOAT fname= Identifier '=' fpl= FloatingPointLiteral | CHAR cname= DecimalLiteral '=' car= CharLiteral | CHAR cname= Identifier '=' car= CharLiteral );";
        }
    }

    public static final BitSet FOLLOW_variable_in_variables113 = new BitSet(new long[] { 0x0000000010000002L });
    public static final BitSet FOLLOW_28_in_variables118 = new BitSet(new long[] { 0x0000000000000DF0L });
    public static final BitSet FOLLOW_variable_in_variables122 = new BitSet(new long[] { 0x0000000010000002L });
    public static final BitSet FOLLOW_STRING_in_variable141 = new BitSet(new long[] { 0x0000000000000400L });
    public static final BitSet FOLLOW_DecimalLiteral_in_variable146 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable148 = new BitSet(new long[] { 0x0000000000004200L });
    public static final BitSet FOLLOW_string_in_variable152 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_STRING_in_variable159 = new BitSet(new long[] { 0x0000000000000800L });
    public static final BitSet FOLLOW_Identifier_in_variable164 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable166 = new BitSet(new long[] { 0x0000000000004200L });
    public static final BitSet FOLLOW_string_in_variable170 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_DATE_in_variable177 = new BitSet(new long[] { 0x0000000000000400L });
    public static final BitSet FOLLOW_DecimalLiteral_in_variable181 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable183 = new BitSet(new long[] { 0x0000000000004200L });
    public static final BitSet FOLLOW_string_in_variable187 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_DATE_in_variable194 = new BitSet(new long[] { 0x0000000000000800L });
    public static final BitSet FOLLOW_Identifier_in_variable198 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable200 = new BitSet(new long[] { 0x0000000000004200L });
    public static final BitSet FOLLOW_string_in_variable204 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_INT_in_variable211 = new BitSet(new long[] { 0x0000000000000400L });
    public static final BitSet FOLLOW_DecimalLiteral_in_variable215 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable217 = new BitSet(new long[] { 0x0000000000000400L });
    public static final BitSet FOLLOW_DecimalLiteral_in_variable221 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_INT_in_variable228 = new BitSet(new long[] { 0x0000000000000800L });
    public static final BitSet FOLLOW_Identifier_in_variable232 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable234 = new BitSet(new long[] { 0x0000000000000400L });
    public static final BitSet FOLLOW_DecimalLiteral_in_variable238 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_FLOAT_in_variable245 = new BitSet(new long[] { 0x0000000000000400L });
    public static final BitSet FOLLOW_DecimalLiteral_in_variable249 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable251 = new BitSet(new long[] { 0x0000000000001000L });
    public static final BitSet FOLLOW_FloatingPointLiteral_in_variable255 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_FLOAT_in_variable262 = new BitSet(new long[] { 0x0000000000000800L });
    public static final BitSet FOLLOW_Identifier_in_variable266 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable268 = new BitSet(new long[] { 0x0000000000001000L });
    public static final BitSet FOLLOW_FloatingPointLiteral_in_variable272 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_CHAR_in_variable279 = new BitSet(new long[] { 0x0000000000000400L });
    public static final BitSet FOLLOW_DecimalLiteral_in_variable283 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable285 = new BitSet(new long[] { 0x0000000000002000L });
    public static final BitSet FOLLOW_CharLiteral_in_variable289 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_CHAR_in_variable296 = new BitSet(new long[] { 0x0000000000000800L });
    public static final BitSet FOLLOW_Identifier_in_variable300 = new BitSet(new long[] { 0x0000000020000000L });
    public static final BitSet FOLLOW_29_in_variable302 = new BitSet(new long[] { 0x0000000000002000L });
    public static final BitSet FOLLOW_CharLiteral_in_variable306 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_StringLiteral_in_string324 = new BitSet(new long[] { 0x0000000000000002L });
    public static final BitSet FOLLOW_NULL_in_string331 = new BitSet(new long[] { 0x0000000000000002L });

}