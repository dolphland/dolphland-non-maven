/*
 * $Log: UIHideCtxInfoEvt.java,v $
 * Revision 1.6 2014/03/18 13:00:52 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * Revision 1.5 2009/08/06 12:55:10 bvatan
 * - Gestion de l'affichage/masque progressif des bulles d'infos
 * Revision 1.4 2008/10/09 09:06:59 jscafi
 * - Prise en compte du positionnement du body pour les bulles � positionnement
 * absolu
 * - Ajout possibilit� de changer la couleur de fond et d'�crit d'une bulle
 * - Correction sur les �v�nements des bulles o� il y avait inversion dans les
 * identifiants d'�venement show et hide
 * - Restriction private package pass� en public pour le CtxInfoManager
 * Revision 1.3 2008/09/15 13:50:36 bvatan
 * - ajout javadoc
 * Revision 1.2 2008/07/23 14:27:27 bvatan
 * - merge avec PRODUCT_PAINTING
 * - correction du syst�me de coordonn�es (int,float->double)
 * - autoalignement des produits avec gestion du mode autoAdjustable
 * - fixation de la taille des produits de mani�re uniforme sur tous les
 * composant via SynoptiqueConf
 * - corrections bugs sur la taille des fl�ches signaux
 * Revision 1.1.2.1 2008/07/21 17:09:09 bvatan
 * - merge avec head
 * - passage du syst�me de coordonn�e entier vers double
 */

package com.dolphland.client.util.event;

import javax.swing.JComponent;

/**
 * Ev�nement d�livr� lorsqu'il faut masquer tous les VisionCtxInfo associ� � un
 * composant.<br>
 * Sur r�ception de cet �v�nement, le framework masquera tous les VisionCtxInfo
 * qui sotn rattach� au composant sp�cifi� dans le constructeur
 * 
 * @author JayJay
 */
public class UIHideCtxInfoEvt extends UIEvent {

    private volatile JComponent component;

    private volatile boolean fadeOut = false;



    /**
     * Construit l'�v�nement
     * 
     * @param comp
     *            Le composant dont ont veut masquer tous les VisionCtxInfo
     */
    public UIHideCtxInfoEvt(JComponent comp) {
        super(AppEvents.UI_HIDE_CTX_INFO_EVT);
        if (comp == null) {
            throw new NullPointerException("comp is null !"); //$NON-NLS-1$
        }
        this.component = comp;
    }



    public void setFadeOut(boolean shouldFadeOut) {
        this.fadeOut = shouldFadeOut;
    }



    public boolean shouldFadeOut() {
        return fadeOut;
    }



    /**
     * Retourne le composant vis� par l'�v�nement
     */
    public JComponent getComponent() {
        return component;
    }

}
