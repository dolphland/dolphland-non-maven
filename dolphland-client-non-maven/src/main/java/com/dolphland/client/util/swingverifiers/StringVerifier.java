package com.dolphland.client.util.swingverifiers;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.dolphland.client.util.i18n.MessageFormater;

/**
 * Verifier pouir les champs de saisie de chaines de caract�res
 * 
 * 
 * @author Bernard Noel
 */
public class StringVerifier extends AbstractInputVerifier {

    private static final MessageFormater FMT = MessageFormater.getFormater(StringVerifier.class);
    private int taille = 0;



    /**
     * Construit un StringVerfier
     * 
     * @param afficheur
     *            L'afficheur de messages � utiliser
     * @param taille
     *            Le nombre de caract�res maximum de la chaine de caract�res �
     *            saisir.
     */
    public StringVerifier(Afficheur afficheur, int taille) {
        super(afficheur);
        this.taille = taille;
    }



    public boolean verify(JComponent input) {
        if (input instanceof JTextField) {
            JTextField zone = (JTextField) input;

            // V�rification taille
            if (zone.getText().length() > taille) {
                afficherErreur(FMT.format("StringVerifier.RS_TAILLE_MAXI_DEPASSEE___0NUMBER_CARACTERES", taille));
                return false;
            }
        }
        getAfficheur().clear();
        return true;
    }
}
