package com.dolphland.client.util.conf;

import java.util.Map;
import java.util.TreeMap;

import com.dolphland.client.util.assertion.AssertUtil;

/**
 * <p>
 * Contains a map of application wide instances that have been initialized by
 * the framework.<br>
 * Applications can access this object to get {@link VlrUser} information for
 * example or check wether the super user mode is enabled or not
 * </p>
 * <p>
 * Default keys are exposed by this class as <code>public static final</code>
 * and are framework reserved. It is unwised to overwrite values bound to theses
 * key as they are reserved for the framework.
 * </p>
 * 
 * @author JayJay
 */
public final class AppContext {

    /** singleton */
    private static volatile AppContext instance = new AppContext();

    /** index of application wide context objects */
    private Map<String, Object> context;



    /**
     * Private constructor. Prevent instanciation.
     */
    private AppContext() {
        context = new TreeMap<String, Object>();
    }



    /**
     * <p>
     * Add or replace the <code>value</code> object indexed by the
     * <code>key</code> key to the context.
     * </p>
     * <p>
     * <li>If <code>key</code> already exists in the context, its value is
     * replaced with the specified <code>value</code> anbd the preceding value
     * is returned by this method.</li>
     * </p>
     * <p>
     * <li>If <code>key</code> does not exists in the context then it is simply
     * added with the specified value. Null is then returned by this method</li>
     * </p>
     * 
     * @param key
     *            The key of the object to be added. Cannot ben ull.
     * @param value
     * @return
     */
    public static final synchronized Object put(String key, Object value) {
        AssertUtil.notNull(key, "key cannot be null !"); //$NON-NLS-1$
        return instance.context.put(key, value);
    }



    /**
     * <p>
     * Get an object from the context by its key.
     * </p>
     * <p>
     * The returned object is autocasted from the assigning type of the caller.
     * ClassCastException might then be thrown is the assigner type does not
     * comply with the looked up object.
     * </p>
     * 
     * @param key
     *            The key of the object to get. Cannot be null.
     * @return The object bound to the specified key or null if none is bound.
     * @throws ClassCastException
     *             If the caller assigner type is not assignabled from the type
     *             of looked up object
     */
    @SuppressWarnings("unchecked")//$NON-NLS-1$
    public static final synchronized <T> T get(String key) {
        AssertUtil.notNull(key, "key cannot be null !"); //$NON-NLS-1$
        return (T) instance.context.get(key);
    }
}
