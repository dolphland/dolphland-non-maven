package com.dolphland.client.util.table;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

/**
 * <p>
 * Contains information on {@link ProductTableRow} sorting operation that should
 * be performed in a given context
 * </p>
 * 
 * @author JayJay
 */
public class ProductTableRowComparatorContext {

    public enum SortNature {
        /**
         * Sort in ascending order : that is in the natural order or
         * alphabetical order
         */
        ASCENT,
        /**
         * Sort in descending order : that is in the reversed natural order or
         * reversed alphabetical order
         */
        DESCENT
    };

    private List<ProductProperty> sortedProperties;

    private SortNature sortOrder;



    ProductTableRowComparatorContext(List<ProductProperty> sortedProperties, SortNature sortOrder) {
        Assert.notNull(sortedProperties, "sortedProperties cannot be null !");
        Assert.notNull(sortOrder, "sortOrder cannot be null !");
        if (sortedProperties.size() == 0) {
            throw new IllegalArgumentException("empty sorted properties !");
        }
        this.sortedProperties = new ArrayList<ProductProperty>(sortedProperties);
        this.sortOrder = sortOrder;
    }



    /**
     * <p>
     * Return the list of {@link ProductAdapter} properties that should used to
     * sort {@link ProductTableRow}s.
     * </p>
     * <p>
     * The returned list always contains at least 1 {@link ProductProperty}
     * </p>
     * <p>
     * Most of the time this list will contain only one element, in this case
     * the sort algorithm should rows on this single property basis
     * </p>
     * <p>
     * When this property list has more than one element, the sort algorithm
     * should sort rows the following way :<br>
     * <li>First, by the first property in the returned property list</li>
     * <li>Then, by the second property in the returned property list</li>
     * <li>Etc.</li>
     * </p>
     * 
     * @return The {@link ProductProperty} list that should be used to sort
     *         {@link ProductTableRow}s
     */
    public List<ProductProperty> getSortedProductProperties() {
        return sortedProperties;
    }



    /**
     * <p>
     * Return the {@link ProductProperty} that should be used to sort
     * {@link ProductTableRow}s.
     * </p>
     * <p>
     * This method will return the first {@link ProductProperty} returned by
     * {@link ProductTableRowComparatorContext#getSortedProductProperty()}
     * </p>
     * 
     * @return The {@link ProductProperty} that should be used to perform sort
     *         comparison on {@link ProductAdapter} so as to sort the
     *         {@link ProductTableRow}s
     */
    public ProductProperty getSortedProductProperty() {
        return getSortedProductProperties().get(0);
    }



    /**
     * <p>
     * Return the {@link SortNature} ascending or descending nature of the sort
     * operation to perform
     * </p>
     * 
     * @return The nature of sort operation to be performed
     */
    public SortNature getSortOrder() {
        return sortOrder;
    }
}
