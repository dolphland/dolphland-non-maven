package com.dolphland.client.util.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import org.apache.log4j.Logger;

import com.dolphland.client.util.i18n.MessageFormater;

class ProductTableModel extends DefaultTableModel {

    private final static Logger log = Logger.getLogger(ProductTableModel.class);

    private static final MessageFormater fmt = MessageFormater.getFormater(ProductTableModel.class);

    private Vector<ProductTableRow> rows;

    private ProductDescriptor productDescriptor;

    private ProductTableContentManager propertyFormater;



    public ProductTableModel(ProductDescriptor pDescriptor, ProductTableContentManager pFormater) {
        super();
        if (pDescriptor == null) {
            throw new NullPointerException("Please specify a not null ProductDescriptor !"); //$NON-NLS-1$
        }
        if (pFormater == null) {
            throw new NullPointerException("Please specify a not null PropertyFormater !"); //$NON-NLS-1$
        }
        this.productDescriptor = pDescriptor;
        this.propertyFormater = pFormater;
        this.rows = new Vector<ProductTableRow>();
    }



    public void setProducts(List<ProductTableRow> barres) {
        rows.clear();
        rows.addAll(barres);
        fireTableDataChanged();
    }



    public boolean isCellEditable(int row, int column) {
        List<ProductProperty> properties = productDescriptor.getProperties();
        if (row < getRowCount() && column < properties.size()) {
            ProductProperty pp = properties.get(column);
            ProductAdapter pa = getRowAt(row).getProduct();
            return propertyFormater.isCellEditable(pp, pa) && !propertyFormater.isCellDisabled(pp, pa);
        }

        return false;
    }



    public int getRowCount() {
        return rows == null ? 0 : rows.size();
    }



    @Override
    public void setValueAt(Object value, int row, int column) {
        List properties = productDescriptor.getProperties();
        if (column < properties.size()) {
            ProductProperty pp = productDescriptor.getProperties().get(column);
            ProductAdapter pa = getRowAt(row).getProduct();
            if (pa != null) {
                try {
                    pa.setValue(pp, value);
                } catch (NullPointerException nsme) {
                    log.error("setValueAt(): Property " + pp.getName() + " not found on ProductAdapter " + pa, nsme); //$NON-NLS-1$ //$NON-NLS-2$
                } catch (Exception e) {
                    log.error("setValueAt(): Error while accessing to the property " + pp.getName() + " from " + pa, e); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        }
    }



    public Object getValueAt(int row, int column) {
        List properties = productDescriptor.getProperties();
        if (column < properties.size()) {
            ProductProperty pp = productDescriptor.getPropertyAt(column);
            ProductAdapter pa = getRowAt(row).getProduct();
            if (pa == null) {
                return "---"; //$NON-NLS-1$
            }
            if (pp.getType() == ProductProperty.ACTION_TYPE) {
                return pa.getValue(pp);
            }
            try {
                return pa.getValue(pp).toString();
            } catch (NullPointerException nsme) {
                log.error("getValueAt(): Property " + pp.getName() + " not found on ProductAdapter " + pa, nsme); //$NON-NLS-1$ //$NON-NLS-2$
                return fmt.format("ProductTableModel.RS_MISSING_PROPERTY_DFT_DISPLAY"); //$NON-NLS-1$
            } catch (Exception e) {
                log.error("getValueAt(): Error while accessing to the property " + pp.getName() + " from " + pa, e); //$NON-NLS-1$ //$NON-NLS-2$
                return fmt.format("ProductTableModel.RS_PROPERTY_ERROR_DISPLAY"); //$NON-NLS-1$
            }
        }
        return null;
    }



    public ProductTableRow getRowAt(int rowIdx) {
        return (ProductTableRow) rows.get(rowIdx);
    }



    public List<ProductTableRow> getRows() {
        return new ArrayList<ProductTableRow>(rows);
    }



    public void sortRows(Comparator<ProductTableRow> comparator) {
        Collections.sort(rows, comparator);
        fireTableRowsUpdated(0, rows.size() - 1);
    }

}
