/*
 * $Log: SimpleListChooserDialog.java,v $
 * Revision 1.2 2013/04/19 14:50:55 bvatan
 * - FWK-54
 * Revision 1.1 2010/02/09 13:44:06 bvatan
 * - initial check in
 * Revision 1.2 2009/10/14 14:07:36 jscafi
 * - Am�liorations saisie num�ro OF
 * Revision 1.1 2009/05/27 07:07:57 jscafi
 * - Un OF ne contient plus 5 coul�es mais n coul�es
 * Revision 1.2 2009/03/16 12:06:15 jscafi
 * - Modifications Topaze
 * Revision 1.1 2008/12/01 11:59:23 bvatan
 * - initial check in
 * Revision 1.2 2008/10/24 12:24:02 cdelfly
 * Relooking des �crans avec l'ancien look topaze
 * Revision 1.1 2008/04/18 14:37:30 bvatan
 * - synchro avant CPs
 * Revision 1.1 2008/03/31 13:13:55 bvatan
 * - initial check in
 */

package com.dolphland.client.util.input;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;

import com.dolphland.client.util.dialogs.FwkDialog;

public class SimpleListChooserDialog<T> extends FwkDialog {

    private boolean canceled;

    private JList jList;

    private JScrollPane listScroller;

    private ListCellRenderer listItemRenderer;

    private List<T> selection;

    private JLabel labelConsigne;

    private SimpleListChooserKeyListener validationKeyEnterKeyListener;



    public SimpleListChooserDialog(Frame parent, ListCellRenderer r) {
        super(parent);
        if (r != null) {
            this.listItemRenderer = r;
        } else {
            this.listItemRenderer = new DefaultListCellRenderer();
        }
        init();
    }



    public SimpleListChooserDialog(Dialog parent, ListCellRenderer r) {
        super(parent);
        if (r != null) {
            this.listItemRenderer = r;
        } else {
            this.listItemRenderer = new DefaultListCellRenderer();
        }
        init();
    }



    private void init() {
        validationKeyEnterKeyListener = new SimpleListChooserKeyListener();
        selection = new ArrayList<T>();
        GridBagConstraints gc = new GridBagConstraints();
        gc.gridx = 0;
        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 0;
        gc.insets = new Insets(5, 5, 5, 5);
        gc.fill = GridBagConstraints.BOTH;
        GridBagConstraints gc00 = new GridBagConstraints();
        gc00.gridx = 0;
        gc00.gridy = 1;
        gc00.weightx = 1;
        gc00.weighty = 1;
        gc00.insets = new Insets(5, 5, 5, 5);
        gc00.fill = GridBagConstraints.BOTH;
        Container cp = getContentPane();
        // cp.setBackground(TopazeUI.PANEL_BACKGROUND);
        cp.setLayout(new GridBagLayout());
        cp.add(getLabelConsigne(), gc);
        cp.add(createListPanel(), gc00);
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                cancel();
            }
        });
    }



    public JLabel getLabelConsigne() {
        if (labelConsigne == null) {
            labelConsigne = new JLabel();
            labelConsigne.setHorizontalAlignment(JLabel.LEFT);
            /*
             * labelConsigne.setFont(VisionUIManager.UI_VISION_PANEL_FONT);
             * labelConsigne.setForeground(VisionUIManager.
             * UI_VISION_PANEL_FOREGROUND);
             * labelConsigne.setBackground(VisionUIManager.
             * UI_VISION_PANEL_BACKGROUND);
             */
        }
        return labelConsigne;
    }



    public void setConsigne(String consigne) {
        getLabelConsigne().setText(consigne);
    }



    private JPanel createListPanel() {
        JPanel out = new JPanel();
        out.setLayout(new GridBagLayout());
        GridBagConstraints gc00 = new GridBagConstraints();
        gc00.gridx = 0;
        gc00.gridy = 0;
        gc00.fill = GridBagConstraints.BOTH;
        gc00.weightx = 1;
        gc00.weighty = 1;
        out.add(createListScroller(), gc00);
        return out;
    }



    private JScrollPane createListScroller() {
        listScroller = new JScrollPane();
        listScroller.getViewport().setView(createJList());
        return listScroller;
    }



    private JList createJList() {
        jList = new JList();
        jList.addMouseListener(new SimpleListChooserMouseListener());
        jList.addMouseMotionListener(new SimpleListChooserMouseMotionListener());
        jList.setBackground(Color.WHITE);
        jList.setCellRenderer(listItemRenderer);
        jList.setModel(new DefaultListModel());
        jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jList.setFixedCellHeight(25);
        return jList;
    }



    public boolean wasCanceled() {
        return canceled;
    }



    private void ok() {
        canceled = false;
        dispose();
    }



    private void cancel() {
        canceled = true;
        dispose();
    }



    /**
     * Fixe les items possibles de la dialogue
     */
    public void setValues(List<T> values) {
        setValues(values, false);
    }



    /**
     * Fixe les items possibles de la dialogue
     * 
     * @param values
     *            Valeurs de la dialogue
     * @param validByKeyEnterEnabled
     *            Indique si la validation est possible par la touche Entr�e
     */
    public void setValues(List<T> values, boolean validByKeyEnterEnabled) {
        DefaultListModel model = (DefaultListModel) jList.getModel();
        model.removeAllElements();
        for (T t : values) {
            model.addElement(t);
        }

        jList.removeKeyListener(validationKeyEnterKeyListener);
        if (validByKeyEnterEnabled) {
            jList.addKeyListener(validationKeyEnterKeyListener);
            if (!values.isEmpty()) {
                jList.setSelectedIndex(0);
            }
        }
    }



    public List<T> getSelectedValues() {
        return new ArrayList<T>(selection);
    }



    public T getSelectedValue() {
        List<T> selection = getSelectedValues();
        if (selection.size() > 0) {
            return selection.get(0);
        } else {
            return null;
        }
    }

    private class SimpleListChooserMouseMotionListener extends MouseMotionAdapter {

        @Override
        public void mouseMoved(MouseEvent e) {
            jList.setSelectedIndex(jList.locationToIndex(e.getPoint()));
        }
    }

    private class SimpleListChooserMouseListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            selection = (List) Arrays.asList(jList.getSelectedValues());
            if (selection.size() == 1) {
                ok();
            }
        }
    }

    private class SimpleListChooserKeyListener extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                selection = (List) Arrays.asList(jList.getSelectedValues());
                if (selection.size() == 1) {
                    ok();
                }
            }
        }
    }
}
