package com.dolphland.client.util.application.components;

import java.awt.Color;
import java.awt.Font;

import javax.swing.UIManager;

/**
 * UIManager par d�faut pour les applications Vision
 * 
 * @author jeremy.scafi
 */
public class FwkUIManager extends UIManager{

	protected static final Color DEFAULT_BACKGROUND = Color.WHITE;
	protected static final Color DEFAULT_FOREGROUND = Color.BLACK;
	protected static final Color DEFAULT_FOREGROUND_TITLE = Color.WHITE;
	protected static final Font DEFAULT_FONT = new Font("Dialog", Font.BOLD, 14);

	// Panel
	protected static final Color PANEL_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Color PANEL_FOREGROUND = null;

	// ScrollPane
	protected static final Color SCROLLPANE_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Color SCROLLPANE_FOREGROUND = null;
	protected static final Font SCROLLPANE_FONT = DEFAULT_FONT;
	
	// Button
	protected static final Font BUTTON_FONT = DEFAULT_FONT;

	// TabbedPane
	protected static final Color TABBEDPANE_BACKGROUND = Color.LIGHT_GRAY;
	protected static final Color TABBEDPANE_FOREGROUND = null;
	protected static final Color TABBEDPANE_SELECTION_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Font TABBEDPANE_FONT = DEFAULT_FONT;

	// Table
	protected static final Color TABLE_BACKGROUND = Color.WHITE;
	protected static final Color TABLE_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Color TABLE_SELECTION_FOREGROUND = Color.BLACK;
	protected static final Font TABLE_FONT = DEFAULT_FONT;
	protected static final Color TABLE_HEADER_BACKGROUND = new Color(192, 192, 192);
	protected static final Color TABLE_HEADER_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font TABLE_HEADER_FONT = DEFAULT_FONT;

	// Tree
	protected static final Color TREE_BACKGROUND = Color.WHITE;
	protected static final Color TREE_FOREGROUND = null;
	protected static final Font TREE_FONT = DEFAULT_FONT;

	// List
	protected static final Color LIST_BACKGROUND = Color.WHITE;
	protected static final Color LIST_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Color LIST_SELECTION_FOREGROUND = Color.BLACK;
	protected static final Font LIST_FONT = DEFAULT_FONT;

	// TextField
	protected static final Color TEXTFIELD_BACKGROUND = Color.WHITE;
	protected static final Color TEXTFIELD_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font TEXTFIELD_FONT = DEFAULT_FONT;

	// TextArea
	protected static final Color TEXTAREA_BACKGROUND = Color.WHITE;
	protected static final Color TEXTAREA_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font TEXTAREA_FONT = DEFAULT_FONT;

	// Label
	protected static final Color LABEL_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Color LABEL_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font LABEL_FONT = DEFAULT_FONT;

	// RadioButton
	protected static final Color RADIOBUTTON_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Color RADIOBUTTON_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font RADIOBUTTON_FONT = DEFAULT_FONT;

	// CheckBox
	protected static final Color CHECKBOX_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Color CHECKBOX_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font CHECKBOX_FONT = DEFAULT_FONT;

	// ComboBox
	protected static final Color COMBOBOX_BACKGROUND = null;
	protected static final Color COMBOBOX_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font COMBOBOX_FONT = DEFAULT_FONT;

	// Dialog
	protected static final Color DIALOG_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Color DIALOG_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font DIALOG_FONT = DEFAULT_FONT;
	
	// OptionPane
	protected static final Color OPTIONPANE_BACKGROUND = DEFAULT_BACKGROUND;
	protected static final Color OPTIONPANE_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font OPTIONPANE_FONT = DEFAULT_FONT;

	// EditorPane
	protected static final Color EDITORPANE_BACKGROUND = Color.WHITE;
	protected static final Color EDITORPANE_FOREGROUND = DEFAULT_FOREGROUND;
	protected static final Font EDITORPANE_FONT = DEFAULT_FONT;

	// Chart
	protected static final Font CHART_FONT = new Font("Dialog", Font.BOLD, 12);  //$NON-NLS-1$

	/**
	 * Initialise l'UIManager
	 */
	public void initUIManager(){
		initUISwing();
		initUIDolph();
	}

	/**
	 * Initialisation des UI des classes Dolph
	 */
	protected void initUIDolph(){
	}

	/**
	 * Initialisation des UI du Swing par d�faut
	 */
	protected void initUISwing(){
		
		// Panel
		UIManager.put("Panel.background", PANEL_BACKGROUND);
		UIManager.put("Panel.foreground", PANEL_FOREGROUND);

		// ScrollPane
		UIManager.put("ScrollPane.background", SCROLLPANE_BACKGROUND);
		UIManager.put("ScrollPane.foreground", SCROLLPANE_FOREGROUND);
		UIManager.put("ScrollPane.font", SCROLLPANE_FONT);
		
		// Button
		UIManager.put("Button.font", BUTTON_FONT);

		// TabbedPane
		UIManager.put("TabbedPane.background", TABBEDPANE_BACKGROUND);
		UIManager.put("TabbedPane.foreground", TABBEDPANE_FOREGROUND);
		UIManager.put("TabbedPane.selected", TABBEDPANE_SELECTION_BACKGROUND);
		UIManager.put("TabbedPane.font", TABBEDPANE_FONT);

		// Table
		UIManager.put("Table.background", TABLE_BACKGROUND);
		UIManager.put("Table.foreground", TABLE_FOREGROUND);
		UIManager.put("Table.selectionForeground", TABLE_SELECTION_FOREGROUND);
		UIManager.put("Table.font", TABLE_FONT);

		UIManager.put("TableHeader.background", TABLE_HEADER_BACKGROUND);
		UIManager.put("TableHeader.foreground", TABLE_HEADER_FOREGROUND);
		UIManager.put("TableHeader.font", TABLE_HEADER_FONT);

		// Tree
		UIManager.put("Tree.background", TREE_BACKGROUND);
		UIManager.put("Tree.foreground", TREE_FOREGROUND);
		UIManager.put("Tree.font", TREE_FONT);

		// List
		UIManager.put("List.background", LIST_BACKGROUND);
		UIManager.put("List.foreground", LIST_FOREGROUND);
		UIManager.put("List.selectionForeground", LIST_SELECTION_FOREGROUND);
		UIManager.put("List.font", LIST_FONT);

		// TextField
		UIManager.put("TextField.background", TEXTFIELD_BACKGROUND);
		UIManager.put("TextField.foreground", TEXTFIELD_FOREGROUND);
		UIManager.put("TextField.font", TEXTFIELD_FONT);

		// TextArea
		UIManager.put("TextArea.background", TEXTAREA_BACKGROUND);
		UIManager.put("TextArea.foreground", TEXTAREA_FOREGROUND);
		UIManager.put("TextArea.font", TEXTAREA_FONT);

		// Label
		UIManager.put("Label.background", LABEL_BACKGROUND);
		UIManager.put("Label.foreground", LABEL_FOREGROUND);
		UIManager.put("Label.font", LABEL_FONT);

		// RadioButton
		UIManager.put("RadioButton.background", RADIOBUTTON_BACKGROUND);
		UIManager.put("RadioButton.foreground", RADIOBUTTON_FOREGROUND);
		UIManager.put("RadioButton.font", RADIOBUTTON_FONT);

		// CheckBox
		UIManager.put("CheckBox.background", CHECKBOX_BACKGROUND);
		UIManager.put("CheckBox.foreground", CHECKBOX_FOREGROUND);
		UIManager.put("CheckBox.font", CHECKBOX_FONT);

		// ComboBox
		UIManager.put("ComboBox.background", COMBOBOX_BACKGROUND);			
		UIManager.put("ComboBox.foreground", COMBOBOX_FOREGROUND);			
		UIManager.put("ComboBox.font", COMBOBOX_FONT);			
		
		// Dialog
		UIManager.put("Dialog.background", DIALOG_BACKGROUND);
		UIManager.put("Dialog.foreground", DIALOG_FOREGROUND);
		UIManager.put("Dialog.font", DIALOG_FONT);
		
		// OptionPane
		UIManager.put("OptionPane.background", OPTIONPANE_BACKGROUND);
		UIManager.put("OptionPane.foreground", OPTIONPANE_FOREGROUND);
		UIManager.put("OptionPane.font", OPTIONPANE_FONT);
		
		// EditorPane
		UIManager.put("EditorPane.background", EDITORPANE_BACKGROUND);
		UIManager.put("EditorPane.foreground", EDITORPANE_FOREGROUND);
		UIManager.put("EditorPane.font", EDITORPANE_FONT);
		
		// Chart
        UIManager.put("AbstractChart.font", CHART_FONT);
	}
}
