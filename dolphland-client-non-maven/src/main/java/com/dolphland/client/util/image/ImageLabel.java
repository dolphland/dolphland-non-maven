package com.dolphland.client.util.image;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Icon;

import com.dolphland.client.util.application.components.FwkLabel;

/**
 * Representation of an image's label
 * 
 * @author jeremy.scafi
 * 
 */
public class ImageLabel extends FwkLabel {

    /**
     * Default constructor with normal icon
     * 
     * @param normalIcon
     *            Normal icon
     */
    public ImageLabel(Icon normalIcon) {
        setIcon(normalIcon);
    }



    /**
     * Constructor with normal and hovered icons
     * 
     * @param hoveredIcon
     *            Hovered icon
     */
    public ImageLabel(final Icon normalIcon, final Icon hoveredIcon) {
        this(normalIcon);
        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                setIcon(hoveredIcon);
            }



            @Override
            public void mouseExited(MouseEvent e) {
                setIcon(normalIcon);
            }
        });

    }

}
