package com.dolphland.client.util.application;

import javax.swing.JPanel;

/**
 * Classe d�finissant une view rattach�e � un view listener
 * 
 * @author jeremy.scafi
 */
public abstract class View extends JPanel {

    // private OverlayManager overlayManager = new OverlayManager(false);

    /** Le listener d'�v�nement en relation avec la partie m�tier */
    private ViewListener viewListener;



    public View() {
        super();
    }



    public ViewListener getViewListener() {
        return viewListener;
    }



    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }



    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        // overlay only if component is disabled (make it unaccessible)
        // overlayManager.setOverlayEnabled(this, !enabled);
    }

}
