package com.dolphland.client.util.chart;

import java.awt.Paint;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.CategoryItemEntity;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * Cette classe permet de repr�senter un chart de type CategoryPlot avec une
 * l�gende am�lior�e.
 * 
 * La principale am�lioration de cette l�gende est de pouvoir d�cocher certaines
 * s�ries pour ne pas les voir appara�tre sur le chart.
 * 
 * La repr�sentation du Chart se fait dans le JComponent componentChart que l'on
 * peut obtenir par getChartComponent()
 * 
 * Possibilit� d'y d�finir :
 * - des entit�s de donn�es. Une entit� de donn�e est une s�rie identifi�e par
 * un Comparable, constitu�e de valeurs (identit� valeur, ordonn�e)
 * 
 * @author jeremy.scafi
 */
abstract class AbstractCategoryPlotChart extends AbstractChart {

    private DefaultCategoryDataset dataset;

    // Indique si le premier auto pack sur l'axe des Y a �t� r�alis�
    private boolean firstAutoPackRealise;



    /**
     * Constructeur
     */
    public AbstractCategoryPlotChart() {
        this(new JPanel(), new ChartConstraints());
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AbstractCategoryPlotChart(ChartConstraints chartConstraints) {
        this(new JPanel(), chartConstraints);
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AbstractCategoryPlotChart(JPanel componentChart) {
        this(componentChart, new ChartConstraints());
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param componentChart
     *            : Composant devant repr�senter le Chart
     * @param chartConstraints
     *            : Contraintes � appliquer au Chart
     */
    public AbstractCategoryPlotChart(JPanel componentChart, ChartConstraints chartConstraints) {
        super(componentChart, chartConstraints);
    }



    /**
     * Initialise les donn�es du chart
     */
    public void initChart() {
        // Indique que le premier auto pack sur l'axe des Y n'a pas �t� r�alis�
        firstAutoPackRealise = false;

        // Stocke toutes les keys des rows et des cols (pour �viter une erreur
        // de modif concurrentielle)
        List keyRows = new ArrayList();
        Iterator itRowKeys = getDataset().getRowKeys().iterator();
        while (itRowKeys.hasNext()) {
            keyRows.add((Comparable) itRowKeys.next());
        }

        List keyCols = new ArrayList();
        Iterator itColKeys = getDataset().getColumnKeys().iterator();
        while (itColKeys.hasNext()) {
            keyCols.add((Comparable) itColKeys.next());
        }

        // Enl�ve toutes les s�ries
        for (int row = 0; row < keyRows.size(); row++) {
            for (int col = 0; col < keyCols.size(); col++) {
                dataset.removeValue((Comparable) keyRows.get(row), (Comparable) keyCols.get(col));
            }
        }

        super.initChart();
    }



    /**
     * Renvoie la liste des items de la l�gende
     */
    protected LegendItem[] getLegendItems() {
        CategoryPlot plot = getChart().getCategoryPlot();
        CategoryItemRenderer renderer = plot.getRenderer();

        LegendItem[] legendItems = new LegendItem[renderer.getLegendItems().getItemCount()];
        for (int i = 0; i < legendItems.length; i++) {
            legendItems[i] = renderer.getLegendItems().get(i);
        }

        return legendItems;
    }



    /**
     * Cr�e l'impl�mentation du listener permettant de modifier les contraintes
     * du graphe en temps r�el
     */
    protected ChartConstraintsListener createChartConstraintsListenerImpl() {
        return new CategoryPlotChartConstraintsListenerImpl();
    }



    /**
     * Ajoute une valeur identifi�e par idValue de valeur value pour l'entit� de
     * donn�es identifi�e par idChartData
     */
    public void addData(Comparable idChartData, Comparable idValue, double value) {
        // On r�cup�re l'entit� des donn�es identifi�e par idChartData
        BarChartData chartData = (BarChartData) getChartData(idChartData);

        // Si elle n'existe pas, on la cr�e et on l'ajoute
        if (chartData == null) {
            chartData = new BarChartData(idChartData);
        }

        // On y ajoute les donn�es pass�es en param�tre
        chartData.addValue(idValue, new Double(value));

        putChartData(chartData);
    }



    /** Fournit le DataSet du chart */
    protected CategoryDataset getDataset() {
        if (dataset == null) {
            dataset = new DefaultCategoryDataset();
        }
        return dataset;
    }

    /**
     * Impl�mentation du listener permettant de modifier les contraintes du
     * graphe en temps r�el
     * 
     * @author jeremy.scafi
     */
    private class CategoryPlotChartConstraintsListenerImpl extends DefaultChartConstraintsListenerImpl {

        /** Fixe le titre de l'axe X du chart */
        @Override
        public void doChangeAxeXTitle() {
            getChart().getCategoryPlot().getDomainAxis().setLabel(getChartConstraints().getAxeXTitle());
            super.doChangeAxeXTitle();
        }



        /** Fixe le titre de l'axe Y du chart */
        @Override
        public void doChangeAxeYTitle() {
            getChart().getCategoryPlot().getRangeAxis().setLabel(getChartConstraints().getAxeYTitle());
            super.doChangeAxeYTitle();
        }



        /** Fixe la visibilit� de l'axe X du chart */
        @Override
        public void doChangeAxeXVisible() {
            getChart().getCategoryPlot().getDomainAxis().setVisible(getChartConstraints().isAxeXVisible());
            super.doChangeAxeXVisible();
        }



        /** Fixe la visibilit� de l'axe Y du chart */
        @Override
        public void doChangeAxeYVisible() {
            getChart().getCategoryPlot().getRangeAxis().setVisible(getChartConstraints().isAxeYVisible());
            super.doChangeAxeYVisible();
        }



        /** Indique le min et le max du range de l'axe Y */
        @Override
        public void doChangeAxeYRange() {
            // Fixe le range de l'axe Y si d�finit dans les contraintes
            if (getChartConstraints().getAxeYRangeMin() != null && getChartConstraints().getAxeYRangeMax() != null)
                getChart().getCategoryPlot().getRangeAxis().setRange(getChartConstraints().getAxeYRangeMin().doubleValue(), getChartConstraints().getAxeYRangeMax().doubleValue());
            else
                getChart().getCategoryPlot().getRangeAxis().setAutoRange(true);
            super.doChangeAxeYRange();
        }



        /** Fixe le pas entre chaque valeur de l'axe des ordonn�es */
        @Override
        public void doChangeAxeYPas() {
            CategoryPlot plot = getChart().getCategoryPlot();
            // Fixe le pas de l'axe des ordonn�es si indiqu�
            if (getChartConstraints().getAxeYPas() != null) {
                if (plot.getRangeAxis() instanceof NumberAxis) {
                    ((NumberAxis) plot.getRangeAxis()).setTickUnit(new NumberTickUnit(getChartConstraints().getAxeYPas().doubleValue()));
                }
            }
            super.doChangeAxeYPas();
        }



        /** Fixe les couleurs des donn�es du chart */
        @Override
        public void doChangeColorsChartData() {
            CategoryItemRenderer renderer = getChart().getCategoryPlot().getRenderer();
            if (getChartConstraints().getColorsChartData() != null) {
                for (int i = 0; i < getChartConstraints().getColorsChartData().length; i++) {
                    renderer.setSeriesPaint(i, getChartConstraints().getColorsChartData()[i]);
                }
            }
            super.doChangeColorsChartData();
        }
    }

    /**
     * Entit� de donn�es propre � un BarChart
     * 
     * @author jeremy.scafi
     */
    private class BarChartData extends ChartData {

        private List idValues;
        private List values;



        public BarChartData(Comparable idChartData) {
            super(idChartData, getChartDataCount());
            this.idValues = new ArrayList();
            this.values = new ArrayList();
        }



        public void addValue(Comparable idValue, Number value) {
            // Si l'idValue existe d�j�, on met � jour
            if (idValues.contains(idValue)) {
                values.set(idValues.indexOf(idValue), value);
                dataset.setValue(value, getIdChartData(), idValue);
            }
            // Si l'idValue n'existe pas, on cr�e
            else {
                idValues.add(idValue);
                values.add(value);

                dataset.addValue(value, getIdChartData(), idValue);
            }

            // Si le range de l'axe des Y est auto packable
            if (getChartConstraints().isAxeYRangeAutoPackable()) {
                ValueAxis rangeY = getChart().getCategoryPlot().getRangeAxis();

                // Si range jamais initialis� par l'auto pack, on le d�limite
                // sur l'ordonn�e
                if (!firstAutoPackRealise) {
                    rangeY.setRange(value.doubleValue() - getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue(), value.doubleValue() + getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue());
                    firstAutoPackRealise = true;
                }
                // Sinon, on fait en sorte que l'ordonn�e soit dans le range
                else {
                    rangeY.setRange(Math.min(rangeY.getLowerBound(), value.doubleValue() - getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue()), Math.max(rangeY.getUpperBound(), value.doubleValue() + getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue()));
                }
            }

            // Si la chartdata n'est pas visible, on cache la valeur ajout�e
            if (!isChartDataVisible(getIdChartData())) {
                double ordAxeAbscisses = Math.max(0, getChart().getCategoryPlot().getRangeAxis().getRange().getLowerBound());
                dataset.setValue(new Double(ordAxeAbscisses), getIdChartData(), idValue);
            }
        }



        protected void hideAllValues() {
            double ordAxeAbscisses = Math.max(0, getChart().getCategoryPlot().getRangeAxis().getRange().getLowerBound());

            // Change les ordonn�es de la barre pour qu'elle se confonde avec
            // l'axe des abscisses
            for (int i = 0; i < idValues.size(); i++) {
                Comparable idValue = (Comparable) idValues.get(i);
                dataset.setValue(new Double(ordAxeAbscisses), getIdChartData(), idValue);
            }
        }



        protected void showAllValues() {
            // Change les ordonn�es de la barre pour restituer ses ordonn�es
            for (int i = 0; i < idValues.size(); i++) {
                Comparable idValue = (Comparable) idValues.get(i);
                Number value = (Number) values.get(i);
                dataset.setValue(value, getIdChartData(), idValue);
            }
        }
    }

    /**
     * Renderer pour g�rer la s�lection d'une barre dans le graphe
     * 
     * @author JayJay
     * 
     */
    protected class BarSelectionRenderer extends BarRenderer3D implements ChartSelection {

        private Integer selectedSeries = null;
        private Integer selectedData = null;
        private CategoryDataset dataSet = null;



        public BarSelectionRenderer(CategoryDataset dataSet) {
            super();
            this.dataSet = dataSet;
        }



        public boolean isSelectionEnabled(ChartEntity entity) {
            if (entity instanceof CategoryItemEntity) {
                return ((CategoryItemEntity) entity).getDataset().equals(dataSet);
            }
            return false;
        }



        public boolean isSelectionActive() {
            return selectedSeries != null && selectedData != null;
        }



        public Integer getSelectedSeries() {
            return selectedSeries;
        }



        public Integer getSelectedData() {
            return selectedData;
        }



        public void setSelectedSeries(Integer indSeries) {
            this.selectedSeries = indSeries;
        }



        public void setSelectedData(Integer indData) {
            this.selectedData = indData;
        }



        public void clearSelection() {
            this.selectedSeries = null;
            this.selectedData = null;
        }



        private boolean isSelected(int series, int item) {
            if (!isSelectionActive()) {
                return false;
            }
            return series == selectedSeries && item == selectedData;
        }



        @Override
        public Paint getItemPaint(int row, int column) {
            if (isSelected(row, column)) {
                return getChartConstraints().getSelectionColor();
            }
            return super.getItemPaint(row, column);
        }
    }
}
