package com.dolphland.client.util.application.startup;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.components.FwkSplashScreen;
import com.dolphland.client.util.conf.CConf;
import com.dolphland.client.util.conf.CEnvironement;
import com.dolphland.client.util.conf.CProperty;
import com.dolphland.client.util.conf.FwkBoot;
import com.dolphland.client.util.conf.FwkBootStep;
import com.dolphland.client.util.exception.AppException;
import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.i18n.MessageFormater;

/**
 * <p>
 * {@link FwkBootStep} to be added top {@link FwkBoot} when application is
 * starting.
 * </p>
 * <p>
 * This boot step is responsible for displaying a splash screen according to the
 * application configuration.
 * </p>
 * <p>
 * If the application defines that splash is enabled, then this boot step is
 * responsible for making it visible at boot time.
 * </p>
 * <p>
 * To enable splash screen, a <code>fwk.ui.splashscreen</code> configuration has to be
 * present in configuration, e.g.:<br>
 * 
 * <pre>
 *  &lt;conf id="fwk"&gt;
 *      &lt;conf id="ui"&gt;
 *          &lt;conf id="splashscreen"&gt;
 *              &lt;property name="splashEnabled" value="true" /&gt;
 *          &lt;/conf&gt;
 *      &lt;/conf&gt;
 *  &lt;/conf&gt;
 * </pre>
 * 
 * </p>
 * <p>
 * When no <code>fwk.ui.splashscreen</code> configuration is found, splash screen is
 * considered not to be shown.
 * </p>
 * 
 * @author JayJay
 */
public class SplashScreenBootStep extends FwkBootStep {

    private final static Logger log = Logger.getLogger(SplashScreenBootStep.class);

    private static final MessageFormater FMT = MessageFormater.getFormater(SplashScreenBootStep.class);

    private static final String CONF_SPLASHSCREEN = "fwk.ui.splashscreen"; //$NON-NLS-1$

    private static final String PROP_SPLASH_ENABLE = "splashEnabled"; //$NON-NLS-1$

    private static final String RS_MESSAGE_CLASSPATH = "splashscreen/messages"; //$NON-NLS-1$

    private MasterFrameBootStep masterFrameStep;

    private FwkSplashScreen splash;



    public SplashScreenBootStep(MasterFrameBootStep masterFrameStep) {
        super(FMT.format("SplashScreenBootStep.RS_NAME"));
        Assert.notNull(masterFrameStep, "masterFrameStep cannot be null !"); //$NON-NLS-1$
        this.masterFrameStep = masterFrameStep;
    }



    @Override
    protected void doExecute(FwkBoot booter) {
        CEnvironement env = booter.getEnvironment();
        CConf conf = null;
        try {
            conf = env.getNestedConf(CONF_SPLASHSCREEN);
        } catch (AppException e) {
            log.warn("doExecute(): No " + CONF_SPLASHSCREEN + " found in configuration, assume no splash screen is required."); //$NON-NLS-1$ //$NON-NLS-2$
            conf = null;
        }
        if (conf == null) {
            return;
        }
        CProperty splashEnabledProperty = conf.getProperty(PROP_SPLASH_ENABLE);
        if (splashEnabledProperty == null) {
            return;
        }
        if (!splashEnabledProperty.getBoolean()) {
            return;
        }
        final MessageFormater SPLASH_FMT = MessageFormater.getFormater(RS_MESSAGE_CLASSPATH);        
        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                public void run() {
                    MasterFrame masterFrame = masterFrameStep.getMasterFrame();
                    if (masterFrame == null) {
                        log.warn("doExecute(): No master frame available. No splash will display."); //$NON-NLS-1$
                    } else {
                        showSplash(SPLASH_FMT);
                    }
                }
            });
        } catch (InterruptedException ie) {
            return;
        } catch (InvocationTargetException ite) {
            throw new AppInfrastructureException(SPLASH_FMT.format("SplashScreenBootStep.RS_SPLASH_ERROR"), ite.getCause()); //$NON-NLS-1$
        }
    }



    /**
     * <p>
     * Show the splash screen using the specified {@link MessageFormater} for
     * message internationalization
     * </p>
     * <p>
     * <b><font style="color:red">AWT event dispath thread required</font></b>
     * </p>
     * @param fmt
     *            The mesage formater to be used for internationalizing
     *            messages.
     */
    private void showSplash(MessageFormater fmt) {
        splash = new FwkSplashScreen();
        //splash.setTitle(fmt.format("SplashScreenBootStep.RS_TITLE")); //$NON-NLS-1$
        //splash.setInfos(fmt.format("SplashScreenBootStep.RS_WAITING_LOADING")); //$NON-NLS-1$
        splash.setAlwaysOnTop(false);
        splash.setVisible(true);
    }



    /**
     * </p>
     * Make the splash dispear if it is enabled and visible.
     * </p>
     * <p>
     * If no splash screen exist, does nothing.
     * </p>
     */
    public void disposeSplash() {
        if (splash != null) {
            splash.dispose();
            splash = null;
        }
    }



    /**
     * <p>
     * Get the splash screen that this instance is displaying or null if no
     * splash screen is enabled.
     * </p>
     * 
     * @return The splash screen or null if none
     */
    public FwkSplashScreen getSplashScreen() {
        return splash;
    }
}
