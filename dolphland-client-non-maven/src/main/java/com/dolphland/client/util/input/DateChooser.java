package com.dolphland.client.util.input;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import com.toedter.calendar.JCalendar;

public class DateChooser extends JPanel implements ActionListener, PropertyChangeListener {

    protected DateTextField dateTextField;

    protected JButton calendarButton;

    protected JCalendar jcalendar;

    protected JPopupMenu popup;

    protected boolean dateSelected;



    /**
     * Creates a new JDateChooser.
     * 
     * @param jcal
     *            the JCalendar to be used
     * @param date
     *            the date or null
     * @param dateFormatString
     *            the date format string or null (then MEDIUM Date format is
     *            used)
     * @param dateEditor
     *            the dateEditor to be used used to display the date. if null, a
     *            JTextFieldDateEditor is used.
     */
    public DateChooser(DateTextField dateEditor) {
        setName("DateChooser2"); //$NON-NLS-1$

        this.dateTextField = dateEditor;
        if (this.dateTextField == null) {
            this.dateTextField = new DateTextField();
        }
        this.dateTextField.addPropertyChangeListener("date", this); //$NON-NLS-1$

        setLayout(new BorderLayout());

        add(getCalendarButton(), BorderLayout.EAST);
        add(getDateTextField(), BorderLayout.CENTER);
    }



    /**
     * Called when the jalendar button was pressed.
     * 
     * @param e
     *            the action event
     */
    public void actionPerformed(ActionEvent e) {
        int x = getCalendarButton().getWidth() - (int) getPopup().getPreferredSize().getWidth();
        int y = getCalendarButton().getY() + getCalendarButton().getHeight();

        Calendar calendar = Calendar.getInstance();
        Date date = getDateTextField().getDate();
        if (date != null) {
            calendar.setTime(date);
        }
        getJCalendar().setCalendar(calendar);
        getPopup().show(getCalendarButton(), x, y);
        dateSelected = false;
    }



    /**
     * Listens for a "date" property change or a "day" property change event
     * from the JCalendar. Updates the date editor and closes the popup.
     * 
     * @param evt
     *            the event
     */
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("day")) { //$NON-NLS-1$
            if (getPopup().isVisible()) {
                dateSelected = true;
                getPopup().setVisible(false);
                setDate(getJCalendar().getCalendar().getTime());
            }
        } else if (evt.getPropertyName().equals("date")) { //$NON-NLS-1$
            if (evt.getSource() == getDateTextField()) {
                firePropertyChange("date", evt.getOldValue(), evt.getNewValue()); //$NON-NLS-1$
            } else {
                setDate((Date) evt.getNewValue());
            }
        }
    }



    /**
     * Returns the date. If the JDateChooser is started with a null date and no
     * date was set by the user, null is returned.
     * 
     * @return the current date
     */
    public Date getDate() {
        return getDateTextField().getDate();
    }



    /**
     * Sets the date. Fires the property change "date" if date != null.
     * 
     * @param date
     *            the new date.
     */
    public void setDate(Date date) {
        getDateTextField().setDate(date);
        if (getParent() != null) {
            getParent().invalidate();
        }
    }



    /**
     * Returns the calendar. If the JDateChooser is started with a null date (or
     * null calendar) and no date was set by the user, null is returned.
     * 
     * @return the current calendar
     */
    public Calendar getCalendar() {
        Date date = getDate();
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }



    /**
     * Sets the calendar. Value null will set the null date on the date editor.
     * 
     * @param calendar
     *            the calendar.
     */
    public void setCalendar(Calendar calendar) {
        if (calendar == null) {
            getDateTextField().setDate(null);
        } else {
            getDateTextField().setDate(calendar.getTime());
        }
    }



    /**
     * Enable or disable the JDateChooser.
     * 
     * @param enabled
     *            the new enabled value
     */
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        getDateTextField().setEnabled(enabled);
        getCalendarButton().setEnabled(enabled);
    }



    /**
     * Returns true, if enabled.
     * 
     * @return true, if enabled.
     */
    public boolean isEnabled() {
        return super.isEnabled();
    }



    /**
     * Sets the icon of the buuton.
     * 
     * @param icon
     *            The new icon
     */
    public void setIcon(ImageIcon icon) {
        getCalendarButton().setIcon(icon);
    }



    /**
     * Returns the JCalendar component. THis is usefull if you want to set some
     * properties.
     * 
     * @return the JCalendar
     */
    public JCalendar getJCalendar() {
        if (jcalendar == null) {
            Date date = new Date();
            jcalendar = new JCalendar(date);
            jcalendar.getDayChooser().addPropertyChangeListener("day", this); //$NON-NLS-1$
            // always fire"day" property even if the user selects
            // the already selected day again
            jcalendar.getDayChooser().setAlwaysFireDayProperty(true);
            setDate(date);
        }
        return jcalendar;
    }



    /**
     * Returns the calendar button.
     * 
     * @return the calendar button
     */
    public JButton getCalendarButton() {
        if (calendarButton == null) {
            // Display a calendar button with an icon
            URL iconURL = getJCalendar().getClass().getResource("images/" + getJCalendar().getName() + "Color16.gif"); //$NON-NLS-1$ //$NON-NLS-2$
            ImageIcon icon = new ImageIcon(iconURL);

            calendarButton = new JButton(icon) {

                private static final long serialVersionUID = -1913767779079949668L;



                public boolean isFocusable() {
                    return false;
                }
            };
            calendarButton.setMargin(new Insets(0, 0, 0, 0));
            calendarButton.setBackground(Color.orange);
            calendarButton.addActionListener(this);

            // Alt + 'C' selects the calendar.
            calendarButton.setMnemonic(KeyEvent.VK_C);
        }
        return calendarButton;
    }



    /**
     * Returns the date editor.
     * 
     * @return the date editor
     */
    public DateTextField getDateTextField() {
        if (this.dateTextField == null) {
            this.dateTextField = new DateTextField();
        }
        return dateTextField;
    }



    public JPopupMenu getPopup() {
        if (popup == null) {
            popup = new JPopupMenu() {

                private static final long serialVersionUID = -6078272560337577761L;



                public void setVisible(boolean b) {
                    Boolean isCanceled = (Boolean) getClientProperty("JPopupMenu.firePopupMenuCanceled"); //$NON-NLS-1$
                    if (b || (!b && dateSelected) || ((isCanceled != null) && !b && isCanceled.booleanValue())) {
                        super.setVisible(b);
                    }
                }
            };

            popup.setLightWeightPopupEnabled(true);

            popup.add(getJCalendar());
        }
        return popup;
    }
}
