package com.dolphland.client.util.table;

import java.util.Comparator;

/**
 * <p>
 * Provides a default comparator for {@link ProductTableRow} based on a single
 * {@link ProductProperty} of a {@link ProductAdapter}.
 * </p>
 * <p>
 * Sort ordering can be specified as <code>ASCENT_ORDER</code> (natural ascent
 * order) or <code>DESCENT_ORDER</code> (natural descent order)
 * </p>
 * 
 * @author JayJay
 */
public class DefaultProductRowComparator implements Comparator<ProductTableRow> {

    private ProductProperty productProperty;
    private boolean sortAscent;



    public DefaultProductRowComparator(ProductProperty pp, boolean sortAscentOrder) {
        this.productProperty = pp;
        this.sortAscent = sortAscentOrder;
    }



    public int compare(ProductTableRow o1, ProductTableRow o2) {
        ProductAdapter pa1 = o1.getProduct();
        ProductAdapter pa2 = o2.getProduct();
        int out;
        if (pa1 == pa2) {
            out = 0;
        } else if (pa1 == null || pa2 == null) {
            if (pa1 == null) {
                out = -1;
            } else {
                out = 1;
            }
        } else {
            Object v1 = pa1.getValue(productProperty);
            Object v2 = pa2.getValue(productProperty);
            if (v1 == v2) {
                out = 0;
            } else if (v1 == null || v2 == null) {
                if (v1 == null) {
                    out = -1;
                } else {
                    out = 1;
                }
            } else {
                if (v1 instanceof Comparable<?> && v2 instanceof Comparable<?>) {
                    Comparable<Object> c1 = (Comparable<Object>) v1;
                    Comparable<Object> c2 = (Comparable<Object>) v2;
                    out = c1.compareTo(c2);
                } else {
                    out = v1.toString().compareTo(v2.toString());
                }
            }
        }
        return out * (sortAscent ? 1 : -1);
    }

}
