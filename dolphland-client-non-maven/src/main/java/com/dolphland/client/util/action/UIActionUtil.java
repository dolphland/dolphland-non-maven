package com.dolphland.client.util.action;

import java.awt.Component;
import java.awt.Font;
import java.util.Iterator;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import org.apache.log4j.Logger;

/**
 * Classe utilitaire pour la gestion des op�rations communes sur les UIAction
 * 
 * @author JayJay
 */
public class UIActionUtil {

    private final static Logger LOG = Logger.getLogger(UIActionUtil.class);



    /**
     * Construit un popup menu � partir de la liste d'actions sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * @return Le popup menu pr�t � �tre affich�
     * 
     * @deprecated Use method that takes an {@link ActionsContext} as parameter
     */
    public static <P extends UIActionEvt> JPopupMenu createPopupMenuFromActions(List<? extends UIAction<P, ?>> actions, P event) {
        return updatePopupMenuFromActions(actions, event, null);
    }



    /**
     * <p>
     * Create a new popup menu from the specified list of {@link UIAction}. <br>
     * When an action is triggered, the specified {@link UIActionEvt} is passed
     * to that action.
     * </p>
     * <p>
     * Beware that only action that owns the {@link UIAction#TRIGGER_CONTEXT}
     * trigger modifier will be added to the menu. Others will be ignored.
     * 
     * @param <P>
     *            {@link UIActionEvt} type to be passed to {@link UIAction}
     *            instances when they are triggered
     * @param actions
     *            {@link UIAction} list to be used for popup menu items creation
     * @param actionsContext
     *            The {@link ActionsContext} instance that will handle
     *            {@link UIAction} bindings
     * @return Le popup menu pr�t � �tre affich�
     */
    public static <P extends UIActionEvt> JPopupMenu createPopupMenuFromActions(List<? extends UIAction<P, ?>> actions, ActionsContext actionsContext) {
        return updatePopupMenuFromActions(actions, actionsContext, null);
    }



    /**
     * <p>
     * Create a new menu bar from the specified list of {@link UIAction}. <br>
     * When an action is triggered, the specified {@link UIActionEvt} is passed
     * to that action.
     * </p>
     * <p>
     * Beware that only action that owns the {@link UIAction#TRIGGER_CONTEXT}
     * trigger modifier will be added to the menu bar. Others will be ignored.
     * 
     * @param <P>
     *            {@link UIActionEvt} type to be passed to {@link UIAction}
     *            instances when they are triggered
     * @param actions
     *            {@link UIAction} list to be used for popup menu items creation
     * @param actionsContext
     *            The {@link ActionsContext} instance that will handle
     *            {@link UIAction} bindings
     * @return Le popup menu pr�t � �tre affich�
     */
    public static <P extends UIActionEvt> JMenuBar createMenuBarFromActions(List<? extends UIAction<P, ?>> actions, P event) {
        return updateMenuBarFromActions(actions, event, null);
    }



    /**
     * <p>
     * Build a menu bar from the specified {@link UIAction} list. When an action
     * is triggered from the created menu bar, the specified {@link UIActionEvt}
     * is passed to this action.
     * </p>
     * <p>
     * Beware that only actions that own the {@link UIAction#TRIGGER_CONTEXT}
     * will be handled, others will be ignored and not included in the menu bar.
     * </p>
     * 
     * @param actions
     *            The action list to be used to create the menu bar
     * @param actionsContext
     *            The {@link ActionsContext} instance that will handle action
     *            bindings
     * @return The menu bar build from the specified list of {@link UIAction}
     */
    public static <P extends UIActionEvt> JMenuBar createMenuBarFromActions(List<? extends UIAction<P, ?>> actions, ActionsContext actionsContext) {
        return updateMenuBarFromActions(actions, actionsContext, null);
    }



    /**
     * Construit un menu � partir de la liste d'actions sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * @return Le menu pr�t � �tre affich�
     * 
     * @deprecated Use the method with the actions context specified
     */
    public static <P extends UIActionEvt> JMenu createMenuFromActions(List<? extends UIAction<P, ?>> actions, P event) {
        return updateMenuFromActions(actions, event, null);
    }



    /**
     * Construit un menu � partir de la liste d'actions sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param actionsContext
     *            actions context
     * @return Le menu pr�t � �tre affich�
     */
    public static <P extends UIActionEvt> JMenu createMenuFromActions(List<? extends UIAction<P, ?>> actions, ActionsContext actionsContext) {
        return updateMenuFromActions(actions, actionsContext, null);
    }



    /**
     * Met � jour un popup menu pass� en param�tres � partir de la liste
     * d'actions sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * @param popup
     *            Le popup menu � mettre � jour (si null, un nouveau popup menu
     *            est instanci�)
     * @return Le popup menu pr�t � �tre affich�
     * 
     * @deprecated Use method that takes an {@link ActionsContext} as parameter
     */
    public static <P extends UIActionEvt> JPopupMenu updatePopupMenuFromActions(List<? extends UIAction<P, ?>> actions, P event, JPopupMenu popup) {
        ActionsContext actionsContext = new ActionsContext(createUIActionEvtFactory(event));
        return updatePopupMenuFromActions(actions, actionsContext, popup);
    }



    /**
     * <p>
     * Update the specified popup menu items with the specified list of
     * {@link UIAction}. <br>
     * When an action is triggered, the specified {@link UIActionEvt} is passed
     * to that action.
     * </p>
     * <p>
     * Beware that only action that owns the {@link UIAction#TRIGGER_CONTEXT}
     * trigger modifier will be added to the menu. Others will be ignored
     * 
     * @param <P>
     *            {@link UIActionEvt} type to be passed to {@link UIAction}
     *            instances when they are triggered
     * @param actions
     *            {@link UIAction} list to be used for popup menu items creation
     * @param actionsContext
     *            The {@link ActionsContext} instance that will handle
     *            {@link UIAction} bindings
     * @return The popup menu ready to be displayed
     */
    public static <P extends UIActionEvt> JPopupMenu updatePopupMenuFromActions(List<? extends UIAction<P, ?>> actions, final ActionsContext actionsContext, JPopupMenu popup) {
        if (popup == null) {
            popup = new JPopupMenu();
        }
        if (actionsContext == null) {
            return popup;
        }
        Iterator<UIAction<P, ?>> itActions = (Iterator<UIAction<P, ?>>) actions.iterator();
        while (itActions.hasNext()) {
            final UIAction<P, ?> action = itActions.next();
            // Si l'action est un groupe d'actions, ajout des actions
            if (action instanceof UIActionGroup) {
                UIActionGroup actionGroup = (UIActionGroup) action;
                // Si le groupe d'actions n'est pas visible, on ne l'ajoute pas
                if (!isActionVisible(actionGroup, actionsContext.getUIActionEvtFactory(actionGroup))) {
                    continue;
                }
                boolean enabled = isActionEnabled(actionGroup, actionsContext.getUIActionEvtFactory(actionGroup));
                // Si groupe d'action r�duit, on ajoute un sous menu contenant
                // les actions du groupe
                if (actionGroup.isCollapsed()) {
                    JMenu subMenu = new JMenu(actionGroup.getName());
                    subMenu.setEnabled(enabled);
                    updateMenuFromActions((List<? extends UIAction<P, ?>>) actionGroup.getActions(), actionsContext, subMenu);
                    popup.add(subMenu);
                }
                // Si groupe d'actions non r�duit, on ajoute les items de menu
                // de chaque action avec ajout d'un s�parateur si n�cessaire
                else {
                    // On ajoute un s�parateur si n�cessaire
                    if (popup.getComponentCount() > 0 && !(popup.getComponent(popup.getComponentCount() - 1) instanceof JPopupMenu.Separator)) {
                        popup.addSeparator();
                    }

                    updatePopupMenuFromActions((List<? extends UIAction<P, ?>>) actionGroup.getActions(), actionsContext, popup);

                    // On ajoute un s�parateur si n�cessaire
                    if (!isLastVisibleAction(actions, action, actionsContext.getUIActionEvtFactory(action))) {
                        popup.addSeparator();
                    }
                }
            }
            // Si l'action n'est pas un groupe d'actions, ajout de l'action
            else {
                final JMenuItem jmi = createMenuItemAction(action, actionsContext);
                action.addUIActionListener(new UIActionAdapter() {

                    @Override
                    public void afterExecution() {
                        action.uninstall(jmi);
                        actionsContext.unbind(action, jmi);
                        action.removeUIActionListener(this);
                    }
                });
                if (jmi != null) {
                    popup.add(jmi);
                }
            }
        }
        if (popup.getComponentCount() > 0) {
            return popup;
        } else {
            return null;
        }
    }



    /**
     * Met � jour une barre de menu pass�e en param�tres � partir de la liste
     * d'actions sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * @param menuBar
     *            La barre de menu � mettre � jour (si null, une nouvelle barre
     *            de menu est instanci�e)
     * @return La barre de menu pr�te � �tre affich�e
     * 
     * @deprecated Use the method with the actions context specified
     */
    public static <P extends UIActionEvt> JMenuBar updateMenuBarFromActions(List<? extends UIAction<P, ?>> actions, P event, JMenuBar menuBar) {
        ActionsContext actionsContext = new ActionsContext(createUIActionEvtFactory(event));
        return updateMenuBarFromActions(actions, actionsContext, menuBar);
    }



    /**
     * Met � jour une barre de menu pass�e en param�tres � partir de la liste
     * d'actions sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param actionsContext
     *            actions context
     * @param menuBar
     *            La barre de menu � mettre � jour (si null, une nouvelle barre
     *            de menu est instanci�e)
     * @return La barre de menu pr�te � �tre affich�e
     */
    public static <P extends UIActionEvt> JMenuBar updateMenuBarFromActions(List<? extends UIAction<P, ?>> actions, ActionsContext actionsContext, JMenuBar menuBar) {
        if (menuBar == null) {
            menuBar = new JMenuBar();
        }
        if (actionsContext == null) {
            return menuBar;
        }
        Iterator<UIAction<P, ?>> itActions = (Iterator<UIAction<P, ?>>) actions.iterator();
        while (itActions.hasNext()) {
            final UIAction<P, ?> action = itActions.next();
            // Si l'action est un groupe d'actions, ajout des actions
            if (action instanceof UIActionGroup) {
                UIActionGroup actionGroup = (UIActionGroup) action;
                // Si le groupe d'actions n'est pas visible, on ne l'ajoute pas
                if (!isActionVisible(actionGroup, actionsContext.getUIActionEvtFactory(actionGroup))) {
                    continue;
                }
                boolean enabled = isActionEnabled(actionGroup, actionsContext.getUIActionEvtFactory(actionGroup));
                // Si groupe d'action r�duit, on ajoute un sous menu contenant
                // les actions du groupe
                if (actionGroup.isCollapsed()) {
                    JMenu subMenu = new JMenu(actionGroup.getName());
                    subMenu.setEnabled(enabled);
                    updateMenuFromActions((List<? extends UIAction<P, ?>>) actionGroup.getActions(), actionsContext, subMenu);
                    menuBar.add(subMenu);
                }
                // Si groupe d'actions non r�duit, on ajoute les items de menu
                // de chaque action avec ajout d'un s�parateur si n�cessaire
                else {
                    // On ajoute un s�parateur si n�cessaire
                    if (menuBar.getComponentCount() > 0 && !(menuBar.getComponent(menuBar.getComponentCount() - 1) instanceof JSeparator)) {
                        menuBar.add(new JSeparator());
                    }

                    updateMenuBarFromActions((List<? extends UIAction<P, ?>>) actionGroup.getActions(), actionsContext, menuBar);

                    // On ajoute un s�parateur si n�cessaire
                    if (!isLastVisibleAction(actions, action, actionsContext.getUIActionEvtFactory(action))) {
                        menuBar.add(new JSeparator());
                    }
                }
            }
            // Si l'action n'est pas un groupe d'actions, ajout de l'action
            else {
                final JMenuItem jmi = createMenuItemAction(action, actionsContext);
                if (jmi != null) {
                    menuBar.add(jmi);
                }
            }
        }
        if (menuBar.getComponentCount() > 0) {
            return menuBar;
        } else {
            return null;
        }
    }



    /**
     * Met � jour un menu pass� en param�tres � partir de la liste d'actions
     * sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * @param menu
     *            Le menu � mettre � jour (si null, un nouveau menu est
     *            instanci�)
     * @return Le menu pr�t � �tre affich�
     * 
     * @deprecated Use the method with the actions context specified
     */
    public static <P extends UIActionEvt> JMenu updateMenuFromActions(List<? extends UIAction<P, ?>> actions, P event, JMenu menu) {
        ActionsContext actionsContext = new ActionsContext(createUIActionEvtFactory(event));
        return updateMenuFromActions(actions, actionsContext, menu);
    }



    /**
     * Met � jour un menu pass� en param�tres � partir de la liste d'actions
     * sp�cifi�e et de
     * l'�v�nement � transmettre aux actions si lors de leur d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param actionsContext
     *            actions context
     * @param menu
     *            Le menu � mettre � jour (si null, un nouveau menu est
     *            instanci�)
     * @return Le menu pr�t � �tre affich�
     */
    public static <P extends UIActionEvt> JMenu updateMenuFromActions(List<? extends UIAction<P, ?>> actions, ActionsContext actionsContext, JMenu menu) {
        if (menu == null) {
            menu = new JMenu();
        }
        if (actionsContext == null) {
            return menu;
        }
        Iterator<UIAction<P, ?>> itActions = (Iterator<UIAction<P, ?>>) actions.iterator();
        while (itActions.hasNext()) {
            final UIAction<P, ?> action = itActions.next();
            // Si l'action est un groupe d'actions, ajout des actions
            if (action instanceof UIActionGroup) {
                UIActionGroup actionGroup = (UIActionGroup) action;
                // Si le groupe d'actions n'est pas visible, on ne l'ajoute pas
                if (!isActionVisible(actionGroup, actionsContext.getUIActionEvtFactory(actionGroup))) {
                    continue;
                }
                boolean enabled = isActionEnabled(actionGroup, actionsContext.getUIActionEvtFactory(actionGroup));
                // Si groupe d'action r�duit, on ajoute un sous menu contenant
                // les actions du groupe
                if (actionGroup.isCollapsed()) {
                    JMenu subMenu = new JMenu(actionGroup.getName());
                    subMenu.setEnabled(enabled);
                    updateMenuFromActions((List<? extends UIAction<P, ?>>) actionGroup.getActions(), actionsContext, subMenu);
                    menu.add(subMenu);
                }
                // Si groupe d'actions non r�duit, on ajoute les items de menu
                // de chaque action avec ajout d'un s�parateur si n�cessaire
                else {
                    // On ajoute un s�parateur si n�cessaire
                    if (menu.getMenuComponentCount() > 0 && !(menu.getMenuComponent(menu.getMenuComponentCount() - 1) instanceof JPopupMenu.Separator)) {
                        menu.addSeparator();
                    }

                    updateMenuFromActions((List<? extends UIAction<P, ?>>) actionGroup.getActions(), actionsContext, menu);

                    // On ajoute un s�parateur si n�cessaire
                    if (!isLastVisibleAction(actions, action, actionsContext.getUIActionEvtFactory(action))) {
                        menu.addSeparator();
                    }
                }
            }
            // Si l'action n'est pas un groupe d'actions, ajout de l'action
            else {
                final JMenuItem jmi = createMenuItemAction(action, actionsContext);
                if (jmi != null) {
                    menu.add(jmi);
                }
            }
        }
        if (menu.getMenuComponentCount() > 0) {
            return menu;
        } else {
            return null;
        }
    }



    /**
     * Ex�cute des actions pour un trigger donn� avec pour param�tre d'ex�cution
     * l'�v�nement pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param actions
     *            Liste des actions � ex�cuter
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * @param comp
     *            Composant � installer
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void executeActions(List<UIAction<P, ?>> actions, P event, int trigger) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        executeActions(actions, actionEvtFactory, trigger);
    }



    /**
     * Ex�cute des actions pour un trigger donn� avec pour param�tre d'ex�cution
     * l'�v�nement pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param actions
     *            Liste des actions � ex�cuter
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     * @param comp
     *            Composant � installer
     */
    public static <P extends UIActionEvt> void executeActions(List<UIAction<P, ?>> actions, UIActionEvtFactory<P> actionEvtFactory, int trigger) {
        for (UIAction<P, ?> action : actions) {
            if (action instanceof UIAction) {
                if (action.isTriggerEnabled(trigger)) {
                    executeAction(action, actionEvtFactory);
                }
            } else if (action instanceof UIActionGroup) {
                executeActions(((UIActionGroup<P, ?>) action).getActions(), actionEvtFactory, trigger);
            }
        }
    }



    /**
     * Ex�cute une action pour param�tre d'ex�cution l'�v�nement pass� en
     * param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param action
     *            Action � ex�cuter
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void executeAction(UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        if (action == null) {
            return;
        }
        updateActionEnabled(action, actionEvtFactory);
        P event = actionEvtFactory.createUIActionEvt();
        if (action.isEnabled() && action.isEnabled(event)) {
            action.execute(event);
        }
    }



    /**
     * Ex�cute les actions pour param�tre d'ex�cution l'�v�nement pass� en
     * param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param action
     *            Action � ex�cuter
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void executeActions(List<UIAction<P, ?>> actions, UIActionEvtFactory<P> actionEvtFactory) {
        if (actions == null || actions.isEmpty()) {
            return;
        }
        for (UIAction<P, ?> action : actions) {
            executeAction(action, actionEvtFactory);
        }
    }



    /**
     * <p>
     * Installs the specified action to the specified component.
     * </p>
     * 
     * @param action
     *            The action whose association is to be made with the
     *            specified component. When null, this method does nothing and
     *            returns silently.
     * @param event
     *            The view state to be sent to the action for testing its
     *            visible and enabled states. Can be null.
     * @param comp
     *            The component to be associated to the specified action.
     * 
     * @deprecated Use bind in {@link VisionView} or {@link ActionsContext}
     */
    public static <P extends UIActionEvt> void installAction(UIAction<P, ?> action, P event, Component comp) {
        if (action == null) {
            return;
        }
        if (comp != null) {
            action.install(comp);
        }
        updateActionEnabled(action, event);
        updateActionVisible(action, event);
    }



    /**
     * <p>
     * Uninstall the specified action from the specified component.
     * </p>
     * 
     * @param action
     *            The action whose association is to be removed from the
     *            specified component. When null, this method does nothing and
     *            returns silently.
     * @param event
     *            The view state at uninstallation time.
     * @param comp
     *            The component that the action is to be uninstalled from. (ie.
     *            the component on which the action was previously installed)
     * 
     * @deprecated Use unbind in {@link VisionView} or {@link ActionsContext}
     */
    public static <P extends UIActionEvt> void uninstallAction(UIAction<P, ?> action, P event, Component comp) {
        if (action == null) {
            return;
        }
        if (comp != null) {
            action.uninstall(comp);
        }
        updateActionState(action, event);
    }



    /**
     * <p>
     * Updates the specified action state (enabled and visible) giving the
     * provided {@link UIActionEvt} view state.
     * </p>
     * 
     * @param action
     *            The action whoses state is to be updated. When null, this
     *            methods does nothing and returns silently.
     * @param event
     *            The view state to be passed to the specified action so that it
     *            can provide it enabled and visible states
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionState(UIAction<P, ?> action, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        updateActionState(action, actionEvtFactory);
    }



    /**
     * <p>
     * Updates the specified action state (enabled and visible) giving the
     * provided {@link UIActionEvt} view state.
     * </p>
     * 
     * @param action
     *            The action whoses state is to be updated. When null, this
     *            methods does nothing and returns silently.
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionState(UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        if (action == null) {
            return;
        }
        updateActionContext(action, actionEvtFactory);
        updateActionVisible(action, actionEvtFactory);
        updateActionEnabled(action, actionEvtFactory);
    }



    /**
     * <p>
     * Update action states for all the actions specified in the provided list.
     * </p>
     * 
     * @param actions
     *            The actions whose states are to be updated.
     * @param event
     *            The view state to be used for updating action states.
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionsStates(List<? extends UIAction<P, ?>> actions, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        updateActionsStates(actions, actionEvtFactory);
    }



    /**
     * <p>
     * Update action states for all the actions specified in the provided list.
     * </p>
     * 
     * @param actions
     *            The actions whose states are to be updated.
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionsStates(List<? extends UIAction<P, ?>> actions, UIActionEvtFactory<P> actionEvtFactory) {
        for (UIAction<P, ?> action : actions) {
            updateActionState(action, actionEvtFactory);
        }
    }



    /**
     * Mise � jour de la visibilit� d'une liste d'actions en fonction d'un
     * �v�nement pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions
     * @param event
     *            L'�v�nement � transmettre aux actions
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionsVisible(List<? extends UIAction<P, ?>> actions, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        updateActionsVisible(actions, actionEvtFactory);
    }



    /**
     * Mise � jour de la visibilit� d'une liste d'actions en fonction d'un
     * �v�nement pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionsVisible(List<? extends UIAction<P, ?>> actions, UIActionEvtFactory<P> actionEvtFactory) {
        for (UIAction<P, ?> action : actions) {
            updateActionVisible(action, actionEvtFactory);
        }
    }



    /**
     * <p>
     * Call the {@link UIAction#contextChanged(UIActionEvt)} method on the
     * specified action.
     * </p>
     * 
     * @param action
     *            The action whose contextChanged() method is to be called.
     *            Cannot be null.
     * @param event
     *            The view state to be passed to the contextChanged() method.
     *            When null, this method does nothing and contextChanged() is
     *            not called on the action
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionContext(UIAction<P, ?> action, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        updateActionContext(action, actionEvtFactory);
    }



    /**
     * <p>
     * Call the {@link UIAction#contextChanged(UIActionEvt)} method on the
     * specified action.
     * </p>
     * 
     * @param action
     *            The action whose contextChanged() method is to be called.
     *            Cannot be null.
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionContext(UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        if (actionEvtFactory != null) {
            action.contextChanged(actionEvtFactory.createUIActionEvt());
        }
    }



    /**
     * Mise � jour de la visibilit� d'une action en fonction d'un �v�nement
     * pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param action
     *            Actions � int�grer dans un item de menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionVisible(UIAction<P, ?> action, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        updateActionVisible(action, actionEvtFactory);
    }



    /**
     * Mise � jour de la visibilit� d'une action en fonction d'un �v�nement
     * pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param action
     *            Actions � int�grer dans un item de menu
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionVisible(UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        action.updateVisible();
        boolean visible = isActionVisible(action, actionEvtFactory);
        for (Component comp : action.getComponents()) {
            comp.setVisible(visible);
        }

        // For each action listener, prevent that visible has changed
        for (UIActionListener listener : action.getUIActionListeners()) {
            listener.visibleChanged(visible);
        }
    }



    /**
     * Mise � jour de l'activation d'une liste d'actions en fonction d'un
     * �v�nement pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions
     * @param event
     *            L'�v�nement � transmettre aux actions
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionsEnabled(List<? extends UIAction<P, ?>> actions, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        updateActionsEnabled(actions, actionEvtFactory);
    }



    /**
     * Mise � jour de l'activation d'une liste d'actions en fonction d'un
     * �v�nement pass� en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� aux actions
     * @param actions
     *            Liste des actions
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionsEnabled(List<? extends UIAction<P, ?>> actions, UIActionEvtFactory<P> actionEvtFactory) {
        for (UIAction<P, ?> action : actions) {
            updateActionEnabled(action, actionEvtFactory);
        }
    }



    /**
     * Mise � jour de l'activation d'une action en fonction d'un �v�nement pass�
     * en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param action
     *            Actions � int�grer dans un item de menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionEnabled(UIAction<P, ?> action, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        updateActionEnabled(action, actionEvtFactory);
    }



    /**
     * Mise � jour de l'activation d'une action en fonction d'un �v�nement pass�
     * en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param action
     *            Actions � int�grer dans un item de menu
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     */
    public static <P extends UIActionEvt> void updateActionEnabled(UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        action.updateEnabled();
        boolean enabled = isActionEnabled(action, actionEvtFactory);
        for (Component comp : action.getComponents()) {
            comp.setEnabled(enabled);
        }

        // For each action listener, prevent that enabled has changed
        for (UIActionListener listener : action.getUIActionListeners()) {
            listener.enabledChanged(enabled);
        }
    }



    /**
     * <p>
     * Tests the visibility state a the specified action with the given view
     * state (event parameter).
     * </p>
     * <p>
     * When event parameter is null, the action visibility state is tested with
     * the {@link UIAction#isVisible()} method only.
     * </p>
     * 
     * @param action
     *            The action whose visibility state is to be tested
     * @param event
     *            The view state to be sent to the specified action. If null
     *            then only the {@link UIAction#isVisible()} will be invoked on
     *            the action.
     * @return The visibility state that the action has returned
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> boolean isActionVisible(UIAction<P, ?> action, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        return isActionVisible(action, actionEvtFactory);
    }



    /**
     * <p>
     * Tests the visibility state a the specified action with the given view
     * state (event parameter).
     * </p>
     * <p>
     * When event parameter is null, the action visibility state is tested with
     * the {@link UIAction#isVisible()} method only.
     * </p>
     * 
     * @param action
     *            The action whose visibility state is to be tested
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     * @return The visibility state that the action has returned
     */
    public static <P extends UIActionEvt> boolean isActionVisible(UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        boolean visible = false;
        if (actionEvtFactory == null) {
            return visible;
        }
        P event = actionEvtFactory.createUIActionEvt();
        try {
            visible = action.isVisible();
        } catch (Exception e) {
            LOG.error("isActionVisible(): UIAction.isVisible() has thrown an exception !", e); //$NON-NLS-1$
        }
        if (visible && event != null) {
            try {
                visible = action.isVisible(event);
            } catch (Exception e) {
                LOG.error("isActionVisible(): UIAction.isVisible(UIActionEvent) has thrown an exception !", e); //$NON-NLS-1$
            }
        }
        return visible;
    }



    /**
     * <p>
     * Tests the visibility state a the specified action with the given view
     * state (event parameter).
     * </p>
     * <p>
     * When event parameter is null, the action visibility state is tested with
     * the {@link UIAction#isVisible()} method only.
     * </p>
     * 
     * @param actions
     *            The actions whose visibility state is to be tested
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     * @return The visibility state that the action has returned
     */
    public static <P extends UIActionEvt> boolean isActionVisible(List<UIAction<P, ?>> actions, UIActionEvtFactory<P> actionEvtFactory) {
        for (UIAction<P, ?> action : actions) {
            if (!isActionVisible(action, actionEvtFactory)) {
                return false;
            }
        }
        return true;
    }



    /**
     * <p>
     * Tests the enabled state a the specified action with the given view state
     * (event parameter).
     * </p>
     * <p>
     * When event parameter is null, the action enabled statee is tested with
     * the {@link UIAction#isEnabled()} method only.
     * </p>
     * 
     * @param action
     *            The action whose enabled state is to be tested
     * @param event
     *            The view state to be sent to the specified action. If null
     *            then only the {@link UIAction#isEnabled()} will be invoked on
     *            the action.
     * @return The enabled state that the action has returned
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    public static <P extends UIActionEvt> boolean isActionEnabled(UIAction<P, ?> action, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        return isActionEnabled(action, actionEvtFactory);
    }



    /**
     * <p>
     * Tests the enabled state a the specified action with the given view state
     * (event parameter).
     * </p>
     * <p>
     * When event parameter is null, the action enabled statee is tested with
     * the {@link UIAction#isEnabled()} method only.
     * </p>
     * 
     * @param action
     *            The action whose enabled state is to be tested
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     * @return The enabled state that the action has returned
     */
    public static <P extends UIActionEvt> boolean isActionEnabled(UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        boolean enabled = false;
        if (actionEvtFactory == null) {
            return enabled;
        }
        P event = actionEvtFactory.createUIActionEvt();
        try {
            enabled = action.isEnabled();
        } catch (Exception e) {
            LOG.error("isActionEnabled(): UIAction.isEnabled() has thrown an exception !", e); //$NON-NLS-1$
        }
        if (enabled && event != null) {
            try {
                enabled = action.isEnabled(event);
            } catch (Exception e) {
                LOG.error("isActionEnabled(): UIAction.isEnabled(UIActionEvent) has thrown an exception !", e); //$NON-NLS-1$
            }
        }
        return enabled;
    }



    /**
     * <p>
     * Tests the enabled state a the specified action with the given view state
     * (event parameter).
     * </p>
     * <p>
     * When event parameter is null, the action enabled statee is tested with
     * the {@link UIAction#isEnabled()} method only.
     * </p>
     * 
     * @param actions
     *            The actions whose enabled state is to be tested
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     * @return The enabled state that the action has returned
     */
    public static <P extends UIActionEvt> boolean isActionsEnabled(List<UIAction<P, ?>> actions, UIActionEvtFactory<P> actionEvtFactory) {
        for (UIAction<P, ?> action : actions) {
            if (!isActionEnabled(action, actionEvtFactory)) {
                return false;
            }
        }
        return true;
    }



    /**
     * Construit un item de menu � partir d'une action sp�cifi�e et de
     * l'�v�nement � transmettre � l'action si lors de son d�clenchement.<br>
     * <br>
     * Attention, seules les action poss�dant le d�clencheur TRIGGER_CONTEXT
     * seront trait�es, les autres seront ignor�es.
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param action
     *            Actions � int�grer dans un item de menu
     * @param actionsContext
     *            actions context
     * @return L'item de menu pr�t � �tre affich�
     */
    private static <P extends UIActionEvt> JMenuItem createMenuItemAction(final UIAction<P, ?> action, ActionsContext actionsContext) {
        if (!action.isTriggerEnabled(UIAction.TRIGGER_CONTEXT)) {
            return null;
        }
        if (!isActionVisible(action, actionsContext.getUIActionEvtFactory(action))) {
            return null;
        }
        boolean enabled = isActionEnabled(action, actionsContext.getUIActionEvtFactory(action));
        JMenuItem jmi = new JMenuItem();
        actionsContext.bind(action, jmi);
        jmi.setEnabled(enabled);
        try {
            action.contextChanged(actionsContext.getUIActionEvtFactory(action).createUIActionEvt());
        } catch (Exception e) {
            LOG.error("updateMenuBarFromActions(): UIAction.contextChanged() has thrown an exception !", e); //$NON-NLS-1$
        }
        // jmi.addActionListener(new ActionListener() {
        //
        // public void actionPerformed(ActionEvent e) {
        // try {
        // action.execute(event);
        // } catch (Exception ex) {
        // LOG.error("actionPerformed(): UIAction.execute() a lev� une exception !",
        // ex);
        // }
        // }
        // });

        Font fontMenuItem = jmi.getFont();
        int style = Font.PLAIN;
        // Si action lan�able au double clic, on met l'item de menu en gras
        if (action.isTriggerEnabled(UIAction.TRIGGER_DBCLICK)) {
            style = Font.BOLD;
        }
        jmi.setFont(new Font(fontMenuItem.getName(), style, fontMenuItem.getSize()));

        return jmi;
    }



    /**
     * Indique si l'action pass�e en param�tre est la derni�re action visible de
     * la liste d'actions pass�e en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param action
     *            Actions � int�grer dans un item de menu
     * @param event
     *            L'�v�nement � transmettre � l'action s�lectionn�e
     * @return Vrai si l'action est la derni�re action visible de la liste (Faux
     *         sinon)
     * 
     * @deprecated Use the method with the factory for UIActionEvt
     */
    private static <P extends UIActionEvt> boolean isLastVisibleAction(List<? extends UIAction<P, ?>> actions, UIAction<P, ?> action, P event) {
        UIActionEvtFactory<P> actionEvtFactory = createUIActionEvtFactory(event);
        return isLastVisibleAction(actions, action, actionEvtFactory);
    }



    /**
     * Indique si l'action pass�e en param�tre est la derni�re action visible de
     * la liste d'actions pass�e en param�tre
     * 
     * @param <P>
     *            Type de l'�v�nement distribu� � l'action
     * @param actions
     *            Liste des actions � int�grer au popup menu
     * @param action
     *            Actions � int�grer dans un item de menu
     * @param actionEvtFactory
     *            Factory for UIActionEvt
     * @return Vrai si l'action est la derni�re action visible de la liste (Faux
     *         sinon)
     */
    private static <P extends UIActionEvt> boolean isLastVisibleAction(List<? extends UIAction<P, ?>> actions, UIAction<P, ?> action, UIActionEvtFactory<P> actionEvtFactory) {
        boolean actionFounded = false;
        for (UIAction<P, ?> anAction : actions) {
            if (anAction == action) {
                actionFounded = true;
                continue;
            }
            if (actionFounded) {
                if (isActionVisible(anAction, actionEvtFactory)) {
                    return false;
                }
            } else {
                if (anAction instanceof UIActionGroup) {
                    if (!isLastVisibleAction(((UIActionGroup<P, ?>) anAction).getActions(), anAction, actionEvtFactory)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }



    /**
     * Create factory for UIActionEvt to always use event specified
     * 
     * @param event
     *            Event
     * 
     * @return Factory for UIActionEvt
     */
    private static <P extends UIActionEvt> UIActionEvtFactory<P> createUIActionEvtFactory(final P event) {
        UIActionEvtFactory<P> actionEvtFactory = new UIActionEvtFactory<P>() {

            public P createUIActionEvt() {
                return event;
            }
        };
        return actionEvtFactory;
    }
}
