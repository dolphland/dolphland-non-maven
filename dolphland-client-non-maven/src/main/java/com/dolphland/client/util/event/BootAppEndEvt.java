/*
 * $Log: BootAppEndEvt.java,v $
 * Revision 1.2 2013/09/11 07:31:23 bvatan
 * - added javadoc
 * Revision 1.1 2008/11/28 10:05:28 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

import com.dolphland.client.util.conf.FwkBoot;
import com.dolphland.client.util.conf.FwkBootStep;

/**
 * <p>
 * Delivered by {@link FwkBoot} when boot sequence has just been completed.<br>
 * This means all boot sequence is done and that any extra {@link FwkBootStep}
 * that might have been added to the boot sequence have been executed.
 * </p>
 * 
 * @author JayJay
 */
public class BootAppEndEvt extends BootAppEvt {

    /**
     * <p>
     * Default constructor that initialize this event to be delivered to the
     * {@link FwkEvents#BOOT_APP_END_EVT} destination.
     * </p>
     */
    public BootAppEndEvt() {
        super(FwkEvents.BOOT_APP_END_EVT);
    }

}
