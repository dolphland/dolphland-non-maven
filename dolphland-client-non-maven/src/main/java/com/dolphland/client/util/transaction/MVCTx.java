package com.dolphland.client.util.transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * Classe de base pour la gestion des changements de threads dans les
 * transactions Swing.<br>
 * Cette classe permet d'effectuer les changement de threads n�cessaires au
 * respect des bonnes pratiques concernant l'utilisation des threads dans un
 * environnement Swing.
 * 
 * @author JayJay
 */
public class MVCTx<P, T> extends SwingAction<P, T> {

    private final static Logger log = Logger.getLogger(MVCMultiTx.class);

    private List<MVCTxListener> listeners = new ArrayList<MVCTxListener>();

    private static enum ExecMode {
        SYNC, ASYNC
    }

    private static enum DispatchService {
        BEFORE_EXECUTION, AFTER_EXECUTION
    }

    static long seqId;

    static final ThreadPoolExecutor poolMonoThread = new ThreadPoolExecutor(1,
        1,
        60,
        TimeUnit.SECONDS,
        new LinkedBlockingQueue<Runnable>(),
        new ThreadFactory() {

            public Thread newThread(Runnable r) {
                Thread thread = null;
                synchronized (MVCTx.class) {
                    thread = new Thread(r, "MVCSingleTx"); //$NON-NLS-1$
                }
                thread.setDaemon(true);
                return thread;
            }
        });

    static final ThreadPoolExecutor poolMultiThread = new ThreadPoolExecutor(5,
        10,
        60,
        TimeUnit.SECONDS,
        new LinkedBlockingQueue<Runnable>(),
        new ThreadFactory() {

            public Thread newThread(Runnable r) {
                Thread thread = null;
                synchronized (MVCTx.class) {
                    thread = new Thread(r, "MVCMultiTx-" + (seqId++)); //$NON-NLS-1$
                }
                thread.setDaemon(true);
                return thread;
            }
        });

    static {
        poolMonoThread.prestartAllCoreThreads();
        poolMultiThread.prestartAllCoreThreads();
    }

    private List<Thread> activeThreads = Collections.synchronizedList(new ArrayList<Thread>());



    /**
     * Add transaction listener
     * 
     * @param listener
     *            Transaction listener
     */
    public synchronized void addListener(MVCTxListener listener) {
        if (listener == null) {
            return;
        }
        listeners.add(listener);
    }



    /**
     * Remove transaction listener
     * 
     * @param listener
     *            Transaction listener
     */
    public synchronized void removeListener(MVCTxListener listener) {
        if (listener == null) {
            return;
        }
        listeners.remove(listener);
    }



    /**
     * Get transaction listeners
     * 
     * @return Transaction listeners
     */
    public synchronized MVCTxListener[] getListeners() {
        MVCTxListener[] result = new MVCTxListener[listeners.size()];
        listeners.toArray(result);
        return result;
    }



    private void dispatch(final DispatchService service) {
        SwingJob<Void> job = new SwingJob<Void>() {

            @Override
            protected Void run() {
                List<MVCTxListener> listeners = new ArrayList<MVCTxListener>(MVCTx.this.listeners);
                switch (service) {
                    case BEFORE_EXECUTION : {
                        for (MVCTxListener l : listeners) {
                            try {
                                l.beforeExecution();
                            } catch (Exception e) {
                                log.error("dispatch(): beforeExecution() has failed !", e); //$NON-NLS-1$
                            }
                        }
                        break;
                    }
                    case AFTER_EXECUTION : {
                        for (MVCTxListener l : listeners) {
                            try {
                                l.afterExecution();
                            } catch (Exception e) {
                                log.error("dispatch(): afterExecution() has failed !", e); //$NON-NLS-1$
                            }
                        }
                        break;
                    }
                }
                return null;
            }
        };
        job.invokeAndWait();
    }



    public void cancel() {
        synchronized (activeThreads) {
            for (Thread t : activeThreads) {
                t.interrupt();
            }
        }
    }



    /**
     * Service appell� juste avant preExecute(). Ce service retourne false si
     * l'ex�cution de la transaction doit �tre annul�e. Si la transaction doit
     * continuer � s'ex�cuter normalement, ce service doit retourner true.<br>
     * Si ce service retourne false, les services preExecute(), doExecute(),
     * updateViewSuccess(), updateViewError() et postExecute() ne seront pas
     * appel�s.
     * 
     * @param param
     *            Le param�tre transmis � execute()
     * @return true si l'action doit s'ex�cuter, false sinon.
     */
    protected boolean shouldExecute(P param) {
        return true;
    }



    /**
     * Service appell� juste avant doExecute() dans le fil du thread AWT
     */
    protected void preExecute(P param) {
    }



    /**
     * Service appell� juste apr�s postExecute(param) dans le fil du thread AWT.
     */
    protected void postExecute() {
    }



    /**
     * Service appell� juste apr�s updateViewSuccess() et
     * updateViewError() mais juste avant postExecute() dans le fil du thread
     * AWT.
     * 
     * @param param�tre
     *            transmis � la transaction dans execute(param).
     */
    protected void postExecute(P param) {

    }



    /**
     * M�thode d'ex�cution de la transaction, contient les traitements qui
     * d�marrent la transaction.
     * 
     * @param param
     *            Param�tre � transmettre � doExecute()
     */
    protected T doExecute(P param) {
        return null;
    }



    /**
     * M�thode appell�e dans le cas o� la transaction s'est d�roul�e sans
     * erreur.
     * 
     * @param data
     *            Objet retourn� par doExecute()
     */
    protected void updateViewSuccess(T data) {
    }



    /**
     * Methode appell�e dans le cas o� une exception a �t� lev�e par
     * doExecute(). L'exception lev�e est pass�e en param�tre.
     * 
     * @param ex
     *            Exception lev�e par doExecute()
     */
    protected void updateViewError(Exception ex) {
    }



    ThreadPoolExecutor getThreadPoolExecutor() {
        return MVCTx.poolMonoThread;
    }



    /**
     * <p>
     * Execute this transaction in asynchronous mode with a null input paramter.
     * </p>
     * <p>
     * <b>This method is a shortcut for <code>MVCTx.execute(null)</code></b>
     * </p>
     */
    public void execute() {
        execute(null);
    }



    /**
     * <p>
     * Execute this transaction with a null input parameter and waits for its
     * termination. When this method returns, all its methods have been invoked
     * and the transaction has completed.
     * </p>
     * <p>
     * If the {@link MVCTx#shouldExecute(Object)} method has canceled the
     * transaction, then this method will return null. </>
     * </p>
     * <p>
     * <b>This method is a shortcut for <code>MVCTx.executeAndWait(null)</code>
     * </b>
     * </p>
     * 
     * @return The value returned by the {@link MVCTx#doExecute(Object)} method
     *         or null if {@link MVCTx#shouldExecute(Object)} has canceled the
     *         transaction.
     * @throws InterruptedException
     *             If the current thread has been interrupted while executing
     *             the action.
     */
    public T executeAndWait() throws InterruptedException {
        return executeAndWait(null);
    }



    /**
     * <p>
     * Execute this transaction in asynchronous mode with the specified input
     * paramter.
     * </p>
     * 
     * @param param
     *            The input parameter to be passed to the transaction for
     *            execution
     */
    public void execute(final P param) {
        try {
            executeImpl(param, ExecMode.ASYNC);
        } catch (InterruptedException ie) {
            // ignore because never rised in this exec mode.
        }
    }



    /**
     * <p>
     * Execute this transaction and waits for its termination. When this method
     * returns, all its methods have been invoked and the transaction has
     * completed.
     * </p>
     * <p>
     * If the {@link MVCTx#shouldExecute(Object)} method has canceled the
     * transaction, then this method will return null. </>
     * </p>
     * 
     * @param param
     *            The input parameter to be passed to the transaction for
     *            execution.
     * @return The value returned by the {@link MVCTx#doExecute(Object)} method
     *         or null if {@link MVCTx#shouldExecute(Object)} has canceled the
     *         transaction.
     * @throws InterruptedException
     *             If the current thread has been interrupted while executing
     *             the action.
     */
    public T executeAndWait(final P param) throws InterruptedException {
        return executeImpl(param, ExecMode.SYNC);
    }



    /**
     * <p>
     * Execute this transaction with the specified input parameter and execution
     * mode.
     * </p>
     * <p>
     * <ul>
     * <li>
     * When {@link ExecMode} is set to {@link ExecMode#ASYNC}, this transaction
     * is executed asynchronously, this means that this method returns
     * immediately and before its <code>doExecute()</code> method is called.<br>
     * <b><u>When this {@link ExecMode} is used, this method always returns
     * null</u></b></li>
     * <li>When {@link ExecMode} is set to {@link ExecMode#SYNC}, this
     * transaction is executed synchronously, this means that this method waits
     * for the transaction termination. In such a case, this method will wait
     * for all methods of this transaction to be invoked.<br>
     * When this {@link ExecMode} is used, this method returns the same value
     * that is returned by the {@link MVCTx#doExecute(Object)} method</li>
     * </ul>
     * </p>
     * 
     * @param input
     *            The input parameter to be passed as input to this transaction
     * @param execMode
     *            The execution mode, either synchronous or asynchronous.
     * @return Always null if {@link ExecMode#ASYNC} mode is used or the value
     *         that is returned by the {@link MVCTx#doExecute(Object)} method
     * @throws InterruptedException
     *             If the current thread is interrupted while executing in
     *             {@link ExecMode#SYNC} mode. When executing in
     *             {@link ExecMode#ASYNC} this exception is never raised.
     */
    private T executeImpl(final P input, final ExecMode execMode) throws InterruptedException {
        // invoke shouldExecute() in AWT-EDT
        SwingJob<Boolean> shouldExecuteSj = new SwingJob<Boolean>() {

            public Boolean run() {
                dispatch(DispatchService.BEFORE_EXECUTION);
                try {
                    return shouldExecute(input);
                } catch (Exception e) {
                    log.error("run(): shouldExecute() has thrown an exception !", e); //$NON-NLS-1$
                    return false;
                }
            }
        };
        boolean activate = shouldExecuteSj.invokeAndWait();
        // si shouldExecute() a mis son v�to, annuler la transaction
        if (!activate) {
            dispatch(DispatchService.AFTER_EXECUTION);
            return null;
        }
        // invoke preExecute() in AWT-EDT
        SwingJob<Object> preExecuteSj = new SwingJob<Object>() {

            @Override
            protected Object run() {
                try {
                    preExecute(input);
                } catch (Exception e) {
                    log.error("execute(): preExecute() has thrown an exception !", e); //$NON-NLS-1$
                }
                return null;
            }
        };
        preExecuteSj.invokeAndWait();
        // declare value to be returned
        T out = null;
        // invoke doExecute in business thread in sync or async mode
        switch (execMode) {
            case ASYNC :
                getThreadPoolExecutor().execute(new Runnable() {

                    public void run() {
                        Thread myThread = Thread.currentThread();
                        activeThreads.add(myThread);
                        log.debug("execute(): Execution of doExecute()"); //$NON-NLS-1$
                        try {
                            T val = doExecute(input);
                            // invoke updateViewSuccess() in AWT-EDT
                            invokeUpdateViewSuccessInEDT(val, execMode);
                        } catch (Exception e) {
                            log.error("execute(): doExecute() has thrown an exception !", e); //$NON-NLS-1$
                            // invoke updateViewError() in AWT-EDT
                            invokeUpdateViewErrorInEDT(e, execMode);
                        } finally {
                            activeThreads.remove(myThread);
                            // invoke postExecute() in AWT-EDT
                            invokePostExecuteInEDT(input, execMode);
                        }
                    }
                });
                break;
            case SYNC :
                Future<T> future = poolMultiThread.submit(new Callable<T>() {

                    public T call() throws Exception {
                        log.debug("execute(): Execution of doExecute()"); //$NON-NLS-1$
                        return doExecute(input);
                    }
                });
                try {
                    // assigned output value with the returned value of
                    // doExecute()
                    out = future.get();
                    // invoke updateViewSuccess() in AWT-EDT
                    invokeUpdateViewSuccessInEDT(out, execMode);
                } catch (ExecutionException ee) {
                    Throwable cause = ee.getCause();
                    log.error("execute(): doExecute() has thrown an exception !", cause); //$NON-NLS-1$
                    if (cause instanceof Exception) {
                        invokeUpdateViewErrorInEDT((Exception) cause, execMode);
                    }
                    throw new AppInfrastructureException(cause.getMessage(), cause);
                } finally {
                    // invoke postExecute() in AWT-EDT
                    invokePostExecuteInEDT(input, execMode);
                }
                break;
        }
        return out;
    }



    /**
     * Invoque le service updateViewSuccess dans le fil du Thread AWT
     * 
     * @param param
     *            param�tre a passer au service
     */
    void invokeUpdateViewSuccessInEDT(final T param, ExecMode execMode) {
        SwingJob<Void> job = new SwingJob<Void>() {

            @Override
            protected Void run() {
                log.debug("run(): Execution of updateViewSuccess()"); //$NON-NLS-1$
                try {
                    updateViewSuccess(param);
                } catch (Exception e) {
                    log.error("updateViewSuccess() has thrown an exception !", e); //$NON-NLS-1$
                }
                return null;
            }
        };
        switch (execMode) {
            case ASYNC :
                job.invokeLater();
                break;
            case SYNC :
                job.invokeAndWait();
                break;
        }
    }



    /**
     * Invoque le service updateViewError dans le fil du Thread AWT
     * 
     * @param param
     *            param�tre a passer au service
     */
    void invokeUpdateViewErrorInEDT(final Exception param, ExecMode execMode) {
        SwingJob<Void> job = new SwingJob<Void>() {

            @Override
            protected Void run() {
                log.debug("run(): Execution of updateViewError()"); //$NON-NLS-1$
                try {
                    updateViewError(param);
                } catch (Exception e) {
                    log.error("updateViewError() has thrown an exception !", e); //$NON-NLS-1$
                }
                return null;
            }
        };
        switch (execMode) {
            case ASYNC :
                job.invokeLater();
                break;
            case SYNC :
                job.invokeAndWait();
                break;
        }
    }



    /**
     * Invoque le service postExecute dans le fil du Thread AWT
     */
    void invokePostExecuteInEDT(final P param, ExecMode execMode) {
        SwingJob<Void> job = new SwingJob<Void>() {

            @Override
            protected Void run() {
                log.debug("run(): Execution of postExecute()"); //$NON-NLS-1$
                try {
                    postExecute(param);
                } catch (Exception e) {
                    log.error("postExecute(param) has thrown an exception !", e); //$NON-NLS-1$
                }
                try {
                    postExecute();
                } catch (Exception e) {
                    log.error("postExecute() has thrown an exception !", e); //$NON-NLS-1$
                }
                dispatch(DispatchService.AFTER_EXECUTION);
                return null;
            }
        };
        switch (execMode) {
            case ASYNC :
                job.invokeLater();
                break;
            case SYNC :
                job.invokeAndWait();
                break;
        }
    }

}
