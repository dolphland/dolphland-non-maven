package com.dolphland.client.util.event;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.transaction.MVCTx;

public class DeregisterKeyEvent extends UIEvent {

    public static final EventId ID = new EventId(FwkEvents.nextFwkId(), "DEREGISTER_KEY_ACTION_EVT"); //$NON-NLS-1$

    private Object action;



    private DeregisterKeyEvent(Runnable action) {
        super(ID);
        this.action = action;
    }



    private DeregisterKeyEvent(UIAction<UIActionEvt, ?> action) {
        super(ID);
        this.action = action;
    }



    private DeregisterKeyEvent(MVCTx<Object, ?> action) {
        super(ID);
        this.action = action;
    }



    public static DeregisterKeyEvent forAction(Runnable action) {
        return new DeregisterKeyEvent(action);
    }



    public static DeregisterKeyEvent forAction(UIAction<UIActionEvt, ?> action) {
        return new DeregisterKeyEvent(action);
    }



    public static DeregisterKeyEvent forAction(MVCTx<Object, ?> action) {
        return new DeregisterKeyEvent(action);
    }



    /**
     * @return the action
     */
    public Object getAction() {
        return action;
    }
}
