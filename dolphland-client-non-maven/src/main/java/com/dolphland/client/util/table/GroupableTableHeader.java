package com.dolphland.client.util.table;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.plaf.TableHeaderUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

class GroupableTableHeader extends JTableHeader {

    protected List<ColumnGroup> columnGroups;



    public GroupableTableHeader() {
        super();
        columnGroups = new ArrayList<ColumnGroup>();
    }



    public GroupableTableHeader(TableColumnModel model) {
        super(model);
        columnGroups = new ArrayList<ColumnGroup>();
        setUI(new GroupableTableHeaderUI());
    }



    @Override
    public void setUI(TableHeaderUI ui) {
        super.setUI(new GroupableTableHeaderUI());
    }



    /**
     * Fournit l'index actuel de la colonne
     */
    private int getIndexForColumn(TableColumn aColumn) {
        TableColumnModel cm = getColumnModel();
        for (int column = 0; column < cm.getColumnCount(); column++) {
            if (cm.getColumn(column) == aColumn) {
                return column;
            }
        }
        return -1;
    }



    /**
     * Fournit la colonne actuellement en drag & drop
     */
    @Override
    public TableColumn getDraggedColumn() {
        TableColumn draggedColumn = super.getDraggedColumn();

        if (draggedColumn != null) {

            // D�termine la borne min et la borne max possible pour le drag &
            // drop
            int indMin = -1;
            int indMax = -1;
            for (ColumnGroup cGroup : columnGroups) {
                for (TableColumn tc : cGroup.getColumnsSameColumnGroups(draggedColumn)) {
                    int indTableColumn = tc.getModelIndex();
                    indMin = indMin == -1 ? indTableColumn : Math.min(indMin, indTableColumn);
                    indMax = indMax == -1 ? indTableColumn : Math.max(indMax, indTableColumn);
                }
            }

            // D�termine l'indice actuel de la colonne en drag and drop
            int draggedCurrentColumn = getIndexForColumn(draggedColumn);

            // D�termine la direction du drag and drop
            boolean leftToRight = getDraggedDistance() > 0;
            boolean rightToLeft = getDraggedDistance() < 0;

            // Interdit le drag and drop d'une borne min d�pass�e
            if (indMin >= 0 && draggedCurrentColumn < indMin) {
                draggedColumn = null;
            }
            // Interdit le drag and drop d'une borne max d�pass�e
            else if (indMax >= 0 && draggedCurrentColumn > indMax) {
                draggedColumn = null;
            }
            // Interdit le drag and drop d'une borne min si drag and drop de la
            // droite vers la gauche
            else if (draggedCurrentColumn == indMin && rightToLeft) {
                draggedColumn = null;
            }
            // Interdit le drag and drop d'une borne min si drag and drop de la
            // gauche vers la droite
            else if (draggedCurrentColumn == indMax && leftToRight) {
                draggedColumn = null;
            }
        }

        return draggedColumn;
    }



    public void addColumnGroup(ColumnGroup cGroup) {
        columnGroups.add(cGroup);
    }



    public Enumeration getColumnGroups(TableColumn col) {
        for (ColumnGroup cGroup : columnGroups) {
            Vector v_ret = (Vector) cGroup.getColumnGroups(col, new Vector());
            if (v_ret != null) {
                return v_ret.elements();
            }
        }
        return null;
    }



    public void setColumnMargin() {
        int columnMargin = getColumnModel().getColumnMargin() - 1;
        for (ColumnGroup cGroup : columnGroups) {
            cGroup.setColumnMargin(columnMargin);
        }
    }
}
