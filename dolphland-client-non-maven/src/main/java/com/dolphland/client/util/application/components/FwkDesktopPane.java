package com.dolphland.client.util.application.components;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JDesktopPane;

import com.dolphland.client.util.image.ImageUtil;

public class FwkDesktopPane extends JDesktopPane{

    // Variables utilis�es pour afficher une image
	private BufferedImage image;
	private boolean mosaicEnabled;
	
	// Permet de corriger un bug 1.5 n'indiquant pas l'opacit� du bureau
	private boolean opaque;	

	public FwkDesktopPane() {
		super();
	}
	
    public BufferedImage getImage() {
		return image;
	}
    
    public void setImage(BufferedImage image) {
		setImage(image, true);
	}
    
    public void setImage(String resourcePath){
    	setImage(resourcePath==null?null:ImageUtil.getImage(resourcePath));
    }

    public void setImage(String resourcePath, boolean mosaicEnabled){
    	setImage(resourcePath==null?null:ImageUtil.getImage(resourcePath), mosaicEnabled);
    }
    
    public void setImage(BufferedImage image, boolean mosaicEnabled) {
		this.image = image;
		this.mosaicEnabled = mosaicEnabled;
	}
    
    @Override
    public boolean isOpaque() {
    	return opaque;
    }
    
    @Override
    public void setOpaque(boolean isOpaque) {
    	opaque = isOpaque;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if(image != null){
        	Graphics2D g2 = (Graphics2D)g;
    		int w = getWidth();
    		int h = getHeight();
    		
    		if(mosaicEnabled){
        		int wI = image.getWidth();
        		int hI = image.getHeight();
        		for(int x = 0 ; (wI * x) < w ; x++){
        			for(int y = 0 ; (hI * y) < h ; y++){
        				g2.drawImage(image, (wI * x), (hI * y), this);
        			}
        		}
    		}else{
        		g2.drawImage(image, 0, 0, w, h, this);
    		}
        }
    }
}
