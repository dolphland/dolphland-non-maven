package com.dolphland.client.util.event;

/**
 * Ev�nement �mis pour demander la fermeture de l'application.<br>
 * A r�ception, le framework d�clenchera l'�mission de
 * </code>AppClosingEvt</code>
 * 
 * @author Benoit vatan
 * @author jeremy.scafi
 * 
 */
public final class CloseAppRequestEvt extends Event {

    private volatile boolean systemExitOnSuccess;



    public CloseAppRequestEvt() {
        super(FwkEvents.CLOSE_APP_REQUEST_EVT);
    }



    /**
     * Indique si le framework doit effectuer un System.exit() � la fin de la
     * proc�dure de fermeture et si cette derni�re � r�ussie (false par d�faut).<br>
     * Une proc�dure de fermeture est r�put�e r�ussie si auccun v�to n'a �t�
     * �mis pendant la proc�dure.
     * 
     * @return
     */
    public boolean shouldSystemExitOnSuccess() {
        return systemExitOnSuccess;
    }



    /**
     * Indique si le framework soit effectuer un System.exit() � la fin de la
     * proc�dure de fermeture si elle a r�ussie.
     * 
     * @param systemExitOnSuccess
     */
    public void setSystemExitOnSuccess(boolean systemExitOnSuccess) {
        this.systemExitOnSuccess = systemExitOnSuccess;
    }
}
