/*
 * $Log: Schedule.java,v $
 * Revision 1.4 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.3 2010/01/28 15:36:11 bvatan
 * - Correction d'un bug de synchronisation sur l'instanciation des Schedule qui
 * pouvait provoquer des �crasements de Schedule.
 * Revision 1.2 2009/04/01 09:30:46 bvatan
 * - correction bug schedule(EventDestination)
 * Revision 1.1 2009/03/24 13:53:09 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

import org.apache.log4j.Logger;

/**
 * Repr�sente une planification d'�mission d'�v�nement.<br>
 * Un Schedule encapsule toutes les informations de planification d'�mission
 * d'un �v�nement.<br>
 * <b>Exemple 1: Programmation de l'�mission d'un �v�nement vers une destination
 * par d�faut</b>
 * 
 * <pre>
 * MyEvent evt = new MyEvent();
 * Schedule sched = Schedule.event(evt).count(5).interval(2000);
 * EventMulticaster.getInstance().schedule(sched);
 * </pre>
 * 
 * L'exemple ci-dessus utilise la destination port�e par l'�v�nement.<br>
 * <br>
 * <br>
 * <b>Exemple 2: Programmation de l'�mission d'un �v�nement vers une destination
 * explicite</b>
 * 
 * <pre>
 * EventDestination myDestination = ...;
 * MyEvent evt = new MyEvent();
 * Schedule sched = Schedule.event(evt).destination(myDestination).count(5).interval(2000);
 * EventMulticaster.getInstance().schedule(sched);
 * </pre>
 * 
 * L'exemple ci-dessus planifie l'�mission de l'�v�nement 5 fois, par intervals
 * de 2 secondes et vers la destination myDestination. La destination port�e par
 * l'�v�nement sera ignor�e.<br>
 * <br>
 * <br>
 * <b>Exemple 3: D�programmation d'une planification :</b><br>
 * <br>
 * Plus tard, pour d�programmer l'�v�nement :<br>
 * 
 * <pre>
 * shed.destroy();
 * </pre>
 * 
 * @author JayJay
 * 
 */
public class Schedule implements Comparable<Schedule> {

    private static final Logger log = Logger.getLogger(Schedule.class);

    public static final int MISSFIRE_RESCHEDULE_CLOCK_TIME = 1;

    public static final int MISSFIRE_RESCHEDULE_EXEC_TIME = 2;

    public static final int MISSFIRE_NO_FIRE = 3;

    private static volatile long seqid;

    private volatile long execTime = -1;

    private volatile long startDate = -1;

    private volatile long endDate = -1;

    private volatile long repeatCount = 0;

    private volatile boolean repeatable;

    private volatile long repeatInterval = -1;

    private volatile long execCount;

    private volatile long id;

    private int missFirePolicy = MISSFIRE_RESCHEDULE_CLOCK_TIME;

    private volatile long lastFireTime;

    private volatile Event event;

    private volatile EventDestination destination;

    private volatile boolean destroyed;

    private volatile boolean locked;



    private Schedule(Event evt, EventDestination dest) {
        if (evt == null) {
            throw new NullPointerException("evt is null !"); //$NON-NLS-1$
        }
        if (dest == null) {
            throw new NullPointerException("dest is null !"); //$NON-NLS-1$
        }
        this.id = nextSeqid();
        startDate = System.currentTimeMillis();
        this.execTime = startDate;
        this.event = evt;
        this.destination = dest;
    }



    private long nextSeqid() {
        synchronized (Schedule.class) {
            return seqid++;
        }
    }



    /**
     * Cr� une planification d'�v�nement. Cette m�thode retourne un Schedule qui
     * contient les informations de planification d'�mission de l'�v�nement
     * sp�cifi�. L'objet retourn� est destin� � �tre fourni au multicasteur
     * d'�v�nements pour programmer l'�mission de l'�v�nement.<br>
     */
    public static final Schedule event(Event e) {
        return new Schedule(e, e.getDestination());
    }



    void lock() {
        this.locked = true;
    }



    private void checkState() {
        if (locked) {
            throw new IllegalStateException("Schedule is already planned, cannont by modified !"); //$NON-NLS-1$
        }
    }



    /**
     * Sp�cifie la destination de l'�v�nement � programmer
     * 
     * @param dest
     *            La destination de l'�v�nement lors de son �mission
     */
    public synchronized Schedule destination(EventDestination dest) {
        checkState();
        if (dest == null) {
            throw new NullPointerException("dest is null !"); //$NON-NLS-1$
        }
        this.destination = dest;
        return this;
    }



    /**
     * Date � partir de laquelle la planification est active.
     */
    public synchronized Schedule startDate(long startDate) {
        checkState();
        if (execTime < startDate) {
            execTime = startDate;
        }
        this.startDate = startDate;
        return this;
    }



    /**
     * Nombre d'�mission maximum de l'�v�nement
     */
    public synchronized Schedule repeat(long count) {
        checkState();
        this.repeatCount = count;
        this.repeatable = repeatInterval > 0 || repeatCount > 0;
        return this;
    }



    /**
     * Interval de temps entre chaque �mission
     */
    public synchronized Schedule interval(long repeatInterval) {
        checkState();
        this.repeatInterval = repeatInterval;
        this.repeatable = repeatInterval > 0 || repeatCount > 0;
        return this;
    }



    /**
     * Date apr�s laquelle la planinification n'est plus active
     */
    public synchronized Schedule endDate(long endDate) {
        checkState();
        this.endDate = endDate;
        return this;
    }



    /**
     * d�finit la politique de r��mission en cas d'�mission ratt�e (par exemple
     * en cas de charge importante)<br>
     * <ul>
     * <li>MISSFIRE_RESCHEDULE_CLOCK_TIME: replanifie l'�v�nement � partir de
     * l'heure courante.</li>
     * <li>MISSFIRE_RESCHEDULE_EXEC_TIME: replanifie l'�v�nement � partir de
     * l'heure � laquelle l'�v�nement aurait d� �tre �mis.</li>
     * <li>MISSFIRE_NO_FIRE : L'�mission ratt�e est ignor�e, l'�v�nement est
     * replanifi� comme si tout s'�tait bien pass�.</li>
     * </ul>
     * 
     * @param missFirePolicy
     *            politique de r��mission.
     */
    public synchronized Schedule missFirePolicy(int missFirePolicy) {
        checkState();
        this.missFirePolicy = missFirePolicy;
        return this;
    }



    /**
     * D�truit le schedule. Il sera purg� par le scheduler lors de sa prochaine
     * �lection.
     */
    public synchronized void destroy() {
        destroyed = true;
    }



    synchronized long getExecTime() {
        return execTime;
    }



    synchronized Schedule setExecTime(long execTime) {
        this.execTime = execTime;
        return this;
    }



    synchronized long getStartDate() {
        return startDate;
    }



    synchronized long getEndDate() {
        return endDate;
    }



    synchronized long getRepeatCount() {
        return repeatCount;
    }



    synchronized boolean isRepeatable() {
        return repeatable;
    }



    synchronized long getRepeatInterval() {
        return repeatInterval;
    }



    synchronized long getId() {
        return id;
    }



    synchronized Schedule setExecCount(long count) {
        this.execCount = count;
        return this;
    }



    synchronized long getExecCount() {
        return execCount;
    }



    synchronized int getMissFirePolicy() {
        return missFirePolicy;
    }



    synchronized boolean isGarbage() {
        if (destroyed)
            return true;
        if (endDate > 0 && System.currentTimeMillis() > endDate) {
            if (log.isDebugEnabled()) {
                log.debug("isGarbage(): " + this + " expired"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            return true;
        }
        if (repeatable && repeatCount > 0 && execCount >= repeatCount) {
            if (log.isDebugEnabled()) {
                log.debug("isGarbage(): " + this + " exhausted"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            return true;
        }
        if (!repeatable && execCount > 0) {
            return true;
        }
        return false;
    }



    synchronized void fire() {
        lastFireTime = System.currentTimeMillis();
        this.execCount++;
    }



    public synchronized final int compareTo(Schedule o) {
        if (id == o.id)
            return 0;
        return execTime < o.execTime ? -1 : execTime > o.execTime ? 1 : id < o.id ? -1 : id > o.id ? 1 : 0;
    }



    @Override
    public final boolean equals(Object obj) {
        return compareTo((Schedule) obj) == 0;
    }



    Event getEvent() {
        return event;
    }



    EventDestination getDestination() {
        return destination;
    }



    @Override
    public String toString() {
        return "sched" + Long.toString(id) + "(execTime=" + execTime + ",repeatCount=" + repeatCount + ",repeatInterval=" + repeatInterval + ",execCount=+" + execCount + ",startDate=" + startDate + ",endDate=" + endDate + ",lastFireTime=" + lastFireTime + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$
    }

}
