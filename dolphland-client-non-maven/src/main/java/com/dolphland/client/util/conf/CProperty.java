/*
 * $Log: CProperty.java,v $
 * Revision 1.5 2012/12/17 12:26:29 bvatan
 * - added NON-NLS comments
 * Revision 1.4 2012/08/10 13:31:53 bvatan
 * - added support for text in properties
 * - added helper method for getting property as a list
 * Revision 1.3 2009/02/26 09:55:24 bvatan
 * - ajout getFile()
 * - ajout getUrl()
 * Revision 1.2 2008/11/28 10:05:51 bvatan
 * - ajout helper par type scalaire
 * Revision 1.1 2008/11/27 07:38:33 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Holds data realtive to <code>&lt;property&gt;</code> tags in application
 * (application.xml) configuration file.
 * </p>
 * 
 * @author JayJay
 */
public class CProperty {

    private static final String DEFAULT_LIST_SEPARATOR = "\n"; //$NON-NLS-1$

    private String name;

    private String value;



    public void setName(String name) {
        this.name = name;
    }



    public void setValue(String value) {
        this.value = value;
    }



    public String getName() {
        return name;
    }



    public String getValue() {
        return value;
    }



    public void addText(String content) {
        if (this.value != null) {
            this.value += content;
        } else {
            this.value = content;
        }
    }



    public int getInt() {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }



    public long getLong() {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }



    public String getString() {
        return value;
    }



    public float getFloat() {
        try {
            return Float.parseFloat(value);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }



    public double getDouble() {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }



    public boolean getBoolean() {
        return "yes".equals(value) || "on".equals(value) || "true".equals(value) || "vrai".equals(value) || "oui".equals(value) || "1".equals(value); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
    }



    public File getFile() {
        return new File(value);
    }



    public URL getUrl() {
        try {
            return new URL(value);
        } catch (MalformedURLException mue) {
            return null;
        }
    }



    /**
     * <p>
     * Get the property as a list of strings where strings are fecthed from the
     * property value and separated by a '\n'.
     * </p>
     * <p>
     * Returned strings are trimed (see {@link String#trim()}).
     * </p>
     * <p>
     * Empty string (after triming) are discarded, that is they won't be added
     * to the returned list.
     * </p>
     * 
     * @return The list of strings extracted from the value of this property
     */
    public List<String> getList() {
        return getList(DEFAULT_LIST_SEPARATOR, true);
    }



    /**
     * <p>
     * Get the property as a list of strings where strings are fecthed from the
     * property value and separated by the specified separator regular
     * expression (see {@link java.util.regex.Pattern}) for expression syntax.
     * </p>
     * <p>
     * Every string in the rteturned list is trimed (see {@link String#trim()})
     * before it is added to the list.
     * </p>
     * 
     * @param separator
     *            The regular expression as defined by
     *            {@link java.util.regex.Pattern} used for separating strings
     * @return The list of strings extracted from the value of this property
     */
    public List<String> getList(String separator) {
        return getList(separator, true);
    }



    /**
     * <p>
     * Get the property as a list of strings where strings are fecthed from the
     * property value and separated by the specified separator regular
     * expression (see {@link java.util.regex.Pattern}) for expression syntax.
     * </p>
     * 
     * @param separator
     *            The regular expression as defined by
     *            {@link java.util.regex.Pattern} used for separating strings
     * @param trim
     *            When true this instructs the method the trim (see
     *            {@link String#trim()}) the strings before thaye are added to
     *            the returned list. When false, string are left as they are.
     * @returnThe list of strings extracted from the value of this property
     */
    public List<String> getList(String separator, boolean trim) {
        return getList(separator, trim, true);
    }



    /**
     * <p>
     * Get the property as a list of strings where strings are fecthed from the
     * property value and separated by the specified separator regular
     * expression (see {@link java.util.regex.Pattern}) for expression syntax.
     * </p>
     * 
     * @param separator
     *            The regular expression as defined by
     *            {@link java.util.regex.Pattern} used for separating strings
     * @param trim
     *            When true this instructs the method the trim (see
     *            {@link String#trim()}) the strings before thaye are added to
     *            the returned list. When false, string are left as they are.
     * @param discardEmpty
     *            When true, empty string (after triming) are not added to the
     *            returned list.
     * @return The list of strings extracted from the value of this property
     */
    public List<String> getList(String separator, boolean trim, boolean discardEmpty) {
        List<String> out = new ArrayList<String>();
        String[] tokens = value.split(separator);
        for (String token : tokens) {
            if (trim) {
                token = token.trim();
            }
            if (token.length() == 0 && discardEmpty) {
                continue;
            }
            out.add(token);
        }
        return out;
    }



    @Override
    public String toString() {
        return "(CProperty name=" + name + ", value=" + value + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }

}
