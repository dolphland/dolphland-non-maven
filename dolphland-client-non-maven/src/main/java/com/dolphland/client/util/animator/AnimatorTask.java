package com.dolphland.client.util.animator;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionUtil;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.image.ImageUtil;

/**
 * Task to animate
 * 
 * @author jayjay
 * 
 */
class AnimatorTask extends Thread {

    private static final Logger LOG = Logger.getLogger(AnimatorTask.class);

    // Default frames by second
    private static final int DEFAULT_FRAMES_BY_SECOND = 60;

    // Default animation's duration in milliseconds
    private static final long DEFAULT_ANIMATION_DURATION_MILLISECOND = 300;

    // Ratio between milliseconds and nanoseconds
    private static final long RATIO_MS_NS = 1000000;

    // For auto-numbering thread
    private static int threadInitNumber;



    private static synchronized int nextThreadNum() {
        return threadInitNumber++;
    }

    // Animated panel
    private FwkPanel animatedPanel;

    // Sequences animations
    private List<SequenceAnimations> sequencesAnimations;

    // Animations to do
    private List<StepAnimationAdapter> animationsToDo;

    // Consistent interval between frames (in nanoseconds)
    private long consistentIntervalBetweenFramesNs;

    // Real interval between frames (in nanoseconds)
    private long realIntervalBetweenFramesTimeNs;

    // Previous frame start time (in nanoseconds)
    private long previousFrameStartTimeNs;

    // Current frame start time (in nanoseconds)
    private long currentFrameStartTimeNs;



    /**
     * Default constructor
     * 
     * @param animatedPanel
     *            Animated panel
     * @param sequencesAnimations
     *            Sequences animations
     */
    public AnimatorTask(FwkPanel animatedPanel, List<SequenceAnimations> sequencesAnimations) {
        this(animatedPanel, sequencesAnimations, DEFAULT_FRAMES_BY_SECOND);
    }



    /**
     * Constructor with frames by second
     * 
     * @param animatedPanel
     *            Animated panel
     * @param sequencesAnimations
     *            Sequences animations
     * @param framesBySecond
     *            Frames by second
     */
    public AnimatorTask(FwkPanel animatedPanel, List<SequenceAnimations> sequencesAnimations, int framesBySecond) {
        super("AnimatorTask" + nextThreadNum());
        AssertUtil.notNull(animatedPanel, "animatedPanel is null !");
        AssertUtil.notNull(sequencesAnimations, "sequencesAnimations is null !");
        setPriority(Thread.MAX_PRIORITY);
        this.animatedPanel = animatedPanel;
        this.sequencesAnimations = new ArrayList<SequenceAnimations>(sequencesAnimations);
        this.animationsToDo = new ArrayList<StepAnimationAdapter>();

        // Calculate interval between frames (in nanoseconds)
        consistentIntervalBetweenFramesNs = (long) ((1000.0d / (double) framesBySecond) * ((double) RATIO_MS_NS));

        if (LOG.isDebugEnabled()) {
            LOG.debug("Consistent interval between frames : " + consistentIntervalBetweenFramesNs + " ms");
        }
    }



    /**
     * Show step animations
     */
    private void showStepAnimations(final List<StepAnimationAdapter> animations) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {

                @Override
                public void run() {
                    for (StepAnimationAdapter stepAnimation : animations) {
                        stepAnimation.getAnimation().getComponent().refreshComponent();
                        if (stepAnimation.isBackgroundLayerStep()) {
                            animatedPanel.setComponentZOrder(stepAnimation.getAnimation().getComponent(), animatedPanel.getComponentCount() - 1);
                            animatedPanel.validate();
                        } else if (stepAnimation.isForegroundLayerStep()) {
                            animatedPanel.setComponentZOrder(stepAnimation.getAnimation().getComponent(), 0);
                            animatedPanel.validate();
                        }
                    }
                }
            });
        } catch (InterruptedException exc) {
            interrupt();
        } catch (InvocationTargetException exc) {
            LOG.error("Error when showing step animations", exc);
        } catch (Exception exc) {
            LOG.error("Error when showing step animations", exc);
        }
    }



    /**
     * Load the frame
     * 
     * @return Wait time in milliseconds before next frame
     */
    private long loadFrame() {
        // Show step animations
        showStepAnimations(new ArrayList<StepAnimationAdapter>(animationsToDo));

        // Increment steps done for each animation to do
        for (StepAnimationAdapter adapter : new ArrayList<StepAnimationAdapter>(animationsToDo)) {
            // If animation is finished
            if (adapter.getStepsToDo() <= adapter.getStepsDone()) {
                // Execute actions after animation
                try {
                    UIActionUtil.executeActions(adapter.getAnimation().getParameter().getActionsAfterAnimation(), adapter.getAnimation().getComponent());
                } catch (Exception exc) {
                    // Do nothing if error
                }

                // Remove animation
                animationsToDo.remove(adapter);

                // Go to next animation
                continue;
            }
        }

        // If no more animations to do, get animations to from next sequence
        if (animationsToDo.isEmpty() && !sequencesAnimations.isEmpty()) {
            SequenceAnimations sequenceAnimations = sequencesAnimations.remove(0);
            for (Animation animation : sequenceAnimations.getAnimations()) {
                // Calculate steps to do about animation's duration and
                // consistent interval between frames
                int stepsToDo = 1;
                if (!new Integer(AnimationParameter.ONLY_ONE_STEP).equals(animation.getParameter().getStepsPolicy())) {
                    long animationDurationMs = animation.getParameter().getAnimationDurationMs() == null ? DEFAULT_ANIMATION_DURATION_MILLISECOND : animation.getParameter().getAnimationDurationMs();
                    stepsToDo = (int) ((animationDurationMs * RATIO_MS_NS) / consistentIntervalBetweenFramesNs);
                }
                // Add animation to do
                animationsToDo.add(new StepAnimationAdapter(animation, stepsToDo));
            }
            if (sequenceAnimations.getWaitTimeMs() > 0) {
                return sequenceAnimations.getWaitTimeMs();
            }
        }

        // Do nothing if no more animations to do
        if (animationsToDo.isEmpty()) {
            return 0;
        }

        // If not first step done
        if (animationsToDo.get(0).getStepsDone() == 0) {
            // Execute actions before for each animation to do
            for (StepAnimationAdapter adapter : animationsToDo) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Transformations to do: deltaX=" + adapter.getRemainingDeltaX() + ", deltaY=" + adapter.getRemainingDeltaY() + ", deltaWidth=" + adapter.getRemainingDeltaWidth() + ", deltaHeight=" + adapter.getRemainingDeltaHeight() + ", deltaDegree=" + adapter.getRemainingDeltaDegree() + ", back=" + adapter.isBackStep());
                }

                try {
                    UIActionUtil.executeActions(adapter.getAnimation().getParameter().getActionsBeforeAnimation(), adapter.getAnimation().getComponent());
                } catch (Exception exc) {
                    // Do nothing if error
                }
            }
        }

        // For each animation to do
        for (StepAnimationAdapter adapter : animationsToDo) {

            // Apply transformations
            adapter.getAnimation().getComponent().applyStepAnimationTransformations(adapter);

            // Increment steps done
            adapter.incrementStep();

            if (LOG.isDebugEnabled()) {
                LOG.debug("Apply step transformations: stepsDone=" + adapter.getStepsDone() + ", stepsToDo=" + adapter.getStepsToDo() + ", deltaX=" + adapter.getDeltaXStep() + ", deltaY=" + adapter.getDeltaYStep() + ", deltaWidth=" + adapter.getDeltaWidthStep() + ", deltaHeight=" + adapter.getDeltaHeightStep() + ", deltaDegree=" + adapter.getDeltaDegreeStep() + ", back=" + adapter.isBackStep());
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("Step done in " + ((double) ((System.nanoTime() - currentFrameStartTimeNs)) / ((double) RATIO_MS_NS)) + " ms");
        }

        return 0;
    }



    @Override
    public void run() {
        while (!isInterrupted()) {
            // Get current frame start time
            currentFrameStartTimeNs = System.nanoTime();

            if (LOG.isDebugEnabled()) {
                realIntervalBetweenFramesTimeNs = previousFrameStartTimeNs == 0 ? 0 : currentFrameStartTimeNs - previousFrameStartTimeNs;
                previousFrameStartTimeNs = currentFrameStartTimeNs;
                LOG.debug("Time between previous and current frames : " + ((double) realIntervalBetweenFramesTimeNs / ((double) RATIO_MS_NS)) + " ms");
            }

            // Load the frame
            long waitTimeNextFrameMs = 0;
            try {
                waitTimeNextFrameMs = loadFrame();
            } catch (Exception exc) {
                LOG.error("Error during frame", exc);
            }

            // If no more animations to do, stop task
            if (animationsToDo.isEmpty()) {
                return;
            }

            // Indicator to know if next frame must be directly called
            boolean nextFrameDirectly = false;

            // If animations contains an animation with only one step, go
            // directly to next frame
            if (waitTimeNextFrameMs <= 0) {
                for (StepAnimationAdapter animation : animationsToDo) {
                    if (animation.getStepsToDo() == 1) {
                        nextFrameDirectly = true;
                        break;
                    }
                }
            }

            // If current frame has passed the delay for a consistent interval
            // between frames, prevent with a warning and load immediately the
            // next frame
            if (!nextFrameDirectly) {
                long intervalNextFrameNs = waitTimeNextFrameMs > 0 ? (long) (waitTimeNextFrameMs * RATIO_MS_NS) : consistentIntervalBetweenFramesNs;
                long timeCurrentFramePassedNs = currentFrameStartTimeNs == 0 ? 0 : System.nanoTime() - currentFrameStartTimeNs;
                if (timeCurrentFramePassedNs >= intervalNextFrameNs) {
                    LOG.warn("Current frame has passed the delay for a consistent interval between frames !");
                    nextFrameDirectly = true;
                }

                // Schedule next frame
                if (!nextFrameDirectly) {
                    try {
                        sleep((long) ((intervalNextFrameNs - timeCurrentFramePassedNs) / RATIO_MS_NS));
                    } catch (InterruptedException e) {
                        interrupt();
                    }
                }
            }
        }
    }



    public static void main(String[] args) {
        // Start EDT
        EDT.start();

        List<SequenceAnimations> sequencesAnimations = new ArrayList<SequenceAnimations>();

        // Create animated panel
        FwkPanel animatedPanel = new FwkPanel();
        animatedPanel.setBackground(Color.RED);

        // Create and show frame, if frame is closed, stop animation
        JFrame frame = new JFrame();
        frame.setSize(640, 480);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(animatedPanel);
        frame.setVisible(true);

        // Add animated component
        BufferedImage frontImage = ImageUtil.getImage("/images/cards/card_ace_hearts.gif");
        BufferedImage backImage = ImageUtil.getImage("/images/cards/card_back.png");
        AnimatedComponent component = new AnimatedComponent("1", frontImage, backImage);
        animatedPanel.add(component);

        // Add first animations sequence
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        DefaultAnimationParameter animationParameter = new DefaultAnimationParameter();
        animationParameter.setX(100);
        animationParameter.setY(200);
        animationParameter.setRatioWidth(2.0d);
        animationParameter.setRatioHeight(2.0d);
        animationParameter.setDegreRotation(-180);
        animationParameter.getActionsBeforeAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("D�but animation " + param.getAnimatedComponentId() + "\t(Temps : " + new Date().getTime() + ")");
            }
        });
        animationParameter.getActionsAfterAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("Fin animation " + param.getAnimatedComponentId() + "\t\t(Temps : " + new Date().getTime() + ")");
            }
        });
        Animation animation = new Animation(component, animationParameter);
        sequenceAnimations.addAnimation(animation);
        sequencesAnimations.add(sequenceAnimations);

        // Add second animations sequence
        sequenceAnimations = new SequenceAnimations(5000);
        animationParameter = new DefaultAnimationParameter();
        animationParameter.setBack(true);
        animationParameter.setDegreRotation(1080);
        animationParameter.setAnimationDurationMs(5000l);
        animationParameter.getActionsBeforeAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("D�but animation " + param.getAnimatedComponentId() + "\t(Temps : " + new Date().getTime() + ")");
            }
        });
        animationParameter.getActionsAfterAnimation().add(new UIAction<AnimationActionEvt, Object>() {

            @Override
            protected void preExecute(AnimationActionEvt param) {
                System.out.println("Fin animation " + param.getAnimatedComponentId() + "\t\t(Temps : " + new Date().getTime() + ")");
            }
        });
        animation = new Animation(component, animationParameter);
        sequenceAnimations.addAnimation(animation);
        sequencesAnimations.add(sequenceAnimations);

        try {
            sleep(1000);
        } catch (InterruptedException e) {
        }

        // Create animator
        final AnimatorTask animator = new AnimatorTask(animatedPanel, sequencesAnimations);

        animator.start();
    }

}
