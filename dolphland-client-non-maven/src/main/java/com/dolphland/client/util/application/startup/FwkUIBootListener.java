/*
 * $Log: FwkUIBootListener.java,v $
 * Revision 1.12  2015/04/13 11:53:12  jscafi
 * - Refactoring
 *
 * Revision 1.11  2015/04/13 07:04:57  jscafi
 * - Refactoring
 *
 * Revision 1.10  2015/04/10 13:45:04  jscafi
 * 				- Ajout de la gestion de logback dans la fenetre des logs
 * 				- Ajout de traces lors du plantage de l'initialisation de FwkBootListener
 *
 * Revision 1.9  2014/07/29 10:39:50  bvatan
 * - fixed javadoc typo
 *
 * Revision 1.8  2012/08/27 13:55:41  bvatan
 * - code formatting
 *
 * Revision 1.7 2012/07/23 14:18:24 bvatan
 * - javadoc
 * - code formatting
 * Revision 1.6 2010/11/23 14:14:59 bvatan
 * - 5 secondes pour la frappe des s�quences de touche
 * Revision 1.5 2010/11/23 13:40:04 bvatan
 * - modification de la s�quence de frappe pour l'arr�t du cmode super
 * utilisateur
 * Revision 1.4 2010/11/23 12:45:36 bvatan
 * - ajout de l'enregistrement des s�quences de frappe pour le mode super
 * utilisateur
 * Revision 1.3 2009/02/09 08:27:27 jscafi
 * - Key actions
 * Revision 1.2 2008/12/05 14:53:34 jscafi
 * - Suppression des warnings
 * Revision 1.1 2008/12/05 14:23:16 bvatan
 * - initial check in
 */

package com.dolphland.client.util.application.startup;

import org.apache.log4j.Logger;

import com.dolphland.client.util.conf.FwkBoot;
import com.dolphland.client.util.conf.FwkBootEvent;
import com.dolphland.client.util.conf.FwkBootListener;
import com.dolphland.client.util.event.FwkEventQueue;
import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.logs.LogsDialog;
import com.dolphland.client.util.logs.UIAppender;

/**
 * <p>
 * Loaded by {@link FwkBoot} at its instantiation time and invoked during the
 * boot process.
 * </p>
 * 
 * @author jeremy.scafi
 */
public class FwkUIBootListener implements FwkBootListener {

    private final static Logger log = Logger.getLogger(FwkUIBootListener.class);

    public FwkUIBootListener(FwkBoot boot) {
        try{
            UIAppender.install();
        }catch(Exception exc){
            log.error("Error when initializing log system !, reason was " + exc.getMessage(), exc);
            throw new AppBusinessException("Error when initializing log system !, reason was " + exc.getMessage(), exc);
        }
        
        try{
            FwkEventQueue.instance().install();
        }catch(Exception exc){
            log.error("Error when initializing FwkEventQueue !, reason was " + exc.getMessage(), exc);
            throw new AppBusinessException("Error when initializing FwkEventQueue !, reason was " + exc.getMessage(), exc);
        }
        
        try{
            LogsDialog.getInstance().install();
        }catch(Exception exc){
            log.error("Error when initializing logs dialog !, reason was " + exc.getMessage(), exc);
            throw new AppBusinessException("Error when initializing logs dialog !, reason was " + exc.getMessage(), exc);
        }
    }



    public void bootStarted(FwkBootEvent e) {

    }



    public void confLoaded(FwkBootEvent e) {
    }



    public void servicesLoaded(FwkBootEvent e) {

    }



    public void bootComplete(FwkBootEvent e) {
    }

}
