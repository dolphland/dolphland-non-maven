/*
 * $Log: BootContext.java,v $
 * Revision 1.5 2014/03/10 16:06:33 bvatan
 * - Internationalized
 * Revision 1.4 2012/12/17 12:25:02 bvatan
 * - replaced empty string "" by StrUtil.EMPTY_STRING
 * Revision 1.3 2012/07/24 08:51:38 bvatan
 * - made constructor public
 * - added and translated javadoc
 * Revision 1.2 2010/07/22 09:02:26 bvatan
 * - ajout service getArg()
 * Revision 1.1 2009/06/18 13:48:37 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import com.dolphland.client.util.string.StrUtil;

/**
 * <p>
 * Context that is passed to the entry point of applications to be loaded by
 * {@link ApplicationManager}.
 * </p>
 * <p>
 * Contains startup information such as command line arguments and
 * {@link FwkBoot} reference that this context is bound to.
 * </p>
 * 
 * @author JayJay
 */
public class BootContext {

    private FwkBoot booter;

    private String[] args;



    /**
     * <p>
     * Builds a new instance from the specified booter and application command
     * line aguments
     * </p>
     * 
     * @param booter
     *            Instance of the booter that this context is bound to.
     * @param args
     *            The commande line application arguments.
     */
    public BootContext(FwkBoot booter, String[] args) {
        if (booter == null) {
            throw new NullPointerException("booter is null !"); //$NON-NLS-1$
        }
        if (args == null) {
            throw new NullPointerException("args is null !"); //$NON-NLS-1$
        }
        this.booter = booter;
        this.args = args;
    }



    /**
     * <p>
     * Get the booter reference that this context is bound to.
     * </p>
     * 
     * @return The booter instance that this context is cound to.
     */
    public FwkBoot getBooter() {
        return booter;
    }



    /**
     * <p>
     * Get application commande line arguments
     * </p>
     * 
     * @return The comamnd line argument as an array of strings
     */
    public String[] getArgs() {
        return args;
    }



    /**
     * <p>
     * Get a command line argument value from the specifiedargument name.
     * </p>
     * <p>
     * Command line argumentd are supposed to be formed as follow :
     * <code>argument=value</code>
     * </p>
     * 
     * @param name
     *            Argument name
     * @return The argument's value or null if no argument of that name is
     *         found. If the argument name exist but no value is specified after
     *         the equal sign, then an empty string is returned.
     */
    public String getArg(String name) {
        return argValue(args, name);
    }



    private static String argValue(String[] args, String name) {
        if (name == null) {
            throw new NullPointerException("name is null !"); //$NON-NLS-1$
        }

        if (name.trim().length() == 0) {
            throw new IllegalArgumentException("name is empty !"); //$NON-NLS-1$
        }
        for (int i = 0; i < args.length; i++) {
            if (args[i] != null && args[i].startsWith(name)) {
                int idx = args[i].indexOf("="); //$NON-NLS-1$
                if (idx >= 0) {
                    if (idx + 1 < args[i].length()) {
                        return args[i].substring(idx + 1);
                    } else {
                        return StrUtil.EMPTY_STRING;
                    }
                }
            }
        }
        return null;
    }

}
