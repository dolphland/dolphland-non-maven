/*
 * $Log: ApplicationManager.java,v $
 * Revision 1.15 2014/03/10 16:05:39 bvatan
 * - Internationalized
 * Revision 1.14 2013/08/01 11:47:29 bvatan
 * - FWK98: fixed bug that prevented System.getenv().putAll() to be called
 * because System.getenv() is an unmodifiable Map
 * Revision 1.13 2013/08/01 11:09:54 bvatan
 * - FWK-98: Fixed so that logs.dir and logs.label resolution is now performed
 * as follow: System.getEnv() + System.getProperties() + App args
 * Revision 1.12 2013/08/01 11:04:48 bvatan
 * - FWK-98: Fixed log installation bug because of some event delivery timing.
 * System properties are now used to look up logs.dir and logs.label parameters
 * Revision 1.11 2013/04/15 13:27:14 bvatan
 * - reorg imports
 * Revision 1.10 2013/04/08 14:01:28 bvatan
 * - correction bug obtention du user.country, user.language et user.variant.
 * jnlp.user.xxxx est prioritaire sur user.xxxx
 * Revision 1.9 2013/04/05 14:01:53 bvatan
 * FWK-50: Modification des noms des propri�t�s user.language, user.country,
 * user.timezone en jnlp.user.language, jnlp.user.country et jnlp.user.timezone
 * Revision 1.8 2012/12/17 12:26:29 bvatan
 * - added NON-NLS comments
 * Revision 1.7 2012/07/24 08:54:03 bvatan
 * - boot() method now takes a BootContext argument instead of an array of
 * String
 * Revision 1.6 2012/07/23 13:47:33 bvatan
 * - now handles entry points that extends AppEntryPoint
 * - remain compatible with former entry points
 * Revision 1.5 2010/11/23 09:06:55 bvatan
 * - reorg imports
 * Revision 1.4 2010/08/06 07:13:40 bvatan
 * - ajout d�marrage du multicaster
 * Revision 1.3 2010/07/22 09:01:45 bvatan
 * - gestion des logs de l'application dans un r�pertoire local de la machine
 * qui ex�cute l'appli
 * Revision 1.2 2009/06/22 07:27:24 bvatan
 * - ajout d�marrage du AppManager
 * Revision 1.1 2009/06/18 13:48:37 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.FwkEventQueue;
import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * Charge les applications en initialisant l'amor�age et maintient les services
 * globaux � toutes les applications.<br>
 * En partioculier, ApplicationManager g�re le workflow des �v�nement de
 * fermeture des applications.<br>
 * <br>
 * La s�quence d'amor�age est la suivante :<br>
 * <ul>
 * <li>Chargement du point d'entr�e de l'application : La classe principale dont
 * le nom est fournit en premier argument de la ligne de commande</li>
 * <li>Invocation de <code>public static void preMain(BootContext)</code> sur le
 * point d'entr�e</li>
 * <li>Invocation de <code>public static void main(BootContext)</code> sur le
 * point d'entr�e</li>
 * <ul>
 * <li>Si �chec, invocation de <code>public static void main(String[])</code>
 * sur le point d'entr�e</li>
 * </ul>
 * </ul> <br>
 * <b><code>public static void preMain(BootContext)</code></b> est utile lorsque
 * des �tapes de d�marrage doivent �tre ajout�es au booteur (FwkBoot). L'appel
 * de <code>public static void preMain()</code> permet au point d'entr�e
 * d'ajouter des <code>FwkBootStep</code> sur le booteur avant que ce dernier
 * n'effectue l'amor�age. <br>
 * 
 * @author JayJay
 */
public class ApplicationManager {

    private static final Logger log = Logger.getLogger(ApplicationManager.class);

    private static AppManagerTask appManagerTask;



    public static void main(String[] args) {
        EDT.start();
        FwkEventQueue.instance().install();
        if (args.length < 1) {
            throw new IllegalArgumentException("No entry point class provided !"); //$NON-NLS-1$
        }
        String entryPoint = args[0];

        log.info("main(): Loading entry point class :" + entryPoint + "..."); //$NON-NLS-1$ //$NON-NLS-2$
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Class<?> entryPointClass = null;
        try {
            entryPointClass = cl.loadClass(entryPoint);
        } catch (Exception e) {
            throw new AppInfrastructureException("Could not load class %s !", e, entryPoint); //$NON-NLS-1$ //$NON-NLS-2$
        }

        log.info("main(): Setting Locale and TimeZone infos."); //$NON-NLS-1$
        String tz = System.getProperty("jnlp.user.timezone", "Europe/Paris"); //$NON-NLS-1$ $NON-NLS-2$ //$NON-NLS-2$
        TimeZone dtz = TimeZone.getTimeZone(tz);
        if (dtz != null) {
            TimeZone.setDefault(dtz);
        }
        String lang = System.getProperty("jnlp.user.language", System.getProperty("user.language", "fr")); //$NON-NLS-1$ $NON-NLS-2$ $NON-NLS-3$ //$NON-NLS-2$ //$NON-NLS-3$
        String coun = System.getProperty("jnlp.user.country", System.getProperty("user.country", null)); //$NON-NLS-1$ $NON-NLS-2$ //$NON-NLS-2$
        String vari = System.getProperty("jnlp.user.variant", System.getProperty("user.variant", null)); //$NON-NLS-1$ $NON-NLS-2$ //$NON-NLS-2$
        Locale locale = null;
        if (vari != null) {
            locale = new Locale(lang, coun, vari);
        } else if (coun != null) {
            locale = new Locale(lang, coun);
        } else {
            locale = new Locale(lang);
        }
        Locale.setDefault(locale);
        log.info("main(): timezone=" + tz + ", language=" + lang + ", country=" + coun + ", variant=" + vari); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$

        appManagerTask = new AppManagerTask();
        appManagerTask.start();

        String[] argss = new String[args.length - 1];
        if (argss.length > 0) {
            System.arraycopy(args, 1, argss, 0, argss.length);
        }
        FwkBoot booter = new FwkBoot();
        BootContext ctx = new BootContext(booter, argss);
        if (AppEntryPoint.class.isAssignableFrom(entryPointClass)) {
            AppEntryPoint appEntryPoint = null;
            try {
                appEntryPoint = (AppEntryPoint) entryPointClass.newInstance();
            } catch (Exception e) {
                throw new AppInfrastructureException("Could not create entry point instance !", e); //$NON-NLS-1$
            }
            appEntryPoint.preBoot(ctx);
            booter.boot(ctx);
            appEntryPoint.postBoot(ctx);
        } else {
            invokeStaticPreMain(entryPointClass, ctx);
            booter.boot(ctx);
            try {
                log.info("main(): invoking main(BootContext) on " + entryPoint + "..."); //$NON-NLS-1$ //$NON-NLS-2$
                invokeStaticMain(entryPointClass, ctx);
            } catch (AppInfrastructureException vie) {
                log.info("main(): No main(BootContext) method found on " + entryPointClass.getName() + ", trying main(String[])..."); //$NON-NLS-1$ //$NON-NLS-2$
                invokeStaticMain(entryPointClass, argss);
            }
        }
    }



    private static void invokeStaticPreMain(Class<?> entryPointClass, BootContext ctx) throws RuntimeException {
        Method preMain = null;
        try {
            preMain = entryPointClass.getMethod("preMain", BootContext.class); //$NON-NLS-1$
        } catch (Exception e) {
            log.info("invokePreMain(): No preMain(FwkBootContext) found on " + entryPointClass.getName()); //$NON-NLS-1$
            return;
        }
        try {
            preMain.invoke(null, (Object) ctx);
        } catch (InvocationTargetException ite) {
            throw new RuntimeException(ite.getCause().getMessage(), ite.getCause());
        } catch (Exception e) {
            throw new RuntimeException("Could not invoke public static void preMain(FwkBootContext) on " + entryPointClass + " !", e); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }



    private static void invokeStaticMain(Class<?> entryPointClass, BootContext ctx) {
        Method main = null;
        try {
            main = entryPointClass.getMethod("main", BootContext.class); //$NON-NLS-1$
        } catch (Exception e) {
            throw new AppInfrastructureException("Could not get public static void main(BootContext) on %s !", e, entryPointClass); //$NON-NLS-1$
        }
        try {
            main.invoke(null, (Object) ctx);
        } catch (InvocationTargetException ite) {
            throw new RuntimeException(ite.getCause().getMessage(), ite.getCause());
        } catch (Exception e) {
            throw new RuntimeException("Could not invoke public static void main(BootContext) on " + entryPointClass + " !", e); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }



    private static void invokeStaticMain(Class<?> entryPointClass, String[] args) {
        Method main = null;
        try {
            main = entryPointClass.getMethod("main", String[].class); //$NON-NLS-1$
        } catch (Exception e) {
            throw new RuntimeException("Could not get public static void main(String[]) on " + entryPointClass.getName() + " !", e); //$NON-NLS-1$ //$NON-NLS-2$
        }
        try {
            main.invoke(null, (Object) args);
        } catch (InvocationTargetException ite) {
            throw new RuntimeException(ite.getCause().getMessage(), ite.getCause());
        } catch (Exception e) {
            throw new RuntimeException("Could not invoke public static void main(String[]) on " + entryPointClass.getName() + " !", e); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }



    /**
     * Extrait les arguments de la ligne de commande sous forme d'une table
     * cl�-valeur.<br>
     * Le format de la ligne de commande est d�fini comme suit :<br>
     * nom1=valeur nom2=valeur2 nom3=valeur ... nomN=valeurN
     * 
     * @param args
     *            Arguments de la ligne de commande
     * @return Un table qui indexe les valeurs aux noms tel que d�crit ci-dessus
     */
    private static Map<String, String> parseArguments(String[] args) {
        Map<String, String> out = new TreeMap<String, String>();
        for (int i = 0; i < args.length; i++) {
            String[] tokens = args[i].split("="); //$NON-NLS-1$
            if (tokens.length >= 2) {
                int idx = args[i].indexOf("="); //$NON-NLS-1$
                if (idx < args[i].length() - 1) {
                    out.put(tokens[0], args[i].substring(idx + 1));
                }
            }
        }
        return out;
    }



    /**
     * <p>
     * Convert the specified {@link Properties} instance to a Map
     * </p>
     * <p>
     * The returned Map is built by invoking the toString() method for each keys
     * and values of the provided {@link Properties} object.
     * </p>
     * 
     * @param props
     *            the {@link Properties} instance to be converted to a
     *            {@link Map}
     * @return A {@link Map} that represents the provided {@link Properties}
     *         instance.
     */
    private static Map<String, String> toMap(Properties props) {
        Map<String, String> out = new TreeMap<String, String>();
        for (Entry<Object, Object> entry : props.entrySet()) {
            out.put(entry.getKey().toString(), entry.getValue().toString());
        }
        return out;
    }

}
