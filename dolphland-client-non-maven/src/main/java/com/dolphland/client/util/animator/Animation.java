package com.dolphland.client.util.animator;

/**
 * Representation of an animation
 * 
 * @author JayJay
 * 
 */
public class Animation {

    private AnimatedComponent component;
    private AnimationParameter parameter;



    /**
     * Constructor with component and parameter
     * 
     * @param component
     *            Component
     * @param parameter
     *            Parameter
     */
    public Animation(AnimatedComponent component, AnimationParameter parameter) {
        this.component = component;
        this.parameter = parameter;
    }



    /**
     * Get component
     * 
     * @return the component
     */
    public AnimatedComponent getComponent() {
        return component;
    }



    /**
     * Get parameter
     * 
     * @return Parameter
     */
    public AnimationParameter getParameter() {
        return parameter;
    }
}
