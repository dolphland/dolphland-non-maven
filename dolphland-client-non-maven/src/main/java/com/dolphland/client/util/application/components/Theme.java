package com.dolphland.client.util.application.components;

import java.awt.Color;
import java.awt.Font;

import javax.swing.UIManager;

class Theme {

	public static final Color SAVE_BUTTON_BACKGROUND = Color.GREEN;

	public static final Color HEADER_TITLE_FOREGROUND = Color.BLACK;

	public static final Color DEFAULT_BACKGROUND = new Color(236, 236, 236);

	public static final Color DEFAULT_VALUE_FOREGROUND = Color.BLUE;

	public static final Font DEFAULT_BOLD_FONT = new Font("Dialog", Font.BOLD, 14);

	public static final Font DEFAULT_REGULAR_FONT = new Font("Dialog", Font.PLAIN, 14);

	public static final Color BUTTON_BACKGROUND = new Color(204, 204, 204);

	public static final Color BUTTON_FOREGROUND = Color.BLACK;

	public static final Color BUTTON_SELECTED_BACKGROUND = UIManager.getColor("Table.selectionBackground");

	public static final Color BUTTON_SELECTED_FOREGROUND = Color.WHITE;

	public static final Color INFO_MESSAGE_FOREGROUND = Color.BLACK;

	public static final Color INFO_MESSAGE_BACKGROUND = Color.GREEN;

	public static final Font INFO_MESSAGE_FONT = DEFAULT_BOLD_FONT;

	public static final Color WARN_MESSAGE_FOREGROUND = Color.WHITE;

	public static final Color WARN_MESSAGE_BACKGROUND = Color.BLUE;

	public static final Font WARN_MESSAGE_FONT = DEFAULT_BOLD_FONT;

	public static final Color ERROR_MESSAGE_FOREGROUND = Color.WHITE;

	public static final Color ERROR_MESSAGE_BACKGROUND = Color.RED;

	public static final Font ERROR_MESSAGE_FONT = DEFAULT_BOLD_FONT;
	
	public static final Color STATE_MESSAGE_FOREGROUND = Color.BLACK;

	public static final Color STATE_MESSAGE_BACKGROUND = Color.ORANGE;

	public static final Font STATE_MESSAGE_FONT = DEFAULT_BOLD_FONT;
	
	public static final Color TEXTFIELD_BACKGROUND = Color.WHITE;
}
