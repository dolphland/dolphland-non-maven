package com.dolphland.client.util.action;

/**
 * Factory for action state creation
 * 
 * @author JayJay
 * 
 * @param <T>
 */
public interface UIActionEvtFactory<T extends UIActionEvt> {

    public T createUIActionEvt();

}
