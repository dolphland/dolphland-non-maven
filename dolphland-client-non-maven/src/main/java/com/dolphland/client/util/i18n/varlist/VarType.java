package com.dolphland.client.util.i18n.varlist;

/**
 * <p>
 * Define variable types used by the {@link Variable} class.
 * </p>
 * 
 * @author JayJay
 */
public enum VarType {

	STRING, CHAR, INTEGER, FLOAT, DATE

}
