package com.dolphland.client.util.action;

import java.util.List;

import javax.swing.Icon;

/**
 * Action listener used with UIAction that prevent when :
 * - enabled has changed
 * - visible has changed
 * - name has changed
 * - icon has changed
 * - description has changed
 * 
 * @author JayJay
 * 
 */
public abstract class UIActionAdapter implements UIActionListener {

    public void beforeExecution() {
    }



    public void afterExecution() {
    }



    public void enabledChanged(boolean enabled) {
    }



    public void visibleChanged(boolean visible) {
    }



    public void nameChanged(String name) {
    }



    public void iconChanged(Icon icon) {
    }



    public void descriptionChanged(String description) {
    }



    public void disabledCausesChanged(List<String> disabledCauses) {
    }
}
