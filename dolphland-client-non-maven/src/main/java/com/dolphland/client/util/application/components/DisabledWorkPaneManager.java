package com.dolphland.client.util.application.components;

import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLayeredPane;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

import com.dolphland.client.util.application.View;
import com.dolphland.client.util.application.WorkPane;
import com.dolphland.client.util.event.AppEvents;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.UIEndDisabledModeEvt;
import com.dolphland.client.util.event.UIStartDisabledModeEvt;

/**
 * Manager permettant de rendre opaque un workPane pour d�sactiver le workPane en affichant un message 
 * 
 * @author J�r�my Scafi
 */
class DisabledWorkPaneManager {

	// Panel permettant d'afficher un message
	private PanelMessage msgPane;
	
	// Indique si le panel de message a �t� ajout� au workPane
	private boolean msgPaneAdded;

	// WorkPane pouvant �tre d�sactiv�s
	private List<WorkPane> workPanes;
	
	// Listener permettant de redimensionner le panel de message lorsque le workPane est redimensionn�
	private ComponentListener resizeMsgPaneComponentListener;

	// Listener permettant de terminer le mode de d�sactivation du workPane lors d'un clic de souris
	private MouseListener endDisabledModeMouseListener;

	// Variables stockant les param�tres du mode de d�sactivation du workPane 
	private boolean disabledModeStarted;
	private String messageDisabledMode;
	private boolean mouseTraversableDisabledMode;

	// Subscriber permettant de s'inscrire aux �v�nements d'activation/d�sactivation 
	private DisabledWorkPaneEventSubscriber disabledWorkPaneEventSubscriber;
	
	/**
	 * Constructeur 
	 */
	public DisabledWorkPaneManager() {
		workPanes = new ArrayList<WorkPane>();
		
		// Cr�ation du listener permettant de redimensionner le panel de message lorsque le workPane est redimensionn�
		resizeMsgPaneComponentListener = new ComponentAdapter(){
			@Override
			public void componentResized(ComponentEvent e) {
				WorkPane wpWithView = getWorkPaneWithView();
				if(wpWithView != null){
					getMsgPane().setSize(wpWithView.getView().getSize());
					for(WorkPane wp : workPanes){
						wp.validate();
					}
					getMsgPane().validate();
				}
			}
		};
		
		// Cr�ation du listener permettant de terminer le mode de d�sactivation du workPane lors d'un clic de souris
		endDisabledModeMouseListener = new MouseAdapter(){
			@Override
			public void mouseClicked(MouseEvent e) {
				endDisabledMode();
			}
		};
		
		disabledWorkPaneEventSubscriber = new DisabledWorkPaneEventSubscriber();
		// Inscription aux �v�nements d'activation/d�sactivation de l'alerte d'identification de nappe
		EDT.subscribe(disabledWorkPaneEventSubscriber)	
		.forEvents(AppEvents.UI_START_DISABLED_MODE_EVT)
		.forEvents(AppEvents.UI_END_DISABLED_MODE_EVT)
		;
	}

	/**
	 * Appel� pour se d�sinscrire des �v�nements  
	 */
	public void freeEDT(){
		EDT.unsubscribe(disabledWorkPaneEventSubscriber).forAllEvents();	
	}
	
	/**
	 * Ajoute un workPane pouvant �tre d�sactiv�
	 */
	public void addWorkPane(WorkPane workPane) {
		// Ajout le listener de redimensionnement
		if(workPane != null){
			// Ajoute le workPane
			workPanes.add(workPane);
			workPane.addComponentListener(resizeMsgPaneComponentListener);
		}
	}
	
	/**
	 * Renvoie le premier workpane avec une vue
	 */
	private WorkPane getWorkPaneWithView() {
		for(WorkPane wp : workPanes){
			if(wp.getView() != null){
				return wp;
			}
		}
		return null;
	}
	
	/**
	 * Fournit le panel de message en l'ajoutant au workPane si non d�j� fait
	 */
	private PanelMessage getMsgPane(){
		if(msgPane == null){
			msgPane = new PanelMessage();
			msgPane.setVisible(false);
		}
		// Si le panel de message n'a pas �t� ajout�, on tente de l'ajouter au workPane
		if(!msgPaneAdded){
			WorkPane wpWithView = getWorkPaneWithView();
			if(wpWithView != null){
				View view = wpWithView.getView();
				JRootPane rootPane = view.getRootPane();
				if(rootPane != null) {
					JLayeredPane lp = rootPane.getLayeredPane();
					Point loc = view.getLocation();
					Point toLoc = SwingUtilities.convertPoint(view, loc, lp);
					msgPane.setSize(view.getSize());
					msgPane.setLocation(toLoc);
					lp.add(msgPane, JLayeredPane.PALETTE_LAYER);
					lp.repaint();
					// Indique que le panel de message a �t� ajout�
					msgPaneAdded = true;
				}
			}
		}		
		return msgPane;
	}
	
	/**
	 * D�sactive le WorkPane en affichant un message
	 * @param message Message � afficher
	 */
	public void startDisabledMode(String message){
		startDisabledMode(message, true);
	}
	
	/**
	 * D�sactive le WorkPane en affichant un message tout en indiquant si la souris peut traverser ou non
	 * @param message Message � afficher
	 * @param mouseTraversable Indique si la souris peut traverser ou non
	 */
	public void startDisabledMode(String message, boolean mouseTraversable){
		
		// D�finit le panel de message
		getMsgPane().setText(message);
		getMsgPane().setMouseTraversable(mouseTraversable);
		
		// Affiche le panel de message
		getMsgPane().setVisible(true);
		
		// Si la souris peut traverser, on ajoute le listener permettant de terminer le mode de d�sactivation du workPane lors d'un clic de souris 
		if(mouseTraversable){
			getMsgPane().addMouseListener(endDisabledModeMouseListener);
		}
		
		// Stockage des param�tres du mode de d�sactivation du workPane 
		disabledModeStarted = true;
		messageDisabledMode = message;
		mouseTraversableDisabledMode = mouseTraversable;
	}
	
	/**
	 * R�active le WorkPane
	 */
	public void endDisabledMode(){
		// Enl�ve le listener permettant de terminer le mode de d�sactivation du workPane lors d'un clic de souris 
		getMsgPane().removeMouseListener(endDisabledModeMouseListener);
		
		// N'affiche plus le panel de message
		getMsgPane().setVisible(false);
		
		// Indique que le mode de d�sactivation du workPane n'est plus d�marr� 
		disabledModeStarted = false;
		messageDisabledMode = null;
		mouseTraversableDisabledMode = false;
	}
	
	public class DisabledWorkPaneEventSubscriber extends DefaultEventSubscriber {
		
		public void eventReceived(final UIStartDisabledModeEvt e) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					startDisabledMode(e.getMsg(), e.isMouseTraversable());
				}
			});
		}
		
		public void eventReceived(final UIEndDisabledModeEvt e) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					endDisabledMode();
				}
			});
		}
	}
}
