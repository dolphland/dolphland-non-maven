package com.dolphland.client.util.input;

import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import org.apache.log4j.Logger;

import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.UIHideCtxInfoEvt;
import com.dolphland.client.util.event.UIShowCtxInfoEvt;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.swingverifiers.Afficheur;

/**
 * JTextField permettant de saisir des caract�res
 */
public class CharacterTextField extends JTextField {

    private final static Logger log = Logger.getLogger(CharacterTextField.class);

    private static final MessageFormater fmt = MessageFormater.getFormater(CharacterTextField.class);

    /**
     * Signe par d�faut
     */
    public final static char DEFAULT_SIGN = '-';

    /**
     * Liste des s�parateurs possibles
     */
    public static final char FRENCH_SEPARATOR = ',';
    public static final char ENGLISH_SEPARATOR = '.';

    /**
     * S�parateur par d�faut
     */
    public final static char DEFAULT_SEPARATOR = ENGLISH_SEPARATOR;

    /**
     * Valeur maximale
     */
    public static int MAX_VALUE = 256;

    /**
     * Next focus d�faut
     */
    public static final boolean DEFAULT_FOCUS_NEXT_ENABLED = true;

    /**
     * Liste des policys de case
     */
    public static final int NORMAL_CASE = 0;
    public static final int ALWAYS_UPPER_CASE = 1;
    public static final int ALWAYS_LOWER_CASE = 2;

    /**
     * Liste des policys de compl�tion
     */
    public static final int NO_COMPLETION = 0;
    public static final int COMPLETION_WHEN_INPUT = 1;
    public static final int COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO = 2;

    /**
     * Compl�tion automatique par d�faut
     */
    public static final int DEFAULT_POLICY_COMPLETION = COMPLETION_WHEN_INPUT;

    /**
     * Dur�e d'affichage de l'info bulle par d�faut
     */
    public final static long DEFAULT_CTXINFO_LIFE_TIME = 3000;

    /**
     * Indicateur de si la bulle ctxInfo est active ou non par d�faut
     */
    public final static boolean DEFAULT_CTXINFO_ENABLED = true;

    /**
     * Variables de classe
     */
    private int nbChar;
    private String representation;
    private String enCoursInsertString;
    private boolean focusNextEnabled;
    private int casePolicy;
    private Afficheur afficheur;
    private Afficheur ctxInfoAfficheur;
    private boolean insertionOK;
    private int completionPolicy;
    private boolean ctxInfoEnabled;
    private long ctxInfoLifeTime;
    protected char separator;
    protected boolean insertionAutomatique;

    /**
     * Liste des focusListener du composant
     */
    private List<FocusListener> focusListeners = null;



    /**
     * construit un CharacterTextField
     */
    public CharacterTextField() {
        this(MAX_VALUE);
    }



    /**
     * construit un CharacterTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     */
    public CharacterTextField(int nbChar) {
        this(nbChar, null);
    }



    /**
     * construit un CharacterTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     */
    public CharacterTextField(int nbChar, Afficheur afficheur) {
        this(nbChar, afficheur, DEFAULT_FOCUS_NEXT_ENABLED);
    }



    /**
     * construit un CharacterTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     * @param focusNextEnabled
     *            : transfert le focus au prochain composant focusable � la fin
     *            de la saisie
     */
    public CharacterTextField(int nbChar, Afficheur afficheur, boolean focusNextEnabled) {
        this.focusListeners = new ArrayList<FocusListener>();
        this.nbChar = nbChar;
        this.afficheur = afficheur != null ? afficheur : new DefaultAfficheur();
        this.ctxInfoAfficheur = new CtxInfoAfficheur();
        this.focusNextEnabled = focusNextEnabled;
        this.representation = ""; //$NON-NLS-1$
        this.enCoursInsertString = ""; //$NON-NLS-1$
        this.separator = DEFAULT_SEPARATOR;
        this.completionPolicy = DEFAULT_POLICY_COMPLETION;
        this.ctxInfoEnabled = DEFAULT_CTXINFO_ENABLED;
        this.ctxInfoLifeTime = DEFAULT_CTXINFO_LIFE_TIME;
        this.insertionOK = false;
        this.insertionAutomatique = false;
        initialize();
        updateFormat();
    }



    @Override
    public boolean isFocusable() {
        // Emp�che le focus d'un composant non �ditable, non enabled ou non
        // visible
        return super.isFocusable() && isEditable() && isEnabled() && isVisible();
    }



    // METHODES PUBLIQUES

    /**
     * Fixe le nombre de caract�res
     */
    public void setNbChar(int nbChar) {
        this.nbChar = nbChar;
        updateFormat();
    }



    /**
     * D�finit l'afficheur
     */
    public void setAfficheur(Afficheur afficheur) {
        this.afficheur = afficheur != null ? afficheur : new DefaultAfficheur();
        updateFormat();
    }



    /**
     * Fixe le next focus
     */
    public void setFocusNextEnabled(boolean focusNextEnabled) {
        this.focusNextEnabled = focusNextEnabled;
        updateFormat();
    }



    /**
     * Fixe la policy
     */
    public void setCasePolicy(int casePolicy) {
        if (casePolicy != ALWAYS_UPPER_CASE && casePolicy != ALWAYS_LOWER_CASE && casePolicy != NORMAL_CASE) {
            throw new IllegalArgumentException("invalid casePolicy"); //$NON-NLS-1$
        }
        this.casePolicy = casePolicy;
        updateFormat();
    }



    /**
     * Fournit le nombre de caract�res
     */
    public int getNbChar() {
        return nbChar;
    }



    /**
     * Fournit la policy
     */
    public int getCasePolicy() {
        return casePolicy;
    }



    /**
     * Indique si le next focus est actif
     */
    public boolean isFocusNextEnabled() {
        // Next focus possible si pas en mode insertion automatique
        return focusNextEnabled && !insertionAutomatique;
    }



    /**
     * Fournit l'afficheur
     */
    public Afficheur getAfficheur() {
        return afficheur;
    }



    /**
     * Fournit la policy de compl�tion automatiquement
     */
    public int getCompletionPolicy() {
        return completionPolicy;
    }



    /**
     * fixe la policy de compl�tion automatiquement
     */
    public void setCompletionPolicy(int completionPolicy) {
        this.completionPolicy = completionPolicy;
        updateFormat();
    }



    /**
     * Indique si la bulle ctxInfo est active ou non
     */
    public boolean isCtxInfoEnabled() {
        return ctxInfoEnabled;
    }



    /**
     * Fixe si la bulle ctxInfo est active ou non
     * 
     * @param ctxInfoEnabled
     */
    public void setCtxInfoEnabled(boolean ctxInfoEnabled) {
        this.ctxInfoEnabled = ctxInfoEnabled;
        updateFormat();
    }



    /**
     * Fournit la dur�e de vie de la bulle ctxInfo
     */
    public long getCtxInfoLifeTime() {
        return ctxInfoLifeTime;
    }



    /**
     * Fixe la dur�e de vie de la bulle ctxInfo
     * 
     * @param ctxInfoLifeTime
     */
    public void setCtxInfoLifeTime(long ctxInfoLifeTime) {
        this.ctxInfoLifeTime = ctxInfoLifeTime;
    }



    /**
     * Renvoie vrai si la valeur est vide
     */
    public boolean isEmpty() {
        return StrUtil.isEmpty(stringValue());
    }



    /**
     * Renvoie la valeur sous la forme d'une cha�ne de caract�res (String)
     */
    public String stringValue() {
        return getText();
    }



    /**
     * Renvoie la valeur sous la forme d'un binaire (boolean)
     */
    public boolean booleanValue() {
        try {
            if (!StrUtil.isEmpty(getText())) {
                return Boolean.parseBoolean(getText());
            }
        } catch (NumberFormatException exc) {
            log.error("Unable to convert text", exc); //$NON-NLS-1$
        }

        return false;
    }



    /**
     * Renvoie la valeur sous la forme d'un byte
     */
    public byte byteValue() {
        try {
            if (!StrUtil.isEmpty(getText())) {
                return Byte.parseByte(getText());
            }
        } catch (NumberFormatException exc) {
            log.error("Unable to convert text", exc); //$NON-NLS-1$
        }

        return 0;
    }



    /**
     * Renvoie la valeur sous la forme d'un byte en indiquant le nombre de
     * d�cimales contenues dans l'entier
     */
    public byte byteValue(int nbDecimal) {
        double value = doubleValue();
        double valReel = value * (double) (Math.pow(10, nbDecimal));
        byte valTronquee = new Double(valReel).byteValue();
        // On arrondit
        byte valArrondie = valTronquee;
        if (valReel - ((double) valTronquee) > 0.5d) {
            valArrondie++;
        }
        return valArrondie;
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (int)
     */
    public int intValue() {
        try {
            if (!StrUtil.isEmpty(getText())) {
                return Integer.parseInt(getText());
            }
        } catch (NumberFormatException exc) {
            log.error("Unable to convert text", exc); //$NON-NLS-1$
        }

        return 0;
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (int) en indiquant le nombre
     * de d�cimales contenues dans l'entier
     */
    public int intValue(int nbDecimal) {
        double value = doubleValue();
        double valReel = value * (double) (Math.pow(10, nbDecimal));
        int valTronquee = new Double(valReel).intValue();
        // On arrondit
        int valArrondie = valTronquee;
        if (valReel - ((double) valTronquee) > 0.5d) {
            valArrondie++;
        }
        return valArrondie;
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (short)
     */
    public short shortValue() {
        try {
            if (!StrUtil.isEmpty(getText())) {
                return Short.parseShort(getText());
            }
        } catch (NumberFormatException exc) {
            log.error("Unable to convert text", exc); //$NON-NLS-1$
        }

        return 0;
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (short) en indiquant le
     * nombre de d�cimales contenues dans l'entier
     */
    public short shortValue(int nbDecimal) {
        double value = doubleValue();
        double valReel = value * (double) (Math.pow(10, nbDecimal));
        short valTronquee = new Double(valReel).shortValue();
        // On arrondit
        short valArrondie = valTronquee;
        if (valReel - ((double) valTronquee) > 0.5d) {
            valArrondie++;
        }
        return valArrondie;
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (long)
     */
    public long longValue() {
        try {
            if (!StrUtil.isEmpty(getText())) {
                return Long.parseLong(getText());
            }
        } catch (NumberFormatException exc) {
            log.error("Unable to convert text", exc); //$NON-NLS-1$
        }

        return 0;
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (long) en indiquant le nombre
     * de d�cimales contenues dans l'entier
     */
    public long longValue(int nbDecimal) {
        double value = doubleValue();
        double valReel = value * (double) (Math.pow(10, nbDecimal));
        long valTronquee = new Double(valReel).longValue();
        // On arrondit
        long valArrondie = valTronquee;
        if (valReel - ((double) valTronquee) > 0.5d) {
            valArrondie++;
        }
        return valArrondie;
    }



    /**
     * Renvoie la valeur sous la forme d'un r�el (double)
     */
    public double doubleValue() {
        try {
            if (!StrUtil.isEmpty(getText())) {
                return Double.parseDouble(getText().replace(separator, ENGLISH_SEPARATOR));
            }
        } catch (NumberFormatException exc) {
            log.error("Unable to convert text", exc); //$NON-NLS-1$
        }

        return 0;
    }



    /**
     * Renvoie la valeur sous la forme d'un r�el (float)
     */
    public float floatValue() {
        try {
            if (!StrUtil.isEmpty(getText())) {
                return Float.parseFloat(getText().replace(separator, ENGLISH_SEPARATOR));
            }
        } catch (NumberFormatException exc) {
            log.error("Unable to convert text", exc); //$NON-NLS-1$
        }

        return 0;
    }



    /**
     * Fixe la valeur � partir d'un texte (String)
     */
    public void setValue(String value) {
        setText(value);
    }



    /**
     * Fixe la valeur � partir d'un binaire primitif (boolean)
     */
    public void setValue(boolean value) {
        setValue(String.valueOf(value));
    }



    /**
     * Fixe la valeur � partir d'un entier primitif (long)
     */
    public void setValue(long value) {
        setValue(String.valueOf(value));
    }



    /**
     * Fixe la valeur � partir d'un entier primitif (long) en indiquant le
     * nombre de d�cimales contenues dans l'entier
     */
    public void setValue(long value, int nbDecimal) {
        setValue((double) value / Math.pow(10, nbDecimal));
    }



    /**
     * Fixe la valeur � partir d'un r�el primitif (double)
     */
    public void setValue(double value) {
        setValue(String.valueOf(value));
    }



    /**
     * Fixe la valeur � partir d'un binaire (Boolean)
     */
    public void setValue(Boolean value) {
        if (value == null) {
            setValue(false);
            return;
        }
        setValue(value.booleanValue());
    }



    /**
     * Fixe la valeur � partir d'un byte (Byte)
     */
    public void setValue(Byte value) {
        if (value == null) {
            setValue(0);
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un byte (Byte) en indiquant le nombre de
     * d�cimales contenues dans l'entier
     */
    public void setValue(Byte value, int nbDecimal) {
        if (value == null) {
            setValue(0);
            return;
        }
        if (nbDecimal > 0) {
            setValue(value.doubleValue() / Math.pow(10, nbDecimal));
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un entier (Integer)
     */
    public void setValue(Integer value) {
        if (value == null) {
            setValue(0);
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un entier (Integer)
     */
    public void setValue(Integer value, int nbDecimal) {
        if (value == null) {
            setValue(0);
            return;
        }
        if (nbDecimal > 0) {
            setValue(value.doubleValue() / Math.pow(10, nbDecimal));
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un entier (Short)
     */
    public void setValue(Short value) {
        if (value == null) {
            setValue(0);
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un entier (Short)
     */
    public void setValue(Short value, int nbDecimal) {
        if (value == null) {
            setValue(0);
            return;
        }
        if (nbDecimal > 0) {
            setValue(value.doubleValue() / Math.pow(10, nbDecimal));
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un entier (Long)
     */
    public void setValue(Long value) {
        if (value == null) {
            setValue(0);
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un entier (Long)
     */
    public void setValue(Long value, int nbDecimal) {
        if (value == null) {
            setValue(0);
            return;
        }
        if (nbDecimal > 0) {
            setValue(value.doubleValue() / Math.pow(10, nbDecimal));
            return;
        }
        setValue(value.longValue());
    }



    /**
     * Fixe la valeur � partir d'un r�el (Double)
     */
    public void setValue(Double value) {
        if (value == null) {
            setValue(0);
            return;
        }
        setValue(value.doubleValue());
    }



    /**
     * Fixe la valeur � partir d'un r�el (Float)
     */
    public void setValue(Float value) {
        if (value == null) {
            setValue(0);
            return;
        }
        setValue(value.doubleValue());
    }



    /**
     * Les FocusListener sont lanc�s par CharacterTextFieldFocusListener
     */
    @Override
    public synchronized void addFocusListener(FocusListener l) {
        if (this.focusListeners == null) {
            super.addFocusListener(l);
        } else {
            this.focusListeners.add(l);
        }
    }



    @Override
    public synchronized void removeFocusListener(FocusListener l) {
        if (this.focusListeners == null) {
            super.removeFocusListener(l);
        } else {
            this.focusListeners.remove(l);
        }
    }



    // METHODES INTERNES AU PACKAGE

    /**
     * Initialise le CharacterTextField
     */
    protected void initialize() {
        super.addFocusListener(new CharacterTextFieldFocusListener());
        setDocument(new CharacterDocument());
    }



    /**
     * Mise � jour du contenu en fonction du format
     */
    protected void updateFormat() {
        setText(getText());
    }



    @Override
    public void setText(String t) {
        // D�but de l'insertion automatique
        insertionAutomatique = true;

        // On ins�re le texte en respectant le document
        super.setText(t);

        // On ex�cute le code de compl�tion Empty When Zero
        doCompletionEmptyWhenZero();

        // On ex�cute le code de compl�tion sauf si vide et empty when z�ro
        boolean enableEmptyWhenZero = getCompletionPolicy() == COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO;
        if (!StrUtil.isEmpty(getText()) || !enableEmptyWhenZero) {
            doCompletionConstraints();
        }

        // Fin de l'insertion automatique
        insertionAutomatique = false;
    }



    /**
     * Ex�cution de la compl�tion empty when zero
     */
    protected void doCompletionEmptyWhenZero() {
        boolean enableEmptyWhenZero = getCompletionPolicy() == COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO;
        if (enableEmptyWhenZero && !StrUtil.isEmpty(getText())) {
            double value = doubleValue();
            if (value == 0) {
                setText(null);
            }
        }
    }



    /**
     * Ex�cution des contraintes de la compl�tion
     */
    protected void doCompletionConstraints() {

    }



    /**
     * Affiche message de format invalide
     */
    protected void invalidFormat() {
        insertionOK = false;
        if (!insertionAutomatique) {
            Toolkit.getDefaultToolkit().beep();
            String message = fmt.format("CharacterTextField.RS_INVALID_FORMAT") + getCorrectFormat(); //$NON-NLS-1$
            afficheur.erreur(message);
            ctxInfoAfficheur.erreur(message);
        }
    }



    /**
     * @return le format correct
     */
    protected String getCorrectFormat() {
        return getNbChar() + fmt.format("CharacterTextField.RS_CHARACTERS"); //$NON-NLS-1$
    }



    protected boolean isInsertionOK() {
        return insertionOK;
    }



    /**
     * Fournit la repr�sentation
     */
    protected String getRepresentation() {
        return representation;
    }



    /**
     * Fixe la repr�sentation
     */
    protected void setRepresentation(String representation) {
        this.representation = representation;
    }



    private CharacterTextField getInstance() {
        return this;
    }



    @Override
    public void transferFocus() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                CharacterTextField.super.transferFocus();
            }
        });
    }

    /**
     * Document permettant de saisir des caract�res
     */
    protected class CharacterDocument extends PlainDocument {

        /**
         * insert la chaine str
         */
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            int len = str.length();
            String currentChar;
            int nbInsertionOK = 0;
            for (int cpt = 0; cpt < len; cpt++) {
                currentChar = String.valueOf(str.charAt(cpt));
                insertOneChar(offs + nbInsertionOK, currentChar, a);
                if (isInsertionOK()) {
                    nbInsertionOK++;
                }
            }
        }



        /**
         * insertion d'un seul caract�re
         */
        public void insertOneChar(int offs, String str, AttributeSet a) throws BadLocationException {
            afficheur.clear();
            ctxInfoAfficheur.clear();

            if (!isDansRepresentation(offs, str) || getRepresentation().length() < nbChar) {
                if (casePolicy == ALWAYS_UPPER_CASE) {
                    str = str.toUpperCase();
                } else {
                    if (casePolicy == ALWAYS_LOWER_CASE) {
                        str = str.toLowerCase();
                    }
                }
                insertionOK = true;
                super.insertString(offs, str, a);
                if (isDansRepresentation(offs, str)) {
                    addStringRepresentation(offs, str);
                    enCoursInsertString = enCoursInsertString.substring(0, offs) + str + enCoursInsertString.substring(offs + 1);
                }
                if (isFocusNextEnabled() && getRepresentation().length() == nbChar) {
                    // passe au champ suivant
                    transferFocus();
                }
                return;
            }
            invalidFormat();
        }



        /**
         * suppression de caract�res
         */
        protected void removeUpdate(DefaultDocumentEvent chng) {
            int offset = chng.getOffset();
            int length = chng.getLength();
            delStringRepresentation(offset, length);
        }



        /**
         * insertion effectu�e
         */
        protected void insertUpdate(DefaultDocumentEvent chng, AttributeSet attr) {
            super.insertUpdate(chng, attr);
            enCoursInsertString = CharacterTextField.this.getText();
        }



        /**
         * ajout du texte dans la repr�sentation
         * 
         * @param offset
         *            : la position de l'ajout
         * @param newString
         *            : la nouvelle chaine � ajouter
         */
        private void addStringRepresentation(int offset, String newString) {
            int offsetInRepresentation = getOffsetInRepresentation(offset);
            if (!getRepresentation().equals("")) { //$NON-NLS-1$
                representation = getRepresentation().substring(0, offsetInRepresentation) + newString + getRepresentation().substring(offsetInRepresentation, getRepresentation().length());
            } else {
                representation = newString;
            }
        }



        /**
         * supprime du texte dans la repr�sentation
         * 
         * @param offset
         *            : la position de la suppression
         * @param lenght
         *            : le nombre de caract�re a supprim�
         */
        private void delStringRepresentation(int offset, int lenght) {
            int offsetInRepresentation = getOffsetInRepresentation(offset);
            if (offsetInRepresentation < 0) {
                setRepresentation("");//efface tout //$NON-NLS-1$
            }
            else {
                StringBuffer resultat = new StringBuffer();
                resultat.append(getRepresentation().substring(0, offsetInRepresentation));

                int lenghtInRepresentation = 0, cpt = 0;
                while (cpt < lenght) {
                    if (isDansRepresentation(offset, enCoursInsertString.substring(offset + cpt, offset + cpt + 1))) {
                        lenghtInRepresentation++;
                    }
                    cpt++;
                }
                resultat.append(getRepresentation().substring(offsetInRepresentation + lenghtInRepresentation, getRepresentation().length()));

                setRepresentation(resultat.toString());
            }
        }



        /**
         * supprime un seul caract�re dans la repr�sentation
         * 
         * @param offset
         *            : la position de la suppression
         */
        protected int getOffsetInRepresentation(int offset) {
            if (offset <= 0)
                return 0;
            int decalage = 0;
            // enlever tous les caract�res pr�c�dents offset qui ne font pas
            // parti de la repr�sentation
            for (int i = 0; i < offset; i++) {
                if (!isDansRepresentation(i, enCoursInsertString.substring(i, i + 1))) {
                    decalage++;
                }
            }
            return offset - decalage;
        }



        /**
         * 
         * @param offset
         * @param newString
         * @return true si le texte donn�e fait partie de la repr�sentation
         */
        protected boolean isDansRepresentation(int offset, String newString) {
            return true;
        }
    }

    private class CharacterTextFieldFocusListener implements FocusListener {

        /**
         * Si gain de focus, s�lection du texte par d�faut
         */
        public void focusGained(java.awt.event.FocusEvent e) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    selectAll();
                }
            });

            // Ex�cution prot�g�e des focusGained du composant
            for (FocusListener focusListener : focusListeners) {
                try {
                    focusListener.focusGained(e);
                } catch (Exception exc) {
                    log.error("Error while executing focusGained", exc); //$NON-NLS-1$
                }
            }
        }



        /**
         * Si perte de focus, efface l'afficheur par d�faut
         */
        public void focusLost(FocusEvent e) {
            // D�but de l'insertion automatique
            insertionAutomatique = true;

            // On enl�ve l'affichage
            afficheur.clear();
            ctxInfoAfficheur.clear();

            // On ex�cute le code de compl�tion Empty When Zero
            doCompletionEmptyWhenZero();

            // On ex�cute le code de compl�tion sauf si vide et empty when z�ro
            boolean enableEmptyWhenZero = getCompletionPolicy() == COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO;
            if (!StrUtil.isEmpty(getText()) || !enableEmptyWhenZero) {
                doCompletionConstraints();
            }

            // Fin de l'insertion automatique
            insertionAutomatique = false;

            // Ex�cution prot�g�e des focusLost du composant
            for (FocusListener focusListener : focusListeners) {
                try {
                    focusListener.focusLost(e);
                } catch (Exception exc) {
                    log.error("Error while executing focusLost", exc); //$NON-NLS-1$
                }
            }
        }
    }

    /**
     * Afficheur par d�faut
     * 
     * @author jeremy.scafi
     */
    private class DefaultAfficheur implements Afficheur {

        /**
         * Clear du Afficheur
         */
        public void clear() {
        }



        /**
         * Erreur du Afficheur
         */
        public void erreur(String message) {
        }



        /**
         * Info du Afficheur
         */
        public void info(String message) {
        }
    }

    /**
     * Afficheur en utilisant des bulles CtxInfo
     * 
     * @author jeremy.scafi
     */
    private class CtxInfoAfficheur implements Afficheur {

        /**
         * Clear du Afficheur
         */
        public void clear() {
            if (isCtxInfoEnabled() && getParent() != null) {
                EDT.postEvent(new UIHideCtxInfoEvt(getInstance()));
            }
        }



        /**
         * Erreur du Afficheur
         */
        public void erreur(String message) {
            if (isCtxInfoEnabled() && getParent() != null) {
                UIShowCtxInfoEvt evt = new UIShowCtxInfoEvt(getInstance(), message, UIShowCtxInfoEvt.TOP);
                evt.setLifeTime(ctxInfoLifeTime);
                EDT.postEvent(evt);
            }
        }



        /**
         * Info du Afficheur
         */
        public void info(String message) {
            if (isCtxInfoEnabled() && getParent() != null) {
                UIShowCtxInfoEvt evt = new UIShowCtxInfoEvt(getInstance(), message, UIShowCtxInfoEvt.TOP);
                evt.setLifeTime(ctxInfoLifeTime);
                EDT.postEvent(evt);
            }
        }
    }
}
