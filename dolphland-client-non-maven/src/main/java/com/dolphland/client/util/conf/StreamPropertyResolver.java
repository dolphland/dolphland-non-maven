/*
 * $Log: StreamPropertyResolver.java,v $
 * Revision 1.5 2014/03/10 16:10:45 bvatan
 * - Internationalized
 * Revision 1.4 2008/12/17 09:05:08 bvatan
 * - refonte de l'interface des services. Les flux sont transmis par l'appelant.
 * Revision 1.3 2008/12/16 08:54:26 bvatan
 * - red�finition des interfaces des services pour une gestion correcte des flux
 * Revision 1.2 2008/12/05 13:47:07 bvatan
 * - ignore les propri�t� dont le nom contient un $
 * Revision 1.1 2008/11/27 07:38:34 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * Utilitaire qui permet de substituer des variables d'un flux de caract�res par
 * des valeurs
 * 
 * @author JayJay
 */
public class StreamPropertyResolver {

    /**
     * Remplace les variables d�clar�es dans le flux in par leur valeurs
     * sp�cifi�es dans props.<br>
     * Les variables sont rep�r�es comme suit : ${nomvariable}<br>
     * Exemple :<br>
     * <br>
     * <code>
     * 	Il est ${heure} heures
     * </code> <br>
     * <br>
     * Ci-dessus, ${heures} d�signe la variable heure.<br>
     * Si props contient la cl� "heure" alors les variables ${heure} seront
     * remplac�es par la valeur associ�e � "heure" dans props.<br>
     * <br>
     * Exemple si "heure" est associ�e � "15":<br>
     * <br>
     * <code>
     * 	Il est 15 heures
     * </code> <br>
     * <br>
     * Les variables non trouv�es dans props ne son pas remplac�es<br>
     * 
     * @param in
     *            Flux de caract�res contenant ou non des variables
     * @param props
     *            Liste des variables � remplacer (associ�es � leurs valeurs)
     * @param out
     *            Flux de destination ou les variable du flux in sont remplac�es
     *            par leur valeurs dans props
     * @return Un flux de caract�res o� les variables ont �t� remplac�es
     * @throws IOException
     *             Si une erreur de lecture/�criture s'est produite pendant la
     *             fusion
     */
    public static void mergeProperties(Reader in, Map<String, String> props, Writer out) throws IOException {
        if (in == null) {
            throw new NullPointerException("in is null !"); //$NON-NLS-1$
        }
        if (props == null) {
            throw new NullPointerException("props is null !"); //$NON-NLS-1$
        }
        if (out == null) {
            throw new NullPointerException("out is null !"); //$NON-NLS-1$
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        BufferedReader br = new BufferedReader(in);
        String line = null;
        while ((line = br.readLine()) != null) {
            pw.println(line);
        }
        String merged = sw.toString();
        for (String key : props.keySet()) {
            if (key.indexOf('$') < 0) {
                merged = merged.replaceAll("\\$\\{" + key + "\\}", props.get(key)); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }
        PrintWriter w = new PrintWriter(out);
        w.print(merged);
    }



    public static String mergeProperties(Reader in, Map<String, String> props) {
        if (in == null) {
            throw new NullPointerException("in is null !"); //$NON-NLS-1$
        }
        if (props == null) {
            throw new NullPointerException("props is null !"); //$NON-NLS-1$
        }
        StringWriter sw = new StringWriter();
        try {
            mergeProperties(in, props, sw);
        } catch (IOException ioe) {
            throw new AppInfrastructureException("Variables replacement has failed !", ioe); //$NON-NLS-1$
        }
        return sw.toString();
    }



    public static String mergeProperties(String in, Map<String, String> props) {
        if (in == null) {
            throw new NullPointerException("in is null !"); //$NON-NLS-1$
        }
        if (props == null) {
            throw new NullPointerException("props is null !"); //$NON-NLS-1$
        }
        StringWriter sw = new StringWriter();
        try {
            mergeProperties(new StringReader(in), props, sw);
        } catch (IOException ioe) {
            throw new AppInfrastructureException("Variables replacement has failed !", ioe); //$NON-NLS-1$
        }
        return sw.toString();
    }



    public static void mergeProperties(String in, Map<String, String> props, Writer out) {
        if (in == null) {
            throw new NullPointerException("in is null !"); //$NON-NLS-1$
        }
        if (props == null) {
            throw new NullPointerException("props is null !"); //$NON-NLS-1$
        }
        if (out == null) {
            throw new NullPointerException("out is null !"); //$NON-NLS-1$
        }
        try {
            mergeProperties(new StringReader(in), props, out);
        } catch (IOException ioe) {
            throw new AppInfrastructureException("The resolutions of variables failed !", ioe); //$NON-NLS-1$
        }
    }

}
