package com.dolphland.client.util.event;

public class EndLoadingAppEvt extends Event {

    public EndLoadingAppEvt() {
        super(FwkEvents.END_LOADING_APP_EVT);
    }

}
