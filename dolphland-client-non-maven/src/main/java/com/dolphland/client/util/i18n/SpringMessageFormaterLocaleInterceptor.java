package com.dolphland.client.util.i18n;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;

public class SpringMessageFormaterLocaleInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOG = Logger.getLogger(SpringMessageFormaterLocaleInterceptor.class);



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Locale locale = RequestContextUtils.getLocale(request);
        if (LOG.isDebugEnabled()) {
            LOG.debug("preHandle(): Looked up Locale <" + locale + "> from context. Initializing " + MessageFormater.class.getName() + " with it"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
        MessageFormater.setThreadLocale(locale);
        return true;
    }



    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (LOG.isDebugEnabled()) {
            LOG.debug("afterCompletion(): Clearing " + MessageFormater.class.getName() + " thread context Locale."); //$NON-NLS-1$ //$NON-NLS-2$
        }
        MessageFormater.clearThreadLocale();
    }

}
