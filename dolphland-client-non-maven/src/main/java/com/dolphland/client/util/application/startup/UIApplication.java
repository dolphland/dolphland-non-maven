package com.dolphland.client.util.application.startup;

import java.awt.Image;
import java.awt.Window;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.components.FwkSplashScreen;
import com.dolphland.client.util.conf.AppContext;
import com.dolphland.client.util.conf.AppEntryPoint;
import com.dolphland.client.util.conf.BootContext;
import com.dolphland.client.util.conf.FwkBoot;
import com.dolphland.client.util.conf.FwkBootStep;
import com.dolphland.client.util.event.CloseAppRequestEvt;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EndLoadingAppEvt;
import com.dolphland.client.util.event.FwkEvents;
import com.dolphland.client.util.event.Schedule;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.transaction.SwingJob;

/**
 * <h1>Overview</h1>
 * <p>
 * Specializes {@link AppEntryPoint} so that specifics GUI operation are taken
 * into account.
 * </p>
 * <p>
 * This class is responsible for handling things like preventing multiple
 * launches of the same application, loading master frame, ask user for login if
 * required etc.
 * </p>
 * <p>
 * It is as well responsible for notifiying subclasses when application has
 * startup, meaning that the {@link MasterFrame} has been made visible.
 * </p>
 * <p>
 * This class handles the following operations at application astartup :<br>
 * <li>If the <code>lock</code> argument is present on the command line (when
 * starting application), check that the application is not already running on
 * the port specified by the lock argument</li>
 * <li>Create the {@link MasterFrame} with
 * {@link UIApplication#createMasterFrame(UIApplicationContext)}</li>
 * <li>Display the splash screen if <code>fwk.ui.splashscreen.splashEnabled=true</code> has
 * been declared in configuration file</li>
 * <li>Request user for login if <code>security.login.required=true</code> has
 * been declared in configuration file</li>
 * <li>Watches for event {@link FwkEvents#END_LOADING_APP_EVT}: when received,
 * check that application boot process is completed then discard the splash
 * screen and display the {@link MasterFrame}.</li>
 * <br>
 * <b><font style="color:red">WARNING: {@link EndLoadingAppEvt} must be posted
 * so that master frame be made visible.</font></b>
 * </p>
 * <br>
 * <h1>Notifications</h1> <br>
 * <p>
 * <ol>
 * <li>When application starts up and before boot process takes place,
 * {@link UIApplication#init(UIApplicationContext)} method is invoked: it is
 * then time for implementors to register booter for extra {@link FwkBootStep}</li>
 * <li>When boot process has started and that configuration and services have
 * been loaded, {@link UIApplication#createMasterFrame(UIApplicationContext)} is
 * called to instruct the application that the {@link MasterFrame} must be
 * created. At this time, both configuration and services are available.</li>
 * <li>When boot process is completed, the
 * {@link UIApplication#startup(UIApplicationContext)} is invoked : at this
 * time, boot process is fully completed, user is logged in and can be lookep up
 * from {@link AppContext}, splash screen has been dsicarded and master frame
 * has been made visible.</li>
 * </ol>
 * </p>
 * 
 * @author JayJay
 */
abstract public class UIApplication extends AppEntryPoint implements MasterFrameFactory {

    private static final MessageFormater FMT = MessageFormater.getFormater(UIApplication.class);

    private EventWatcher eventWatcher = new EventWatcher();

    /**
     * master frame loading step that is responsible for loading the application
     * master frame
     */
    private MasterFrameBootStep masterFrameStep;

    /** splash screen step that handles splash screen display (or not) */
    private SplashScreenBootStep splashStep;

    /** true when {@link EndLoadingAppEvt} have been received */
    private boolean appLoaded;

    /** true when {@link FwkBoot} has completed the boot process */
    private boolean bootCompleted;

    /** captured {@link BootContext} reference in preBoot() */
    private BootContext bootContext;

    /** servicer socket used to prevent multiple launches of the application */
    @SuppressWarnings("unused") //$NON-NLS-1$
    private static ServerSocket server;



    /**
     * <p>
     * Default constructor
     * </p>
     */
    public UIApplication() {
        masterFrameStep = new MasterFrameBootStep(this);
        splashStep = new SplashScreenBootStep(masterFrameStep);
    }



    /**
     * <p>
     * <li>Check that the <code>lock</code> command line argument exist and if
     * so, prevent the application to lauch is another one is already running on
     * the port specified by le lock argument (e.g.: lock=1234)</li>
     * <li>Prepare the application for displaying splash screen and master frame
     * depending on the configuration properties (e.g.
     * <code>splashscreen.enabled=true</code>)</li>
     * <li>Handles user authentication when required by application
     * configuration</li>
     * </p>
     */
    @SuppressWarnings("deprecation") //$NON-NLS-1$
    @Override
    public final void preBoot(final BootContext ctx) {
        // capture BootContext reference for later use
        this.bootContext = ctx;
        // prevent multiple launches of the same application if lock argument is
        // present
        try {
            int lock = 0;
            try {
                lock = Integer.parseInt(ctx.getArg("lock")); //$NON-NLS-1$
            } catch (Exception e) {
                lock = 0;
            }
            if (lock > 0) {
                server = new ServerSocket(lock);
            }
        } catch (IOException e) {
            CloseAppRequestEvt cae = new CloseAppRequestEvt();
            cae.setSystemExitOnSuccess(true);
            EDT.schedule(Schedule.event(cae).startDate(System.currentTimeMillis() + 5000));
            JOptionPane.showMessageDialog(null, FMT.format("UIApplication.RS_APP_ALREADY_LOADED"));
            EDT.postEvent(cae);
            return;
        }
        // add extra boot steps and register event watcher
        EDT.subscribe(eventWatcher).forEvents(FwkEvents.END_LOADING_APP_EVT);
        FwkBoot booter = ctx.getBooter();
        // load master frame from context
        booter.addBootStep(masterFrameStep);
        // display splash screen if required
        booter.addBootStep(splashStep);
        SwingJob<?> job = new SwingJob<Object>() {

            @Override
            protected Object run() {
                UIApplicationContext uiCtx = new UIApplicationContext();
                uiCtx.setBootContext(ctx);
                init(uiCtx);
                return null;
            }
        };
        job.invokeAndWait();
    }



    /**
     * <p>
     * Hide splash screen and display master frame when the following conditions
     * have been met :<br>
     * <li>{@link EndLoadingAppEvt} have been received</li>
     * <li>Boot process is completed : always true when this method is called</li>
     * </p>
     */
    @Override
    public final void postBoot(final BootContext ctx) {
        SwingJob<?> job = new SwingJob<Object>() {

            @Override
            protected Object run() {
                bootCompleted = true;
                hideSplashAndShowMasterFrame();
                return null;
            }
        };
        job.invokeLater();
    }



    abstract public MasterFrame createMasterFrame(UIApplicationContext ctx);



    /**
     * <p>
     * Called when the application is about to start.
     * </p>
     * <p>
     * When this method is called, the boot process has yet started, it is still
     * time to add axtra {@link FwkBootStep} through the provided context.
     * </p>
     * 
     * @param ctx
     *            The UI context that contains the application startup context
     *            information. Beware that some information might be missing at
     *            this time because the boot process has not been executed yet.
     */
    public void init(UIApplicationContext ctx) {

    }



    /**
     * <p>
     * Called when application has finished to initialize and that all data has
     * been acquired
     * </p>
     * <p>
     * When this method is called, the master frame is visible and the splash
     * screen has disapeared which means that boot is completed and
     * {@link EndLoadingAppEvt} has been received.
     * </p>
     * 
     * @param ctx
     *            The UI context that contains the application startup context
     *            information
     */
    public void startup(UIApplicationContext ctx) {

    }



    /**
     * <p>
     * Hide the splash screen if it visible and show the application master
     * frame.<br>
     * If no splash screen is present the method deals with it and is fail safe.
     * <br>
     * If no master frame has been defined the method deals with it and is fail
     * safe as well.
     * </p>
     * <p>
     * After splash screen has been discraded and master frame has been made
     * visible, the startup() method is called
     * </p>
     * <p>
     * <b><font style="color:red">Required: To be invoked within AWT
     * thread</font></b>
     * </p>
     * 
     * @return true if conditions have been met to discard splash screen and
     *         make master frame visible or
     *         false otherwise
     */
    private boolean hideSplashAndShowMasterFrame() {
        if (bootCompleted && appLoaded) {
            splashStep.disposeSplash();
            masterFrameStep.show();
            UIApplicationContext uiCtx = new UIApplicationContext();
            uiCtx.setBootContext(bootContext);
            uiCtx.setMasterFrame(masterFrameStep.getMasterFrame());
            startup(uiCtx);
            return true;
        }
        return false;
    }



    /**
     * <p>
     * Watches the {@link EndLoadingAppEvt} that states that application all
     * data from the INFI part (real time system) has been received
     * </p>
     * 
     * @author JayJay
     */
    private class EventWatcher extends DefaultEventSubscriber {

        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(EndLoadingAppEvt e) {
            EDT.unsubscribe(this).forAllEvents();
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    appLoaded = true;
                    hideSplashAndShowMasterFrame();
                }
            });
        }
    }

}
