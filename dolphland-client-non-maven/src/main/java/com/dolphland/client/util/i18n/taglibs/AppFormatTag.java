package com.dolphland.client.util.i18n.taglibs;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;

/**
 * <p>
 * JSP Tag implementation to enable message formatting inside a JSP page while
 * still using the {@link MessageFormater} class from the framework
 * </p>
 * 
 * @author Julien Delrue
 * @author JayJay
 */
public class AppFormatTag extends TagSupport {

    private String key = StrUtil.EMPTY_STRING;

    private MessageFormater fmt = null;

    private String arg0 = null;
    private String arg1 = null;
    private String arg2 = null;
    private String arg3 = null;
    private String arg4 = null;

    private boolean arg0Set = false;
    private boolean arg1Set = false;
    private boolean arg2Set = false;
    private boolean arg3Set = false;
    private boolean arg4Set = false;



    @Override
    public int doStartTag() throws JspException {
        MessageFormater fmt = (MessageFormater) pageContext.getAttribute(AppFormatterConstants.FORMATTER);
        if (fmt == null) {
            throw new JspException("Unable to get the message formater"); //$NON-NLS-1$
        }

        this.fmt = fmt;
        return super.doStartTag();
    }



    /**
     * <p>
     * Check <code>arg</code> attribute list consistency. This method returns
     * false if the provided arg attribute list is not consistent according the
     * following rules:<br>
     * <ul>
     * <li>Arguments are named arg0, arg1, arg2, arg3 and arg4</li>
     * <li>An argument can be used only if its predecessor exist: e.g.
     * <code>arg0, arg1</code> is valid but not <code>arg0, arg2</code></li>
     * </ul>
     * More formally, the argument must follow the following pattern:<br>
     * <code> [arg0[,arg1[,arg2[,arg3[,arg4]]]]]</code>
     * </p>
     * 
     * @return true if argument list is consistent, false otherwise.
     */
    private boolean controlArgsConsistency() {
        if (!isArg0Set()) {
            // if no arg0 attribute is available then no other argx should exist
            if (isArg1Set() || isArg2Set() || isArg3Set() || isArg4Set()) {
                return false;
            }
        } else {
            // if arg0 attribute exists then check for arg list consistency
            // there can be no arg2 attribute if no arg1 exist, etc.
            if (isArg1Set()) {
                if (isArg2Set()) {
                    if (!isArg3Set()) {
                        // if no arg3 exist then no more argx should exist
                        if (isArg4Set()) {
                            // inconsistent
                            return false;
                        }
                    }
                } else {
                    // if no arg2 exist then no more argx should exist
                    if (isArg3Set() || isArg4Set()) {
                        return false;
                    }
                }
            } else {
                // si no arg1 exist then no more argx should exist
                if (isArg2Set() || isArg3Set() || isArg4Set()) {
                    return false;
                }
            }
        }
        return true;
    }



    @Override
    public int doEndTag() throws JspException {
        try {
            if (controlArgsConsistency()) {
                Map<String, Object> args = new HashMap<String, Object>();
                if (isArg0Set()) {
                    args.put("0", getArg0()); //$NON-NLS-1$
                }
                if (isArg1Set()) {
                    args.put("1", getArg1()); //$NON-NLS-1$
                }
                if (isArg2Set()) {
                    args.put("2", getArg2()); //$NON-NLS-1$
                }
                if (isArg3Set()) {
                    args.put("3", getArg3()); //$NON-NLS-1$
                }
                if (isArg4Set()) {
                    args.put("4", getArg4()); //$NON-NLS-1$
                }
                pageContext.getOut().print(fmt.format(getKey(), args));
            } else {
                throw new JspException("Arguments consistency is broken. Please set arguments in the right order starting with arg0 then arg1, arg2, etc. Formal pattern is [arg0[,arg1[,arg2[,arg3[,arg4]]]]]"); //$NON-NLS-1$
            }
        } catch (IOException e) {
            throw new JspException("I/O error occured while rendering VlrFormatTag !", e);
        }
        return super.doEndTag();
    }



    /**
     * <p>
     * Return the key attribute value
     * </p>
     * 
     * @return the key the key attribute value declared in the tag
     */
    public String getKey() {
        return key;
    }



    /**
     * @param key
     *            the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }



    /**
     * @return the fmt
     */
    public MessageFormater getFmt() {
        return fmt;
    }



    /**
     * @param fmt
     *            the fmt to set
     */
    public void setFmt(MessageFormater fmt) {
        this.fmt = fmt;
    }



    /**
     * <p>
     * Return the arg0 attribute value
     * </p>
     * 
     * @return The arg0 attribute value declared on the tag
     */
    public String getArg0() {
        return arg0;
    }



    /**
     * @param args0
     *            the args0 to set
     */
    public void setArg0(String arg0) {
        arg0Set = true;
        this.arg0 = arg0;
    }



    /**
     * <p>
     * Return the arg1 attribute value
     * </p>
     * 
     * @return The arg1 attribute value declared on the tag
     */
    public String getArg1() {
        return arg1;
    }



    /**
     * <p>
     * Set the arg1 attribute value on the tag
     * </p>
     * 
     * @param arg1
     *            the attribute value
     */
    public void setArg1(String arg1) {
        arg1Set = true;
        this.arg1 = arg1;
    }



    /**
     * <p>
     * Return the arg2 attribute value
     * </p>
     * 
     * @return The arg2 attribute value declared on the tag
     */
    public String getArg2() {
        return arg2;
    }



    /**
     * <p>
     * Set the arg2 attribute value on the tag
     * </p>
     * 
     * @param arg2
     *            the attribute value
     */
    public void setArg2(String arg2) {
        arg2Set = true;
        this.arg2 = arg2;
    }



    /**
     * <p>
     * Return the arg3 attribute value
     * </p>
     * 
     * @return The arg3 attribute value declared on the tag
     */
    public String getArg3() {
        return arg3;
    }



    /**
     * <p>
     * Set the arg3 attribute value on the tag
     * </p>
     * 
     * @param arg3
     *            the attribute value
     */
    public void setArg3(String arg3) {
        arg3Set = true;
        this.arg3 = arg3;
    }



    /**
     * <p>
     * Return the arg4 attribute value
     * </p>
     * 
     * @return The arg4 attribute value declared on the tag
     */
    public String getArg4() {
        return arg4;
    }



    /**
     * <p>
     * Set the arg4 attribute value on the tag
     * </p>
     * 
     * @param arg4
     *            the attribute value
     */
    public void setArg4(String arg4) {
        arg4Set = true;
        this.arg4 = arg4;
    }



    /**
     * <p>
     * 
     * </p>
     * @return the arg0Set
     */
    public boolean isArg0Set() {
        return arg0Set;
    }



    /**
     * @param arg0Set
     *            the arg0Set to set
     */
    public void setArg0Set(boolean arg0Set) {
        this.arg0Set = arg0Set;
    }



    /**
     * @return the arg1Set
     */
    public boolean isArg1Set() {
        return arg1Set;
    }



    /**
     * @param arg1Set
     *            the arg1Set to set
     */
    public void setArg1Set(boolean arg1Set) {
        this.arg1Set = arg1Set;
    }



    /**
     * @return the arg2Set
     */
    public boolean isArg2Set() {
        return arg2Set;
    }



    /**
     * @param arg2Set
     *            the arg2Set to set
     */
    public void setArg2Set(boolean arg2Set) {
        this.arg2Set = arg2Set;
    }



    /**
     * @return the arg3Set
     */
    public boolean isArg3Set() {
        return arg3Set;
    }



    /**
     * @param arg3Set
     *            the arg3Set to set
     */
    public void setArg3Set(boolean arg3Set) {
        this.arg3Set = arg3Set;
    }



    /**
     * @return the arg4Set
     */
    public boolean isArg4Set() {
        return arg4Set;
    }



    /**
     * @param arg4Set
     *            the arg4Set to set
     */
    public void setArg4Set(boolean arg4Set) {
        this.arg4Set = arg4Set;
    }

}
