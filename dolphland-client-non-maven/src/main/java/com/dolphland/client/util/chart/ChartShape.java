package com.dolphland.client.util.chart;

/**
 * Shapes used for a chart
 * 
 * @author JayJay
 * 
 */
public enum ChartShape {

    // Shape line
    LINE,

    // Shape circle
    CIRCLE,

    // Shape cross
    CROSS,

    // Shape diamond
    DIAMOND,

    // Shape down triangle
    DOWN_TRIANGLE,

    // Shape up triangle
    UP_TRIANGLE,

    // Shape left triangle
    LEFT_TRIANGLE,

    // Shape right triangle
    RIGHT_TRIANGLE,

    // Shape horizontal rectangle
    HORIZONTAL_RECTANGLE,

    // Shape vertical rectangle
    VERTICAL_RECTANGLE,

    // Shape square
    SQUARE,

    // Shape dot
    DOT
}
