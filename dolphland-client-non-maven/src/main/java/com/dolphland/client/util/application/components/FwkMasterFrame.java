package com.dolphland.client.util.application.components;

import info.clearthought.layout.TableLayout;

import java.awt.Dimension;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.WorkPane;
import com.dolphland.client.util.event.AppEvents;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EventMulticaster;
import com.dolphland.client.util.event.LoadDialogAProposAppEvt;
import com.dolphland.client.util.event.UIEndDisabledModeEvt;
import com.dolphland.client.util.event.UIStartDisabledModeEvt;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;

/**
 * La classe VisionFrame est l'impl�mentation de la MasterFrame pour Vision
 * constitu�e d'un ent�te, d'un menu, d'une barre de message et d'une barre
 * d'alertes. Il est possible de cacher chacun de ces �l�ments. Le workpane se
 * situe � droite du menu, en bas de l'ent�te et en haut des panels de message.
 * 
 * @author jscafi
 * 
 */
public abstract class FwkMasterFrame extends MasterFrame {

    private static final Logger log = Logger.getLogger(FwkMasterFrame.class);

    private AProposDialog dialogAPropos;
    
    private FwkUIManager uiManager;

    /**
     * Variables d'UI
     */
    private MessagePane messagePane;

    /**
     * Constructeur
     */
    public FwkMasterFrame() {
        super();
        uiManager = new FwkUIManager();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        new CtxInfoManager();
        subscribe();
        initialize();
    }

    private void subscribe() {
        EventMulticaster.getInstance()
            .subscribe(new LoadingEventSubscriber())
            .forEvent(AppEvents.LOAD_DIALOG_APROPOS_APP_EVT)
            ;
    }
    
    private void initialize() {
        setSize(new Dimension(1280, 1024));
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setContentPane(getContentPane());

        TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
        
        setLayout(layoutBuilder.get());
        
        add(getWorkPane(), layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
        .row(0)
        .width(TableLayout.FILL)
        .height(TableLayout.FILL)
        .get()));
        /*
        add(getMessagePane(), layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
        .row(1)
        .height(TableLayout.PREFERRED)
        .get()));*/
    }

    private AProposDialog getDialogAPropos() {

        if(dialogAPropos == null) {
            dialogAPropos = new AProposDialog(getWorkPane().getMasterFrame());
        }
        return dialogAPropos;
    }
    
    /**
     * This method initializes messagePane
     */
    private MessagePane getMessagePane() {
        if(messagePane == null) {
            messagePane = new MessagePane();
            messagePane.setPreferredSize(new Dimension(14, 30));
            messagePane.setMinimumSize(new Dimension(14, 30));
        }
        return messagePane;
    }
    
    /** METHODES PUBLIQUES UTILISABLES DE L'EXTERIEUR */

    /** M�thode cr�ant le workpane */
    public WorkPane createWorkPane() {
        return new FwkWorkPane(this);
    }

    /** Ajoute une info "A Propos" */
    public void addInfoAPropos(String libelle, String valeur) {
        getDialogAPropos().addInfo(libelle, valeur);
    }

    /** M�thode d'initialisation de la Frame */
    public void init(String[] args) {
        uiManager.initUIManager();
        super.init(args);
        addInfoAPropos("Version", getVersion()); //$NON-NLS-1$
    }

    /**
     * This method initializes messageBox
     */
    protected MessageBox createMessageBox() {
        MessageBox out = new MessageBox(this);
        return out;
    }

    /**
     * M�thodes impl�ment�es pour le Shower
     */
    public void showInfoDialog(String msg) {
        createMessageBox().showMsgInfo(msg);
    }

    public void showWarningDialog(String msg) {
        createMessageBox().showMsgWarning(msg);
    }

    public void showErrorDialog(String msg) {
        createMessageBox().showMsgError(msg);
    }

    public boolean showConfirmDialog(String msg) {
        return createMessageBox().showMsgConfirmationAlert(msg);
    }

    public String showInputDialog(String msg) {
        return createMessageBox().showInputDialog(msg);
    }

    public String showInputDialog(String msg, String initialInputValue) {
        return createMessageBox().showInputDialog(msg, initialInputValue);
    }

    public Object showInputDialog(String msg, Object[] elements) {
        return createMessageBox().showInputDialog(msg, elements);
    }

    public String showPasswordDialog(String msg) {
        return createMessageBox().showPasswordDialog(msg);
    }
    
    public String showPasswordDialog(String msg, String initialPasswordValue) {
        return createMessageBox().showPasswordDialog(msg, initialPasswordValue);
    }

    public void clearMessages() {
        getMessagePane().clearMsg();
        EDT.postEvent(new UIEndDisabledModeEvt());
    }

    public void showInfoMessage(final String message) {
        getMessagePane().setInfoMessage(message);
        EDT.postEvent(new UIStartDisabledModeEvt(message, false));
    }

    public void showWarningMessage(String message) {
        getMessagePane().setWarningMessage(message);
        EDT.postEvent(new UIStartDisabledModeEvt(message, true));
    }

    public void showErrorMessage(String message) {
        getMessagePane().setErrorMessage(message);
        EDT.postEvent(new UIStartDisabledModeEvt(message, true));
    }
    
    /**
     * Renvoie l'UIManager de l'application
     */
    public FwkUIManager getUIManager() {
        return uiManager;
    }

    /**
     * Fixe l'UIManager de l'application
     */
    public void setUiManager(FwkUIManager uiManager) {
        this.uiManager = uiManager;
    }
    
    public class LoadingEventSubscriber extends DefaultEventSubscriber {

        public void eventReceived(final LoadDialogAProposAppEvt rse) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    getDialogAPropos().init();
                    getDialogAPropos().setVisible(true);
                }
            });
        }
    }
}
