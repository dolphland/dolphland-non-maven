package com.dolphland.client.util.chart;

import org.apache.log4j.Logger;

/**
 * Classe d�finissant une entit� de donn�es d'un chart
 * 
 * @author jeremy.scafi
 */
abstract class ChartData {

    private final static Logger log = Logger.getLogger(ChartData.class);

    private int indSeries;
    private Comparable idChartData;
    private boolean visible;



    public ChartData(Comparable idChartData, int indSeries) {
        this.idChartData = idChartData;
        this.indSeries = indSeries;
        this.visible = true;
    }



    protected abstract void hideAllValues();



    protected abstract void showAllValues();



    Comparable getIdChartData() {
        return idChartData;
    }



    int getIndSeries() {
        return indSeries;
    }



    boolean isVisible() {
        return visible;
    }



    void setVisible(boolean visible) {
        if (this.visible != visible) {
            this.visible = visible;
            if (visible) {
                try {
                    showAllValues();
                } catch (Exception e) {
                    log.error("Unable to display entity's values", e); //$NON-NLS-1$
                }
            }
            else {
                try {
                    hideAllValues();
                } catch (Exception e) {
                    log.error("Unable to hide entity's values", e); //$NON-NLS-1$
                }
            }
        }
    }
}
