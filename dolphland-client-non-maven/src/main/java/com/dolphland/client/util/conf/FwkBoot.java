/*
 * $Log: FwkBoot.java,v $
 * Revision 1.26 2014/03/10 16:10:46 bvatan
 * - Internationalized
 * Revision 1.25 2013/03/25 15:05:27 bvatan
 * - FWK-46: services.xml file is not stored on disk anymore
 * Revision 1.24 2013/01/24 10:37:03 bvatan
 * - rolled back to log4j logger
 * Revision 1.23 2013/01/24 10:28:47 bvatan
 * - moved log.fatal() occurences to log.error()
 * Revision 1.22 2012/12/17 12:26:20 bvatan
 * - added NON-NLS comments
 * - indented code
 * Revision 1.21 2012/07/24 08:52:45 bvatan
 * - boot() method now takes a BootContext argument instead of an array of
 * String
 * Revision 1.20 2012/07/23 13:49:28 bvatan
 * - added javadoc
 * - code formatted with new javanorms formatter
 * - added command line args when dispatching FwkBootEvent
 * Revision 1.19 2012/07/03 12:06:52 bvatan
 * - CEnvironement is set on FwkServiceLocator as soon as it is intialized and
 * not after spring container has been initialized. This make it possible for
 * beans in the spring container to access configuration in the early stage of
 * application start.
 * Revision 1.18 2012/05/02 14:27:48 bvatan
 * - utilisation API MessageFormater
 * Revision 1.17 2012/04/30 14:03:23 bvatan
 * - mise � niveau API cahrgement des string externalis�es
 * Revision 1.16 2012/03/08 14:47:34 bvatan
 * - I18N
 * Revision 1.15 2011/09/08 15:07:16 bvatan
 * - correction bug chargement du fichier de service Spring pour permettre
 * l'import de fichier de resources � partir du fichier de service SPring
 * Revision 1.14 2011/09/01 12:33:04 bvatan
 * - ajout de la version dans l'environement apr�s que application.xml a �t�
 * pars�
 * Revision 1.13 2010/10/11 12:27:01 bvatan
 * - restauration version pr�c�dente
 * Revision 1.12 2010/10/11 12:20:33 bvatan
 * - protection de l'appel � BootSetp.execute()
 * Revision 1.11 2010/01/28 15:30:09 bvatan
 * - ajout du chargement de application.xml � partir du r�pertoire de lancement
 * de l'application si tous les autres mode de localisation du fichier ont
 * �chou�s
 * Revision 1.10 2009/06/18 13:48:59 bvatan
 * - correction des traces de logs dans le constructeur
 * Revision 1.9 2009/01/28 13:34:43 bvatan
 * - positionnement de l'environement s�lectionn� sur le FwkServiceLocator
 * Revision 1.8 2008/12/16 08:54:02 bvatan
 * - correction utilisation des flux dans la r�solution des variables de ces
 * flux
 * Revision 1.7 2008/12/12 14:05:21 bvatan
 * - utilisation des varargs pour FwkBoot.boot()
 * Revision 1.6 2008/12/05 14:22:23 bvatan
 * - gestion desl isteners de boot
 * Revision 1.5 2008/12/03 13:29:00 bvatan
 * - resturturation code, correction bugs
 * Revision 1.4 2008/11/28 10:06:47 bvatan
 * - suppression du FwkBootListener
 * - les �v�nements passent tous par EDT
 * Revision 1.3 2008/11/27 16:30:02 bvatan
 * - mise � niveau suite renommage sur FwkBootListener
 * Revision 1.2 2008/11/27 16:29:00 bvatan
 * - suppression du splash screen et gestion des �tapes suppl�mentaires
 * Revision 1.1 2008/11/27 07:38:33 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import com.dolphland.client.util.event.BootAppEndEvt;
import com.dolphland.client.util.event.BootAppErrorEvt;
import com.dolphland.client.util.event.BootAppEvt;
import com.dolphland.client.util.event.BootAppStartEvt;
import com.dolphland.client.util.event.BootAppStepEvt;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EndLoadingAppEvt;
import com.dolphland.client.util.event.EventMulticaster;
import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.string.StrUtil;

/**
 * <h1><u>Overview</u></h1>
 * <p>
 * First object to be run by the framework (by {@link ApplicationManager}) and
 * responsible for loading, in that order :<br>
 * <li>Configuration file (application.xml) : mandatory</li>
 * <li>Services file (services.xml) : optionnal</li>
 * <li>Extra boot steps ({@link FwkBootStep}) that have been added to the
 * {@link FwkBoot} instance: optionnal</li>
 * </p>
 * <br>
 * <h1><u>Notifications</u></h1> <br>
 * <p>
 * <h2>FwkBootListener Notifications</h2>
 * <br>
 * The {@link FwkBoot} instance will load {@link FwkBootListener} when it is
 * instanciated.<br>
 * These listeners will be notified of the differents steps that occur during
 * the boot process.
 * </p>
 * <p>
 * {@link FwkBootListener} are registered by loading
 * <code>META-INF/fwkbootlisteners.inf</code> resources found in the classpath.<br>
 * A resourcefile contains a list of listeners classnames, each one declared on
 * a line.
 * </p>
 * <br>
 * <h2>BootAppEvt Notifications</h2><br>
 * During the boot process, {@link BootAppEvt} are delivered by the
 * {@link EventMulticaster}. Following events are delivered :<br>
 * <table border=1>
 * <tr>
 * <td><b>Event type</b></td>
 * <td><b>Description</b></td>
 * </tr>
 * <tr>
 * <td> {@link BootAppStartEvt}</td>
 * <td>Delivered when the boot process has just started. At this time,
 * {@link FwkBootListener} have already been notified of the boot start process.
 * </td>
 * </tr>
 * <tr>
 * <td>{@link BootAppStepEvt}</td>
 * <td>Delivered when a boot step has completed. Each step in the boot process
 * will emit this event when it's completed. This event own progress information
 * that could be used to display some kind of progress information.</td>
 * </tr>
 * <tr>
 * <td> {@link BootAppEndEvt}</td>
 * <td>Delivered when the boot process has just ended. At this time,
 * {@link FwkBootListener} have not yet been notified of the boot complete
 * event. When this event is delivered, it means that all boot steps have been
 * completed including extra boot steps ({@link FwkBootStep}). Be aware that
 * {@link FwkBootStep} may have spawn threads not miht not have been completed
 * their task when this event is tiggered.</td>
 * </tr>
 * </table>
 * <br>
 * 
 * @author JayJay
 */
public class FwkBoot {

    private static final Logger log = Logger.getLogger(FwkBoot.class);

    private static final String BOOT_LISTENERS_RESOURCE = "META-INF/fwkbootlisteners.inf"; //$NON-NLS-1$

    private static final int BOOT_STARTED_EVT = 1;

    private static final int CONF_LOADED_EVT = 2;

    private static final int SERVICES_LOADED_EVT = 3;

    private static final int BOOT_COMPLETE_EVT = 4;

    private static final long MINIMUM_TIME_BETWEEN_BOOT_APP_EVT_MS = 1000;

    /** Nom du fichier application charg� � partir du classpath */
    public static final String APPLICATION_XML = "application.xml"; //$NON-NLS-1$

    /** Nom du fichier spring qui contient la d�claration des services */
    public static final String SERVICES_XML = "services.xml"; //$NON-NLS-1$

    /** Configuration de l'application */
    private CApplication application;

    /** Environement s�lectionn� dans APPLICATION_XML */
    private CEnvironement environment;

    /** Liste des �tapes suppl�mentaires � ajouter � la s�quene de boot */
    private List<FwkBootStep> extraSteps;

    /** Liste des listeners de la s�quence de boot */
    private List<FwkBootListener> listeners;



    /**
     * Construit un nouveau booter
     */
    public FwkBoot() {
        extraSteps = new ArrayList<FwkBootStep>();
        listeners = loadFwkListeners(Thread.currentThread().getContextClassLoader());
        if (log.isInfoEnabled()) {
            log.info("(constructor): Charg� " + listeners.size() + " listener(s) de boot"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if (log.isDebugEnabled()) {
            for (FwkBootListener fwkBootListener : listeners) {
                log.debug("(constructor): Listener de boot " + fwkBootListener); //$NON-NLS-1$
            }
        }
    }



    /**
     * Retourne la configuration de l'application
     * 
     * @return La racine du fichier xml de configuration de l'application
     */
    public CApplication getApplication() {
        return application;
    }



    /**
     * Retourne l'environement s�lectionn� dans APPLICATION_XML
     * 
     * @return
     */
    public CEnvironement getEnvironment() {
        return environment;
    }



    /**
     * Extrait les arguments de la ligne de commande sous forme d'une table
     * cl�-valeur.<br>
     * Le format de la ligne de commande est d�fini comme suit :<br>
     * nom1=valeur nom2=valeur2 nom3=valeur ... nomN=valeurN
     * 
     * @param args
     *            Arguments de la ligne de commande
     * @return Un table qui indexe les valeurs aux noms tel que d�crit ci-dessus
     */
    private static Map<String, String> parseArguments(String[] args) {
        Map<String, String> out = new TreeMap<String, String>();
        for (int i = 0; i < args.length; i++) {
            String[] tokens = args[i].split("="); //$NON-NLS-1$
            if (tokens.length >= 2) {
                int idx = args[i].indexOf("="); //$NON-NLS-1$
                if (idx < args[i].length() - 1) {
                    out.put(tokens[0], args[i].substring(idx + 1));
                }
            }
        }
        return out;
    }



    /**
     * Ajoute une �tape suppl�mentaire � la s�quence de boot
     * 
     * @param step
     *            L'�tape � ajouter
     */
    public void addBootStep(FwkBootStep step) {
        if (step == null) {
            throw new NullPointerException("step est null !"); //$NON-NLS-1$
        }
        extraSteps.add(step);
    }



    /**
     * Charge les services d�clar�s dans le flux sp�cifi� par servicesUrl. Ce
     * flux est fusionn� avec les propri�t�s d�finies dans properties. Les
     * variables du flux services d�clar�es ${nomvariable} seront remplac�es par
     * leurs valeurs correspondantes dans properties.<br>
     * Les variables non trouv�es dans properties seront laiss�es inchang�es.
     * 
     * @param servicesUrl
     *            Fichier d'assemblage des services.
     * @param properties
     *            Liste des variables � remplacer dans le flux servicesUrl
     */
    private void loadServices(URL servicesUrl, Map<String, String> properties) {
        // chargement des services
        Reader servicesReader = null;
        try {
            servicesReader = new InputStreamReader(servicesUrl.openStream(), "UTF-8"); //$NON-NLS-1$
            if (log.isInfoEnabled()) {
                log.info("loadServices(): Chargement des services..."); //$NON-NLS-1$
            }
            Resource resource = new UrlResource(servicesUrl);
            FwkServiceLocator.getInstance().loadServices(resource, properties);
        } catch (Exception e) {
            log.error("loadServices(): Echec de chargement des services !", e); //$NON-NLS-1$
            throw new AppInfrastructureException("Echec de chargement des services !", e); //$NON-NLS-1$
        } finally {
            if (servicesReader != null) {
                try {
                    servicesReader.close();
                } catch (Exception e) {
                    // ignore
                }
            }
            if (servicesReader != null) {
                try {
                    servicesReader.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
    }



    /**
     * Charge la configuration � partir de l'url sp�cifi�.
     * 
     * @param confUrl
     *            URL du fichier APPLICATION_XML
     * @param params
     *            Param�tres de lancement de l'application
     * @return L'objet CApplication correspondant � la configration charg�e.
     */
    private CApplication loadConfiguration(URL confUrl, Map<String, String> params) {
        ConfLoader loader = new ConfLoader();
        CApplication cAppli = null;
        InputStreamReader confReader = null;
        Reader confResolved = null;
        try {
            if (log.isInfoEnabled()) {
                log.info("boot(): Chargement et r�solution des variables de " + APPLICATION_XML + "..."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            confReader = new InputStreamReader(confUrl.openStream(), "UTF-8"); //$NON-NLS-1$
            String merged = StreamPropertyResolver.mergeProperties(confReader, params);
            confResolved = new StringReader(merged);
            if (log.isInfoEnabled()) {
                log.info("boot(): Interpr�tation de " + APPLICATION_XML + "..."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            cAppli = loader.parse(confResolved);
        } catch (Exception e) {
            log.error("boot(): Echec de chargement de " + APPLICATION_XML + ", abandon.", e); //$NON-NLS-1$ //$NON-NLS-2$
            throw new AppInfrastructureException("Echec de chargement de " + APPLICATION_XML + " !", e); //$NON-NLS-1$ //$NON-NLS-2$
        } finally {
            if (confReader != null) {
                try {
                    confReader.close();
                } catch (Exception e) {
                    // ignore
                }
            }
            if (confResolved != null) {
                try {
                    confResolved.close();
                } catch (Exception e) {
                    // ignore
                }
            }
        }
        return cAppli;
    }



    private List<FwkBootListener> loadFwkListeners(ClassLoader cl) {
        Enumeration<URL> fwklstrs = null;
        try {
            fwklstrs = cl.getResources(BOOT_LISTENERS_RESOURCE); //$NON-NLS-1$
        } catch (IOException ioe) {
            log.error("boot(): Echec de localisation des listeners framework !", ioe); //$NON-NLS-1$
        }
        ArrayList<FwkBootListener> out = new ArrayList<FwkBootListener>();
        if (fwklstrs != null) {
            while (fwklstrs.hasMoreElements()) {
                URL fwklstr = fwklstrs.nextElement();
                InputStream is = null;
                try {
                    is = fwklstr.openStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is, "ASCII")); //$NON-NLS-1$
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        if (!StrUtil.isEmpty(line)) {
                            Class<?> listenerClass = null;
                            try {
                                listenerClass = cl.loadClass(line);
                            } catch (Throwable cause) {
                                log.warn("loadFwkListeners(): Impossible de charger la classe [" + line + "]", cause); //$NON-NLS-1$ //$NON-NLS-2$
                                continue;
                            }

                            if (FwkBootListener.class.isAssignableFrom(listenerClass)) {
                                FwkBootListener lstr = null;
                                try {
                                    try {
                                        Constructor<?> cons = listenerClass.getConstructor(FwkBoot.class);
                                        lstr = (FwkBootListener) cons.newInstance(this);
                                    } catch (Exception e) {
                                        lstr = (FwkBootListener) listenerClass.newInstance();
                                    }
                                } catch (Exception e) {
                                    log.error("loadFwkListeners(): Impossible d'instancier le listener  [" + listenerClass + "]", e); //$NON-NLS-1$ //$NON-NLS-2$
                                    continue;
                                }
                                out.add(lstr);
                            } else {
                                if (log.isInfoEnabled()) {
                                    log.info("loadFwkListeners(): Le listener [" + listenerClass + "] n'est pas un listener valide !"); //$NON-NLS-1$ //$NON-NLS-2$
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    log.error("loadFwkListeners(): Impossible de charger " + fwklstr + " !", e); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        }
        return out;
    }



    private void dispatchEvent(int evt, FwkBootEvent e) {
        for (FwkBootListener l : listeners) {
            try {
                switch (evt) {
                    case BOOT_STARTED_EVT :
                        l.bootStarted(e);
                        break;
                    case CONF_LOADED_EVT :
                        l.confLoaded(e);
                        break;
                    case SERVICES_LOADED_EVT :
                        l.servicesLoaded(e);
                        break;
                    case BOOT_COMPLETE_EVT :
                        l.bootComplete(e);
                        break;
                }
            } catch (Throwable cause) {
                log.error("boot(): Un listener de boot a provoqu� une erreur !", cause); //$NON-NLS-1$
            }
        }
    }



    /**
     * <p>
     * Starts boot process
     * </p>
     * 
     * @param ctx
     *            Boot context instance from {@link ApplicationManager}
     */
    public void boot(BootContext ctx) {
        try {
            bootImpl(ctx);
        } catch (Exception e) {
            log.error("boot(): Error pendant le d�marrage de l'application", e); //$NON-NLS-1$
            EDT.postEvent(new BootAppErrorEvt("Une erreur s'est produite pendant le d�marrage de l'application.", e)); //$NON-NLS-1$
        }
    }



    /**
     * Poste un {@link BootAppEvt}
     */
    private void postBootAppEvt(BootAppEvt bae) {
        EDT.postEvent(bae);
        try {
            Thread.sleep(MINIMUM_TIME_BETWEEN_BOOT_APP_EVT_MS);
        } catch (InterruptedException e) {
            // Do nothing
        }
    }



    private void bootImpl(BootContext ctx) {
        String[] args = ctx.getArgs();
        if (log.isInfoEnabled()) {
            log.info("boot(): D�but de la s�quence de boot"); //$NON-NLS-1$
        }
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        dispatchEvent(BOOT_STARTED_EVT, new FwkBootEvent(this, args));
        EDT.start();
        // 1 �tape + les �tapes suppl�mentaires
        int nbSteps = extraSteps.size() + 1;
        BootAppStartEvt be = new BootAppStartEvt();
        be.setNbSteps(nbSteps);
        be.setStepIndex(-1);
        be.setStep(null);
        postBootAppEvt(be);
        // ANALYSE DES PARAMETRES DE LANCEMENT
        Map<String, String> params = parseArguments(args);
        if (log.isInfoEnabled()) {
            log.info("boot(): Boot avec les param�tres :" + params); //$NON-NLS-1$
        }
        String env = params.get("env"); //$NON-NLS-1$
        if (env == null) {
            log.warn("boot(): Pas d'environement sp�cifi� dans les param�tres !"); //$NON-NLS-1$
        } else {
            if (log.isInfoEnabled()) {
                log.info("boot(): S�lection de l'environement [" + env + "]"); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }

        // CHARGEMENT DE LA CONFIGURATION
        CApplication cAppli = null;
        if (log.isInfoEnabled()) {
            log.info("boot(): Localisation du fichier " + APPLICATION_XML + "..."); //$NON-NLS-1$ //$NON-NLS-2$
        }
        URL confUrl = null;
        // Obtention du param�tre conf s'il existe
        String confParam = params.get("conf"); //$NON-NLS-1$
        if (confParam == null || confParam.trim().length() == 0) {
            log.info("boot(): Param�tre conf non sp�cifi� au lancement, recherche dans le classpath."); //$NON-NLS-1$
            confUrl = cl.getResource(APPLICATION_XML);
        } else {
            if (log.isInfoEnabled()) {
                log.info("boot(): Param�tre conf sp�cifi� au lancement, chargement depuis [" + confParam + "]"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            try {
                confUrl = new URL(confParam);
            } catch (Exception e) {
                log.info("Le param�tre conf sp�cifi� n'est pas un URL valide [" + confParam + "], recherche du chemin dans le classpath..."); //$NON-NLS-1$ //$NON-NLS-2$
                confUrl = cl.getResource(confParam);
                if (confUrl == null) {
                    log.warn("boot(): [" + confParam + "] n'a pas �t� trouv� dans le classpath !"); //$NON-NLS-1$ //$NON-NLS-2$
                    log.info("boot(): Recherche de <" + confParam + "> dans le r�pertoire courant..."); //$NON-NLS-1$ //$NON-NLS-2$
                    File confFile = new File(confParam);
                    if (!confFile.exists()) {
                        log.error("boot(): Impossible de trouver <" + confFile.getAbsolutePath() + ">"); //$NON-NLS-1$ //$NON-NLS-2$
                    } else {
                        try {
                            confUrl = confFile.toURL();
                        } catch (Exception ex) {
                            log.error("boot(): Impossible d'obtenir l'URL du fichier <" + confFile.getAbsolutePath() + "> !", ex); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                    }
                }
            }
        }
        if (confUrl == null) {
            log.error("Impossible de localiser la configuration, Abandon !"); //$NON-NLS-1$
            throw new AppInfrastructureException("Impossible de localiser la configuration, Abandon !"); //$NON-NLS-1$
        } else {
            if (log.isInfoEnabled()) {
                log.info("boot(): Chargement de la configuration � partir de [" + confUrl + "] ..."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            cAppli = loadConfiguration(confUrl, params);
        }
        // SELECTION DE L'ENVIRONEMENT
        CEnvironement environment = null;
        if (env == null) {
            log.error("boot(): Pas d'environement sp�cifi� ! Abandon !"); //$NON-NLS-1$
            throw new AppInfrastructureException("Aucun environement sp�cifi�, abandon du d�marrage !"); //$NON-NLS-1$
        }
        // obtention de l'environement s�lectionn�
        environment = cAppli.getEnvironement(env);
        if (environment == null) {
            log.error("boot(): L'environement [" + env + "] n'existe pas dans " + APPLICATION_XML + ", abandon !"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
            throw new AppInfrastructureException("L'environement [" + env + "] n'existe pas dans " + APPLICATION_XML + " !"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
        FwkServiceLocator.getInstance().setEnvironment(environment);
        CProperty versionProperty = new CProperty();
        versionProperty.setName("version"); //$NON-NLS-1$
        versionProperty.setValue(cAppli.getVersion());
        environment.addProperty(versionProperty);
        dispatchEvent(CONF_LOADED_EVT, new FwkBootEvent(this, args, environment));
        // extraction des variables � partir de APPLICATION_XML et fusion
        // avec SERVICE_XML
        if (log.isInfoEnabled()) {
            log.info("boot(): Extraction des variables r�sultantes de " + APPLICATION_XML + "..."); //$NON-NLS-1$ //$NON-NLS-2$
        }
        Map<String, String> properties = cAppli.extractProperties(environment);
        if (log.isInfoEnabled()) {
            for (String key : properties.keySet()) {
                log.info("boot(): " + key + "=" + properties.get(key)); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }

        // CHARGEMENT DES SERVICES
        URL servicesUrl = null;
        String pServices = params.get("services"); //$NON-NLS-1$
        if (pServices == null || pServices.trim().length() == 0) {
            if (log.isInfoEnabled()) {
                log.info("boot(): Param�tre services non sp�cifi�, chargement � partir de " + SERVICES_XML + " � partir du classpath..."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            servicesUrl = cl.getResource(SERVICES_XML);
        } else {
            if (log.isInfoEnabled()) {
                log.info("boot(): Param�tre service sp�cifi� au lancement [" + pServices + "] chargement � partir de cet URL..."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            try {
                servicesUrl = new URL(pServices);
            } catch (MalformedURLException mfe) {
                if (log.isInfoEnabled()) {
                    log.info("boot(): [" + pServices + "] n'est pas un URL valide, recherche du chemin dans le classpath..."); //$NON-NLS-1$ //$NON-NLS-2$
                }
                servicesUrl = cl.getResource(pServices);
                if (servicesUrl == null) {
                    log.warn("boot(): [" + pServices + "] n'a pas �t� trouv� dans le classpath !"); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        }
        if (servicesUrl == null) {
            log.warn("boot(): Descripteur de services non trouv�, la couche service ne sera pas initialis�e !"); //$NON-NLS-1$
        } else {
            if (log.isInfoEnabled()) {
                log.info("boot(): Chargement des services � partir de [" + servicesUrl + "]"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            loadServices(servicesUrl, properties);
        }

        this.application = cAppli;
        this.environment = environment;

        dispatchEvent(SERVICES_LOADED_EVT, new FwkBootEvent(this, args, environment));

        // ETAPES SUPPLEMENTAIRES
        Iterator<FwkBootStep> it = extraSteps.iterator();
        int i = 0;
        while (it.hasNext()) {
            FwkBootStep extraStep = it.next();
            BootAppStepEvt bse = new BootAppStepEvt();
            bse.setEnvironment(env);
            bse.setStep(extraStep);
            bse.setNbSteps(nbSteps);
            bse.setStepIndex(i);
            bse.setStepText(extraStep.getName());
            postBootAppEvt(bse);
            // les exceptions sont catch�es dans le service appelant
            extraStep.setBootContext(ctx);
            extraStep.execute(this);
            i++;
        }

        // FIN DE SEQUENCE DE BOOT
        BootAppEndEvt bee = new BootAppEndEvt();
        bee.setApplication(cAppli);
        bee.setEnvironment(env);
        bee.setStep(null);
        bee.setStepText("Config complete"); //$NON-NLS-1$
        postBootAppEvt(bee);
        dispatchEvent(BOOT_COMPLETE_EVT, new FwkBootEvent(this, args, environment));

        EDT.postEvent(new EndLoadingAppEvt());
    }
}
