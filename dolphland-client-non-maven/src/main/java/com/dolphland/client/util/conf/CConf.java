/*
 * $Log: CConf.java,v $
 * Revision 1.3 2014/03/10 16:07:01 bvatan
 * - Internationalized
 * Revision 1.2 2009/04/01 09:33:13 jscafi
 * - Résolution des warnings
 * Revision 1.1 2008/11/27 07:38:34 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CConf {

    private String id;

    private Map<String, CProperty> properties;

    private Map<String, CConf> confs;



    public CConf() {
        properties = new TreeMap<String, CProperty>();
        confs = new TreeMap<String, CConf>();
    }



    public String getId() {
        return id;
    }



    public void setId(String id) {
        this.id = id;
    }



    public void addProperty(CProperty p) {
        properties.put(p.getName(), p);
    }



    public Set<String> getPropertiesIds() {
        return properties.keySet();
    }



    public CProperty getProperty(String name) {
        return properties.get(name);
    }



    public void addConf(CConf conf) {
        confs.put(conf.getId(), conf);
    }



    public List<CConf> getConfs() {
        return new ArrayList<CConf>(confs.values());
    }



    public CConf getConf(String id) {
        return confs.get(id);
    }



    @Override
    public String toString() {
        String out = "(Conf:id=" + id; //$NON-NLS-1$
        Set<String> names = getPropertiesIds();
        for (String name : names) {
            out += ",(" + name + "=" + getProperty(name).getValue() + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
        out += ")"; //$NON-NLS-1$
        return out;
    }
}
