/*
 * $Log: Scheduler.java,v $
 * Revision 1.9 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.8 2012/12/17 12:44:47 bvatan
 * - added NON-NLS comments
 * Revision 1.7 2010/11/12 15:47:13 bvatan
 * - Ajout support des schedules de type crontab
 * Revision 1.6 2010/01/28 15:32:15 bvatan
 * - ajout trace de log sur l'ajout d'un Schedule au scheduler
 * Revision 1.5 2009/07/15 10:27:42 bvatan
 * - coreection bug sur �mission �v�nement : la destination n'�tait pas
 * sp�cifi�e
 * Revision 1.4 2009/04/17 10:53:13 jscafi
 * - Suppression warnings inutiles
 * Revision 1.3 2009/04/17 08:03:02 bvatan
 * - correction divers bug de synchro
 * Revision 1.2 2009/03/27 06:54:48 bvatan
 * - private package
 * Revision 1.1 2009/03/24 13:53:09 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.apache.log4j.Logger;

/**
 * Scheduler d'�v�nements
 * 
 * @author JayJay
 */
class Scheduler {

    private static final Logger log = Logger.getLogger(Scheduler.class);

    private static final EventDestination MY_DESTINATION = new EventDestination();

    private List<CrontabSchedule> crontabSchedules;

    private TreeSet<Schedule> schedules;

    private volatile Thread thread;

    private volatile boolean shouldRun;

    private EventWatcher eventWatcher;



    Scheduler() {
        crontabSchedules = new ArrayList<CrontabSchedule>();
        schedules = new TreeSet<Schedule>();
        eventWatcher = new EventWatcher();
    }



    synchronized void start() {
        if (shouldRun) {
            return;
        }
        thread = new Thread(new Runner(), "Scheduler"); //$NON-NLS-1$
        thread.setDaemon(true);
        shouldRun = true;
        thread.start();
        // scruter toutes les 1 seconde les CrontabSchedule
        EDT.subscribe(eventWatcher).forEvents(MY_DESTINATION);
        EDT.schedule(Schedule.event(new Event(MY_DESTINATION)).interval(1000));
    }



    synchronized void stop() {
        if (!shouldRun) {
            return;
        }
        shouldRun = false;
        thread.interrupt();
        EDT.unsubscribe(eventWatcher).forAllEvents();
    }



    void addSchedule(Schedule s) {
        if (s == null) {
            throw new NullPointerException("schedule is null !"); //$NON-NLS-1$
        }
        if (s.isRepeatable()) {
            if (s.getRepeatInterval() < 0) {
                throw new IllegalArgumentException("schedule repeatable without repeatInterval !"); //$NON-NLS-1$
            }
        }
        synchronized (schedules) {
            schedules.add(s);
            schedules.notify();
        }
        if (log.isDebugEnabled()) {
            log.debug("addSchedule(Schedule): Schedule added <" + s + ">"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }



    void addSchedule(CrontabSchedule s) {
        if (s == null) {
            throw new NullPointerException("schedule is null !"); //$NON-NLS-1$
        }
        synchronized (crontabSchedules) {
            crontabSchedules.add(s);
        }
        if (log.isDebugEnabled()) {
            log.debug("addSchedule(CrontabSchedule): Schedule added <" + s + ">"); //$NON-NLS-1$ //$NON-NLS-2$
        }
    }



    private boolean isMissFire(Schedule s) {
        boolean missfire = false;
        if (System.currentTimeMillis() - s.getExecTime() > 20) {
            missfire = true;
        }
        return missfire;
    }



    private void reschedule(Schedule s) {
        if (isMissFire(s)) {
            switch (s.getMissFirePolicy()) {
                case Schedule.MISSFIRE_RESCHEDULE_CLOCK_TIME :
                    s.setExecTime(System.currentTimeMillis() + s.getRepeatInterval());
                    break;
                case Schedule.MISSFIRE_RESCHEDULE_EXEC_TIME :
                case Schedule.MISSFIRE_NO_FIRE :
                    s.setExecTime(s.getExecTime() + s.getRepeatInterval());
                    break;
            }
        } else {
            s.setExecTime(s.getExecTime() + s.getRepeatInterval());
        }
        synchronized (schedules) {
            schedules.add(s);
        }
    }



    /*
     * private void logSchedules() {
     * synchronized(schedules) {
     * Iterator<Schedule> it = schedules.iterator();
     * log.debug("logSchedules(): ---------- current schedules ----------");
     * while(it.hasNext()) {
     * log.debug(it.next());
     * }
     * log.debug("logSchedules(): ---------------------------------------");
     * }
     * }
     */

    private Schedule nextSchedule() throws InterruptedException {
        Schedule next = null;
        while (true) {
            synchronized (schedules) {
                if (schedules.size() > 0) {
                    next = schedules.first();
                    if (next.isGarbage()) {
                        if (log.isDebugEnabled()) {
                            log.debug("nextSchedule(): garbage :" + next); //$NON-NLS-1$
                        }
                        schedules.remove(next);
                        continue;
                    }
                } else {
                    // logSchedules();
                    schedules.wait();
                    continue;
                }
                long waitTime = next.getExecTime() - System.currentTimeMillis();
                if (waitTime > 0) {
                    if (log.isDebugEnabled()) {
                        log.debug("nextSchedule(): wait " + waitTime + " for " + next); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                    // logSchedules();
                    schedules.wait(waitTime);
                    continue;
                }
                schedules.remove(next);
            }
            if (next.isGarbage()) {
                if (log.isDebugEnabled()) {
                    log.debug("nextSchedule(): garbage :" + next); //$NON-NLS-1$
                }
                continue;
            }
            next.fire();
            if (next.isRepeatable()) {
                reschedule(next);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("nextSchedule(): not repeatable " + next); //$NON-NLS-1$
                }
            }
            return next;
        }
    }

    private class Runner implements Runnable {

        public void run() {
            try {
                while (shouldRun) {
                    Schedule s = nextSchedule();
                    if (isMissFire(s) && s.getMissFirePolicy() == Schedule.MISSFIRE_NO_FIRE) {
                        continue;
                    } else {
                        if (log.isDebugEnabled()) {
                            log.debug("run(): running schedule " + s); //$NON-NLS-1$
                        }
                        EDT.postEvent(s.getEvent(), s.getDestination());
                    }
                }
            } catch (InterruptedException ie) {
                if (shouldRun) {
                    log.error("run(): Unexpected interruption !", ie); //$NON-NLS-1$
                }
                shouldRun = false;
            }
            thread = null;
        }
    }



    int size() {
        synchronized (schedules) {
            return schedules.size();
        }
    }

    private class EventWatcher extends DefaultEventSubscriber {

        public void eventReceived(Event e) {
            List<CrontabSchedule> events = new ArrayList<CrontabSchedule>();
            synchronized (crontabSchedules) {
                List<CrontabSchedule> garbage = new ArrayList<CrontabSchedule>();
                for (CrontabSchedule sched : crontabSchedules) {
                    if (sched.isGarbage()) {
                        garbage.add(sched);
                    } else if (sched.isTriggerable()) {
                        events.add(sched);
                    }
                }
                for (CrontabSchedule sched : garbage) {
                    crontabSchedules.remove(sched);
                }
            }
            for (CrontabSchedule sched : events) {
                EDT.postEvent(sched.getEvent(), sched.getDestination());
                sched.fired();
            }
        }
    }

}
