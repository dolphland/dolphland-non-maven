package com.dolphland.client.util.event;

/**
 * Ev�nement d�livr� lorsqu'on stoppe le mode inactif
 * 
 * @author JayJay
 * 
 */
public class UIEndDisabledModeEvt extends UIEvent {

    public UIEndDisabledModeEvt() {
        super(AppEvents.UI_END_DISABLED_MODE_EVT);
    }
}
