package com.dolphland.client.util.table.demo;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;


public class BooleanTableCellEditor extends DefaultCellEditor{

    
    public BooleanTableCellEditor() {
        super(new JCheckBox());
    }
}
