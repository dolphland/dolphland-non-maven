/*
 * $Log: FwkEvents.java,v $
 * Revision 1.14 2012/12/17 12:18:19 bvatan
 * - added NON-NLS comments
 * Revision 1.13 2010/07/22 09:04:59 bvatan
 * - gestion de l'installation des logs de l'application dans un r�pertoire
 * local de la machine qui ex�cute l'appli
 * Revision 1.12 2009/08/21 13:55:57 bvatan
 * - ajout d'une destination pour les �v�nements du relayeur d'�v�nements
 * Revision 1.11 2009/06/23 08:24:36 bvatan
 * - passage des EventId pour les �v�nement de fermeture de l'application en
 * EventDestination
 * Revision 1.10 2009/06/22 07:40:05 bvatan
 * - ajout AppClosedEvent
 * Revision 1.9 2009/06/18 10:58:20 jscafi
 * - Am�liorations du d�marrage d'applications
 * Revision 1.8 2009/03/05 15:33:51 bvatan
 * - rollback suite r�gression
 * Revision 1.7 2009/03/05 15:13:04 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.6 2008/12/04 16:01:28 bvatan
 * - ajout de LOGS_REQUEST_EVT
 * Revision 1.5 2008/12/03 13:30:03 bvatan
 * - ajout ev�ne�ment erreur lors de la s�quence de boot
 * Revision 1.4 2008/11/28 10:07:23 bvatan
 * - ajout des �v�nements relatifs � l'amor�age de l'appli
 * Revision 1.3 2008/11/27 16:35:11 bvatan
 * - ajotu des EventDestination pour les �v�nements applicatifs
 * Revision 1.2 2008/02/04 09:05:15 bvatan
 * - les ids r�serv�s au fwk commencent � 1 milliard
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/27 07:25:01 bvatan
 * - initital check in
 */
package com.dolphland.client.util.event;

public class FwkEvents {

    private static int nextFwkEvtId = 1000000000;



    public static int nextFwkId() {
        synchronized (FwkEvents.class) {
            return nextFwkEvtId++;
        }
    }

    /**
     * D�signe les �v�nements transportant des exceptions lev�es qui n'ont pas
     * �t� trait�es
     */
    public static final EventDestination FWK_UNHANDLED_EXCEPTION_EVENT = new EventDestination("FWK_UNHANDLED_EXCEPTION_EVENT"); //$NON-NLS-1$

    /** Ev�nement poubelle non intercept� */
    public static final EventId DEVNULL_EVT = new EventId("DEVNULL_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� au d�marrage du chargement des donn�es de l'application
     * (c'est � dire apr�s la s�quence de boot)
     */
    public static final EventId START_LOADING_APP_EVT = new EventId("DEBUT_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� pour chaque �tape de la s�quence de chargement des
     * donn�es de l'application
     */
    public static final EventId STEP_LOADING_APP_EVT = new EventId("STEP_LOADING_APP_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� � la fin de la s�quence de chargement des donn�es de
     * l'application
     */
    public static final EventId END_LOADING_APP_EVT = new EventId("FIN_EVT"); //$NON-NLS-1$

    /** Ev�nement post� lors d'une demande de sortie de l'application */
    public static final EventDestination CLOSE_APP_REQUEST_EVT = new EventDestination("CLOSE_APP_REQUEST_EVT"); //$NON-NLS-1$

    /** Ev�nement post� lors de la phase de pr�-fermeture de l'application */
    public static final EventDestination APP_CLOSING_EVT = new EventDestination("APP_CLOSING_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� lors de la phase de fermeture d�finitive de l'application
     */
    public static final EventDestination APP_CLOSED_EVT = new EventDestination("APP_CLOSED_EVT"); //$NON-NLS-1$

    public static final EventId SHOW_LOADING_APP_EVT = new EventId("SHOW_LOADING_APP_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� au d�marrage de la s�quence d'amor�age de l'application
     * (FwkBoot)
     */
    public static final EventId BOOT_APP_START_EVT = new EventId("BOOT_APP_START_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� avant chaque �tap de la s�quence d'amor�age de
     * l'application (FwkBoot)
     */
    public static final EventId BOOT_APP_STEP_EVT = new EventId("BOOT_APP_STEP_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� � la fin de la s�quence d'amor�age de l'application
     * (FwkBoot)
     */
    public static final EventId BOOT_APP_END_EVT = new EventId("BOOT_APP_END_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� lorsqu'une erreur s'est produite pendant la s�quence de
     * boot (FwkBoot)
     */
    public static final EventId BOOT_APP_ERROR_EVT = new EventId("BOOT_APP_ERROR_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� lorsqu'une demande d'afichage/masquage des logs a �t�
     * efectu�e
     */
    public static final EventId LOGS_REQUEST_EVT = new EventId("REQUEST_LOGS_EVT"); //$NON-NLS-1$

    /** Ddestination des �v�nements �mis par les relayeurs d'�v�nements infi */
    public static final EventDestination EVENT_RELAYER_STATE_EVT = new EventDestination("EVENT_RELAYER_STATE_EVT"); //$NON-NLS-1$

    /**
     * Ev�nement post� pour d�clencher la redirection des logs dans le
     * r�pertoire d�fini dans les param�tres de lancement de l'application
     * (logs.dir et logs.label)
     */
    public static final EventDestination INSTALL_LOG_EVT = new EventDestination("INSTALL_LOG_EVT"); //$NON-NLS-1$
}
