package com.dolphland.client.util.layout;

/**
 * Cette classe repr�sente un Builder permettant de construire un
 * <code>TableLayoutConstraints</code>. <br>
 * <br>
 * La classe <code>TableLayoutConstraints</code> sp�cifie les contraintes pour
 * un composant utilisant comme layout la classe <code>TableLayout</code>. <br>
 * <br>
 * L'utilisation de cette classe se fait via la m�thode
 * <code>addComponent</code> de la classe <code>TableLayoutBuilder</code>. <br>
 * <br>
 * Exemple d'utilisation :<br>
 * <br>
 * <ol>
 * <li>Cr�ation et affectation du TableLayout au conteneur <code>
 * <pre> 	
 * TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
 * panel.setLayout(layoutBuilder.get());
 * </pre>
 * </code> <br>
 * <li>Ajout d'un composant au conteneur <code>
 * <pre> 
 * panel.add(getLabel(), layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
 * .width(TableLayout.FILL)
 * .height(TableLayout.FILL)
 * .row(0)
 * .column(0)
 * .get()
 * ));
 * </pre>
 * </code>
 * </ol>
 * 
 * @author jeremy.scafi
 */
public class TableLayoutConstraintsBuilder {

    private TableLayoutConstraints tableLayoutConstraints;



    /**
     * Constructeur sans param�tre
     */
    public TableLayoutConstraintsBuilder() {
        tableLayoutConstraints = new TableLayoutConstraints();
    }



    /**
     * Constructeur avec un mod�le de <code>TableLayoutConstraints</code> en
     * param�tre.
     * 
     * @param tableLayoutConstraintsModele
     *            Mod�le de <code>TableLayoutConstraints</code>.
     */
    public TableLayoutConstraintsBuilder(TableLayoutConstraints tableLayoutConstraintsModele) {
        tableLayoutConstraints = new TableLayoutConstraints(tableLayoutConstraintsModele);
    }



    /**
     * Cette m�thode fixe le champ leftInsets du
     * <code>TableLayoutConstraints</code>. <br>
     * Ce champ repr�sente la marge gauche du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param leftInsets
     *            Marge gauche du composant.
     */
    public TableLayoutConstraintsBuilder leftInsets(double leftInsets) {
        tableLayoutConstraints.setLeftInsets(leftInsets);
        return this;
    }



    /**
     * Cette m�thode fixe le champ rightInsets du
     * <code>TableLayoutConstraints</code>. <br>
     * Ce champ repr�sente la marge droite du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param rightInsets
     *            Marge droite du composant.
     */
    public TableLayoutConstraintsBuilder rightInsets(double rightInsets) {
        tableLayoutConstraints.setRightInsets(rightInsets);
        return this;
    }



    /**
     * Cette m�thode fixe le champ topInsets du
     * <code>TableLayoutConstraints</code>. <br>
     * Ce champ repr�sente la marge haute du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param topInsets
     *            Marge haute du composant.
     */
    public TableLayoutConstraintsBuilder topInsets(double topInsets) {
        tableLayoutConstraints.setTopInsets(topInsets);
        return this;
    }



    /**
     * Cette m�thode fixe le champ bottomInsets du
     * <code>TableLayoutConstraints</code>. <br>
     * Ce champ repr�sente la marge basse du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param bottomInsets
     *            Marge basse du composant.
     */
    public TableLayoutConstraintsBuilder bottomInsets(double bottomInsets) {
        tableLayoutConstraints.setBottomInsets(bottomInsets);
        return this;
    }



    /**
     * Cette m�thode fixe le champ width du <code>TableLayoutConstraints</code>. <br>
     * Ce champ repr�sente la largeur du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param width
     *            Largeur du composant.
     */
    public TableLayoutConstraintsBuilder width(double width) {
        tableLayoutConstraints.setWidth(width);
        return this;
    }



    /**
     * Cette m�thode fixe le champ height du <code>TableLayoutConstraints</code>
     * . <br>
     * Ce champ repr�sente la hauteur du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param height
     *            Hauteur du composant.
     */
    public TableLayoutConstraintsBuilder height(double height) {
        tableLayoutConstraints.setHeight(height);
        return this;
    }



    /**
     * Cette m�thode fixe le champ row du <code>TableLayoutConstraints</code>. <br>
     * Ce champ repr�sente la ligne du composant.
     * <p>
     * Les valeurs possibles sont entre 0 et n. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param row
     *            Ligne du composant.
     */
    public TableLayoutConstraintsBuilder row(int row) {
        tableLayoutConstraints.setRow(row);
        return this;
    }



    /**
     * Cette m�thode fixe le champ column du <code>TableLayoutConstraints</code>
     * . <br>
     * Ce champ repr�sente la colonne du composant.
     * <p>
     * Les valeurs possibles sont entre 0 et n. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param column
     *            Colonne du composant.
     */
    public TableLayoutConstraintsBuilder column(int column) {
        tableLayoutConstraints.setColumn(column);
        return this;
    }



    /**
     * Renvoie le <code>TableLayoutConstraints</code> construit
     * 
     * @return <code>TableLayoutConstraints</code> construit
     */
    public TableLayoutConstraints get() {
        return tableLayoutConstraints;
    }
}
