package com.dolphland.client.util.swingverifiers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.dolphland.client.util.i18n.MessageFormater;

/**
 * Verifier pour les champs de saisi devant produire des nombres flotants.
 * 
 * @author Bernard Noel
 * @author JayJay
 */
public class FloatVerifier extends AbstractInputVerifier {

    private static final MessageFormater fmt = MessageFormater.getFormater(FloatVerifier.class);

    private int tailleEntiers = 0;

    private int tailleDecimales = 0;



    /**
     * Construit un FloatVerifier
     * 
     * @param afficheur
     *            L'afficheur de message � utiliser
     * @param motif
     *            Le motif du nombre flottant � controler.<br>
     *            <b>Exemple: 3.2</b> indique un nombre d'au plus 3 chiffres
     *            pour partie enti�re et d'au
     *            plus 2 chiffres pour sa partie d�cimale : 127.52 est valide
     *            alors que 1562.12 ou 12.1456
     *            ne le sont pas.
     */
    public FloatVerifier(Afficheur afficheur, String motif) {
        super(afficheur);
        if (motif == null) {
            throw new NullPointerException("motif cannot be null !"); //$NON-NLS-1$
        }
        if (motif.trim().length() == 0) {
            throw new NullPointerException("motif is empty !"); //$NON-NLS-1$
        }
        String[] parts = motif.split("\\."); //$NON-NLS-1$
        if (parts.length != 2) {
            throw new IllegalArgumentException("motif must be of the x.y form"); //$NON-NLS-1$
        }
        int left;
        int right;
        try {
            left = Integer.parseInt(parts[0]);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(parts[0] + " is not an integer !"); //$NON-NLS-1$
        }
        try {
            right = Integer.parseInt(parts[1]);
        } catch (NumberFormatException nfe) {
            throw new IllegalArgumentException(parts[1] + " is not an integer !"); //$NON-NLS-1$
        }
        this.tailleEntiers = left;
        this.tailleDecimales = right;
    }



    /**
     * Construit un FloatVerifier
     * 
     * @param afficheur
     *            L'affichaure de messages
     * @param tailleEntiers
     *            nombre de chiffres de la partie enti�re.
     * @param tailleDecimales
     *            nombre de chiffres de la partie d�cimale.
     */
    public FloatVerifier(Afficheur afficheur, int tailleEntiers, int tailleDecimales) {
        super(afficheur);
        this.tailleEntiers = tailleEntiers;
        this.tailleDecimales = tailleDecimales;
    }



    public boolean verify(JComponent input) {
        if (input instanceof JTextField) {
            JTextField zone = (JTextField) input;

            // V�rification taille
            if (zone.getText().length() > getTailleTotale()) {
                afficherErreur(fmt.format("FloatVerifier.RS_SIZE_EXCEEDED", getTailleTotale())); //$NON-NLS-1$
                return false;
            }

            // V�rification format
            if (zone.getText().length() > 0) {
                Pattern p = Pattern.compile("([0-9]{1," + tailleEntiers + "})|(" + (tailleDecimales > 0 ? "[0-9]{1," + tailleEntiers + "}(\\.|\\,)[0-9]{1," + tailleDecimales + "}" : "") + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
                Matcher m = p.matcher(zone.getText().trim());
                if (!m.matches()) {
                    afficherErreur(fmt.format("FloatVerifier.RS_INVALID_FORMAT")); //$NON-NLS-1$
                    return false;
                }
            }
        }// End if (input instanceof JTextField)
        getAfficheur().clear();
        return true;
    }



    private int getTailleTotale() {
        return tailleEntiers + tailleDecimales + (tailleDecimales > 0 ? 1 : 0);
    }
}
