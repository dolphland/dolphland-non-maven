package com.dolphland.client.util.message;

/**
 * Enumeration for type message
 * 
 * @author JayJay
 * 
 */
public enum TypeMessage {
    INFO_MESSAGE,
    ERROR_MESSAGE,
    WARNING_MESSAGE
}
