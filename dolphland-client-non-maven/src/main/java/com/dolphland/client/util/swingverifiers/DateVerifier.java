package com.dolphland.client.util.swingverifiers;

import java.text.SimpleDateFormat;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.dolphland.client.util.i18n.MessageFormater;

/**
 * V�rification des champs de saisie de dates
 * 
 * @author cdelfly
 * 
 */
public class DateVerifier extends AbstractInputVerifier {

    private static final MessageFormater FMT = MessageFormater.getFormater(DateVerifier.class);

    private String pattern;

    private SimpleDateFormat sdf;



    /**
     * Contruit un DateVerifier
     * 
     * @param afficheur
     *            l'afficheur de message associ� au v�rifieur
     * @param pattern
     *            le format de date utilis� pour le SimpleDateFormat de contr�le
     *            de la date saisie
     */
    public DateVerifier(Afficheur afficheur, String pattern) {
        super(afficheur);
        if (pattern == null)
            throw new NullPointerException("pattern cannot be null !"); //$NON-NLS-1$
        if (pattern.trim().length() == 0)
            throw new NullPointerException("pattern is empty !"); //$NON-NLS-1$
        this.pattern = pattern;
        this.sdf = new SimpleDateFormat(pattern);
    }



    /**
     * V�rification de la validit� de la saisie en fonction du pattern
     */
    public boolean verify(JComponent input) {
        if (input instanceof JTextField) {
            JTextField zone = (JTextField) input;
            // V�rification du format de la saisie
            if (zone.getText() != null && zone.getText().trim().length() > 0) {
                try {
                    sdf.parse(zone.getText().trim());
                } catch (Exception e) {
                    afficherErreur(FMT.format("DateVerifier.RS_INVALID_FORMAT", pattern)); //$NON-NLS-1$
                    return false;
                }
            }
        }
        getAfficheur().clear();
        return true;
    }

}
