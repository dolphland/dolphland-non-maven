package com.dolphland.client.util.table;

import org.springframework.util.Assert;

/**
 * <p>
 * Event produced by {@link ProductTable} instances
 * </p>
 * 
 * @author JayJay
 */
public class ProductTableEvent {

    private ProductTable productTable;

    private int[] columnSizes;



    public ProductTableEvent(ProductTable table, int[] columnSizes) {
        Assert.notNull(table, "table is null !"); //$NON-NLS-1$
        Assert.notNull(columnSizes, "columnSizes is null !"); //$NON-NLS-1$
        this.productTable = table;
        this.columnSizes = columnSizes;
    }



    /**
     * <p>
     * Return the {@link ProductTable} that emited that event
     * </p>
     * 
     * @return the {@link ProductTable} from which that event comes from
     */
    public ProductTable getProductTable() {
        return productTable;
    }



    /**
     * <p>
     * Return the columns sizes of the {@link ProductTable} that emited that
     * event
     * </p>
     * 
     * @return The columns sizes as an array of integer values. Columns sizes
     *         are indexed by their corresponding column index.
     */
    public int[] getColumnSizes() {
        return columnSizes;
    }

}
