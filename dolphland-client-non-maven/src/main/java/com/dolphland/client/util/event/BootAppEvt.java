/*
 * $Log: BootAppEvt.java,v $
 * Revision 1.3 2012/07/23 12:44:16 bvatan
 * - made the event public but restrict constructor access to package
 * Revision 1.2 2009/03/05 15:13:04 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.1 2008/11/28 10:05:28 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

import com.dolphland.client.util.conf.CApplication;
import com.dolphland.client.util.conf.FwkBootStep;

public class BootAppEvt extends Event {

    private CApplication application;

    private String environment;

    private int stepIndex = -1;

    private int nbSteps;

    private FwkBootStep step;

    private String stepText;



    BootAppEvt(EventDestination id) {
        super(id);
    }



    public void setApplication(CApplication application) {
        this.application = application;
    }



    public void setNbSteps(int nbSteps) {
        this.nbSteps = nbSteps;
    }



    public int getNbSteps() {
        return nbSteps;
    }



    public int getStepIndex() {
        return stepIndex;
    }



    public FwkBootStep getStep() {
        return step;
    }



    public void setStepIndex(int stepIndex) {
        this.stepIndex = stepIndex;
    }



    public void setStep(FwkBootStep lastStep) {
        this.step = lastStep;
    }



    public void setEnvironment(String environment) {
        this.environment = environment;
    }



    /**
     * Retourne l'environement s�lectionn�
     * 
     * @return L'identifiant de l'environement s�electionn�
     */
    public String getEnvironment() {
        return environment;
    }



    /**
     * Retourne l'objet CApplication repr�sentant la racine du fichier xml de
     * configuration de l'application
     * 
     * @return La confugration correspondante au fichier application.xml
     */
    public CApplication getApplication() {
        return application;
    }



    public void setStepText(String text) {
        this.stepText = text;
    }



    /**
     * Retourne l'intitul� de la derni�re �tape ex�cut�e
     */
    public String getStepText() {
        return stepText;
    }

}
