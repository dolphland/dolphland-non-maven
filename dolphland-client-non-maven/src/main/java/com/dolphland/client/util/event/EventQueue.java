/*
 * $Log: EventQueue.java,v $
 * Revision 1.3 2014/03/10 16:15:20 bvatan
 * - Internationalized
 * Revision 1.2 2009/03/30 09:34:32 jscafi
 * - Suppression des warnings inutiles
 * Revision 1.1 2009/03/30 09:27:51 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

import java.util.LinkedList;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

public class EventQueue {

    private static final Logger log = Logger.getLogger(EventQueue.class);

    private LinkedList<Event> events;

    private EventWatcher watcher;



    public EventQueue() {
        watcher = new EventWatcher();
        events = new LinkedList<Event>();
    }



    public Event peek() {
        synchronized (events) {
            try {
                return events.getFirst();
            } catch (NoSuchElementException nse) {
                return null;
            }
        }
    }



    public Event pop() throws InterruptedException {
        try {
            synchronized (events) {
                while (peek() == null) {
                    events.wait();
                }
                return events.removeFirst();
            }
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
            throw ie;
        }
    }



    public int size() {
        synchronized (events) {
            return events.size();
        }
    }



    public void forEvents(EventDestination dest) {
        EDT.subscribe(watcher).forEvents(dest);
    }



    public void forAllEvents() {
        EDT.subscribe(watcher).forAllEvents();
    }



    @Override
    protected void finalize() throws Throwable {
        log.debug("finalize(): Cleaning..."); //$NON-NLS-1$
        EDT.unsubscribe(watcher).forAllEvents();
    }

    private class EventWatcher extends DefaultEventSubscriber {

        public void eventReceived(Event e) {
            synchronized (events) {
                events.add(e);
                events.notifyAll();
            }
        }
    }

}
