/*
 * $Log: BootAppStartEvt.java,v $
 * Revision 1.1 2008/11/28 10:05:28 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

public class BootAppStartEvt extends BootAppEvt {

    public BootAppStartEvt() {
        super(FwkEvents.BOOT_APP_START_EVT);
    }

}
