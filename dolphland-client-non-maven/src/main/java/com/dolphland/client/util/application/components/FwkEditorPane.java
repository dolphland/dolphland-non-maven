package com.dolphland.client.util.application.components;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JEditorPane;

import com.dolphland.client.util.string.StrUtil;

/**
 * Editor pane
 * 
 * @author JayJay
 * 
 */
public class FwkEditorPane extends JEditorPane {

    // Contents
    private List<String> contents;

    // Manual refresh indicator
    private boolean manualRefresh;



    /**
     * Default constructor
     */
    public FwkEditorPane() {
        this(false);
    }



    /**
     * Constructor with manual refresh indicator
     * 
     * @param manualRefresh
     *            True if manual refresh, False if auto refresh
     */
    public FwkEditorPane(boolean manualRefresh) {
        super("text/html", StrUtil.EMPTY_STRING);
        setEditable(false);
        this.contents = new ArrayList<String>();
        this.manualRefresh = manualRefresh;
    }



    /**
     * Get text from contents
     * 
     * @return Text
     */
    private String getTextContents() {
        String out = "<html>";
        for (String content : new ArrayList<String>(contents)) {
            out += content;
            out += "<br>";
        }
        out += "</html>";
        return out;
    }
    
    
    
    /**
     * Append content 
     * 
     * @param content
     *            Content
     */
    public void appendContent(String content) {
        appendContent(content, null);
    }



    /**
     * Append content with color specified
     * 
     * @param content
     *            Content
     * @param color
     *            Color (null if default color)
     */
    public void appendContent(String content, Color color) {
        String htmlColor = "#000000";
        if (color != null) {
            htmlColor = "#" + Integer.toHexString(color.getRGB()).substring(2);
        }
        StringBuffer htmlContent = new StringBuffer();
        htmlContent.append("<font color = \"" + htmlColor + "\">");
        htmlContent.append(content);
        appendHtmlContent(htmlContent.toString());
    }



    /**
     * Append html content
     * 
     * @param htmlContent
     *            Html content
     */
    public void appendHtmlContent(String htmlContent) {
        contents.add(htmlContent);
        if (!manualRefresh) {
            refreshContents();
        }
    }



    /**
     * Refresh contents
     */
    public void refreshContents() {
        setText(getTextContents());
    }
}