package com.dolphland.client.util.chart;

import java.awt.Shape;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * This class represents a chart of scatter plot using the API JFreeChart with
 * an improved legend.
 * 
 * The main improvement of this legend is to check for certain series do not
 * appear on the chart.
 * 
 * The chart's representation is done with the JComponent componentChart that
 * can get by getChartComponent ()
 * 
 * Possibility to define:
 * - data entities. A data entity is identified by a Comparable consisting of
 * values (abscissa, ordinate)
 * - horizontal markers. A horizontal marker can distinguish one area in an
 * abscissa interval or in an abscissa value. The distinction is made by the
 * text and / or by a color
 * - vertical markers. A vertical marker can distinguish one area in an ordinate
 * interval or in an ordinate value. The distinction is made by the text and /
 * or by a color
 * - fixed values. Data set containing only one value. Fixed values are used to
 * represent a limit.
 * 
 * The chart's configuration is done with a ChartConstraint where it is possible
 * to use a builder (ChartConstraintBuilder)
 * 
 * Concrete example of creating a ScatterPlotChat:
 * 
 * VisionScatterPlotChart chart = new
 * VisionScatterPlotChart(ChartConstraintsBuilder.create()
 * .axeXTitle("Position")
 * .axeYTitle("Thickness (mm)")
 * .axeYRangeAutoPackable(true)
 * .menuItemVisible(ChartConstraints.MENU_ITEM_PROPRIETES, false)
 * .menuItemVisible(ChartConstraints.MENU_ITEM_ECHELLE_AUTOMATIQUE, false)
 * .zoomWithClickEnabled(false)
 * .get()
 * );
 * 
 * @author jeremy.scafi
 */
abstract class AbstractScatterPlotChart extends AbstractXYPlotChart {

    // DataSet for plots
    private XYSeriesCollection plotDataset;

    private int chartDataCount = 0;



    /**
     * Default constructor
     */
    public AbstractScatterPlotChart() {
        super();
    }



    /**
     * Constructor with chart constraints
     * 
     * @param chartConstraints
     *            Chart constraints
     */
    public AbstractScatterPlotChart(ChartConstraints chartConstraints) {
        super(chartConstraints);
    }



    /**
     * Constructor with chart component
     * 
     * @param componentChart
     *            Chart component
     */
    public AbstractScatterPlotChart(JPanel componentChart) {
        super(componentChart);
    }



    /**
     * Constructor with chart component and chart constraints
     * 
     * @param componentChart
     *            Chart component
     * @param chartConstraints
     *            Chart constraints
     */
    public AbstractScatterPlotChart(JPanel componentChart, ChartConstraints chartConstraints) {
        super(componentChart, chartConstraints);
    }



    /** Get data set for plots */
    protected XYDataset getPlotDataSet() {
        if (plotDataset == null) {
            plotDataset = new XYSeriesCollection();
        }
        return plotDataset;
    }



    /** Initialize data set for plots */
    protected void initPlotDataSet() {
        plotDataset.removeAllSeries();
    }



    /** Get series for plots identified by an indice */
    protected XYSeries getPlotSeries(int indSeries) {
        return plotDataset.getSeries(indSeries);
    }



    /** Add a series for plots */
    protected void addPlotSeries(XYSeries series) {
        plotDataset.addSeries(series);
    }



    @Override
    protected void clearChartData() {
        super.clearChartData();

        chartDataCount = 0;
    }



    @Override
    protected Shape getDefaultLegendShape(int indLegendItem, LegendItem legendItem) {
        return legendItem.getShape();
    }



    @Override
    protected Comparable getIdChartData(int indSeries) {
        for (ChartData chartData : getAllChartData()) {
            if (chartData instanceof PlotChartData && chartData.getIndSeries() == indSeries) {
                return chartData.getIdChartData();
            }
        }
        return null;
    }



    @Override
    public void initChart() {
        super.initChart();

        // Initialize data set for plots
        initPlotDataSet();
    }



    @Override
    public void addData(Comparable idChartData, double abs, double ord) {
        // We get the data entities identified by idChartData
        PlotChartData chartData = (PlotChartData) getChartData(idChartData);

        // If entity doesn't exist, we create it and we add it
        if (chartData == null) {
            chartData = new PlotChartData(idChartData);
            chartDataCount++;
        }

        // We add in the entity the parameters data
        chartData.addValue(new Double(abs), new Double(ord));

        // Add data in chart
        addData(chartData, abs, ord);
    }

    /**
     * Chart data for a ScatterPlotChart
     * 
     * @author jeremy.scafi
     */
    private class PlotChartData extends ChartData {

        private XYSeries series;
        private List listAbs;
        private List listOrd;



        public PlotChartData(Comparable idChartData) {
            super(idChartData, chartDataCount);
            series = new XYSeries(idChartData);
            this.listAbs = new ArrayList();
            this.listOrd = new ArrayList();
        }



        public XYSeries getXYSeries() {
            return series;
        }



        public void addValue(Number abs, Number ord) {
            // If firt value, we add the data serie
            if (listAbs.size() == 0) {
                addPlotSeries(series);
            }
            // We create the plot
            listAbs.add(abs);
            listOrd.add(ord);
            series.add(abs, ord);

            // If the range of x-axis is auto packable
            if (getChartConstraints().isAxeXRangeAutoPackable()) {
                ValueAxis rangeX = getChart().getXYPlot().getDomainAxis();

                // If range never initialized by the auto pack, we limit it on
                // abscissa
                if (!firstAutoPackAxeXRealise) {
                    rangeX.setRange(abs.doubleValue() - getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue(), abs.doubleValue() + getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue());
                    firstAutoPackAxeXRealise = true;
                }
                // Else, we ensure that the abscissa is in Range
                else {
                    rangeX.setRange(Math.min(rangeX.getLowerBound(), abs.doubleValue() - getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue()), Math.max(rangeX.getUpperBound(), abs.doubleValue() + getChartConstraints().getAxeXRangeAutoPackMarge().doubleValue()));
                }
            }

            // If the range of y-axis is auto packable
            if (getChartConstraints().isAxeYRangeAutoPackable()) {
                ValueAxis rangeY = getChart().getXYPlot().getRangeAxis();

                // If range never initialized by the auto pack, we limit it on
                // ordinate
                if (!firstAutoPackAxeYRealise) {
                    rangeY.setRange(ord.doubleValue() - getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue(), ord.doubleValue() + getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue());
                    firstAutoPackAxeYRealise = true;
                }
                // Else, we ensure that the ordinate is in Range
                else {
                    rangeY.setRange(Math.min(rangeY.getLowerBound(), ord.doubleValue() - getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue()), Math.max(rangeY.getUpperBound(), ord.doubleValue() + getChartConstraints().getAxeYRangeAutoPackMarge().doubleValue()));
                }
            }
        }



        protected void hideAllValues() {
            // Hide all values
            XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
            renderer.setSeriesVisible(getIndSeries(), false);
        }



        protected void showAllValues() {
            // Show all values
            XYItemRenderer renderer = getChart().getXYPlot().getRenderer();
            renderer.setSeriesVisible(getIndSeries(), true);
        }
    }
}
