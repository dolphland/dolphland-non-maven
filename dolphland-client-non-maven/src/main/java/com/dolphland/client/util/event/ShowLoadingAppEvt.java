package com.dolphland.client.util.event;

public class ShowLoadingAppEvt extends Event {

    public ShowLoadingAppEvt() {
        super(FwkEvents.SHOW_LOADING_APP_EVT);
    }

}
