package com.dolphland.client.util.table.demo;

import java.util.ArrayList;
import java.util.List;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.application.components.FwkScenario;
import com.dolphland.client.util.table.ProductActionEvt;


public class DemoScenario extends FwkScenario{

    private DemoView view;
    
    
    public DemoScenario() {
        view = new DemoView();
        view.addProductAction(new ActionTest1());
        view.addProductAction(new ActionRefresh());
        view.setAction(new ActionTest2());
        
        List<DemoProductAdapter> products = new ArrayList<DemoProductAdapter>();
        for(int i = 1 ; i <= 3 ; i++){
            DemoProductAdapter pa = new DemoProductAdapter();
            pa.setValue(DemoProductDescriptor.PROP_TEST, "Test " + i);
            pa.setValue(DemoProductDescriptor.PROP_ACTION_1, new ActionDemo("Action 1"));
            pa.setValue(DemoProductDescriptor.PROP_ACTION_2, new ActionDemo("Action 2"));
            products.add(pa);
        }
        view.setProducts(products);
    }
    
    @Override
    protected void doEnter(Object param) {
        getWorkPane().setView(view);
    }
    
    private class ActionDemo extends UIAction<ProductActionEvt, String> {
        
        
        public ActionDemo(String name) {
            super(name);
            setDescription("Ceci est un test");
        }
        
        @Override
        protected boolean shouldExecute(ProductActionEvt param) {
            return param.getSelectedProducts().size() == 1;
        }
        
        @Override
        protected String doExecute(ProductActionEvt param) {
            DemoProductAdapter pa = (DemoProductAdapter)param.getSelectedProducts().get(0);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
            return getName() + " => " + pa.getValue(DemoProductDescriptor.PROP_TEST);
        }
        
        @Override
        protected void updateViewSuccess(String data) {
            getWorkPane().showInfoDialog(data);
        }
    }
    
    private class ActionTest1 extends UIAction<ProductActionEvt, Object>{
        
        
        public ActionTest1() {
            super("Test 1");
            setTriggerModifier(TRIGGER_OPTION);
        }
        
        @Override
        protected Object doExecute(ProductActionEvt param) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
            return super.doExecute(param);
        }
    }
    
    private class ActionRefresh extends UIAction<ProductActionEvt, Object>{
        
        
        public ActionRefresh() {
            setTriggerModifier(TRIGGER_CLICK);
        }

        @Override
        protected void updateViewSuccess(Object data) {
            view.updateActionsStates();
        }
    }
    
    private class ActionTest2 extends UIAction<DemoActionEvt, Object>{
        
        
        public ActionTest2() {
            super("Test 2");
        }
        
        @Override
        protected Object doExecute(DemoActionEvt param) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
            }
            return super.doExecute(param);
        }
    }

}
