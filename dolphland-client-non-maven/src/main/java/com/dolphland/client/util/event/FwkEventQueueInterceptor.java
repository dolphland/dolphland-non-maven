package com.dolphland.client.util.event;

import java.awt.AWTEvent;

/**
 * Repr�sente un untercepteur l'�v�nement AWT.<br>
 * <br>
 * Les impl�mentations de cette interfaces peuvent s'enregistrer aupr�s de
 * <code>FwkEventQueue</code> pour �tre notifi� des d'�missions d'�v�nements.<br>
 * <br>
 * Cet intercepteur permet entre autre de tracer et/ou filtrer les �v�nements
 * AWT et d'intercepter les exceptions et errors lev�es dans le cadre de leur
 * �mission.
 * 
 * @author JayJay
 */
public interface FwkEventQueueInterceptor {

    /**
     * Invoqu� juste avant dispatching de l'�v�nement sp�cifi�.<br>
     * Ce service permet de filtrer les �v�nements AWT.<br>
     * Attention, le filtrage des �v�nements AWT peut �tre dangereux.
     * 
     * @param e
     *            L'�v�nement sur le point d'�tre d�livr�
     * @return true si l'�v�nement doit �tre d�livr�, false sinon.
     */
    public boolean beforeDispatch(AWTEvent e);



    /**
     * Invoqu� juste apr�s qu'un �v�nement AWT a �t� d�livr�.
     * 
     * @param e
     *            L'�v�nement AWT qui juste d'�tre d�livr�
     */
    public void afterDispatch(AWTEvent e);



    /**
     * Invoqu� lorsque la d�livrance de l'�v�nement sp�cifi� a produit une
     * erreur (Exception ou Error).<br>
     * Ce servcie est toujours invoqu� avant <code>afterDispatch()</code>
     * 
     * @param e
     *            L'�v�nement d�livr� qui a produit la lev� d'exception ou
     *            d'error
     * @param cause
     *            La cause de l'erreur (Exception ou Error)
     */
    public void dispatchFailed(AWTEvent e, Throwable cause);
}
