package com.dolphland.client.util.layout;

import info.clearthought.layout.TableLayout;

/**
 * Cette classe repr�sente un Builder permettant de construire un
 * <code>TableLayout</code> tout en ajoutant
 * les composants au conteneur concern� par le <code>TableLayout</code>. <br>
 * <br>
 * L'ajout de composants se fait via la m�thode <code>addComponent</code>,
 * n�cessitant l'utilisation de la
 * classe <code>TableLayoutConstraints</code> qui a pour builder la classe
 * <code>TableLayoutConstraintsBuilder</code>. <br>
 * <br>
 * Exemple d'utilisation :<br>
 * <br>
 * <ol>
 * <li>Cr�ation et affectation du TableLayout au conteneur <code>
 * <pre> 	
 * TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
 * panel.setLayout(layoutBuilder.get());
 * </pre>
 * </code> <br>
 * <li>Ajout d'un composant au conteneur <code>
 * <pre> 
 * panel.add(getLabel(), layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
 * .width(TableLayout.FILL)
 * .height(TableLayout.FILL)
 * .row(0)
 * .column(0)
 * .get()
 * ));
 * </pre>
 * </code>
 * </ol>
 * 
 * @author jeremy.scafi
 */
public class TableLayoutBuilder {

    private TableLayout layout;



    public TableLayoutBuilder() {
        layout = new TableLayout();
    }



    public TableLayoutBuilder(TableLayoutBuilder layoutModele) {
        layout = new TableLayout(new double[][] { layoutModele.layout.getColumn(), layoutModele.layout.getRow() });
    }



    public TableLayout get() {
        return layout;
    }



    /**
     * Ajoute au layout un composant en respectant les contraintes sp�cifi�es.
     * La m�thode renvoie l'objet contraintes
     * du <code>TableLayout</code> n�cessaires lors de l'ajout du composant dans
     * un container
     * 
     * @param constraints
     *            Contraintes du composant
     * @return Objet repr�sentant les contraintes du <code>TableLayout</code>
     */
    public Object addComponent(TableLayoutConstraints constraints) {

        // On traite les contraintes de colonnes
        double[] columns = layout.getColumn();

        int oldNbColumns = columns.length;
        int newNbColumns = oldNbColumns;
        int column = (constraints.getColumn() * 3) + 1;

        if (column >= newNbColumns) {
            newNbColumns = column + 2;
        }

        double[] newColumns = new double[newNbColumns];

        for (int indCol = 0; indCol < newColumns.length; indCol++) {
            double newValue = indCol < oldNbColumns ? columns[indCol] : 0;
            if (indCol == column - 1) {
                if (constraints.getLeftInsets() != 0) {
                    newValue = newValue != 0 ? Math.max(newValue, constraints.getLeftInsets()) : constraints.getLeftInsets();
                }
            } else if (indCol == column) {
                if (constraints.getWidth() != 0) {
                    newValue = newValue != 0 ? Math.max(newValue, constraints.getWidth()) : constraints.getWidth();
                }
            } else if (indCol == column + 1) {
                if (constraints.getRightInsets() != 0) {
                    newValue = newValue != 0 ? Math.max(newValue, constraints.getRightInsets()) : constraints.getRightInsets();
                }
            }
            newColumns[indCol] = newValue;
        }

        // On traite les contraintes de lignes
        double[] rows = layout.getRow();

        int oldNbRows = rows.length;
        int newNbRows = oldNbRows;
        int row = (constraints.getRow() * 3) + 1;

        if (row >= newNbRows) {
            newNbRows = row + 2;
        }

        double[] newRows = new double[newNbRows];

        for (int indRow = 0; indRow < newRows.length; indRow++) {
            double newValue = indRow < oldNbRows ? rows[indRow] : 0;
            if (indRow == row - 1) {
                if (constraints.getTopInsets() != 0) {
                    newValue = newValue != 0 ? Math.max(newValue, constraints.getTopInsets()) : constraints.getTopInsets();
                }
            } else if (indRow == row) {
                if (constraints.getHeight() != 0) {
                    newValue = newValue != 0 ? Math.max(newValue, constraints.getHeight()) : constraints.getHeight();
                }
            } else if (indRow == row + 1) {
                if (constraints.getBottomInsets() != 0) {
                    newValue = newValue != 0 ? Math.max(newValue, constraints.getBottomInsets()) : constraints.getBottomInsets();
                }
            }
            newRows[indRow] = newValue;
        }

        // On fixe les nouvelles lignes et colonnes au layout
        layout.setColumn(newColumns);
        layout.setRow(newRows);

        // On construit l'objet de contrainte d'un TableLayout : "column,row"
        String result = column + "," + row; //$NON-NLS-1$

        return result;
    }
}
