package com.dolphland.client.util.table;

import org.springframework.util.Assert;

public class ResizeableTableEvent {

    private ResizeableExtendedTable table;



    public ResizeableTableEvent(ResizeableExtendedTable table) {
        Assert.notNull(table, "null table provided !"); //$NON-NLS-1$
        this.table = table;
    }



    public ResizeableExtendedTable getTable() {
        return table;
    }

}
