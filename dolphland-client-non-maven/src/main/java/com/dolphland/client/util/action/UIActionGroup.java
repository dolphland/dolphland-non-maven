package com.dolphland.client.util.action;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

/**
 * Classe repr�sentant une liste d'actions (<code>UIAction</code>).
 * 
 * @author jeremy.scafi
 */
public class UIActionGroup<P extends UIActionEvt, T> extends UIAction<P, T> {

    // Liste d'actions
    private List<UIAction<P, ?>> actions = new ArrayList<UIAction<P, ?>>();

    // Indique si le groupe d'actions est r�duit ou non
    private boolean collapsed;



    /**
     * Constructeur sans param�tre
     */
    public UIActionGroup() {
        super();
        setCollapsed(true);
    }



    /**
     * Constructeur o� l'on sp�cifie le nom
     * 
     * @param name
     *            Nom de l'action
     */
    public UIActionGroup(String name) {
        super(name);
        setCollapsed(true);
    }



    /**
     * Constructeur o� l'on sp�cifie un nom et une ic�ne
     * 
     * @param name
     *            Nom de l'action
     * @param icon
     *            Ic�ne de l'action
     */
    public UIActionGroup(String name, Icon icon) {
        super(name, icon);
        setCollapsed(true);
    }



    /**
     * Ajoute une action au groupe d'actions
     * 
     * @param action
     *            Action � ajouter
     */
    public UIActionGroup<P, T> addAction(UIAction<P, ?> action) {
        if (action == null) {
            throw new NullPointerException("action is null !"); //$NON-NLS-1$
        }
        actions.add(action);
        return this;
    }



    /**
     * Retourne les actions du groupe d'actions
     */
    public List<UIAction<P, ?>> getActions() {
        return actions;
    }



    /**
     * R�duit ou non le groupe d'actions
     * 
     * @param collapsed
     *            Indique si le groupe d'actions est r�duit ou non
     */
    public void setCollapsed(boolean collapsed) {
        this.collapsed = collapsed;
    }



    /**
     * Indique si le groupe d'actions est r�duit ou non
     * 
     * @return
     */
    public boolean isCollapsed() {
        return collapsed;
    }



    /**
     * Etend ou non le groupe d'actions
     * 
     * @param extended
     *            Indique si le groupe d'actions est �tendu ou non
     */
    public void setExtended(boolean extended) {
        this.collapsed = !extended;
    }



    /**
     * Indique si le groupe d'actions est �tendu ou non
     * 
     * @return
     */
    public boolean isExtended() {
        return !collapsed;
    }
}
