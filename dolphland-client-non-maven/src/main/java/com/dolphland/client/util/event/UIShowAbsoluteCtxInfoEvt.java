/*
 * $Log: UIShowAbsoluteCtxInfoEvt.java,v $
 * Revision 1.6 2014/03/18 13:00:52 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * Revision 1.5 2009/08/06 12:55:10 bvatan
 * - Gestion de l'affichage/masque progressif des bulles d'infos
 * Revision 1.4 2009/04/24 15:08:09 bvatan
 * - ajout possibilit� de fixer le body point dasn les infos bulles en position
 * absolue
 * Revision 1.3 2008/10/09 09:07:00 jscafi
 * - Prise en compte du positionnement du body pour les bulles � positionnement
 * absolu
 * - Ajout possibilit� de changer la couleur de fond et d'�crit d'une bulle
 * - Correction sur les �v�nements des bulles o� il y avait inversion dans les
 * identifiants d'�venement show et hide
 * - Restriction private package pass� en public pour le CtxInfoManager
 * Revision 1.2 2008/09/15 13:50:36 bvatan
 * - ajout javadoc
 * Revision 1.1 2008/07/29 08:47:37 bvatan
 * - initiak check in
 * Revision 1.2 2008/07/23 14:27:26 bvatan
 * - merge avec PRODUCT_PAINTING
 * - correction du syst�me de coordonn�es (int,float->double)
 * - autoalignement des produits avec gestion du mode autoAdjustable
 * - fixation de la taille des produits de mani�re uniforme sur tous les
 * composant via SynoptiqueConf
 * - corrections bugs sur la taille des fl�ches signaux
 * Revision 1.1.2.1 2008/07/21 17:09:09 bvatan
 * - merge avec head
 * - passage du syst�me de coordonn�e entier vers double
 */

package com.dolphland.client.util.event;

import java.awt.Color;
import java.awt.Point;

import javax.swing.JComponent;

/**
 * Ev�nement d�livr� lorqu'il faut afficher un VisionCtxInfo � une position
 * absolue de l'�cran.<br>
 * Lorsque cet �v�nement est re�u par le framework, ce dernier affiche une bulle
 * d'info contextuelle (VisionCtxInfo) dont le point d'attache est sp�cifi� en
 * param�tre du constructeur.<br>
 * Le point d'attache est exprim� dasn el syst�me de coordonn�es du composant
 * sp�cifi� dans le constructeur.<br>
 * La position du corps de la bulle est exprim�e relativement au point
 * d'attache.
 * 
 * @author JayJay
 */
public class UIShowAbsoluteCtxInfoEvt extends UIEvent {

    public static final int TOP_LEFT = 0;

    public static final int TOP = 1;

    public static final int TOP_RIGHT = 2;

    public static final int RIGHT = 3;

    public static final int BOTTTOM_RIGHT = 4;

    public static final int BOTTOM = 5;

    public static final int BOTTOM_LEFT = 6;

    public static final int LEFT = 7;

    /**
     * Composant de r�f�rence pour le calcul des coordonn�es
     */
    private JComponent component;

    /**
     * Texte a afficher dans la bulle
     */
    private String text;

    /**
     * Position du corps de la bulle d'info par rapport au point d'attache
     */
    private int bodyPosition;

    /**
     * Position du point d'attache de la bulle exprim� dans le syst�me de
     * coordonn�es du composant r�f�rence.
     */
    private Point attachPoint;

    /**
     * Position du corps de la bulle exprim� dans le syst�me de coordonn�es du
     * composant r�f�renc� coordonn�es du composant r�f�rence.
     */
    private Point bodyPoint;

    /**
     * Couleur du texte de la bulle
     */
    private Color foreground;

    /**
     * Couleur de fond de la bulle
     */
    private Color background;

    private boolean bodyPointAbsolute;

    /**
     * Indique si la bulle apparait progressivement
     */
    private boolean fadeIn;

    /**
     * Indique si la bulle disparait progressivement
     */
    private boolean fadeOut;

    /**
     * Indique la dur�e d'affichage de la bulle en ms avant disparition
     * automatique
     */
    private long lifeTime = -1;



    /**
     * Construit l'�v�nement
     * 
     * @param comp
     *            Le composant vis� par la bulle d'info
     * @param text
     *            Le texte � afficher dans la bulle d'info
     * @param attachPoint
     *            Le point d'attache de la bulle dans le syst�me de coordonn�es
     *            du composant sp�cifi�
     * @param bodyPosition
     *            La position du corps de la bulle relativement au point
     *            d'attache parmi: <li>TOP_LEFT <li>TOP <li>TOP_RIGHT <li>RIGHT
     *            <li>BOTTOM_RIGHT <li>BOTTOM <li>BOTTOM_LEFT <li>LEFT
     */
    public UIShowAbsoluteCtxInfoEvt(JComponent comp, String text, Point attachPoint, int bodyPosition) {
        super(AppEvents.UI_SHOW_CTX_INFO_EVT);
        if (comp == null) {
            throw new NullPointerException("comp is null !"); //$NON-NLS-1$
        }
        if (attachPoint == null) {
            throw new NullPointerException("attachPoint is null !"); //$NON-NLS-1$
        }
        this.component = comp;
        this.text = text;
        this.bodyPosition = bodyPosition;
        this.attachPoint = attachPoint;
        this.bodyPointAbsolute = false;
    }



    /**
     * Construit l'�v�nement
     * 
     * @param comp
     *            Le composant vis� par la bulle d'info
     * @param text
     *            Le texte � afficher dans la bulle d'info
     * @param attachPoint
     *            Le point d'attache de la bulle dans le syst�me de coordonn�es
     *            du composant sp�cifi�
     * @param bodyPoint
     *            La position du corps de la bulle dans le syst�me coordonn�es
     *            du composant sp�cifi�
     */
    public UIShowAbsoluteCtxInfoEvt(JComponent comp, String text, Point attachPoint, Point bodyPoint) {
        super(AppEvents.UI_SHOW_CTX_INFO_EVT);
        if (comp == null) {
            throw new NullPointerException("comp is null !"); //$NON-NLS-1$
        }
        if (attachPoint == null) {
            throw new NullPointerException("attachPoint is null !"); //$NON-NLS-1$
        }
        if (bodyPoint == null) {
            throw new NullPointerException("bodyPoint is null !"); //$NON-NLS-1$
        }
        this.component = comp;
        this.text = text;
        this.bodyPoint = bodyPoint;
        this.attachPoint = attachPoint;
        this.bodyPointAbsolute = true;
    }



    /**
     * D�finit si la bulle doit s'afficher progressivement
     */
    public void setFadeIn(boolean fadeIn) {
        this.fadeIn = fadeIn;
    }



    /**
     * Indique si la bulle apparait progressivement
     */
    public boolean shouldFadeIn() {
        return fadeIn;
    }



    /**
     * D�finit si la bulle doit dispara�tre progressivement
     */
    public void setFadeOut(boolean fadeOut) {
        this.fadeOut = fadeOut;
    }



    /**
     * Indique si la bulle disparait progressivement
     */
    public boolean shouldFadeOut() {
        return fadeOut;
    }



    /**
     * D�finit la dur�e d'affichage de la bulle avant disparition
     * 
     * @param lifeTime
     *            La dur�e d'affichage en millisecondes
     */
    public void setLifeTime(long lifeTime) {
        this.lifeTime = lifeTime;
    }



    /**
     * Retourne la dur�e d'affichage de la buille avant disparition automatique
     * 
     * @return La dur�e d'affichage en millisecondes
     */
    public long getLifeTime() {
        return lifeTime;
    }



    /**
     * Retourne le composant d�sign� par la bulle d'info
     * 
     * @return Le JComponent d�sign� par la bulle
     */
    public JComponent getComponent() {
        return component;
    }



    /**
     * Retourne le texte � afficher dans la bulle
     * 
     * @return Un String au format text ou html
     */
    public String getText() {
        return text;
    }



    /**
     * Retourne le positionnement de la bulle par rapport au point d'attache
     * 
     * @return Un int parmi les constanes d�finies dans cette classe
     */
    public int getBodyPosition() {
        return bodyPosition;
    }



    /**
     * Retourne les coordonn�es du corps de la bulle exprim�es dans le syst�me
     * de coordonn�es du composant r�f�renc� par la bulle.<br>
     * Cette m�thode retourne null si <code>isBodyPointAbsolute()</code>
     * retourne false.
     */
    public Point getBodyPoint() {
        return bodyPoint;
    }



    /**
     * Retourne le point d'attache ed la bulle dansl e syst�me de coordonn�s du
     * composant sp�cifi�
     */
    public Point getAttachPoint() {
        return attachPoint;
    }



    /**
     * Fixe la couleur de fond de la bulle
     */
    public void setBackground(Color background) {
        this.background = background;
    }



    /**
     * Retourne la couleur de fond de la bulle
     */
    public Color getBackground() {
        return background;
    }



    /**
     * Fixe la couleur d'�crit de la bulle
     */
    public void setForeground(Color foreground) {
        this.foreground = foreground;
    }



    /**
     * Retourne la couleur d'�crit de la bulle
     */
    public Color getForeground() {
        return foreground;
    }



    /**
     * Indique si les coordonn�es du corps de la bulle sont d�finies de mani�re
     * absolue. Si tel est le cas getBodyPoint() retournera les coordonn�es du
     * corps de la bulle.
     * 
     * @return true si les corrdonn�es du corps de la bulle sont sp�cifi�es de
     *         mani�r absolue, fasle sinon
     */
    public boolean isBodyPointAbsolute() {
        return bodyPointAbsolute;
    }

}
