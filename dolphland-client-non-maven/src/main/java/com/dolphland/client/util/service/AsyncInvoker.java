package com.dolphland.client.util.service;

import com.dolphland.client.util.concurrent.Executor;
import com.dolphland.client.util.concurrent.JobQueue;

public class AsyncInvoker {

    private Executor executor;
    private JobQueue queue;

    private static AsyncInvoker singleton;



    public AsyncInvoker(String name) {
        if (name == null) {
            throw new NullPointerException("Please specify a name for AsyncInvoker !"); //$NON-NLS-1$
        }
        queue = new JobQueue();
        executor = new Executor(queue, name);
    }



    public static final void run(Runnable job) {
        getDefaultInvoker().invoke(job);
    }



    public static AsyncInvoker getDefaultInvoker() {
        synchronized (AsyncInvoker.class) {
            if (singleton == null) {
                singleton = new AsyncInvoker("DefaultInvoker"); //$NON-NLS-1$
                singleton.start();
            }
        }
        return singleton;
    }



    public void start() {
        executor.start();
    }



    public void invoke(Runnable job) {
        queue.queueJob(job);
    }



    public void stop() {
        executor.stop();
    }

}
