/*
 * $Log: MessagePane.java,v $
 * Revision 1.3  2014/03/18 13:15:31  bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * - externalized translatable strings
 *
 * Revision 1.2  2008/04/10 07:32:46  jscafi
 * - Ajout de configuration sur synoptique
 * - Factorisation des traitements sur synoptique dans VisionViewScalableSynoptique
 * - Modif de SynoptiquePlanUsine en SynoptiqueScalable
 * - Ajout d'une structure de sc�narios dans package application
 * - Factorisation de l'impl�mentation de la structure de sc�narios dans package application.vision
 * - Mise en deprecated des classes du package scenario
 *
 * Revision 1.1  2008/04/09 14:18:01  jscafi
 * - Ajout de configuration sur synoptique
 * - Factorisation des traitements sur synoptique dans VisionViewScalableSynoptique
 * - Modif de SynoptiquePlanUsine en SynoptiqueScalable
 * - Ajout d'une structure de sc�narios dans package application
 * - Factorisation de l'impl�mentation de la structure de sc�narios dans package application.vision
 * - Mise en deprecated des classes du package scenario
 *
 * Revision 1.1  2008/04/08 12:50:13  jscafi
 * - Ajout de configuration sur synoptique
 * - Factorisation des traitements sur synoptique dans VisionViewScalableSynoptique
 * - Modif de SynoptiquePlanUsine en SynoptiqueScalable
 * - Ajout d'une structure de sc�narios dans package application
 * - Factorisation de l'impl�mentation de la structure de sc�narios dans package application.vision
 * - Mise en deprecated des classes du package scenario
 *
 * Revision 1.2  2008/02/29 10:47:08  kgesquiere
 *  - Modifie la taille du messagePanel en fonction de la taille qu'il contient
 *
 * Revision 1.1  2007/11/13 13:07:54  jscafi
 * - Cr�ation Structure StreamLining
 *
 * Revision 1.7  2007/06/11 15:26:30  cdelfly
 * Nouveau type de message dans le panel d'affichage des messages
 *
 * Revision 1.6  2007/05/24 12:33:12  jscafi
 * - Respect des conventions d'�criture
 *
 * Revision 1.5  2007/05/24 12:27:34  jscafi
 * - Ajout de classes Abstract g�rant la partie FrameWork
 *
 * Revision 1.4  2007/05/24 07:44:36  jscafi
 * - Ajout de classes Abstract g�rant la partie FrameWork
 *
 * Revision 1.3  2007/05/23 10:11:05  jscafi
 * - Visualisation des bons � d�couper
 *
 * Revision 1.2  2007/04/26 16:23:58  bvatan
 * - ajout gestion de l'effacement message
 *
 * Revision 1.1  2007/04/06 17:04:43  bvatan
 * *** empty log message ***
 *
 */
package com.dolphland.client.util.application.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.SoftBevelBorder;

import org.apache.log4j.Logger;

import com.dolphland.client.util.string.StrUtil;

class MessagePane extends JPanel {

	private static final Logger log = Logger.getLogger(MessagePane.class);

	public static final int INFO_MESSAGE_TYPE = 0;

	public static final int ERROR_MESSAGE_TYPE = 1;

	public static final int WARN_MESSAGE_TYPE = 2;
	
	public static final int STATE_MESSAGE_TYPE = 3;

	private static final long serialVersionUID = 1L;

	private JLabel jlMessage = null;
	
	private MessagePaneListener listener;

	public MessagePane() {
		super();
		initialize();
	}

	public void clearMsg() {
		setInfoMessage(StrUtil.EMPTY_STRING);
	}

	public void setInfoMessage(String message) {
		setMessage(message, INFO_MESSAGE_TYPE);
	}

	public void setErrorMessage(String message) {
		setMessage(message, ERROR_MESSAGE_TYPE);
	}

	public void setWarningMessage(String message) {
		setMessage(message, WARN_MESSAGE_TYPE);
	}
	
	public void setStateMessage(String message) {
		setMessage(message, STATE_MESSAGE_TYPE);
	}

	public void setMessage(String message, int type) {
		switch (type) {
			case INFO_MESSAGE_TYPE:
				setMessage(message, Theme.INFO_MESSAGE_FOREGROUND, Theme.INFO_MESSAGE_BACKGROUND, Theme.INFO_MESSAGE_FONT);
				break;
			case ERROR_MESSAGE_TYPE:
				setMessage(message, Theme.ERROR_MESSAGE_FOREGROUND, Theme.ERROR_MESSAGE_BACKGROUND, Theme.ERROR_MESSAGE_FONT);
				break;
			case WARN_MESSAGE_TYPE:
				setMessage(message, Theme.WARN_MESSAGE_FOREGROUND, Theme.WARN_MESSAGE_BACKGROUND, Theme.WARN_MESSAGE_FONT);
				break;
			case STATE_MESSAGE_TYPE:
				setMessage(message, Theme.STATE_MESSAGE_FOREGROUND, Theme.STATE_MESSAGE_BACKGROUND, Theme.STATE_MESSAGE_FONT);
				break;
		}
	}

	public void setMessage(final String message, final Color foreground, final Color background, final Font font) {
		Runnable job = new Runnable() {
			public void run() {
				if (message != null && message.length() > 0) {
					jlMessage.setForeground(foreground);
					setBackground(background);
					jlMessage.setText(message == null ? StrUtil.EMPTY_STRING : message);
					jlMessage.setFont(font);
				} else {
					jlMessage.setText(StrUtil.EMPTY_STRING);
					jlMessage.setFont(font);
					setBackground(Theme.DEFAULT_BACKGROUND);
				}
				if(listener!=null) {
					try {
						listener.textChanged(message);
					} catch(Exception e) {
		    			log.error("listener has thrown an exception", e.getCause()); //$NON-NLS-1$
					}
				}
			}
		};
		if (SwingUtilities.isEventDispatchThread()) {
			job.run();
		} else {
			SwingUtilities.invokeLater(job);
		}

	}

	public Dimension getPreferedSizeLabel() {
		if(jlMessage==null) return new Dimension(0,0);
		else return jlMessage.getPreferredSize(); 
		
	}
	
	
	public void setListener(MessagePaneListener listener) {
	this.listener=listener;	
	}
	
	private void initialize() {
		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.insets = new Insets(3, 5, 3, 3);
		gridBagConstraints.weightx = 1.0;
		gridBagConstraints.anchor = GridBagConstraints.WEST;
		gridBagConstraints.gridy = 0;
		jlMessage = new JLabel();
		jlMessage.setText(StrUtil.EMPTY_STRING);
		this.setSize(727, 174);
		this.setLayout(new GridBagLayout());
		this.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
		this.add(jlMessage, gridBagConstraints);
	}
} // @jve:decl-index=0:visual-constraint="10,10"
