package com.dolphland.client.util.transaction;

/**
 * Adapteur pour {@link SwingAction}
 * 
 * @author JayJay
 */
public class SwingActionAdapter<P, V> extends SwingAction<P, V> {

    @Override
    protected boolean shouldExecute(P param) {
        return true;
    }



    @Override
    protected void preExecute(P param) {

    }



    @Override
    protected V doExecute(P param) {
        return null;
    }



    @Override
    protected void updateViewSuccess(V data) {

    }



    @Override
    protected void updateViewError(Exception ex) {

    }



    @Override
    protected void postExecute() {

    }

}
