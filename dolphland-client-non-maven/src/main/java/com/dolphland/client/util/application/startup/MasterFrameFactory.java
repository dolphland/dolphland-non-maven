package com.dolphland.client.util.application.startup;

import com.dolphland.client.util.application.MasterFrame;

/**
 * <p>
 * Defines an object responsible for providing instances of {@link MasterFrame}
 * </p>
 * 
 * @author JayJay
 */
public interface MasterFrameFactory {

    /**
     * <p>
     * Creates a new {@link MasterFrame} from the provided
     * {@link UIApplicationContext}
     * </p>
     * 
     * @return A {@link MasterFrame} instance.
     */
    public MasterFrame createMasterFrame(UIApplicationContext ctx);

}
