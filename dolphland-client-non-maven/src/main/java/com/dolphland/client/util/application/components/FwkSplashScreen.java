/*
 * $Log: VisionSplashScreen.java,v $
 * Revision 1.19  2014/03/18 13:15:32  bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * - externalized translatable strings
 *
 * Revision 1.18  2013/09/06 14:39:28  bvatan
 * - FWK-102 : in depth rework to comply with new One Vallourec rules. See ticket for details
 *
 * Revision 1.17.4.2 2013/09/03 13:30:15 bvatan
 * - extracted Toolbar logic
 * - removed System.err.println()
 * - Now make use of ActionContext bindings for toolbar actions
 * Revision 1.17.4.1 2013/09/02 14:43:46 bvatan
 * - Reworked for One Vallourec specs
 * Revision 1.17 2012/11/07 13:10:59 bvatan
 * - added possiblity to change title, subtitle font size and familly
 * - added possibility to set title image, vendor image and background image
 * Revision 1.16 2012/07/23 14:12:35 bvatan
 * - now extends VisionMasterFrame
 * - removed dirty main() method
 * Revision 1.15 2012/05/02 14:32:24 bvatan
 * - mise� niveau de l'ensemble des classes pour utilisation de MessageFormater
 * � la place de la classe Messages g�n�r�e par le plugin eclipse.
 * Revision 1.14 2011/08/29 08:28:00 bvatan
 * - Externalisatiion des chaines de caract�res pour traduction : I18N
 * Revision 1.13.2.1 2011/08/25 16:07:42 bvatan
 * - Externalisation des Strings
 * Revision 1.13 2009/06/23 07:55:46 bvatan
 * - systemExitOnSuccess=true lors du clic sur quitter
 * Revision 1.12 2009/06/18 10:59:41 jscafi
 * - Am�liorations du d�marrage d'applications
 * Revision 1.11 2009/03/06 14:35:33 bvatan
 * - rollback vers API EventRegisterer compatible avec l'API depr�ci�
 * Revision 1.10 2009/03/05 15:15:06 bvatan
 * - mise � niveau EventId -> EventDestination
 * Revision 1.9 2009/02/10 14:12:42 jscafi
 * - Modifs Topaze
 * Revision 1.8 2008/12/18 08:42:48 jscafi
 * - Suppression warnings inutiles
 * Revision 1.7 2008/12/15 09:06:40 bvatan
 * ** empty log message ***
 * Revision 1.6 2008/12/05 14:24:10 bvatan
 * - s�curisation de setVisible et dispose pour l'EDT AWT
 * - suppression de l'initalisation de la fen�tre de logs
 * Revision 1.5 2008/12/03 13:25:57 bvatan
 * - affichage ErrorReporter en cas d'erreur
 * - affichage du dialog de logs
 * Revision 1.4 2008/11/28 11:02:53 jscafi
 * - Suppression des warnings
 * Revision 1.3 2008/11/28 10:08:38 bvatan
 * - connexion aux �v�nements d'amor�age via le multicasteur
 * - mise � niveau suite suppression de la notion de FwkBootListener
 * Revision 1.2 2008/11/27 17:04:06 bvatan
 * - correction progression du chargement
 * Revision 1.1 2008/11/27 16:40:23 bvatan
 * - initial check in
 */

package com.dolphland.client.util.application.components;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.border.AbstractBorder;

import com.dolphland.client.util.dialogs.ErrorReporter;
import com.dolphland.client.util.event.BootAppEndEvt;
import com.dolphland.client.util.event.BootAppErrorEvt;
import com.dolphland.client.util.event.BootAppStartEvt;
import com.dolphland.client.util.event.BootAppStepEvt;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EndLoadingAppEvt;
import com.dolphland.client.util.event.FwkEvents;
import com.dolphland.client.util.event.StartLoadingAppEvt;
import com.dolphland.client.util.event.StepLoadingAppEvt;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.transaction.SwingInvoker;

/**
 * <p>
 * Splash screen to be displayed at application startup to help user wait for
 * the application loading
 * </p>
 * <p>
 * The screen displays basic application information as well as Vallourec logo
 * and CTIV logo and progress bars
 * </p>
 * 
 * @author JayJay
 */
public class FwkSplashScreen extends FwkMasterFrame {

    private JProgressBar bootProgressBar;



    public FwkSplashScreen() {
        // setModal(false);
        setContentPane(createContents());
        setResizable(false);
        setUndecorated(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim.width - getWidth()) / 2, (dim.height - getHeight()) / 2);

        ImageIcon icon = new ImageIcon(getClass().getResource("/images/splash/splashicon.png"));
        List<Image> icons = new ArrayList<Image>();
        icons.add(icon.getImage());
        setIconImages(icons);
        //@formatter:off
        EDT.subscribe(new AppEventWatcher())
            .forEvent(FwkEvents.BOOT_APP_START_EVT)
            .forEvent(FwkEvents.BOOT_APP_STEP_EVT)
            .forEvent(FwkEvents.BOOT_APP_END_EVT)
            .forEvent(FwkEvents.BOOT_APP_ERROR_EVT)
            .forEvent(FwkEvents.START_LOADING_APP_EVT)
            .forEvent(FwkEvents.STEP_LOADING_APP_EVT)
            .forEvent(FwkEvents.END_LOADING_APP_EVT);
        //@formatter:on
    }


   


    private FwkPanel createContents() {
        ClassLoader ccl = Thread.currentThread().getContextClassLoader();
        URL backgroundUrl = ccl.getResource("images/splash/splashimage.jpg");
        ImageIcon backgroundIcon = new ImageIcon(backgroundUrl);
        setSize(backgroundIcon.getIconWidth(), backgroundIcon.getIconHeight());
        FwkPanel out = new FwkPanel();
        out.setImage(backgroundIcon);
        out.setBorder(new SplashBorder());
        
        TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
        
        out.setLayout(layoutBuilder.get());
        
        bootProgressBar = new JProgressBar();
        bootProgressBar.setBorder(null);
        bootProgressBar.setPreferredSize(new Dimension(getWidth(), 20));
        bootProgressBar.setForeground(Color.BLUE);
        bootProgressBar.setStringPainted(true);

        out.add(bootProgressBar, layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
            .topInsets(TableLayout.FILL)
            .height(TableLayout.PREFERRED)
            .bottomInsets(FwkPanel.DEFAULT_GAP)
            .leftInsets(FwkPanel.DEFAULT_GAP)
            .width(TableLayout.FILL)
            .rightInsets(FwkPanel.DEFAULT_GAP)
            .get()));
        return out;
    }

    private class SplashBorder extends AbstractBorder {

        private Insets insets = new Insets(2, 2, 2, 2);



        @Override
        public Insets getBorderInsets(Component c) {
            return insets;
        }



        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            g.setColor(Color.WHITE);
            g.drawLine(1, 1, getWidth() - 1 - 1, 1);
            g.drawLine(1, 1, 1, getHeight() - 1 - 1);
            g.setColor(Color.BLACK);
            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
    }

    private class AppEventWatcher extends DefaultEventSubscriber {

        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(final BootAppStartEvt e) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    bootProgressBar.setMaximum(e.getNbSteps());
                    bootProgressBar.setString(e.getStepText());
                }
            });
        }



        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(final BootAppStepEvt e) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    bootProgressBar.setMaximum(e.getNbSteps());
                    bootProgressBar.setValue(e.getStepIndex());
                    bootProgressBar.setString(e.getStepText());
                }
            });
        }



        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(final BootAppEndEvt e) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    bootProgressBar.setValue(bootProgressBar.getMaximum());
                    bootProgressBar.setString(e.getStepText());
                }
            });
        }



        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(final BootAppErrorEvt e) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    // ErrorReporter er = new
                    // ErrorReporter(VisionSplashScreen.this);
                    ErrorReporter er = new ErrorReporter();
                    er.setModal(false);
                    er.setAlwaysOnTop(true);
                    er.setMessage(e.getMessage());
                    er.reportThrowable(e.getCause());
                    er.setLocationRelativeTo(FwkSplashScreen.this);
                    er.setVisible(true);
                }
            });
        }



        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(final StartLoadingAppEvt e) {
            new SwingInvoker() {

                public void run() {
                    //loadProgressBar.setValue(0);
                    //loadProgressBar.setMaximum(e.getNbEvenement() - 1);
                }
            }.invokeLater();
        }



        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(final StepLoadingAppEvt e) {
            new SwingInvoker() {

                public void run() {
                    //int p = (int) (((float) e.getCompteur() / (float) loadProgressBar.getMaximum()) * 100f);
                    //loadProgressBar.setString(FMT.format("FwkSplashScreen.RS_LOADING_DATA") + " (" + p + "%)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    //loadProgressBar.setValue(e.getCompteur());
                }
            }.invokeLater();
        }



        @SuppressWarnings("unused") //$NON-NLS-1$
        public void eventReceived(final EndLoadingAppEvt e) {
            new SwingInvoker() {

                public void run() {
                    //loadProgressBar.setValue(loadProgressBar.getMaximum());
                    //loadProgressBar.setString(FMT.format("FwkSplashScreen.RS_LOADING_DATA") + " (100%)"); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }.invokeLater();
        }
    }





    @Override
    public void setVisible(final boolean b) {
        new SwingInvoker() {

            public void run() {
                FwkSplashScreen.super.setVisible(b);
            }
        }.invokeAndWait();
    }



    @Override
    public void dispose() {
        new SwingInvoker() {

            public void run() {
                FwkSplashScreen.super.dispose();
            }
        }.invokeAndWait();
    }



    @Override
    protected void doInit(String[] args) {

    }



    @Override
    protected String getVersion() {
        return StrUtil.EMPTY_STRING;
    }
}
