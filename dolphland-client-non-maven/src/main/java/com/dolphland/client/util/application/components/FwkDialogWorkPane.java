package com.dolphland.client.util.application.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JDialog;

import com.dolphland.client.util.application.MasterFrame;


public class FwkDialogWorkPane extends FwkWorkPane {

	private JDialog dialog;
	
	private boolean wasCanceled;
	
	private DisabledWorkPaneManager disabledWorkPaneManager;
	
	/**
	 * Construteur
	 */
	public FwkDialogWorkPane(MasterFrame frame, JDialog parent) {
		super(frame);
		if(parent == null) {
			dialog = new JDialog(frame);
		} else {
			dialog = new JDialog(parent);
		}
		setModal(true);
		dialog.setContentPane(this);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				cancel();
			}
		});
		
		// D�finit le manager de d�sactivation du workPane
		disabledWorkPaneManager = new DisabledWorkPaneManager();
		disabledWorkPaneManager.addWorkPane(this);
	}
	
	public FwkDialogWorkPane(MasterFrame frame) {
		this(frame, null);
	}
	
	/**
	 * D�sactive le WorkPane en affichant un message
	 * @param message Message � afficher
	 */
	public void startDisabledMode(String message){
		disabledWorkPaneManager.startDisabledMode(message);
	}
	
	/**
	 * D�sactive le WorkPane en affichant un message tout en indiquant si la souris peut traverser ou non
	 * @param message Message � afficher
	 * @param mouseTraversable Indique si la souris peut traverser ou non
	 */
	public void startDisabledMode(String message, boolean mouseTraversable){
		disabledWorkPaneManager.startDisabledMode(message, mouseTraversable);
	}
	
	/**
	 * R�active le WorkPane
	 */
	public void endDisabledMode(){
		disabledWorkPaneManager.endDisabledMode();
	}
	
	/** Annule la dialog */
	public void cancel(){
		wasCanceled = true;
		close();
	}
	
	/** Indique si le dialog a �t� annul� par l'utilisateur */
	public boolean wasCanceled() {
		return wasCanceled;
	}

	/** Affiche la dialog */
	public void show() {
		setVisible(true);
	}

	/** Cache la dialog */
	public void hide() {
		setVisible(false);
	}

	/** La fermeture de la zone de travail entra�ne la fermeture de la dialog */
	public void close() {
		disabledWorkPaneManager.freeEDT();
		dialog.dispose();
	}
	
	/** Fixe le titre de la dialog */
	public void setTitle(String title) {
		dialog.setTitle(title);
	}
	
	/** Fixe la taille de la dialog */
	public void setSize(int width, int height) {
		dialog.setSize(width, height);
	}

	public void setSize(Dimension dim) {
		dialog.setSize(dim);
	}

	/** Fixe la position de la dialog relativement � un composant */
	public void setLocationRelativeTo(Component c) {
		dialog.setLocationRelativeTo(c);
	}

	/** Fixe la position de la dialog */
	public void setLocation(int x, int y) {
		dialog.setLocation(x, y);
	}

	/** Fixe la position de la dialog */
	public void setLocation(Point p) {
		dialog.setLocation(p);
	}	
	
	/** Affiche/Cache la dialog */
	public void setVisible(boolean aFlag) {
		dialog.setVisible(aFlag);
	}

	/** Rend modal ou non modal la dialog */
	public void setModal(boolean aFlag) {
		dialog.setModal(aFlag);
	}

	/** Ferme la dialog */
	public void dispose() {
		close();
	}

	/** Ajoute un listener Window sur la dialog */
	public void addWindowListener(WindowListener listener) {
		dialog.addWindowListener(listener);
	}
	
	/** R�cup�re la dialog */
	public JDialog getDialog() {
		return dialog;
	}
}
