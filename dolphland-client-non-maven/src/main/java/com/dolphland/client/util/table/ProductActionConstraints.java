package com.dolphland.client.util.table;

import javax.swing.SwingConstants;

/**
 * Classe recensant les contraintes pouvant �tre appliqu�es � un
 * <code>ProductAction</code> :
 * - Position de la <code>ProductAction</code> si de trigger TRIGGER_OPTION
 * (Valeurs possibles : POSITION_TOP, POSITION_BOTTOM, POSITION_LEFT,
 * POSITION_RIGHT)
 * 
 * @author jeremy.scafi
 */
public class ProductActionConstraints {

    public final static int POSITION_TOP = SwingConstants.TOP;

    public final static int POSITION_BOTTOM = SwingConstants.BOTTOM;

    public final static int POSITION_LEFT = SwingConstants.LEFT;

    public final static int POSITION_RIGHT = SwingConstants.RIGHT;

    public final static int DEFAULT_POSITION = POSITION_BOTTOM;

    // Variables contraintes
    private int positionTriggerOption = POSITION_BOTTOM;



    /**
     * Fournit la position de la <code>ProductAction</code> si de trigger
     * TRIGGER_OPTION
     */
    public int getPositionTriggerOption() {
        return positionTriggerOption;
    }



    /**
     * D�finit la position de la <code>ProductAction</code> si de trigger
     * TRIGGER_OPTION
     * Valeurs possibles : POSITION_TOP, POSITION_BOTTOM, POSITION_LEFT,
     * POSITION_RIGHT
     * 
     * @param positionTriggerOption
     *            Position de la <code>ProductAction</code>
     */
    public void setPositionTriggerOption(int positionTriggerOption) {
        switch (positionTriggerOption) {
            case POSITION_TOP :
            case POSITION_BOTTOM :
            case POSITION_LEFT :
            case POSITION_RIGHT :
                this.positionTriggerOption = positionTriggerOption;
                break;

            default :
                this.positionTriggerOption = DEFAULT_POSITION;
                break;
        }
    }
}
