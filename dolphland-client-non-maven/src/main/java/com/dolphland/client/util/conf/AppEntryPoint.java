package com.dolphland.client.util.conf;

/**
 * <p>
 * Defines the application entry point loaded by {@link ApplicationManager}
 * </p>
 * <p>
 * Applications should subclass this to define their entry point.<br>
 * When specified as the first argument on the command line (full class name),
 * {@link ApplicationManager} will load the implementation, create an instance
 * and fire {@link AppEntryPoint#preBoot(BootContext)} and
 * {@link AppEntryPoint#postBoot(BootContext)} respectively before and after
 * application boot process.
 * </p>
 * <p>
 * <table border=1>
 * <tr>
 * <td><b>Service</b></td>
 * <td><b>When</b></td>
 * </tr>
 * <tr>
 * <td>{@link AppEntryPoint#preBoot(BootContext)}</td>
 * <td>Called just <b>before</b> {@link FwkBoot#boot(String...)} is called</td>
 * </tr>
 * <tr>
 * <td>{@link AppEntryPoint#postBoot(BootContext)}</td>
 * <td>Called just <b>after</b> {@link FwkBoot#boot(String...)} is called</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @author JayJay
 */
public class AppEntryPoint {

    /**
     * <p>
     * Invoked by {@link ApplicationManager} just <b>before</b>
     * {@link FwkBoot#boot(String...)} is beeing called.
     * </p>
     * <p>
     * Implementors can add extra {@link FwkBootStep} the the {@link FwkBoot}
     * instance provided in the {@link BootContext} parameter.
     * </p>
     * 
     * @param ctx
     *            Contains application startup context. No configuration or
     *            service object is available at this time.
     */
    public void preBoot(BootContext ctx) {

    }



    /**
     * <p>
     * Invoked by {@link ApplicationManager} just <b>after</b>
     * {@link FwkBoot#boot(String...)} has been called.
     * </p>
     * <p>
     * Implementors are guaranted that application configuration and services
     * (if any) have been loaded when this service is called.
     * </p>
     * 
     * @param ctx
     *            Contains application startup context. Configuration and
     *            service objects are available at this time.
     */
    public void postBoot(BootContext ctx) {

    }

}
