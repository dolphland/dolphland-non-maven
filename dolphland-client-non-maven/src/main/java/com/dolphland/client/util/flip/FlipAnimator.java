package com.dolphland.client.util.flip;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.animator.AnimatedComponent;
import com.dolphland.client.util.animator.AnimatedPanel;
import com.dolphland.client.util.animator.Animation;
import com.dolphland.client.util.animator.AnimationActionEvt;
import com.dolphland.client.util.animator.AnimationParameter;
import com.dolphland.client.util.animator.DefaultAnimationParameter;
import com.dolphland.client.util.animator.SequenceAnimations;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;

/**
 * Animator for a flip's panel
 * 
 * @author JayJay
 * 
 */
class FlipAnimator {

    private final static int DEFAULT_GAP = 10;

    private final static double FLIP_RATIO = 0.3d;

    private AnimatedPanel flipAnimatedPanel;

    private FlipAnimationParameter flipAnimationParameter;

    private FlipAnimationParameter previousFlipAnimationParameter;



    /**
     * Constructor with flip's panel
     * 
     * @param flipAnimatedPanel
     *            Flip's panel
     */
    public FlipAnimator(AnimatedPanel flipAnimatedPanel) {
        AssertUtil.notNull(flipAnimatedPanel, "flipAnimatedPanel is null !");
        this.flipAnimatedPanel = flipAnimatedPanel;
        this.flipAnimatedPanel.addComponentListener(new ComponentAdapter() {

            @Override
            public void componentResized(ComponentEvent e) {
                if (isAnimationResizable() && flipAnimationParameter != null) {
                    // Update animations if animated flip resized
                    flipAnimationParameter.setStepsPolicy(AnimationParameter.ONLY_ONE_STEP);
                    flipAnimationParameter.setActionsBefore(null);
                    executeAnimations(flipAnimationParameter);
                }
            }
        });
        executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MAXIMIZE_SELECTED_APPLICATION)
            .stepsPolicy(AnimationParameter.ONLY_ONE_STEP)
            .get());
    }



    /**
     * Load the animations
     * 
     * @param typeAction
     *            Type action
     * @param selectedApplicationId
     *            Selected application's id
     * @param position
     *            Position
     * @param stepsPolicy
     *            Steps policy
     */
    public void executeAnimations(FlipAnimationParameter flipAnimationParameter) {
        AssertUtil.notNull(flipAnimationParameter, "flipAnimationParameter is null !");

        // Stop animation
        flipAnimatedPanel.stopAnimate();

        // Update animation parameters
        this.previousFlipAnimationParameter = this.flipAnimationParameter;
        this.flipAnimationParameter = flipAnimationParameter;

        // Remove all animations
        flipAnimatedPanel.removeAllAnimations();

        // Add all animations sequences
        for (SequenceAnimations sequenceAnimations : getSequencesAnimations()) {
            flipAnimatedPanel.addSequenceAnimations(sequenceAnimations);
        }

        // Start animation
        flipAnimatedPanel.startAnimate();
    }



    /**
     * Tell if an animation is resizable
     */
    private boolean isAnimationResizable() {
        return flipAnimationParameter.getTypeAction() != FlipAnimationType.MAXIMIZE_SELECTED_APPLICATION && flipAnimationParameter.getTypeAction() != FlipAnimationType.POP_UP_SELECTED_APPLICATION;
    }



    /**
     * Get sequences animations
     * 
     * @return Sequences animations
     */
    private List<SequenceAnimations> getSequencesAnimations() {
        List<SequenceAnimations> result = new ArrayList<SequenceAnimations>();
        switch (flipAnimationParameter.getTypeAction()) {

            case MOVE_HORIZONTALLY_ALL_APPLICATIONS :
                result.addAll(getSequencesAnimationsMoveHorizontallyAllApplications());
                break;

            case MOVE_VERTICALLY_SELECTED_APPLICATION :
                result.addAll(getSequencesAnimationsMoveVerticallySelectedApplication());
                break;

            case MINIMIZE_SELECTED_APPLICATION :
                result.addAll(getSequencesAnimationsMinimizeSelectedApplication());
                break;

            case MAXIMIZE_SELECTED_APPLICATION :
                result.addAll(getSequencesAnimationsMaximizeSelectedApplication());
                break;

            case POP_UP_SELECTED_APPLICATION :
                result.addAll(getSequencesAnimationsPopupSelectedApplication());
                break;

            case MOVE_OUT_OF_SCREEN_SELECTED_APPLICATION :
                result.addAll(getSequencesAnimationsMoveOutOfScreenSelectedApplication());
                break;

            default :
                throw new AppInfrastructureException("Type action not implemented !");
        }
        return result;
    }



    /**
     * Get sequences animations when move horizontally all applications
     * 
     * @return Sequences animations
     */
    private List<SequenceAnimations> getSequencesAnimationsMoveHorizontallyAllApplications() {
        List<SequenceAnimations> result = new ArrayList<SequenceAnimations>();

        Animation animation = null;
        AnimatedComponent animatedApplication = null;
        DefaultAnimationParameter animationParameter = null;

        int currentX = 0;

        // Create sequence to move horizontally all applications
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        result.add(sequenceAnimations);

        // If a selected application is specified, all applications are
        // move to the left in order to have the selected application in
        // the center
        if (flipAnimationParameter.getSelectedApplicationId() != null) {
            for (String applicationId : flipAnimatedPanel.getAllAnimatedComponentsIds()) {
                animatedApplication = flipAnimatedPanel.getAnimatedComponent(applicationId);
                if (flipAnimationParameter.getSelectedApplicationId().equals(applicationId)) {
                    if (flipAnimationParameter.getInAttachPoint() != null) {
                        // If an attach point is fixed, center the
                        // application to the attach point's horizontal
                        // coordinate
                        currentX += flipAnimationParameter.getInAttachPoint().x + DEFAULT_GAP - getDefaultWidth() / 2;
                    } else {
                        // If no position is fixed, center the
                        // application
                        currentX += flipAnimatedPanel.getWidth() / 2 - getDefaultWidth() / 2;
                    }
                    break;
                }
                currentX -= getDefaultWidth() + DEFAULT_GAP;
            }
        } else {
            currentX = DEFAULT_GAP;
        }

        for (String applicationId : flipAnimatedPanel.getAllAnimatedComponentsIds()) {
            animatedApplication = flipAnimatedPanel.getAnimatedComponent(applicationId);
            animationParameter = new DefaultAnimationParameter();
            animationParameter.getActionsBeforeAnimation().addAll(flipAnimationParameter.getActionsBefore() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsBefore());
            animationParameter.getActionsAfterAnimation().addAll(flipAnimationParameter.getActionsAfter() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsAfter());
            animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
            animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
            animationParameter.setWidth(getDefaultWidth());
            animationParameter.setHeight(getDefaultHeight());
            animationParameter.setX(currentX);
            animationParameter.setY(flipAnimatedPanel.getHeight() / 2 - getDefaultHeight() / 2);
            animation = new Animation(animatedApplication, animationParameter);
            sequenceAnimations.addAnimation(animation);
            currentX += getDefaultWidth() + DEFAULT_GAP;
        }

        return result;
    }



    /**
     * Get sequences animations when move vertically the selected application
     * 
     * @return Sequences animations
     */
    private List<SequenceAnimations> getSequencesAnimationsMoveVerticallySelectedApplication() {
        List<SequenceAnimations> result = new ArrayList<SequenceAnimations>();

        Animation animation = null;
        AnimatedComponent animatedApplication = null;
        DefaultAnimationParameter animationParameter = null;

        if (flipAnimationParameter.getSelectedApplicationId() != null) {
            animatedApplication = flipAnimatedPanel.getAnimatedComponent(flipAnimationParameter.getSelectedApplicationId());
        }

        // Do nothing if no selected application
        if (animatedApplication == null) {
            return result;
        }

        // Create sequence to move vertically the selected application
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        result.add(sequenceAnimations);

        animationParameter = new DefaultAnimationParameter();
        animationParameter.getActionsBeforeAnimation().addAll(flipAnimationParameter.getActionsBefore() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsBefore());
        animationParameter.getActionsAfterAnimation().addAll(flipAnimationParameter.getActionsAfter() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsAfter());
        animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
        animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
        animationParameter.setWidth(getDefaultWidth());
        animationParameter.setHeight(getDefaultHeight());
        animationParameter.setX(animatedApplication.getBoundsWithoutRotation().x);
        if (flipAnimationParameter.getInAttachPoint() != null) {
            // If an attach point is fixed, center the
            // application to the attach point's vertical coordinate
            animationParameter.setY(flipAnimationParameter.getInAttachPoint().y - getDefaultHeight() / 2);
        } else {
            // If no position is fixed, center the
            // application
            animationParameter.setY(flipAnimatedPanel.getHeight() / 2 - getDefaultHeight() / 2);
        }

        animation = new Animation(animatedApplication, animationParameter);
        sequenceAnimations.addAnimation(animation);

        return result;
    }



    /**
     * Get sequences animations when minimize the selected application
     * 
     * @return Sequences animations
     */
    private List<SequenceAnimations> getSequencesAnimationsMinimizeSelectedApplication() {
        List<SequenceAnimations> result = new ArrayList<SequenceAnimations>();

        Animation animation = null;
        AnimatedComponent animatedApplication = null;
        DefaultAnimationParameter animationParameter = null;

        // If no application is selected, auto select the last application
        if (flipAnimationParameter.getSelectedApplicationId() == null && flipAnimatedPanel.getAnimatedComponentsCount() > 0) {
            flipAnimationParameter.setSelectedApplicationId(flipAnimatedPanel.getAllAnimatedComponentsIds().get(flipAnimatedPanel.getAnimatedComponentsCount() - 1));
        }

        // Do nothing if no selected application
        if (flipAnimationParameter.getSelectedApplicationId() == null) {
            return result;
        }

        int currentX = 0;

        // Create sequence to position the applications before animation
        // minimize
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        result.add(sequenceAnimations);

        for (String applicationId : flipAnimatedPanel.getAllAnimatedComponentsIds()) {
            animatedApplication = flipAnimatedPanel.getAnimatedComponent(applicationId);
            if (flipAnimationParameter.getSelectedApplicationId().equals(applicationId)) {
                break;
            }
            currentX -= getDefaultWidth() + DEFAULT_GAP;
        }

        for (String applicationId : flipAnimatedPanel.getAllAnimatedComponentsIds()) {
            animatedApplication = flipAnimatedPanel.getAnimatedComponent(applicationId);
            if (applicationId.equals(flipAnimationParameter.getSelectedApplicationId())) {
                animationParameter = new DefaultAnimationParameter();
                animationParameter.getActionsBeforeAnimation().addAll(flipAnimationParameter.getActionsBefore() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsBefore());
                animationParameter.getActionsAfterAnimation().addAll(flipAnimationParameter.getActionsAfter() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsAfter());
                animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
                animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
                animationParameter.setWidth(getMaximizedWidth());
                animationParameter.setHeight(getMaximizedHeight());
                animationParameter.setX(0);
                animationParameter.setY(0);
                animation = new Animation(animatedApplication, animationParameter);
                sequenceAnimations.addAnimation(animation);
                currentX += getMaximizedWidth() + DEFAULT_GAP;
            } else {
                animationParameter = new DefaultAnimationParameter();
                animationParameter.getActionsBeforeAnimation().addAll(flipAnimationParameter.getActionsBefore() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsBefore());
                animationParameter.getActionsAfterAnimation().addAll(flipAnimationParameter.getActionsAfter() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsAfter());
                animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
                animationParameter.setStepsPolicy(AnimationParameter.ONLY_ONE_STEP);
                animationParameter.setWidth(getDefaultWidth());
                animationParameter.setHeight(getDefaultHeight());
                animationParameter.setX(currentX);
                animationParameter.setY(flipAnimatedPanel.getHeight() / 2 - getDefaultHeight() / 2);
                animation = new Animation(animatedApplication, animationParameter);
                sequenceAnimations.addAnimation(animation);
                currentX += getDefaultWidth() + DEFAULT_GAP;
            }
        }

        // Center the applications
        result.addAll(getSequencesAnimationsMoveHorizontallyAllApplications());

        return result;
    }



    /**
     * Get sequences animations when maximize the selected application
     * 
     * @return Sequences animations
     */
    private List<SequenceAnimations> getSequencesAnimationsMaximizeSelectedApplication() {
        List<SequenceAnimations> result = new ArrayList<SequenceAnimations>();

        Animation animation = null;
        AnimatedComponent animatedApplication = null;
        DefaultAnimationParameter animationParameter = null;

        // If no application is selected, auto select the last application
        if (flipAnimationParameter.getSelectedApplicationId() == null && flipAnimatedPanel.getAnimatedComponentsCount() > 0) {
            flipAnimationParameter.setSelectedApplicationId(flipAnimatedPanel.getAllAnimatedComponentsIds().get(flipAnimatedPanel.getAnimatedComponentsCount() - 1));
        }

        // Do nothing if no selected application
        if (flipAnimationParameter.getSelectedApplicationId() == null) {
            return result;
        }

        // Create sequence to maximize the selected application
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        result.add(sequenceAnimations);

        boolean selectedApplicationFounded = false;
        for (String applicationId : flipAnimatedPanel.getAllAnimatedComponentsIds()) {
            animatedApplication = flipAnimatedPanel.getAnimatedComponent(applicationId);
            if (applicationId.equals(flipAnimationParameter.getSelectedApplicationId())) {
                animationParameter = new DefaultAnimationParameter();
                animationParameter.getActionsBeforeAnimation().addAll(flipAnimationParameter.getActionsBefore() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsBefore());
                animationParameter.getActionsAfterAnimation().addAll(flipAnimationParameter.getActionsAfter() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsAfter());
                animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
                animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
                animationParameter.setWidth(getMaximizedWidth());
                animationParameter.setHeight(getMaximizedHeight());
                animationParameter.setX(0);
                animationParameter.setY(0);
                animation = new Animation(animatedApplication, animationParameter);
                sequenceAnimations.addAnimation(animation);
                selectedApplicationFounded = true;
            } else {
                animationParameter = new DefaultAnimationParameter();
                animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
                animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
                animationParameter.setWidth(getDefaultWidth());
                animationParameter.setHeight(getDefaultHeight());
                animationParameter.setX(selectedApplicationFounded ? getMaximizedWidth() : -getDefaultWidth());
                animationParameter.setY(flipAnimatedPanel.getHeight() / 2 - getDefaultHeight() / 2);
                animation = new Animation(animatedApplication, animationParameter);
                sequenceAnimations.addAnimation(animation);
            }
        }

        return result;
    }



    /**
     * Get sequences animations when pop-up the selected application
     * 
     * @return Sequences animations
     */
    private List<SequenceAnimations> getSequencesAnimationsPopupSelectedApplication() {
        List<SequenceAnimations> result = new ArrayList<SequenceAnimations>();

        Animation animation = null;
        AnimatedComponent animatedApplication = null;
        DefaultAnimationParameter animationParameter = null;

        // If no application is selected, auto select the last application
        if (flipAnimationParameter.getSelectedApplicationId() == null && flipAnimatedPanel.getAnimatedComponentsCount() > 0) {
            flipAnimationParameter.setSelectedApplicationId(flipAnimatedPanel.getAllAnimatedComponentsIds().get(flipAnimatedPanel.getAnimatedComponentsCount() - 1));
        }

        // Do nothing if no selected application
        if (flipAnimationParameter.getSelectedApplicationId() == null) {
            return result;
        }

        // Create sequence to center the selected application with no width and
        // no height
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        result.add(sequenceAnimations);

        animatedApplication = flipAnimatedPanel.getAnimatedComponent(flipAnimationParameter.getSelectedApplicationId());
        animationParameter = new DefaultAnimationParameter();
        animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
        animationParameter.setStepsPolicy(AnimationParameter.ONLY_ONE_STEP);
        animationParameter.setLayerPolicy(AnimationParameter.LAYER_FOREGROUND);
        animationParameter.setWidth(10);
        animationParameter.setHeight(10);
        if (flipAnimationParameter.getInAttachPoint() != null) {
            // If an attach point is fixed, center the
            // application to the attach point's coordinate
            animationParameter.setX(flipAnimationParameter.getInAttachPoint().x - animationParameter.getWidth() / 2);
            animationParameter.setY(flipAnimationParameter.getInAttachPoint().y - animationParameter.getHeight() / 2);
        } else {
            // If no position is fixed, center the
            // application
            animationParameter.setX(getMaximizedWidth() / 2 - animationParameter.getWidth() / 2);
            animationParameter.setY(getMaximizedHeight() / 2 - animationParameter.getHeight() / 2);
        }
        animation = new Animation(animatedApplication, animationParameter);
        sequenceAnimations.addAnimation(animation);

        // Create sequence to pop up the selected application
        sequenceAnimations = new SequenceAnimations();
        result.add(sequenceAnimations);
        for (String applicationId : flipAnimatedPanel.getAllAnimatedComponentsIds()) {
            animatedApplication = flipAnimatedPanel.getAnimatedComponent(applicationId);
            if (applicationId.equals(flipAnimationParameter.getSelectedApplicationId())) {
                animationParameter = new DefaultAnimationParameter();
                animationParameter.getActionsBeforeAnimation().addAll(flipAnimationParameter.getActionsBefore() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsBefore());
                animationParameter.getActionsAfterAnimation().addAll(flipAnimationParameter.getActionsAfter() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsAfter());
                animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
                animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
                animationParameter.setWidth(getMaximizedWidth());
                animationParameter.setHeight(getMaximizedHeight());
                animationParameter.setX(0);
                animationParameter.setY(0);
                animation = new Animation(animatedApplication, animationParameter);
                sequenceAnimations.addAnimation(animation);
            } else if (applicationId.equals(previousFlipAnimationParameter.getSelectedApplicationId())) {
                animationParameter = new DefaultAnimationParameter();
                animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
                animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
                animationParameter.setWidth(0);
                animationParameter.setHeight(0);
                if (flipAnimationParameter.getOutAttachPoint() != null) {
                    // If an out attach point is fixed, center the previous
                    // application to the attach point's coordinate
                    animationParameter.setX(flipAnimationParameter.getOutAttachPoint().x);
                    animationParameter.setY(flipAnimationParameter.getOutAttachPoint().y);
                } else {
                    // If no position is fixed, the application is pushed to the
                    // center
                    animationParameter.setX(flipAnimatedPanel.getWidth() / 2);
                    animationParameter.setY(flipAnimatedPanel.getHeight() / 2);
                }
                animation = new Animation(animatedApplication, animationParameter);
                sequenceAnimations.addAnimation(animation);
            }
        }

        return result;
    }



    /**
     * Get sequences animations when move out of screen (to the top) selected
     * application
     * 
     * @return Sequences animations
     */
    private List<SequenceAnimations> getSequencesAnimationsMoveOutOfScreenSelectedApplication() {
        List<SequenceAnimations> result = new ArrayList<SequenceAnimations>();

        Animation animation = null;
        AnimatedComponent animatedApplication = null;
        DefaultAnimationParameter animationParameter = null;

        if (flipAnimationParameter.getSelectedApplicationId() != null) {
            animatedApplication = flipAnimatedPanel.getAnimatedComponent(flipAnimationParameter.getSelectedApplicationId());
        }

        // Do nothing if no selected application
        if (animatedApplication == null) {
            return result;
        }

        // Create sequence to hide the selected application
        SequenceAnimations sequenceAnimations = new SequenceAnimations();
        result.add(sequenceAnimations);

        animationParameter = new DefaultAnimationParameter();
        animationParameter.getActionsBeforeAnimation().addAll(flipAnimationParameter.getActionsBefore() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsBefore());
        animationParameter.getActionsAfterAnimation().addAll(flipAnimationParameter.getActionsAfter() == null ? new ArrayList<UIAction<AnimationActionEvt, ?>>() : flipAnimationParameter.getActionsAfter());
        animationParameter.setAnimationDurationMs(flipAnimationParameter.getAnimationDurationMs());
        animationParameter.setStepsPolicy(flipAnimationParameter.getStepsPolicy());
        animationParameter.setWidth(getDefaultWidth());
        animationParameter.setHeight(getDefaultHeight());
        animationParameter.setX(animatedApplication.getBoundsWithoutRotation().x);
        animationParameter.setY(-animatedApplication.getWidth());
        animation = new Animation(animatedApplication, animationParameter);
        sequenceAnimations.addAnimation(animation);

        return result;
    }



    private int getDefaultWidth() {
        return (int) (((double) flipAnimatedPanel.getWidth()) * FLIP_RATIO);
    }



    private int getDefaultHeight() {
        return (int) (((double) flipAnimatedPanel.getHeight()) * FLIP_RATIO);
    }



    private int getMaximizedWidth() {
        return flipAnimatedPanel.getWidth();
    }



    private int getMaximizedHeight() {
        return flipAnimatedPanel.getHeight();
    }



    public static void main(String[] args) {
        // Start EDT
        EDT.start();

        final AnimatedPanel flipAnimatedPanel = new AnimatedPanel();
        flipAnimatedPanel.setBackground(Color.RED);

        JFrame frame = new JFrame();
        frame.setSize(640, 480);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(flipAnimatedPanel);
        frame.setVisible(true);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        Color[] colorsApplications = new Color[] { Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN, Color.ORANGE, Color.PINK };

        for (int i = 0; i < colorsApplications.length; i++) {
            AnimatedComponent component = new AnimatedComponent(new Integer(i).toString());
            FwkPanel applicationPanel = new FwkPanel();
            applicationPanel.setBackground(colorsApplications[i]);
            TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
            component.setLayout(layoutBuilder.get());
            component.add(applicationPanel, layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
                .width(TableLayout.FILL)
                .height(TableLayout.FILL)
                .get()));
            flipAnimatedPanel.addAnimatedComponent(component);
        }

        FlipAnimator animator = new FlipAnimator(flipAnimatedPanel);

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
            .selectedApplicationId("1")
            .get());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
            .selectedApplicationId("1")
            .inAttachPoint(new Point(-100, 0))
            .get());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
            .selectedApplicationId("1")
            .inAttachPoint(new Point(200, 0))
            .get());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
            .selectedApplicationId("1")
            .inAttachPoint(new Point(-100, 0))
            .get());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_VERTICALLY_SELECTED_APPLICATION)
            .selectedApplicationId("3")
            .inAttachPoint(new Point(0, 400))
            .get());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_VERTICALLY_SELECTED_APPLICATION)
            .selectedApplicationId("3")
            .inAttachPoint(new Point(0, 100))
            .get());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
            .selectedApplicationId("2")
            .get());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
        }

        animator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MAXIMIZE_SELECTED_APPLICATION)
            .selectedApplicationId("2")
            .get());
    }
}
