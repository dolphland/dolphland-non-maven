package com.dolphland.client.util.application;

import org.apache.log4j.Logger;

/**
 * Classe repr�sentant un sc�nario
 * 
 * @author J�r�my Scafi
 * @author JayJay
 */
public abstract class Scenario {

    private final static Logger log = Logger.getLogger(Scenario.class);

    private WorkPane workPane;



    /**
     * Liste des traitements r�alis�s lorsqu'on entre dans le sc�nario, par
     * enter() (� d�finir)
     */
    protected abstract void doEnter(Object param);



    /** Fixe le workPane du sc�nario */
    public Scenario setWorkPane(WorkPane mp) {
        this.workPane = mp;
        return this;
    }



    /** Entre dans le sc�nario, sans param�tre */
    public void enter() {
        enter(null);
    }



    /** Entre dans le sc�nario, avec param�tre */
    public void enter(Object param) {
        doEnter(param);
    }



    /** Quitte le sc�nario */
    public void leave() {
        WorkPane wp = getWorkPane();
        if (wp != null) {
            wp.close();
        }
        try {
            doLeave();
        } catch (Exception e) {
            log.error("leaveAndEnter() has thrown an exception !", e); //$NON-NLS-1$
        }
    }



    /** Quitte le sc�nario pour entrer dans un autre sc�nario */
    public void leaveAndEnter(Scenario scenario) {
        leaveAndEnter(scenario, null);
    }



    public void leaveAndEnter(Scenario scenario, Object param) {
        try {
            doLeave();
            scenario.enter(param);
        } catch (Exception e) {
            log.error("leaveAndEnter() has thrown an exception !", e); //$NON-NLS-1$
        }
    }



    /** Liste des traitements r�alis�s lorsqu'on quitte le sc�nario, par leave() */
    protected void doLeave() {

    }



    /** Fournit le workpane du sc�nario */
    public WorkPane getWorkPane() {
        return workPane;
    }

}
