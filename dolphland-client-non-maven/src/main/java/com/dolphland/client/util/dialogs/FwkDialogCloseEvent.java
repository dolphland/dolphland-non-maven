package com.dolphland.client.util.dialogs;

import java.awt.event.WindowListener;

import com.dolphland.client.util.event.CloseAllDialogsEvt;

/**
 * <p>
 * Enumeration for {@link FwkDialogClosingEvent} closing events types
 * </p>
 * 
 * @author JayJay
 */
public enum FwkDialogCloseEvent {
    /** User has pressed the ESCAPE key */
    USER_ESCAPE_EVENT,
    /** the dialog has received an {@link CloseAllDialogsEvt} event */
    CLOSE_ALL_DIALOGS_EVENT,
    /**
     * User has closed the dialog window, or
     * {@link WindowListener#windowClosing(java.awt.event.WindowEvent)} has been
     * called
     */
    WINDOW_CLOSING_EVENT
}
