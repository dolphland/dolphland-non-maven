/*
 * $Log: BootAppErrorEvt.java,v $
 * Revision 1.1 2008/12/03 13:28:30 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

public class BootAppErrorEvt extends Event {

    private String message;

    private Throwable cause;



    public BootAppErrorEvt(String message, Throwable cause) {
        super(FwkEvents.BOOT_APP_ERROR_EVT);
        this.message = message;
        this.cause = cause;
    }



    public String getMessage() {
        return message;
    }



    public Throwable getCause() {
        return cause;
    }

}
