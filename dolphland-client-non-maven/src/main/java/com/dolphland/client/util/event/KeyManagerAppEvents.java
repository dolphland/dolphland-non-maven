package com.dolphland.client.util.event;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.transaction.MVCTx;

/**
 * Classe permettant de fixer les combinaisons de touches suivantes :
 * - Touche de rotation synoptique (Exemple : touche TAB)
 * - Touche d'affichage des signaux synoptique (Touche CTRL par d�faut)
 * - Touches de fonctions synoptique : avec la touche Alt enfonc�e (Touche Alt +
 * F1 par d�faut)
 * - Touches de fonctions applicatif : sans autres touches enfonc�es (Touche F1
 * par d�faut)
 * 
 * Lors de l'appui d'une de ces combinaisons, la classe envoie l'�v�nement
 * ad�quate
 * 
 * @author jeremy.scafi
 */
public class KeyManagerAppEvents {

    public final static int KEY_ACTION_LOAD_DIALOG_APROPOS = 1;

    private Map<Integer, Runnable> keyActionMap;



    public KeyManagerAppEvents() {
        keyActionMap = new HashMap<Integer, Runnable>();
    }



    public Runnable getAction(int idKeyAction) {
        Runnable action = keyActionMap.get(idKeyAction);
        if (action == null) {
            switch (idKeyAction) {
                case KEY_ACTION_LOAD_DIALOG_APROPOS :
                    action = new Runnable() {

                        public void run() {
                            EDT.postEvent(new LoadDialogAProposAppEvt());
                        }
                    };
                    break;

                default :
                    throw new AppBusinessException("Action " + idKeyAction + " inconnue !");
            }

            keyActionMap.put(idKeyAction, action);
        }

        return action;
    }



    public void registerKeyActionEvent(RegisterKeyEvent registerKeyEvent) {
        deregisterKeyActionEvent(registerKeyEvent);
        EDT.postEvent(registerKeyEvent);
    }



    private void deregisterKeyActionEvent(RegisterKeyEvent registerKeyEvent) {
        if (registerKeyEvent.getAction() instanceof Runnable) {
            EDT.postEvent(DeregisterKeyEvent.forAction((Runnable) registerKeyEvent.getAction()));
        } else if (registerKeyEvent.getAction() instanceof UIAction) {
            EDT.postEvent(DeregisterKeyEvent.forAction((UIAction<UIActionEvt, ?>) registerKeyEvent.getAction()));
        } else if (registerKeyEvent.getAction() instanceof MVCTx) {
            EDT.postEvent(DeregisterKeyEvent.forAction((MVCTx<Object, ?>) registerKeyEvent.getAction()));
        }
    }



    /**
     * Lancement du manager du KeyManager des AppEvents
     * 
     */
    public void registerKeyEvents() {

        // par d�faut, on fixe la touche F1 pour l'affichage de la dialogue
        // "A propos" de l'application
        registerKeyActionEvent(RegisterKeyEvent.forAction(getAction(KEY_ACTION_LOAD_DIALOG_APROPOS))
            .keyCode(KeyEvent.VK_F1)
            .ctrlDown());
    }
}
