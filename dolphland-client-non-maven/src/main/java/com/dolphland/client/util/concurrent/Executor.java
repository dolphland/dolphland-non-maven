/*
 * $Log: Executor.java,v $
 * Revision 1.4 2014/03/10 15:59:26 bvatan
 * - Internationalized
 * Revision 1.3 2012/07/23 13:29:46 bvatan
 * - removed useless FwkUnhandledExceptionEvent
 * Revision 1.2 2007/04/16 09:39:53 bvatan
 * - modification du format des traces de logs
 * Revision 1.1 2007/04/06 15:34:50 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.2 2007/03/27 07:23:07 bvatan
 * - Poste un FWKUnhandledException sur les exceptions non g�r�es
 * Revision 1.1 2007/03/02 08:41:06 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.1 2007/01/26 13:58:49 bvatan
 * *** empty log message ***
 */
package com.dolphland.client.util.concurrent;

import org.apache.log4j.Logger;

/**
 * Thread d'ex�cution de t�ches � partir d'une file de t�ches � ex�cuter.
 * 
 * @author JayJay
 */
public class Executor {

    private static final Logger log = Logger.getLogger(Executor.class);

    private Thread thread;

    private boolean shouldRun = true;

    private String name;

    private Runner runner;

    private JobQueue queue;

    private static int nbExecutors = 0;

    private volatile long totalJobs = 0;

    private volatile long successfulJobs = 0;

    private volatile long failedJobs = 0;



    public Executor(JobQueue queue) {
        this(queue, "Executor-" + (nbExecutors++)); //$NON-NLS-1$
    }



    public Executor(JobQueue queue, String name) {
        if (queue == null) {
            throw new NullPointerException("queue cannot be null !"); //$NON-NLS-1$
        }
        if (name == null) {
            throw new NullPointerException("name cannot be null !"); //$NON-NLS-1$
        }
        this.name = name;
        this.queue = queue;
        runner = new Runner();
    }



    /**
     * D�marre l'Executor. Une fois d�marr�, l'Executor obtient les t�ches �
     * ex�cuter depuis la file d'attente.
     */
    public synchronized void start() {
        if (thread != null) {
            return;
        }
        thread = new Thread(runner, name);
        shouldRun = true;
        totalJobs = 0;
        successfulJobs = 0;
        failedJobs = 0;
        thread.start();
    }



    /**
     * Arr�te l'executor. Si une t�che est en cours d'ex�cution, elle se termine
     * avant que l'executor ne se termine � son tour.
     */
    public synchronized void stop() {
        if (thread == null) {
            return;
        }
        shouldRun = false;
        thread.interrupt();
        thread = null;
    }



    public long getFailedJobs() {
        return failedJobs;
    }



    public long getSuccessfulJobs() {
        return successfulJobs;
    }



    public long getTotalJobs() {
        return totalJobs;
    }

    private class Runner implements Runnable {

        public void run() {
            if (log.isInfoEnabled()) {
                log.info("run(): [" + name + "] d�marr�."); //$NON-NLS-1$ //$NON-NLS-2$
            }
            Runnable job = null;
            while (shouldRun) {
                try {
                    job = queue.popJob();
                } catch (InterruptedException ie) {
                    if (log.isDebugEnabled()) {
                        log.debug("run(): [" + name + "] interrompu !"); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                    shouldRun = false;
                    continue;
                }
                if (log.isDebugEnabled()) {
                    log.debug("run(): ex�cution d'un job, reste " + queue.size() + " job(s) en attente."); //$NON-NLS-1$ //$NON-NLS-2$
                }
                try {
                    job.run();
                    successfulJobs++;
                } catch (Exception e) {
                    failedJobs++;
                    log.error("run(): Erreur pendant l'ex�cution d'un job !", e); //$NON-NLS-1$
                } finally {
                    totalJobs++;
                }
                job = null;
            }
            if (log.isInfoEnabled()) {
                log.info("run(): [" + name + "] arr�t�."); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }
    }

}
