package com.dolphland.client.util.chart;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.dolphland.client.util.i18n.MessageFormater;

/**
 * Classe d�finissant l'ensemble des contraintes que peut avoir un chart
 * 
 * Il est ainsi possible de d�finir :
 * - Le titre du chart
 * - Le sens d'orientation du chart (si le chart en poss�de un)
 * - L'activation du zoom lors d'un clic sur le chart
 * - L'indicateur de l�gende cochable ou non sur le chart
 * - La possibilit� d'afficher la l�gende sous une forme compress�e pouvant
 * s'�tendre si souris dessus
 * - La position de la l�gende par rapport au chart
 * - La position de la barre d'actions par rapport au chart
 * - La liste des items de menu contextuel du chart non visibles
 * - L'affichage ou non de bordures du chart
 * - Les couleurs des s�ries de donn�es
 * - Les formes des s�ries de donn�es
 * - La couleur de s�lection
 * - Le titre de l'axe des abscisses (si le chart en poss�de un)
 * - Le titre de l'axe des ordonn�es (si le chart en poss�de un)
 * - La visibilit� de l'axe des abscisses (si le chart en poss�de un)
 * - La visibilit� de l'axe des ordonn�es (si le chart en poss�de un)
 * - Le range min et/ou max de l'axe des abscisses (si le chart en poss�de un)
 * - Le range min et/ou max de l'axe des ordonn�es (si le chart en poss�de un)
 * - Le range se packe automatiquement pour l'axe des abscisses (si le chart en
 * poss�de un)
 * Ainsi, l'axe des abscisses se r�ajuste automatiquement de fa�on � d�buter �
 * la valeur min des s�ries du chart et de terminer � la valeur max des s�ries
 * du chart
 * Exemple : Si un graphe contient une abscisse en 10 et une abscisse en 5,
 * l'axe des abscisses d�butera en 5 et se terminera en 10
 * - Le range se packe automatiquement pour l'axe des ordonn�es (si le chart en
 * poss�de un)
 * Ainsi, l'axe des ordonn�es se r�ajuste automatiquement de fa�on � d�buter �
 * la valeur min des s�ries du chart et de terminer � la valeur max des s�ries
 * du chart
 * Exemple : Si un graphe contient une ordonn�e en 10 et une ordonn�e en 5,
 * l'axe des ordonn�es d�butera en 5 et se terminera en 10
 * - La marge de range utilis�e lors du package automatique pour l'axe des
 * abscisses (si le chart en poss�de un)
 * Exemple : Avec une marge de 1, si un graphe contient une abscisse en 10 et
 * une abscisse en 5, l'axe des abscisses d�butera en 4 et se terminera en 11
 * - La marge de range utilis�e lors du package automatique pour l'axe des
 * ordonn�es (si le chart en poss�de un)
 * Exemple : Avec une marge de 1, si un graphe contient une ordonn�e en 10 et
 * une ordonn�e en 5, l'axe des ordonn�es d�butera en 4 et se terminera en 11
 * - Le pas entre chaque valeur de l'axe des abscisses
 * - Le pas entre chaque valeur de l'axe des ordonn�es
 * - L'indicateur de visibilit� d'�tiquettes sur les charts (si le chart peut en
 * poss�der)
 * - L'angle de d�part des donn�es du chart (si le chart en poss�de un)
 * - Le g�n�rateur de tooltip
 * 
 * Cette classe s'utilise en param�tre du constructeur du chart
 * (VisionAbstractChartPanel).
 * 
 * Pour faciliter la d�finition des contraintes du chart, il est �galement
 * possible d'utiliser un builder (ChartConstraintsBuilder)
 * 
 * Les contraintes peuvent �tre chang�es en temps r�el car elles sont impact�es
 * au Chart par l'interface ChartConstraintsListener
 * 
 * Exemple simple d'utilisation :
 * 
 * LineChart chart = new LineChart(new VisionPanel(),
 * ChartConstraintsBuilder.create()
 * .chartTitle("Titre")
 * .get());
 * 
 * @author jeremy.scafi
 */
public class ChartConstraints {

    private final static Logger log = Logger.getLogger(ChartConstraints.class);

    private static final MessageFormater fmt = MessageFormater.getFormater(ChartConstraints.class);

    /** Couleur par d�faut de la s�lection */
    private final static Color DEFAULT_SELECTION_COLOR = Color.BLACK;

    /** Palette par d�faut des couleurs du chart */
    private final static Color[] DEFAULT_COLORS_PAL = new Color[] {
    Color.blue,
    Color.red,
    Color.magenta,
    Color.orange,
    Color.green,
    Color.gray,
    Color.pink,
    Color.black,
    Color.cyan
    };

    /** Liste des menu items du chart */
    public final static String MENU_ITEM_PROPRIETES = fmt.format("ChartConstraints.RS_ITEM_PROPERTIES"); //$NON-NLS-1$
    public final static String MENU_ITEM_ENREGISTRER_SOUS = fmt.format("ChartConstraints.RS_ITEM_SAVE_AS"); //$NON-NLS-1$
    public final static String MENU_ITEM_IMPRIMER = fmt.format("ChartConstraints.RS_ITEM_PRINT"); //$NON-NLS-1$
    public final static String MENU_ITEM_ZOOM_AVANT = fmt.format("ChartConstraints.RS_ITEM_ZOOM_IN"); //$NON-NLS-1$
    public final static String MENU_ITEM_ZOOM_ARRIERE = fmt.format("ChartConstraints.RS_ITEM_ZOOM_OUT"); //$NON-NLS-1$
    public final static String MENU_ITEM_ECHELLE_AUTOMATIQUE = fmt.format("ChartConstraints.RS_ITEM_AUTO_SCALE"); //$NON-NLS-1$

    /** Liste des positions de la l�gende */
    public final static int POSITION_NONE = 0;
    public final static int POSITION_TOP_LEFT = 1;
    public final static int POSITION_TOP_RIGHT = 2;
    public final static int POSITION_BOTTOM_RIGHT = 3;
    public final static int POSITION_BOTTOM_LEFT = 4;
    public final static int POSITION_LEFT = 5;
    public final static int POSITION_RIGHT = 6;
    public final static int POSITION_BOTTOM = 7;
    public final static int POSITION_TOP = 8;
    public final static int POSITION_LEFT_TOP = 9;
    public final static int POSITION_RIGHT_TOP = 10;
    public final static int POSITION_RIGHT_BOTTOM = 11;
    public final static int POSITION_LEFT_BOTTOM = 12;

    /** Liste des positions possibles pour la barre de boutons */
    public final static int BUTTONS_PANEL_POSITION_LEFT = POSITION_LEFT;
    public final static int BUTTONS_PANEL_POSITION_RIGHT = POSITION_RIGHT;
    public final static int BUTTONS_PANEL_POSITION_BOTTOM = POSITION_BOTTOM;
    public final static int BUTTONS_PANEL_POSITION_TOP = POSITION_TOP;

    /** Variables contenant les contraintes de chart */
    private String chartTitle;
    private boolean horizontal;
    private boolean zoomWithClickEnabled;
    private boolean legendeCheckable;
    private boolean legendeExtendable;
    private int legendePosition;
    private int buttonsPanelPosition;
    private List listMenuItemsNonVisibles;
    private boolean borderVisible;
    private Color[] colorsChartData;
    private ChartShape[] shapesChartData;
    private Color selectionColor;
    private String axeXTitle;
    private String axeYTitle;
    private boolean axeXVisible;
    private boolean axeYVisible;
    private Number axeXRangeMin;
    private Number axeYRangeMin;
    private Number axeXRangeMax;
    private Number axeYRangeMax;
    private boolean axeXRangeAutoPackable;
    private boolean axeYRangeAutoPackable;
    private Number axeXRangeAutoPackMarge;
    private Number axeYRangeAutoPackMarge;
    private Number axeXPas;
    private Number axeYPas;
    private boolean labelsVisible;
    private Double startAngle;
    private ChartToolTipGenerator toolTipGenerator;

    /** Listener permettant d'impacter les modifs de contraintes sur le chart */
    private ChartConstraintsListener listener;



    /**
     * Constructeur
     */
    public ChartConstraints() {
        listMenuItemsNonVisibles = new ArrayList();
        // Initialisation des contraintes avec les valeurs par d�faut
        setChartTitle(""); //$NON-NLS-1$
        setHorizontal(false);
        setZoomWithClickEnabled(true);
        setLegendeCheckable(true);
        setLegendeExtendable(false);
        setLegendePosition(POSITION_BOTTOM);
        setButtonsPanelPosition(BUTTONS_PANEL_POSITION_BOTTOM);
        setBorderVisible(true);
        setColorsChartData(DEFAULT_COLORS_PAL);
        setShapesChartData(null);
        setSelectionColor(DEFAULT_SELECTION_COLOR);
        setAxeXTitle(""); //$NON-NLS-1$
        setAxeYTitle(""); //$NON-NLS-1$
        setAxeXVisible(true);
        setAxeYVisible(true);
        setAxeXRange(null, null);
        setAxeYRange(null, null);
        setAxeXRangeAutoPackable(true);
        setAxeYRangeAutoPackable(true);
        setAxeXRangeAutoPackMarge(new Double(0));
        setAxeYRangeAutoPackMarge(new Double(1));
        setAxeXPas(null);
        setAxeYPas(null);
        setLabelsVisible(true);
        setToolTipGenerator(null);
    }



    /** Applique toutes les contraintes */
    void applyAllConstraints() {
        try {
            if (listener != null) {
                listener.doChangeChartTitle();
                listener.doChangeAxeXTitle();
                listener.doChangeAxeYTitle();
                listener.doChangeAxeXVisible();
                listener.doChangeAxeYVisible();
                listener.doChangeHorizontal();
                listener.doChangeAxeYRange();
                listener.doChangeAxeYRangeAutoPackable();
                listener.doChangeAxeYRangeAutoPackMarge();
                listener.doChangeAxeXRange();
                listener.doChangeAxeXRangeAutoPackable();
                listener.doChangeAxeXRangeAutoPackMarge();
                listener.doChangeZoomWithClickEnabled();
                listener.doChangeLegendePosition();
                listener.doChangeLegendeCheckable();
                listener.doChangeLegendeExtendable();
                listener.doChangeButtonsPanelPosition();
                listener.doChangeMenuItemVisibility();
                listener.doChangeAxeYPas();
                listener.doChangeAxeXPas();
                listener.doChangeBorderVisibility();
                listener.doChangeColorsChartData();
                listener.doChangeShapesChartData();
                listener.doChangeSelectionColor();
                listener.doChangeLabelsVisible();
                listener.doChangeStartAngle();
                listener.doChangeToolTipGenerator();
            }
        } catch (Exception ex) {
            log.error("Error while applying constraints", ex); //$NON-NLS-1$
        }
    }



    /**
     * Fixe le listener permettant d'impacter les modifs de contraintes au chart
     * en temps r�el
     */
    void setListener(ChartConstraintsListener listener) {
        this.listener = listener;
    }



    /** Fixe le titre du chart */
    public void setChartTitle(String chartTitle) {
        this.chartTitle = chartTitle;
        try {
            if (listener != null)
                listener.doChangeChartTitle();
        } catch (Exception ex) {
            log.error("Error while changing title", ex); //$NON-NLS-1$
        }
    }



    public String getChartTitle() {
        return chartTitle;
    }



    /**
     * Indique si le chart est � l'horizontal (si ce n'est pas le cas, il sera �
     * la vertical)
     */
    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
        try {
            if (listener != null)
                listener.doChangeHorizontal();
        } catch (Exception ex) {
            log.error("Error while changing orientation", ex); //$NON-NLS-1$
        }
    }



    public boolean isHorizontal() {
        return horizontal;
    }



    /** Indique si le zoom par clic est possible */
    public void setZoomWithClickEnabled(boolean zoomWithClickEnabled) {
        this.zoomWithClickEnabled = zoomWithClickEnabled;
        try {
            if (listener != null)
                listener.doChangeZoomWithClickEnabled();
        } catch (Exception ex) {
            log.error("Error when changing zoom capability by click", ex); //$NON-NLS-1$
        }
    }



    public boolean isZoomWithClickEnabled() {
        return zoomWithClickEnabled;
    }



    /** Indique si les items de la l�gende sont cochables */
    public void setLegendeCheckable(boolean legendeCheckable) {
        this.legendeCheckable = legendeCheckable;
        try {
            if (listener != null)
                listener.doChangeLegendeCheckable();
        } catch (Exception ex) {
            log.error("Error while changing ability to tick the items in the legend", ex); //$NON-NLS-1$
        }
    }



    public boolean isLegendeCheckable() {
        return legendeCheckable;
    }



    /**
     * Indique si la l�gende est sous une forme compress�e pouvant s'�tendre si
     * souris dessus
     */
    public void setLegendeExtendable(boolean legendeExtendable) {
        this.legendeExtendable = legendeExtendable;
        try {
            if (listener != null)
                listener.doChangeLegendeExtendable();
        } catch (Exception ex) {
            log.error("Error while changing the compressed form whom may extend if the mouse is over the legend", ex); //$NON-NLS-1$
        }
    }



    public boolean isLegendeExtendable() {
        return legendeExtendable;
    }



    /** Fixe la position de la l�gende */
    public void setLegendePosition(int legendePosition) {
        this.legendePosition = legendePosition;
        try {
            if (listener != null)
                listener.doChangeLegendePosition();
        } catch (Exception ex) {
            log.error("Error while changing the legend position", ex); //$NON-NLS-1$
        }
    }



    public int getLegendePosition() {
        return legendePosition;
    }



    /** Fixe la position de la barre de boutons */
    public void setButtonsPanelPosition(int buttonsPanelPosition) {
        this.buttonsPanelPosition = buttonsPanelPosition;
        try {
            if (listener != null)
                listener.doChangeButtonsPanelPosition();
        } catch (Exception ex) {
            log.error("Error while changing the buttons panel position", ex); //$NON-NLS-1$
        }
    }



    public int getButtonsPanelPosition() {
        return buttonsPanelPosition;
    }



    /** Fixe la visibilit� d'un menu item */
    public void setMenuItemVisible(String menuItem, boolean menuItemVisible) {
        if (menuItemVisible) {
            listMenuItemsNonVisibles.remove(menuItem);
        } else {
            listMenuItemsNonVisibles.add(menuItem);
        }
        try {
            if (listener != null)
                listener.doChangeMenuItemVisibility();
        } catch (Exception ex) {
            log.error("Error while changing MenuItem visibility", ex); //$NON-NLS-1$
        }
    }



    public boolean isMenuItemVisible(String menuItem) {
        return !listMenuItemsNonVisibles.contains(menuItem);
    }



    /** Rend visible ou non les bordures du chart */
    public void setBorderVisible(boolean borderVisible) {
        this.borderVisible = borderVisible;
        try {
            if (listener != null)
                listener.doChangeBorderVisibility();
        } catch (Exception ex) {
            log.error("Error while changing borders visibility", ex); //$NON-NLS-1$
        }
    }



    public boolean isBorderVisible() {
        return borderVisible;
    }



    /** Fixe la palette de couleurs des donn�es du chart */
    public void setColorsChartData(Color[] colorsChartData) {
        this.colorsChartData = colorsChartData;
        try {
            if (listener != null)
                listener.doChangeColorsChartData();
        } catch (Exception ex) {
            log.error("Error while changing chart data colors", ex); //$NON-NLS-1$
        }
    }



    public Color[] getColorsChartData() {
        return colorsChartData;
    }



    /** Fixe les formes des donn�es du chart */
    public void setShapesChartData(ChartShape[] shapesChartData) {
        this.shapesChartData = shapesChartData;
        try {
            if (listener != null)
                listener.doChangeShapesChartData();
        } catch (Exception ex) {
            log.error("Error while changing chart data shapes", ex); //$NON-NLS-1$
        }
    }



    public ChartShape[] getShapesChartData() {
        return shapesChartData;
    }



    /** Fixe la couleur de la s�lection */
    public void setSelectionColor(Color selectionColor) {
        this.selectionColor = selectionColor;
        try {
            if (listener != null)
                listener.doChangeSelectionColor();
        } catch (Exception ex) {
            log.error("Error while changing selection color", ex); //$NON-NLS-1$
        }
    }



    public Color getSelectionColor() {
        return selectionColor;
    }



    /** Fixe le titre de l'axe X du chart */
    public void setAxeXTitle(String axeXTitle) {
        this.axeXTitle = axeXTitle;
        try {
            if (listener != null)
                listener.doChangeAxeXTitle();
        } catch (Exception ex) {
            log.error("Error while changing X axis title", ex); //$NON-NLS-1$
        }
    }



    public String getAxeXTitle() {
        return axeXTitle;
    }



    /** Fixe le titre de l'axe Y du chart */
    public void setAxeYTitle(String axeYTitle) {
        this.axeYTitle = axeYTitle;
        try {
            if (listener != null)
                listener.doChangeAxeYTitle();
        } catch (Exception ex) {
            log.error("Error while changing Y axis title", ex); //$NON-NLS-1$
        }

    }



    public String getAxeYTitle() {
        return axeYTitle;
    }



    /** Fixe la visibilit� de l'axe X du chart */
    public void setAxeXVisible(boolean axeXVisible) {
        this.axeXVisible = axeXVisible;
        try {
            if (listener != null)
                listener.doChangeAxeXVisible();
        } catch (Exception ex) {
            log.error("Error while changing X axis visibility", ex); //$NON-NLS-1$
        }
    }



    public boolean isAxeXVisible() {
        return axeXVisible;
    }



    /** Fixe la visibilit� de l'axe Y du chart */
    public void setAxeYVisible(boolean axeYVisible) {
        this.axeYVisible = axeYVisible;
        try {
            if (listener != null)
                listener.doChangeAxeYVisible();
        } catch (Exception ex) {
            log.error("Error while changing Y axis visibility", ex); //$NON-NLS-1$
        }
    }



    public boolean isAxeYVisible() {
        return axeYVisible;
    }



    /** Indique le min et le max du range de l'axe X */
    public void setAxeXRange(Number axeXRangeMin, Number axeXRangeMax) {
        // Le fait de fixer le range d�sactive l'auto pack
        if (axeXRangeMin != null && axeXRangeMax != null) {
            setAxeXRangeAutoPackable(false);
        }

        this.axeXRangeMin = axeXRangeMin;
        this.axeXRangeMax = axeXRangeMax;
        try {
            if (listener != null)
                listener.doChangeAxeXRange();
        } catch (Exception ex) {
            log.error("Error while changing axe X range", ex); //$NON-NLS-1$
        }
    }



    public Number getAxeXRangeMin() {
        return axeXRangeMin;
    }



    public Number getAxeXRangeMax() {
        return axeXRangeMax;
    }



    /** Indique le min et le max du range de l'axe Y */
    public void setAxeYRange(Number axeYRangeMin, Number axeYRangeMax) {
        // Le fait de fixer le range d�sactive l'auto pack
        if (axeYRangeMin != null && axeYRangeMax != null) {
            setAxeYRangeAutoPackable(false);
        }

        this.axeYRangeMin = axeYRangeMin;
        this.axeYRangeMax = axeYRangeMax;
        try {
            if (listener != null)
                listener.doChangeAxeYRange();
        } catch (Exception ex) {
            log.error("Error while changing axe Y range", ex); //$NON-NLS-1$
        }
    }



    public Number getAxeYRangeMin() {
        return axeYRangeMin;
    }



    public Number getAxeYRangeMax() {
        return axeYRangeMax;
    }



    /** Indique si le range de l'axe Y se pack automatiquement */
    public void setAxeXRangeAutoPackable(boolean axeXRangeAutoPackable) {
        // Le fait d'activer l'auto pack efface le range fix�
        if (axeXRangeAutoPackable) {
            setAxeXRange(null, null);
        }

        this.axeXRangeAutoPackable = axeXRangeAutoPackable;
        try {
            if (listener != null)
                listener.doChangeAxeXRangeAutoPackable();
        } catch (Exception ex) {
            log.error("Error while changing X axis range automatic packing", ex); //$NON-NLS-1$
        }
    }



    public boolean isAxeXRangeAutoPackable() {
        return axeXRangeAutoPackable;
    }



    /** Indique si le range de l'axe Y se pack automatiquement */
    public void setAxeYRangeAutoPackable(boolean axeYRangeAutoPackable) {
        // Le fait d'activer l'auto pack efface le range fix�
        if (axeYRangeAutoPackable) {
            setAxeYRange(null, null);
        }

        this.axeYRangeAutoPackable = axeYRangeAutoPackable;
        try {
            if (listener != null)
                listener.doChangeAxeYRangeAutoPackable();
        } catch (Exception ex) {
            log.error("Error while changing Y axis range automatic packing", ex); //$NON-NLS-1$
        }
    }



    public boolean isAxeYRangeAutoPackable() {
        return axeYRangeAutoPackable;
    }



    /** D�finit la marge du range de l'axe X apr�s l'auto pack */
    public void setAxeXRangeAutoPackMarge(Number axeXRangeAutoPackMarge) {
        this.axeXRangeAutoPackMarge = axeXRangeAutoPackMarge;
        try {
            if (listener != null)
                listener.doChangeAxeXRangeAutoPackMarge();
        } catch (Exception ex) {
            log.error("Error while changing margin for range automatic packing", ex); //$NON-NLS-1$
        }
    }



    public Number getAxeXRangeAutoPackMarge() {
        return axeXRangeAutoPackMarge;
    }



    /** D�finit la marge du range de l'axe Y apr�s l'auto pack */
    public void setAxeYRangeAutoPackMarge(Number axeYRangeAutoPackMarge) {
        this.axeYRangeAutoPackMarge = axeYRangeAutoPackMarge;
        try {
            if (listener != null)
                listener.doChangeAxeYRangeAutoPackMarge();
        } catch (Exception ex) {
            log.error("Error while changing margin for range automatic packing", ex); //$NON-NLS-1$
        }
    }



    public Number getAxeYRangeAutoPackMarge() {
        return axeYRangeAutoPackMarge;
    }



    /** Fixe le pas entre chaque valeur de l'axe des abscisses */
    public void setAxeXPas(Number axeXPas) {
        this.axeXPas = axeXPas;
        try {
            if (listener != null)
                listener.doChangeAxeXPas();
        } catch (Exception ex) {
            log.error("Error while changing axe X step", ex); //$NON-NLS-1$
        }
    }



    public Number getAxeXPas() {
        return axeXPas;
    }



    /** Fixe le pas entre chaque valeur de l'axe des ordonn�es */
    public void setAxeYPas(Number axeYPas) {
        this.axeYPas = axeYPas;
        try {
            if (listener != null)
                listener.doChangeAxeYPas();
        } catch (Exception ex) {
            log.error("Error while changing axe Y step", ex); //$NON-NLS-1$
        }
    }



    public Number getAxeYPas() {
        return axeYPas;
    }



    /** Fixe la visibilit� des �tiquettes */
    public void setLabelsVisible(boolean labelsVisible) {
        this.labelsVisible = labelsVisible;
        try {
            if (listener != null)
                listener.doChangeLabelsVisible();
        } catch (Exception ex) {
            log.error("Error while changing labels visibility", ex); //$NON-NLS-1$
        }
    }



    public boolean isLabelsVisible() {
        return labelsVisible;
    }



    /** Fixe l'angle de d�part des donn�es du chart (si le chart en poss�de un) */
    public void setStartAngle(Double startAngle) {
        this.startAngle = startAngle;
        try {
            if (listener != null)
                listener.doChangeStartAngle();
        } catch (Exception ex) {
            log.error("Error while changing the starting data angle", ex); //$NON-NLS-1$
        }
    }



    public Double getStartAngle() {
        return startAngle;
    }



    /** Fixe le g�n�rateur de tooltip */
    public void setToolTipGenerator(ChartToolTipGenerator toolTipGenerator) {
        this.toolTipGenerator = toolTipGenerator;
        try {
            if (listener != null)
                listener.doChangeToolTipGenerator();
        } catch (Exception ex) {
            log.error("Error while changing the tooltip generator", ex); //$NON-NLS-1$
        }
    }



    public ChartToolTipGenerator getToolTipGenerator() {
        return toolTipGenerator;
    }
}
