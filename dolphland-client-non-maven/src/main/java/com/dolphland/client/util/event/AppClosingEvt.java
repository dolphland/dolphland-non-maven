package com.dolphland.client.util.event;

/**
 * Ev�nement �mis par le framework lors de la phase de fermeture de
 * l'application.<br>
 * Si aucun v�to n'est positionn� sur l'�v�nement, <code>AppClosedEvet</code>
 * sera �mis pour fermer d�finitivement l'application.
 * 
 * @author JayJay
 * @author jeremy.scafi
 * 
 */
public final class AppClosingEvt extends Event {

    private volatile boolean vetoClosing;



    public AppClosingEvt() {
        super(FwkEvents.APP_CLOSING_EVT);
    }



    /**
     * Positione le v�to � la demande de fermeture. Si le v�to est positionn�,
     * la fermeture sera annul�e.
     */
    public void vetoClosing() {
        this.vetoClosing = true;
    }



    /**
     * Indique si un v�to a �t� mis � la fermetue de l'application
     */
    public boolean isVetoClosing() {
        return vetoClosing;
    }

}
