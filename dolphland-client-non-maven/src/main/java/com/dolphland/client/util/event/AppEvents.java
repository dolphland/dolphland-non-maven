package com.dolphland.client.util.event;

public class AppEvents {

    public static final EventDestination LOAD_DIALOG_APROPOS_APP_EVT = new EventDestination("LOAD_DIALOG_APROPOS_APP_EVT");

    public static final EventDestination UI_SHOW_CTX_INFO_EVT = new EventDestination("UI_SHOW_CTX_INFO_EVT");

    public static final EventDestination UI_HIDE_CTX_INFO_EVT = new EventDestination("UI_HIDE_CTX_INFO_EVT");

    public static final EventDestination UI_START_DISABLED_MODE_EVT = new EventDestination("UI_START_DISABLED_MODE_EVT");

    public static final EventDestination UI_END_DISABLED_MODE_EVT = new EventDestination("UI_END_DISABLED_MODE_EVT");
}
