package com.dolphland.client.util.input;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.swingverifiers.Afficheur;

public class DateTextField extends CharacterTextField {

    private static final MessageFormater fmt = MessageFormater.getFormater(DateTextField.class);

    /**
     * Liste des policys pr�d�finies
     */
    public static final int ONLY_DATE = 0;
    public static final int ONLY_TIME = 1;
    public static final int DATE_AND_TIME = 2;

    /**
     * Caract�re �ditable par d�faut
     */
    public static final char DEFAULT_EDITABLE_CARACTERE = '_';

    /**
     * Constantes de classes
     */
    private static final String DEFAULT_VALUE_DATE_ONLY = "__/__/____"; //$NON-NLS-1$
    private static final String DEFAULT_VALUE_TIME_ONLY = "__:__:__"; //$NON-NLS-1$
    private static final String DEFAULT_VALUE_DATE_AND_TIME = "__/__/____.__:__:__"; //$NON-NLS-1$

    private static final String PATTERN_DATE_ONLY = "dd/MM/yyyy"; //$NON-NLS-1$
    private static final String PATTERN_TIME_ONLY = "HH:mm:ss"; //$NON-NLS-1$
    private static final String PATTERN_DATE_AND_TIME = "dd/MM/yyyy.HH:mm:ss"; //$NON-NLS-1$

    /**
     * Variables de classe
     */
    private int datePolicy;
    private boolean autoselection;
    private String correctFormat;
    private String defaultValue;
    private SimpleDateFormat sdf;
    private char editableCaractere;



    /**
     * construit un DateTextField
     */
    public DateTextField() {
        this(ONLY_DATE);
    }



    /**
     * construit un DateTextField
     * 
     * @param datePolicy
     *            : politique de date
     */
    public DateTextField(int datePolicy) {
        this(datePolicy, null);
    }



    /**
     * construit un DateTextField
     * 
     * @param datePolicy
     *            : politique de date
     * @param afficheur
     *            : afficheur de message d'erreur
     */
    public DateTextField(int datePolicy, Afficheur afficheur) {
        this(datePolicy, afficheur, DEFAULT_FOCUS_NEXT_ENABLED);
    }



    /**
     * construit un DateTextField
     * 
     * @param datePolicy
     *            : politique de date
     * @param afficheur
     *            : afficheur de message d'erreur
     * @param focusNextEnabled
     *            : transfert le focus au prochain composant focusable � la fin
     *            de la saisie
     */
    public DateTextField(int datePolicy, Afficheur afficheur, boolean focusNextEnabled) {
        super();
        editableCaractere = DEFAULT_EDITABLE_CARACTERE;
        autoselection = false;
        addCaretListener(new DateTextFieldCaretListener());
        addKeyListener(new DateTextFieldKeyListener());
        addFocusListener(new DateTextFieldFocusListener());
        setDatePolicy(datePolicy);
        setAfficheur(afficheur);
        setFocusNextEnabled(focusNextEnabled);
        setCaretColor(getSelectionColor());
        updateFormat();
    }



    @Override
    public boolean isFocusable() {
        // Emp�che le focus d'un composant non �ditable, non enabled ou non
        // visible
        return super.isFocusable() && isEditable() && isEnabled() && isVisible();
    }



    // METHODES PUBLIQUES

    /**
     * Initialise la date
     */
    public void initDate() {
        setText(defaultValue);
        select(0, 1);
        updateFormat();
    }



    /**
     * Fixe la date
     */
    public void setDate(Date date) {
        if (date == null) {
            initDate();
        } else {
            setText(sdf.format(date));
        }
    }



    /**
     * Fixe la policy de la date
     */
    public void setDatePolicy(int datePolicy) {

        if (datePolicy != ONLY_DATE && datePolicy != ONLY_TIME && datePolicy != DATE_AND_TIME) {
            throw new IllegalArgumentException("invalid datePolicy"); //$NON-NLS-1$
        }

        this.datePolicy = datePolicy;
        switch (datePolicy) {
            case ONLY_DATE :
                setDateFormat(PATTERN_DATE_ONLY, DEFAULT_VALUE_DATE_ONLY, DEFAULT_EDITABLE_CARACTERE);
                break;
            case ONLY_TIME :
                setDateFormat(PATTERN_TIME_ONLY, DEFAULT_VALUE_TIME_ONLY, DEFAULT_EDITABLE_CARACTERE);
                break;
            case DATE_AND_TIME :
                setDateFormat(PATTERN_DATE_AND_TIME, DEFAULT_VALUE_DATE_AND_TIME, DEFAULT_EDITABLE_CARACTERE);
                break;
        }

        updateFormat();
    }



    /**
     * Fixe le format de la date
     * 
     * @param patternValue
     *            : Pattern de la date (ex. : "dd/MM/yyyy.HH:mm:ss")
     * @param defaultValue
     *            : Valeur par d�faut du pattern (ex. : "__/__/____.__:__:__")
     * @param editableCaractere
     *            : Caract�re �ditable dans la valeur par d�faut du pattern (ex.
     *            : '_')
     */
    public void setDateFormat(String patternValue, String defaultValue, char editableCaractere) {
        this.editableCaractere = editableCaractere;
        this.defaultValue = defaultValue;
        setNbChar(defaultValue.length());
        sdf = new SimpleDateFormat(patternValue);
        initDate();

        updateFormat();
    }



    /**
     * Fournit la policy de la date
     */
    public int getDatePolicy() {
        return datePolicy;
    }



    /**
     * Renvoie la date
     */
    public Date getDate() {
        Date out = new Date();
        try {
            out = sdf.parse(getText());
        } catch (ParseException e) {
            return null;
        }
        return out;
    }



    /**
     * Indique si le next focus est actif
     */
    public boolean isFocusNextEnabled() {
        // Si le focusnext est vrai et la date est correcte
        if (super.isFocusNextEnabled() && verifDate()) {

            // On d�termine la position du prochain champ saissable
            int nextCaretPosition = getCaretPosition() - 1;

            do {
                nextCaretPosition++;

                if (nextCaretPosition >= getNbChar())
                    nextCaretPosition = 0;
            } while (!isEditableCaractere(new Character(defaultValue.charAt(nextCaretPosition)).toString()));

            // Le focus suivant est possible uniquement si le curseur est le
            // dernier champ saisissable
            return nextCaretPosition <= 0;
        }

        return false;
    }



    // METHODES INTERNES AU PACKAGE

    protected void initialize() {
        super.initialize();
        setDocument(new DateDocument());
    }



    protected String getCorrectFormat() {
        return correctFormat;
    }



    private boolean isEditableCaractere(String caractere) {
        return caractere.equals(new Character(editableCaractere).toString());
    }



    private boolean existAnyEditableCaracteres() {
        return getText().indexOf(editableCaractere) > 0;
    }



    private boolean verifDate() {

        // S'il reste des caract�res �ditables, la date ne peut �tre v�rifi�e
        if (existAnyEditableCaracteres()) {
            return false;
        }

        try {
            sdf.setLenient(false);
            return sdf.parse(getText()) != null;
        } catch (ParseException e) {
            return false;
        }
    }



    private void invalidFormatDate() {
        correctFormat = fmt.format("DateTextField.RS_INVALID_DATE"); //$NON-NLS-1$
        invalidFormat();
    }



    @Override
    public void transferFocus() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                DateTextField.super.transferFocus();
            }
        });
    }

    protected class DateDocument extends CharacterDocument {

        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            int len = str.length();

            // On ins�re la cha�ne
            super.insertString(offs, str, a);

            // Si insertion d'une cha�ne longue au d�but, on ne teste pas la
            // date
            if (offs == 0 && len > 1) {
                return;
            }

            // Si insertion d'un caract�re �ditable, on ne teste pas la date
            if (isEditableCaractere(str)) {
                return;
            }

            // S'il y a encore un caract�re �ditable, on ne teste pas la date
            if (existAnyEditableCaracteres()) {
                return;
            }

            // Si la date n'est pas bonne, on affiche un message d'erreur
            if (!verifDate()) {
                invalidFormatDate();
                return;
            }
        }
    }

    private class DateTextFieldCaretListener implements CaretListener {

        public void caretUpdate(CaretEvent e) {
            if (!autoselection) {
                // Si champ non saisissable, on passe au caract�re suivant
                if (e.getMark() >= defaultValue.length() || !isEditableCaractere(new Character(defaultValue.charAt(e.getMark())).toString())) {
                    int mark = e.getMark();

                    do {
                        mark++;

                        if (mark >= defaultValue.length())
                            mark = 0;
                    } while (!isEditableCaractere(new Character(defaultValue.charAt(mark)).toString()));

                    autoselect(mark, mark + 1);
                }
                else if (Math.abs(e.getDot() - e.getMark()) != 1) {
                    if (e.getMark() < getNbChar()) {
                        autoselect(e.getMark(), e.getMark() + 1);
                    } else {
                        autoselect(e.getMark() - 1, e.getMark());
                    }
                }
            }
        }



        /**
         * Lorsqu'on s�lectionne automatiquement
         */
        private void autoselect(final int selectionStart, final int selectionEnd) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    autoselection = true;
                    select(selectionStart, selectionEnd);
                    autoselection = false;
                }
            });
        }
    }

    private class DateTextFieldKeyListener extends KeyAdapter {

        /**
         * Lorsque on supprime une lettre
         */
        public void keyPressed(KeyEvent e) {
            // Si CTRL - X, on annule l'event
            if (e.getKeyCode() == KeyEvent.VK_X) {
                if (e.isControlDown()) {
                    e.consume();
                }
            }
            // Si CTRL - V, on annule l'event
            else if (e.getKeyCode() == KeyEvent.VK_V) {
                if (e.isControlDown()) {
                    e.consume();
                }
            }
            // Si par touche supprime
            else if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                e.consume();
                try {
                    int caretPosition = getSelectionStart();
                    // D�but de l'insertion automatique
                    insertionAutomatique = true;
                    getDocument().remove(caretPosition, 1);
                    getDocument().insertString(caretPosition, "" + editableCaractere, null); //$NON-NLS-1$
                    // Fin de l'insertion automatique
                    insertionAutomatique = false;
                    setCaretPosition(caretPosition);
                } catch (BadLocationException e1) {
                    // ignore
                }
            }
            // Si par touche retour arri�re
            else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                e.consume();
                try {
                    int caretPosition = getSelectionStart();
                    // D�but de l'insertion automatique
                    insertionAutomatique = true;
                    getDocument().remove(caretPosition, 1);
                    getDocument().insertString(caretPosition, new Character(editableCaractere).toString(), null);
                    // Fin de l'insertion automatique
                    insertionAutomatique = false;

                    // On recherche le caract�re pr�c�dent �tant �ditable
                    do {
                        caretPosition--;

                        if (caretPosition < 0) {
                            caretPosition = defaultValue.length() - 1;
                        }
                    } while (!isEditableCaractere(new Character(defaultValue.charAt(caretPosition)).toString()));

                    setCaretPosition(caretPosition);
                } catch (BadLocationException e1) {
                    // ignore
                }
            }
            // Si fl�che droite
            else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                setCaretPosition(getCaretPosition() - 1);
            }
            // Si fl�che droite
            else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                int caretPosition = getSelectionStart();

                // On recherche le caract�re pr�c�dent �tant �ditable
                do {
                    caretPosition--;

                    if (caretPosition < 0) {
                        caretPosition = defaultValue.length() - 1;
                    }
                } while (!isEditableCaractere(new Character(defaultValue.charAt(caretPosition)).toString()));

                setCaretPosition(caretPosition + 1);
            }
        }
    }

    private class DateTextFieldFocusListener implements FocusListener {

        /**
         * Lorsque l'on perd le focus
         */
        public void focusLost(FocusEvent e) {
            // Si la date n'est pas la valeur par d�faut et si la date n'est pas
            // v�rifi�e, on reprend le focus
            if (!getText().equals(defaultValue) && !verifDate()) {
                requestFocus();
                invalidFormatDate();
            } else {
                setCaretPosition(0);
            }
        }



        /**
         * Lorsque l'on gagne le focus
         */
        public void focusGained(FocusEvent e) {
            // initDate();
        }
    }
}
