package com.dolphland.client.util.application.components;

import info.clearthought.layout.TableLayout;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JMenuBar;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.action.UIActionGroup;
import com.dolphland.client.util.action.UIActionSupport;
import com.dolphland.client.util.action.UIActionUtil;
import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.View;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;

public class FwkInternalFrameWorkPane extends FwkWorkPane implements UIActionSupport{

	private JInternalFrame internalFrame;

	private JMenuBar menuBar;

	private FwkWorkPane workPaneDelegated;
	
	private List<UIAction<UIActionEvt, ?>> actions;

	private DisabledWorkPaneManager disabledWorkPaneManager;
	
	/**
	 * Construteur
	 */
	public FwkInternalFrameWorkPane(MasterFrame frame) {
		super(frame);
		internalFrame = new JInternalFrame();
		internalFrame.setResizable(true);
		internalFrame.setMaximizable(true);
		internalFrame.setClosable(true);
		internalFrame.setIconifiable(true);
		internalFrame.setContentPane(this);
		internalFrame.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		internalFrame.addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				dispose();
			}
		});
		
		actions = new ArrayList<UIAction<UIActionEvt,?>>();

		workPaneDelegated = new FwkWorkPane(frame);
		
		initialize();

		// D�finit le manager de d�sactivation du workPane
		disabledWorkPaneManager = new DisabledWorkPaneManager();
		disabledWorkPaneManager.addWorkPane(this);
	}
	
	private void initialize(){
		TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
		setLayout(layoutBuilder.get());

		add(getMenuBar(), layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
				.row(0)
				.width(TableLayout.FILL)
				.height(TableLayout.PREFERRED)
				.get()));
		
		add(workPaneDelegated, layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
				.row(1)
				.height(TableLayout.FILL)
				.get()));
	}
	
	@Override
	public void setView(View view) {
		workPaneDelegated.setView(view);
	}
	
	@Override
	public View getView() {
		return workPaneDelegated==null?null:workPaneDelegated.getView();
	}
	
	/**
	 * Ajout d'une action (<code>UIAction</code>) ou d'un groupe d'actions (<code>UIActionGroup</code>)
	 * 
	 * @param action Action ou groupe d'actions � ajouter
	 */
	public void addAction(UIAction action){
		addTriggerContext(action);
		actions.add(action);
		updateActions();
	}
	
	/**
	 * Suppression de toutes les actions
	 */
	public void removeAllActions(){
		actions = new ArrayList<UIAction<UIActionEvt,?>>();
		updateActions();
	}
	
	private void addTriggerContext(UIAction action){
		if(action instanceof UIActionGroup){
			for(UIAction<UIActionEvt, ?> subAction : ((UIActionGroup<UIActionEvt, ?>)action).getActions()){
				addTriggerContext(subAction);
			}
		}else{
			action.setTriggerModifier(action.getTriggerModifier() | UIAction.TRIGGER_CONTEXT);
		}
	}
	
	/**
	 * Mise � jour des actions
	 * 
	 */
	public void updateActions(){
		getMenuBar().removeAll();
		UIActionUtil.updateMenuBarFromActions(actions, new UIActionEvt(), getMenuBar());
	}
	
	private JMenuBar getMenuBar(){
		if(menuBar == null){
			menuBar = new JMenuBar();
		}
		return menuBar;
	}
	
	/**
	 * D�sactive le WorkPane en affichant un message
	 * @param message Message � afficher
	 */
	public void startDisabledMode(String message){
		disabledWorkPaneManager.startDisabledMode(message);
	}
	
	/**
	 * D�sactive le WorkPane en affichant un message tout en indiquant si la souris peut traverser ou non
	 * @param message Message � afficher
	 * @param mouseTraversable Indique si la souris peut traverser ou non
	 */
	public void startDisabledMode(String message, boolean mouseTraversable){
		disabledWorkPaneManager.startDisabledMode(message, mouseTraversable);
	}
	
	/**
	 * R�active le WorkPane
	 */
	public void endDisabledMode(){
		disabledWorkPaneManager.endDisabledMode();
	}
	
	/** Affiche la fen�tre interne */
	public void show() {
		setVisible(true);
	}

	/** Cache la fen�tre interne */
	public void hide() {
		setVisible(false);
	}

	/** La fermeture de la zone de travail entra�ne la fermeture de la fen�tre interne */
	public void close() {
		disabledWorkPaneManager.freeEDT();
		internalFrame.dispose();
	}
	
	/** Fixe le titre de la fen�tre interne */
	public void setTitle(String title) {
		internalFrame.setTitle(title);
	}
	
	/** Fixe la taille de la fen�tre interne */
	public void setSize(int width, int height) {
		internalFrame.setSize(width, height);
	}

	public void setSize(Dimension dim) {
		internalFrame.setSize(dim);
	}

	/** Fixe la position de la fen�tre interne */
	public void setLocation(int x, int y) {
		internalFrame.setLocation(x, y);
	}

	/** Fixe la position de la fen�tre interne */
	public void setLocation(Point p) {
		internalFrame.setLocation(p);
	}	
	
	/** Affiche/Cache la fen�tre interne */
	public void setVisible(boolean aFlag) {
		internalFrame.setVisible(aFlag);
	}

	/** Ferme la fen�tre interne */
	public void dispose() {
		close();
	}

	/** Ajoute un listener InternalFrame sur la fen�tre interne */
	public void addInternalFrameListener(InternalFrameListener listener) {
		internalFrame.addInternalFrameListener(listener);
	}
	
	/** R�cup�re la fen�tre interne */
	public JInternalFrame getInternalFrame() {
		return internalFrame;
	}
}
