package com.dolphland.client.util.chart;

import com.dolphland.client.util.action.UIActionEvt;

/**
 * Event class to get necessary parameters to execute a <code>ChartAction</code>
 * 
 * @author J�r�my Scafi
 * @author JayJay
 * 
 */
public class ChartActionEvt extends UIActionEvt {

    private Comparable selectedIdChartData;
    private Integer selectedIndData;



    /**
     * @return the selectedIdChartData
     */
    public Comparable getSelectedIdChartData() {
        return selectedIdChartData;
    }



    /**
     * @param selectedIdChartData
     *            the selectedIdChartData to set
     */
    public void setSelectedIdChartData(Comparable selectedIdChartData) {
        this.selectedIdChartData = selectedIdChartData;
    }



    /**
     * @return the selectedIndData
     */
    public Integer getSelectedIndData() {
        return selectedIndData;
    }



    /**
     * @param selectedIndData
     *            the selectedIndData to set
     */
    public void setSelectedIndData(Integer selectedIndData) {
        this.selectedIndData = selectedIndData;
    }
}
