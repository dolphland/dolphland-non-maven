package com.dolphland.client.util.application.components;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.dolphland.client.util.dialogs.DialogUtil;
import com.dolphland.client.util.dialogs.FwkDialog;
import com.dolphland.client.util.i18n.MessageFormater;

class AProposDialog extends FwkDialog{

	private static final MessageFormater fmt = MessageFormater.getFormater(AProposDialog.class);
	
	private JScrollPane jspAPropos;
	
	private JPanel panelAPropos;
	
	private int nbInfosAPropos;
	
	public AProposDialog(Frame owner) {
		super(owner);
		nbInfosAPropos = 0;
		setTitle(fmt.format("AProposDialog.RS_ABOUT")); //$NON-NLS-1$
		setModal(false);
		setContentPane(getJspAPropos());
	}
	
	public void init(){
		setSize(400, 200);
		DialogUtil.centerOnScreen(this);
	}

	private JScrollPane getJspAPropos(){
		if(jspAPropos == null){
			jspAPropos = new JScrollPane();
			jspAPropos.setViewportView(getPanelAPropos());
		}
		
		return jspAPropos;
	}
	
	private JPanel getPanelAPropos(){
		if(panelAPropos == null){
			panelAPropos = new JPanel();
			panelAPropos.setLayout(new GridBagLayout());
		}
		
		return panelAPropos;
	}
	
	public void addInfo(String libelle, String valeur){
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = nbInfosAPropos;
		gc.anchor = GridBagConstraints.EAST;
		getPanelAPropos().add(new JLabel(libelle + " : "), gc); //$NON-NLS-1$
		gc = new GridBagConstraints();
		gc.gridx = 1;
		gc.gridy = nbInfosAPropos;
		gc.anchor = GridBagConstraints.WEST;
		getPanelAPropos().add(new JLabel(valeur), gc);
		nbInfosAPropos++;
	}
	
	public void setVisible(boolean visible) {
		
		// On n'affiche la dialog d'A Propos qu'uniquement si des infos y ont �t� ajout�es
		super.setVisible(visible && nbInfosAPropos > 0);
	}
}
