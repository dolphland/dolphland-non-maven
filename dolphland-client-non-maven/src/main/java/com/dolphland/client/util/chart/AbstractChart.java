package com.dolphland.client.util.chart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.LegendItem;
import org.jfree.chart.entity.CategoryItemEntity;
import org.jfree.chart.entity.PieSectionEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.util.ShapeUtilities;

import com.dolphland.client.util.action.ActionsContext;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.action.UIActionEvtFactory;
import com.dolphland.client.util.action.UIActionUtil;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.UIShowAbsoluteCtxInfoEvt;
import com.dolphland.client.util.event.UIShowCtxInfoEvt;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;

/**
 * Cette classe permet de repr�senter un chart avec une l�gende am�lior�e.
 * 
 * La principale am�lioration de cette l�gende est de pouvoir d�cocher certaines
 * s�ries pour ne pas les voir appara�tre sur le chart.
 * 
 * La repr�sentation du Chart se fait dans le JComponent componentChart que l'on
 * peut obtenir par getChartComponent()
 * 
 * @author jeremy.scafi
 */
public abstract class AbstractChart extends JComponent implements UIActionEvtFactory<ChartActionEvt> {

    private final static Logger log = Logger.getLogger(AbstractChart.class);

    private final static int PIXEL_TOLERANCE_CHART_MOUSE_POSITION = 10;

    private static final MessageFormater fmt = MessageFormater.getFormater(AbstractChart.class);

    private final static int SYMBOL_SIZE_PIXEL = 10;

    private final static int INSET_LEGENDE_PIXEL = 5;

    private final static long CTXINFO_LIFE_TIME = 3000;

    // Contient les entit�s de donn�es identifi�es par une clef
    private HashMap<Comparable, ChartData> mapChartData;

    // Contraintes du chart
    private ChartConstraints chartConstraints;

    // Objet Chart JFreeChart
    private JFreeChart chart;

    // Listener reliant le Chart au ChartConstraints pour modifier les
    // contraintes en temps r�el
    private ChartConstraintsListener chartListener;

    // UI du Chart
    private ChartPanel panelChart;

    // L�gende du chart
    private JPanel panelLegendeCompress;
    private PanelLegende panelLegende;
    private boolean legendExtended;
    private LegendItem[] legendItems;

    // Font de la l�gende
    private Font legendFont;

    // Bordure par d�faut du chart
    private Border defaultBorder;

    // Composant principal du chart
    private FwkPanel mainComponent;

    // Composant contenant le chart
    private JComponent chartComponent;

    // Stockage de la visibilit� dans la l�gende, du checkable et de l'affichage
    // d'entit�s
    private Map<Comparable, Boolean> mapChartDataCheckable;
    private Map<Comparable, Boolean> mapChartDataVisibleInLegende;
    private Map<Comparable, Boolean> mapChartDataVisible;

    // Actions
    private List<UIAction<ChartActionEvt, ?>> actions;

    // Contexte pour les actions
    private ActionsContext actionsContext;

    // Panels des actions
    private JPanel topActionsPanel;
    private JPanel bottomActionsPanel;
    private JPanel leftActionsPanel;
    private JPanel rightActionsPanel;
    private int groupActionCount = Integer.MAX_VALUE;



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartComponent
     *            : Composant devant repr�senter le Chart
     * @param chartConstraints
     *            : Contraintes � appliquer au Chart
     */
    public AbstractChart(JPanel chartComponent, ChartConstraints chartConstraints) {
        this.chartConstraints = chartConstraints;
        this.chartComponent = chartComponent;
        this.legendItems = new LegendItem[0];
        this.actions = new ArrayList<UIAction<ChartActionEvt, ?>>();
        this.actionsContext = new ActionsContext(this);
        this.topActionsPanel = createActionsPanel();
        this.bottomActionsPanel = createActionsPanel();
        this.leftActionsPanel = createActionsPanel();
        this.rightActionsPanel = createActionsPanel();
        chartComponent.addAncestorListener(new AncestorListener() {

            public void ancestorRemoved(AncestorEvent event) {
                compressLegend();
            }



            public void ancestorAdded(AncestorEvent event) {
            }



            public void ancestorMoved(AncestorEvent event) {
            }
        });
        mapChartDataCheckable = new HashMap<Comparable, Boolean>();
        mapChartDataVisibleInLegende = new HashMap<Comparable, Boolean>();
        mapChartDataVisible = new HashMap<Comparable, Boolean>();
        legendExtended = false;
        defaultBorder = chartComponent.getBorder();
        chartConstraints.setListener(getChartListener());
        mapChartData = new HashMap<Comparable, ChartData>();
        if (UIManager.get("AbstractChart.font") != null) {
            legendFont = UIManager.getFont("AbstractChart.font");
        }
        getChartConstraints().applyAllConstraints();
    }



    /** Cr�e le chart (� d�finir) */
    protected abstract JFreeChart createChart();



    /** Renvoie la liste des items de la l�gende (� d�finir) */
    protected abstract LegendItem[] getLegendItems();



    /**
     * Cr�e l'impl�mentation du listener permettant de modifier les contraintes
     * du graphe en temps r�el (� d�finir)
     */
    protected abstract ChartConstraintsListener createChartConstraintsListenerImpl();



    /**
     * Fournit l'impl�mentation de l'interface ChartSelection permettant de
     * s�lectionner une donn�e du graphe
     */
    protected abstract ChartSelection getChartSelection();



    /**
     * Fournit l'identifiant de l'entit� de donn�es � partir de son indice dans
     * le chart
     */
    protected Comparable getIdChartData(int indSeries) {
        for (ChartData chartData : getAllChartData()) {
            if (chartData.getIndSeries() == indSeries) {
                return chartData.getIdChartData();
            }
        }
        return null;
    }



    /** Fournit le Shape de la l�gende */
    protected Shape getLegendShape(int indLegendItem, LegendItem legendItem) {
        if (getChartConstraints().getShapesChartData() != null && getChartConstraints().getShapesChartData().length > indLegendItem) {
            return createShape(getChartConstraints().getShapesChartData()[indLegendItem]);
        }
        return getDefaultLegendShape(indLegendItem, legendItem);
    }



    /** Fournit le Shape par d�faut */
    protected Shape getDefaultLegendShape(int indLegendItem, LegendItem legendItem) {
        return createShape(ChartShape.SQUARE);
    }



    /** Cr�e le shape � partir du chartShape */
    protected Shape createShape(ChartShape chartShape) {
        Shape shape = null;
        switch (chartShape) {
            case LINE :
                shape = new Line2D.Double(-3, -1.5, 6, -1.5);
                break;

            case SQUARE :
                shape = new Rectangle2D.Double(-3, -3, 6, 6);
                break;

            case CIRCLE :
                shape = new Ellipse2D.Double(-3, -3, 6, 6);
                break;

            case UP_TRIANGLE :
                shape = new Polygon(new int[] { 0, 3, -3 }, new int[] { -3, 3, 3 }, 3);
                break;

            case DIAMOND :
                shape = new Polygon(new int[] { 0, 3, 0, -3 }, new int[] { -3, 0, 3, 0 }, 4);
                break;

            case HORIZONTAL_RECTANGLE :
                shape = new Rectangle2D.Double(-3, -1.5, 6, 3);
                break;

            case DOWN_TRIANGLE :
                shape = new Polygon(new int[] { -3, 3, 0 }, new int[] { -3, -3, 3 }, 3);
                break;

            case CROSS :
                shape = ShapeUtilities.createDiagonalCross(3, 1);
                break;

            case RIGHT_TRIANGLE :
                shape = new Polygon(new int[] { -3, 3, -3 }, new int[] { -3, 0, 3 }, 3);
                break;

            case VERTICAL_RECTANGLE :
                shape = new Rectangle2D.Double(-1.5, -3, 3, 6);
                break;

            case LEFT_TRIANGLE :
                shape = new Polygon(new int[] { -3, 3, 3 }, new int[] { 0, -3, 3 }, 3);
                break;

            case DOT :
                shape = new Rectangle2D.Double(-3, -3, -3, -3);
                break;

            default :
                shape = new Rectangle2D.Double(-3, -3, 6, 6);
                break;
        }
        return shape;
    }



    /** Indique si la s�lection est possible dans le graphe */
    protected boolean isSelectionEnabled() {
        // La s�lection est possible si au moins une action est rattach�e au
        // graphe
        return actions != null && !actions.isEmpty();
    }



    /** Renvoie les contraintes du chart */
    public ChartConstraints getChartConstraints() {
        return chartConstraints;
    }



    /** Fournit le chart sous la forme d'un composant swing */
    public JComponent getChartComponent() {
        if (panelChart == null) {
            panelChart = createPanelChart();
            panelLegende = createPanelLegende();
            panelLegendeCompress = createPanelLegendeCompress();

            chartComponent.setLayout(new GridBagLayout());

            GridBagConstraints gcChart = new GridBagConstraints();
            gcChart.fill = GridBagConstraints.BOTH;
            gcChart.gridx = 1;
            gcChart.gridy = 1;
            gcChart.weightx = 1;
            gcChart.weighty = 1;

            chartComponent.add(panelChart, gcChart);

            // Raffra�chit la visibilit� des menu item
            refreshVisibilityMenuItems();

            // Raffra�chit si le zoom par clic est possible
            refreshMouseZoomable();

            // Raffra�chit les items de l�gende
            refreshLegendItems();

            // Raffra�chit la position de la l�gende
            refreshLegendPosition();

            // Cr�e le composant principal du graphe contenant le graphe et les
            // barres d'actions
            mainComponent = new FwkPanel();
            mainComponent.setTypeBorder(FwkPanel.TYPE_NO_BORDER);
            mainComponent.setOpaque(false);

            mainComponent.setLayout(new BorderLayout());

            mainComponent.add(chartComponent, BorderLayout.CENTER);
            mainComponent.add(topActionsPanel, BorderLayout.NORTH);
            mainComponent.add(bottomActionsPanel, BorderLayout.SOUTH);
            mainComponent.add(leftActionsPanel, BorderLayout.WEST);
            mainComponent.add(rightActionsPanel, BorderLayout.EAST);
        }
        return mainComponent;
    }



    /** Fournit le chart sous la forme d'une image */
    public BufferedImage getChartImage(int width, int height) {
        if (getChart() == null) {
            return null;
        }
        return getChart().createBufferedImage(width, height);
    }



    /**
     * Fixe le nombre d'actions � regrouper sur un bloc d'affichage. Un bloc
     * d'affichage
     * peut �tre une ligne ou une colonne suivant le mode d'affichage choisi
     * pour les actions.<br>
     * Si le bloc est une ligne, groupActionCount est le nombre d'actions par
     * ligne, si le bloc
     * est une colonne, groupActionCount est le nombre d'actions par colonne.
     * 
     * @param groupActionCount
     *            Le nombre d'actions par bloc.
     */
    public void setGroupActionCount(int groupActionCount) {
        this.groupActionCount = groupActionCount;
    }



    /**
     * Cr�e le panel d'actions
     * 
     * @return Panel d'actions
     */
    private JPanel createActionsPanel() {
        JPanel out = new JPanel();
        out.setLayout(new GridBagLayout());
        return out;
    }



    /** Fixe les items de la l�gende */
    private void setLegendItems(LegendItem[] legendItems) {
        this.legendItems = legendItems;
        refreshLegendItems();
    }



    private ChartConstraintsListener getChartListener() {
        if (chartListener == null) {
            chartListener = createChartConstraintsListenerImpl();
        }
        return chartListener;
    }



    /** Raffra�chit le panel du chart */
    private void repaintPanelChart() {
        if (panelChart == null) {
            return;
        }
        panelChart.repaint();
    }



    /** Raffra�chit le panel de l�gende */
    private void repaintPanelLegende() {
        if (panelLegende == null) {
            return;
        }
        panelLegende.repaint();
    }



    /** Raffra�chit la visibilit� des menu item */
    private void refreshVisibilityMenuItems() {
        if (panelChart == null) {
            return;
        }

        // Parcourt des menu items du popup menu
        JPopupMenu popMenu = panelChart.getPopupMenu();

        for (int i = 0; i < popMenu.getComponentCount(); i++) {

            Component popComponent = popMenu.getComponent(i);

            if (popComponent instanceof JMenuItem) {
                JMenuItem popItem = (JMenuItem) popComponent;

                // Si menu item � cacher
                if (!getChartConstraints().isMenuItemVisible(popItem.getText())) {
                    popItem.setVisible(false);

                    // Indique si l'item pr�c�dent est un item menu visible
                    boolean itemMenuVisible = (i - 1 >= 0) && (popMenu.getComponent(i - 1) instanceof JMenuItem) && ((JMenuItem) popMenu.getComponent(i - 1)).isVisible();

                    // Si l'�l�ment suivant est un s�parateur
                    boolean itemSeparator = (i + 1 < popMenu.getComponentCount()) && (popMenu.getComponent(i + 1) instanceof JSeparator);

                    if (!itemMenuVisible && itemSeparator) {
                        popMenu.getComponent(i + 1).setVisible(false);
                    }

                    // Indique si l'item suivant est un item menu visible
                    itemMenuVisible = (i + 1 < popMenu.getComponentCount()) && (popMenu.getComponent(i + 1) instanceof JMenuItem) && ((JMenuItem) popMenu.getComponent(i + 1)).isVisible();

                    // Si l'�l�ment pr�c�dent est un s�parateur
                    itemSeparator = (i - 1 >= 0) && (popMenu.getComponent(i - 1) instanceof JSeparator);

                    if (!itemMenuVisible && itemSeparator) {
                        popMenu.getComponent(i - 1).setVisible(false);
                    }
                }
                // Si menu item � montrer
                else {
                    popItem.setVisible(true);

                    // Si l'�l�ment suivant est un s�parateur
                    boolean itemSeparator = (i + 1 < popMenu.getComponentCount()) && (popMenu.getComponent(i + 1) instanceof JSeparator);

                    if (itemSeparator) {
                        popMenu.getComponent(i + 1).setVisible(true);
                    }

                    // Si l'�l�ment pr�c�dent est un s�parateur
                    itemSeparator = (i - 1 >= 0) && (popMenu.getComponent(i - 1) instanceof JSeparator);

                    if (itemSeparator) {
                        popMenu.getComponent(i - 1).setVisible(true);
                    }
                }
            }
        }
    }



    /** Raffra�chit si le zoom par clic est possible */
    private void refreshMouseZoomable() {
        if (panelChart == null) {
            return;
        }
        panelChart.setMouseZoomable(getChartConstraints().isZoomWithClickEnabled());
    }



    /** Raffra�chit les items de l�gende */
    private void refreshLegendItems() {
        if (panelLegende == null) {
            return;
        }
        panelLegende.setLegendItems(legendItems);
    }



    /** Raffra�chit la position de la l�gende */
    private void refreshLegendPosition() {
        if (chartComponent == null || panelLegende == null || panelLegendeCompress == null) {
            return;
        }

        chartComponent.remove(panelLegende);
        chartComponent.remove(panelLegendeCompress);

        GridBagConstraints gcLegende = new GridBagConstraints();
        gcLegende.gridx = 1;
        gcLegende.gridy = 1;

        switch (getChartConstraints().getLegendePosition()) {
            case ChartConstraints.POSITION_BOTTOM_LEFT :
                gcLegende.fill = GridBagConstraints.VERTICAL;
                gcLegende.weightx = 1;
                gcLegende.gridx = gcLegende.gridx;
                gcLegende.gridy = gcLegende.gridy + 1;
                gcLegende.anchor = GridBagConstraints.WEST;
                break;

            case ChartConstraints.POSITION_BOTTOM :
                gcLegende.fill = GridBagConstraints.VERTICAL;
                gcLegende.weightx = 1;
                gcLegende.gridx = gcLegende.gridx;
                gcLegende.gridy = gcLegende.gridy + 1;
                gcLegende.anchor = GridBagConstraints.CENTER;
                break;

            case ChartConstraints.POSITION_BOTTOM_RIGHT :
                gcLegende.fill = GridBagConstraints.VERTICAL;
                gcLegende.weightx = 1;
                gcLegende.gridx = gcLegende.gridx;
                gcLegende.gridy = gcLegende.gridy + 1;
                gcLegende.anchor = GridBagConstraints.EAST;
                break;

            case ChartConstraints.POSITION_TOP_LEFT :
                gcLegende.fill = GridBagConstraints.VERTICAL;
                gcLegende.weightx = 1;
                gcLegende.gridx = gcLegende.gridx;
                gcLegende.gridy = gcLegende.gridy - 1;
                gcLegende.anchor = GridBagConstraints.WEST;
                break;

            case ChartConstraints.POSITION_TOP :
                gcLegende.fill = GridBagConstraints.VERTICAL;
                gcLegende.weightx = 1;
                gcLegende.gridx = gcLegende.gridx;
                gcLegende.gridy = gcLegende.gridy - 1;
                gcLegende.anchor = GridBagConstraints.CENTER;
                break;

            case ChartConstraints.POSITION_TOP_RIGHT :
                gcLegende.fill = GridBagConstraints.VERTICAL;
                gcLegende.weightx = 1;
                gcLegende.gridx = gcLegende.gridx;
                gcLegende.gridy = gcLegende.gridy - 1;
                gcLegende.anchor = GridBagConstraints.EAST;
                break;

            case ChartConstraints.POSITION_LEFT_TOP :
                gcLegende.fill = GridBagConstraints.HORIZONTAL;
                gcLegende.weighty = 1;
                gcLegende.gridx = gcLegende.gridx - 1;
                gcLegende.gridy = gcLegende.gridy;
                gcLegende.anchor = GridBagConstraints.NORTH;
                break;

            case ChartConstraints.POSITION_LEFT :
                gcLegende.fill = GridBagConstraints.HORIZONTAL;
                gcLegende.weighty = 1;
                gcLegende.gridx = gcLegende.gridx - 1;
                gcLegende.gridy = gcLegende.gridy;
                gcLegende.anchor = GridBagConstraints.CENTER;
                break;

            case ChartConstraints.POSITION_LEFT_BOTTOM :
                gcLegende.fill = GridBagConstraints.HORIZONTAL;
                gcLegende.weighty = 1;
                gcLegende.gridx = gcLegende.gridx - 1;
                gcLegende.gridy = gcLegende.gridy;
                gcLegende.anchor = GridBagConstraints.SOUTH;
                break;

            case ChartConstraints.POSITION_RIGHT_TOP :
                gcLegende.fill = GridBagConstraints.HORIZONTAL;
                gcLegende.weighty = 1;
                gcLegende.gridx = gcLegende.gridx + 1;
                gcLegende.gridy = gcLegende.gridy;
                gcLegende.anchor = GridBagConstraints.NORTH;
                break;

            case ChartConstraints.POSITION_RIGHT :
                gcLegende.fill = GridBagConstraints.HORIZONTAL;
                gcLegende.weighty = 1;
                gcLegende.gridx = gcLegende.gridx + 1;
                gcLegende.gridy = gcLegende.gridy;
                gcLegende.anchor = GridBagConstraints.CENTER;
                break;

            case ChartConstraints.POSITION_RIGHT_BOTTOM :
                gcLegende.fill = GridBagConstraints.HORIZONTAL;
                gcLegende.weighty = 1;
                gcLegende.gridx = gcLegende.gridx + 1;
                gcLegende.gridy = gcLegende.gridy;
                gcLegende.anchor = GridBagConstraints.SOUTH;
                break;

            default :
                break;
        }

        if (getChartConstraints().getLegendePosition() != ChartConstraints.POSITION_NONE) {

            // Si la l�gende est sous une forme compress�e pouvant s'�tendre si
            // souris dessus
            if (getChartConstraints().isLegendeExtendable()) {
                chartComponent.add(panelLegendeCompress, gcLegende);
            }
            // Sinon
            else {
                chartComponent.add(panelLegende, gcLegende);
            }
        }
    }



    /** Fournit le chart */
    protected JFreeChart getChart() {
        if (chart == null) {
            chart = createChart();
        }
        return chart;
    }



    /** Met � jour les couleurs des donn�es */
    protected void updateColorsChartData() {
        // Fixe les couleurs des donn�es du chart
        try {
            getChartListener().doChangeColorsChartData();
        } catch (Exception ex) {
            log.error("Error while changing chart data's colors", ex); //$NON-NLS-1$
        }
    }



    /** Met � jour les formes des donn�es */
    protected void updateShapesChartData() {
        // Fixe les formes des donn�es du chart
        try {
            getChartListener().doChangeShapesChartData();
        } catch (Exception ex) {
            log.error("Error while changing chart data's shapes", ex); //$NON-NLS-1$
        }
    }



    /** Fournit l'entit� de donn�es identifi�e par idChartData */
    protected ChartData getChartData(Comparable idChartData) {
        return mapChartData.get(idChartData);
    }



    /** Fournit l'ensemble des entit�s de donn�es */
    protected Collection<ChartData> getAllChartData() {
        return mapChartData.values();
    }



    /** Stocke l'entit� de donn�es identifi�e par idChartData */
    protected void putChartData(ChartData chartData) {
        if (!mapChartData.containsKey(chartData.getIdChartData())) {
            mapChartData.put(chartData.getIdChartData(), chartData);

            // On indique que les entit�s de donn�es affich�es ont chang�
            fireChartDataChanged();
        }
    }



    /** Indique le nombre de donn�es */
    protected int getChartDataCount() {
        return mapChartData.size();
    }



    /** Supprime toutes les entit�s de donn�es */
    protected void clearChartData() {
        // On r�initialise la liste de entit�s de donn�es
        mapChartData = new HashMap();

        // On indique que les entit�s de donn�es affich�es ont chang�
        fireChartDataChanged();
    }



    /** Indique si une entit� de donn�es identifi�e par idChartData est connue */
    protected boolean containsChartData(Comparable idChartData) {
        return mapChartData.containsKey(idChartData);
    }



    /**
     * Fixe si une entit� de donn�es identifi�e par idChartData est checkable ou
     * non
     */
    public void setChartDataCheckable(Comparable idChartData, boolean checkable) {

        mapChartDataCheckable.put(idChartData, checkable);

        // On indique que les entit�s de donn�es affich�es ont chang�
        fireChartDataChanged();
    }



    /**
     * Fixe si une entit� de donn�es identifi�e par idChartData est visible dans
     * la legende ou non
     */
    public void setChartDataVisibleInLegende(Comparable idChartData, boolean visibleInLegende) {

        mapChartDataVisibleInLegende.put(idChartData, visibleInLegende);

        // On indique que les entit�s de donn�es affich�es ont chang�
        fireChartDataChanged();
    }



    /** Affiche/Cache une entit� de donn�es identifi�e par idChartData */
    public void setChartDataVisible(Comparable idChartData, boolean visible) {

        mapChartDataVisible.put(idChartData, visible);

        // On raffra�chit l'affichage du chart et de sa l�gende
        repaintPanelChart();
        repaintPanelLegende();
    }



    /**
     * Indique si une entit� de donn�es identifi�e par idChartData est checkable
     * ou non
     */
    public boolean isChartDataCheckable(Comparable idChartData) {

        if (mapChartDataCheckable.containsKey(idChartData)) {
            return mapChartDataCheckable.get(idChartData).booleanValue();
        }

        // Valeur par d�faut, vrai
        return true;
    }



    /**
     * Indique si une entit� de donn�es identifi�e par idChartData est visible
     * dans la legende ou non
     */
    public boolean isChartDataVisibleInLegende(Comparable idChartData) {

        if (mapChartDataVisibleInLegende.containsKey(idChartData)) {
            return mapChartDataVisibleInLegende.get(idChartData).booleanValue();
        }

        // Valeur par d�faut, vrai
        return true;
    }



    /** Indique si une entit� de donn�es identifi�e par idChartData est visible */
    public boolean isChartDataVisible(Comparable idChartData) {

        if (mapChartDataVisible.containsKey(idChartData)) {
            return mapChartDataVisible.get(idChartData).booleanValue();
        }

        // Valeur par d�faut, vrai
        return true;
    }



    /**
     * Permet d'initialiser ou de r�initialiser le chart.
     */
    public void initChart() {
        clearChartData();
    }



    /**
     * D�finis le contexte des actions
     * 
     * @param actionsContext
     *            Contexte des actions
     */
    public void setActionsContext(ActionsContext actionsContext) {
        this.actionsContext = actionsContext;
    }



    /**
     * Attache une action au graphe
     * 
     * @param action
     *            Action
     */
    public <P extends UIActionEvt> void bind(UIAction<P, ?> action) {
        if (actionsContext == null) {
            throw new NullPointerException("Please set ActionsContext !"); //$NON-NLS-1$
        }
        if (action == null) {
            throw new NullPointerException("Please specify a ChartAction not null !"); //$NON-NLS-1$
        }

        // Add action
        actions.add((UIAction<ChartActionEvt, ?>) action);

        // If action is trigger option, create and bind button
        if (action.isTriggerEnabled(UIAction.TRIGGER_OPTION)) {
            JButton button = new JButton();

            GridBagConstraints gc = new GridBagConstraints();

            switch (chartConstraints.getButtonsPanelPosition()) {
            // Si position haute
                case ChartConstraints.POSITION_TOP :
                    gc.gridx = topActionsPanel.getComponentCount() % groupActionCount;
                    gc.gridy = (int) Math.floor((double) topActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    topActionsPanel.add(button, gc);
                    break;

                // Si position basse
                case ChartConstraints.POSITION_BOTTOM :
                    gc.gridx = bottomActionsPanel.getComponentCount() % groupActionCount;
                    gc.gridy = (int) Math.floor((double) bottomActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    bottomActionsPanel.add(button, gc);
                    break;

                // Si position gauche
                case ChartConstraints.POSITION_LEFT :
                    gc.gridx = (int) Math.floor((double) leftActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.gridy = leftActionsPanel.getComponentCount() % groupActionCount;
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    leftActionsPanel.add(button, gc);
                    break;

                // Si position droite
                case ChartConstraints.POSITION_RIGHT :
                    gc.gridx = (int) Math.floor((double) rightActionsPanel.getComponentCount() / (double) groupActionCount);
                    gc.gridy = rightActionsPanel.getComponentCount() % groupActionCount;
                    gc.weightx = 0;
                    gc.weighty = 0;
                    gc.anchor = GridBagConstraints.CENTER;
                    gc.fill = GridBagConstraints.HORIZONTAL;
                    gc.insets = new Insets(5, 5, 5, 5);
                    rightActionsPanel.add(button, gc);
                    break;
            }

            // Bind button for action
            actionsContext.bind(action, button);
        }
    }



    /**
     * D�tache une action du graphe
     * 
     * @param action
     *            Action
     */
    public <P extends UIActionEvt> void unbind(UIAction<P, ?> action) {
        if (actionsContext != null && action != null) {
            // Remove action
            actions.remove((UIAction<ChartActionEvt, ?>) action);

            // Unbind action
            actionsContext.unbind((UIAction<UIActionEvt, ?>) action);
        }
    }



    /**
     * Cr�e l'�v�nement utilis� par les actions du graphe
     */
    public ChartActionEvt createUIActionEvt() {
        ChartSelection chartSelection = getChartSelection();
        ChartActionEvt evt = new ChartActionEvt();
        if (chartSelection.isSelectionActive()) {
            evt.setSelectedIdChartData(getIdChartData(chartSelection.getSelectedSeries()));
            evt.setSelectedIndData(chartSelection.getSelectedData());
        }
        return evt;
    }



    private ChartPanel createPanelChart() {
        ChartPanel out = new ChartPanel(getChart()) {

            @Override
            public void paintComponent(Graphics g) {
                // Parcourt des entit�s
                for (ChartData chartData : mapChartData.values()) {
                    // Si la visilibilit� du chartData n'est pas updat�e, on
                    // rend visible/invisible l'entit�
                    if (chartData.isVisible() != isChartDataVisible(chartData.getIdChartData())) {
                        chartData.setVisible(isChartDataVisible(chartData.getIdChartData()));
                    }
                }

                super.paintComponent(g);
            }
        };

        // Cr�e le gestionnaire des �v�nements souris du graphe
        ChartMouseHandler mouseHandler = new ChartMouseHandler();

        // Ajoute le gestionnaire aux �v�nements souris du graphe
        out.addChartMouseListener(mouseHandler);
        out.addMouseListener(mouseHandler);

        // Parcourt des menu items du popup menu pour modifier les actions
        // Enregistrer et Imprimer
        JPopupMenu popMenu = out.getPopupMenu();

        for (int i = 0; i < popMenu.getComponentCount(); i++) {

            Component popComponent = popMenu.getComponent(i);

            if (popComponent instanceof JMenuItem) {
                JMenuItem popItem = (JMenuItem) popComponent;

                // Si menu item enregistrer sous
                if (popItem.getText().equals(ChartConstraints.MENU_ITEM_ENREGISTRER_SOUS)) {

                    // On change l'action du menu item pour enregistrer le
                    // VisionPanelChart et non uniquement le chart
                    ActionListener[] actions = popItem.getActionListeners();
                    for (int j = 0; j < actions.length; j++) {
                        popItem.removeActionListener(actions[j]);
                    }

                    popItem.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            goSaveAs();
                        }
                    });
                }
                // Si menu item imprimer
                else if (popItem.getText().equals(ChartConstraints.MENU_ITEM_IMPRIMER)) {

                    // On change l'action du menu item pour imprimer le
                    // VisionPanelChart et non uniquement le chart
                    ActionListener[] actions = popItem.getActionListeners();
                    for (int j = 0; j < actions.length; j++) {
                        popItem.removeActionListener(actions[j]);
                    }

                    popItem.addActionListener(new ActionListener() {

                        public void actionPerformed(ActionEvent e) {
                            try {
                                goPrint();
                            } catch (Exception exc) {
                                log.error("Error while printing chart", exc); //$NON-NLS-1$
                            }
                        }
                    });
                }
            }
        }

        return out;
    }



    /**
     * M�thode � appeler lorsque les donn�es du chart ont chang� afin de mettre
     * � jour la l�gende
     */
    private void fireChartDataChanged() {
        // On met � jour les items de la l�gende
        setLegendItems(getLegendItems());
        // On raffra�chit l'affichage du chart et de sa l�gende
        repaintPanelChart();
        repaintPanelLegende();
    }



    private void goPrint() {
        if (chartComponent == null) {
            return;
        }
        PrintUIWindow hardcopy = new PrintUIWindow(chartComponent);
        hardcopy.printComponent();
    }



    private void goSaveAs() {
        if (panelChart == null) {
            return;
        }

        final String[] saveExtension = { "png", "Format PNG (Portable Network Graphics) (*.pnj)" }; //$NON-NLS-1$ //$NON-NLS-2$

        // Cr�ation du filtre
        final FileFilter filterSaveChooser = new FileFilter() {

            public boolean accept(File file) {
                if (file.isDirectory()) {
                    return true;
                }
                String nomFichier = file.getName().toLowerCase();
                return nomFichier.endsWith(saveExtension[0]);
            }



            public String getDescription() {
                return saveExtension[1];
            }
        };

        // Cr�ation du file chooser
        JFileChooser saveChooser = new JFileChooser() {

            public void approveSelection()
            {
                // Si le fichier ne respecte pas le filtre, on lui ajoute
                // l'extension
                if (!filterSaveChooser.accept(getSelectedFile()))
                    setSelectedFile(new File(getSelectedFile().getAbsoluteFile() + "." + saveExtension[0])); //$NON-NLS-1$

                // Si le fichier existe, on demande confirmation
                if (getSelectedFile().exists())
                {
                    int answer = JOptionPane.showConfirmDialog(
                        this, getSelectedFile() + fmt.format("AbstractChart.RS_CONFIRM_OVERWRITE"), fmt.format("AbstractChart.RS_SAVE_TITLE"), //$NON-NLS-1$ //$NON-NLS-2$
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE
                        );
                    if (answer != JOptionPane.OK_OPTION)
                    {
                        return;
                    }
                }
                super.approveSelection();
            }
        };

        saveChooser.setFileFilter(filterSaveChooser);
        int returnVal = saveChooser.showSaveDialog(panelChart);

        // Si confirmation, on exporte sous format image
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            goOutputImage(saveExtension[0], saveChooser.getSelectedFile());
        }
    }



    private void goOutputImage(String extension, File fileOutput) {
        if (chartComponent == null) {
            return;
        }

        BufferedImage tamponSauvegarde = new BufferedImage(chartComponent.getSize().width, chartComponent.getSize().height, BufferedImage.TYPE_3BYTE_BGR);
        Graphics g = tamponSauvegarde.getGraphics();
        chartComponent.paint(g);
        try {
            FileImageOutputStream out = new FileImageOutputStream(fileOutput);
            ImageIO.write(tamponSauvegarde, extension.toUpperCase(), out);
            out.close();
        } catch (Exception e) {
            log.error("Error while converting chart to image", e); //$NON-NLS-1$
        }
    }



    private PanelLegende createPanelLegende() {
        PanelLegende out = new PanelLegende();
        out.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                // Si la l�gende n'est pas d�j� �tendue
                if (!legendExtended) {
                    // On �tend la l�gende
                    extendLegend();
                }
            }



            @Override
            public void mouseExited(MouseEvent e) {
                // Si la l�gende est �tendue
                if (legendExtended) {
                    // On compresse la l�gende
                    compressLegend();
                }
            }
        });

        return out;
    }



    private JPanel createPanelLegendeCompress() {
        JPanel out = new JPanel();
        JLabel lblLegend = new JLabel(fmt.format("AbstractChart.RS_LEGEND_TITLE")); //$NON-NLS-1$
        if (legendFont != null)
            lblLegend.setFont(legendFont);
        lblLegend.setForeground(Color.BLACK);
        out.add(lblLegend);
        out.setBorder(new LineBorder(Color.BLACK));
        out.setBackground(Color.WHITE);
        out.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                // Si la l�gende n'est pas d�j� �tendue
                if (!legendExtended) {
                    // On �tend la l�gende
                    extendLegend();
                }
            }
        });

        return out;
    }



    private JLayeredPane getRootLayerPane() {
        if (chartComponent == null) {
            return null;
        }

        JRootPane rootPane = chartComponent.getRootPane();
        if (rootPane == null) {
            log.error("getRootLayerPane(): " + chartComponent + " has no RootPaneContainer ancestor !"); //$NON-NLS-1$ //$NON-NLS-2$
            return null;
        }
        JLayeredPane layerPane = rootPane.getLayeredPane();
        return layerPane;
    }



    private void extendLegend() {
        if (chartComponent == null || panelLegende == null || panelLegendeCompress == null) {
            return;
        }

        JLayeredPane layerPane = getRootLayerPane();
        if (layerPane != null) {
            legendExtended = true;
            layerPane.add(panelLegende, JLayeredPane.PALETTE_LAYER);

            // Calcul de la largeur max et de la hauteur max
            int width = INSET_LEGENDE_PIXEL;
            int height = INSET_LEGENDE_PIXEL;

            for (int i = 0; i < legendItems.length; i++) {
                LegendItem legendItem = legendItems[i];

                // Si l'item l�gend n'est pas � afficher
                if (getChartData(legendItem.getLabel()) == null || !isChartDataVisibleInLegende(legendItem.getLabel())) {
                    continue;
                }

                int largeur = INSET_LEGENDE_PIXEL;

                height += SYMBOL_SIZE_PIXEL + INSET_LEGENDE_PIXEL;

                // ON COMPTE DANS LA LARGEUR LE CHECKBOX DE L'ITEM LEGENDE (SI
                // LA CONTRAINTE L'AUTORISE)
                if (getChartConstraints().isLegendeCheckable()) {
                    largeur += SYMBOL_SIZE_PIXEL + INSET_LEGENDE_PIXEL;
                }

                // ON COMPTE DANS LA LARGEUR LE SYMBOL
                largeur += SYMBOL_SIZE_PIXEL + INSET_LEGENDE_PIXEL;

                // PARCOURT DES LIGNES DU LABEL DE LEGENDITEM POUR CALCULER
                // LARGEUR ET HAUTEUR MAX
                String[] lignesLabel = StrUtil.split(legendItem.getLabel(), '\n');
                for (int j = 0; j < lignesLabel.length; j++) {
                    int tailleTexte = 0;

                    if (j != 0) {
                        height += SYMBOL_SIZE_PIXEL + INSET_LEGENDE_PIXEL;
                    }

                    // ON COMPTE DANS LA LARGEUR LE TEXTE DE L'ITEM LEGENDE
                    if (chartComponent.getGraphics() != null) {
                        if (legendFont != null)
                            tailleTexte = chartComponent.getGraphics().getFontMetrics(legendFont).stringWidth(lignesLabel[j]);
                        else
                            tailleTexte = chartComponent.getGraphics().getFontMetrics().stringWidth(lignesLabel[j]);
                    }

                    if (largeur + tailleTexte + INSET_LEGENDE_PIXEL > width) {
                        width = largeur + tailleTexte + INSET_LEGENDE_PIXEL;
                    }
                }

                if (largeur > width) {
                    width = largeur;
                }
            }

            Dimension dim = new Dimension(width, height);
            panelLegende.setSize(dim);
            panelLegende.setPreferredSize(dim);
            panelLegende.setMinimumSize(dim);
            panelLegende.setMaximumSize(dim);

            // On r�cup�re la localit� du panel de la l�gende compress�e par
            // rapport au layerPane
            Point p = SwingUtilities.convertPoint(panelLegendeCompress.getParent(), panelLegendeCompress.getLocation(), layerPane);
            double x = p.getX();
            double y = p.getY();
            double widthLegend = panelLegende.getWidth();
            double heightLegend = panelLegende.getHeight();
            double widthComp = panelLegendeCompress.getWidth();
            double heightComp = panelLegendeCompress.getHeight();

            // Affichage de la l�gende en fonction de la position de la l�gende
            switch (getChartConstraints().getLegendePosition()) {
                case ChartConstraints.POSITION_BOTTOM_LEFT :
                    break;

                case ChartConstraints.POSITION_BOTTOM :
                    x = x - widthLegend / 2.0f + widthComp / 2.0f;
                    break;

                case ChartConstraints.POSITION_BOTTOM_RIGHT :
                    x = x - widthLegend + widthComp;
                    break;

                case ChartConstraints.POSITION_TOP_LEFT :
                    y = y - heightLegend + heightComp;
                    break;

                case ChartConstraints.POSITION_TOP :
                    x = x - widthLegend / 2.0f + widthComp / 2.0f;
                    y = y - heightLegend + heightComp;
                    break;

                case ChartConstraints.POSITION_TOP_RIGHT :
                    x = x - widthLegend + widthComp;
                    y = y - heightLegend + heightComp;
                    break;

                case ChartConstraints.POSITION_LEFT_TOP :
                    x = x - widthLegend + widthComp;
                    break;

                case ChartConstraints.POSITION_LEFT :
                    x = x - widthLegend + widthComp;
                    y = y - heightLegend / 2.0f + heightComp / 2.0f;
                    break;

                case ChartConstraints.POSITION_LEFT_BOTTOM :
                    x = x - widthLegend + widthComp;
                    y = y - heightLegend + heightComp;
                    break;

                case ChartConstraints.POSITION_RIGHT_TOP :
                    break;

                case ChartConstraints.POSITION_RIGHT :
                    y = y - heightLegend / 2.0f + heightComp / 2.0f;
                    break;

                case ChartConstraints.POSITION_RIGHT_BOTTOM :
                    y = y - heightLegend + heightComp;
                    break;

                default :
                    break;
            }

            // On emp�che que la l�gende soit coup�e � gauche;
            x = Math.max(x, 0);
            // On emp�che que la l�gende soit coup�e en haut;
            y = Math.max(y, 0);
            // On emp�che que la l�gende soit coup�e � droite;
            x = Math.min(x, layerPane.getWidth() - panelLegende.getWidth());
            // On emp�che que la l�gende soit coup�e en bas;
            y = Math.min(y, layerPane.getHeight() - panelLegende.getHeight());

            // y = Math.max(y, layerPane.getHeight() -
            // getPanelLegende().getHeight());
            p.setLocation(x, y);

            panelLegende.setLocation(p);
        }
    }



    private void compressLegend() {
        if (panelLegende == null) {
            return;
        }

        JLayeredPane layerPane = getRootLayerPane();
        if (layerPane != null) {
            layerPane.remove(panelLegende);
            layerPane.repaint(panelLegende.getBounds());
            legendExtended = false;
        }
    }

    private class PanelLegende extends JPanel implements MouseListener {

        private LegendItem[] legendItems;
        private Shape[] checkShapeItems;



        public PanelLegende() {
            super();
            setBorder(new LineBorder(Color.BLACK));
            setBackground(Color.WHITE);

            legendItems = new LegendItem[0];
            checkShapeItems = new Shape[0];
            addMouseListener(this);
        }



        public LegendItem[] getLegendItems() {
            return legendItems;
        }



        public void setLegendItems(LegendItem[] legendItems) {
            this.legendItems = legendItems;
            checkShapeItems = new Shape[legendItems.length];
        }



        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2 = (Graphics2D) g;

            float abs = 0;
            float ord = 0;

            for (int i = 0; i < legendItems.length; i++) {

                LegendItem legendItem = legendItems[i];

                // Si l'item l�gend n'est pas � afficher
                if (!isChartDataVisibleInLegende(legendItem.getLabel())) {
                    continue;
                }

                abs = INSET_LEGENDE_PIXEL;
                ord += INSET_LEGENDE_PIXEL;

                // ON DESSINE LE CHECKBOX DE L'ITEM LEGENDE (SI LA CONTRAINTE
                // L'AUTORISE)
                if (getChartConstraints().isLegendeCheckable()) {
                    Shape legendCheck = checkShapeItems[i];
                    if (legendCheck == null)
                        legendCheck = new Rectangle2D.Float(abs, ord, SYMBOL_SIZE_PIXEL, SYMBOL_SIZE_PIXEL);
                    checkShapeItems[i] = legendCheck;

                    g2.setPaint(Color.WHITE);
                    g2.fill(legendCheck);

                    // Si l'item l�gend n'est pas checkable, on n'affiche pas le
                    // checkbox
                    if (isChartDataCheckable(legendItem.getLabel()))
                        g2.setPaint(Color.BLACK);
                    else
                        g2.setPaint(Color.WHITE);

                    g2.draw(legendCheck);

                    if (isChartDataVisible(legendItem.getLabel())) {
                        g2.drawLine((int) abs, (int) ord, (int) (abs + SYMBOL_SIZE_PIXEL), (int) (ord + SYMBOL_SIZE_PIXEL));
                        g2.drawLine((int) abs, (int) (ord + SYMBOL_SIZE_PIXEL), (int) (abs + SYMBOL_SIZE_PIXEL), (int) ord);
                    }

                    abs += SYMBOL_SIZE_PIXEL + INSET_LEGENDE_PIXEL;
                }

                // ON DESSINE LE SYMBOL DE L'ITEM LEGENDE
                Shape legendShape = getLegendShape(i, legendItem);
                AffineTransform tr = new AffineTransform();
                // Translate le symbol
                tr.translate(abs + (SYMBOL_SIZE_PIXEL / 2), ord + (SYMBOL_SIZE_PIXEL / 2));
                // Corrige un bug graphique sur les ellipses
                if (legendShape instanceof Ellipse2D) {
                    tr.scale(1.3d, 1.3d);
                }
                Shape legendSymbol = tr.createTransformedShape(legendShape);

                g2.setPaint(legendItem.getFillPaint());
                g2.fill(legendSymbol);
                g2.setPaint(legendItem.getOutlinePaint());
                g2.draw(legendSymbol);

                abs += SYMBOL_SIZE_PIXEL + INSET_LEGENDE_PIXEL;
                ord += SYMBOL_SIZE_PIXEL;

                // ON DESSINE LE TEXTE DE L'ITEM LEGENDE
                g2.setPaint(Color.BLACK);

                if (legendFont != null)
                    g2.setFont(legendFont);

                // On parcourt les lignes du label legendItem pour calculer la
                // largeur max et la hauteur max
                String[] lignesLabel = StrUtil.split(legendItem.getLabel(), '\n');
                for (int j = 0; j < lignesLabel.length; j++) {

                    if (j != 0) {
                        ord += SYMBOL_SIZE_PIXEL + INSET_LEGENDE_PIXEL;
                    }
                    g2.drawString(lignesLabel[j], abs, ord);
                }
            }
        }



        public void mouseClicked(MouseEvent e) {
            for (int i = 0; i < checkShapeItems.length; i++) {
                if (checkShapeItems[i] != null && checkShapeItems[i].contains(e.getPoint())) {
                    // Si l'item de l�gende est checkable
                    if (isChartDataCheckable(legendItems[i].getLabel())) {
                        if (!isChartDataVisible(legendItems[i].getLabel())) {
                            setChartDataVisible(legendItems[i].getLabel(), true);
                        } else {
                            setChartDataVisible(legendItems[i].getLabel(), false);
                        }
                    }
                }
            }
        }



        public void mouseEntered(MouseEvent e) {
        }



        public void mouseExited(MouseEvent e) {
        }



        public void mousePressed(MouseEvent e) {
        }



        public void mouseReleased(MouseEvent e) {
        }
    }

    /**
     * Impl�mentation du listener permettant de modifier les contraintes du
     * graphe en temps r�el
     * 
     * @author jeremy.scafi
     */
    protected class DefaultChartConstraintsListenerImpl implements ChartConstraintsListener {

        /** Fixe le titre du chart */
        public void doChangeChartTitle() {
            getChart().setTitle(getChartConstraints().getChartTitle());
        }



        /** Fixe le titre de l'axe X du chart */
        public void doChangeAxeXTitle() {
        }



        /** Fixe le titre de l'axe Y du chart */
        public void doChangeAxeYTitle() {
        }



        /** Fixe la visibilit� de l'axe X du chart */
        public void doChangeAxeXVisible() {
        }



        /** Fixe la visibilit� de l'axe Y du chart */
        public void doChangeAxeYVisible() {
        }



        /**
         * Indique si le chart est � l'horizontal (si ce n'est pas le cas, il
         * sera � la vertical)
         */
        public void doChangeHorizontal() {

        }



        /** Indique le min et le max du range de l'axe Y */
        public void doChangeAxeYRange() {
            initChart();
        }



        /** Indique si le range de l'axe Y se pack automatiquement */
        public void doChangeAxeYRangeAutoPackable() {
            initChart();
        }



        /** D�finit la marge du range de l'axe Y apr�s l'auto pack */
        public void doChangeAxeYRangeAutoPackMarge() {
            initChart();
        }



        /** Indique le min et le max du range de l'axe X */
        public void doChangeAxeXRange() {
            initChart();
        }



        /** Indique si le range de l'axe X se pack automatiquement */
        public void doChangeAxeXRangeAutoPackable() {
            initChart();
        }



        /** D�finit la marge du range de l'axe X apr�s l'auto pack */
        public void doChangeAxeXRangeAutoPackMarge() {
            initChart();
        }



        /** Indique si le zoom par clic est possible */
        public void doChangeZoomWithClickEnabled() {
            refreshMouseZoomable();
        }



        /** Fixe la position de la barre de boutons */
        public void doChangeButtonsPanelPosition() {
        }



        /** Fixe la position de la l�gende */
        public void doChangeLegendePosition() {
            refreshLegendPosition();
        }



        /** Indique si les items de la l�gende sont cochables */
        public void doChangeLegendeCheckable() {
            refreshLegendItems();
        }



        /**
         * Indique si la l�gende est sous une forme compress�e pouvant s'�tendre
         * si souris dessus
         */
        public void doChangeLegendeExtendable() {
            doChangeLegendePosition();
        }



        /** Fixe la visibilit� d'un menu item */
        public void doChangeMenuItemVisibility() {
            refreshVisibilityMenuItems();
        }



        /** Fixe le pas entre chaque valeur de l'axe des abscisses */
        public void doChangeAxeXPas() {
        }



        /** Fixe le pas entre chaque valeur de l'axe des ordonn�es */
        public void doChangeAxeYPas() {
        }



        /** Fixe les couleurs des donn�es du chart */
        public void doChangeColorsChartData() {
            fireChartDataChanged();
        }



        /** Fixe les couleurs des donn�es du chart */
        public void doChangeShapesChartData() {
            fireChartDataChanged();
        }



        /** Fixe la couleur de s�lection */
        public void doChangeSelectionColor() {
        }



        /** Rend visible ou non les bordures du chart */
        public void doChangeBorderVisibility() {
            if (getChartConstraints().isBorderVisible()) {
                chartComponent.setBorder(defaultBorder);
            } else {
                chartComponent.setBorder(null);
            }
        }



        /** Fixe la visibilit� des �tiquettes */
        public void doChangeLabelsVisible() {
        }



        /**
         * Fixe l'angle de d�part des donn�es du chart (si le chart en poss�de
         * un)
         */
        public void doChangeStartAngle() {
        }



        /** Fixe le g�n�rateur de tooltip */
        public void doChangeToolTipGenerator() {
        }
    }

    /**
     * Impl�mentation des listeners souris du graphe
     * 
     * @author JayJay
     * 
     */
    private class ChartMouseHandler implements ChartMouseListener, MouseListener {

        private MouseChartPosition previousDataPosition;



        public void chartMouseMoved(ChartMouseEvent e) {
            if (!isSelectionEnabled()) {
                return;
            }

            ChartSelection chartSelection = getChartSelection();

            // R�cup�ration de la position de la souris dans le graphe
            MouseChartPosition chartPosition = new MouseChartPosition(chartSelection, e);

            // Si la position correspond � une position d'une donn�e, on stocke
            // la position
            if (chartPosition.isDataPosition()) {
                previousDataPosition = chartPosition;
            }

            if (chartConstraints.getToolTipGenerator() != null && chartPosition.isDataPosition()) {
                String toolTip = chartConstraints.getToolTipGenerator().generateToolTip(getIdChartData(chartPosition.indSeries), chartPosition.indData);
                if (!StrUtil.isEmpty(toolTip)) {
                    UIShowAbsoluteCtxInfoEvt evt = new UIShowAbsoluteCtxInfoEvt(chartComponent, toolTip, e.getTrigger().getPoint(), UIShowCtxInfoEvt.BOTTOM_RIGHT);
                    evt.setLifeTime(CTXINFO_LIFE_TIME);
                    EDT.postEvent(evt);
                }
            }
        }



        public void chartMouseClicked(ChartMouseEvent e) {
        }



        public void mouseClicked(MouseEvent e) {
            if (!isSelectionEnabled()) {
                return;
            }

            ChartSelection chartSelection = getChartSelection();

            // R�cup�ration de la position de la souris dans le graphe
            MouseChartPosition chartPosition = new MouseChartPosition(e);

            if (chartPosition.equals(previousDataPosition)) {
                chartPosition = previousDataPosition;
            }

            if (!chartPosition.isDataPosition()) {
                // Efface la s�lection si aucune donn�e n'est point�e par la
                // souris
                chartSelection.clearSelection();
            } else if (e.getClickCount() == 1 && !e.isPopupTrigger() && chartPosition.indSeries == chartSelection.getSelectedSeries() && chartPosition.indData == chartSelection.getSelectedData()) {
                // Efface la s�lection si la donn�e point�e par la souris est
                // d�j� s�lectionn�e
                chartSelection.clearSelection();
            } else {
                // Sinon, r�alise la s�lection de la donn�e
                chartSelection.setSelectedSeries(chartPosition.indSeries);
                chartSelection.setSelectedData(chartPosition.indData);
            }

            // On ne r�alise des actions que si un �l�ment est s�lectionn�
            if (chartSelection.getSelectedSeries() != null && chartSelection.getSelectedData() != null) {
                // Si premier clic sur une ligne
                if (e.getClickCount() == 1) {
                    UIActionUtil.executeActions(actions, AbstractChart.this, UIAction.TRIGGER_CLICK);
                }

                // Si double clic sur une ligne
                if (e.getClickCount() == 2) {
                    UIActionUtil.executeActions(actions, AbstractChart.this, UIAction.TRIGGER_DBCLICK);
                }
            }

            // Indique au graphe que des changements ont eu lieu
            getChart().fireChartChanged();
        }



        public void mousePressed(MouseEvent e) {
        }



        public void mouseReleased(MouseEvent e) {
            if (!isSelectionEnabled()) {
                return;
            }

            ChartSelection chartSelection = getChartSelection();

            // R�cup�ration de la position de la souris dans le graphe
            MouseChartPosition chartPosition = new MouseChartPosition(e);

            if (chartPosition.equals(previousDataPosition)) {
                chartPosition = previousDataPosition;
            }

            // Si clic pour afficher un popup menu
            if (e.isPopupTrigger()) {
                // On consid�re que le clic droit est un clic de souris pour
                // s�lectionner/d�s�lectionner les donn�es du graphe
                mouseClicked(e);

                // On n'affiche le popup que si un �l�ment est s�lectionn�
                if (chartSelection.getSelectedSeries() != null && chartSelection.getSelectedData() != null) {
                    // Parcourt des actions de trigger CONTEXT
                    JPopupMenu popup = new JPopupMenu();
                    popup.setVisible(false);
                    popup.removeAll();
                    if (actionsContext != null) {
                        UIActionUtil.updatePopupMenuFromActions(actions, actionsContext, popup);
                    } else {
                        UIActionUtil.updatePopupMenuFromActions(actions, new ActionsContext(AbstractChart.this), popup);
                    }
                    if (popup.getComponentCount() > 0) {
                        popup.show(e.getComponent(), e.getX(), e.getY());
                    }
                }
            }
        }



        public void mouseEntered(MouseEvent e) {
        }



        public void mouseExited(MouseEvent e) {
        }
    }

    /**
     * Classe repr�sentant la position de la souris dans le graphe
     * 
     * @author JayJay
     * 
     */
    private static class MouseChartPosition implements Comparable<MouseChartPosition> {

        int abs;
        int ord;
        Integer indSeries;
        Integer indData;



        /**
         * Constructeur avec MouseEvent
         * 
         * @param e
         *            MouseEvent
         */
        private MouseChartPosition(MouseEvent e) {
            abs = e.getX();
            ord = e.getY();
        }



        /**
         * Constructeur avec ChartSelection et ChartMouseEvent
         * 
         * @param chartSelection
         *            ChartSelection
         * @param e
         *            ChartMouseEvent
         */
        private MouseChartPosition(ChartSelection chartSelection, ChartMouseEvent e) {
            abs = e.getTrigger().getX();
            ord = e.getTrigger().getY();
            if (chartSelection.isSelectionEnabled(e.getEntity())) {
                if (e.getEntity() instanceof PieSectionEntity) {
                    PieSectionEntity item = (PieSectionEntity) e.getEntity();
                    indSeries = item.getSectionIndex();
                    indData = item.getPieIndex();
                } else if (e.getEntity() instanceof CategoryItemEntity) {
                    CategoryItemEntity item = (CategoryItemEntity) e.getEntity();
                    indSeries = item.getSeries();
                    indData = item.getCategoryIndex();
                } else if (e.getEntity() instanceof XYItemEntity) {
                    XYItemEntity item = (XYItemEntity) e.getEntity();
                    indSeries = item.getSeriesIndex();
                    indData = item.getItem();
                }
            }
        }



        private boolean isDataPosition() {
            return indSeries != null && indData != null;
        }



        public int compareTo(MouseChartPosition position) {
            if (abs < position.abs - PIXEL_TOLERANCE_CHART_MOUSE_POSITION) {
                return -1;
            }
            if (abs > position.abs + PIXEL_TOLERANCE_CHART_MOUSE_POSITION) {
                return 1;
            }
            if (ord < position.ord - PIXEL_TOLERANCE_CHART_MOUSE_POSITION) {
                return -1;
            }
            if (ord > position.ord + PIXEL_TOLERANCE_CHART_MOUSE_POSITION) {
                return 1;
            }
            return 0;
        }



        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MouseChartPosition && compareTo((MouseChartPosition) obj) == 0) {
                return true;
            }
            return super.equals(obj);
        }
    }
}
