package com.dolphland.client.util.image;

import info.clearthought.layout.TableLayout;

import java.awt.BorderLayout;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JPanel;

import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;


public class ImagePanel extends JPanel{

    public final static int DEFAULT_GAP = 10;
    
    private Map<ImagePosition, FwkPanel> panelsByPosition;
    private int leftGap;
    private int topGap;
    private int rightGap;
    private int bottomGap;
    private double imageWith;
    private double imageHeight;
    
    public ImagePanel(){
        this(DEFAULT_GAP, DEFAULT_GAP, DEFAULT_GAP, DEFAULT_GAP, TableLayout.PREFERRED, TableLayout.PREFERRED);
    }
    
    public ImagePanel(int leftGap, int topGap, int rightGap, int bottomGap){
        this(leftGap, topGap, rightGap, bottomGap, TableLayout.PREFERRED, TableLayout.PREFERRED);
    }
    
    public ImagePanel(double imageWidth, double imageHeight){
        this(DEFAULT_GAP, DEFAULT_GAP, DEFAULT_GAP, DEFAULT_GAP, imageWidth, imageHeight);
    }
    
    public ImagePanel(int leftGap, int topGap, int rightGap, int bottomGap, double imageWidth, double imageHeight){
        super();
        this.leftGap = leftGap;
        this.topGap = topGap;
        this.rightGap = rightGap;
        this.bottomGap = bottomGap;
        this.imageWith = imageWidth;
        this.imageHeight = imageHeight;
        setOpaque(false);
        panelsByPosition = new TreeMap<ImagePosition, FwkPanel>();
        initialize();
    }
    
    private void initialize(){
        int row = 0;
        int column = 0;
        double topInsets = 0;
        double bottomInsets = 0;
        double leftInsets = 0;
        double rightInsets = 0;

        TableLayoutBuilder layoutBuider = new TableLayoutBuilder();
        
        setLayout(layoutBuider.get());
        
        for(ImagePosition position : ImagePosition.values()){
            FwkPanel imagePanel = new FwkPanel();
            imagePanel.setOpaque(false);
            switch (position) {
                case TOP :
                    row = 0;
                    column = 1;
                    topInsets = topGap;
                    bottomInsets = 1f / 2f;
                    rightInsets = 1f / 2f;
                    break;

                case TOP_RIGHT :
                    row = 0;
                    column = 2;
                    topInsets = topGap;
                    rightInsets = rightGap;
                    bottomInsets = 1f / 2f;
                    break;

                case RIGHT :
                    row = 1;
                    column = 2;
                    rightInsets = rightGap;
                    bottomInsets = 1f / 2f;
                    break;

                case BOTTOM_RIGHT :
                    row = 2;
                    column = 2;
                    rightInsets = rightGap;
                    bottomInsets = bottomGap;
                    break;

                case BOTTOM :
                    row = 2;
                    column = 1;
                    bottomInsets = bottomGap;
                    rightInsets = 1f / 2f;
                    break;

                case BOTTOM_LEFT :
                    row = 2;
                    column = 0;
                    leftInsets = leftGap;
                    bottomInsets = bottomGap;
                    rightInsets = 1f / 2f;
                    break;

                case LEFT :
                    row = 1;
                    column = 0;
                    leftInsets = leftGap;
                    bottomInsets = 1f / 2f;
                    rightInsets = 1f / 2f;
                    break;

                case TOP_LEFT :
                    row = 0;
                    column = 0;
                    topInsets = topGap;
                    leftInsets = leftGap;
                    bottomInsets = 1f / 2f;
                    rightInsets = 1f / 2f;
                    break;

                default :
                    break;
            }

            add(imagePanel, layoutBuider.addComponent(new TableLayoutConstraintsBuilder()
                .row(row)
                .column(column)
                .topInsets(topInsets)
                .height(imageHeight)
                .bottomInsets(bottomInsets)
                .leftInsets(leftInsets)
                .width(imageWith)
                .rightInsets(rightInsets)
                .get()));
            
            panelsByPosition.put(position, imagePanel);
        }
    }
    
    

    public void setImage(ImagePosition position, ImageLabel imageLabel){
        FwkPanel imagePanel = panelsByPosition.get(position); 
        imagePanel.removeAll();
        if(imageLabel != null){
            imagePanel.setLayout(new BorderLayout());
            imagePanel.add(imageLabel, BorderLayout.CENTER);
        }
        validate();
    }
}
