package com.dolphland.client.util.table.demo;

import com.dolphland.client.util.table.DefaultProductTableContentManager;
import com.dolphland.client.util.table.ProductAdapter;
import com.dolphland.client.util.table.ProductProperty;


public class DemoProductTableContentManager extends DefaultProductTableContentManager{

    
    @Override
    public boolean isProductTableRowSelectionAllowed() {
        return true;
    }
    
    @Override
    public boolean isProductTableSelectionEnabled() {
        return false;
    }
    
    @Override
    public boolean isCellEditable(ProductProperty pp, ProductAdapter pa) {
        if(pp.getType() == ProductProperty.ACTION_TYPE){
            return true;
        }
        return super.isCellEditable(pp, pa);
    }
}
