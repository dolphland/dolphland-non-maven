package com.dolphland.client.util.flip;

/**
 * Animation's type used to flip
 * 
 * @author p-jeremy.scafi
 * 
 */
public enum FlipAnimationType {
    /**
     * Animation's type used to move horizontally all applications.
     * If a selected application is specified, all applications are move to the
     * left in order to have the selected application in the center.
     * If an attach point is also specified, the selected application is
     * centered to the attach point's horizontal coordinate.
     */
    MOVE_HORIZONTALLY_ALL_APPLICATIONS,

    /**
     * Animation's type used to move vertically the selected application.
     * If an attach point is specified, the selected application is centered to
     * the attach point's vertical coordinate.
     */
    MOVE_VERTICALLY_SELECTED_APPLICATION,

    /** Animation's type used to minimize the selected application. */
    MINIMIZE_SELECTED_APPLICATION,

    /** Animation's type used to maximize the selected application. */
    MAXIMIZE_SELECTED_APPLICATION,

    /**
     * Animation's type used to pop-up the selected application.
     * If an attach point is specified, the selected application pup-up from the
     * attach point and the previous application is pushed to the opposite
     */
    POP_UP_SELECTED_APPLICATION,

    /**
     * Animation's type used to move selected application out of screen (to the
     * top).
     */
    MOVE_OUT_OF_SCREEN_SELECTED_APPLICATION,
}
