package com.dolphland.client.util.application;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;

public abstract class WorkPane extends JPanel implements Messenger {

    private MasterFrame frame = null;

    private View view = null;

    private JPanel overlappingPane;

    private OverlappingComponentListener overlappingComponentListener = new OverlappingComponentListener();



    public WorkPane(MasterFrame frame) {
        super();
        if (frame == null) {
            throw new NullPointerException("MasterFrame is null !"); //$NON-NLS-1$
        }
        this.frame = frame;
        this.overlappingComponentListener = new OverlappingComponentListener();
        setLayout(new BorderLayout());
    }



    /** Ferme le WorkPane (� d�finir) */
    public abstract void close();



    /**
     * Fixe la vue du WorkPane
     */
    public void setView(View view) {
        this.view = view;
        removeAll();
        if (view != null) {
            add(view, BorderLayout.CENTER);
        }
        validate();
        repaint();
    }



    /**
     * Fournit la vue du WorkPane
     */
    public View getView() {
        return view;
    }



    /** Fournit la MasterFrame rattach�e */
    public MasterFrame getMasterFrame() {
        return frame;
    }



    /**
     * Affiche le panel sp�cifi� au dessus du workpane. Le panel fourni sera
     * redimensionn� et repositionn� pour �tre � la m�me taille que celle du
     * workpane et affich� juste au dessus aux m�me coordonn�es.<br>
     * S'il existe d�j� un overlappingPane, il est remplac� par le nouveau.<br>
     * Si null est sp�cifi�, le overlappingPane existant est supprim� (
     * <code>clearOverlappingPane</code>)
     * 
     * @param pane
     *            Le panel � afficher au dessus du workpane, ou null pour
     *            supprimer le overlappingPane existant
     */
    public void setOverlappingPane(JPanel pane) {
        if (pane == null) {
            clearOverlappingPane();
            return;
        }
        if (overlappingPane != null && overlappingPane != pane) {
            clearOverlappingPane();
        }
        JRootPane rootPane = getRootPane();
        if (rootPane == null) {
            return;
        }
        this.overlappingPane = pane;
        JLayeredPane jlp = rootPane.getLayeredPane();
        jlp.add(overlappingPane, JLayeredPane.PALETTE_LAYER);
        relocateOverlappingPane(overlappingPane);
        this.addComponentListener(overlappingComponentListener);
        jlp.validate();
    }



    private void relocateOverlappingPane(JPanel pane) {
        if (pane == null) {
            return;
        }
        JRootPane rootPane = getRootPane();
        if (rootPane == null) {
            return;
        }
        JLayeredPane jlp = rootPane.getLayeredPane();
        Dimension wpSize = getSize();
        pane.setSize(wpSize);
        pane.setPreferredSize(wpSize);
        pane.setMaximumSize(wpSize);
        Point paneLoc = SwingUtilities.convertPoint(this, getLocation(), jlp);
        pane.setLocation(paneLoc);
    }



    /**
     * Supprime le panel au dessus du workpane. S'il n'y a pas de panel au
     * dessus du workpane, ne fait rien.
     */
    public void clearOverlappingPane() {
        if (overlappingPane == null) {
            return;
        }
        JRootPane rootPane = getRootPane();
        if (rootPane == null) {
            return;
        }
        JLayeredPane jlp = rootPane.getLayeredPane();
        jlp.remove(overlappingPane);
        this.removeComponentListener(overlappingComponentListener);
        jlp.repaint();
    }



    /**
     * M�thodes impl�ment�es pour le Shower
     */
    public void showInfoDialog(String msg) {
        getMasterFrame().showInfoDialog(msg);
    }



    public void showWarningDialog(String msg) {
        getMasterFrame().showWarningDialog(msg);
    }



    public void showErrorDialog(String msg) {
        getMasterFrame().showErrorDialog(msg);
    }



    public boolean showConfirmDialog(String msg) {
        return getMasterFrame().showConfirmDialog(msg);
    }



    public String showInputDialog(String msg) {
        return getMasterFrame().showInputDialog(msg);
    }



    public String showInputDialog(String msg, String initialInputValue) {
        return getMasterFrame().showInputDialog(msg, initialInputValue);
    }



    public Object showInputDialog(String msg, Object[] elements) {
        return getMasterFrame().showInputDialog(msg, elements);
    }



    public String showPasswordDialog(String msg) {
        return getMasterFrame().showPasswordDialog(msg);
    }



    public String showPasswordDialog(String msg, String initialPasswordValue) {
        return getMasterFrame().showPasswordDialog(msg, initialPasswordValue);
    }



    public void clearMessages() {
        getMasterFrame().clearMessages();
    }



    public void showInfoMessage(String message) {
        getMasterFrame().showInfoMessage(message);
    }



    public void showWarningMessage(String message) {
        getMasterFrame().showWarningMessage(message);
    }



    public void showErrorMessage(String message) {
        getMasterFrame().showErrorMessage(message);
    }

    private class OverlappingComponentListener extends ComponentAdapter {

        @Override
        public void componentResized(ComponentEvent e) {
            relocateOverlappingPane(overlappingPane);
        }



        @Override
        public void componentMoved(ComponentEvent e) {
            relocateOverlappingPane(overlappingPane);
        }



        @Override
        public void componentHidden(ComponentEvent e) {
            if (overlappingPane != null) {
                overlappingPane.setVisible(false);
            }
        }



        @Override
        public void componentShown(ComponentEvent e) {
            if (overlappingPane != null) {
                overlappingPane.setVisible(true);
            }
        }
    }
}
