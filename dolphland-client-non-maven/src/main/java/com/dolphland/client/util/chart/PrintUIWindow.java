package com.dolphland.client.util.chart;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JComponent;

import org.apache.log4j.Logger;

/**
 * Classe permettant d'imprimer un composant swing sur une imprimante du syst�me
 * 
 * Exemple d'utilisation :
 * 
 * PrintUIWindow hardcopy = new PrintUIWindow(state.getResultatsTube());
 * hardcopy.printComponent();
 * 
 * @author jeremy.scafi
 */
public class PrintUIWindow implements Printable {

    private final static Logger log = Logger.getLogger(PrintUIWindow.class);

    private JComponent componentToPrint;
    private int m_Orientation = PageFormat.PORTRAIT;



    /**
     * Constructeur avec en param�tre le composant swing � imprimer
     */
    public PrintUIWindow(JComponent c) {
        componentToPrint = c;
    }



    /**
     * Imprime un graphics sur une page
     */
    public int print(Graphics g, PageFormat pf, int page) throws PrinterException {

        if (page > 0) { /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }

        // ----- Always scale to fit
        Rectangle componentBounds = componentToPrint.getBounds(null);
        double scaleX = pf.getImageableWidth() / componentBounds.width;
        double scaleY = pf.getImageableHeight() / componentBounds.height;
        if (scaleX < 1 || scaleY < 1) {
            if (scaleX < scaleY) {
                scaleY = scaleX;
            } else {
                scaleX = scaleY;
            }
        }

        /*
         * User (0,0) is typically outside the imageable area, so we must
         * translate by the X and Y values in the PageFormat to avoid clipping
         */
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pf.getImageableX(), pf.getImageableY());
        g2d.scale(scaleX, scaleY);

        /* Now print the window and its visible contents */
        componentToPrint.printAll(g);

        /* tell the caller that this page is part of the printed document */
        return PAGE_EXISTS;
    }



    /** Imprime le composant pass� en param�tre du constructeur */
    public void printComponent() {
        PrinterJob job = PrinterJob.getPrinterJob();
        PageFormat pf = job.defaultPage();
        pf.setOrientation(m_Orientation);
        job.setPrintable(this, pf);
        boolean ok = job.printDialog();
        if (ok) {
            try {
                job.print();
            } catch (PrinterException ex) {
                log.error("Error while printing component", ex); //$NON-NLS-1$
            }
        }
    }



    /** Fixe l'orientation de l'impression */
    public void setOrientation(int orientation) throws IllegalArgumentException {
        if ((orientation != PageFormat.LANDSCAPE) && (orientation != PageFormat.PORTRAIT) && (orientation != PageFormat.REVERSE_LANDSCAPE)) {
            throw new IllegalArgumentException("Invalid orientation for printing"); //$NON-NLS-1$
        }
        m_Orientation = orientation;
    }
}
