package com.dolphland.client.util.dialogs;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.dolphland.client.util.i18n.MessageFormater;

public class ErrorReporter extends FwkDialog {

    private static final MessageFormater FMT = MessageFormater.getFormater(ErrorReporter.class);
    private JPanel jContentPane = null;
    private JPanel rootPanel = null;
    private JLabel jlMessage = null;
    private JTextArea jtaRapport = null;
    private JButton jbOk = null;



    /**
     * This is the default constructor
     */
    public ErrorReporter() {
        super();
        initialize();
    }



    public ErrorReporter(Frame parent) {
        super(parent);
        initialize();
    }



    public ErrorReporter(Dialog parent) {
        super(parent);
        initialize();
    }



    /**
     * This method initializes this
     * 
     * @return void
     */
    private void initialize() {
        this.setSize(654, 271);
        this.setModal(true);
        this.setContentPane(getJContentPane());
        this.addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });
    }



    /**
     * This method initializes jContentPane
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getJContentPane() {
        if (jContentPane == null) {
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new java.awt.Insets(10, 10, 5, 10);
            gridBagConstraints.gridy = 0;
            jContentPane = new JPanel();
            jContentPane.setLayout(new GridBagLayout());
            jContentPane.add(getRootPanel(), gridBagConstraints);
        }
        return jContentPane;
    }



    /**
     * This method initializes rootPanel
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getRootPanel() {
        if (rootPanel == null) {
            GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
            gridBagConstraints3.gridx = 0;
            gridBagConstraints3.insets = new java.awt.Insets(5, 0, 5, 0);
            gridBagConstraints3.gridy = 2;
            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.fill = java.awt.GridBagConstraints.BOTH;
            gridBagConstraints2.gridy = 1;
            gridBagConstraints2.weightx = 1.0;
            gridBagConstraints2.weighty = 1.0;
            gridBagConstraints2.gridx = 0;
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.gridx = 0;
            gridBagConstraints1.fill = java.awt.GridBagConstraints.HORIZONTAL;
            gridBagConstraints1.weightx = 0.5;
            gridBagConstraints1.insets = new java.awt.Insets(0, 0, 5, 0);
            gridBagConstraints1.anchor = java.awt.GridBagConstraints.NORTH;
            gridBagConstraints1.gridy = 0;
            jlMessage = new JLabel();
            jlMessage.setText(FMT.format("ErrorReporter.RS_UNE_ERREUR_SEST_PRODUITE"));
            rootPanel = new JPanel();
            rootPanel.setLayout(new GridBagLayout());
            rootPanel.add(jlMessage, gridBagConstraints1);
            rootPanel.add(new JScrollPane(getJtaRapport()), gridBagConstraints2);
            rootPanel.add(getJbOk(), gridBagConstraints3);
        }
        return rootPanel;
    }



    /**
     * This method initializes jtaRapport
     * 
     * @return javax.swing.JTextArea
     */
    private JTextArea getJtaRapport() {
        if (jtaRapport == null) {
            jtaRapport = new JTextArea();
        }
        return jtaRapport;
    }



    /**
     * This method initializes jbOk
     * 
     * @return javax.swing.JButton
     */
    private JButton getJbOk() {
        if (jbOk == null) {
            jbOk = new JButton();
            jbOk.setText(FMT.format("ErrorReporter.RS_OK"));
            jbOk.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
        }
        return jbOk;
    }



    public void setMessage(String message) {
        jlMessage.setText(message);
    }



    public void setRapport(String rapport) {
        getJtaRapport().setText(rapport);
    }



    public void reportThrowable(Throwable cause) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        cause.printStackTrace(pw);
        pw.flush();
        setRapport(sw.toString());
    }

} // @jve:decl-index=0:visual-constraint="10,10"
