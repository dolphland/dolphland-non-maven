package com.dolphland.client.util.chart;

import java.awt.Color;

/**
 * Classe permettant de cr�er un ChartConstraints
 * 
 * N.B. : le get() permettant d'obtenir le ChartConstraints ainsi cr�� doit se
 * trouver en fin d'appel
 * 
 * Exemple simple d'utilisation du Builder :
 * 
 * ChartConstraintsBuilder.create()
 * .chartTitle("Titre")
 * .get();
 * 
 * @author jeremy.scafi
 */
public class ChartConstraintsBuilder {

    private ChartConstraints chartConstraints;



    private ChartConstraintsBuilder() {
        chartConstraints = new ChartConstraints();
    }



    public static ChartConstraintsBuilder create() {
        return new ChartConstraintsBuilder();
    }



    /** Fixe le titre du chart */
    public ChartConstraintsBuilder chartTitle(String chartTitle) {
        chartConstraints.setChartTitle(chartTitle);
        return this;
    }



    /** Fixe le titre de l'axe X du chart */
    public ChartConstraintsBuilder axeXTitle(String axeXTitle) {
        chartConstraints.setAxeXTitle(axeXTitle);
        return this;
    }



    /** Fixe le titre de l'axe Y du chart */
    public ChartConstraintsBuilder axeYTitle(String axeYTitle) {
        chartConstraints.setAxeYTitle(axeYTitle);
        return this;
    }



    /** Fixe la visibilit� de l'axe X du chart */
    public ChartConstraintsBuilder axeXVisible(boolean axeXVisible) {
        chartConstraints.setAxeXVisible(axeXVisible);
        return this;
    }



    /** Fixe la visibilit� de l'axe Y du chart */
    public ChartConstraintsBuilder axeYVisible(boolean axeYVisible) {
        chartConstraints.setAxeYVisible(axeYVisible);
        return this;
    }



    /**
     * Indique si le chart est � l'horizontal (si ce n'est pas le cas, il sera �
     * la vertical)
     */
    public ChartConstraintsBuilder horizontal(boolean horizontal) {
        chartConstraints.setHorizontal(horizontal);
        return this;
    }



    /** Indique le min et le max du range de l'axe Y */
    public ChartConstraintsBuilder axeYRange(Number axeYRangeMin, Number axeYRangeMax) {
        chartConstraints.setAxeYRange(axeYRangeMin, axeYRangeMax);
        return this;
    }



    /** Indique le range de l'axe Y se pack automatiquement */
    public ChartConstraintsBuilder axeYRangeAutoPackable(boolean axeYRangeAutoPackable) {
        chartConstraints.setAxeYRangeAutoPackable(axeYRangeAutoPackable);
        return this;
    }



    /** D�finit la marge du range de l'axe Y apr�s l'auto pack */
    public ChartConstraintsBuilder axeYRangeAutoPackMarge(Number axeYRangeAutoPackMarge) {
        chartConstraints.setAxeYRangeAutoPackMarge(axeYRangeAutoPackMarge);
        return this;
    }



    /** Indique le min et le max du range de l'axe X */
    public ChartConstraintsBuilder axeXRange(Number axeXRangeMin, Number axeXRangeMax) {
        chartConstraints.setAxeXRange(axeXRangeMin, axeXRangeMax);
        return this;
    }



    /** Indique le range de l'axe X se pack automatiquement */
    public ChartConstraintsBuilder axeXRangeAutoPackable(boolean axeXRangeAutoPackable) {
        chartConstraints.setAxeXRangeAutoPackable(axeXRangeAutoPackable);
        return this;
    }



    /** D�finit la marge du range de l'axe X apr�s l'auto pack */
    public ChartConstraintsBuilder axeXRangeAutoPackMarge(Number axeXRangeAutoPackMarge) {
        chartConstraints.setAxeXRangeAutoPackMarge(axeXRangeAutoPackMarge);
        return this;
    }



    /** Fixe la visibilit� d'un menu item */
    public ChartConstraintsBuilder menuItemVisible(String menuItem, boolean menuItemVisible) {
        chartConstraints.setMenuItemVisible(menuItem, menuItemVisible);
        return this;
    }



    /** Indique si le zoom par clic est possible */
    public ChartConstraintsBuilder zoomWithClickEnabled(boolean zoomWithClickEnabled) {
        chartConstraints.setZoomWithClickEnabled(zoomWithClickEnabled);
        return this;
    }



    /** Indique si les items de la l�gende sont cochables */
    public ChartConstraintsBuilder legendeCheckable(boolean legendeCheckable) {
        chartConstraints.setLegendeCheckable(legendeCheckable);
        return this;
    }



    /**
     * Indique si la l�gende est sous une forme compress�e pouvant s'�tendre si
     * souris dessus
     */
    public ChartConstraintsBuilder legendeExtendable(boolean legendeExtendable) {
        chartConstraints.setLegendeExtendable(legendeExtendable);
        return this;
    }



    /** Fixe la position de la l�gende */
    public ChartConstraintsBuilder legendePosition(int legendePosition) {
        chartConstraints.setLegendePosition(legendePosition);
        return this;
    }



    /** Fixe la position de la barre de boutons */
    public ChartConstraintsBuilder buttonsPanelPosition(int buttonsPanelPosition) {
        chartConstraints.setButtonsPanelPosition(buttonsPanelPosition);
        return this;
    }



    /** Fixe le pas entre chaque valeur de l'axe des abscisses */
    public ChartConstraintsBuilder axeXPas(Number axeXPas) {
        chartConstraints.setAxeXPas(axeXPas);
        return this;
    }



    /** Fixe le pas entre chaque valeur de l'axe des ordonn�es */
    public ChartConstraintsBuilder axeYPas(Number axeYPas) {
        chartConstraints.setAxeYPas(axeYPas);
        return this;
    }



    /** Rend visible ou non les bordures du chart */
    public ChartConstraintsBuilder borderVisible(boolean borderVisible) {
        chartConstraints.setBorderVisible(borderVisible);
        return this;
    }



    /** Fixe la palette de couleurs des donn�es du chart */
    public ChartConstraintsBuilder colorsChartData(Color[] colorsChartData) {
        chartConstraints.setColorsChartData(colorsChartData);
        return this;
    }



    /** Fixe les formes des donn�es du chart */
    public ChartConstraintsBuilder shapesChartData(ChartShape[] shapesChartData) {
        chartConstraints.setShapesChartData(shapesChartData);
        return this;
    }



    /** Fixe la couleur de la s�lection */
    public ChartConstraintsBuilder selectionColor(Color selectionColor) {
        chartConstraints.setSelectionColor(selectionColor);
        return this;
    }



    /** Fixe la visibilit� des �tiquettes */
    public ChartConstraintsBuilder labelsVisible(boolean labelsVisible) {
        chartConstraints.setLabelsVisible(labelsVisible);
        return this;
    }



    /** Fixe l'angle de d�part des donn�es du chart (si le chart en poss�de un) */
    public ChartConstraintsBuilder startAngle(Double startAngle) {
        chartConstraints.setStartAngle(startAngle);
        return this;
    }



    /** Fixe le g�n�rateur de tooltip */
    public ChartConstraintsBuilder toolTipGenerator(ChartToolTipGenerator toolTipGenerator) {
        chartConstraints.setToolTipGenerator(toolTipGenerator);
        return this;
    }



    /** Renvoie la contrainte du graphe */
    public ChartConstraints get() {
        return chartConstraints;
    }

}
