package com.dolphland.client.util.chart;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;

/**
 * Cette classe permet de repr�senter un chart Area en utilisant l'API
 * JFreeChart avec une l�gende am�lior�e.
 * 
 * La principale am�lioration de cette l�gende est de pouvoir d�cocher certaines
 * s�ries pour ne pas les voir appara�tre sur le chart.
 * 
 * La repr�sentation du Chart se fait dans le JComponent componentChart que l'on
 * peut obtenir par getChartComponent()
 * 
 * Possibilit� d'y d�finir :
 * - des entit�s de donn�es. Une entit� de donn�e est une s�rie identifi�e par
 * un Comparable, constitu�e de valeurs (abscisse, ordonn�e)
 * - des markers horizontaux. Un marker horizontal permet de distinguer une zone
 * du chart comprise dans un intervale d'abscisses ou une valeur d'abscisses. La
 * distinction se fait par du texte et/ou par une couleur
 * - des markers verticaux. Un marker vertical permet de distinguer une zone du
 * chart comprise dans un intervale d'ordonn�es ou une valeur d'ordonn�e. La
 * distinction se fait par du texte et/ou par une couleur
 * - des valeurs fixes. S�rie de donn�es ne contenant qu'une seule valeur. On
 * utilise des valeurs fixes pour repr�senter un seuil.
 * 
 * La configuration du chart se fait � l'aide d'un ChartConstraint avec
 * possibilit� de passer par son builder (ChartConstraintBuilder)
 * 
 * Exemple concret de cr�ation d'un LineChart :
 * 
 * AreaChart chart = new AreaChart(new VisionPanel(),
 * ChartConstraintsBuilder.create()
 * .axeXTitle("Position par rapport au d�but du tube")
 * .axeYTitle("Epaisseur (mm)")
 * .axeYRangeAutoPackable(true)
 * .menuItemVisible(ChartConstraints.MENU_ITEM_PROPRIETES, false)
 * .menuItemVisible(ChartConstraints.MENU_ITEM_ECHELLE_AUTOMATIQUE, false)
 * .zoomWithClickEnabled(false)
 * .get()
 * );
 * 
 * @author jeremy.scafi
 */
public class AreaChart extends AbstractXYPlotChart {

    /**
     * Constructeur
     */
    public AreaChart() {
        super();
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AreaChart(ChartConstraints chartConstraints) {
        super(chartConstraints);
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AreaChart(JPanel componentChart) {
        super(componentChart);
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param componentChart
     *            : Composant devant repr�senter le Chart
     * @param chartConstraints
     *            : Contraintes � appliquer au Chart
     */
    public AreaChart(JPanel componentChart, ChartConstraints chartConstraints) {
        super(componentChart, chartConstraints);
    }



    /** Cr�e le chart */
    protected JFreeChart createChart() {
        boolean legend = false;
        boolean tooltips = false;
        boolean urls = false;
        PlotOrientation orientation = (getChartConstraints().isHorizontal()) ? PlotOrientation.HORIZONTAL : PlotOrientation.VERTICAL;

        JFreeChart chart = ChartFactory.createXYAreaChart(
            getChartConstraints().getChartTitle(),
            getChartConstraints().getAxeXTitle(),
            getChartConstraints().getAxeYTitle(),
            getDataSet(),
            orientation,
            legend,
            tooltips,
            urls);

        // Get plot
        XYPlot plot = chart.getXYPlot();

        // Fixe le renderer
        plot.setRenderer(0, new AreaSelectionRenderer(getDataSet()));

        return chart;
    }



    @Override
    protected ChartSelection getChartSelection() {
        return (ChartSelection) getChart().getXYPlot().getRenderer(0);
    }
}
