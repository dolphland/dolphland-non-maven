/*
 * $Log: ConfLoader.java,v $
 * Revision 1.4 2014/03/10 16:07:42 bvatan
 * - Internationalized
 * Revision 1.3 2012/12/17 12:26:29 bvatan
 * - added NON-NLS comments
 * Revision 1.2 2012/08/10 13:30:06 bvatan
 * - addedsupport for setting text contained within <property> tags into
 * CProperty instances
 * Revision 1.1 2008/11/27 07:38:34 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.io.Reader;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;

import org.apache.commons.digester.Digester;
import org.apache.commons.digester.Rule;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;

import com.dolphland.client.util.exception.AppInfrastructureException;

public class ConfLoader {

    private static final Logger log = Logger.getLogger(ConfLoader.class);

    private static final String APPLICATION_NODE = "application"; //$NON-NLS-1$

    private static final String ENVIRONEMENT_NODE = APPLICATION_NODE + "/env"; //$NON-NLS-1$

    private static final String PROPERTY_NODE = "*/property"; //$NON-NLS-1$

    private static final String CONF_NODE = "*/conf"; //$NON-NLS-1$



    public ConfLoader() {
    }



    public CApplication parse(Reader in) {
        try {
            Digester digester = new Digester();

            digester.addObjectCreate(APPLICATION_NODE, CApplication.class);
            digester.addSetProperties(APPLICATION_NODE);

            digester.addObjectCreate(ENVIRONEMENT_NODE, CEnvironement.class);
            digester.addSetProperties(ENVIRONEMENT_NODE);
            digester.addSetTop(ENVIRONEMENT_NODE, "setApplication", CApplication.class.getName()); //$NON-NLS-1$
            digester.addSetNext(ENVIRONEMENT_NODE, "addEnvironement", CEnvironement.class.getName()); //$NON-NLS-1$

            digester.addObjectCreate(PROPERTY_NODE, CProperty.class);
            digester.addSetProperties(PROPERTY_NODE);
            digester.addSetNext(PROPERTY_NODE, "addProperty", CProperty.class.getName()); //$NON-NLS-1$
            digester.addRule(PROPERTY_NODE, new AddTextRule());

            digester.addObjectCreate(CONF_NODE, CConf.class);
            digester.addSetProperties(CONF_NODE);
            digester.addSetNext(CONF_NODE, "addConf", CConf.class.getName()); //$NON-NLS-1$

            CApplication application = (CApplication) digester.parse(in);
            if (log.isDebugEnabled()) {
                log.debug("parse(): " + application); //$NON-NLS-1$
            }
            Set<String> envs = application.getEnvironementIds();
            for (String envId : envs) {
                CEnvironement env = application.getEnvironement(envId);
                if (log.isDebugEnabled()) {
                    log.debug("parse():\t " + envId + " " + env); //$NON-NLS-1$ //$NON-NLS-2$
                }
                List<CProperty> envProperties = env.getProperties();
                for (CProperty property : envProperties) {
                    if (log.isDebugEnabled()) {
                        log.debug("parse(): \t\t" + property); //$NON-NLS-1$
                    }
                }
                logConfs("\t\t", env); //$NON-NLS-1$
            }
            return application;
        } catch (Exception e) {
            throw new AppInfrastructureException("Unable to parse settings file !", e); //$NON-NLS-1$
        }
    }



    private void logConfs(String prefix, CEnvironement env) {
        List<CConf> confs = env.getConfs();
        for (CConf conf : confs) {
            logConf(prefix, conf);
        }
    }



    private void logConf(String prefix, CConf conf) {
        if (log.isDebugEnabled()) {
            log.debug("parse(): " + prefix + conf); //$NON-NLS-1$
        }
        List<CConf> subConf = conf.getConfs();
        for (CConf conf2 : subConf) {
            logConf(prefix + "\t", conf2); //$NON-NLS-1$
        }
    }

    private class AddTextRule extends Rule {

        @Override
        public void begin(String namespace, String name, Attributes attributes) throws Exception {
        }



        @Override
        public void body(String namespace, String name, String text) throws Exception {
            Object top = digester.peek();
            Method m = null;
            try {
                m = top.getClass().getMethod("addText", String.class); //$NON-NLS-1$
            } catch (Exception e) {
                m = null;
            }
            if (m != null) {
                try {
                    m.invoke(top, text);
                } catch (Exception e) {
                    log.debug("body(): " + name + ".addText(text=" + text + ") failed !", e); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                }
            }
        }



        @Override
        public void end(String namespace, String name) throws Exception {
        }



        @Override
        public void finish() throws Exception {

        }
    }
}
