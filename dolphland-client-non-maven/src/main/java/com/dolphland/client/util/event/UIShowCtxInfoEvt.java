/*
 * $Log: UIShowCtxInfoEvt.java,v $
 * Revision 1.7 2014/03/18 13:00:52 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * Revision 1.6 2009/08/06 12:55:10 bvatan
 * - Gestion de l'affichage/masque progressif des bulles d'infos
 * Revision 1.5 2008/10/09 09:06:59 jscafi
 * - Prise en compte du positionnement du body pour les bulles � positionnement
 * absolu
 * - Ajout possibilit� de changer la couleur de fond et d'�crit d'une bulle
 * - Correction sur les �v�nements des bulles o� il y avait inversion dans les
 * identifiants d'�venement show et hide
 * - Restriction private package pass� en public pour le CtxInfoManager
 * Revision 1.4 2008/09/15 13:50:36 bvatan
 * - ajout javadoc
 * Revision 1.3 2008/07/29 08:47:23 bvatan
 * - renommage getter/setter
 * Revision 1.2 2008/07/23 14:27:26 bvatan
 * - merge avec PRODUCT_PAINTING
 * - correction du syst�me de coordonn�es (int,float->double)
 * - autoalignement des produits avec gestion du mode autoAdjustable
 * - fixation de la taille des produits de mani�re uniforme sur tous les
 * composant via SynoptiqueConf
 * - corrections bugs sur la taille des fl�ches signaux
 * Revision 1.1.2.1 2008/07/21 17:09:09 bvatan
 * - merge avec head
 * - passage du syst�me de coordonn�e entier vers double
 */

package com.dolphland.client.util.event;

import java.awt.Color;

import javax.swing.JComponent;

/**
 * Ev�nement d�livr� lorsqu'il faut afficher une bulle d'info contextuelle
 * (VisionCtxInfo) sur un composant.<br>
 * Lorque cet �v�nement est re�u par le framework, ce dernier affiche une bulle
 * d'info contextuelle qui d�signe le composant sp�cifi� dans le constructeur.
 * 
 * @author JayJay
 */
public class UIShowCtxInfoEvt extends UIEvent {

    public static final int TOP_LEFT = 0;

    public static final int TOP = 1;

    public static final int TOP_RIGHT = 2;

    public static final int RIGHT = 3;

    public static final int BOTTOM_RIGHT = 4;

    public static final int BOTTOM = 5;

    public static final int BOTTOM_LEFT = 6;

    public static final int LEFT = 7;

    public static final int AUTO = 8;

    /**
     * Composant d�sign� par la bulle
     */
    private JComponent component;

    /**
     * Texte a afficher dans la bulle
     */
    private String text;

    /**
     * Position de la bulle d'info par rapport au composant auquel elle se
     * rattache
     */
    private int bodyPointStrategy;

    /**
     * Position du point d'ancrage de la bulle (partie qui d�signe le composant)
     * sur le composant auquel elle se rattache
     */
    private int attachPointStrategy;

    /**
     * Couleur du texte de la bulle
     */
    private Color foreground;

    /**
     * Couleur de fond de la bulle
     */
    private Color background;

    /**
     * Indique si la bulle apparait progressivement
     */
    private boolean fadeIn = false;

    /**
     * Indique si la bulle disparait progressivement
     */
    private boolean fadeOut = false;

    /**
     * Dur�e d'affichage de la bulle avant disparition
     */
    private long lifeTime = -1;



    /**
     * Construit l'�v�nement
     * 
     * @param comp
     *            Le composant pour lequel on affiche le VisionCtxInfo
     * @param text
     *            Le texte � afficher
     * @param bodyPointStrategy
     *            La position du VisionCtxInfo par rapport � comp<br>
     *            <li>TOP_LEFT le CtxInfo se place en haut � gauche du composant
     *            <li>TOP le CtxInfo se place en haut au centre du composant <li>
     *            TOP_RIGHT le CtxInfo se place en haut � droite du composant
     *            <li>RIGHT le CtxInfo se place � droite au centre du composant
     *            <li>BOTTOM_RIGHT le CtxInfo se place en bas � droite du
     *            composant <li>BOTTOM le CtxInfo se place en bas au centre du
     *            composant <li>BOTTOM_LEFT le CtxInfo se place en bas � gauche
     *            du composant <li>LEFT le CtxInfo se place � gauche au centre
     *            du composant
     * @param attachPointStrategy
     *            La position du point d'attache du VisionCtxInfo sur comp (idem
     *            position)
     */
    public UIShowCtxInfoEvt(JComponent comp, String text, int bodyPointStrategy, int attachPointStrategy) {
        super(AppEvents.UI_SHOW_CTX_INFO_EVT);
        if (comp == null) {
            throw new NullPointerException("comp is null !"); //$NON-NLS-1$
        }
        this.component = comp;
        this.text = text;
        this.bodyPointStrategy = bodyPointStrategy;
        this.attachPointStrategy = attachPointStrategy;
    }



    /**
     * Constructeur simplifi�, raccourcit vers this(comp, text, TOP_RIGHT, AUTO)
     * 
     * @param comp
     *            Le composant � d�signer
     * @param text
     *            Le texte � afficher
     * @param bodyPointStrategy
     *            Voir constructeur principal
     */
    public UIShowCtxInfoEvt(JComponent comp, String text, int bodyPointStrategy) {
        this(comp, text, bodyPointStrategy, AUTO);
    }



    public void setLifeTime(long lifeTime) {
        this.lifeTime = lifeTime;
    }



    public long getLifeTime() {
        return lifeTime;
    }



    public void setFadeIn(boolean fadeIn) {
        this.fadeIn = fadeIn;
    }



    public boolean shouldFadeIn() {
        return fadeIn;
    }



    public void setFadeOut(boolean fadeOut) {
        this.fadeOut = fadeOut;
    }



    public boolean shouldFadeOut() {
        return fadeOut;
    }



    /**
     * Retourne le colmposant d�sign� par la bulle d'info
     * 
     * @return Le JComponent d�sign� par la bulle
     */
    public JComponent getComponent() {
        return component;
    }



    /**
     * Retourne le texte � afficher dans la bulle
     * 
     * @return Un String au format text ou html
     */
    public String getText() {
        return text;
    }



    /**
     * Retourne le positionnement de la bulle par rapport au composant qu'elle
     * d�signe
     * 
     * @return Un int parmi les constanes d�finies dans cette classe
     */
    public int getBodyPointStrategy() {
        return bodyPointStrategy;
    }



    /**
     * Retourne le positionnement du point d'ancrage de la bulle sur le
     * composant qu'elle d�signe
     * 
     * @return Un int parmi les constanes d�finies dans cette classe
     */
    public int getAttachPointStrategy() {
        return attachPointStrategy;
    }



    /**
     * Fixe la couleur de fond de la bulle
     */
    public void setBackground(Color background) {
        this.background = background;
    }



    /**
     * Retourne la couleur de fond de la bulle
     */
    public Color getBackground() {
        return background;
    }



    /**
     * Fixe la couleur d'�crit de la bulle
     */
    public void setForeground(Color foreground) {
        this.foreground = foreground;
    }



    /**
     * Retourne la couleur d'�crit de la bulle
     */
    public Color getForeground() {
        return foreground;
    }
}
