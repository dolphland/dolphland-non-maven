package com.dolphland.client.util.url;

import java.net.URL;

import com.dolphland.client.util.exception.AppBusinessException;

/**
 * Url util
 * 
 * @author JayJay
 * 
 */
public class UrlUtil {

    /**
     * Get url
     * 
     * @param url
     *            Url
     * 
     * @return
     */
    public final static URL getUrl(String url) {
        try {
            return new URL(url);
        } catch (Exception exc) {
            throw new AppBusinessException("Error when getting url '" + url + "'", exc);
        }
    }
}
