/*
 * $Log: ProductTableRow.java,v $
 * Revision 1.3 2014/03/18 15:17:44 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * - externalized translatable strings
 * Revision 1.2 2011/01/11 10:44:16 bvatan
 * - devient public
 * Revision 1.1 2009/02/04 13:38:39 jscafi
 * - Ajout de ProducTable permettant de g�rer des tables de produits
 * Revision 1.1 2009/01/06 14:17:05 bvatan
 * - ajotu gestion du blocage des barre par coul�e
 */

package com.dolphland.client.util.table;

public class ProductTableRow {

    private int index;

    private ProductAdapter product;



    ProductTableRow(int index) {
        this.index = index;
    }



    public int getIndex() {
        return index;
    }



    public ProductAdapter getProduct() {
        return product;
    }



    public void setProduct(ProductAdapter pa) {
        this.product = pa;
    }



    @Override
    public String toString() {
        return "(ProducttableRow idx=" + index + ",product=" + product.getId() + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }

}
