package com.dolphland.client.util.i18n;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.i18n.varlist.VarType;
import com.dolphland.client.util.i18n.varlist.Variable;
import com.dolphland.client.util.i18n.varlist.Variables;

/**
 * <p>
 * Represents a structured internationalized message.
 * </p>
 * <p>
 * This class contains information for localized message instanciation such as :
 * <br>
 * <li>Message key</li>
 * <li>Message arguments. These argments are typed.</li>
 * <li>{@link MessageFormater} reference enventually used to instanciate the
 * message. This is provided at contruction time.</li>
 * </p>
 * 
 * @author JayJay
 */
public class I18nMessage {

    private String key;

    private MessageFormater formater;

    private List<Variable> variables;

    private Variables vars;

    private boolean dirty;

    private int argNum = 0;



    /**
     * <p>
     * Build a nes message from specified message formater and message key.
     * </p>
     * 
     * @param formater
     *            The message formatter used by the format() method to provide
     *            localized message represented by this instance
     * @param key
     *            The key of the message that the message formatter will user to
     *            lookup the message pattern from its underlying ResourceBundle
     */
    public I18nMessage(MessageFormater formater, String key) {
        AssertUtil.notNull(formater, "formatter cannot be null !"); //$NON-NLS-1$
        AssertUtil.notEmpty(key, "key cannot be null or empty !"); //$NON-NLS-1$
        this.formater = formater;
        this.key = key;
        this.variables = new ArrayList<Variable>();
    }



    /**
     * <p>
     * Return message key that identifies the message pattern
     * </p>
     * <p>
     * This key is the key used by some message instanciator to lookup the
     * message pattern from some {@link ResourceBundle} or any other message
     * data source.
     * </p>
     * 
     * @return The message key.
     */
    public String getMessageKey() {
        return key;
    }



    /**
     * <p>
     * Add a unnamed string parameter to the message.
     * </p>
     * <p>
     * Unnamed parameters are automatically indexed from zero.<br>
     * 1st unanamed paramter is zero, second is 1, etc.
     * </p>
     * 
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage string(String value) {
        dirty = true;
        variables.add(new Variable(VarType.STRING, Integer.toString(argNum++), value));
        return this;
    }



    /**
     * <p>
     * Add a named string parameter to the message.
     * </p>
     * 
     * @param name
     *            The parameter named used in the message pattern. Cannot be
     *            null or
     *            empty
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage string(String name, String value) {
        AssertUtil.notEmpty(name, "name cannot be null or empty !"); //$NON-NLS-1$
        dirty = true;
        variables.add(new Variable(VarType.STRING, name, value));
        return this;
    }



    /**
     * <p>
     * Add a unnamed float parameter to the message.
     * </p>
     * <p>
     * Unnamed parameters are automatically indexed from zero.<br>
     * 1st unanamed paramter is zero, second is 1, etc.
     * </p>
     * 
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage decimal(Number value) {
        dirty = true;
        variables.add(new Variable(VarType.FLOAT, Integer.toString(argNum++), value));
        return this;
    }



    /**
     * <p>
     * Add a named decimal parameter to the message.
     * </p>
     * 
     * @param name
     *            The parameter named used in the message pattern. Cannot be
     *            null or
     *            empty
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage decimal(String name, Number value) {
        AssertUtil.notEmpty(name, "name cannot be null or empty !"); //$NON-NLS-1$
        dirty = true;
        variables.add(new Variable(VarType.FLOAT, name, value));
        return this;
    }



    /**
     * <p>
     * Add a unnamed integer parameter to the message.
     * </p>
     * <p>
     * Unnamed parameters are automatically indexed from zero.<br>
     * 1st unanamed paramter is zero, second is 1, etc.
     * </p>
     * 
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage integer(Number value) {
        dirty = true;
        variables.add(new Variable(VarType.INTEGER, Integer.toString(argNum++), value));
        return this;
    }



    /**
     * <p>
     * Add a named integer parameter to the message.
     * </p>
     * 
     * @param name
     *            The parameter named used in the message pattern. Cannot be
     *            null or
     *            empty
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage integer(String name, Number value) {
        AssertUtil.notEmpty(name, "name cannot be null or empty !"); //$NON-NLS-1$
        dirty = true;
        variables.add(new Variable(VarType.INTEGER, name, value));
        return this;
    }



    /**
     * <p>
     * Add a unnamed date parameter to the message.
     * </p>
     * <p>
     * Unnamed parameters are automatically indexed from zero.<br>
     * 1st unanamed paramter is zero, second is 1, etc.
     * </p>
     * 
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage date(Date value) {
        dirty = true;
        variables.add(new Variable(VarType.DATE, Integer.toString(argNum++), value));
        return this;
    }



    /**
     * <p>
     * Add a named date parameter to the message.
     * </p>
     * 
     * @param name
     *            The parameter named used in the message pattern. Cannot be
     *            null or
     *            empty
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage date(String name, Date value) {
        AssertUtil.notEmpty(name, "name cannot be null or empty !"); //$NON-NLS-1$
        dirty = true;
        variables.add(new Variable(VarType.DATE, name, value));
        return this;
    }



    /**
     * <p>
     * Add a character parameter to the message.
     * </p>
     * <p>
     * Unnamed parameters are automatically indexed from zero.<br>
     * 1st unanamed paramter is zero, second is 1, etc.
     * </p>
     * 
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage character(char value) {
        dirty = true;
        variables.add(new Variable(VarType.DATE, Integer.toString(argNum++), value));
        return this;
    }



    /**
     * <p>
     * Add a named character parameter to the message.
     * </p>
     * 
     * @param name
     *            The parameter named used in the message pattern. Cannot be
     *            null or
     *            empty
     * @param value
     *            The parameter value
     * @return This instance for method chainning
     */
    public I18nMessage character(String name, char value) {
        AssertUtil.notEmpty(name, "name cannot be null or empty !"); //$NON-NLS-1$
        dirty = true;
        variables.add(new Variable(VarType.DATE, name, value));
        return this;
    }



    /**
     * <p>
     * Return this message parameter set as {@link Variables} instance.
     * </p>
     * 
     * @return The set of parameters of the message represented by this instance
     */
    public Variables getVariables() {
        if (dirty) {
            this.vars = new Variables(this.variables);
            dirty = false;
        }
        return vars;
    }



    /**
     * <p>
     * Return the instanciated message from this message key and parameters.
     * </p>
     * <p>
     * Message instanciation is done by this instance underlying
     * {@link MessageFormater}.
     * </p>
     * 
     * @return The mesage instanciated, ready to be displayed
     */
    public String format() {
        return formater.format(key, getVariables().getValueMap());
    }



    @Override
    public String toString() {
        return format();
    }

}
