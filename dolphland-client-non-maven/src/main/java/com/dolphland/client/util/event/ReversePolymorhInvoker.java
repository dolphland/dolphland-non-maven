/*
 * $Log: ReversePolymorhInvoker.java,v $
 * Revision 1.8 2014/03/10 15:42:30 bvatan
 * - Internationalized
 * Revision 1.7 2009/04/09 09:54:51 bvatan
 * - genericisation
 * Revision 1.6 2009/03/12 12:53:27 bvatan
 * - correction bug getDeclaredMethod() �choue dans certains cas -> retour �
 * getMethod() et fall back vers getDeclaredMethod() si getMethod() �choue.
 * Revision 1.5 2009/02/03 11:35:52 bvatan
 * - utilise getDeclaredMethod() au lieu de getMethod()
 * Revision 1.4 2008/01/07 13:03:09 bvatan
 * - correction message exception quand service non trouv� (retrait des +
 * inutiles)
 * Revision 1.3 2008/01/03 16:54:09 bvatan
 * - fiabilisation des invocations lorsque l'argument du service est null ou que
 * le param�tre effzctif est null. Une exception est lev�e si le service �
 * invoqu� n'a pas �t� trouv�.
 * Revision 1.2 2008/01/03 16:46:31 bvatan
 * - ajout trace de log en cas d'erreur d'invocation du service
 * Revision 1.1 2007/03/27 07:19:32 bvatan
 * -renommage packets CTIV -> Vision
 */
package com.dolphland.client.util.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * Classe permettant d'effectuer une invocation de service sur un objet en
 * utilisant un polymorphisme de m�thode ascendant.<br>
 * Exemple :
 * 
 * <pre>
 *  class C1 {
 *  }
 *  
 *  class C2 {
 *  }
 *  
 *  class C3 extends C2 {
 *  }
 *  
 *  class C4 {
 *  }
 *  
 *  class Bar {
 *  	public void service(Object obj) {
 *  		System.out.println(&quot;service with Object param&quot;);
 *  	}
 *  }
 *  
 *  class Foo extends Bar {
 *  
 *  	public void service(C1 obj) {
 *  		System.out.println(&quot;service with C1 param&quot;);
 *  	}
 *  
 *  	public void service(C2 obj) {
 *  		System.out.println(&quot;service with C2 param&quot;);
 *  	}
 *  
 *  	public static void main(String[] args) {
 *  		Foo foo = new Foo();
 *  		ReversePolymorhInvoker ivk = new ReversePolymorhInvoker();
 *  		ivk.invoke(foo,&quot;service&quot;,C1.class,new C1());
 *  		ivk.invoke(foo,&quot;service&quot;,C2.class,new C2());
 *  		ivk.invoke(foo,&quot;service&quot;,C3.class,new C3());
 *  		ivk.invoke(foo,&quot;service&quot;,C4.class,new C4());
 *  	}
 *  }
 *  
 *  donnera :
 *  
 *  service with C1 param
 *  service with C2 param
 *  service with C2 param
 *  service with Object param
 * 
 * </pre>
 * 
 * @author JayJay
 */
public class ReversePolymorhInvoker {

    /**
     * Invoque un service prenant un seul param�tre sur un objet (invokee).
     * 
     * @param invokee
     *            L'object cible sur lequel le service doit �tre invoqu�
     * @param service
     *            Le no�m du service � invoquer
     * @param argType
     *            Le type (Class) de l'argument du service
     * @param arg
     *            L'argument (Object) effectif du service
     * @return L'Object retourn� par le service
     * @throws InvocationTargetException
     *             Si le service cible � lev� une exception
     */
    public Object invoke(Object invokee, String service, Class<?> argType, Object arg) throws InvocationTargetException {
        if (invokee == null) {
            throw new NullPointerException("invokee is null !"); //$NON-NLS-1$
        }
        Object out = null;
        Object[] args = null;
        if (arg != null) {
            args = new Object[] { arg };
        }
        Class<?> clazz = invokee.getClass();
        Method method = null;
        while (clazz != null) {
            Class<?>[] argTypes = null;
            if (argType != null) {
                argTypes = new Class[] { argType };
            }
            try {
                method = clazz.getMethod(service, argTypes);
                break;
            } catch (NoSuchMethodException e) {
                try {
                    method = clazz.getDeclaredMethod(service, argTypes);
                    break;
                } catch (Exception nsme) {
                    // keep on...
                }
                if (argType == null) {
                    break;
                }
                argType = argType.getSuperclass();
            } catch (Exception e) {
                throw new AppInfrastructureException("invokee not invoked !", e); //$NON-NLS-1$
            }
        }
        if (method == null) {
            throw new AppInfrastructureException("Unable to invoke the service " + service + "(" + argType + "). Service not found"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
        }
        try {
            if ((method.getModifiers() & Modifier.PUBLIC) != Modifier.PUBLIC) {
                method.setAccessible(true);
            } else {
                method.setAccessible(true);
            }
            out = method.invoke(invokee, args);
        } catch (InvocationTargetException e) {
            throw e;
        } catch (Exception e) {
            throw new AppInfrastructureException("invokee not invoked ! invocation problem !", e); //$NON-NLS-1$
        }
        return out;
    }

}
