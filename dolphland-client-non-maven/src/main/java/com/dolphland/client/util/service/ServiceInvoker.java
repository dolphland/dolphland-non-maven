package com.dolphland.client.util.service;

import org.apache.log4j.Logger;

import com.dolphland.client.util.concurrent.Executor;
import com.dolphland.client.util.concurrent.JobQueue;

public class ServiceInvoker {

    private final static Logger log = Logger.getLogger(ServiceInvoker.class);

    private static ServiceInvoker defaultInvoker;

    private JobQueue jobQueue;

    private Executor[] executors;

    private boolean started = false;

    private static int nbInstances = 0;



    public ServiceInvoker(int nbExecutors) {
        this.jobQueue = new JobQueue();
        executors = new Executor[nbExecutors];
    }



    public synchronized void start() {
        if (started) {
            return;
        }
        synchronized (ServiceInvoker.class) {
            for (int i = 0; i < executors.length; i++) {
                executors[i] = new Executor(jobQueue, "ServiceInvoker-" + (nbInstances++)); //$NON-NLS-1$
                executors[i].start();
            }
        }
        started = true;
    }



    public synchronized void stop() {
        if (!started) {
            return;
        }
        for (int i = 0; i < executors.length; i++) {
            executors[i].stop();
            executors[i] = null;
        }
        started = false;
    }



    public static ServiceInvoker getDefaultInvoker() {
        if (defaultInvoker == null) {
            defaultInvoker = new ServiceInvoker(1);
        }
        return defaultInvoker;
    }



    public void invoke(final Service svc) {
        Runnable job = new Runnable() {

            public void run() {
                Object resp = null;
                try {
                    resp = svc.executeRequest();
                } catch (Exception e) {
                    log.error("invoke(): Unable to execute service !", e); //$NON-NLS-1$
                }
                try {
                    svc.handleResponse(resp);
                } catch (Exception e) {
                    log.error("invoke(): Unable to notify ServiceResponseHadler !", e); //$NON-NLS-1$
                }
            }
        };
        jobQueue.queueJob(job);
    }

}
