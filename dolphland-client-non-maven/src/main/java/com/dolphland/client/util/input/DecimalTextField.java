package com.dolphland.client.util.input;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

import com.dolphland.client.util.exception.AppBusinessException;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.swingverifiers.Afficheur;

public class DecimalTextField extends CharacterTextField {

    private static final MessageFormater fmt = MessageFormater.getFormater(DecimalTextField.class);

    /**
     * Variables de classe
     */
    private boolean signAuthorized;
    private int nbEntier;
    private int nbDecimal;
    private List separatorDecimalList;



    /**
     * construit un DecimalTextField
     */
    public DecimalTextField() {
        this(MAX_VALUE, MAX_VALUE, null);
    }



    /**
     * construit un DecimalTextField
     * 
     * @param nbEntier
     *            : nombre d'entiers
     * @param nbDecimal
     *            : nombre de d�cimaux
     */
    public DecimalTextField(int nbEntier, int nbDecimal) {
        this(nbEntier, nbDecimal, null);
    }



    /**
     * construit un DecimalTextField
     * 
     * @param nbEntier
     *            : nombre d'entiers
     * @param nbDecimal
     *            : nombre de d�cimaux
     * @param afficheur
     *            : afficheur de message d'erreur
     */
    public DecimalTextField(int nbEntier, int nbDecimal, Afficheur afficheur) {
        this(nbEntier, nbDecimal, afficheur, DEFAULT_FOCUS_NEXT_ENABLED);
    }



    /**
     * construit un DecimalTextField
     * 
     * @param nbEntier
     *            : nombre d'entiers
     * @param nbDecimal
     *            : nombre de d�cimaux
     * @param afficheur
     *            : afficheur de message d'erreur
     * @param focusNextEnabled
     *            : transfert le focus au prochain composant focusable � la fin
     *            de la saisie
     */
    public DecimalTextField(int nbEntier, int nbDecimal, Afficheur afficheur, boolean focusNextEnabled) {
        super(nbEntier + nbDecimal + (nbDecimal > 0 ? 1 : 0), afficheur, focusNextEnabled);
        separatorDecimalList = new ArrayList();
        separatorDecimalList.add(new Character(ENGLISH_SEPARATOR));
        separatorDecimalList.add(new Character(FRENCH_SEPARATOR));
        setNbEntier(nbEntier);
        setNbDecimal(nbDecimal);
        setSignAuthorized(false);
        updateFormat();
        setValue(StrUtil.EMPTY_STRING);
    }



    // METHODES PUBLIQUES

    /**
     * Fixe si le signe est autoris�
     */
    public void setSignAuthorized(boolean signAuthorized) {
        this.signAuthorized = signAuthorized;
        updateFormat();
    }



    /**
     * Fixe le nombre de d�cimal
     */
    public void setNbDecimal(int nbDecimal) {
        if (nbDecimal < 0) {
            throw new AppBusinessException("nbDecimal must be greater or equal than 0 !"); //$NON-NLS-1$
        }
        this.nbDecimal = nbDecimal;
        setNbChar(nbEntier + nbDecimal + (nbDecimal > 0 ? 1 : 0));
        updateFormat();
    }



    /**
     * Fixe le nombre d'entier
     */
    public void setNbEntier(int nbEntier) {
        if (nbEntier <= 0) {
            throw new AppBusinessException("nbEntier must be greater than 0 !"); //$NON-NLS-1$
        }
        this.nbEntier = nbEntier;
        setNbChar(nbEntier + nbDecimal + (nbDecimal > 0 ? 1 : 0));
        updateFormat();
    }



    /**
     * Fixe le s�parateur
     */
    public void setSeparator(char separator) {
        this.separator = separator;
        this.separatorDecimalList.add(new Character(separator));
        updateFormat();
    }



    /**
     * Indique si le signe est autoris�
     */
    public boolean isSignAuthorized() {
        return signAuthorized;
    }



    /**
     * Renvoie le s�parateur
     */
    public char getSeparator() {
        return separator;
    }



    /**
     * Renvoie le nombre d'entiers
     */
    public int getNbEntier() {
        return nbEntier;
    }



    /**
     * Renvoie le nombre de d�cimals
     */
    public int getNbDecimal() {
        return nbDecimal;
    }



    /**
     * Renvoie la valeur sous la forme d'un byte
     */
    @Override
    public byte byteValue() {
        return byteValue(getNbDecimal());
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (short)
     */
    @Override
    public short shortValue() {
        return shortValue(getNbDecimal());
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (int)
     */
    @Override
    public int intValue() {
        return intValue(getNbDecimal());
    }



    /**
     * Renvoie la valeur sous la forme d'un entier (long)
     */
    @Override
    public long longValue() {
        return longValue(getNbDecimal());
    }



    /**
     * Fixe la valeur � partir d'un entier primitif (long)
     */
    @Override
    public void setValue(long value) {
        setValue(value, getNbDecimal());
    }



    /**
     * Fixe la valeur � partir d'un byte (Byte)
     */
    @Override
    public void setValue(Byte value) {
        setValue(value, getNbDecimal());
    }



    /**
     * Fixe la valeur � partir d'un entier (Short)
     */
    @Override
    public void setValue(Short value) {
        setValue(value, getNbDecimal());
    }



    /**
     * Fixe la valeur � partir d'un entier (Integer)
     */
    @Override
    public void setValue(Integer value) {
        setValue(value, getNbDecimal());
    }



    /**
     * Fixe la valeur � partir d'un entier (Long)
     */
    @Override
    public void setValue(Long value) {
        setValue(value, getNbDecimal());
    }



    // METHODES INTERNES AU PACKAGE

    /**
     * initialise le DecimalTextField
     */
    protected void initialize() {
        super.initialize();
        setDocument(new DecimalDocument());
    }



    protected String getCorrectFormat() {
        return getNbEntier() + fmt.format("DecimalTextField.RS_INTEGERS") + separator + " " + getNbDecimal() + fmt.format("DecimalTextField.RS_DECIMALS"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }



    protected void setSeparatorDecimalList(List separatorDecimalList) {
        this.separatorDecimalList = separatorDecimalList;
        updateFormat();
    }



    protected List getSeparatorDecimalList() {
        return separatorDecimalList;
    }



    @Override
    protected void doCompletionConstraints() {

        int indiceSeparateur = getText().indexOf(separator);
        int indiceSigne = getText().indexOf(DEFAULT_SIGN);
        int longueurTexte = getText().length();

        // On ne r�alise la compl�tion seulement dans le cas ou :
        // - COMPLETION_WHEN_INPUT et champ non vide
        // - COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO

        boolean enableInputCompletion = ((getCompletionPolicy() == COMPLETION_WHEN_INPUT) && longueurTexte > 0) || getCompletionPolicy() == COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO;

        // Si la compl�tion doit �tre r�alis�e
        if (enableInputCompletion) {
            // Dans le cas o� le format n�cessite des d�cimals
            if (nbDecimal > 0) {
                // Si pas de s�parateur, on ajoute separator
                if (indiceSeparateur < 0) {
                    try {
                        getDocument().insertString(longueurTexte, new Character(separator).toString(), null);
                        indiceSeparateur = getText().indexOf(separator);
                        indiceSigne = getText().indexOf(DEFAULT_SIGN);
                        longueurTexte = getText().length();
                    } catch (BadLocationException e1) {
                        // ignore
                    }
                }

                // Si aucun entier saisi, on ajoute un 0
                if (longueurTexte <= 1 || indiceSeparateur <= indiceSigne + 1) {
                    try {
                        getDocument().insertString(indiceSigne + 1, "0", null); //$NON-NLS-1$
                        indiceSeparateur = getText().indexOf(separator);
                        indiceSigne = getText().indexOf(DEFAULT_SIGN);
                        longueurTexte = getText().length();
                    } catch (BadLocationException e1) {
                        // ignore
                    }
                }

                // On compl�te les d�cimals par des 0 pour atteindre le bon
                // nombre de d�cimals
                while (longueurTexte - (indiceSeparateur + 1) < nbDecimal) {
                    try {
                        getDocument().insertString(longueurTexte, "0", null); //$NON-NLS-1$
                        indiceSeparateur = getText().indexOf(separator);
                        indiceSigne = getText().indexOf(DEFAULT_SIGN);
                        longueurTexte = getText().length();
                    } catch (BadLocationException e1) {
                        // ignore
                    }
                }
            }
            // Dans le cas o� le format ne n�cessite pas de d�cimals
            else {
                // Si aucun entier saisi, on ajoute un 0
                if (longueurTexte <= 0) {
                    try {
                        getDocument().insertString(0, "0", null); //$NON-NLS-1$
                        longueurTexte = getText().length();
                    } catch (BadLocationException e1) {
                        // ignore
                    }
                }
            }
        }

        super.doCompletionConstraints();
    }

    /**
     * Document permettant de saisir uniquement des nombres d�cimaux
     */
    protected class DecimalDocument extends CharacterDocument {

        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            int len = str.length();
            String currentChar;
            int nbInsertionOK = 0;
            for (int cpt = 0; cpt < len; cpt++) {
                currentChar = String.valueOf(str.charAt(cpt));
                // Si le caract�re courant est un s�parateur alors que le format
                // ne n�cessite pas de d�cimals
                if (nbDecimal == 0 && currentChar.equals(new Character(separator).toString())) {
                    // On limite l'insertion � la partie enti�re
                    break;
                }
                insertOneDecimal(offs + nbInsertionOK, currentChar, a);
                if (isInsertionOK()) {
                    nbInsertionOK++;
                }
            }
        }



        /**
         * permet de saisir uniquement un caract�re du nombre d�cimal
         */
        public void insertOneDecimal(int offs, String str, AttributeSet a) throws BadLocationException {
            int offsetInRepresentation = getOffsetInRepresentation(offs);

            if (signAuthorized && offs == 0 && str.equals(new Character(DEFAULT_SIGN).toString()) && DecimalTextField.this.getText().indexOf(DEFAULT_SIGN) == -1) {
                super.insertString(offs, str, a);
                return;
            }
            else if (isOffsetOnPartieEntiere(offsetInRepresentation)) {
                // pas encore de . OK si longueur totale < nbEntier
                if (getIndexSeparator() == -1 && getRepresentation().length() < nbEntier && Character.isDigit(str.charAt(0))) {
                    super.insertString(offs, str, a);
                    return;
                }
                // le . est deja mis ok si le nombre de caractere avant le . est
                // < au nb entier
                if (getIndexSeparator() >= 0 && getIndexSeparator() < nbEntier && Character.isDigit(str.charAt(0))) {
                    super.insertString(offs, str, a);
                    return;
                }
                // ok si pas de .
                if (getIndexSeparator() == -1 && nbDecimal > 0 && isAuthorizedSeparator(str.charAt(0))) {
                    super.insertString(offs, String.valueOf(separator), a);
                    return;
                }
            }
            else if (isOffsetOnPartieDecimale(offsetInRepresentation)) {
                if (getRepresentation().length() - getIndexSeparator() - 1 < nbDecimal && Character.isDigit(str.charAt(0))) {
                    super.insertString(offs, str, a);
                    if (isFocusNextEnabled() && getRepresentation().length() - getIndexSeparator() - 1 == nbDecimal) {
                        transferFocus();
                    }
                    return;
                }
            }
            invalidFormat();
        }



        private boolean isAuthorizedSeparator(char car) {
            if (separatorDecimalList == null) {
                return false;
            }
            for (Iterator iter = separatorDecimalList.iterator(); iter.hasNext();) {
                Object object = iter.next();
                if (object != null && object instanceof Character) {
                    Character element = (Character) object;
                    if (car == element.charValue()) {
                        return true;
                    }
                }
            }
            return false;
        }



        /**
         * @param offs
         *            offset
         * @return vrai si l'edition du textField se fait dans la partie entiere<br>
         *         faux sinon
         */
        private boolean isOffsetOnPartieEntiere(int offs) {
            int index = getIndexSeparator();
            if (index == -1) {
                // si pas de . on �dite la partie entiere
                return true;
            } else {
                return (offs <= index);
            }
        }



        /**
         * @return la position du s�parateur
         *         -1 si pas s�parateur
         */
        private int getIndexSeparator() {
            return getRepresentation().indexOf(String.valueOf(separator));
        }



        /**
         * 
         * @param offs
         * @return vrai si l'edition du textField se fait dans la partie
         *         d�cimale<br>
         *         faux sinon
         */
        private boolean isOffsetOnPartieDecimale(int offs) {
            return !isOffsetOnPartieEntiere(offs);
        }



        @Override
        protected boolean isDansRepresentation(int offset, String newString) {
            return !newString.equals(new Character(DEFAULT_SIGN).toString());
        }
    }
}
