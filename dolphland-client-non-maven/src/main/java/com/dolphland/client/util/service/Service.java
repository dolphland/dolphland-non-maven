package com.dolphland.client.util.service;

/**
 * D�finit la structure d'un service
 * 
 * @author JayJay
 */
public abstract class Service {

    /** Handler notifi� du r�sultat de l'ex�cution du service */
    private ServiceResponseHandler serviceResponseHandler;

    /** Invoker charg� de l'ex�cution du service */
    private ServiceInvoker serviceInvoker;



    /**
     * Construit le service
     * 
     * @param srvInvoker
     *            L'invoker a utiliser pour ex�cuter le service
     * @param serviceResponseHandler
     *            Le handler notifi� du r�sulta de l'ex�cution
     */
    public Service(ServiceInvoker srvInvoker, ServiceResponseHandler serviceResponseHandler) {
        if (srvInvoker == null) {
            throw new NullPointerException("srvInvoker is null !"); //$NON-NLS-1$
        }
        if (serviceResponseHandler == null) {
            throw new NullPointerException("serviceResponseHandler is null !"); //$NON-NLS-1$
        }
        this.serviceResponseHandler = serviceResponseHandler;
        this.serviceInvoker = srvInvoker;
    }



    /**
     * Execute la requ�te
     */
    abstract protected Object executeRequest();



    /**
     * Retourne le Class de l'objet r�ponse qui doit �tre consid�r� comme �tant
     * une r�ponse d'erreur.
     * 
     * @return La classe des objets r�ponses consid�r�s comme des erreurs.
     */
    abstract protected Class getResponseErrorClass();



    /**
     * Ex�cute le service, soit respectivement :<br>
     * <li>Ex�cute Service.executeRequest();</li> <li>Ex�cute
     * Service.handleResponse();</li>
     * 
     */
    final public void execute() {
        serviceInvoker.invoke(this);
    }



    /**
     * G�re le r�sultat de l'ex�cution du service.<br>
     * En cas de succ�s :<br>
     * <br>
     * <ul>
     * <li>Invocation de ServiceResponseHandler.setServiceResult()</li>
     * </ul>
     * En cas d'erreur (suivant Service.getResponseErrorClass() :<br>
     * <br>
     * <ul>
     * <li>Invocation de ServiceResponseHandler.setServiceError()</li>
     * </ul>
     * 
     * @param response
     */
    final public void handleResponse(Object response) {
        if (response.getClass().equals(getResponseErrorClass())) {
            serviceResponseHandler.setServiceError(response);
        } else {
            serviceResponseHandler.setServiceResult(response);
        }
    }

}
