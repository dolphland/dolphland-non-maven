package com.dolphland.client.util.input;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.swingverifiers.Afficheur;

/**
 * JTextField permettant de saisir uniquement des lettres
 */
public class LetterTextField extends CharacterTextField {

    private static final MessageFormater fmt = MessageFormater.getFormater(LetterTextField.class);



    /**
     * construit un LetterTextField
     */
    public LetterTextField() {
        this(MAX_VALUE, null);
    }



    /**
     * construit un LetterTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     */
    public LetterTextField(int nbChar) {
        this(nbChar, null);
    }



    /**
     * construit un LetterTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     */
    public LetterTextField(int nbChar, Afficheur afficheur) {
        this(nbChar, afficheur, DEFAULT_FOCUS_NEXT_ENABLED);
    }



    /**
     * construit un LetterTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     * @param focusNextEnabled
     *            : transfert le focus au prochain composant focusable � la fin
     *            de la saisie
     */
    public LetterTextField(int nbChar, Afficheur afficheur, boolean focusNextEnabled) {
        super(nbChar, afficheur, focusNextEnabled);
        updateFormat();
    }



    /**
     * initialise le LetterTextField
     */
    protected void initialize() {
        super.initialize();
        setDocument(new LetterDocument());
    }



    protected String getCorrectFormat() {
        return getNbChar() + fmt.format("LetterTextField.RS_LETTERS"); //$NON-NLS-1$
    }

    /**
     * Document permettant de saisir des lettres
     */
    protected class LetterDocument extends CharacterDocument {

        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            int len = str.length();
            String currentChar;
            int nbInsertionOK = 0;
            for (int cpt = 0; cpt < len; cpt++) {
                currentChar = String.valueOf(str.charAt(cpt));
                insertOneLetter(offs + nbInsertionOK, currentChar, a);
                if (isInsertionOK()) {
                    nbInsertionOK++;
                }
            }
        }



        /**
         * insert juste une lettre
         */
        public void insertOneLetter(int offs, String str, AttributeSet a) throws BadLocationException {
            if (Character.isLetter(str.charAt(0))) {
                super.insertString(offs, str, a);
                return;
            }
            invalidFormat();
        }
    }
}
