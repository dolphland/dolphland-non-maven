package com.dolphland.client.util.table;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Classe impl�mentant le rendu d'un ent�te de colonne d'une table avec colonnes
 * groupables
 * 
 * @author jeremy.scafi
 * 
 */
public class DefaultGroupableTableHeaderRenderer implements TableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        return table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}
