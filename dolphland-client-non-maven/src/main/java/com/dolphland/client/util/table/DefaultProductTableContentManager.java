/*
 * $Log: DefaultProductTableContentManager.java,v $
 * Revision 1.9 2014/03/18 15:17:44 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * - externalized translatable strings
 * Revision 1.8 2013/08/27 13:22:18 bvatan
 * - FWK-54: remove all references to ProductAction, all replaced by
 * UIAction<ProductActionevt, ?>
 * Revision 1.7 2013/04/22 08:26:57 bvatan
 * - added cell editor support
 * Revision 1.6 2010/12/02 08:37:39 bvatan
 * - ajout possibilit� d'ajouter une bulle de description sur une colonne
 * et/ou sur une cellule
 * Revision 1.5 2010/07/29 09:51:27 bvatan
 * - gestion des ProductActionConstraints permettant d'associer des contrainte
 * de positionnement (ou autre) sur les ProductAction
 * Revision 1.4 2010/07/21 07:29:37 bvatan
 * - getProductActionConstraints retourne null par d�faut
 * Revision 1.3 2010/06/09 09:47:46 bvatan
 * - Ajout possibilit� de positionner les ProductAction sur les 4 c�t�s de
 * de la table
 * Revision 1.2 2009/08/07 12:53:13 bvatan
 * - suppression de formatAsString()
 * Revision 1.1 2009/02/04 13:38:39 jscafi
 * - Ajout de ProducTable permettant de g�rer des tables de produits
 * Revision 1.1 2009/01/06 14:17:04 bvatan
 * - ajotu gestion du blocage des barre par coul�e
 */

package com.dolphland.client.util.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.table.TableCellEditor;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.table.demo.ButtonTableCellEditor;

/**
 * Classe impl�mentant un manager du contenu de la table des produits par
 * d�faut
 * 
 * @author jeremy.scafi
 * @author JayJay
 * 
 */
public class DefaultProductTableContentManager extends ProductTableContentManager {

    /**
     * Renderer de produit fournissant le composant de rendu du contenu de la
     * propri�t� de produit pour le produit donn�
     * 
     * @param input
     *            Composant de rendu de la cellule concern�e dans la table
     * @param pp
     *            Propri�t� de produit
     * @param pa
     *            Produit
     * @param isSelected
     *            Indique si la cellule concern�e dans la table est
     *            s�lectionn�e
     * @param hasFocus
     *            Indique si la cellule concern�e dans la table a le focus
     * @param row
     *            Indice de ligne de la cellule concern�e dans la table
     * @param column
     *            Indice de colonne de la cellule concern�e dans la table
     * @return Composant de rendu du contenu de la propri�t� de produit pour
     *         le produit donn�
     */
    public Component renderProduct(Component input, ProductProperty pp, ProductAdapter pa, boolean isSelected, boolean hasFocus, int row, int column) {
        if (pp.getType() == ProductProperty.ACTION_TYPE) {
            UIAction<ProductActionEvt, ?> action = (UIAction<ProductActionEvt, ?>) pa.getValue(pp);
            return getLabelButton(action, row, column);
        }
        return input;
    }



    /**
     * Renderer de propri�t� de produit fournissant le composant de rendu de
     * la propri�t� de produit donn�
     * 
     * @param input
     *            Composant de rendu de l'ent�te concern� dans la table
     * @param pp
     *            Propri�t� de produit
     * @param isSelected
     *            Indique si l'ent�te concern� dans la table est
     *            s�lectionn�
     * @param hasFocus
     *            Indique si l'ent�te concern� dans la table a le focus
     * @param row
     *            Indice de ligne de l'ent�te concern� dans la table
     * @param column
     *            Indice de colonne de l'ent�te concern� dans la table
     * @return
     */
    public Component renderProductProperty(Component input, ProductProperty pp, boolean isSelected, boolean hasFocus, int row, int column) {
        return input;
    }



    /**
     * Fixe la largeur d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur de la propri�t� de produit
     */
    public int getProductPropertyWidth(ProductProperty pp) {
        return getProductPropertyPreferredWidth(pp);
    }



    /**
     * Fixe la largeur minimum d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur minimum de la propri�t� de produit
     */
    public int getProductPropertyMinWidth(ProductProperty pp) {
        return 15;
    }



    /**
     * Fixe la largeur maximum d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur maximum de la propri�t� de produit
     */
    public int getProductPropertyMaxWidth(ProductProperty pp) {
        return Integer.MAX_VALUE;
    }



    /**
     * Fixe la largeur pr�f�r�e d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur pr�f�r�e de la propri�t� de produit
     */
    public int getProductPropertyPreferredWidth(ProductProperty pp) {
        return 80;
    }



    /**
     * Indique si la s�lection est autoris�e dans la table des produits
     */
    public boolean isProductTableSelectionEnabled() {
        return true;
    }



    /**
     * Indique si la s�lection de ligne est autoris�e dans la table des
     * produits
     */
    public boolean isProductTableRowSelectionAllowed() {
        return true;
    }



    /**
     * Indique si le r�ordonnancement de colonnes est autoris� dans la table
     * des produits
     */
    public boolean isProductTableReorderingAllowed() {
        return false;
    }



    /**
     * Indique le type de s�lection de la table des produits
     * Valeurs possibles :
     * - ListSelectionModel.SINGLE_SELECTION : S�lection simple
     * - ListSelectionModel.SINGLE_INTERVAL_SELECTION : S�lection intervale
     * simple
     * - ListSelectionModel.MULTIPLE_INTERVAL_SELECTION : S�lection intervale
     * multiple
     */
    public int getProductTableSelectionMode() {
        return ListSelectionModel.MULTIPLE_INTERVAL_SELECTION;
    }



    /**
     * Renvoie la hauteur de ligne de la table des produits
     */
    public int getProductTableRowHeight() {
        return 30;
    }



    /**
     * Indique si la table des produits est opaque
     */
    public boolean isProductTableOpaque() {
        return true;
    }



    /**
     * Indique si les grille de la table des produits sont visibles
     */
    public boolean isProductTableShowGrid() {
        return true;
    }



    /**
     * Indique si les lignes horizontables de la table des produits sont
     * visibles
     */
    public boolean isProductTableShowHorizontalLines() {
        return true;
    }



    /**
     * Indique si la cellule est �ditable
     */
    public boolean isCellEditable(ProductProperty pp, ProductAdapter pa) {
        return false;
    }



    /**
     * Indique si la cellule est �ditable
     */
    public boolean isCellDisabled(ProductProperty pp, ProductAdapter pa) {
        if (pp.getType() == ProductProperty.ACTION_TYPE) {
            UIAction<ProductActionEvt, ?> action = (UIAction<ProductActionEvt, ?>) pa.getValue(pp);
            if (action != null && (!action.isEnabled() || !action.isEnabled(getActionEvtFactory().createUIActionEvt()))) {
                return true;
            }
        }
        return false;
    }



    /**
     * Renvoie la bordure de la table des produits
     */
    public Border getProductTableBorder() {
        return new MatteBorder(1, 1, 1, 1, Color.BLACK);
    }



    /**
     * Renvoie la font par d�faut de la table des produits
     */
    public Font getProductTableDefaultFont() {
        return new Font("Dialog", Font.BOLD, 16); //$NON-NLS-1$
    }



    /**
     * Fournit les contraintes associ�es au <code>ProductAction</code>
     * sp�cifi�.
     */
    public ProductActionConstraints getProductActionConstraints(UIAction<ProductActionEvt, ?> pa) {
        return new ProductActionConstraints();
    }



    /**
     * Fournit le tooltip d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Tooltip de la propri�t� de produit
     */
    public String getProductPropertyTooltip(ProductProperty pp) {
        return null;
    }



    /**
     * Fournit le tooltip d'une cellule de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @param pa
     *            Produit
     * @return Tooltip de la cellule de produit
     */
    public String getCellTooltip(ProductProperty pp, ProductAdapter pa) {
        return null;
    }



    /**
     * Fournit l'�diteur de cellule d'une table
     * 
     * @param pp
     *            Propri�t� de produit
     * @param column
     *            Indice de colonne de la cellule concern�e dans la table
     * @return Editeur de cellule d'une table
     */
    public TableCellEditor getTableCellEditor(ProductProperty pp, int column) {
        if (pp.getType() == ProductProperty.ACTION_TYPE) {
            return new ButtonTableCellEditor<JButton>(new JButton(), getActionEvtFactory());
        }
        return new DefaultCellEditor(new JTextField());
    }
}
