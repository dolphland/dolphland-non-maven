package com.dolphland.client.util.table;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;

/**
 * <p>
 * Custom {@link ExtendedTable} implementation that supports column resize event
 * triggering
 * </p>
 * 
 * @author Sebastien Laout
 * @author JayJay
 */
class ResizeableExtendedTable extends ExtendedTable {

    private static final Logger LOG = Logger.getLogger(ResizeableExtendedTable.class);

    private boolean columnsWereResized = false;

    private ResizeableTableListener listener;



    public void setResizeableTableListener(ResizeableTableListener listener) {
        this.listener = listener;
    }



    @Override
    public void setColumnModel(TableColumnModel columnModel) {
        columnModel.addColumnModelListener(new TableColumnModelListener() {

            public void columnSelectionChanged(ListSelectionEvent e) {
                // Nothing to do
            }



            public void columnRemoved(TableColumnModelEvent e) {
                // Nothing to do
            }



            public void columnMoved(TableColumnModelEvent e) {
                // Nothing to do
            }



            public void columnMarginChanged(ChangeEvent e) {
                columnsWereResized = true;
            }



            public void columnAdded(TableColumnModelEvent e) {
                // Nothing to do
            }
        });

        super.setColumnModel(columnModel);
    }



    @Override
    public void doLayout() {
        columnsWereResized = false;
        super.doLayout();
        if (columnsWereResized && listener != null) {
            ResizeableTableEvent rte = new ResizeableTableEvent(this);
            try {
                listener.columnsResized(rte);
            } catch (Exception e) {
                LOG.error("doLayout(): ResizeableTableListener.columnsResized() has failed !", e); //$NON-NLS-1$
            }
        }
    }
}
