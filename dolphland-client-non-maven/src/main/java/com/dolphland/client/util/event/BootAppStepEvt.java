/*
 * $Log: BootAppStepEvt.java,v $
 * Revision 1.1 2008/11/28 10:05:28 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

public class BootAppStepEvt extends BootAppEvt {

    public BootAppStepEvt() {
        super(FwkEvents.BOOT_APP_STEP_EVT);
    }

}
