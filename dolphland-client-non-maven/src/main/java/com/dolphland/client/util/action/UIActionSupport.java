package com.dolphland.client.util.action;

/**
 * Interface permettant l'ajout et la suppression d'actions (
 * <code>UIAction</code>) et de groupes d'actions (<code>UIActionGroup</code>).
 * 
 * @author jeremy.scafi
 */
public interface UIActionSupport {

    /**
     * Ajout d'une action (<code>UIAction</code>) ou d'un groupe d'actions (
     * <code>UIActionGroup</code>)
     * 
     * @param action
     *            Action ou groupe d'actions � ajouter
     */
    public void addAction(UIAction action);



    /**
     * Suppression de toutes les actions
     */
    public void removeAllActions();



    /**
     * Mise � jour des actions
     * 
     */
    public void updateActions();
}
