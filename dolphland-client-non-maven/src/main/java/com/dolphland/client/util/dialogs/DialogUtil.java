package com.dolphland.client.util.dialogs;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.input.PasswordTextField;

public class DialogUtil {

    private static final MessageFormater FMT = MessageFormater.getFormater(DialogUtil.class);



    /**
     * Affiche une dialogue de saisie de mot de passe
     * 
     * @param parentComponent
     *            Composant parent de la dialogue
     * @param message
     *            Objet contenant le message de saisie
     * @param title
     *            Titre de la dialogue
     * @param messageType
     *            Type de message (JOptionPane.ERROR_MESSAGE,
     *            JOptionPane.INFORMATION_MESSAGE, JOptionPane.WARNING_MESSAGE,
     *            JOptionPane.QUESTION_MESSAGE et JOptionPane.PLAIN_MESSAGE)
     * 
     * @return Mot de passe saisi (null si fermeture de la dialogue ou choix
     *         annuler)
     */
    public static String showPasswordDialog(Component parentComponent, Object message, String title, int messageType) {
        return showPasswordDialog(parentComponent, message, title, messageType, null, null);
    }



    /**
     * Affiche une dialogue de saisie de mot de passe
     * 
     * @param parentComponent
     *            Composant parent de la dialogue
     * @param message
     *            Objet contenant le message de saisie
     * @param title
     *            Titre de la dialogue
     * @param messageType
     *            Type de message (JOptionPane.ERROR_MESSAGE,
     *            JOptionPane.INFORMATION_MESSAGE, JOptionPane.WARNING_MESSAGE,
     *            JOptionPane.QUESTION_MESSAGE et JOptionPane.PLAIN_MESSAGE)
     * @param icon
     *            Icone de la dialogue
     * @param initialValue
     *            Valeur initiale de la saisie
     * 
     * @return Mot de passe saisi (null si fermeture de la dialogue ou choix
     *         annuler)
     */
    public static String showPasswordDialog(Component parentComponent, Object message, String title, int messageType, Icon icon, String initialValue) {
        PasswordTextField txtPwd = new PasswordTextField();
        if (initialValue != null) {
            txtPwd.setValue(initialValue);
        }
        Object[] msg = { message, txtPwd };
        Object[] options = new Object[] { FMT.format("DialogUtil.RS_OK"), FMT.format("DialogUtil.RS_ANNULER") };
        JOptionPane pane = new JOptionPane(msg, messageType, JOptionPane.OK_CANCEL_OPTION, icon, options, txtPwd);
        final JDialog dialog = pane.createDialog(parentComponent, title);
        txtPwd.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                dialog.setVisible(false);
            }
        });
        pane.setInitialValue(txtPwd);
        pane.selectInitialValue();
        dialog.setVisible(true);
        dialog.dispose();

        Object selectedValue = pane.getValue();
        if (selectedValue == null || new Integer(JOptionPane.CLOSED_OPTION).equals(selectedValue) || options[1].equals(selectedValue)) {
            return null;
        }
        return txtPwd.stringValue();
    }



    /**
     * Centre le dialogue s�pcifi� par rapport � la taille de l'�cran
     * 
     * @param d
     *            Le dialogue � centrer
     */
    public static void centerOnScreen(Dialog d) {
        if (d == null) {
            return;
        }
        Dimension sSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dSize = d.getSize();
        d.setLocation((sSize.width - dSize.width) / 2, (sSize.height - dSize.height) / 2);
    }



    /**
     * Centre le dialogue sp�cifi� par rapport au composant sp�cifi�. Si comp
     * est null, el dialogue sera centr� � l'�cran
     * 
     * @param d
     *            Le dialogue � centrer
     * @param comp
     *            Le composant sur lequel centrer le dialogue
     */
    public static void centerOnComp(Dialog d, Component comp) {
        if (d == null) {
            return;
        }
        if (comp == null) {
            centerOnScreen(d);
            return;
        }
        Dimension cSize = comp.getSize();
        Dimension dSize = d.getSize();
        int x = (cSize.width - dSize.width) / 2;
        int y = (cSize.height - dSize.height) / 2;
        Point p = new Point(x, y);
        SwingUtilities.convertPointToScreen(p, comp);
        p.x = p.x < 0 ? 0 : p.x;
        p.y = p.y < 0 ? 0 : p.y;
        d.setLocation(p);
    }



    public static Dimension getScreenSize() {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }



    public static Window getParentWindow(Component current) {
        while (current != null) {
            current = current.getParent();
            if (current instanceof JDialog) {
                return (Window) current;
            }
            if (current instanceof JFrame) {
                return (Window) current;
            }
        }
        throw new RuntimeException("Error while searching the parent container!"); //$NON-NLS-1$
    }
}
