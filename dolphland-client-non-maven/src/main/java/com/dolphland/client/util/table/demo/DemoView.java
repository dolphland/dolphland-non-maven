package com.dolphland.client.util.table.demo;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.application.components.FwkView;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.table.ProductActionEvt;
import com.dolphland.client.util.table.ProductAdapter;
import com.dolphland.client.util.table.ProductTable;


public class DemoView extends FwkView<DemoActionEvt>{

    private ProductTable tableDemo;
    private JButton btnAction;
    
    public DemoView() {
        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();
        
        setLayout(layoutBuilder.get());
        
        add(getTableTest(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .row(0)
            .topInsets(1f/2f)
            .height(TableLayout.PREFERRED)
            .leftInsets(1f/2f)
            .width(TableLayout.PREFERRED)
            .rightInsets(1f/2f)
            .get()));
        
        add(createPanelActions(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .row(1)
            .topInsets(DEFAULT_GAP)
            .height(TableLayout.PREFERRED)
            .bottomInsets(1f/2f)
            .get()));
    }
    
    private FwkPanel createPanelActions(){
        FwkPanel out = new FwkPanel();
        out.setBackground(Color.WHITE);
        
        TableLayoutBuilder layoutBuilder = createTableLayoutBuilder();
        
        out.setLayout(layoutBuilder.get());
        
        out.add(getBtnAction(), layoutBuilder.addComponent(createTableLayoutConstraintsBuilder()
            .height(TableLayout.PREFERRED)
            .leftInsets(1f/2f)
            .width(TableLayout.PREFERRED)
            .rightInsets(1f/2f)
            .get()));
        
        return out;
    }
    
    private ProductTable getTableTest(){
        if(tableDemo == null){
            tableDemo = new ProductTable(new DemoProductDescriptor(), new DemoProductTableContentManager());
        }
        return tableDemo;
    }
    
    private JButton getBtnAction(){
        if(btnAction == null){
            btnAction = new JButton();
        }
        return btnAction;
    }
    
    public void setProducts(List<DemoProductAdapter> products){
        getTableTest().setProducts(new ArrayList<ProductAdapter>(products));
    }
    
    public void addProductAction(UIAction<ProductActionEvt, ?> actionProduct){
        bind(actionProduct, getTableTest());
    }
    
    public void setAction(UIAction<DemoActionEvt, ?> action){
        bind(action, getBtnAction());
    }
}
