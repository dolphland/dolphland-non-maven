package com.dolphland.client.util.table.demo;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;

import javax.swing.AbstractButton;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvtFactory;
import com.dolphland.client.util.action.UIActionUtil;
import com.dolphland.client.util.application.components.FwkLabel;
import com.dolphland.client.util.application.components.FwkPanel;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.table.ProductActionEvt;
import com.dolphland.client.util.table.ProductTable;


public class ButtonTableCellEditor<T extends AbstractButton> extends AbstractCellEditor implements TableCellEditor{
    
    private T editorComponent;
    
    private UIAction<ProductActionEvt, ?> action;
    
    
    public ButtonTableCellEditor(T editorComponent, final UIActionEvtFactory<ProductActionEvt> actionEvtFactory) {
        this.editorComponent = editorComponent;
        getEditorComponent().addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                // Execute action if possible
                if(action != null && actionEvtFactory != null){
                    UIActionUtil.executeAction(action, actionEvtFactory);   
                }
                
                // Cancel editing after action's execution
                cancelCellEditing();
            }
        });

    }
    
    
    public T getEditorComponent() {
        return editorComponent;
    }
    
    @Override
    public Object getCellEditorValue() {
        return action;
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        // Get action from value
        action = (UIAction<ProductActionEvt, ?>)value;
        
        // Set button's text
        if(action != null && !StrUtil.isEmpty(action.getName())){
            getEditorComponent().setText(action.getName());
        }        

        // Create and return button's label
        return createLabelButton(getEditorComponent(), action);
    }
    
    public static FwkLabel createLabelButton(AbstractButton button, UIAction<ProductActionEvt, ?> action){
        FwkLabel label = new FwkLabel();
        TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
        label.setLayout(layoutBuilder.get());
        label.add(button, layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
            .topInsets(1f/2f)
            .height(TableLayout.PREFERRED)
            .bottomInsets(1f/2F)
            .leftInsets(1f/2f)
            .width(TableLayout.PREFERRED)
            .rightInsets(1f/2f)
            .get()));
        
        // Set label's tooltip
        if(action != null){
            label.setToolTipText(action.getDescription());
        }
        return label;
    }

}
