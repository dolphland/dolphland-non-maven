package com.dolphland.client.util.chart;

import javax.swing.JPanel;

import org.jfree.chart.LegendItem;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.PieSectionEntity;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 * Cette classe permet de repr�senter un chart de type PiePlot avec une l�gende
 * am�lior�e.
 * 
 * La principale am�lioration de cette l�gende est de pouvoir d�cocher certaines
 * s�ries pour ne pas les voir appara�tre sur le chart.
 * 
 * La repr�sentation du Chart se fait dans le JComponent componentChart que l'on
 * peut obtenir par getChartComponent()
 * 
 * Possibilit� d'y d�finir :
 * - des entit�s de donn�es. Une entit� de donn�e est une s�rie identifi�e par
 * un Comparable, constitu�e de valeurs (identit� valeur, ordonn�e)
 * 
 * @author jeremy.scafi
 */
public abstract class AbstractPiePlotChart extends AbstractChart {

    private DefaultPieDataset dataset;

    private ChartSelection chartSelection;



    /**
     * Constructeur
     */
    public AbstractPiePlotChart() {
        this(new JPanel(), new ChartConstraints());
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AbstractPiePlotChart(ChartConstraints chartConstraints) {
        this(new JPanel(), chartConstraints);
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public AbstractPiePlotChart(JPanel componentChart) {
        this(componentChart, new ChartConstraints());
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param componentChart
     *            : Composant devant repr�senter le Chart
     * @param chartConstraints
     *            : Contraintes � appliquer au Chart
     */
    public AbstractPiePlotChart(JPanel componentChart, ChartConstraints chartConstraints) {
        super(componentChart, chartConstraints);
    }



    /**
     * Initialise les donn�es du chart
     */
    public void initChart() {
        dataset.clear();
        super.initChart();
    }



    /**
     * Renvoie la liste des items de la l�gende
     */
    protected LegendItem[] getLegendItems() {
        PiePlot plot = ((PiePlot) getChart().getPlot());

        LegendItem[] legendItems = new LegendItem[plot.getLegendItems().getItemCount()];
        for (int i = 0; i < legendItems.length; i++) {
            legendItems[i] = plot.getLegendItems().get(i);
        }

        return legendItems;
    }



    /**
     * Cr�e l'impl�mentation du listener permettant de modifier les contraintes
     * du graphe en temps r�el
     */
    protected ChartConstraintsListener createChartConstraintsListenerImpl() {
        return new PiePlotChartConstraintsListenerImpl();
    }



    @Override
    protected ChartSelection getChartSelection() {
        if (chartSelection == null) {
            chartSelection = new PieSelectionRenderer(getDataset());
        }
        return chartSelection;
    }



    /** Ajoute une valeur pour l'entit� de donn�es identifi�e par idChartData */
    public void addData(Comparable idChartData, double value) {
        // On r�cup�re l'entit� des donn�es identifi�e par idChartData
        PieChartData chartData = (PieChartData) getChartData(idChartData);

        // Si elle n'existe pas, on la cr�e et on l'ajoute
        if (chartData == null) {
            chartData = new PieChartData(idChartData);
        }

        // On y ajoute les donn�es pass�es en param�tre
        chartData.addValue(idChartData, new Double(value));

        putChartData(chartData);
    }



    /** Fournit le DataSet du chart */
    protected PieDataset getDataset() {
        if (dataset == null) {
            dataset = new DefaultPieDataset();
        }
        return dataset;
    }



    /** Mise � jour de la couleur des sections */
    protected void updateColorSections() {
        ChartSelection chartSelection = getChartSelection();
        PiePlot plot = (PiePlot) getChart().getPlot();
        if (getChartConstraints().getColorsChartData() != null) {
            for (int i = 0; i < getChartConstraints().getColorsChartData().length; i++) {
                if (dataset.getItemCount() > i) {
                    plot.setSectionPaint(dataset.getKey(i), getChartConstraints().getColorsChartData()[i]);
                }
            }
        }
        if (chartSelection.isSelectionActive()) {
            plot.setSectionPaint(dataset.getKey(chartSelection.getSelectedSeries()), getChartConstraints().getSelectionColor());
        }
    }

    /**
     * Impl�mentation du listener permettant de modifier les contraintes du
     * graphe en temps r�el
     * 
     * @author jeremy.scafi
     */
    private class PiePlotChartConstraintsListenerImpl extends DefaultChartConstraintsListenerImpl {

        /** Fixe les couleurs des donn�es du chart */
        @Override
        public void doChangeColorsChartData() {
            updateColorSections();
            super.doChangeColorsChartData();
        }



        /** Fixe la visibilit� des �tiquettes */
        @Override
        public void doChangeLabelsVisible() {
            PiePlot plot = (PiePlot) getChart().getPlot();

            if (getChartConstraints().isLabelsVisible()) {
                // Permet de ne pas afficher les �tiquettes des entit�s non
                // visibles dans la l�gende
                plot.setLabelGenerator(new StandardPieSectionLabelGenerator() {

                    @Override
                    public String generateSectionLabel(PieDataset dataset, Comparable key) {
                        String result = null;
                        double valueTotal = 0;
                        for (int i = 0; i < dataset.getItemCount(); i++) {
                            valueTotal += dataset.getValue(i).doubleValue();
                        }

                        if (isChartDataVisibleInLegende(key) && isChartDataVisible(key)) {
                            result = key.toString() + "\n" + dataset.getValue(key) + " (" + Math.round(dataset.getValue(key).doubleValue() / valueTotal * 100d) + " %)"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                        }
                        return result;
                    }
                });
            } else {
                plot.setLabelGenerator(null);
            }
        }



        /**
         * Fixe l'angle de d�part des donn�es du chart (si le chart en poss�de
         * un)
         */
        @Override
        public void doChangeStartAngle() {
            PiePlot plot = (PiePlot) getChart().getPlot();
            Double startAngle = getChartConstraints().getStartAngle();

            if (startAngle != null) {
                plot.setStartAngle(startAngle);
            } else {
                plot.setStartAngle(PiePlot.DEFAULT_START_ANGLE);
            }
        }
    }

    /**
     * Entit� de donn�es propre � un BarChart
     * 
     * @author jeremy.scafi
     */
    private class PieChartData extends ChartData {

        private int indChartData;
        private Comparable idValue;
        private Number value;



        public PieChartData(Comparable idChartData) {
            super(idChartData, getChartDataCount());
            this.indChartData = getChartDataCount();
        }



        public void addValue(Comparable idValue, Number value) {
            this.idValue = idValue;
            this.value = value;
            dataset.insertValue(indChartData, idValue, value);

            // Fixe les couleurs des donn�es du chart
            updateColorsChartData();

            // Fixe les formes des donn�es du chart
            updateShapesChartData();

            // Si la chartdata n'est pas visible, on cache la valeur ajout�e
            if (!isChartDataVisible(getIdChartData())) {
                dataset.setValue(idValue, 0);
            }
        }



        protected void hideAllValues() {
            dataset.setValue(idValue, 0);
        }



        protected void showAllValues() {
            dataset.setValue(idValue, value);
        }
    }

    /**
     * Gestion de la s�lection d'une part de camembert dans le graphe
     * 
     * @author JayJay
     * 
     */
    protected class PieSelectionRenderer implements ChartSelection {

        private Integer selectedSeries = null;
        private Integer selectedData = null;
        private PieDataset dataSet = null;



        public PieSelectionRenderer(PieDataset dataSet) {
            this.dataSet = dataSet;
        }



        public boolean isSelectionEnabled(ChartEntity entity) {
            if (entity instanceof PieSectionEntity) {
                return ((PieSectionEntity) entity).getDataset().equals(dataSet);
            }
            return false;
        }



        public boolean isSelectionActive() {
            return selectedSeries != null && selectedData != null;
        }



        public Integer getSelectedSeries() {
            return selectedSeries;
        }



        public Integer getSelectedData() {
            return selectedData;
        }



        public void setSelectedSeries(Integer indSeries) {
            this.selectedSeries = indSeries;
            updateColorSections();
        }



        public void setSelectedData(Integer indData) {
            this.selectedData = indData;
            updateColorSections();
        }



        public void clearSelection() {
            this.selectedSeries = null;
            this.selectedData = null;
            updateColorSections();
        }
    }
}
