package com.dolphland.client.util.swingverifiers;

import javax.swing.InputVerifier;

/**
 * Classe de base
 * 
 * 
 * @author Bernard Noel
 */
public abstract class AbstractInputVerifier extends InputVerifier {

    private Afficheur afficheur = null;



    public AbstractInputVerifier(Afficheur afficheur) {
        this.afficheur = afficheur;
    }



    public Afficheur getAfficheur() {
        return afficheur;
    }



    protected void afficherErreur(String message) {
        if (getAfficheur() != null) {
            getAfficheur().erreur(message);
        }
    }



    protected void afficherInfo(String message) {
        if (getAfficheur() != null) {
            getAfficheur().info(message);
        }
    }

}
