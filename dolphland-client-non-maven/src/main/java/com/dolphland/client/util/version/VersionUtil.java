package com.dolphland.client.util.version;

import com.dolphland.client.util.conf.FwkServiceLocator;
import com.dolphland.core.ws.data.ApplicationId;

/**
 * Version util
 * 
 * @author JayJay
 * 
 */
public class VersionUtil {

    /**
     * Get dolphland's version
     * 
     * @return Dolphland's version
     */
    public final static String getVersion() {
        return FwkServiceLocator.getInstance().getEnvironment().getApplication().getVersion();
    }



    /**
     * Get application's version
     * 
     * @param applicationId
     *            Application
     * @return Application's version
     */
    public final static String getVersion(ApplicationId applicationId) {
        return getVersion(applicationId.toString());
    }



    /**
     * Get application's version
     * 
     * @param applicationId
     *            Application
     * @return Application's version
     */
    private final static String getVersion(String applicationId) {
        try {
            return FwkServiceLocator.getInstance().getEnvironment().getConf("version").getProperty(applicationId).getString();
        } catch (Exception e) {
            return "";
        }
    }
}
