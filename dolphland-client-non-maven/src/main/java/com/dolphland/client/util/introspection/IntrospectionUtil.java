package com.dolphland.client.util.introspection;

import java.lang.reflect.Method;
import java.util.Collection;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * <p>
 * Utils to do introspection
 * </p>
 * 
 * @author Benoit Vatan
 */
public class IntrospectionUtil {

    private static ClassLoader MY_CL = IntrospectionUtil.class.getClassLoader();



    /**
     * <p>
     * Loads the specified class from the classloader that loaded this class.
     * </p>
     * 
     * @param name
     *            Full name of the class to be loaded.
     * @return The loaded class or null if not found or an error has occured
     */
    public static Class<?> loadClass(String name) {
        Class<?> out = null;
        try {
            out = MY_CL.loadClass(name);
        } catch (Exception ex) {
            out = null;
        }
        return out;
    }



    /**
     * <p>
     * Creates a new instance the the specified class name and return that
     * instance.
     * </p>
     * 
     * @param name
     *            The name of the class from which the instance is to be create.
     * @return The created instance or null if some error occured.
     */
    @SuppressWarnings("unchecked")//$NON-NLS-1$
    public static <T> T newInstance(String name) {
        T out = null;
        try {
            Class<?> clazz = loadClass(name);
            if (clazz != null) {
                out = (T) clazz.newInstance();
            }
        } catch (ClassCastException ex) {
            throw ex;
        } catch (Exception ex) {
            out = null;
        }
        return out;
    }



    /**
     * <p>
     * Invoke a method on the specfied class with the specified parameters. Note
     * that parameter types are determined from the getClass() method on each
     * parameters. Thus the looked up method signature must be of the exact same
     * parameter types otherwise the invokation will fail.
     * </p>
     * 
     * @param clazz
     *            The class on which to invoke the specified method
     * @param method
     *            The method name to be invoked
     * @param args
     *            The method arguments to be passed to the target method (beware
     *            of the types)
     * @return The object returned byt the invoked method
     */
    public static Object invoke(Class<?> clazz, String method, Object... args) {
        Object out = null;
        try {
            Class<?>[] parameterTypes = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                parameterTypes[i] = args[i].getClass();
            }
            Method m = clazz.getMethod(method, parameterTypes);
            out = m.invoke(null, args);
        } catch (Exception ex) {
            out = null;
        }
        return out;
    }



    /**
     * <p>
     * Same as the other invoke method but targets an instance instead of a
     * static method.
     * </p>
     * 
     * @param target
     *            The target instance on which to invoke the specified method
     * @param method
     *            The method name to be invoked.
     * @param args
     *            The parameters to be passed to the specified method (beware of
     *            types)
     * @return The object returned by the invoked method or null if an error
     *         occured
     */
    public static Object invoke(Object target, String method, Object... args) {
        Object out = null;
        try {
            Class<?>[] parameterTypes = new Class[args.length];
            for (int i = 0; i < args.length; i++) {
                parameterTypes[i] = args[i].getClass();
            }
            Method m = target.getClass().getMethod(method, parameterTypes);
            out = m.invoke(target, args);
        } catch (Exception ex) {
            out = null;
        }
        return out;
    }



    /**
     * Return String value from an object
     * 
     * @param obj
     *            Object
     * 
     * @return String value
     */
    public static String toString(Object obj) {
        return ToStringBuilder.reflectionToString(obj, new RecursiveToStringStyle());
    }



    /**
     * <p>
     * Return String value from an object with max depth for recursion.
     * <p>
     * Setting {@link #maxDepth} to 0 will have the same effect as using
     * original {@link #ToStringStyle}: it will print all 1st level values
     * without traversing into them. Setting to 1 will traverse up to 2nd level
     * and so on.
     * 
     * @param obj
     *            Object
     * @param maxDepth
     *            Max depth for recursion
     * 
     * @return String value
     */
    public static String toString(Object obj, int maxDepth) {
        return ToStringBuilder.reflectionToString(obj, new RecursiveToStringStyle(maxDepth));
    }

    /**
     * {@link ToStringStyle} used to convert an object to a string recursively
     * 
     * @author jeremy.scafi
     * 
     */
    private static class RecursiveToStringStyle extends ToStringStyle {

        private static final int INFINITE_DEPTH = -1;
        
        private static final String PRIMITIVE_PACKAGE = "java.lang.";

        private int maxDepth;

        private int depth;



        private RecursiveToStringStyle() {
            this(INFINITE_DEPTH);
        }



        private RecursiveToStringStyle(int maxDepth) {
            setUseShortClassName(true);
            setUseIdentityHashCode(false);
            this.maxDepth = maxDepth;
        }



        @Override
        protected void appendDetail(StringBuffer buffer, String fieldName, Object value) {
            if (value.getClass().getName().startsWith(PRIMITIVE_PACKAGE)) {
                // If primitive
                buffer.append(value);
            } else if (value.getClass() instanceof Class && ((Class<?>) value.getClass()).isEnum()) {
                // If enumeration
                buffer.append(value);
            } else if (maxDepth != INFINITE_DEPTH && depth >= maxDepth) {
                // If out of max depth
                buffer.append(value);
            } else {
                depth++;
                buffer.append(ReflectionToStringBuilder.toString(value, this));
                depth--;
            }
        }



        @Override
        protected void appendDetail(StringBuffer buffer, String fieldName, @SuppressWarnings("rawtypes") Collection coll) {
            depth++;
            buffer.append(ReflectionToStringBuilder.toString(coll.toArray(), this, true, true));
            depth--;
        }
    }
}
