package com.dolphland.client.util.application.startup;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.application.components.FwkMasterFrame;
import com.dolphland.client.util.conf.FwkBoot;
import com.dolphland.client.util.conf.FwkBootStep;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.transaction.SwingJob;

public class MasterFrameBootStep extends FwkBootStep {

    private final static Logger log = Logger.getLogger(MasterFrameBootStep.class);

    private static final MessageFormater FMT = MessageFormater.getFormater(MasterFrameBootStep.class);

    private volatile MasterFrame masterFrame;

    private MasterFrameFactory masterFrameFactory;



    public MasterFrameBootStep(MasterFrameFactory masterFrameFactory) {
        super(FMT.format("MasterFrameBootStep.RS_NAME"));
        Assert.notNull(masterFrameFactory, "masterFrameFactory cannot be null !"); //$NON-NLS-1$
        this.masterFrameFactory = masterFrameFactory;
    }



    @Override
    protected void doExecute(FwkBoot booter) {
        SwingJob<MasterFrame> job = new SwingJob<MasterFrame>() {

            @Override
            protected MasterFrame run() {
                UIApplicationContext ctx = new UIApplicationContext();
                ctx.setBootContext(getBootContext());
                MasterFrame frame = null;
                try {
                    frame = masterFrameFactory.createMasterFrame(ctx);
                    if (frame == null) {
                        log.warn("Master frame failed to be created ! Null master frame provided."); //$NON-NLS-1$
                    } else {
                        frame.init(getBootContext().getArgs());
                    }
                } catch (Exception e) {
                    frame = null;
                    log.warn("Master frame failed to be created !", e); //$NON-NLS-1$
                }
                return frame;
            }
        };

        this.masterFrame = job.invokeAndWait();
    }



    /**
     * <p>
     * Get the {@link MasterFrame} loaded by this step or null is none ios
     * available.
     * </p>
     * 
     * @return The loaded {@link MasterFrame} from the bean context
     */
    public MasterFrame getMasterFrame() {
        return masterFrame;
    }



    /**
     * <p>
     * If a {@link MasterFrame} could be loaded by this step then the following
     * action take place:<br>
     * <li>The frame is set to its MAXIMIZED extended state</li>
     * </li></li>
     * </p>
     * 
     */
    public void show() {
        if (masterFrame != null) {
            masterFrame.setExtendedState(FwkMasterFrame.MAXIMIZED_BOTH);
            masterFrame.setVisible(true);
        }
    }
}
