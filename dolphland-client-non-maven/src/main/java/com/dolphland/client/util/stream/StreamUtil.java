package com.dolphland.client.util.stream;

import java.io.Closeable;

/**
 * Stream util
 * 
 * @author JayJay
 * 
 */
public class StreamUtil {

    /**
     * Close silently the stream
     * 
     * @param stream
     *            Stream to close silently
     */
    public static void safeClose(Closeable stream) {
        try {
            stream.close();
        } catch (Exception e) {
            // ignore
        }
    }
}
