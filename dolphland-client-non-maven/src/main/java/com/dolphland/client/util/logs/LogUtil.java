package com.dolphland.client.util.logs;

import org.slf4j.LoggerFactory;

import com.dolphland.client.util.introspection.IntrospectionUtil;

/**
 * <p>
 * Provides fail safe services to install a local log file whose content
 * originates from log4j events.<br>
 * The install() method looks for the log4j library in the classpath and then
 * installs a local log file on the file system if log4j is found.
 * </p>
 * <p>
 * A new Appender is dynamically added the the log4j root logger. If for
 * whatever reason, an error occurs while installing the extra appender, then no
 * log file will be created and the setup will silently returns.
 * </p>
 * 
 * @author Benoit Vatan
 */
public class LogUtil {

    private static LogSystem currentLog4jSystem;



    /**
     * <p>
     * Get current log's system
     * </p>
     * 
     * @return Current log's system
     */
    public static LogSystem getCurrentLogSystem() {
        if (currentLog4jSystem == null) {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            if (cl.getResource("logback.xml") != null) { //$NON-NLS-1$
                currentLog4jSystem = LogSystem.LOGBACK;
            } else {
                currentLog4jSystem = LogSystem.OTHER;
            }
        }
        return currentLog4jSystem;
    }



    /**
     * <p>
     * Returns root logger
     * </p>
     * 
     * @return The root logger
     */
    public static final Object getRootLogger() {
        Object rootLogger = null;
        Class<?> loggerClazz = null;
        switch (getCurrentLogSystem()) {
            case LOGBACK :
                loggerClazz = IntrospectionUtil.loadClass("ch.qos.logback.classic.Logger"); //$NON-NLS-1$
                if (loggerClazz != null) {
                    rootLogger = LoggerFactory.getLogger("ROOT"); //$NON-NLS-1$
                }
                break;

            case OTHER :
            default :
                break;
        }
        return rootLogger;
    }



    /**
     * <p>
     * Returns a specific appender from a class
     * </p>
     * 
     * @param clazz
     *            Class
     * @param appenderName
     *            Appender's name
     * 
     * @return The specific appender from a class
     */
    public static final Object getAppender(Class<?> clazz, String appenderName) {
        Object logger = null;
        Class<?> loggerClazz = null;
        Object appender = null;
        switch (getCurrentLogSystem()) {
            case LOGBACK :
                loggerClazz = IntrospectionUtil.loadClass("ch.qos.logback.classic.Logger"); //$NON-NLS-1$
                if (loggerClazz != null) {
                    logger = LoggerFactory.getLogger(clazz);
                    if (logger != null) {
                        appender = IntrospectionUtil.invoke(logger, "getAppender", appenderName); //$NON-NLS-1$
                    }
                }
                break;

            case OTHER :
            default :
                break;
        }
        return appender;
    }
}
