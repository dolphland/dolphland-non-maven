package com.dolphland.client.util.chart;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;

/**
 * Cette classe permet de repr�senter un chart Camembert en utilisant l'API
 * JFreeChart avec une l�gende am�lior�e.
 * 
 * La principale am�lioration de cette l�gende est de pouvoir d�cocher certaines
 * s�ries pour ne pas les voir appara�tre sur le chart.
 * 
 * La repr�sentation du Chart se fait dans le JComponent componentChart que l'on
 * peut obtenir par getChartComponent()
 * 
 * Possibilit� d'y d�finir :
 * - des entit�s de donn�es. Une entit� de donn�e est une s�rie identifi�e par
 * un Comparable, constitu�e de valeurs (identit� valeur, ordonn�e)
 * 
 * La configuration du chart se fait � l'aide d'un ChartConstraint avec
 * possibilit� de passer par son builder (ChartConstraintBuilder)
 * 
 * Exemple concret de cr�ation d'un PieChart :
 * 
 * PieChart chart = new PieChart(new VisionPanel(),
 * ChartConstraintsBuilder.create()
 * .chartTitle("Camembert")
 * .get()
 * );
 * 
 * @author jeremy.scafi
 */
public class PieChart extends AbstractPiePlotChart {

    /**
     * Constructeur
     */
    public PieChart() {
        super();
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public PieChart(ChartConstraints chartConstraints) {
        super(chartConstraints);
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param chartConstraints
     */
    public PieChart(JPanel componentChart) {
        super(componentChart);
    }



    /**
     * Constructeur avec le component devant accueillir le chart et les
     * contraintes du chart
     * 
     * @param componentChart
     *            : Composant devant repr�senter le Chart
     * @param chartConstraints
     *            : Contraintes � appliquer au Chart
     */
    public PieChart(JPanel componentChart, ChartConstraints chartConstraints) {
        super(componentChart, chartConstraints);
    }



    /** Cr�e le chart */
    protected JFreeChart createChart() {
        boolean legend = false;
        boolean tooltips = false;
        boolean urls = false;

        JFreeChart chart = ChartFactory.createPieChart(getChartConstraints().getChartTitle(),
            getDataset(),
            legend,
            tooltips,
            urls);

        return chart;
    }
}
