package com.dolphland.client.util.exception;

/**
 * Exception for Business
 * 
 * @author JayJay
 * 
 */
public class AppBusinessException extends AppException {

    public AppBusinessException() {
        super();
    }



    public AppBusinessException(String pattern, Object... args) {
        super(pattern, args);
    }



    public AppBusinessException(String pattern, Throwable cause, Object... args) {
        super(pattern, cause, args);
    }



    public AppBusinessException(String message, Throwable cause) {
        super(message, cause);
    }



    public AppBusinessException(String message) {
        super(message);
    }



    public AppBusinessException(Throwable cause) {
        super(cause);
    }
}
