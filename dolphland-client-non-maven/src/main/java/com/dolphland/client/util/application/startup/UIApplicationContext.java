package com.dolphland.client.util.application.startup;

import com.dolphland.client.util.application.MasterFrame;
import com.dolphland.client.util.conf.BootContext;
import com.dolphland.client.util.conf.CEnvironement;
import com.dolphland.client.util.conf.FwkBootStep;


public class UIApplicationContext {

    private BootContext bootContext;
    
    private MasterFrame masterFrame;
    
    void setBootContext(BootContext ctx) {
        this.bootContext = ctx;
    }
    
    
    public BootContext getBootContext() {
        return bootContext;
    }


    void setMasterFrame(MasterFrame masterFrame) {
        this.masterFrame = masterFrame;
    }
    
    
    public MasterFrame getMasterFrame() {
        return masterFrame;
    }


    public void addBootStep(FwkBootStep step) {
        bootContext.getBooter().addBootStep(step);
    }


    public CEnvironement getEnvironment() {
        return bootContext.getBooter().getEnvironment();
    }

}
