package com.dolphland.client.util.application.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JPanel;

import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.UIHideCtxInfoEvt;
import com.dolphland.client.util.event.UIShowAbsoluteCtxInfoEvt;

/**
 * Panel permettant d'afficher un message
 * 
 * @author J�r�my Scafi
 */
class PanelMessage extends JPanel implements MouseListener {

	private String text;

	/**
	 * Constructeur sans param�tre
	 */
	public PanelMessage() {
		this("");
	}
	
	/**
	 * Constructeur avec pour seul param�tre le texte � afficher
	 * @param text Texte � afficher
	 */
	public PanelMessage(String text) {
		setOpaque(false);
		setForeground(new Color(55, 55, 55));
		setFont(new Font("Dialog", Font.BOLD, 120));
		setText(text);
		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				EDT.postEvent(new UIShowAbsoluteCtxInfoEvt(PanelMessage.this, getText(), e.getPoint(), e.getPoint()));
			}
		});
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	@Override
	public void setVisible(boolean visible) {
		if(!visible){
			EDT.postEvent(new UIHideCtxInfoEvt(this));
		}
		super.setVisible(visible);
	}
	
	public void setMouseTraversable(boolean mouseTraversable) {
		removeMouseListener(this);
		if(!mouseTraversable){
			addMouseListener(this);
		}
	}
	
	public void mouseClicked(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
}
