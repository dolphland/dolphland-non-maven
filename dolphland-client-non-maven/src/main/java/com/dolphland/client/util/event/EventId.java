/*
 * $Log: EventId.java,v $
 * Revision 1.7 2009/03/30 09:34:32 jscafi
 * - Suppression des warnings inutiles
 * Revision 1.6 2009/03/05 15:01:59 bvatan
 * - mise en d�su�tude
 * Revision 1.5 2009/02/27 16:35:47 bvatan
 * - l'id n'est plus utilis�
 * Revision 1.4 2009/02/19 08:35:14 bvatan
 * - javadoc javadoc
 * Revision 1.3 2009/01/30 15:10:52 bvatan
 * - Serializable
 * Revision 1.2 2007/08/29 10:24:45 bvatan
 * - le contructeur devient public pour permettre de sp�cifier un id (int) pour
 * pallier au manque de fiabilit� de la m�thode hashCode() de String
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.2 2007/03/08 09:40:01 bvatan
 * - les Eventid ne sont plus identifi�s que par un String
 * Revision 1.1 2007/03/02 08:41:08 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 */
package com.dolphland.client.util.event;

/**
 * Repr�sente un identifiant d'�v�nement
 * 
 * @author JayJay
 * @deprecated Utiliser EventChannel � la place
 */
@Deprecated
public class EventId extends EventDestination {

    @Deprecated
    public EventId() {
        super();
    }



    /**
     * @deprecated id est ignor�: �quivalent � EventId(String)
     */
    public EventId(int id, String rep) {
        super(rep);
    }



    @Deprecated
    public EventId(String rep) {
        super(rep);
    }

}
