package com.dolphland.client.util.chart;

/**
 * Interface to generate a tooltip for a chart
 * 
 * @author JayJay
 * 
 */
public interface ChartToolTipGenerator {

    /**
     * Generate tooltip for chart data id and indice data
     * 
     * @param idChartData
     *            Chart data id
     * @param indData
     *            Indice data
     * @return Tootip
     */
    public String generateToolTip(Comparable idChartData, int indData);
}
