/*
 * $Log: FwkBootListener.java,v $
 * Revision 1.4 2008/12/05 14:22:23 bvatan
 * - gestion desl isteners de boot
 */

package com.dolphland.client.util.conf;

/**
 * Listener de s�quence de boot du framework.<br>
 * <code>FwkBoot</code> �met des �v�nements au travers de cette interface lors
 * de la s�quence de boot du framework
 * 
 * @author JayJay
 */
public interface FwkBootListener {

    /**
     * Appel� au d�marrage de la s�quence de boot juste apr�s le chargement des
     * listeners de boot.<br>
     * Ce service est appel� avant le d�marrage du multicasteur d'�v�nements et
     * avant toutes les �tapes de d�marrage du framework.<br>
     * Seul l'instance de FwkBoot est disponible sur l'�v�nement
     * 
     * @param e
     *            L'�v�nement d�livr� par FwkBoot
     */
    public void bootStarted(FwkBootEvent e);



    /**
     * Appel� pendant la s�quence de boot apr�s que cette derni�re a charg�e la
     * configuration et s�lectionn� l'environement<br>
     * A ce stade, l'�v�bnement fournit l'environement s�lectionn� dans la
     * configuration charg�e.
     * 
     * @param e
     *            L'�v�nement d�livr� par FwkBoot
     */
    public void confLoaded(FwkBootEvent e);



    /**
     * Appel� apr�s le chargement des services. Fournit les m�mes donn�es que
     * confLoaded()
     * 
     * @param e
     *            L'�v�nement d�livr� par FwkBoot
     */
    public void servicesLoaded(FwkBootEvent e);



    /**
     * Appel� lorsque la s�quence de boot est termin�e, �tape de boot
     * suppl�mentaires incluses
     * 
     * @param e
     *            L'�v�nement d�livr� par FwkBoot
     */
    public void bootComplete(FwkBootEvent e);

}
