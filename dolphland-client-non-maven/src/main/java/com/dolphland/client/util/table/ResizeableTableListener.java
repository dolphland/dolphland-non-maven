package com.dolphland.client.util.table;

/**
 * <p>
 * Listener that is notified of {@link ResizeableExtendedTable} events
 * </p>
 * 
 * @author JayJay
 */
public class ResizeableTableListener {

    /**
     * <p>
     * Tiggered when one or more columns have been resized on the table
     * </p>
     */
    public void columnsResized(ResizeableTableEvent e) {

    }

}
