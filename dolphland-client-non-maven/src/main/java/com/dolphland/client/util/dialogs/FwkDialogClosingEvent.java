package com.dolphland.client.util.dialogs;

import org.springframework.util.Assert;

/**
 * <p>
 * Event provided to {@link FwkDialog#dialogClosing(FwkDialogClosingEvent)} and
 * {@link VisionDialogWorkPane#dialogClosing(FwkDialogClosingEvent)}
 * </p>
 * 
 * @author JayJay
 */
public class FwkDialogClosingEvent {

    private boolean vetoClosing = false;

    private FwkDialogCloseEvent closeEvent;



    public FwkDialogClosingEvent(FwkDialogCloseEvent evt) {
        Assert.notNull(evt);
        this.closeEvent = evt;
    }



    /**
     * <p>
     * Return the event type that made the dialog close.
     * </p>
     * 
     * @return The close event type.
     */
    public FwkDialogCloseEvent getCloseEvent() {
        return closeEvent;
    }



    /**
     * <p>
     * Set a veto for closing on this event. Calling this method with a true
     * parameter will prevent the dialog from closing.
     * </p>
     * 
     * @param vetoClosing
     *            true when the dialog should not close, false otherwise.
     */
    public void setVetoClosing(boolean vetoClosing) {
        this.vetoClosing = vetoClosing;
    }



    /**
     * <p>
     * Return the veto state of this event. See
     * {@link FwkDialogClosingEvent#vetoClosing(boolean)} for more details.
     * </p>
     * 
     * @return true is the dialog should <b>not</b> close and false otherwise.
     */
    public boolean isVetoClosing() {
        return vetoClosing;
    }

}
