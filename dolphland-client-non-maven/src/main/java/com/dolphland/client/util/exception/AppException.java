package com.dolphland.client.util.exception;

import java.util.Formatter;

/**
 * Exception
 * 
 * @author JayJay
 * 
 */
public class AppException extends RuntimeException {

    private Formatter formatter = new Formatter();

    private String message = "";



    public AppException() {
        super();
    }



    public AppException(String message) {
        super(message);
        this.message = message;
    }



    public AppException(Throwable cause) {
        super(cause);
    }



    public AppException(String message, Throwable cause) {
        this(message, cause, new Object[] {});
    }



    public AppException(String pattern, Object... args) {
        message = formatter.format(pattern, args).toString();
    }



    public AppException(String pattern, Throwable cause, Object... args) {
        super(cause);
        message = formatter.format(pattern, args).toString();
    }



    @Override
    public String getMessage() {
        return message;
    }
}
