package com.dolphland.client.util.action;

import java.awt.Component;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

import org.apache.log4j.Logger;

import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.transaction.MVCTx;

/**
 * Classe bas�e sur le syst�me d'Actions de Swing permettant d'ex�cuter une
 * proc�dure (MVCTx) � partir d'un ou plusieurs composants Swing.<br>
 * <br>
 * Typage:
 * <ul>
 * <li>P: Param�tre de l'action h�ritant de <code>UIActionEvt</code>
 * <li>T: R�sultat de l'action provenant de la m�thode <code>doExecute</code>
 * </ul>
 * <br>
 * Pr�conisations d'utilisation des actions (<code>UIAction</code> et d�riv�es): <br>
 * <ul>
 * <li>Cr�er les actions � l'int�rieur du sc�nario (classe interne) et non dans
 * une classe propre.<br>
 * Les vues sont cens�es ne manipuler que des <code>UIAction</code> fournies par
 * le sc�nario via un setter.
 * <li>Eviter dans la mesure du possible l'utilisation de variables dans les
 * actions � cause du multithreading
 * <li>D�clarer les �v�nements des actions en private package tout en suivant la
 * norme NomActionEvt
 * <li>Eviter dans la mesure du possible de faire des setEnabled � partir de la
 * vue.<br>
 * C'est au sc�nario de prendre cette d�cision et non � la vue.
 * </ul>
 * <br>
 * Exemple respectant les pr�conisations d'utilisation:<br>
 * <br>
 * <ol>
 * <li>Cr�ation de la classe �v�nement h�ritant de <code>UIActionEvt</code>, qui
 * contient tous les param�tres n�cessaires � l'action <br>
 * <code>
 * <pre>
 * class MonActionEvt extends UIActionEvt{
 * 	private String text;
 * 	public void setText(String text){
 * 		this.text = text;
 * 	}
 * 	public String getText(){
 * 		return text;
 * 	}
 * }
 * </pre>
 * </code>
 * 
 * <li>Dans la vue, d�claration puis installation de l'action via un setter
 * <code>
 * <pre>
 * UIAction&lt;MonActionEvt,?&gt; monAction ;
 *      
 * public void setMonAction(UIAction&lt;MonActionEvt,?&gt; monAction){
 * 	this.monAction = monAction;
 * 	monAction.install(btnMonAction); 
 * }
 * </pre>
 * </code>
 * 
 * <li>Dans la vue, ex�cution de l'action lors d'un �v�nement Swing avec pour
 * param�tre d'ex�cution l'�v�nement de l'action <code>
 * <pre>
 * JButton btnMonAction;
 *      
 * public JButton getBtnMonAction(){
 * 	if(btnMonAction == null){
 * 		btnMonAction = new JButton("Go Action");
 * 		btnMonAction.addActionListener(new ActionListener(){
 * 			public void actionPerformed(ActionEvent e) {
 * 				if(monAction != null){
 * 					MonActionEvt evt = new MonActionEvt();
 * 					evt.setText("Hello");
 * 					monAction.execute(evt);
 * 				}
 * 			}			
 * 		});
 * 	}
 * 	return btnMonAction;
 * }
 * </pre>
 * </code>
 * 
 * <li>Dans le sc�nario, cr�ation de la classe interne puis affectation de
 * l'action � la vue <code>
 * <pre> 
 * private class MonAction extends UIAction&lt;MonActionEvt,Boolean&gt;{
 * 	protected Boolean doExecute(MonActionEvt param) {			
 * 		log.info("Ex�cution de MonAction : " + param.getText());
 * 		return true;
 * 	}
 * }
 * 
 * view.setMonAction(new MonAction());
 * </pre>
 * </code>
 * </ol>
 * 
 * @author J�r�my Scafi
 * @author JayJay
 * 
 */
public class UIAction<P extends UIActionEvt, T> extends MVCTx<P, T> {

    private final static Logger log = Logger.getLogger(UIAction.class);

    // Liste des triggers possibles
    // S�quence � utiliser pour les triggers = 1 2 4 8 16 32...
    public final static int TRIGGER_CLICK = 1;

    public final static int TRIGGER_DBCLICK = 2;

    public final static int TRIGGER_OPTION = 4;

    public final static int TRIGGER_MENU = 8;

    public final static int TRIGGER_CONTEXT = 16;

    public final static int TRIGGER_RIGHT_CLICK = 32;

    public final static int TRIGGER_RIGHT_DBCLICK = 64;

    private List<Component> components = new ArrayList<Component>();

    private String name;

    private Icon icon;

    private String description;

    private boolean isEnabled;

    private boolean isVisible;

    private int triggerModifier;

    private List<UIActionListener> actionsListeners;

    // List of disabled causes
    private List<String> disabledCauses;



    /**
     * Constructeur sans param�tre
     */
    public UIAction() {
        this(null, null);
    }



    /**
     * Constructeur o� l'on sp�cifie le nom
     * 
     * @param name
     *            Nom de l'action
     */
    public UIAction(String name) {
        this(name, null);
    }



    /**
     * Constructeur o� l'on sp�cifie un nom et une ic�ne
     * 
     * @param name
     *            Nom de l'action
     * @param icon
     *            Ic�ne de l'action
     */
    public UIAction(String name, Icon icon) {
        super();
        disabledCauses = new ArrayList<String>();
        actionsListeners = new ArrayList<UIActionListener>();
        setName(name);
        setIcon(icon);
        setVisible(true);
        setEnabled(true);
        setTriggerModifier(TRIGGER_CLICK);
    }



    /**
     * Service appel� par le framework lorsque le contexte de l'action a chang�
     * 
     * @param ctx
     *            Le nouveau contexte de l'action
     */
    public void contextChanged(P ctx) {

    }



    /**
     * Add action listener
     * 
     * @param listener
     *            Action listener
     */
    public synchronized void addUIActionListener(UIActionListener listener) {
        if (listener == null) {
            return;
        }
        actionsListeners.add(listener);
        super.addListener(listener);
    }



    /**
     * Remove action listener
     * 
     * @param listener
     *            Action listener
     */
    public synchronized void removeUIActionListener(UIActionListener listener) {
        if (listener == null) {
            return;
        }
        actionsListeners.remove(listener);
        super.removeListener(listener);
    }



    /**
     * Get action listeners
     * 
     * @return Action listeners
     */
    public synchronized UIActionListener[] getUIActionListeners() {
        UIActionListener[] result = new UIActionListener[actionsListeners.size()];
        actionsListeners.toArray(result);
        return result;
    }



    @Override
    public void execute(P param) {
        // If action not visible, can't be executed
        if (!UIActionUtil.isActionVisible(this, param)) {
            return;
        }
        // If action not enabled, can't be executed
        if (!UIActionUtil.isActionEnabled(this, param)) {
            return;
        }

        super.execute(param);
    }



    /**
     * D�sinstalle l'action du composant sp�cifi�
     * 
     * @deprecated Use unbind in VisionView or ActionsContext
     */
    public void uninstall(Component component) {
        components.remove(component);
    }



    /**
     * Installe l'action dans un composant
     * 
     * @deprecated Use bind in VisionView or ActionsContext
     */
    public void install(Component component) {
        if (!components.contains(component)) {
            try {
                // Si l'action a un nom, on fixe le texte du composant
                if (!StrUtil.isEmpty(getName())) {
                    Method methSetText = component.getClass().getMethod("setText", String.class); //$NON-NLS-1$
                    methSetText.invoke(component, getName());
                }
            } catch (NoSuchMethodError e) {
                log.warn("install(): setText on component", e); //$NON-NLS-1$
            } catch (Exception e) {
                log.warn("install(): Error while setting text on component", e); //$NON-NLS-1$
            }

            try {
                // Si l'action a une description, on tente de fixer le texte
                // tooltip du composant
                if (!StrUtil.isEmpty(getDescription())) {
                    Method methSetText = component.getClass().getMethod("setToolTipText", String.class); //$NON-NLS-1$
                    methSetText.invoke(component, getDescription());
                }
            } catch (NoSuchMethodError e) {
                log.warn("install(): setToolTipText on component", e); //$NON-NLS-1$
            } catch (Exception e) {
                log.warn("install(): Error while setting ToolTipText on component", e); //$NON-NLS-1$
            }

            try {
                // Si l'action a une ic�ne, on fixe l'ic�ne du composant
                if (getIcon() != null) {
                    Method methSetText = component.getClass().getMethod("setIcon", Icon.class); //$NON-NLS-1$
                    methSetText.invoke(component, getIcon());
                }
            } catch (NoSuchMethodError e) {
                log.warn("updateIcon(): setIcon sur le composant", e); //$NON-NLS-1$
            } catch (Exception e) {
                log.warn("updateIcon(): Error while setting Icon on component", e); //$NON-NLS-1$
            }

            // Ajout du composant
            components.add(component);

            component.setEnabled(isEnabled());
        }
    }



    /**
     * Indique si l'action est activep our le contexte sp�cifi�
     * 
     * @param ctx
     *            Le contexte dans lequel l'action doit tester si elle est
     *            active ou non.
     * @return true si l'action est activep our le contexte fourni, false sinon.
     */
    public boolean isEnabled(P ctx) {
        return true;
    }



    /** Indique si l'action est active ou non */
    public boolean isEnabled() {
        return isEnabled;
    }



    /** Fixe si l'action est active ou non */
    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;

        updateEnabled();
    }



    /**
     * M�thode appel�e lorsque l'action est activ�e/d�sactiv�e pour
     * activer/d�sactiver les composants o� elle est install�e
     */
    public void updateEnabled() {
        // Parcourt des composants li�s par l'action pour mettre � jour leur
        // �tat
        for (Component component : components) {
            component.setEnabled(isEnabled());
        }

        // For each action listener, prevent that enabled has changed
        for (UIActionListener listener : getUIActionListeners()) {
            try {
                listener.enabledChanged(isEnabled());
            } catch (Exception e) {
                log.error("updateEnabled() : enabledChanged has thrown an exception !", e); //$NON-NLS-1$
            }
        }
    }



    /**
     * Retourne true si l'action est activ�e pour le contexte sp�cifi�e, false
     * sinon.
     */
    public boolean isVisible(P ctx) {
        return true;
    }



    /** Indique si l'action est visible ou non */
    public boolean isVisible() {
        return isVisible;
    }



    /** Fixe si l'action est visible ou non */
    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;

        updateVisible();
    }



    /**
     * M�thode appel�e lorsque l'action est visible/invisible pour rendre
     * visible/invisible les composants o� elle est install�e
     */
    public void updateVisible() {
        // Parcourt des composants li�s par l'action pour mettre � jour leur
        // �tat
        for (Component component : components) {
            component.setVisible(isVisible());
        }

        // For each action listener, prevent that visible has changed
        for (UIActionListener listener : getUIActionListeners()) {
            try {
                listener.visibleChanged(isVisible());
            } catch (Exception e) {
                log.error("updateVisible() : visibleChanged has thrown an exception !", e); //$NON-NLS-1$
            }
        }
    }



    /** Renvoie le nom de l'action */
    public String getName() {
        return name;
    }



    /** Fixe le nom de l'action */
    public void setName(String name) {
        this.name = name;
        updateName();
    }



    /**
     * M�thode appel�e lorsque le nom de l'action est chang� pour changer le nom
     * des composants o� elle est install�e
     */
    public void updateName() {
        // Parcourt des composants li�s par l'action pour mettre � jour leur
        // �tat
        for (Component component : components) {
            try {
                Method methSetText = component.getClass().getMethod("setText", String.class); //$NON-NLS-1$
                methSetText.invoke(component, getName());
            } catch (NoSuchMethodError e) {
                log.warn("updateName(): setText on component", e); //$NON-NLS-1$
            } catch (Exception e) {
                log.warn("updateName(): Error while setting text on component", e); //$NON-NLS-1$
            }
        }

        // For each action listener, prevent that name has changed
        for (UIActionListener listener : getUIActionListeners()) {
            try {
                listener.nameChanged(getName());
            } catch (Exception e) {
                log.error("updateName() : nameChanged has thrown an exception !", e); //$NON-NLS-1$
            }
        }
    }



    /** Renvoie l'ic�ne de l'action */
    public Icon getIcon() {
        return icon;
    }



    /** Fixe l'ic�ne de l'action */
    public void setIcon(Icon icon) {
        this.icon = icon;
        updateIcon();
    }



    /**
     * M�thode appel�e lorsque l'ic�ne de l'action est chang�e pour changer
     * l'ic�ne des composants o� elle est install�e
     */
    public void updateIcon() {
        // Parcourt des composants li�s par l'action pour mettre � jour leur
        // �tat
        for (Component component : components) {
            try {
                Method methSetText = component.getClass().getMethod("setIcon", Icon.class); //$NON-NLS-1$
                methSetText.invoke(component, getIcon());
            } catch (NoSuchMethodError e) {
                log.warn("updateIcon(): setIcon on component", e); //$NON-NLS-1$
            } catch (Exception e) {
                log.warn("updateIcon(): Error while setting Icon on component", e); //$NON-NLS-1$
            }
        }

        // For each action listener, prevent that icon has changed
        for (UIActionListener listener : getUIActionListeners()) {
            try {
                listener.iconChanged(getIcon());
            } catch (Exception e) {
                log.error("updateIcon() : iconChanged has thrown an exception !", e); //$NON-NLS-1$
            }
        }
    }



    /** Renvoie la description */
    public String getDescription() {
        return description;
    }



    /** Fixe la description */
    public void setDescription(String description) {
        this.description = description;
        updateDescription();
    }



    /**
     * M�thode appel�e lorsque la description de l'action est chang�e pour
     * changer la description des composants o� elle est install�e
     */
    public void updateDescription() {
        // Parcourt des composants li�s par l'action pour mettre � jour leur
        // �tat
        for (Component component : components) {
            try {
                Method methSetText = component.getClass().getMethod("setToolTipText", String.class); //$NON-NLS-1$
                methSetText.invoke(component, getDescription());
            } catch (NoSuchMethodError e) {
                log.warn("updateDescription(): setToolTipText on component", e); //$NON-NLS-1$
            } catch (Exception e) {
                log.warn("updateDescription(): Error while setting ToolTipText on component", e); //$NON-NLS-1$
            }
        }

        // For each action listener, prevent that description has changed
        for (UIActionListener listener : getUIActionListeners()) {
            try {
                listener.descriptionChanged(getDescription());
            } catch (Exception e) {
                log.error("updateDescription() : descriptionChanged has thrown an exception !", e); //$NON-NLS-1$
            }
        }
    }



    /**
     * Performed when disabled causes have changed
     */
    public void updateDisabledCauses() {
        // For each action listener, prevent that disabled causes have changed
        for (UIActionListener listener : getUIActionListeners()) {
            try {
                listener.disabledCausesChanged(getDisabledCauses());
            } catch (Exception e) {
                log.error("updateDisabledCauses() : disabledCausesChanged has thrown an exception !", e); //$NON-NLS-1$
            }
        }
    }



    /**
     * Renvoie le mask de trigger
     */
    public int getTriggerModifier() {
        return triggerModifier;
    }



    /**
     * Fixe le mask de trigger
     * 
     * Ex. : setTrigger(TRIGGER_CLICK | TRIGGER_DBCLICK);
     */
    public void setTriggerModifier(int triggerModifier) {
        this.triggerModifier = triggerModifier;
    }



    /**
     * Indique si le trigger est actif
     */
    public boolean isTriggerEnabled(int trigger) {
        return (trigger & getTriggerModifier()) == trigger;
    }



    /**
     * Fournit la liste des composants install�s sur l'action
     */
    public List<Component> getComponents() {
        return new ArrayList<Component>(components);
    }



    /**
     * Get all disabled causes
     * 
     * @return Disabled causes
     */
    public List<String> getDisabledCauses() {
        return new ArrayList<String>(disabledCauses);
    }



    /**
     * Add a disabled cause
     * 
     * @param disabledCause
     *            Disabled cause
     */
    public void addDisabledCause(String disabledCause) {
        this.disabledCauses.add(disabledCause);
        updateDisabledCauses();
    }



    /**
     * Add disabled causes
     * 
     * @param disabledCauses
     *            Disabled causes
     */
    public void addDisabledCauses(List<String> disabledCauses) {
        if (disabledCauses == null) {
            return;
        }
        this.disabledCauses.addAll(disabledCauses);
        updateDisabledCauses();
    }



    /**
     * Set disabled causes
     * 
     * @param disabledCauses
     *            Disabled causes
     */
    public void setDisabledCauses(List<String> disabledCauses) {
        this.disabledCauses.clear();
        if (disabledCauses != null) {
            this.disabledCauses.addAll(disabledCauses);
        }
        updateDisabledCauses();
    }



    /**
     * Clear disabled causes
     */
    public void clearDisabledCauses() {
        this.disabledCauses.clear();
        updateDisabledCauses();
    }
}
