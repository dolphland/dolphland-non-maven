/*
 * $Log: JobQueue.java,v $
 * Revision 1.2 2014/03/10 16:02:44 bvatan
 * - Internationalized
 * Revision 1.1 2007/04/06 15:34:50 bvatan
 * - d�placement du multicaster vers fwk-common
 * Revision 1.1 2007/03/02 08:41:06 bvatan
 * - renommage packages ctiv->vision
 * - renommage des EventListener->EventSubscriber
 * Revision 1.1 2007/01/26 13:58:49 bvatan
 * *** empty log message ***
 */
package com.dolphland.client.util.concurrent;

import java.util.LinkedList;

import org.apache.log4j.Logger;

/**
 * Impl�mente une file FIFO de t�ches en attente d'ex�cution � destination d'un
 * Executor.
 * 
 * @author JayJay
 */
public class JobQueue {

    private static final Logger log = Logger.getLogger(JobQueue.class);

    /**
     * Liste des t�ches en attente
     */
    private LinkedList queue;



    public JobQueue() {
        queue = new LinkedList();
    }



    /**
     * Ajoute un t�che en attente d'ex�cution
     * 
     * @param job
     *            La t�che � ex�cuter par un Executor
     */
    public void queueJob(Runnable job) {
        if (job == null) {
            log.warn("queueJob(): Added job is null: ignored"); //$NON-NLS-1$
            return;
        }
        synchronized (queue) {
            if (log.isDebugEnabled()) {
                log.debug("queueJob(): Added new Job in queue and noptifying executors."); //$NON-NLS-1$
            }
            queue.addLast(job);
            queue.notifyAll();
        }
    }



    /**
     * Retire et retourne la prochaine t�che a ex�cuter dans l'ordre d'insertion
     * (FIFO). Si aucune t�che a ex�cuter alors bloque jusqu'� ce qu'une t�che
     * soit disponible.
     * 
     * @return La t�che a ex�cuter.
     * @throws InterruptedException
     *             Si le thread en attente de t�che est interrompu.
     */
    public Runnable popJob() throws InterruptedException {
        Runnable out = null;
        synchronized (queue) {
            while (queue.size() == 0) {
                if (log.isDebugEnabled()) {
                    log.debug("popJob(): No task in queue, waiting..."); //$NON-NLS-1$
                }
                queue.wait();
            }
            out = (Runnable) queue.removeFirst();
            if (log.isDebugEnabled()) {
                log.debug("popJob(): Task got, " + queue.size() + " task(s) left and waiting."); //$NON-NLS-1$ //$NON-NLS-2$
            }
        }
        return out;
    }



    /**
     * Retourne le nombre de t�che en attente d'ex�cution dans la file.
     * 
     * @return Le nombre de t�che dans la file.
     */
    public int size() {
        synchronized (queue) {
            return queue.size();
        }
    }

}
