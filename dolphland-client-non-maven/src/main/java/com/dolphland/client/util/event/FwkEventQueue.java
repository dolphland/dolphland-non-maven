package com.dolphland.client.util.event;

import java.awt.AWTEvent;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.transaction.MVCTx;

public class FwkEventQueue extends EventQueue {

    private final static Logger log = Logger.getLogger(FwkEventQueue.class);

    private static FwkEventQueue instance = new FwkEventQueue();

    private Map<Key, KeyActionManager> keyMap;

    private TreeSet<KeyActionManager> listKeyActionManagerActifs;

    private List<KeyActionManager> listKeyActionManagerToStop;

    private FwkEventQueueInterceptor interceptor;

    private Map<KeySequence, KeySequenceWatcher> keySequences;



    private FwkEventQueue() {
        keySequences = new TreeMap<KeySequence, KeySequenceWatcher>();
        keyMap = new TreeMap<Key, KeyActionManager>();
        listKeyActionManagerActifs = new TreeSet<KeyActionManager>();
        listKeyActionManagerToStop = new ArrayList<KeyActionManager>();
        EDT.subscribe(new EventKeyRegistrationWatcher())
            .forEvent(RegisterKeyEvent.ID)
            .forEvent(DeregisterKeyEvent.ID)
            .forEvents(RegisterKeySequenceEvent.DEST)
            .forEvents(DeregisterKeySequenceEvent.DEST);
    }



    public static FwkEventQueue instance() {
        return instance;
    }



    public void install() {
        if (Toolkit.getDefaultToolkit().getSystemEventQueue() == instance) {
            return;
        }
        Toolkit.getDefaultToolkit().getSystemEventQueue().push(instance());
    }



    public void dispatchEvent(AWTEvent awte) {
        try {
            if (awte instanceof KeyEvent) {
                KeyEvent ke = (KeyEvent) awte;

                // On ne s'int�resse qu'aux �v�nements de pression et de
                // rel�chement de touches
                if (ke.getID() == KeyEvent.KEY_PRESSED || ke.getID() == KeyEvent.KEY_RELEASED) {

                    Key key = new Key(ke);
                    KeyActionManager keyActionManagerActif = keyMap.get(key);

                    listKeyActionManagerToStop.clear();
                    for (KeyActionManager keyActionManager : listKeyActionManagerActifs) {
                        // On stoppe l'action si la touche appuy�e est rel�ch�e
                        // alors qu'elle doit �tre press�e ou inversement
                        if (key.keyCode == keyActionManager.key.keyCode && key.keyId != keyActionManager.key.keyId) {
                            listKeyActionManagerToStop.add(keyActionManager);
                        }
                        // On stoppe l'action si le modifier courant ne contient
                        // plus le modifier de l'action
                        else if (!isModifierSet(key.keyModifier, keyActionManager.getKey().keyModifier)) {
                            listKeyActionManagerToStop.add(keyActionManager);
                        }
                    }

                    for (KeyActionManager keyActionManager : listKeyActionManagerToStop) {
                        try {
                            keyActionManager.stopAction();
                        } catch (Exception cause) {
                            log.error("dispatchEvent(): Error while stopping the action associated with the " + KeyEvent.getKeyText(ke.getKeyCode()) + " key!", cause); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                        listKeyActionManagerActifs.remove(keyActionManager);
                    }

                    if (keyActionManagerActif != null) {
                        try {
                            keyActionManagerActif.startAction();
                        } catch (Exception cause) {
                            log.error("dispatchEvent(): Error while executing the action associated with the " + KeyEvent.getKeyText(ke.getKeyCode()) + " key!", cause); //$NON-NLS-1$ //$NON-NLS-2$
                        }
                        listKeyActionManagerActifs.add(keyActionManagerActif);
                    }
                }
            }
        } finally {
            boolean shouldDispatch = beforeDispatch(awte);
            try {
                if (shouldDispatch) {
                    super.dispatchEvent(awte);
                }
            } catch (Throwable cause) {
                log.error("dispatch failed", cause); //$NON-NLS-1$
                dispatchFailed(awte, cause);
            } finally {
                if (shouldDispatch) {
                    afterDispatch(awte);
                }
            }
        }
    }



    /**
     * Installe l'interceptor sp�cifi� sur la queue d'�v�nement.<br>
     * Utiliser un interceptor null pour d�sinstaller l'interceptor en cours.
     * 
     * @param interceptor
     *            L'interceptor � instakller sur la queue ou null pour supprimer
     *            celui existant.
     */
    public void setInterceptor(FwkEventQueueInterceptor interceptor) {
        this.interceptor = interceptor;
    }



    private boolean beforeDispatch(AWTEvent e) {
        if (interceptor == null) {
            return true;
        }
        try {
            return interceptor.beforeDispatch(e);
        } catch (Throwable cause) {
            log.error("beforeDispatch(): FwkEventQueueInterceptor.beforeDispatch() thrown an error !", cause); //$NON-NLS-1$
        }
        return true;
    }



    private void afterDispatch(AWTEvent e) {
        if (interceptor == null) {
            return;
        }
        try {
            interceptor.afterDispatch(e);
        } catch (Throwable cause) {
            log.error("afterDispatch(): FwkEventQueueInterceptor.afterDispatch() thrown an error !", cause); //$NON-NLS-1$
        }
    }



    private void dispatchFailed(AWTEvent e, Throwable cause) {
        if (interceptor == null) {
            return;
        }
        try {
            interceptor.dispatchFailed(e, cause);
        } catch (Throwable c) {
            log.error("afterDispatch(): FwkEventQueueInterceptor.dispatchFailed() thrown an error !", c); //$NON-NLS-1$
        }
    }



    private boolean isModifierSet(int modifier, int mask) {
        return (mask & modifier) == mask;
    }



    private void executeAction(Object action) {
        if (action instanceof Runnable) {
            try {
                ((Runnable) action).run();
            } catch (Exception exc) {
                log.error("Error occuring while executing the action", exc); //$NON-NLS-1$
            }
        } else if (action instanceof UIAction) {
            ((UIAction<UIActionEvt, ?>) action).execute(new UIActionEvt());
        } else if (action instanceof MVCTx) {
            ((MVCTx<Object, ?>) action).execute();
        }
    }

    private class EventKeyRegistrationWatcher extends DefaultEventSubscriber {

        public void eventReceived(RegisterKeyEvent e) {
            Object action = e.getAction();
            if (action == null) {
                return;
            }
            // Get key action manager for the key corresponding to current
            // register key event
            KeyActionManager keyActionManager = keyMap.get(new Key(e));
            if (keyActionManager != null && !e.isReplaceExisting()) {
                // Add current register key event to existing key action manager
                // if replace existing indicator is off
                keyActionManager.addRegisterKeyEvent(e);
            } else {
                // Else create a new key action manager for current register key
                // event
                keyActionManager = new KeyActionManager(e);
                keyMap.put(keyActionManager.getKey(), keyActionManager);
            }
        }



        public void eventReceived(DeregisterKeyEvent e) {
            Object action = e.getAction();
            if (action == null) {
                return;
            }
            // For each key action manager
            for (KeyActionManager keyActionManager : new ArrayList<KeyActionManager>(keyMap.values())) {
                // For each register key event
                for (RegisterKeyEvent registerKeyEvent : new ArrayList<RegisterKeyEvent>(keyActionManager.getRegisterKeyEvents())) {
                    // Remove register key event if action is the current action
                    if (registerKeyEvent.getAction() == action) {
                        keyActionManager.removeRegisterKeyEvent(registerKeyEvent);
                    }
                }
                // Remove key action manager if no more register key events
                if (keyActionManager.getRegisterKeyEventsCount() == 0) {
                    keyMap.remove(keyActionManager.getKey());
                }
            }
        }



        public void eventReceived(RegisterKeySequenceEvent e) {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            KeySequence key = new KeySequence(e.getKeySequence());
            KeySequenceWatcher ksw = keySequences.get(key);
            if (ksw != null) {
                toolkit.removeAWTEventListener(ksw);
            }
            ksw = new KeySequenceWatcher(e.getKeySequence(), e.getEvent(), e.getEventDestination(), e.getDelay());
            toolkit.addAWTEventListener(ksw, AWTEvent.KEY_EVENT_MASK);
            keySequences.put(key, ksw);
        }



        public void eventReceived(DeregisterKeySequenceEvent e) {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            KeySequence key = new KeySequence(e.getKeySequence());
            KeySequenceWatcher ksw = keySequences.get(key);
            if (ksw != null) {
                toolkit.removeAWTEventListener(ksw);
            }
        }
    }

    private class Key implements Comparable<Key> {

        private int keyCode;

        private int keyModifier;

        private int keyId;



        Key(RegisterKeyEvent registerKeyEvent) {
            keyCode = registerKeyEvent.getKeyCode();
            keyId = registerKeyEvent.isKeyReleased() ? KeyEvent.KEY_RELEASED : KeyEvent.KEY_PRESSED;
            keyModifier = 0;

            if (registerKeyEvent.isMetaDown()) {
                keyModifier = keyModifier | KeyEvent.META_MASK | KeyEvent.META_DOWN_MASK;
            }
            if (registerKeyEvent.isCtrlDown()) {
                keyModifier = keyModifier | KeyEvent.CTRL_MASK | KeyEvent.CTRL_DOWN_MASK;
            }
            if (registerKeyEvent.isAltDown()) {
                keyModifier = keyModifier | KeyEvent.ALT_MASK | KeyEvent.ALT_DOWN_MASK;
            }
            if (registerKeyEvent.isShiftDown()) {
                keyModifier = keyModifier | KeyEvent.SHIFT_MASK | KeyEvent.SHIFT_DOWN_MASK;
            }
            if (registerKeyEvent.isAltGraphDown()) {
                keyModifier = keyModifier | KeyEvent.ALT_GRAPH_MASK | KeyEvent.ALT_GRAPH_DOWN_MASK;
            }
        }



        Key(KeyEvent keyEvent) {
            int modifiers = keyEvent.getModifiers();
            keyCode = keyEvent.getKeyCode();
            keyId = keyEvent.getID();
            keyModifier = 0;
            if (isModifierSet(modifiers, KeyEvent.META_MASK) || isModifierSet(modifiers, KeyEvent.META_DOWN_MASK)) {
                keyModifier = keyModifier | KeyEvent.META_MASK | KeyEvent.META_DOWN_MASK;
            }
            if (isModifierSet(modifiers, KeyEvent.CTRL_MASK) || isModifierSet(modifiers, KeyEvent.CTRL_DOWN_MASK)) {
                keyModifier = keyModifier | KeyEvent.CTRL_MASK | KeyEvent.CTRL_DOWN_MASK;
            }
            if (isModifierSet(modifiers, KeyEvent.ALT_MASK) || isModifierSet(modifiers, KeyEvent.ALT_DOWN_MASK)) {
                keyModifier = keyModifier | KeyEvent.ALT_MASK | KeyEvent.ALT_DOWN_MASK;
            }
            if (isModifierSet(modifiers, KeyEvent.SHIFT_MASK) || isModifierSet(modifiers, KeyEvent.SHIFT_DOWN_MASK)) {
                keyModifier = keyModifier | KeyEvent.SHIFT_MASK | KeyEvent.SHIFT_DOWN_MASK;
            }
            if (isModifierSet(modifiers, KeyEvent.ALT_GRAPH_MASK) || isModifierSet(modifiers, KeyEvent.ALT_GRAPH_DOWN_MASK)) {
                keyModifier = keyModifier | KeyEvent.ALT_GRAPH_MASK | KeyEvent.ALT_GRAPH_DOWN_MASK;
            }
        }



        public int compareTo(Key k) {
            int diffKeyCode = keyCode - k.keyCode;
            if (diffKeyCode != 0) {
                return diffKeyCode;
            } else {
                int diffKeyModifier = keyModifier - k.keyModifier;
                if (diffKeyModifier != 0) {
                    return diffKeyModifier;
                } else {
                    int diffKeyId = keyId - k.keyId;
                    if (diffKeyId != 0) {
                        return diffKeyId;
                    } else {
                        return 0;
                    }
                }
            }
        }



        @Override
        public boolean equals(Object obj) {
            Key other = (Key) obj;
            return keyCode == other.keyCode && keyModifier == other.keyModifier;
        }
    }

    private class KeyActionManager implements Comparable<KeyActionManager> {

        private List<RegisterKeyEvent> registerKeyEvents;

        private Key key;



        public KeyActionManager(RegisterKeyEvent registerKeyEvent) {
            this.registerKeyEvents = new ArrayList<RegisterKeyEvent>();
            this.addRegisterKeyEvent(registerKeyEvent);
            this.key = new Key(registerKeyEvent);
        }



        public void startAction() {
            // For each register key event
            for (RegisterKeyEvent registerKeyEvent : registerKeyEvents) {
                // Execute action if action should be executed
                if (shouldActionExecute(registerKeyEvent)) {
                    executeAction(registerKeyEvent.getAction());
                    registerKeyEvent.actionExecuted = true;
                }
            }
        }



        public void stopAction() {
            // For each register key event
            for (RegisterKeyEvent registerKeyEvent : registerKeyEvents) {
                // Execute post action if exists
                if (registerKeyEvent.getPostAction() != null) {
                    executeAction(registerKeyEvent.getPostAction());
                }
                registerKeyEvent.actionExecuted = false;
            }
        }



        /**
         * Get all register key events
         * 
         * @return Register key events
         */
        public List<RegisterKeyEvent> getRegisterKeyEvents() {
            return registerKeyEvents;
        }



        /**
         * Get the number of register key events
         * 
         * @return Number of register key events
         */
        public int getRegisterKeyEventsCount() {
            return registerKeyEvents.size();
        }



        /**
         * Add register key event
         * 
         * @param registerKeyEvent
         *            Register key event to add
         */
        public void addRegisterKeyEvent(RegisterKeyEvent registerKeyEvent) {
            this.registerKeyEvents.add(registerKeyEvent);
        }



        /**
         * Remove register key event
         * 
         * @param registerKeyEvent
         *            Register key event to remove
         */
        public void removeRegisterKeyEvent(RegisterKeyEvent registerKeyEvent) {
            this.registerKeyEvents.remove(registerKeyEvent);
        }



        public Key getKey() {
            return key;
        }



        /** En fonction des contraintes, indique si l'action doit �tre ex�cut�e */
        private boolean shouldActionExecute(RegisterKeyEvent registerKeyEvent) {
            boolean shouldActionInvoke = true;
            // Si l'action ne doit pas �tre r�p�t�e
            if (!registerKeyEvent.isRepeat()) {
                shouldActionInvoke = !registerKeyEvent.actionExecuted;
            }
            return shouldActionInvoke;
        }



        public int compareTo(KeyActionManager other) {
            return getKey().compareTo(other.getKey());
        }
    }

    private static class KeySequence implements Comparable<KeySequence> {

        private int[] sequence;



        public KeySequence(int[] sequence) {
            this.sequence = sequence;
        }



        @Override
        public boolean equals(Object obj) {
            KeySequence other = (KeySequence) obj;
            if (sequence.length != other.sequence.length) {
                return false;
            }
            for (int i = 0; i < sequence.length; i++) {
                if (sequence[i] != other.sequence[i]) {
                    return false;
                }
            }
            return true;
        }



        public int compareTo(KeySequence o) {
            int out = 0;
            int diff = 0;
            for (int i = 0; i < sequence.length && i < o.sequence.length; i++) {
                diff = sequence[i] - o.sequence[i];
                if (diff == 0) {
                    continue;
                }
                if (diff < 0) {
                    out = -1;
                    break;
                }
                if (diff > 0) {
                    out = 1;
                    break;
                }
            }
            if (diff == 0) {
                if (sequence.length < o.sequence.length) {
                    out = -1;
                } else if (sequence.length > o.sequence.length) {
                    out = 1;
                } else {
                    out = 0;
                }
            }
            return out;
        }



        @Override
        public int hashCode() {
            return sequence.hashCode();
        }
    }

    private static class KeySequenceWatcher implements AWTEventListener {

        private int[] keySequence;

        private int delay;

        private int i;

        private long startTime = 0;

        private Event event;

        private EventDestination destination;



        public KeySequenceWatcher(int[] keySequence, Event evt, EventDestination dest, int delaySec) {
            this.keySequence = keySequence;
            this.delay = delaySec * 1000;
            this.event = evt;
            this.destination = dest;
        }



        public void eventDispatched(AWTEvent event) {
            KeyEvent ke = (KeyEvent) event;
            if (ke.getID() != KeyEvent.KEY_PRESSED) {
                return;
            }
            if (i == 0) {
                startTime = System.currentTimeMillis();
            }
            if (System.currentTimeMillis() - startTime > delay) {
                i = 0;
                startTime = System.currentTimeMillis();
            }
            int kc = keySequence[i];
            // System.out.print(" "+i+"="+KeyEvent.getKeyText(ke.getKeyCode()));
            if (ke.getKeyCode() != kc) {
                i = 0;
                return;
            }
            i++;
            if (i == keySequence.length) {
                i = 0;
                EDT.postEvent(this.event, destination);
            }
        }
    }

}
