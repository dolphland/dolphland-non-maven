package com.dolphland.client.util.transaction;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.dolphland.client.util.exception.AppException;
import com.dolphland.client.util.service.AsyncInvoker;
import com.dolphland.client.util.service.ServiceResultListener;

/**
 * Classe de base pour la gestion des changements de threads dans les
 * transactions Swing.<br>
 * Cette classe permet d'effectuer les changement de threads n�cessaires au
 * respect des bonnes pratiques concernant l'utilisation des thread dans un
 * envoronnement Swing.
 * 
 * @author JayJay
 */
public abstract class MVCTransaction implements ServiceResultListener {

    private final static Logger log = Logger.getLogger(MVCTransaction.class);

    /** indicateur interne d'invocation du service updateViewError() */
    protected static final int INVOKE_UPDATEVIEW_ERROR = 1;

    /** indicateur interne d'invocation du service updateViewSuccess() */
    protected static final int INVOKE_UPDATEVIEW_SUCCESS = 2;

    /** indicateur interne d'invocation du service preExecute() */
    protected static final int INVOKE_PRE_EXECUTE = 3;

    /** indicateur interne d'invocation du service postExecute() */
    protected static final int INVOKE_POST_EXECUTE = 4;

    /** Exception lev�e suite � l'ex�cution de la transaction */
    protected AppException exception;

    /** Donn�es renvoy�es suite � l'ex�cution de la transaction */
    protected Object data;

    private static final AsyncInvoker invoker = new AsyncInvoker("MVCInvoker"); //$NON-NLS-1$

    static {
        invoker.start();
    }



    /**
     * Service appell� juste avant doExecute() dans le fil du thread AWT
     */
    protected void preExecute(Object param) {
        log.debug("preExecute(): not overloaded."); //$NON-NLS-1$
    }



    /**
     * Service appell� juste apr�s updateViewLater() updateViewSuccess() et
     * updateViewError() dans le fil du thread AWT.
     */
    protected void postExecute() {
        log.debug("postExecute(): not overloaded."); //$NON-NLS-1$
    }



    /**
     * M�thode d'ex�cution de la transaction, contient les traitements qui
     * d�marrent la transaction.
     * 
     * @param param
     *            Param�tre � transmettre � doExecute()
     */
    protected abstract void doExecute(Object param);



    /**
     * M�thode mise � jour de la vue. Cette m�thode est ex�cut� par l'EDT AWT.
     */
    protected void updateView() {
        log.debug("updateView(): not overridden."); //$NON-NLS-1$
    }



    protected void updateViewSuccess() {
        log.debug("updateViewSuccess(): not overridden."); //$NON-NLS-1$
    }



    protected void updateViewError() {
        log.debug("updateViewError(): not overridden."); //$NON-NLS-1$
    }



    /**
     * M�thode invoqu�e par la couche m�tier lorque les r�sultats de la
     * transaction sont diponibles. LOCK HELD
     * 
     * @param data
     *            Les donn�es r�sultat issues de la transaction
     */
    protected void results(Object data) {

    }



    /**
     * M�thode invoqu�e par la couche m�tier lorsque la demande de la
     * transaction a g�n�r� une erreur. LOCK HELD
     * 
     * @param e
     *            L'exception lev�e pendant la la demande.
     */
    protected void error(AppException e) {

    }



    public void execute() {
        execute(null);
    }



    /**
     * Ex�cute la transaction. Ex�cute le service doExecute() dans le thread
     * m�tier.
     */
    public void execute(final Object param) {
        Runnable job = new Runnable() {

            public void run() {
                try {
                    preExecute(param);
                } catch (Exception e) {
                    log.error("execute(): preExecute() has thrown an exception !", e); //$NON-NLS-1$
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            job.run();
        } else {
            invokeInEDT(INVOKE_PRE_EXECUTE, param);
        }
        invoker.invoke(new Runnable() {

            public void run() {
                log.debug("execute(): Execution of doExecute()"); //$NON-NLS-1$
                synchronized (this) {
                    try {
                        doExecute(param);
                    } catch (Exception e) {
                        log.error("execute(): doExecute has thrown an exeption !", e); //$NON-NLS-1$
                        setError(new AppException(e.getMessage(), e));
                    }
                }
            }
        });
    }



    /**
     * D�clenche l'ex�cution de updateView() dans l'EDT AWT
     */
    private final void updateViewLater() {
        Runnable job = new Runnable() {

            public void run() {
                log.debug("run(): Execution of updateView()"); //$NON-NLS-1$
                try {
                    updateView();
                } catch (Exception e) {
                    log.error("run(): updateView() has thrown an exception !", e); //$NON-NLS-1$
                }
            }
        };
        SwingUtilities.invokeLater(job);
    }



    /**
     * Invoque le service sp�cifi� dans le fil du Thread AWT
     * 
     * @param service
     *            incateur du service � invoquer parmis :<br>
     *            <li>INVOKE_UPDATEVIEW_ERROR <li>INVOKE_UPDATEVIEW_SUCCESS <li>
     *            INVOKE_PRE_EXECUTE <li>INVOKE_POST_EXECUTE
     * @param param
     *            param�tre a passer au service si besoin
     */
    protected void invokeInEDT(final int service, final Object param) {
        Runnable job = null;
        switch (service) {
            case INVOKE_UPDATEVIEW_SUCCESS :
                job = new Runnable() {

                    public void run() {
                        log.debug("run(): Execution of updateViewSuccess()"); //$NON-NLS-1$
                        try {
                            updateViewSuccess();
                        } catch (Exception e) {
                            log.error("updateViewLater(): updateViewSuccess() has thrown an exception !", e); //$NON-NLS-1$
                        }
                    }
                };
                break;
            case INVOKE_UPDATEVIEW_ERROR :
                job = new Runnable() {

                    public void run() {
                        log.debug("run(): Execution of updateViewError()"); //$NON-NLS-1$
                        try {
                            updateViewError();
                        } catch (Exception e) {
                            log.error("updateViewLater(): updateViewError() has thrown an exception !", e); //$NON-NLS-1$
                        }
                    }
                };
                break;
            case INVOKE_PRE_EXECUTE :
                job = new Runnable() {

                    public void run() {
                        log.debug("run(): Execution of preExecute()"); //$NON-NLS-1$
                        try {
                            preExecute(param);
                        } catch (Exception e) {
                            log.error("updateViewLater(): preExecute() has thrown an exception !", e); //$NON-NLS-1$
                        }
                    }
                };
                break;
            case INVOKE_POST_EXECUTE :
                job = new Runnable() {

                    public void run() {
                        log.debug("run(): Execution of postExecute()"); //$NON-NLS-1$
                        try {
                            postExecute();
                        } catch (Exception e) {
                            log.error("updateViewLater(): postExecute() has thrown an exception !", e); //$NON-NLS-1$
                        }
                    }
                };
                break;
            default :
                log.error("updateViewLater(): Unknown service ! [" + service + "]"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        if (job != null) {
            SwingUtilities.invokeLater(job);
        }
    }



    /*
     * D�clenche l'appel de error() en mode synchronis�
     */
    public final void setError(AppException ve) {
        try {
            synchronized (this) {
                this.exception = ve;
                this.data = null;
                error(ve);
            }
        } finally {
            synchronized (this) {
                updateViewLater();
                invokeInEDT(INVOKE_UPDATEVIEW_ERROR, null);
                invokeInEDT(INVOKE_POST_EXECUTE, null);
            }
        }
    }



    /*
     * D�clenche l'appel de results() en mode synchronis�
     */
    public final void setResult(Object results) {
        try {
            synchronized (this) {
                this.exception = null;
                this.data = results;
                results(results);
            }
        } finally {
            synchronized (this) {
                updateViewLater();
                invokeInEDT(INVOKE_UPDATEVIEW_SUCCESS, null);
                invokeInEDT(INVOKE_POST_EXECUTE, null);
            }
        }
    }
}
