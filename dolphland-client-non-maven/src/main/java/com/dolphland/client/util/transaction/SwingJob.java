/*
 * $Log: SwingJob.java,v $
 * Revision 1.4 2014/03/18 14:10:00 bvatan
 * - marked all non-nls strings
 * - translated french messages into english
 * - externalized translatable strings
 * Revision 1.3 2012/07/23 14:16:58 bvatan
 * - fixed exception propagation when job failed
 * Revision 1.2 2009/03/12 07:06:27 bvatan
 * - ajout traces de logs en debug
 * - gestion des exceptions plus fine
 * - suppression des blocs synchronis�s et rempalcement par des flush volatiles
 * Revision 1.1 2008/11/14 15:07:54 bvatan
 * - initial check in
 */

package com.dolphland.client.util.transaction;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import com.dolphland.client.util.exception.AppException;
import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * Utilitaire qui permet d'ex�cuter du code dans le fil du thread AWT et de
 * r�cup�rer un r�sultat. Ce m�canisme n'est pas possible avec les outils
 * fournis par le JDK.
 * 
 * @param <T>
 *            Type de l'objet que doit retourner la fraction de code � ex�cuter
 *            dans le thread AWT
 * 
 * @author JayJay
 */
public abstract class SwingJob<T> {

    private static final Logger log = Logger.getLogger(SwingJob.class);

    /** r�sultat du job */
    private volatile T result;

    /** erreur provoqu� par l'ex�cution du job */
    private volatile Throwable cause;



    /**
     * Ex�cute le job dans le tfil du thread AWT, attend que le code ait �t�
     * ex�cut� et retourne le r�sultat.<br>
     * En cas d'interruption du thread, retourn null et le thread est
     * r�interrompu.
     * 
     * @return Le r�sultat fourni par l'ex�cution du job dabns le thread AWT.
     */
    public T invokeAndWait() {
        try {
            if (SwingUtilities.isEventDispatchThread()) {
                try {
                    execute();
                } catch (AppException e) {
                    throw e;
                } catch (Exception e) {
                    throw new AppInfrastructureException(e);
                }
            } else {
                SwingUtilities.invokeAndWait(new Runnable() {

                    public void run() {
                        try {
                            execute();
                        } catch (AppException e) {
                            throw e;
                        } catch (Exception e) {
                            throw new AppInfrastructureException(e);
                        }
                    }
                });
            }
        } catch (InterruptedException ie) {
            if (Log.isDebugEnabled()) {
                log.debug("invokeAndWait(): The thread has been interrupted: return null !", ie); //$NON-NLS-1$
            }
            Thread.currentThread().interrupt();
            return null;
        } catch (InvocationTargetException ite) {
            Throwable cause = ite.getCause();
            log.error("invokeAndWait(): SwinJob has thrown an exception !", ite.getCause()); //$NON-NLS-1$
            if (cause instanceof AppException) {
                throw (RuntimeException) cause;
            } else {
                throw new AppInfrastructureException(cause.getMessage(), cause);
            }
        } catch (Exception e) {
            log.error("invokeAndWait(): Invoking SwingJob has failed", e); //$NON-NLS-1$
            throw new AppInfrastructureException("Invoking SwingJob has failed", e); //$NON-NLS-1$
        }
        return getResult();
    }



    /**
     * Ex�cute le job dans le fil du thread AWT sans attendre le r�sultat. Le
     * r�sultat sera disponible par l'appel de getResult() mais sans qu'on
     * puisse d�terminer quand.
     */
    public void invokeLater() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                try {
                    execute();
                } catch (Exception e) {
                    log.error("run(): Failed to execute SwingJob !", e); //$NON-NLS-1$
                }
            }
        });
    }



    // appel� depuis invokeAndWait() ou invokeLater()
    private void execute() throws Exception {
        T value = null;
        try {
            cause = null;
            log.debug("execute(): Executing SwingJob..."); //$NON-NLS-1$
            value = run();
            log.debug("execute(): Executing SwingJob successfull."); //$NON-NLS-1$
        } catch (Exception e) {
            cause = e;
            log.debug("execute(): Execution has failed !", e); //$NON-NLS-1$
            throw e;
        }
        result = value;
    }



    /**
     * Retourne le r�sultat de l'ex�cution du Job dans le fil du thread AWT
     * 
     * @return Le r�sultat fourni par le Job
     */
    public T getResult() {
        return result;
    }



    /**
     * Retourne la cause de l'erreur survenue lors de l'ex�cution du job.
     * 
     * @return Le throwable lev� lors de l'ex�cution ou null si pas d'erreur.
     */
    public Throwable getThrowable() {
        return cause;
    }



    /**
     * Le code � ex�cuter dans le fil du thread AWT
     * 
     * @return Le r�sultat de l'ex�cution
     */
    protected abstract T run();

}
