/*
 * $Log: ProductAdapter.java,v $
 * Revision 1.2 2009/08/07 12:15:09 bvatan
 * - getValue() retourne Object au lieu de String
 * - ajout setValue()
 * Revision 1.1 2009/02/04 13:38:39 jscafi
 * - Ajout de ProducTable permettant de g�rer des tables de produits
 * Revision 1.1 2009/01/06 14:17:04 bvatan
 * - ajotu gestion du blocage des barre par coul�e
 */

package com.dolphland.client.util.table;

/**
 * Repr�sentation d'un produit
 * 
 * @author jeremy.scafi
 * @author JayJay
 */
public abstract class ProductAdapter {

    /** Identifiant du produit */
    private String id;



    /**
     * Construit un produit avec son identifiant
     * 
     * @param id
     *            L'identitifiant uique du produit
     */
    public ProductAdapter(String id) {
        this.id = id;
    }



    /** Retourne la valeur de la propri�t� de produit donn�e (� d�finir) */
    public abstract Object getValue(ProductProperty pp);



    /** Fixe la valeur de la propri�t� de produit donn�e (� d�finir) */
    public abstract void setValue(ProductProperty pp, Object value);



    /**
     * Retourne l'identifiant du produit
     */
    public String getId() {
        return id;
    }
}
