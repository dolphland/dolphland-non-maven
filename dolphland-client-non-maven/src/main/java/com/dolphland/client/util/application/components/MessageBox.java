package com.dolphland.client.util.application.components;

import info.clearthought.layout.TableLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.StringTokenizer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicHTML;

import com.dolphland.client.util.dialogs.DialogUtil;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.layout.TableLayoutBuilder;
import com.dolphland.client.util.layout.TableLayoutConstraintsBuilder;
import com.dolphland.client.util.string.StrUtil;

public class MessageBox {

	private static final MessageFormater fmt = MessageFormater.getFormater(MessageBox.class);
	
	public static final int INFO_MESSAGE_TYPE = 0;

	public static final int ERROR_MESSAGE_TYPE = 1;

	public static final int WARN_MESSAGE_TYPE = 2;
	
	// Max length of a message row
	private static final int MAX_MESSAGE_ROW_LENGTH = 150;
	
	// Tolerance of max length of a message row
    private static final int MAX_TOLERANCE_MESSAGE_ROW_LENGTH = 20;

	private JLabel jlMessage = null;

	private Component parent = null;

	private FwkPanel panelMessage = null;
	
	private ImageIcon imageIcon = null;

	public MessageBox(Component parent) {
		this(parent, null);
	}

	public MessageBox(Component parent, ImageIcon imageIcon) {
	    super();
	    this.parent = parent;
	    this.imageIcon = imageIcon;
	}

	public void showMsgInfo(String msg) {
		setInfoMessage(msg);
		JOptionPane.showMessageDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_INFORMATION"), JOptionPane.INFORMATION_MESSAGE, getImageIcon()); //$NON-NLS-1$
	}

	public void showMsgError(String msg) {
		setErrorMessage(msg);
		JOptionPane.showMessageDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_ERROR"), JOptionPane.ERROR_MESSAGE, getImageIcon()); //$NON-NLS-1$
	}

	public void showMsgWarning(String msg) {
		setWarningMessage(msg);
		JOptionPane.showMessageDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_ALERT"), JOptionPane.WARNING_MESSAGE, getImageIcon()); //$NON-NLS-1$
	}

	public String showInputDialog(String msg) {
		setInfoMessage(msg);
		String ret = (String)JOptionPane.showInputDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_INPUT"), JOptionPane.QUESTION_MESSAGE, getImageIcon(), null, null); //$NON-NLS-1$

		return ret;
	}
	
	public String showPasswordDialog(String msg) {
		setInfoMessage(msg);
		
		String ret = DialogUtil.showPasswordDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_INPUT"), JOptionPane.QUESTION_MESSAGE, getImageIcon(), null); //$NON-NLS-1$
		return ret;
	}
	
	public String showPasswordDialog(String msg, String initialPasswordValue) {
		setInfoMessage(msg);
		
		String ret = DialogUtil.showPasswordDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_INPUT"), JOptionPane.QUESTION_MESSAGE, getImageIcon(), initialPasswordValue); //$NON-NLS-1$
		return ret;
	}

	public String showInputDialog(String msg, String initialInputValue) {
		setInfoMessage(msg);
		String ret = (String)JOptionPane.showInputDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_INPUT"), JOptionPane.QUESTION_MESSAGE, getImageIcon(), null, initialInputValue); //$NON-NLS-1$

		return ret;
	}

	public Object showInputDialog(String msg, Object[] elements) {
		setInfoMessage(msg);
		Object value = JOptionPane.showInputDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_INPUT"), JOptionPane.QUESTION_MESSAGE, getImageIcon(), elements, (elements!=null && elements.length>0)?elements[0]:null); //$NON-NLS-1$
		return value;
	}
	
	public boolean showMsgConfirmationInfo(String msg) {
		setInfoMessage(msg);
		int ret = JOptionPane.showConfirmDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_CONFIRM"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, getImageIcon()); //$NON-NLS-1$

		return ret == JOptionPane.YES_OPTION;
	}

	public boolean showMsgConfirmationAlert(String msg) {
		setWarningMessage(msg);
		int ret = JOptionPane.showConfirmDialog(parent, getPanelMessage(), fmt.format("MessageBox.RS_ALERT"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, getImageIcon()); //$NON-NLS-1$

		return ret == JOptionPane.YES_OPTION;
	}

	private void setInfoMessage(String message) {
		setMessage(message, INFO_MESSAGE_TYPE);
	}

	private void setErrorMessage(String message) {
		setMessage(message, ERROR_MESSAGE_TYPE);
	}

	private void setWarningMessage(String message) {
		setMessage(message, WARN_MESSAGE_TYPE);
	}

	private void setMessage(String message, int type) {
	    switch (type) {
			case INFO_MESSAGE_TYPE:
				setMessage(message, Theme.INFO_MESSAGE_FOREGROUND, Theme.INFO_MESSAGE_BACKGROUND, Theme.INFO_MESSAGE_FONT);
				break;
			case ERROR_MESSAGE_TYPE:
				setMessage(message, Theme.ERROR_MESSAGE_FOREGROUND, Theme.ERROR_MESSAGE_BACKGROUND, Theme.ERROR_MESSAGE_FONT);
				break;
			case WARN_MESSAGE_TYPE:
				setMessage(message, Theme.WARN_MESSAGE_FOREGROUND, Theme.WARN_MESSAGE_BACKGROUND, Theme.WARN_MESSAGE_FONT);
				break;
		}
	}

	private void setMessage(final String message, final Color foreground, final Color background, final Font font) {
		Runnable job = new Runnable() {
			public void run() {
                getlblMessage().setFont(font);
				if (!StrUtil.isEmpty(message)) {
					getlblMessage().setForeground(foreground);
					getPanelMessage().setBackground(background);
					getlblMessage().setText(formatMessage(message, MAX_MESSAGE_ROW_LENGTH, MAX_TOLERANCE_MESSAGE_ROW_LENGTH));
				} else {
					getlblMessage().setText(StrUtil.EMPTY_STRING);
					getPanelMessage().setBackground(Theme.DEFAULT_BACKGROUND);
				}
			}
		};
		if (SwingUtilities.isEventDispatchThread()) {
			job.run();
		} else {
			SwingUtilities.invokeLater(job);
		}

	}

    private FwkPanel getPanelMessage(){
        if(panelMessage == null){
            panelMessage = new FwkPanel();
            panelMessage.setTypeBorder(FwkPanel.TYPE_BORDER_ROUNDED_BORDER);
            
            TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
            
            panelMessage.setLayout(layoutBuilder.get());

            panelMessage.add(getlblMessage(), layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
            .topInsets(FwkView.DEFAULT_GAP)
            .height(TableLayout.PREFERRED)
            .bottomInsets(FwkView.DEFAULT_GAP)
            .leftInsets(FwkView.DEFAULT_GAP)
            .width(TableLayout.PREFERRED)
            .rightInsets(FwkView.DEFAULT_GAP)
            .get()));
        }
        
        return panelMessage;
    }
    
    private JLabel getlblMessage(){
        if(jlMessage == null){
            jlMessage = new JLabel();
        }
        return jlMessage;
    }



    /**
     * Format message specified<br>
     * <br>
     * If html message, no formatting<br>
     * <br>
     * If not html message, convert message to html where each row is under max
     * length row specified. Each row try wrap word in tolerance
     * (idx in [maxMessageRowLength - maxToleranceMessageRowLength ;
     * maxMessageRowLength]
     * <br>
     * @param message
     *            Message
     * @param maxMessageRowLength
     *            Max length of a message row
     * @param maxToleranceMessageRowLength
     *            Tolerance of max length of a message row
     * @return Message formatted
     */
    private String formatMessage(String message, int maxMessageRowLength, int maxToleranceMessageRowLength) {
        // If empty message
        if (StrUtil.isEmpty(message)) {
            return message;
        }

        // If html message
        if (BasicHTML.isHTMLString(message)) {
            return message;
        }

        String out = StrUtil.EMPTY_STRING;
        int rowLength = 0;

        // Limit message row to MAX_MESSAGE_ROW_LENGTH
        StringTokenizer strToken = new StringTokenizer(message, StrUtil.NEW_LINE);
        while (strToken.hasMoreTokens()) {
            String token = strToken.nextToken();

            // If out is not empty, add a backslash
            if (!StrUtil.isEmpty(out)) {
                out += StrUtil.NEW_LINE;
            }

            while (maxMessageRowLength <= token.length()) {
                // Try to wrap a word in tolerance
                // (idx in [maxMessageRowLength - maxToleranceMessageRowLength ; maxMessageRowLength]
                rowLength = -1;
                for (int idx = maxMessageRowLength - 1; idx >= maxMessageRowLength - maxToleranceMessageRowLength; idx--) {
                    // If a word is in tolerance, row length is its idx
                    if (token.charAt(idx) == ' ') {
                        rowLength = idx;
                        break;
                    }
                }

                // If no word is in tolerance, row length is maxMessageRowLength
                if (rowLength == -1) {
                    rowLength = maxMessageRowLength;
                }

                // Add row on out
                out += token.substring(0, rowLength);

                // Substract row on token
                token = token.substring(rowLength);

                // If rest of token is not empty, add a backslash
                if (!StrUtil.isEmpty(token)) {
                    out += StrUtil.NEW_LINE;
                }
            }

            // Add rest of token in out
            out += token;
        }

        // Convert message to html
        out = "<html>" + out.replaceAll("<", "&lt;").replace(">", "&gt;").replaceAll(StrUtil.NEW_LINE, "<br>") + "</html>"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$

        return out;
    }

    private ImageIcon getImageIcon(){
        return imageIcon;
    }	
} // @jve:decl-index=0:visual-constraint="10,10"

