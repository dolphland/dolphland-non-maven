package com.dolphland.client.util.swingverifiers;

import javax.swing.JComponent;
import javax.swing.JTextField;

import com.dolphland.client.util.i18n.MessageFormater;

/**
 * Verifier pour les champs de saisie de nombres entiers
 * 
 * 
 * @author Bernard Noel
 */
public class IntegerVerifier extends AbstractInputVerifier {

    private static final MessageFormater FMT = MessageFormater.getFormater(IntegerVerifier.class);
    private int taille = 0;



    /**
     * Construit un IntegerVerifier
     * 
     * @param afficheur
     *            L'afficheur de messages � utiliser
     * @param taille
     *            Le nombre de chiffres maximum autoris� pour l'entier � saisir
     */
    public IntegerVerifier(Afficheur afficheur, int taille) {
        super(afficheur);
        this.taille = taille;
    }



    public boolean verify(JComponent input) {
        if (input instanceof JTextField) {
            JTextField zone = (JTextField) input;

            // V�rification taille
            if (zone.getText().length() > taille) {
                afficherErreur(FMT.format("IntegerVerifier.RS_TAILLE_MAXI_DEPASSEE___0NUMBER_CARACTERES", taille));
                return false;
            }

            // V�rification format (int)
            if (zone.getText().length() > 0) {
                try {
                    Integer.parseInt(zone.getText().trim());
                } catch (NumberFormatException e) {
                    afficherErreur(FMT.format("IntegerVerifier.RS_FORMAT_INVALIDE"));
                    return false;
                }
            }
        }
        getAfficheur().clear();
        return true;
    }
}
