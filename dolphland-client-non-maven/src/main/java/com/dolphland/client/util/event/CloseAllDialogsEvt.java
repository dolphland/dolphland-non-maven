package com.dolphland.client.util.event;

/**
 * Event dispatched when all currently opened dialogs should be closed.
 */
public class CloseAllDialogsEvt extends UIEvent {

    public final static EventDestination DEST = new EventDestination();



    public CloseAllDialogsEvt() {
        super(DEST);

    }

}
