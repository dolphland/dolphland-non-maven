package com.dolphland.client.util.conf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StringUtils;

class ServicesResource implements Resource {

    private File serviceFile;

    private String serviceFileResourcePath;



    ServicesResource(String serviceFileResourcePath, File sFile) {
        this.serviceFile = sFile;
        this.serviceFileResourcePath = serviceFileResourcePath;
    }



    public InputStream getInputStream() throws IOException {
        return new FileInputStream(serviceFile);
    }



    public long lastModified() throws IOException {
        return serviceFile.lastModified();
    }



    public boolean isReadable() {
        return true;
    }



    public boolean isOpen() {
        return true;
    }



    public URL getURL() throws IOException {
        return serviceFile.toURL();
    }



    public URI getURI() throws IOException {
        return serviceFile.toURI();
    }



    public String getFilename() {
        return serviceFile.getName();
    }



    public File getFile() throws IOException {
        return serviceFile;
    }



    public String getDescription() {
        return "Spring service file resource <" + serviceFile.getAbsolutePath() + ">"; //$NON-NLS-1$ //$NON-NLS-2$
    }



    public boolean exists() {
        return serviceFile.isFile();
    }



    public Resource createRelative(String relativePath) throws IOException {
        String resultPath = null;
        if (relativePath.startsWith("/")) { //$NON-NLS-1$
            resultPath = relativePath;
        } else {
            resultPath = StringUtils.applyRelativePath(serviceFileResourcePath, relativePath);
        }
        return new ClassPathResource(resultPath);
    }



    public long contentLength() throws IOException {
        return getFile().length();
    }
}
