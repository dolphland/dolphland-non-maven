/*
 * $Log: Request.java,v $
 * Revision 1.2 2009/03/05 15:03:15 bvatan
 * - ajout gestion des prioritÚs
 * Revision 1.1 2009/02/03 11:35:20 bvatan
 * - initial check in
 */

package com.dolphland.client.util.task;

import com.dolphland.client.util.event.Event;

public class Request {

    static final int MIN_PRIORITY = Event.MIN_PRIORITY;

    static final int DFT_PRIORITY = Event.DFT_PRIORITY;

    static final int MAX_PRIORITY = Event.MAX_PRIORITY;

    static final int MIN_SYS_PRIORITY = Event.MIN_SYS_PRIORITY;

    static final int MAX_SYS_PRIORITY = Event.MAX_SYS_PRIORITY;

    private Object monitor = new Object();

    private int id;

    private boolean done;

    private boolean stoped;

    private Throwable cause;

    private int priority;



    Request() {
        id = System.identityHashCode(this);
        this.priority = DFT_PRIORITY;
    }



    final int getId() {
        return id;
    }



    void done() {
        synchronized (monitor) {
            done = true;
            stoped = false;
            cause = null;
        }
    }



    void stop() {
        synchronized (monitor) {
            done = false;
            stoped = true;
            cause = null;
        }
    }



    void fail(Throwable e) {
        synchronized (monitor) {
            done = false;
            stoped = false;
            cause = e;
        }
    }



    void waitForCompletion() throws InterruptedException {
        synchronized (monitor) {
            try {
                while (!done) {
                    monitor.wait();
                }
            } catch (InterruptedException ie) {
                throw ie;
            }
        }
    }



    void signal() {
        synchronized (monitor) {
            monitor.notify();
        }
    }



    boolean isDone() {
        synchronized (monitor) {
            return done;
        }
    }



    boolean hasStoped() {
        synchronized (monitor) {
            return stoped;
        }
    }



    boolean hasFailed() {
        synchronized (monitor) {
            return cause != null;
        }
    }



    Throwable getCause() {
        synchronized (monitor) {
            return cause;
        }
    }



    void setPriority(int priority) {
        if (priority < MIN_PRIORITY) {
            this.priority = MIN_PRIORITY;
        } else if (priority > MAX_SYS_PRIORITY) {
            this.priority = MAX_SYS_PRIORITY;
        } else {
            this.priority = priority;
        }
    }



    int getPriority() {
        return priority;
    }

}
