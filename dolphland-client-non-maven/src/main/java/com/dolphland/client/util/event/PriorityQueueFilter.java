/*
 * $Log: PriorityQueueFilter.java,v $
 * Revision 1.2 2009/04/01 09:33:13 jscafi
 * - Résolution des warnings
 * Revision 1.1 2009/03/05 15:02:57 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

public class PriorityQueueFilter<T> {

    public static final int ACCEPT = 0;

    public static final int FILTER = 1;

    public static final int DISCARD = 2;



    public int filter(T item) {
        return ACCEPT;
    }

}
