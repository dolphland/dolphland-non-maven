package com.dolphland.client.util.transaction;

import java.awt.EventQueue;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.service.Service;
import com.dolphland.client.util.service.ServiceResponseHandler;

/**
 * D�finit la structure d'une transaction destin�e � ex�cuter un
 * <code>Service</code> dans un
 * environnement graphique Swing ou AWT.<br>
 * La transaction est responsable de l'ex�cution du service et de la
 * notification du r�sultat de l'ex�cution du service.<br>
 * <code>
 * 1 - Appel de SVCTransaction.execute(Object) par le code client<br>
 * 2 - Appel de SVCTransaction.createService() (Thread AWT)<br>
 * 3 - Ex�cution du service (Thread service (d�di�)<br>
 * 4 - Appel de updateViewSuccess() si l'ex�cution du service a r�ussie. (Thread AWT).<br>
 * 5 - Appel de updateViewError() si l'ecution du service a �chou� (Thread AWT).
 * </code><br>
 * <br>
 * Dans tous les cas, les services execute(), createService(),
 * updateViewSuccess() et updateViewError() sont ex�cut�s par l'EDT AWT.
 * 
 * @author JayJay
 */
public abstract class SVCTransaction implements ServiceResponseHandler {

    private final static Logger log = Logger.getLogger(SVCTransaction.class);



    /**
     * M�thode d'ex�cution de la transaction, contient les traitements qui
     * d�marrent la transaction.
     * 
     * @param param
     *            Param�tres re�us de execute(Object param);
     */
    protected abstract Service createService(Object param);



    /**
     * Service de mise � jour de la vue. Cette m�thode est ex�cut� par l'EDT AWT
     * si le service a r�ussi.
     */
    protected abstract void updateViewSuccess(Object data);



    /**
     * M�thode mise � jour de la vue. Cette m�thode est ex�cut� par l'EDT AWT si
     * le service a �chou�.
     */
    protected abstract void updateViewError(Object error);



    /**
     * Ex�cute la transaction.
     */
    public void execute(final Object param) {
        Runnable job = new Runnable() {

            public void run() {
                Service svc = null;
                try {
                    svc = createService(param);
                } catch (Exception e) {
                    if (log.isInfoEnabled()) {
                        log.info("execute(): createService(Object) has thrown an exception !", e); //$NON-NLS-1$
                    }
                    throw new AppInfrastructureException("createService(Object) has failed !", e); //$NON-NLS-1$
                }
                if (svc == null) {
                    throw new NullPointerException("createService(Object) has return a null service !"); //$NON-NLS-1$
                }
                svc.execute();
            }
        };
        if (EventQueue.isDispatchThread()) {
            job.run();
        } else {
            EventQueue.invokeLater(job);
        }
    }



    /**
     * D�clenche l'ex�cution de updateView() dans l'EDT AWT
     */
    private void updateViewLater(final boolean success, final Object res) {
        Runnable job = new Runnable() {

            public void run() {
                if (success) {
                    try {
                        updateViewSuccess(res);
                    } catch (Exception e) {
                        log.error("updateViewLater(): Erreur in updateViewSuccess() !", e); //$NON-NLS-1$
                    }
                } else {
                    try {
                        updateViewError(res);
                    } catch (Exception e) {
                        log.error("updateViewLater(): Erreur in updateViewError() !", e); //$NON-NLS-1$
                    }
                }
            }
        };
        SwingUtilities.invokeLater(job);
    }



    /* Impl�mentation de ServiceResponseHandler */
    public void setServiceResult(Object res) {
        updateViewLater(true, res);
    }



    /* Impl�mentation de ServiceResponseHandler */
    public void setServiceError(Object err) {
        updateViewLater(false, err);
    }
}
