package com.dolphland.client.util.action;

import java.util.List;

import javax.swing.Icon;

import com.dolphland.client.util.transaction.MVCTxListener;

/**
 * Action listener used with UIAction that prevent when :
 * - enabled has changed
 * - visible has changed
 * - name has changed
 * - icon has changed
 * - description has changed
 * 
 * @author JayJay
 * 
 */
public interface UIActionListener extends MVCTxListener {

    /**
     * Performed when enabled has changed
     * 
     * @param enabled
     *            New enabled value
     */
    public void enabledChanged(boolean enabled);



    /**
     * Performed when visible has changed
     * 
     * @param visible
     *            New visible value
     */
    public void visibleChanged(boolean visible);



    /**
     * Performed when name has changed
     * 
     * @param name
     *            New name value
     */
    public void nameChanged(String name);



    /**
     * Performed when icon has changed
     * 
     * @param icon
     *            New icon value
     */
    public void iconChanged(Icon icon);



    /**
     * Performed when description has changed
     * 
     * @param description
     *            New description value
     */
    public void descriptionChanged(String description);



    /**
     * Performed when disabled causes has changed
     * 
     * @param disabledCauses
     *            New disabled causes
     */
    public void disabledCausesChanged(List<String> disabledCauses);
}
