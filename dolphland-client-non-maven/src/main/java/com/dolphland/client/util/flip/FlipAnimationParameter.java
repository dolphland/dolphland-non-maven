package com.dolphland.client.util.flip;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.animator.AnimationActionEvt;
import com.dolphland.client.util.assertion.AssertUtil;

/**
 * Represents all animation's parameters used to flip
 * 
 * @author jeremy.scafi
 * 
 */
public class FlipAnimationParameter {

    // Default animation's duration in milliseconds
    private final static long DEFAULT_ANIMATION_DURATION_MILLISECOND = 200;

    private FlipAnimationType typeAction;
    private String selectedApplicationId;
    private Point inAttachPoint;
    private Point outAttachPoint;
    private Integer stepsPolicy;
    private List<UIAction<AnimationActionEvt, ?>> actionsBefore;
    private List<UIAction<AnimationActionEvt, ?>> actionsAfter;
    private Long animationDurationMs = DEFAULT_ANIMATION_DURATION_MILLISECOND;



    /**
     * Default constructor with {@link FlipAnimationType} specified
     * 
     * @param typeAction
     *            {@link FlipAnimationType}
     */
    public FlipAnimationParameter(FlipAnimationType typeAction) {
        setTypeAction(typeAction);
        this.actionsBefore = new ArrayList<UIAction<AnimationActionEvt, ?>>();
        this.actionsAfter = new ArrayList<UIAction<AnimationActionEvt, ?>>();
    }



    /**
     * @return the typeAction
     */
    public FlipAnimationType getTypeAction() {
        return typeAction;
    }



    /**
     * @param typeAction
     *            the typeAction to set
     */
    public void setTypeAction(FlipAnimationType typeAction) {
        AssertUtil.notNull(typeAction, "typeAction is null !");
        this.typeAction = typeAction;
    }



    /**
     * @return the selectedApplicationId
     */
    public String getSelectedApplicationId() {
        return selectedApplicationId;
    }



    /**
     * @param selectedApplicationId
     *            the selectedApplicationId to set
     */
    public void setSelectedApplicationId(String selectedApplicationId) {
        this.selectedApplicationId = selectedApplicationId;
    }



    /**
     * @return the inAttachPoint
     */
    public Point getInAttachPoint() {
        return inAttachPoint;
    }



    /**
     * @param inAttachPoint
     *            the inAttachPoint to set
     */
    public void setInAttachPoint(Point inAttachPoint) {
        this.inAttachPoint = inAttachPoint;
    }



    /**
     * @return the outAttachPoint
     */
    public Point getOutAttachPoint() {
        return outAttachPoint;
    }



    /**
     * @param outAttachPoint
     *            the outAttachPoint to set
     */
    public void setOutAttachPoint(Point outAttachPoint) {
        this.outAttachPoint = outAttachPoint;
    }



    /**
     * @return the stepsPolicy
     */
    public Integer getStepsPolicy() {
        return stepsPolicy;
    }



    /**
     * @param stepsPolicy
     *            the stepsPolicy to set
     */
    public void setStepsPolicy(Integer stepsPolicy) {
        this.stepsPolicy = stepsPolicy;
    }



    /**
     * @return the actionsBefore
     */
    public List<UIAction<AnimationActionEvt, ?>> getActionsBefore() {
        return actionsBefore;
    }



    /**
     * @param actionsBefore
     *            the actionsBefore to set
     */
    public void setActionsBefore(List<UIAction<AnimationActionEvt, ?>> actionsBefore) {
        this.actionsBefore = actionsBefore;
    }



    /**
     * @return the actionsAfter
     */
    public List<UIAction<AnimationActionEvt, ?>> getActionsAfter() {
        return actionsAfter;
    }



    /**
     * @param actionsAfter
     *            the actionsAfter to set
     */
    public void setActionsAfter(List<UIAction<AnimationActionEvt, ?>> actionsAfter) {
        this.actionsAfter = actionsAfter;
    }



    /**
     * @return the animationDurationMs
     */
    public Long getAnimationDurationMs() {
        return animationDurationMs;
    }



    /**
     * @param animationDurationMs
     *            the animationDurationMs to set
     */
    public void setAnimationDurationMs(Long animationDurationMs) {
        this.animationDurationMs = animationDurationMs;
    }

}
