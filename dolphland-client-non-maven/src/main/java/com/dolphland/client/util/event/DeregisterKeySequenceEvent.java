package com.dolphland.client.util.event;

/**
 * Supprime l'�mission d'un �v�nement qui avait �t� conditionn�e par la frappe
 * d'une s�quence de touches.<br>
 * 
 * @author JayJay
 */
public class DeregisterKeySequenceEvent extends UIEvent {

    static final EventDestination DEST = new EventDestination();
    private int[] keySequence;



    public DeregisterKeySequenceEvent(int[] keySequence) {
        super(DEST);
        if (keySequence == null) {
            throw new NullPointerException("keySequence is null !"); //$NON-NLS-1$
        }
        this.keySequence = keySequence;
    }



    public int[] getKeySequence() {
        return keySequence;
    }

}
