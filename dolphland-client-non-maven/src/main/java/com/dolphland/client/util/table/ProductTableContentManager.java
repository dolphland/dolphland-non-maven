/*
 * $Log: ProductTableContentManager.java,v $
 * Revision 1.9 2014/04/15 14:51:47 bvatan
 * - FWK-143: Added support for row sorting when a double click is performed on
 * a ProtuctTable column header
 * Revision 1.8 2013/08/27 13:23:38 bvatan
 * - FWK-54: remove all references to ProductAction, all replaced by
 * UIAction<ProductActionevt, ?>
 * - Fixed bug that did not handle double-clic properly
 * Revision 1.7 2013/04/22 08:26:57 bvatan
 * - added cell editor support
 * Revision 1.6 2010/12/02 08:37:39 bvatan
 * - ajout possibilit� d'ajouter une bulle de description sur une colonne
 * et/ou
 * sur une cellule
 * Revision 1.5 2010/07/29 09:51:27 bvatan
 * - gestion des ProductActionConstraints permettant d'associer des contrainte
 * de positionnement (ou autre) sur les ProductAction
 * Revision 1.4 2010/07/21 07:29:50 bvatan
 * - ajotu de getProductActionConstraints()
 * Revision 1.3 2010/06/09 09:47:46 bvatan
 * - Ajout possibilit� de positionner les ProductAction sur les 4 c�t�s de
 * de la
 * table
 * Revision 1.2 2009/08/07 12:17:31 bvatan
 * - ajout isCellEditable()
 * - renderXXX retournent Compoenet au lieu de JComponent
 * Revision 1.1 2009/02/04 13:38:39 jscafi
 * - Ajout de ProducTable permettant de g�rer des tables de produits
 * Revision 1.1 2009/01/06 14:17:04 bvatan
 * - ajotu gestion du blocage des barre par coul�e
 */

package com.dolphland.client.util.table;

import java.awt.Component;
import java.awt.Font;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;

import com.dolphland.client.util.action.ActionsContext;
import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvtFactory;
import com.dolphland.client.util.application.components.FwkLabel;
import com.dolphland.client.util.table.demo.ButtonTableCellEditor;

/**
 * Classe abstraite permettant de manager le contenu de la table des produits
 * 
 * @author jeremy.scafi
 * @author JayJay
 * 
 */
public abstract class ProductTableContentManager {

    private UIActionEvtFactory<ProductActionEvt> actionEvtFactory;
    private ActionsContext actionsContext;
    private Component tableComponent;
    private Map<ButtonKey, FwkLabel> bindedButtons;



    public ProductTableContentManager() {
        bindedButtons = new TreeMap<ButtonKey, FwkLabel>();
    }



    protected FwkLabel getLabelButton(UIAction<ProductActionEvt, ?> action, int row, int column) {
        if (action == null) {
            return createLabelButton(null);
        }
        ButtonKey buttonKey = new ButtonKey(action, row, column);
        FwkLabel labelButton = bindedButtons.get(buttonKey);
        if (labelButton == null) {
            bindedButtons.put(buttonKey, createLabelButton(action));
        }
        return labelButton;
    }



    protected FwkLabel createLabelButton(UIAction<ProductActionEvt, ?> action) {
        // Create button
        JButton button = null;

        if (action == null) {
            // If no action, button is in error state
            button = new JButton("ERROR");
        } else {
            // Else, create button and force table's repaint if button's state
            // has changed
            button = new JButton() {

                @Override
                public void setVisible(boolean aFlag) {
                    super.setVisible(aFlag);
                    tableComponent.repaint();
                }



                public void setEnabled(boolean b) {
                    super.setEnabled(b);
                    tableComponent.repaint();
                }



                @Override
                public void setText(String text) {
                    super.setText(text);
                    tableComponent.repaint();
                }
            };

            // Bind action and button
            getActionsContext().bind(action, button);
        }

        // Create and return button's label
        return ButtonTableCellEditor.createLabelButton(button, action);
    }

    private static class ButtonKey implements Comparable<ButtonKey> {

        private UIAction<ProductActionEvt, ?> action;
        private int row;
        private int column;



        public ButtonKey(UIAction<ProductActionEvt, ?> action, int row, int column) {
            this.action = action;
            this.row = row;
            this.column = column;
        }



        @Override
        public int compareTo(ButtonKey o) {
            return new Integer(hashCode()).compareTo(o.hashCode());
        }



        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((action == null) ? 0 : action.hashCode());
            result = prime * result + column;
            result = prime * result + row;
            return result;
        }
    }



    public void setTableComponent(Component tableComponent) {
        this.tableComponent = tableComponent;
    }



    public Component getTableComponent() {
        return this.tableComponent;
    }



    public void setActionEvtFactory(UIActionEvtFactory<ProductActionEvt> actionEvtFactory) {
        this.actionEvtFactory = actionEvtFactory;
    }



    public UIActionEvtFactory<ProductActionEvt> getActionEvtFactory() {
        return actionEvtFactory;
    }



    public void setActionsContext(ActionsContext actionsContext) {
        this.actionsContext = actionsContext;
    }



    public ActionsContext getActionsContext() {
        return actionsContext;
    }



    /**
     * Renderer de produit fournissant le composant de rendu du contenu de la
     * propri�t� de produit pour le produit donn�
     * 
     * @param input
     *            Composant de rendu de la cellule concern�e dans la table
     * @param pp
     *            Propri�t� de produit
     * @param pa
     *            Produit
     * @param isSelected
     *            Indique si la cellule concern�e dans la table est
     *            s�lectionn�e
     * @param hasFocus
     *            Indique si la cellule concern�e dans la table a le focus
     * @param row
     *            Indice de ligne de la cellule concern�e dans la table
     * @param column
     *            Indice de colonne de la cellule concern�e dans la table
     * @return Composant de rendu du contenu de la propri�t� de produit pour
     *         le
     *         produit donn�
     */
    public abstract Component renderProduct(Component input, ProductProperty pp, ProductAdapter pa, boolean isSelected, boolean hasFocus, int row, int column);



    /**
     * Renderer de propri�t� de produit fournissant le composant de rendu de
     * la
     * propri�t� de produit donn�
     * 
     * @param input
     *            Composant de rendu de l'ent�te concern� dans la table
     * @param pp
     *            Propri�t� de produit
     * @param isSelected
     *            Indique si l'ent�te concern� dans la table est
     *            s�lectionn�
     * @param hasFocus
     *            Indique si l'ent�te concern� dans la table a le focus
     * @param row
     *            Indice de ligne de l'ent�te concern� dans la table
     * @param column
     *            Indice de colonne de l'ent�te concern� dans la table
     * @return
     */
    public abstract Component renderProductProperty(Component input, ProductProperty pp, boolean isSelected, boolean hasFocus, int row, int column);



    /**
     * Fixe la largeur d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur de la propri�t� de produit
     */
    public abstract int getProductPropertyWidth(ProductProperty pp);



    /**
     * Fixe la largeur minimum d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur minimum de la propri�t� de produit
     */
    public abstract int getProductPropertyMinWidth(ProductProperty pp);



    /**
     * Fixe la largeur maximum d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur maximum de la propri�t� de produit
     */
    public abstract int getProductPropertyMaxWidth(ProductProperty pp);



    /**
     * Fixe la largeur pr�f�r�e d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Largeur pr�f�r�e de la propri�t� de produit
     */
    public abstract int getProductPropertyPreferredWidth(ProductProperty pp);



    /**
     * Indique si la s�lection est autoris�e dans la table des produits
     */
    public abstract boolean isProductTableSelectionEnabled();



    /**
     * Indique si la s�lection de ligne est autoris�e dans la table des
     * produits
     */
    public abstract boolean isProductTableRowSelectionAllowed();



    /**
     * Indique si le r�ordonnancement de colonnes est autoris� dans la table
     * des
     * produits
     */
    public abstract boolean isProductTableReorderingAllowed();



    /**
     * Indique si la cellule est �ditable
     */
    public abstract boolean isCellEditable(ProductProperty pp, ProductAdapter pa);



    /**
     * Indique si la cellule est �ditable
     */
    public abstract boolean isCellDisabled(ProductProperty pp, ProductAdapter pa);



    /**
     * Indique le type de s�lection de la table des produits Valeurs possibles
     * :
     * - ListSelectionModel.SINGLE_SELECTION : S�lection simple -
     * ListSelectionModel.SINGLE_INTERVAL_SELECTION : S�lection intervale
     * simple
     * - ListSelectionModel.MULTIPLE_INTERVAL_SELECTION : S�lection intervale
     * multiple
     */
    public abstract int getProductTableSelectionMode();



    /**
     * Renvoie la hauteur de ligne de la table des produits
     */
    public abstract int getProductTableRowHeight();



    /**
     * Indique si la table des produits est opaque
     */
    public abstract boolean isProductTableOpaque();



    /**
     * Indique si les grille de la table des produits sont visibles
     */
    public abstract boolean isProductTableShowGrid();



    /**
     * Indique si les lignes horizontables de la table des produits sont
     * visibles
     */
    public abstract boolean isProductTableShowHorizontalLines();



    /**
     * Renvoie la bordure de la table des produits
     */
    public abstract Border getProductTableBorder();



    /**
     * Renvoie la font par d�faut de la table des produits
     */
    public abstract Font getProductTableDefaultFont();



    /**
     * Fournit les contraintes associ�es au <code>ProductAction</code>
     * sp�cifi�.
     * 
     * @param pa
     *            Le ProductAction dont on veut obtenir les contraintes
     * @return Les contraintes de positionnement associ�es au ProductAction
     *         sp�cifi�
     */
    public abstract ProductActionConstraints getProductActionConstraints(UIAction<ProductActionEvt, ?> pa);



    /**
     * Fournit le tooltip d'une propri�t� de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @return Tooltip de la propri�t� de produit
     */
    public abstract String getProductPropertyTooltip(ProductProperty pp);



    /**
     * Fournit le tooltip d'une cellule de produit
     * 
     * @param pp
     *            Propri�t� de produit
     * @param pa
     *            Produit
     * @return Tooltip de la cellule de produit
     */
    public abstract String getCellTooltip(ProductProperty pp, ProductAdapter pa);



    /**
     * Fournit l'�diteur de cellule d'une table
     * 
     * @param pp
     *            Propri�t� de produit
     * @param column
     *            Indice de colonne de la cellule concern�e dans la table
     * @return Editeur de cellule d'une table
     */
    public abstract TableCellEditor getTableCellEditor(ProductProperty pp, int column);



    /**
     * <p>
     * Provide a comparator for {@link ProductTableRow} in order to perform row
     * sort operation that might be requested by user.
     * </p>
     * <p>
     * This service take a context as parameter so as to provide information on
     * how the sort operation should occur.
     * </p>
     * <p>
     * See {@link ProductTableRowComparatorContext} for more information on sort
     * operation and how it should be performed
     * </p>
     * 
     * @return A comparator to be used for {@link ProductTableRow} sort
     *         operation.
     */
    public Comparator<ProductTableRow> createRowComparator(ProductTableRowComparatorContext ctx) {
        boolean sortAscentOrder;
        switch (ctx.getSortOrder()) {
            default :
            case ASCENT :
                sortAscentOrder = true;
                break;
            case DESCENT :
                sortAscentOrder = false;
                break;
        }
        return new DefaultProductRowComparator(ctx.getSortedProductProperty(), sortAscentOrder);
    }
}
