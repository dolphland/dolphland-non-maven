package com.dolphland.client.util.message;

import java.util.Date;

import com.dolphland.client.event.EventPartyJoined;
import com.dolphland.client.event.EventPartyLeft;
import com.dolphland.client.event.EventUserDisconnection;
import com.dolphland.client.event.EventUserLogin;
import com.dolphland.client.event.EventUserLogout;
import com.dolphland.client.event.EventUserOffline;
import com.dolphland.client.event.EventUserReOnline;
import com.dolphland.client.event.EventUserReplacement;
import com.dolphland.client.util.date.DateUtil;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.core.ws.data.User;

/**
 * Message util
 * 
 * @author JayJay
 * 
 */
public class MessageUtil {

    private static final MessageFormater FMT = MessageFormater.getFormater(MessageUtil.class);



    /**
     * Format a message's content to html from its content and sender
     * 
     * @param content
     *            Content
     * @param sender
     *            Sender
     * 
     * @return Html message's content
     */
    public final static String toHtml(String content, User sender) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_CHAT_MESSAGE", msgDate, sender.getFirstName(), sender.getLastName(), content));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventUserLogin
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventUserLogin event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_USER_LOGIN_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventUserLogout
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventUserLogout event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_USER_LOGOUT_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventUserOffline
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventUserOffline event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_USER_OFFLINE_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventUserReOnline
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventUserReOnline event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_USER_REONLINE_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventUserDisconnection
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventUserDisconnection event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_USER_DISCONNECTION_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventUserReplacement
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventUserReplacement event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_USER_REPLACEMENT_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventPartyJoined
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventPartyJoined event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_PARTY_JOINED_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }



    /**
     * Format a message's content to html from an EventPartyLeft
     * 
     * @param event
     *            Event
     * 
     * @return Html message's content
     */
    public final static String toHtml(EventPartyLeft event) {
        StringBuffer htmlContent = new StringBuffer();
        String msgDate = DateUtil.FRM_TIME_FR.format(new Date());
        htmlContent.append(FMT.format("MessageUtil.RS_PARTY_LEFT_MESSAGE", msgDate, event.getUser().getFirstName(), event.getUser().getLastName()));
        return htmlContent.toString();
    }
}
