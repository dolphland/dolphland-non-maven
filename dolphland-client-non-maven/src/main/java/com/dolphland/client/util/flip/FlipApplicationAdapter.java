package com.dolphland.client.util.flip;

import java.awt.Component;

import com.dolphland.client.util.assertion.AssertUtil;

/**
 * Representation for a flip application
 * 
 * @author p-jeremy.scafi
 * 
 */
public class FlipApplicationAdapter {

    public final static int FLIP_WIDTH = 640;
    public final static int FLIP_HEIGHT = 320;

    private String applicationId;
    private Component applicationComponent;
    private FlipApplicationLoader applicationLoader;
    private FlipAnimationParameter flipAnimationMinimize;
    private FlipAnimationParameter flipAnimationMaximize;
    private FlipAnimationParameter flipAnimationShow;
    private FlipAnimationParameter flipAnimationHide;



    /**
     * Default constructor
     * 
     * @param applicationId
     *            Application's id
     * @param applicationComponent
     *            Application's component
     * @param applicationLoader
     *            Application's loader
     */
    public FlipApplicationAdapter(String applicationId, Component applicationComponent, FlipApplicationLoader applicationLoader) {
        AssertUtil.notNull(applicationId, "applicationId is null !");
        AssertUtil.notNull(applicationComponent, "applicationComponent is null !");
        AssertUtil.notNull(applicationId, "applicationId is null !");
        this.applicationId = applicationId;
        this.applicationComponent = applicationComponent;
        this.applicationLoader = applicationLoader;
        this.flipAnimationMinimize = new FlipAnimationParameterBuilder(FlipAnimationType.MINIMIZE_SELECTED_APPLICATION).selectedApplicationId(applicationId).get();
        this.flipAnimationMaximize = new FlipAnimationParameterBuilder(FlipAnimationType.MAXIMIZE_SELECTED_APPLICATION).selectedApplicationId(applicationId).get();
        this.flipAnimationShow = new FlipAnimationParameterBuilder(FlipAnimationType.POP_UP_SELECTED_APPLICATION).selectedApplicationId(applicationId).get();
        this.flipAnimationHide = new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_OUT_OF_SCREEN_SELECTED_APPLICATION).selectedApplicationId(applicationId).get();
    }



    /**
     * @return the applicationId
     */
    public String getApplicationId() {
        return applicationId;
    }



    /**
     * @return the applicationComponent
     */
    public Component getApplicationComponent() {
        return applicationComponent;
    }



    /**
     * @return the applicationLoader
     */
    public FlipApplicationLoader getApplicationLoader() {
        return applicationLoader;
    }



    /**
     * @return the flipAnimationMinimize
     */
    public FlipAnimationParameter getFlipAnimationMinimize() {
        return flipAnimationMinimize;
    }



    /**
     * @return the flipAnimationMaximize
     */
    public FlipAnimationParameter getFlipAnimationMaximize() {
        return flipAnimationMaximize;
    }



    /**
     * @return the flipAnimationShow
     */
    public FlipAnimationParameter getFlipAnimationShow() {
        return flipAnimationShow;
    }



    /**
     * @return the flipAnimationHide
     */
    public FlipAnimationParameter getFlipAnimationHide() {
        return flipAnimationHide;
    }
}
