package com.dolphland.client.util.layout;

/**
 * Cette classe sp�cifie les contraintes pour un composant utilisant comme
 * layout la classe <code>TableLayout</code>. <br>
 * <br>
 * Elle a pour builder la classe <code>TableLayoutConstraintsBuilder</code>. <br>
 * <br>
 * L'utilisation de cette classe se fait via la m�thode
 * <code>addComponent</code> de la classe <code>TableLayoutBuilder</code>. <br>
 * <br>
 * Exemple d'utilisation :<br>
 * <br>
 * <ol>
 * <li>Cr�ation et affectation du TableLayout au conteneur <code>
 * <pre> 	
 * TableLayoutBuilder layoutBuilder = new TableLayoutBuilder();
 * panel.setLayout(layoutBuilder.get());
 * </pre>
 * </code> <br>
 * <li>Ajout d'un composant au conteneur <code>
 * <pre> 
 * panel.add(getLabel(), layoutBuilder.addComponent(new TableLayoutConstraintsBuilder()
 * .width(TableLayout.FILL)
 * .height(TableLayout.FILL)
 * .row(0)
 * .column(0)
 * .get()
 * ));
 * </pre>
 * </code>
 * </ol>
 * 
 * @author jeremy.scafi
 */
public class TableLayoutConstraints {

    private double leftInsets;
    private double rightInsets;
    private double topInsets;
    private double bottomInsets;

    private double width;
    private double height;

    private int row;
    private int column;



    /**
     * Constructeur sans param�tre
     */
    public TableLayoutConstraints() {
        leftInsets = 0;
        rightInsets = 0;
        topInsets = 0;
        bottomInsets = 0;
        width = 0;
        height = 0;
        row = 0;
        column = 0;
    }



    /**
     * Constructeur avec un mod�le de <code>TableLayoutConstraints</code> en
     * param�tre.
     * 
     * @param tableLayoutConstraintsModele
     *            Mod�le de <code>TableLayoutConstraints</code>.
     */
    public TableLayoutConstraints(TableLayoutConstraints tableLayoutConstraintsModele) {
        leftInsets = tableLayoutConstraintsModele.leftInsets;
        rightInsets = tableLayoutConstraintsModele.rightInsets;
        topInsets = tableLayoutConstraintsModele.topInsets;
        bottomInsets = tableLayoutConstraintsModele.bottomInsets;
        width = tableLayoutConstraintsModele.width;
        height = tableLayoutConstraintsModele.height;
        row = tableLayoutConstraintsModele.row;
        column = tableLayoutConstraintsModele.column;
    }



    /**
     * Renvoie la marge gauche du composant
     */
    public double getLeftInsets() {
        return leftInsets;
    }



    /**
     * Cette m�thode fixe la marge gauche du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param leftInsets
     *            Marge gauche du composant.
     */
    public void setLeftInsets(double leftInsets) {
        this.leftInsets = leftInsets;
    }



    /**
     * Renvoie la marge droite du composant
     */
    public double getRightInsets() {
        return rightInsets;
    }



    /**
     * Cette m�thode fixe la marge droite du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param rightInsets
     *            Marge droite du composant.
     */
    public void setRightInsets(double rightInsets) {
        this.rightInsets = rightInsets;
    }



    /**
     * Renvoie la marge haut du composant
     */
    public double getTopInsets() {
        return topInsets;
    }



    /**
     * Cette m�thode fixe la marge haute du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param topInsets
     *            Marge haute du composant.
     */
    public void setTopInsets(double topInsets) {
        this.topInsets = topInsets;
    }



    /**
     * Renvoie la marge bas du composant
     */
    public double getBottomInsets() {
        return bottomInsets;
    }



    /**
     * Cette m�thode fixe la marge basse du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param bottomInsets
     *            Marge basse du composant.
     */
    public void setBottomInsets(double bottomInsets) {
        this.bottomInsets = bottomInsets;
    }



    /**
     * Renvoie la largeur du composant
     */
    public double getWidth() {
        return width;
    }



    /**
     * Cette m�thode fixe la largeur du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param width
     *            Largeur du composant.
     */
    public void setWidth(double width) {
        this.width = width;
    }



    /**
     * Renvoie la hauteur du composant
     */
    public double getHeight() {
        return height;
    }



    /**
     * Cette m�thode fixe la hauteur du composant.
     * <p>
     * Il existe deux types de valeurs possibles : pr�d�finies et libres.<br>
     * <br>
     * Les valeurs pr�d�finies sont:
     * <ul>
     * <li><code>TableLayoutConstants.FILL</code>,
     * <li><code>TableLayoutConstants.PREFERRED</code>,
     * <li><code>TableLayoutConstants.MINIMUM</code>,
     * </ul>
     * Les valeurs libres sont strictement positives. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param height
     *            Hauteur du composant.
     */
    public void setHeight(double height) {
        this.height = height;
    }



    /**
     * Renvoie la ligne du composant
     */
    public int getRow() {
        return row;
    }



    /**
     * Cette m�thode fixe la ligne du composant.
     * <p>
     * Les valeurs possibles sont entre 0 et n. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param row
     *            Ligne du composant.
     */
    public void setRow(int row) {
        this.row = row;
    }



    /**
     * Renvoie la colonne du composant
     */
    public int getColumn() {
        return column;
    }



    /**
     * Cette m�thode fixe la colonne du composant.
     * <p>
     * Les valeurs possibles sont entre 0 et n. <br>
     * <br>
     * La valeur par d�faut est 0. <br>
     * <br>
     * 
     * @param column
     *            Colonne du composant.
     */
    public void setColumn(int column) {
        this.column = column;
    }
}
