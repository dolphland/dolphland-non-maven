package com.dolphland.client.util.decimal;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Decimal util
 * 
 * @author JayJay
 * 
 */
public class DecimalUtil {

    // Formats pour les doubles
    public final static int WIDTH_DOUBLE_VALUE_DIGIT = 5;
    public final static int WIDTH_DOUBLE_VALUE_FRACTION = 3;



    /**
     * Format value
     * 
     * @param value
     *            Value
     * @return Value formatted
     */
    public final static String formatValue(double value) {
        DecimalFormat frm = new DecimalFormat();
        frm.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
        frm.setMinimumFractionDigits(0);
        frm.setMaximumFractionDigits(WIDTH_DOUBLE_VALUE_FRACTION);
        frm.setMinimumIntegerDigits(1);
        frm.setMaximumIntegerDigits(WIDTH_DOUBLE_VALUE_DIGIT);
        return frm.format(value);
    }
}
