package com.dolphland.client.util.swingverifiers;

/**
 * Sp�cifie les traitements relatifs � un afficheur de messages.<br>
 * Un afficheur de messages est responsable de l'affichage des message d�livr�s
 * par les InputVerifiers.<br>
 * <br>
 * Les messages d�livr�s par les InputVerifier peuvent �tre des message
 * d'information ou des
 * message d'erreur.
 * 
 * @author Bernard Noel
 * @author JayJay
 */
public interface Afficheur {

    public void info(String message);



    public void erreur(String message);



    public void clear();
}
