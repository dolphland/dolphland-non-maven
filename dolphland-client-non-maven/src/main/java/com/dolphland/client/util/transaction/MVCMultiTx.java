package com.dolphland.client.util.transaction;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Classe de base pour la gestion des changements de threads dans les
 * transactions Swing.<br>
 * Cette classe permet d'effectuer les changement de threads nécessaires au
 * respect des bonnes pratiques concernant l'utilisation des threads dans un
 * environnement Swing.
 * 
 * @author JayJay
 */
public class MVCMultiTx<P, T> extends MVCTx<P, T> {

    static final ThreadPoolExecutor poolMultiThread = new ThreadPoolExecutor(5, 5, 60, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

    static long seqId;

    static {
        ThreadFactory factory = new ThreadFactory() {

            public Thread newThread(Runnable r) {
                Thread thread = null;
                synchronized (MVCTx.class) {
                    thread = new Thread(r, "MVCTx-" + (seqId++)); //$NON-NLS-1$
                }
                thread.setDaemon(true);
                return thread;
            }
        };
        poolMultiThread.setThreadFactory(factory);
        poolMultiThread.prestartAllCoreThreads();
    }



    @Override
    ThreadPoolExecutor getThreadPoolExecutor() {
        return MVCMultiTx.poolMultiThread;
    }

}
