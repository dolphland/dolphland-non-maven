package com.dolphland.client.util.event;

/**
 * Ev�nement �mis pour d�clencher la redirection des logs de l'appllication vers
 * le r�pertoire d�fini dans les arguments de lancement de l'application.<br>
 * 
 * Le r�pertoire de stockage des logs est d�fini par:<br>
 * <li><b>logs.dir</b>=path/to/dir <br>
 * <br>
 * Le pr�fixe des logs est d�fini par :<br> <li><b>logs.label</b>=somelabel <br>
 * <br>
 * 
 * @author JayJay
 * 
 */
public final class InstallLogEvt extends Event {

    /** Chemin des logs */
    private String logPath;

    /** Racine du nom des fichiers de log */
    private String logLabel;



    public InstallLogEvt(String logPath) {
        this(logPath, null);
    }



    public InstallLogEvt(String logPath, String logLabel) {
        super(FwkEvents.APP_CLOSED_EVT);
        if (logPath == null) {
            throw new IllegalArgumentException("logPath is null !"); //$NON-NLS-1$
        }
        this.logPath = logPath;
        this.logLabel = logLabel;
    }



    public String getLogPath() {
        return logPath;
    }



    public String getLogLabel() {
        return logLabel;
    }

}
