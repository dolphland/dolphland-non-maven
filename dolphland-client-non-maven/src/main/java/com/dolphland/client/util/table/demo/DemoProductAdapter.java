package com.dolphland.client.util.table.demo;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.table.ProductAction;
import com.dolphland.client.util.table.ProductActionEvt;
import com.dolphland.client.util.table.ProductAdapter;
import com.dolphland.client.util.table.ProductProperty;


public class DemoProductAdapter extends ProductAdapter{

    private static int id;
    
    private String text;
    private UIAction<ProductActionEvt, ?> action1;
    private UIAction<ProductActionEvt, ?> action2;
    
    
    public DemoProductAdapter() {
        super(new Integer(id++).toString());
        
    }
    
    @Override
    public Object getValue(ProductProperty pp) {
        if(DemoProductDescriptor.PROP_NO.equals(pp)){
            return getId();
        }
        if(DemoProductDescriptor.PROP_TEST.equals(pp)){
            return text;
        }
        if(DemoProductDescriptor.PROP_ACTION_1.equals(pp)){
            return action1;
        }
        if(DemoProductDescriptor.PROP_ACTION_2.equals(pp)){
            return action2;
        }
        return null;
    }
    
    @Override
    public void setValue(ProductProperty pp, Object value) {
        if(DemoProductDescriptor.PROP_TEST.equals(pp)){
            text = (String)value;
        }
        if(DemoProductDescriptor.PROP_ACTION_1.equals(pp)){
            action1 = (UIAction<ProductActionEvt, ?>)value;
        }
        if(DemoProductDescriptor.PROP_ACTION_2.equals(pp)){
            action2 = (UIAction<ProductActionEvt, ?>)value;
        }
    }
}
