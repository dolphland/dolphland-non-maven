package com.dolphland.client.util.input;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.swingverifiers.Afficheur;

public class DigitTextField extends CharacterTextField {

    private static final MessageFormater fmt = MessageFormater.getFormater(DigitTextField.class);

    /**
     * Variables de classe
     */
    private boolean signAuthorized;



    /**
     * construit un DigitTextField
     */
    public DigitTextField() {
        this(MAX_VALUE, null);
    }



    /**
     * construit un DigitTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     */
    public DigitTextField(int nbChar) {
        this(nbChar, null);
    }



    /**
     * construit un DigitTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     */
    public DigitTextField(int nbChar, Afficheur afficheur) {
        this(nbChar, afficheur, DEFAULT_FOCUS_NEXT_ENABLED);
    }



    /**
     * construit un DigitTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     * @param focusNextEnabled
     *            : transfert le focus au prochain composant focusable � la fin
     *            de la saisie
     */
    public DigitTextField(int nbChar, Afficheur afficheur, boolean focusNextEnabled) {
        super(nbChar, afficheur, focusNextEnabled);
        setSignAuthorized(false);
        updateFormat();
        setValue(""); //$NON-NLS-1$
    }



    // METHODES PUBLIQUES

    /**
     * Fixe si le signe est autoris�
     */
    public void setSignAuthorized(boolean signAuthorized) {
        this.signAuthorized = signAuthorized;
        updateFormat();
    }



    /**
     * Indique si le signe est autoris�
     */
    public boolean isSignAuthorized() {
        return signAuthorized;
    }



    // METHODES INTERNES AU PACKAGE

    /**
     * initialise le DigitTextField
     */
    protected void initialize() {
        super.initialize();
        setDocument(new DigitDocument());
    }



    protected String getCorrectFormat() {
        return getNbChar() + fmt.format("DigitTextField.RS_INTEGERS"); //$NON-NLS-1$
    }



    @Override
    protected void doCompletionConstraints() {
        int longueurTexte = getText().length();

        // On ne r�alise la compl�tion seulement dans le cas ou :
        // - COMPLETION_WHEN_INPUT et champ non vide
        // - COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO

        boolean enableInputCompletion = ((getCompletionPolicy() == COMPLETION_WHEN_INPUT) && longueurTexte > 0) || getCompletionPolicy() == COMPLETION_WHEN_INPUT_EMPTY_WHEN_ZERO;

        // Si aucun entier saisi, on ajoute un 0
        if (longueurTexte <= 0 && enableInputCompletion) {
            try {
                getDocument().insertString(0, "0", null); //$NON-NLS-1$
                longueurTexte = getText().length();
            } catch (BadLocationException e1) {
                // ignore
            }
        }

        super.doCompletionConstraints();
    }

    /**
     * Document permettant de saisir uniquement des chiffres
     */
    protected class DigitDocument extends CharacterDocument {

        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            int len = str.length();
            String currentChar;
            int nbInsertionOK = 0;
            for (int cpt = 0; cpt < len; cpt++) {
                currentChar = String.valueOf(str.charAt(cpt));
                insertOneDigit(offs + nbInsertionOK, currentChar, a);
                if (isInsertionOK()) {
                    nbInsertionOK++;
                }
            }
        }



        /**
         * insert uniquement un chiffre
         */
        public void insertOneDigit(int offs, String str, AttributeSet a) throws BadLocationException {
            if (signAuthorized && offs == 0 && str.equals("-") && DigitTextField.this.getText().indexOf("-") == -1) { //$NON-NLS-1$ //$NON-NLS-2$
                super.insertString(offs, str, a);
                return;
            } else if (Character.isDigit(str.charAt(0))) {
                super.insertString(offs, str, a);
                return;
            }
            invalidFormat();
        }



        @Override
        protected boolean isDansRepresentation(int offset, String newString) {
            return !newString.equals("-"); //$NON-NLS-1$
        }
    }
}
