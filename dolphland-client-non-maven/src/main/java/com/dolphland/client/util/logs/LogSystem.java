package com.dolphland.client.util.logs;

/**
 * <p>
 * Enumeration for each log's system
 * </p>
 * </p>
 * <p>
 * This enumeration must is used by :
 * <li>each log4j implementation (@see {@link LogSetup})
 * <li>each appender implementation
 * </p>
 * 
 * @author jeremy.scafi
 * 
 */
public enum LogSystem {

    /** Logback's system that replace previous log's system */
    LOGBACK,

    /** Not implemented log's system */
    OTHER;
}
