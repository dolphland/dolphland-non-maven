package com.dolphland.client.util.table;

import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.dolphland.client.util.string.StrUtil;

class ColumnGroup implements SwingConstants {

    private TableCellRenderer renderer;

    private Vector v;

    private String text;

    private int margin = 0;

    private int horizontalAlignement;
    private int verticalAlignement;



    public ColumnGroup() {
        this(null, StrUtil.EMPTY_STRING);
    }



    public ColumnGroup(String text) {
        this(null, text);
    }



    public ColumnGroup(TableCellRenderer renderer, String text) {
        horizontalAlignement = -1;
        verticalAlignement = -1;
        if (renderer == null) {
            this.renderer = new ColumnGroupRenderer();
        } else {
            this.renderer = renderer;
        }
        this.text = text;
        v = new Vector();
    }



    /**
     * @param obj
     *            TableColumn or ColumnGroup
     */
    public void add(Object obj) {
        if (obj == null) {
            return;
        }
        v.addElement(obj);
    }



    /**
     * Fournit la liste des colonnes appartenant aux m�mes groupes que la
     * colonne c
     * 
     * @param c
     *            Colonne concern�e
     * @return Colonnes appartenant aux m�mes groupes que la colonne c
     */
    public List<TableColumn> getColumnsSameColumnGroups(TableColumn c) {
        List<TableColumn> result = new ArrayList<TableColumn>();
        if (v.contains(c)) {
            Enumeration enumer = v.elements();
            while (enumer.hasMoreElements()) {
                Object obj = enumer.nextElement();
                if (obj instanceof TableColumn) {
                    result.add((TableColumn) obj);
                }
            }
        } else {
            Enumeration enumer = v.elements();
            while (enumer.hasMoreElements()) {
                Object obj = enumer.nextElement();
                if (obj instanceof ColumnGroup) {
                    result.addAll(((ColumnGroup) obj).getColumnsSameColumnGroups(c));
                }
            }
        }
        return result;
    }



    /**
     * @param c
     *            TableColumn
     * @param v
     *            ColumnGroups
     */
    public Vector getColumnGroups(TableColumn c, Vector g) {
        g.addElement(this);
        if (v.contains(c)) {
            return g;
        }
        Enumeration enumer = v.elements();
        while (enumer.hasMoreElements()) {
            Object obj = enumer.nextElement();
            if (obj instanceof ColumnGroup) {
                Vector groups = (Vector) ((ColumnGroup) obj).getColumnGroups(c, (Vector) g.clone());
                if (groups != null) {
                    return groups;
                }
            }
        }
        return null;
    }



    public TableCellRenderer getHeaderRenderer() {
        return renderer;
    }



    public void setHeaderRenderer(TableCellRenderer renderer) {
        if (renderer != null) {
            this.renderer = renderer;
        }
    }



    public Object getHeaderValue() {
        return text;
    }



    public Dimension getSize(JTable table) {
        Component comp = renderer.getTableCellRendererComponent(table, getHeaderValue(), false, false, -1, -1);
        int height = comp.getPreferredSize().height;
        int width = 0;
        Enumeration enumer = v.elements();
        while (enumer.hasMoreElements()) {
            Object obj = enumer.nextElement();
            if (obj instanceof TableColumn) {
                TableColumn aColumn = (TableColumn) obj;
                width += aColumn.getWidth();
                width += margin;
            } else {
                width += ((ColumnGroup) obj).getSize(table).width;
            }
        }
        return new Dimension(width, height);
    }



    public void setColumnMargin(int margin) {
        this.margin = margin;
        Enumeration enumer = v.elements();
        while (enumer.hasMoreElements()) {
            Object obj = enumer.nextElement();
            if (obj instanceof ColumnGroup) {
                ((ColumnGroup) obj).setColumnMargin(margin);
            }
        }
    }



    public int getHorizontalAlignement() {
        return horizontalAlignement;
    }



    public void setHorizontalAlignement(int horizontalAlignement) {
        this.horizontalAlignement = horizontalAlignement;
    }



    public int getVerticalAlignement() {
        return verticalAlignement;
    }



    public void setVerticalAlignement(int verticalAlignement) {
        this.verticalAlignement = verticalAlignement;
    }



    public String getText() {
        return text;
    }



    public void setText(String text) {
        this.text = text;
    }

    private class ColumnGroupRenderer extends DefaultTableCellRenderer {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JTableHeader header = table.getTableHeader();
            if (header != null && header.getForeground() != null) {
                setForeground(header.getForeground());
            } else {
                setForeground(UIManager.getColor("TableHeader.foreground")); //$NON-NLS-1$
            }

            if (header != null && header.getBackground() != null) {
                setBackground(header.getBackground());
            } else {
                setBackground(UIManager.getColor("TableHeader.background")); //$NON-NLS-1$
            }

            if (header != null && header.getBorder() != null) {
                setBorder(header.getBorder());
            } else {
                setBorder(UIManager.getBorder("TableHeader.cellBorder")); //$NON-NLS-1$
            }

            if (header != null && header.getFont() != null) {
                setFont(header.getFont());
            } else {
                setFont(UIManager.getFont("TableHeader.font")); //$NON-NLS-1$
            }

            if (horizontalAlignement >= 0) {
                setHorizontalAlignment(horizontalAlignement);
            } else {
                setHorizontalAlignment(JLabel.CENTER);
            }

            if (verticalAlignement >= 0) {
                setVerticalAlignment(verticalAlignement);
            } else {
                setVerticalAlignment(JLabel.CENTER);
            }

            setText((value == null) ? StrUtil.EMPTY_STRING : value.toString());
            return this;
        }
    }

}
