package com.dolphland.client.util.event;

/**
 * Ev�nement d�livr� lorsqu'on d�marre le mode inactif
 * 
 * @author JayJay
 * 
 */
public class UIStartDisabledModeEvt extends UIEvent {

    /**
     * Texte a afficher
     */
    private String msg;

    /**
     * Indique si la souris peut traverser ou non
     */
    private boolean mouseTraversable;



    public UIStartDisabledModeEvt(String msg, boolean mouseTraversable) {
        super(AppEvents.UI_START_DISABLED_MODE_EVT);
        setMsg(msg);
        setMouseTraversable(mouseTraversable);
    }



    public String getMsg() {
        return msg;
    }



    public void setMsg(String msg) {
        this.msg = msg;
    }



    public boolean isMouseTraversable() {
        return mouseTraversable;
    }



    public void setMouseTraversable(boolean mouseTraversable) {
        this.mouseTraversable = mouseTraversable;
    }
}
