package com.dolphland.client.util.i18n.varlist;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;

/**
 * <p>
 * Class to be used for parsing variables defined as a <code>String</code>
 * </p>
 * <p>
 * <b><u>Example</u></b>:<br>
 * Let the <code>input</code> string be initialized with the following string:<br>
 * 
 * <pre>
 * string var1="some text",int num=123,float real=123.456,date birth="20/05/1960 14:45:12"
 * </pre>
 * 
 * <code>VarParser</code> should be used this way :<br>
 * 
 * <pre>
 * 
 * 
 * Variables vars = VarParser.parse(input);
 * </pre>
 * 
 * </p>
 * <p>
 * Syntax is the following:<br>
 * 
 * <pre>
 * variables : variable (',' variable)*
 * 
 * variable : type name '=' value
 * 
 * type : 'string' | 'int' | 'float' | 'char' | 'date'
 * </pre>
 * 
 * <b>Example:</b><br>
 * 
 * <pre>
 * string text="some text",int num=123,float real=123.456,date birth="22/03/1975 10:21:45"
 * </pre>
 * 
 * </p>
 * 
 * @author JayJay
 */
public class VarParser {

    private static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy HH:mm:ss"; //$NON-NLS-1$



    /**
     * <p>
     * Parses the input string to extract the variable list.
     * </p>
     * <p>
     * Note: Default date format is assumed to be "dd/MM/yyyy HH:mm:ss"
     * </p>
     * 
     * @param input
     *            The input string to be parsed that contains variables
     *            definitions.
     * @return A {@link Variables} instance that holds all variables information
     *         and methods for extracting, values, types and names.
     */
    public static Variables parse(String input) {
        return parse(input, DEFAULT_DATE_FORMAT);
    }



    /**
     * <p>
     * Parses the input string to extract the variable list using the specified
     * date format pattern for parsing date variables.
     * </p>
     * <p>
     * Note: Default date format is assumed to be "dd/MM/yyyy HH:mm:ss"
     * </p>
     * 
     * @param input
     *            The input string to be parsed that contains variables
     *            definitions.
     * @param dateFormat
     *            The date format pattern to be used for parsing date variables,
     *            see {@link SimpleDateFormat} for pattern description
     * @return A {@link Variables} instance that holds all variables information
     *         and methods for extracting, values, types and names.
     */
    public static Variables parse(String input, String dateFormat) {
        ANTLRStringStream ass = new ANTLRStringStream(input);
        VarListLexer lexer = new VarListLexer(ass);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        VarListParser parser = new VarListParser(tokenStream);
        parser.setDateFormatPattern(dateFormat);
        Variables out = null;
        try {
            out = parser.variables();
        } catch (VarParseException e) {
            throw e;
        } catch (Exception e) {
            throw new VarParseException("Could not parse properties from <%s>", e, input); //$NON-NLS-1$
        }
        return out;
    }



    /**
     * <p>
     * Format the specified variables to a string that can be parsed again with
     * {@link VarParser#parse(String)}
     * </p>
     * <p>
     * This method is the symetric of {@link VarParser#parse(String)}
     * </p>
     * 
     * @param variables
     *            The variables to be represented as a string
     * @return A string representing the specified variables. This string is
     *         parseable again to give a {@link Variables} instance aquivalent
     *         to the one passed in parameter.
     */
    public static String toString(Variables variables) {
        return toString(variables, DEFAULT_DATE_FORMAT);
    }



    /**
     * <p>
     * Format the specified variables to a string that can be parsed again with
     * {@link VarParser#parse(String, String)} with the same date format
     * </p>
     * <p>
     * This method is the symetric of {@link VarParser#parse(String, String)}
     * </p>
     * 
     * @param variables
     *            The variables to be represented as a String
     * @param dateFormat
     *            The date format to be used for formating date variables.See
     *            {@link SimpleDateFormat} for the date format pattern syntax.
     * @return A string representing the specified variables. This string is
     *         parseable again to give a {@link Variables} instance aquivalent
     *         to the one passed in parameter.
     */
    public static String toString(Variables variables, String dateFormat) {
        StringBuilder out = new StringBuilder();
        SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
        List<Variable> vars = variables.getVariables();
        Iterator<Variable> it = vars.iterator();
        while (it.hasNext()) {
            Variable v = it.next();
            String strVal = "null"; //$NON-NLS-1$
            switch (v.getType()) {
                case STRING : {
                    Object val = v.getValue();
                    if (val != null) {
                        strVal = val.toString();
                        strVal = strVal.replaceAll("\"", "\\\\\""); //$NON-NLS-1$ //$NON-NLS-2$
                        strVal = strVal.replaceAll("\n", "\\\\n"); //$NON-NLS-1$ //$NON-NLS-2$
                        strVal = strVal.replaceAll("\t", "\\\\t"); //$NON-NLS-1$ //$NON-NLS-2$
                        strVal = strVal.replaceAll("\r", "\\\\r"); //$NON-NLS-1$ //$NON-NLS-2$
                        strVal = "\"" + strVal + "\""; //$NON-NLS-1$ //$NON-NLS-2$
                    }
                    out.append("string ").append(v.getName()).append("=").append(strVal); //$NON-NLS-1$ //$NON-NLS-2$
                    break;
                }
                case CHAR : {
                    Object val = v.getValue();
                    if (val != null) {
                        strVal = v.getValue().toString();
                    }
                    out.append("char ").append(v.getName()).append("='").append(strVal).append("'"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    break;
                }
                case DATE : {
                    Date val = (Date) v.getValue();
                    if (val != null) {
                        strVal = v.getValue().toString();
                        strVal = dateFormatter.format(val);
                    }
                    out.append("date ").append(v.getName()).append("=\"").append(strVal).append("\""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    break;
                }
                case FLOAT : {
                    Number val = (Number) v.getValue();
                    if (val != null) {
                        strVal = v.getValue().toString();
                    }
                    out.append("float ").append(v.getName()).append("=").append(strVal); //$NON-NLS-1$ //$NON-NLS-2$
                    break;
                }
                case INTEGER : {
                    Number val = (Number) v.getValue();
                    if (val != null) {
                        strVal = v.getValue().toString();
                    }
                    out.append("int ").append(v.getName()).append("=").append(strVal); //$NON-NLS-1$ //$NON-NLS-2$
                    break;
                }
            }
            if (it.hasNext()) {
                out.append(","); //$NON-NLS-1$
            }
        }
        return out.toString();
    }

}
