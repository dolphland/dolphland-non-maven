package com.dolphland.client.util.application;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import com.dolphland.client.util.event.AppClosedEvt;
import com.dolphland.client.util.event.CloseAppRequestEvt;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.EventMulticaster;
import com.dolphland.client.util.event.FwkEvents;
import com.dolphland.client.util.event.KeyManagerAppEvents;
import com.dolphland.client.util.i18n.MessageFormater;

/**
 * D�finit la frame principale pouvant contenir un titre, un sous-titre et un
 * ent�te
 * 
 * @author JayJay
 * @author jeremy.scafi
 */
public abstract class MasterFrame extends JFrame implements Messenger {

    private final static Logger log = Logger.getLogger(MasterFrame.class);

    private static final MessageFormater fmt = MessageFormater.getFormater(MasterFrame.class);

    private WorkPane workPane;

    private KeyManagerAppEvents keyManagerAppEvent = null;



    public MasterFrame() {
        super();
        EventMulticaster.getInstance().subscribe(new CloseAppSubscriber()).forEvent(FwkEvents.APP_CLOSED_EVT);

        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                if (showConfirmDialog(fmt.format("MasterFrame.RS_CONFIRM_EXIT"))) { //$NON-NLS-1$
                    CloseAppRequestEvt evt = new CloseAppRequestEvt();
                    evt.setSystemExitOnSuccess(true);
                    EDT.postEvent(evt);
                }
            }
        });

        // Affectation d'un manager de touches par d�faut
        setKeyManagerAppEvent(new KeyManagerAppEvents());
    }



    /** M�thode fournissant la version */
    protected abstract String getVersion();



    /** M�thode cr�ant le workpane */
    protected abstract WorkPane createWorkPane();



    /**
     * Traitements � r�aliser lors de l'initialisation de la Frame, par
     * init(args) (� d�finir)
     */
    protected abstract void doInit(String[] args);



    /** M�thode d'initialisation de la Frame */
    public void init(String[] args) {
        try {
            doInit(args);
        } catch (Exception e) {
            log.error("init(): Error while initializing application !", e); //$NON-NLS-1$
            showErrorMessage(fmt.format("MasterFrame.RS_LAUNCH_FAILURE")); //$NON-NLS-1$
        }

        // Installe le listener des �v�nements clavier globaux � l'application
        if (getKeyManagerAppEvent() != null) {
            getKeyManagerAppEvent().registerKeyEvents();
        }
    }



    public void setTitle(String title) {
        super.setTitle(title + " (" + getVersion() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
    }



    public KeyManagerAppEvents getKeyManagerAppEvent() {
        return keyManagerAppEvent;
    }



    public void setKeyManagerAppEvent(KeyManagerAppEvents keyManagerAppEvent) {
        this.keyManagerAppEvent = keyManagerAppEvent;
    }



    /**
     * Fournit le workpane
     */
    public WorkPane getWorkPane() {
        if (workPane == null) {
            workPane = createWorkPane();
        }
        return workPane;
    }

    private class CloseAppSubscriber extends DefaultEventSubscriber {

        public void eventReceived(final AppClosedEvt car) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    dispose();
                }
            });
        }
    }
}
