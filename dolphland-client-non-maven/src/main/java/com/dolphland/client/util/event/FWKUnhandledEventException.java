/*
 * $Log: FWKUnhandledEventException.java,v $
 * Revision 1.1 2007/04/06 15:34:49 bvatan
 * - d�placement du multicaster vers fwk-common
 */
package com.dolphland.client.util.event;

import com.dolphland.client.util.exception.AppInfrastructureException;

/**
 * Lev�e lorsqu'un �v�nement n'a pas �t� g�r� par un
 * <code>EventSubscriber</code>
 * 
 * @author JayJay
 */
public class FWKUnhandledEventException extends AppInfrastructureException {

    private Event unhandledEvent;



    public FWKUnhandledEventException(Event evt) {
        this.unhandledEvent = evt;
    }



    public FWKUnhandledEventException(String message, Event evt) {
        super(message);
        this.unhandledEvent = evt;
    }



    public FWKUnhandledEventException(Throwable cause, Event evt) {
        super(cause);
        this.unhandledEvent = evt;
    }



    public FWKUnhandledEventException(String message, Throwable cause, Event evt) {
        super(message, cause);
        this.unhandledEvent = evt;
    }



    /**
     * Retourne l'�v�nement non g�r�
     */
    public Event getUnhandledEvent() {
        return unhandledEvent;
    }

}
