package com.dolphland.client.util.application.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.border.LineBorder;

class StyledLineBorder extends LineBorder{

	private final static int DEFAULT_TAILLE_LIGNES = 30;
	
	private int tailleLignes;
	private int offsetX1;
	private int offsetY1;
	private int offsetX2;
	private int offsetY2;
	
    public StyledLineBorder(Color color) {
		super(color);
		this.tailleLignes = DEFAULT_TAILLE_LIGNES;
	}

    public StyledLineBorder(Color color, int thickness) {
		super(color, thickness);
		this.tailleLignes = DEFAULT_TAILLE_LIGNES;
	}

    public StyledLineBorder(Color color, int thickness, int tailleLigne) {
		super(color, thickness);
		this.tailleLignes = tailleLigne;
	}
	

	public int getOffsetX1() {
		return offsetX1;
	}

	public void setOffsetX1(int offsetX1) {
		this.offsetX1 = offsetX1;
	}

	public int getOffsetX2() {
		return offsetX2;
	}

	public void setOffsetX2(int offsetX2) {
		this.offsetX2 = offsetX2;
	}

	public int getOffsetY1() {
		return offsetY1;
	}

	public void setOffsetY1(int offsetY1) {
		this.offsetY1 = offsetY1;
	}

	public int getOffsetY2() {
		return offsetY2;
	}

	public void setOffsetY2(int offsetY2) {
		this.offsetY2 = offsetY2;
	}

	/**
     * Paints the border for the specified component with the 
     * specified position and size.
     * @param c the component for which this border is being painted
     * @param g the paint graphics
     * @param x the x position of the painted border
     * @param y the y position of the painted border
     * @param width the width of the painted border
     * @param height the height of the painted border
     */
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        Color oldColor = g.getColor();
        int i;

        g.setColor(lineColor);
        for(i = 0; i < thickness; i++)  {
        	g.drawLine(x+i+offsetX1+1, y+i+offsetY1+1, x+i+offsetX1+1 + tailleLignes, y+i+offsetY1+1);
        	g.drawLine(x+i+offsetX1+1, y+i+offsetY1+1, x+i+offsetX1+1, y+i+offsetY1+1 + tailleLignes);
        	
        	g.drawLine(width-i-offsetX2-2, height-i-offsetY2-2, width-i-offsetX2-2 - tailleLignes, height-i-offsetY2-2);
        	g.drawLine(width-i-offsetX2-2, height-i-offsetY2-2, width-i-offsetX2-2, height-i-offsetY2-2 - tailleLignes);
        }
        g.setColor(oldColor);
    }
}
