package com.dolphland.client.util.user;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import com.dolphland.client.AppConstants;
import com.dolphland.client.util.animator.AnimatedComponent;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.UIShowCtxInfoEvt;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.image.ImageLabel;
import com.dolphland.client.util.image.ImagePanel;
import com.dolphland.client.util.image.ImagePosition;
import com.dolphland.client.util.image.ImageUtil;
import com.dolphland.client.util.url.UrlUtil;
import com.dolphland.core.ws.data.User;

/**
 * User component
 * 
 * @author JayJay
 * 
 */
public class UserComponent extends AnimatedComponent {

    private static final MessageFormater FMT = MessageFormater.getFormater(UserComponent.class);

    // Default background color
    private final static Color DEFAULT_BACKGROUND_COLOR = Color.WHITE;

    // Default size
    public final static int DEFAULT_WIDTH = 80;
    public final static int DEFAULT_HEIGHT = 80;

    // Selection's color
    private final static Color COLOR_SELECTION = Color.RED;
    
    // Image panel
    private ImagePanel imagePanel;

    // User
    private User user;



    /**
     * Constructor with user
     * 
     * @param user
     *            User
     */
    public UserComponent(User user) {
        super(user.getId());
        setBackground(DEFAULT_BACKGROUND_COLOR);
        setImage(ImageUtil.getImage(UrlUtil.getUrl(AppConstants.URL_IMAGE_AVATAR + user.getAvatar())));
        setTypeBorder(TYPE_BORDER_ROUNDED_BORDER);
        setSelectedEnabled(true);
        setSelectedBackground(COLOR_SELECTION);
        setPreferredSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
        setLayout(new BorderLayout());
        add(BorderLayout.CENTER, getImagePanel());
        setUser(user);
    }



    private ImagePanel getImagePanel() {
        if (imagePanel == null) {
            imagePanel = new ImagePanel(4, 4, 10, 10);
        }
        return imagePanel;
    }
    
    
    
    private ImageLabel createImageLabel() {
        ImageLabel out = null;
        if (user != null && user.isConnected()) {
            if (user.isOffline()) {
                out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_BUTTON_RED));
                out.setToolTipText(FMT.format("UserComponent.RS_USER_OFFLINE", user.getFirstName(), user.getLastName()));
            } else {
                out = new ImageLabel(ImageUtil.getImageIcon(AppConstants.PATH_IMAGE_ICON_BUTTON_GREEN));
                out.setToolTipText(FMT.format("UserComponent.RS_USER_ONLINE", user.getFirstName(), user.getLastName()));
            }
        }
        return out;
    }
    
    
    
    /**
     * Get user id
     * 
     * @return User id
     */
    public String getUserId() {
        return user == null ? null : user.getId();
    }



    /**
     * Set user
     * 
     * @param user
     *            User
     */
    public void setUser(User user) {
        this.user = user;
        getImagePanel().setImage(ImagePosition.BOTTOM_RIGHT, createImageLabel());
    }



    /**
     * Show dialog bubble
     * 
     * @param content
     *            Content
     */
    public void showDialogBubble(String content) {
        UIShowCtxInfoEvt evt = new UIShowCtxInfoEvt(this, content, UIShowCtxInfoEvt.TOP);
        evt.setLifeTime(5000);
        EDT.postEvent(evt);
    }
}
