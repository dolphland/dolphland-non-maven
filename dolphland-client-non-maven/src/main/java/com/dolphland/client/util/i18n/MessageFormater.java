package com.dolphland.client.util.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.event.DefaultEventSubscriber;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.Event;
import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.Schedule;
import com.dolphland.client.util.exception.AppInfrastructureException;
import com.dolphland.client.util.stream.StreamUtil;
import com.ibm.icu.text.MessageFormat;

/**
 * <p>
 * Intended for I18N messages formating purpose.<br>
 * <br>
 * Instances of this class should be declared as static final on top of each
 * class where string references have to be internationalized
 * </p>
 * <p>
 * Underlying {@link ResourceBundle} are loaded from the classpath and cached
 * for limiting memory footprint. It is thus possible for
 * {@link MessageFormater} to be declared as <code>private static final</code>
 * fields in each class without overloading memory in large scale situations.
 * </p>
 * <b>Declaration example :</b><br>
 * 
 * <code>
 * 
 * public class MyClass {<br>
 * <br>
 *     private static final MessageFormater fmt = MessageFormater.getFormater(MyClass.class);<br>
 * <br>
 * }<br>
 * </code> <br>
 * 
 * @author JayJay
 */
public class MessageFormater {

    private final static Logger LOG = Logger.getLogger(MessageFormater.class);

    private static final String HTTP_SCHEME = "http"; //$NON-NLS-1$

    /**
     * Locale instance to be used as default locale key in the cache. internal
     * use only
     */
    private static final Locale DEFAULT_LOCALE_KEY = new Locale("MessageFormaterCacheDefaultLocaleForInternalUse"); //$NON-NLS-1$

    /* Locale attached to the current thread, if any */
    private static final ThreadLocal<Locale> contextLocale = new ThreadLocal<Locale>();

    /** formatters cache */
    private static final Map<String, Map<String, MessageFormater>> formaterCache = Collections.synchronizedMap(new TreeMap<String, Map<String, MessageFormater>>());

    /** ResourceBundle to be used for messages loading */
    private volatile ResourceBundle bundle;

    /** URI of the resource bundle represented by this instance */
    private volatile URI bundleUri;

    /** duration between each bundle reloading. Reloading enabled only if > 0 */
    private volatile long reloadDelay;

    private Schedule schedule;

    /**
     * Locale used when this instance was created. This does not necessarily
     * reflects the actual locale that matches the loaded message bundle.
     * See constructor code for details
     */
    private Locale locale;

    /** path of the resource bundle loaded by this instance */
    private String bundlePath;

    /** ClassLoader that was used at creation time to load the resource bundle */
    private ClassLoader loader;

    /**
     * flag that indicates if this formatter refers to the default bundle, i.e.
     * messages.properties
     */
    private volatile boolean isDefaultBundle;

    /**
     * Full bundle name that was actually loaded by this formatter. This might
     * not match the Locale. For example, if Locale is fr_CA, actualBundleName
     * might be messages_fr.properties, not messages_fr_CA.properties because it
     * is missing. Likewise, it might as well be messages.properties if no other
     * bundle is available in classpath
     */
    private String actualBundleName;



    /**
     * <p>
     * Builds a {@link MessageFormater} that will lookup message from the bundle
     * whose path is specified
     * </p>
     * <p>
     * If the specified {@link Locale} is null then this is the default bundle
     * for the specified <code>bundlePath</code> that will be looked up. For
     * example, the default bundle if often named
     * <code>messages.properties</code>
     * </p>
     * 
     * @param bundlePath
     *            The bundle path where message will be looked up
     */
    private MessageFormater(Locale locale, String bundlePath, ClassLoader cl) {
        String propertySuffix = ".properties"; //$NON-NLS-1$
        URL resource = null;
        // bundle path where dots have been replaced with slashes
        String normalizedBundlePath = bundlePath.replaceAll("\\.", "/"); //$NON-NLS-1$ //$NON-NLS-2$
        String bundleName = null;
        if (locale != null) {
            // a Locale has been provided as a parameter : find the bundle for
            // that Locale
            String localeSuffix = locale.toString();
            if (localeSuffix.length() == 0) {
                // This corrects some strange behavior in Locale where
                // new Locale("", "", "VARIANT").toString == ""
                localeSuffix = "__" + locale.getVariant(); //$NON-NLS-1$
            }
            bundleName = normalizedBundlePath + "_" + localeSuffix + propertySuffix; //$NON-NLS-1$
            resource = cl.getResource(bundleName);
            if (LOG.isDebugEnabled()) {
                LOG.debug("MessageFormater(): Looking for bundle " + bundleName + " :" + (resource == null ? "not found" : "found"));
            }
            while (resource == null) {
                int idx = bundleName.lastIndexOf('_');
                if (idx < 0) {
                    break;
                }
                bundleName = bundleName.substring(0, idx);
                resource = cl.getResource(bundleName + propertySuffix);
                if (LOG.isDebugEnabled()) {
                    LOG.debug("MessageFormater(): Looking for bundle " + bundleName + " :" + (resource == null ? "not found" : "found"));
                }
            }
        } else {
            // we don't have a Locale provided as parameter : find the default
            // bundle for the specified path
            bundleName = normalizedBundlePath + propertySuffix;
            resource = cl.getResource(bundleName);
            if (LOG.isDebugEnabled()) {
                LOG.debug("MessageFormater(): Looking for bundle " + bundleName + " :" + (resource == null ? "not found" : "found"));
            }
        }
        this.actualBundleName = bundleName;
        if (resource == null) {
            //          throw new VisionInfrastructureException("Resource bundle not found in classpath ! " + bundlePath); //$NON-NLS-1$
            LOG.error("MessageFormater(): No properties resource bundle found in classpath for bundleName=" + bundleName + " ! Using a ghost empty bundle for fail safety...");
            this.isDefaultBundle = true;
            try {
                // create a ghost bundle that contains absolutely no key at all
                this.bundle = new PropertyResourceBundle(new InputStream() {

                    @Override
                    public int read() throws IOException {
                        return -1;
                    }
                });
            } catch (IOException ioe) {
                // will never occur
            }
        } else {
            this.isDefaultBundle = bundleName.indexOf('_') < 0;
            InputStream is = null;
            try {
                is = resource.openStream();
                this.bundle = new PropertyResourceBundle(is);
            } catch (Exception e) {
                throw new AppInfrastructureException("Failed to lookup bundle from classpath ! %s", e, bundleName); //$NON-NLS-1$
            } finally {
                StreamUtil.safeClose(is);
            }
        }
        this.locale = locale;
        this.bundlePath = bundlePath;
        this.loader = cl;
    }



    /**
     * <p>
     * Builds a formatter that will lookup messages from the specified URI
     * </p>
     * 
     * @param bundleUri
     *            The bundle URI to lookup message from.
     */
    private MessageFormater(URI bundleUri, long reloadEverySeconds) {
        this.bundleUri = bundleUri;
        this.bundle = loadBundle(bundleUri);
        this.reloadDelay = reloadEverySeconds * 1000;
        if (this.reloadDelay > 0) {
            EDT.start();
            BundleReloader reloader = new BundleReloader();
            EDT.subscribe(reloader).forEvents(reloader.destination());
            this.schedule = Schedule.event(new Event(reloader.destination())).interval(this.reloadDelay);
            EDT.schedule(this.schedule);
        }
    }

    /**
     * <p>
     * Notified by multicaster every period of time when this formatter has been
     * configured to refresh its resource bundle.
     * </p>
     */
    private class BundleReloader extends DefaultEventSubscriber {

        private EventDestination MY_DESTINATION = new EventDestination();



        public EventDestination destination() {
            return MY_DESTINATION;
        }



        public void eventReceived(Event e) {
            if (MessageFormater.this.bundleUri != null) {
                try {
                    ResourceBundle bundle = loadBundle(bundleUri);
                    MessageFormater.this.bundle = bundle;
                } catch (Exception ex) {
                    LOG.error("eventReceived(): Failed to reload bundle from URI " + bundleUri.toString(), ex); //$NON-NLS-1$
                }
            }
        }
    }



    /**
     * <p>
     * Get a {@link MessageFormater} for the specified class.
     * </p>
     * <p>
     * The returned formatter assumes that resource bundles can be found in the
     * specified class' package.
     * </p>
     * <p>
     * Resource bundles are assumed to be properties files whose names starts
     * with <code>messages</code> and ends with <code>.properties</code><br>
     * Example of valid names could be :<br>
     * <li><code>messages.properties</code></li>
     * <li><code>messages_en.properties</code></li>
     * <li><code>messages_de.properties</code></li>
     * <li><code>messages_fr_FR.properties</code></li>
     * <li><code>messages_fr_CA.properties</code></li>
     * </p>
     * <br>
     * 
     * @param clazz
     *            The class for which the formatter is to be returned
     * @return The message formatter configured for the specified class and
     *         locale.
     * @param locale
     *            The locale to be used for looking up the underlying resource
     *            bundle.
     * @throws MissingResourceException
     *             If no resource bundle could be found in the package of the
     *             specified class.
     */
    public static final MessageFormater getFormater(Class<?> clazz, Locale locale) {
        return getFormater(clazz, "messages", locale); //$NON-NLS-1$
    }



    /**
     * <p>
     * Get a formater for the default Locale
     * </p>
     * <p>
     * See {@link Locale#getDefault()}<br>
     * See {@link MessageFormater#getFormater(Class, String, Locale)}
     * </p>
     * 
     * @param clazz
     *            The class for which the formater is to be returned
     * @return The message formater configured for the specified class with
     *         default locale.
     */
    public static final MessageFormater getFormater(Class<?> clazz) {
        return getFormater(clazz, "messages", Locale.getDefault()); //$NON-NLS-1$
    }



    public static final MessageFormater getFormater(Class<?> clazz, ClassLoader cl) {
        return getFormater(clazz, "messages", Locale.getDefault(), cl); //$NON-NLS-1$
    }



    /**
     * <p>
     * Get a {@link MessageFormater} for the specified class and whose
     * underlying property resource bundle name is specified.
     * </p>
     * <p>
     * The returned formater assumes that resource bundles can be found in the
     * specified class package.
     * </p>
     * <p>
     * Resource bundles are assumed to be properties files whose names starts
     * with the specified <code>bundleRootName</code> parameter and ends with
     * <code>.properties</code><br>
     * <b>Example :</b><br>
     * if <code>bundleRootName</code> has been set to "appMessages", then valid
     * names could be :<br>
     * <li><code>appMessages.properties</code></li>
     * <li><code>appMessages_en.properties</code></li>
     * <li><code>appMessages_de.properties</code></li>
     * <li><code>appMessages_fr_FR.properties</code></li>
     * <li><code>appMessages_fr_CA.properties</code></li>
     * </p>
     * <br>
     * <p>
     * The resource bundle will be determined from the specified {@link Locale}
     * </p>
     * 
     * @param clazz
     *            The class for which the formater is to be returned
     * @param bundleRootName
     *            The root name of the underlying property resource bundle file
     * @param locale
     *            The locale to be used to lookup the underlying resource bundle
     * @return The message formater configured for the specified class,
     *         bundleRootName and locale.
     * @throws MissingResourceException
     *             If no resource bundle could be found in the package of the
     *             specified class.
     */
    public static final MessageFormater getFormater(Class<?> clazz, String bundleRootName, Locale locale) {
        return getFormater(clazz, bundleRootName, locale, clazz.getClassLoader());
    }



    /**
     * <p>
     * Get a {@link MessageFormater} for the specified class and whose
     * underlying property resource bundle name is specified.
     * </p>
     * <p>
     * The returned formater assumes that resource bundles can be found in the
     * specified class package.
     * </p>
     * <p>
     * Resource bundles are assumed to be properties files whose names starts
     * with the specified <code>bundleRootName</code> parameter and ends with
     * <code>.properties</code><br>
     * <b>Example :</b><br>
     * if <code>bundleRootName</code> has been set to "appMessages", then valid
     * names could be :<br>
     * <li><code>appMessages.properties</code></li>
     * <li><code>appMessages_en.properties</code></li>
     * <li><code>appMessages_de.properties</code></li>
     * <li><code>appMessages_fr_FR.properties</code></li>
     * <li><code>appMessages_fr_CA.properties</code></li>
     * </p>
     * <br>
     * <p>
     * The resource bundle will be determined from the specified {@link Locale}
     * </p>
     * 
     * @param clazz
     *            The class for which the formater is to be returned
     * @param bundleRootName
     *            The root name of the underlying property resource bundle file
     * @param locale
     *            The locale to be used to lookup the underlying resource bundle
     * @param cl
     *            The {@link ClassLoader} to be used to load the resource bundle
     * @return The message formater configured for the specified class,
     *         bundleRootName and locale.
     * @throws MissingResourceException
     *             If no resource bundle could be found in the package of the
     *             specified class.
     */
    public static final MessageFormater getFormater(Class<?> clazz, String bundleRootName, Locale locale, ClassLoader cl) {
        AssertUtil.notNull(clazz, "Cannot return formater for null classes !"); //$NON-NLS-1$
        AssertUtil.notEmpty(bundleRootName, "bundleRootName is null or empty !"); //$NON-NLS-1$
        AssertUtil.notNull(locale, "Cannot specify null locale !"); //$NON-NLS-1$
        String bundlePath = clazz.getPackage().getName() + "." + bundleRootName; //$NON-NLS-1$
        return lookupFormater(locale, bundlePath, cl);
    }



    /**
     * <p>
     * Get a {@link MessageFormater} for the specified class and whose
     * underlying property resource bundle name is specified.
     * </p>
     * <p>
     * The locale used will be the default locale using
     * {@link Locale#getDefault()}
     * </p>
     * <p>
     * See {@link MessageFormater#getFormater(Class, String, Locale)} form more
     * details.
     * </p>
     * 
     * @param clazz
     *            The class to lookup the resource bundle from.
     * @param bundleRootName
     *            The resource bundle root name.
     * @return The message formater configured for the specified class,
     *         bundleRootName and default locale
     */
    public static final MessageFormater getFormater(Class<?> clazz, String bundleRootName) {
        return getFormater(clazz, bundleRootName, Locale.getDefault());
    }



    /**
     * <p>
     * Get a {@link MessageFormater} whose property resource bundle classpath is
     * specified.
     * </p>
     * <p>
     * The {@link Locale} used to fetch the underlying resource bundle will be
     * the default locale ({@link Locale#getDefault()})
     * </p>
     * <p>
     * See {@link MessageFormater#getFormater(String, Locale)} for more
     * information.
     * <p>
     * 
     * @param bundlePath
     *            The classpath of the property resource bundle to be used for
     *            the returned {@link MessageFormater}
     * @param cl
     *            The {@link ClassLoader} to be used to load the resource bundle
     * @return The message formater build for the specified classpath bundlePath
     * @throws MissingResourceException
     *             If no resource bundle could be found for the specified
     *             classpath.
     * @see {@link MessageFormater#getFormater(String, Locale)}
     */
    public static final MessageFormater getFormater(String bundlePath, ClassLoader cl) {
        return lookupFormater(Locale.getDefault(), bundlePath, cl);
    }



    /**
     * <p>
     * Get a {@link MessageFormater} whose property resource bundle classpath is
     * specified.
     * </p>
     * <p>
     * The {@link Locale} used to fetch the underlying resource bundle will be
     * the default locale ({@link Locale#getDefault()})
     * </p>
     * <p>
     * See {@link MessageFormater#getFormater(String, Locale)} for more
     * information.
     * <p>
     * 
     * @param bundlePath
     *            The classpath of the property resource bundle to be used for
     *            the returned {@link MessageFormater}
     * @return The message formater build for the specified classpath bundlePath
     * @throws MissingResourceException
     *             If no resource bundle could be found for the specified
     *             classpath.
     * @see {@link MessageFormater#getFormater(String, Locale)}
     */
    public static final MessageFormater getFormater(String bundlePath) {
        return lookupFormater(Locale.getDefault(), bundlePath, MessageFormater.class.getClassLoader());
    }



    /**
     * <p>
     * Get a {@link MessageFormater} whose {@link Locale} and property resource
     * bundle classpath are specified.
     * </p>
     * <p>
     * <b>Example:</><br>
     * If bundlePath is <code>com.foo.bar.some.package.messages</code> and
     * locale is <code>en</code> then the full resolved resource bundle
     * classpath will be
     * <code>com.foo.bar.some.package.messages_en.properties</code>
     * </p>
     * 
     * @param bundlePath
     *            The property resoruce bundle classpath to ne used without any
     *            file extension and locale information:<br>
     *            ex. <code>com.foo.bar.messages</code>
     * @param cl
     *            The {@link ClassLoader} to be used to load the resource bundle
     * @param locale
     *            The {@link Locale} to be used for loading the specified
     *            resource bundle.
     * @return The message formater for the specified bundle path and locale.
     */
    public static final MessageFormater getFormater(String bundlePath, Locale locale, ClassLoader cl) {
        return lookupFormater(locale, bundlePath, cl);
    }



    /**
     * <p>
     * Get a {@link MessageFormater} whose {@link Locale} and property resource
     * bundle classpath are specified.
     * </p>
     * <p>
     * <b>Example:</><br>
     * If bundlePath is <code>com.foo.bar.some.package.messages</code> and
     * locale is <code>en</code> then the full resolved resource bundle
     * classpath will be
     * <code>com.foo.bar.some.package.messages_en.properties</code>
     * </p>
     * 
     * @param bundlePath
     *            The property resoruce bundle classpath to ne used without any
     *            file extension and locale information:<br>
     *            ex. <code>com.foo.bar.messages</code>
     * @param locale
     *            The {@link Locale} to be used for loading the specified
     *            resource bundle.
     * @return The message formater for the specified bundle path and locale.
     */
    public static final MessageFormater getFormater(String bundlePath, Locale locale) {
        return lookupFormater(locale, bundlePath, MessageFormater.class.getClassLoader());
    }



    /**
     * <p>
     * Get a message formater instance from the specified URI with no refresh
     * feature. The resource is loaded once and for all and won't be updated
     * until a fresh new instance of {@link MessageFormater} is requested for
     * the same URI.
     * </p>
     * <p>
     * This is a shortcut for
     * {@link MessageFormater#getFormaterFromURI(String, long)} with refresh
     * parameter set to zero.
     * </p>
     * 
     * @param uri
     *            the URI of the message resource to be loaded. See
     *            {@link MessageFormater#getFormaterFromURI(String, long)} for
     *            extended documentation
     */
    public static final MessageFormater getFormaterFromURI(String uri) {
        return getFormaterFromURI(uri, 0);
    }



    /**
     * <p>
     * <b><font style="color:red">Beware that using this API refresh
     * functionality is provided for development purpose ONLY and is subject to
     * memory leaks, CPU load and EventMulticaster locking if used
     * extensively.</font></b>
     * </p>
     * <p>
     * Get a new {@link MessageFormater} from the specied URI.
     * </p>
     * <p>
     * Currently, supported URI schemes are described in the following table.<br>
     * For each scheme, the remote resource must of the type described by the
     * following table:<br>
     * <table border=1>
     * <tr>
     * <td><b>Scheme</b></td>
     * <td><b>Resource type</b></td>
     * </tr>
     * <tr>
     * <td>http</td>
     * <td>Properties file to parsed by a {@link PropertyResourceBundle}</td>
     * </tr>
     * </table>
     * </p>
     * <p>
     * When providing URI with other schemes than those listed above, an
     * excpetion will be thrown.
     * </p>
     * 
     * @param uri
     *            The URI that point to the source of messages that this
     *            {@link MessageFormater} will rely on.<br>
     * @param reloadSeconds
     *            When set to a non zero positive value (i.e. &gt;0), the
     *            specified URI will be reloaded every specified seconds. This
     *            is true only when the URI scheme makes it possible.
     * @return A {@link MessageFormater} instance build on top of the specified
     *         message resource URI.
     */
    public static final MessageFormater getFormaterFromURI(String uri, long reloadSeconds) {
        AssertUtil.notEmpty(uri, "null or empty URI provided !"); //$NON-NLS-1$
        URI bundleUri = null;
        try {
            bundleUri = new URI(uri);
        } catch (Exception e) {
            throw new AppInfrastructureException("Invalid URI <%s> !", e, uri); //$NON-NLS-1$
        }
        if (HTTP_SCHEME.equalsIgnoreCase(bundleUri.getScheme())) {
            return new MessageFormater(bundleUri, reloadSeconds);
        } else {
            throw new AppInfrastructureException("Unsupported URI scheme <%s> !", bundleUri.getScheme()); //$NON-NLS-1$
        }
    }



    /**
     * <p>
     * Load a {@link PropertyResourceBundle} from the specified {@link URI}.
     * </p>
     * <p>
     * A connection is established with the resource pointed by the specified
     * URI, then the content is loaded and {@link ResourceBundle} is build from
     * that content. <br>
     * Content is expected to by parseable by a {@link PropertyResourceBundle}.
     * </p>
     * 
     * @param bundleUri
     *            the full uri of the resource to load.
     * @return The resoruce bundle that could be loaded from the specified URI.
     * @throws VisionInfrastructureException
     *             If some error while loading the resource or if the content of
     *             the resource is not compliant with
     *             {@link PropertyResourceBundle}
     */
    private static ResourceBundle loadBundle(URI bundleUri) {
        if (LOG.isInfoEnabled()) {
            LOG.info("loadBundle(): Loading bundle from URI... (" + bundleUri.toString() + ")"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        ResourceBundle out = null;
        URLConnection cnx = null;
        InputStream is = null;
        try {
            URL bundleUrl = bundleUri.toURL();
            cnx = bundleUrl.openConnection();
            cnx.setConnectTimeout(1000);
            cnx.setReadTimeout(1000);
            cnx.connect();
            is = cnx.getInputStream();
            out = new PropertyResourceBundle(is);
        } catch (Exception ioe) {
            throw new AppInfrastructureException("Could not read resource bundle from provided URI ! <%s>", ioe, bundleUri.toString()); //$NON-NLS-1$
        } finally {
            StreamUtil.safeClose(is);
        }
        return out;
    }



    /**
     * <p>
     * Lookup a {@link MessageFormater} by its undernlying
     * {@link ResourceBundle} path from the cache.
     * </p>
     * <p>
     * If no formater could be found in the cache, it is created and cached<br>
     * If no formater could be created because the underlying
     * {@link ResourceBundle} could not be found in classpath, a
     * {@link MissingResourceException} is thrown.
     * </p>
     * 
     * @param locale
     *            The {@link Locale} for which the returned
     *            {@link MessageFormater} should be looked up. Can be null, in
     *            that case this is the {@link MessageFormater} for the default
     *            bundle that will be returned (e.g: messages.properties).
     * @param bundlePath
     *            Path (in classpath) of the formater's underlying resource
     *            bundle to load
     * @param cl
     *            the {@link ClassLoader} to be used for loading
     *            {@link ResourceBundle} from the classpath.
     * @return The looked up {@link MessageFormater}
     * @throws MissingResourceException
     *             If no formater could be found in the cache or loaded from
     *             classpath
     */
    private static synchronized final MessageFormater lookupFormater(Locale locale, String bundlePath, ClassLoader cl) {
        AssertUtil.notEmpty(bundlePath, "null or empty bundle path provided !"); //$NON-NLS-1$
        String localeKey = (locale == null ? DEFAULT_LOCALE_KEY : locale).toString();
        Map<String, MessageFormater> cache = formaterCache.get(localeKey);
        if (cache == null) {
            cache = new TreeMap<String, MessageFormater>();
            formaterCache.put(localeKey, cache);
        }
        MessageFormater mf = cache.get(bundlePath);
        if (mf == null) {
            // here locale can still be null but constructor supports null
            // Locale.
            mf = new MessageFormater(locale, bundlePath, cl);
            cache.put(bundlePath, mf);
        }
        return mf;
    }



    /**
     * <p>
     * Return a {@link MessageFormater} that will lookup message from the
     * default {@link ResourceBundle} found at the specified
     * <code>bundlePath</code>
     * </p>
     * <p>
     * This is most of the time the {@link ResourceBundle} for the
     * <code>message.properties</code> file.
     * </p>
     * 
     * @param bundlePath
     *            The bundle path to be looked up. An example would be
     *            com.company.path.to.bundle.messages
     * @param cl
     *            the {@link ClassLoader} to be used for loading the bundle
     * @return The {@link MessageFormater} instance pointing to the default
     *         bundle at the specified path
     */
    private static final MessageFormater lookupDefaultFormater(String bundlePath, ClassLoader cl) {
        return lookupFormater(null, bundlePath, cl);
    }



    /**
     * <p>
     * Format the message for the specified key and arguments.
     * </p>
     * <p>
     * Message arguments are passed as an array of object. Thus, it is assumed
     * that message arguments have been specified on an index basis, i.e.
     * "There is {0,number,00} tubes on table {1}"
     * </p>
     * <p>
     * This method is based on the {@link MessageFormater#getString(String)} and
     * is thus guaranted to return non null strings.
     * </p>
     * 
     * @param key
     *            The message key (null not allowed)
     * @param args
     *            The message arguments
     * @return The formated message where arguments have been replaces by their
     *         corresponding values
     */
    public String format(String key, Object... args) {
        return MessageFormat.format(getString(key), args);
    }



    /**
     * <p>
     * Format the message corresponding to the specified key and arguments.
     * </p>
     * <p>
     * Message arguments are provided as a map where keys are arguments names.
     * This method should be used for messages where names have been used
     * instead of indexes.
     * </p>
     * <p>
     * i.e: The message could be
     * <b>"There is {nbTubes,number,000} on table {tableName}"</b><br>
     * <br>
     * The provided map should then contains :<br>
     * <br>
     * <b> "nbTubes"=Number(27)<br>
     * "tableName"="G27"<br>
     * </b> <br>
     * The resulting formated message would then be :<br>
     * <br>
     * <b>&quot;There is 027 tubes on table G27&quot;</b>
     * </p>
     * <p>
     * This method is based on the {@link MessageFormater#getString(String)} and
     * is thus guaranted to return non null strings.
     * </p>
     * 
     * @param key
     *            The key of the message to format
     * @param args
     *            The message argument indexed by argument names.
     * @return The message where arguments have been replaced by their
     *         corresponding values (looked up in the the provided map).
     */
    public String format(String key, Map<String, Object> args) {
        return MessageFormat.format(getString(key), args);
    }



    /**
     * <p>
     * Lookup the underlying message for the specified key, and return its
     * formatted representation
     * </p>
     * <p>
     * The formatted representation is the string resulting from the replacement
     * of message arguments by their corresponding values.
     * </p>
     * <p>
     * When invoking this method, no arguments is passed, therefore this method
     * should only be used for messages that contains no argument.
     * </p>
     * <p>
     * This method is based on the {@link MessageFormater#getString(String)} and
     * is thus guaranteed to return non null strings.
     * </p>
     * 
     * @param key
     *            The key of the message to format
     * @return The formated message for the provided key or null if no message
     *         could be found for that key.
     */
    public String format(String key) {
        return MessageFormat.format(getString(key), Collections.<String, Object> emptyMap());
    }



    /**
     * <p>
     * Get a {@link I18nMessage} instance for the specified message key
     * </p>
     * <p>
     * The returned {@link I18nMessage} can be used with its chained methods to
     * add arguments to the message.
     * </p>
     * <p>
     * <b>Example:</b><br>
     * <code>
     * MessageFormater fmt = new MessageFormater(...);<br>
     * I18nMessage message = fmt.message("MESSAGE_KEY").decimal("trs", 78.2).integer("nbTubes", 327);<br>
     * </code>
     * </p>
     * <p>
     * Not that {@link I18nMessage} can be passed to {@link VisionException}
     * constructor.
     * </p>
     * 
     * @param key
     *            The key of the message to return.
     * @return An {@link I18nMessage} reference that can be used to pass
     *         arguments and eventually format the final message
     */
    public I18nMessage message(String key) {
        return new I18nMessage(this, key);
    }



    /**
     * <p>
     * Return the message for the specified key from the underlying resource
     * bundle.<br>
     * If no such key exist in the resource bundle then return the
     * <code>whenMissing</code> parameter.
     * </p>
     * 
     * @param key
     *            The key of the message to get
     * @param whenMissing
     *            The message to return when the specified key has not been
     *            found in the underlying resource bundle (null allowed)
     * @return The message for the specified key of <code>whenMissing</code>
     *         value is no message exist for the key
     */
    public String getString(String key, String whenMissing) {
        MessageFormater formater = getFormaterToBeUsedFromContext();
        String out = null;
        boolean missingKey = false;
        try {
            out = formater.getStringNoFallback(key);
        } catch (MissingResourceException mre) {
            missingKey = true;
        }
        if (missingKey || out.length() == 0) {
            if (isDefaultBundle) {
                // key does not exist in default bundle : return whenMissing
                // value
                out = whenMissing;
            } else {
                // key is not found in the localized bundle : try the default
                // bundle
                try {
                    out = lookupDefaultFormater(bundlePath, this.loader).getStringNoFallback(key);
                } catch (MissingResourceException mre) {
                    out = whenMissing;
                }
            }
        }
        return out;
    }



    /**
     * <p>
     * Return the string for the specified key.
     * </p>
     * <p>
     * This method is guaranteed to return non null value.
     * </p>
     * <p>
     * When the key is missing in the underlying {@link ResourceBundle} then the
     * returned string is the key enclosed within '!' signs.<br>
     * <br>
     * <b>e.g.</b>: if the key is <code>RS_KEY</code> and that key is missing,
     * the returned string will be <code>!RS_KEY!</code>
     * </p>
     * 
     * @param key
     *            The key of the message to get
     * @return The message for the key or the key itself, surrounded by '!'
     *         characters if no such key exists in the underlying resource
     *         bundle.
     */
    public String getString(String key) {
        MessageFormater formater = getFormaterToBeUsedFromContext();
        String out = null;
        boolean missingKey = false;
        try {
            // get string for requested key on that specific formatter : do not
            // fall back to default formatter : we need to see if that key
            // actually exists for that formatter
            out = formater.getStringNoFallback(key);
        } catch (MissingResourceException mre) {
            missingKey = true;
        }
        MessageFormater defaultFormater = null;
        if (missingKey || out.length() == 0) {
            if (isDefaultBundle) {
                // key does not exist in default bundle : this is hopeless,
                // message cannot be resolved
                out = null;
                LOG.warn("getString(): Missing message key <" + key + "> : not found in default bundle <" + formater.actualBundleName + ">");
            } else {
                // key does not exist (or is empty) in the localized bundle
                // try the default bundle (i.e. messages.properties)
                try {
                    defaultFormater = lookupDefaultFormater(bundlePath, this.loader);
                    out = defaultFormater.getStringNoFallback(key);
                } catch (MissingResourceException mre) {
                    out = null;
                    LOG.warn("getString(): Missing message key <" + key + "> : not found in bundle <" + defaultFormater.actualBundleName + ">");
                }
            }
        }
        if (out == null) {
            // key does not exist anywhere: return key name surrounded by '!'
            // signs
            out = MessageFormat.format("!{0}!", key); //$NON-NLS-1$
        }
        // here out cannot be null
        return out;
    }



    private String getStringNoFallback(String key) throws MissingResourceException {
        // get string for request key on that specific formatter : do not
        // fall back to default formatter : we need to see if the key
        // actually exists for that formatter
        return bundle.getString(key);
    }



    /**
     * <p>
     * Returns the {@link MessageFormater} to be used to lookup messages from
     * according to the current Thread context.
     * </p>
     * <p>
     * First, the current Thread associated Locale is tested to check if a
     * Locale is associated or not.<br>
     * <ul>
     * <li>If a {@link Locale} is associated, it is used to lookup the
     * corresponding {@link MessageFormater} from the cache and that formatter
     * is returned.</li>
     * <li>If no {@link Locale} is associated to the current Thread then this
     * {@link MessageFormater} is returned</li>
     * </ul>
     * </p>
     * 
     * @return The {@link MessageFormater} to be to lookup message according to
     *         the current thread context.
     */
    private MessageFormater getFormaterToBeUsedFromContext() {
        MessageFormater out = null;
        Locale locale = contextLocale.get();
        if (locale == null) {
            out = this;
        } else {
            out = lookupFormater(locale, bundlePath, loader);
        }
        return out;
    }



    /**
     * <p>
     * Set the Locale which will be associated to the current thread when this
     * method is invoked.
     * </p>
     * <p>
     * If the specified Locale is not null, subsequent calls to :<br>
     * <ul>
     * <li>{@link MessageFormater#format(String)}</li>
     * <li>{@link MessageFormater#format(String, Map)}</li>
     * <li>{@link MessageFormater#format(String, Object...)}</li>
     * <li>{@link MessageFormater#getString(String)}</li>
     * <li>{@link MessageFormater#getString(String, String)}</li>
     * </ul>
     * <br>
     * Will use the bundle for the Locale associated to the thread invoking
     * these methods.
     * </p>
     * 
     * @param locale
     *            The {@link Locale} to be associated to the current thread.
     *            When null, the thread associated {@link Locale} is cleared (if
     *            one is associated) and {@link MessageFormater} will use the
     *            bundle selected at the time it was created.
     */
    public static final void setThreadLocale(Locale locale) {
        contextLocale.set(locale);
    }



    /**
     * <p>
     * Clear the {@link Locale} associated to the thread that invoke that method
     * </p>
     * <p>
     * After this method has been called, subsequent calls to :<br>
     * <ul>
     * <li>{@link MessageFormater#format(String)}</li>
     * <li>{@link MessageFormater#format(String, Map)}</li>
     * <li>{@link MessageFormater#format(String, Object...)}</li>
     * <li>{@link MessageFormater#getString(String)}</li>
     * <li>{@link MessageFormater#getString(String, String)}</li>
     * </ul>
     * <br>
     * Will use the bundle that was selected at {@link MessageFormater} creation
     * time.
     * </p>
     * 
     */
    public static final void clearThreadLocale() {
        setThreadLocale(null);
    }
}
