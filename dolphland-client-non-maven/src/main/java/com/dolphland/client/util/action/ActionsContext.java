package com.dolphland.client.util.action;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTML;

import org.apache.log4j.Logger;

import com.dolphland.client.util.chart.AbstractChart;
import com.dolphland.client.util.image.ImageLabel;
import com.dolphland.client.util.input.DateChooser;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.table.ProductTable;
import com.dolphland.client.util.transaction.MVCTxListener;

/**
 * Context for actions that manage actions and components binded
 * 
 * This class
 * 
 * @author JayJay
 */
public class ActionsContext {

    private final static Logger LOG = Logger.getLogger(ActionsContext.class);

    // Default UIActionEvt factory
    private UIActionEvtFactory<?> defaultUIActionEvtFactory;

    // Listeners for an action independently a component
    private Map<UIAction<UIActionEvt, ?>, ListenersMetaData> actionsListeners;

    // Listeners for a component independently an action
    private Map<JComponent, ListenersMetaData> componentsListeners;

    // Listeners for a component and an action
    private Map<JComponent, Map<UIAction<UIActionEvt, ?>, ListenersMetaData>> componentsActionsListeners;

    // Indicator that specify if all actions are enabled or disabled
    private boolean actionsEnabled;



    /**
     * Default constructor
     */
    public ActionsContext() {
        this(null);
    }



    /**
     * Constructor with default UIActionEvt factory specified
     * 
     * @param defaultUIActionEvtFactory
     *            Default UIActionEvt factory
     */
    public ActionsContext(UIActionEvtFactory<?> defaultUIActionEvtFactory) {
        this.defaultUIActionEvtFactory = defaultUIActionEvtFactory;
        this.actionsListeners = new HashMap<UIAction<UIActionEvt, ?>, ActionsContext.ListenersMetaData>();
        this.componentsListeners = new HashMap<JComponent, ActionsContext.ListenersMetaData>();
        this.componentsActionsListeners = new HashMap<JComponent, Map<UIAction<UIActionEvt, ?>, ListenersMetaData>>();
        this.actionsEnabled = true;
    }



    /**
     * Find next component that will gain focus from current component
     * 
     * @return Next component that will gain focus
     */
    private Component findNextFocus() {
        Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        Container root = c.getFocusCycleRootAncestor();
        FocusTraversalPolicy policy = root.getFocusTraversalPolicy();
        Component nextFocus = policy.getComponentAfter(root, c);
        if (nextFocus == null) {
            nextFocus = policy.getDefaultComponent(root);
        }
        return nextFocus;
    }



    /**
     * Execute action specified if possible. If action can't be executed, the
     * component specified transfer its focus if possible
     * 
     * @param action
     *            Action
     * @param component
     *            Component
     * 
     * @param <P>
     *            UIActionEvt type
     * 
     */
    private <P extends UIActionEvt> void executeAction(UIAction<P, ?> action, JComponent component) {
        UIActionEvtFactory<P> actionEvtFactory = getUIActionEvtFactory(action);
        // Execute action if possible (action enabled and visible)
        if (action != null && UIActionUtil.isActionEnabled(action, actionEvtFactory) && UIActionUtil.isActionVisible(action, actionEvtFactory)) {
            action.execute(createUIActionEvt(action));
        } else {
            // Transfer focus if possible (next focus component is not a button)
            if (!(findNextFocus() instanceof JButton)) {
                component.transferFocus();
            }
        }
    }



    /**
     * Set tooltip text in button specified for action specified
     * 
     * @param action
     *            Action
     * @param button
     *            Button
     * 
     * @param <P>
     *            UIActionEvt type
     * 
     */
    private <P extends UIActionEvt> void setToolTipText(UIAction<P, ?> action, Component button) {
        // No tooltip text if no description and no disabled causes to show
        if (StrUtil.isEmpty(action.getDescription()) && (button.isEnabled() || action.getDisabledCauses().isEmpty())) {
            setToolTipText(button, null);
            return;
        }

        // Construct tooltip text in html
        StringBuffer html = new StringBuffer();
        html.append("<" + HTML.Tag.HTML + "><" + HTML.Tag.BODY + ">"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

        // If description for the action
        if (!StrUtil.isEmpty(action.getDescription())) {
            html.append(action.getDescription());
        }

        // Show disabled causes in tooltip text only if button is disabled
        // and one or more disabled causes for the action
        if (!button.isEnabled() && !action.getDisabledCauses().isEmpty()) {
            if (!StrUtil.isEmpty(action.getDescription())) {
                html.append("<" + HTML.Tag.BR + ">" + "<" + HTML.Tag.BR + ">"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
            }
            // If several disabled causes
            for (Iterator<String> it = action.getDisabledCauses().iterator(); it.hasNext();) {
                html.append(" - ").append(it.next()); //$NON-NLS-1$
                if (it.hasNext()) {
                    html.append("<" + HTML.Tag.BR + ">"); //$NON-NLS-1$ //$NON-NLS-2$
                }
            }
        }

        html.append("</" + HTML.Tag.BODY + "></" + HTML.Tag.HTML + ">"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

        // Set tooltip text in button
        setToolTipText(button, html.toString());
    }



    private void setToolTipText(Component component, String text) {
        try {
            Method method = component.getClass().getMethod("setToolTipText", String.class);
            method.invoke(component, text);
        } catch (Exception exc) {
            LOG.error("Error when setting tooltip's text in class " + component.getClass(), exc);
        }
    }



    /**
     * Recursively method to determine if the action is contained in actions
     * specified
     * 
     * @param action
     *            Action
     * @param actions
     *            Actions
     * 
     * @return True if action is contained in actions specified
     */
    private boolean containsAction(UIAction<UIActionEvt, ?> action, List<UIAction<UIActionEvt, ?>> actions) {
        if (actions.contains(action)) {
            return true;
        }

        for (UIAction<UIActionEvt, ?> currentAction : actions) {
            if (currentAction instanceof UIActionGroup) {
                if (containsAction(action, ((UIActionGroup) currentAction).getActions())) {
                    return true;
                }
            }
        }

        return false;
    }



    /**
     * Get UIActionEvt factory for the action specified
     * 
     * @param Action
     * 
     * @return UIActionEvt factory
     * 
     * @param <P>
     *            UIActionEvt type
     * 
     */
    public <P extends UIActionEvt> UIActionEvtFactory<P> getUIActionEvtFactory(UIAction<P, ?> action) {
        // For each components
        for (JComponent component : new ArrayList<JComponent>(componentsActionsListeners.keySet())) {
            // If component is not an UIActionEvtFactory, continue
            if (!(component instanceof UIActionEvtFactory)) {
                continue;
            }
            // If action is binded with the current component
            if (containsAction((UIAction<UIActionEvt, ?>) action, new ArrayList<UIAction<UIActionEvt, ?>>(componentsActionsListeners.get(component).keySet()))) {
                try {
                    // Try cast
                    return (UIActionEvtFactory<P>) component;
                } catch (Exception exc) {
                }
            }
        }

        try {
            return (UIActionEvtFactory<P>) defaultUIActionEvtFactory;
        } catch (Exception exc) {
            // If default UIActionEvt factory is not a P factory, return null
            return null;
        }
    }



    /**
     * Create action state for action generated by action state factory (return
     * null if no
     * action state factory)
     * 
     * @param Action
     * 
     * @return Action state
     * 
     * @param <P>
     *            UIActionEvt type
     * 
     */
    public <P extends UIActionEvt> P createUIActionEvt(UIAction<P, ?> action) {
        UIActionEvtFactory<P> actionEvtFactory = getUIActionEvtFactory(action);
        return actionEvtFactory == null ? null : actionEvtFactory.createUIActionEvt();
    }



    /**
     * Update all actions states
     */
    public void updateActionsStates() {

        // postpone action state update by invokeLatering() them in order to let
        // components update their contents
        // WARNING : do not remove the SwingUtilities.invokeLater() as it is
        // required to postpone the update.
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                // Update states for UIActions
                for (UIAction<UIActionEvt, ?> action : new ArrayList<UIAction<UIActionEvt, ?>>(actionsListeners.keySet())) {
                    action.setEnabled(actionsEnabled);
                    if (actionsEnabled) {
                        UIActionUtil.updateActionState(action, getUIActionEvtFactory(action));
                    }
                }
            }
        });
    }



    /**
     * Specify if all actions are enabled or disabled
     * 
     * @param actionsEnabled
     */
    public void setActionsEnabled(boolean actionsEnabled) {
        this.actionsEnabled = actionsEnabled;

        // Update all actions states
        updateActionsStates();
    }



    /**
     * Bind component for action specified
     * 
     * @param action
     *            Action
     * @param component
     *            Component
     */
    public <P extends UIActionEvt> void bind(UIAction<P, ?> action, JComponent component) {
        if (component != null && action != null) {
            if (component instanceof JButton) {
                new JButtonBinder().bind((UIAction<UIActionEvt, ?>) action, (JButton) component);
            } else if (component instanceof ImageLabel) {
                new ImageButtonBinder().bind((UIAction<UIActionEvt, ?>) action, (ImageLabel) component);
            } else if (component instanceof JMenuItem) {
                new JMenuItemBinder().bind((UIAction<UIActionEvt, ?>) action, (JMenuItem) component);
            } else if (component instanceof JTextComponent) {
                new JTextComponentBinder().bind((UIAction<UIActionEvt, ?>) action, (JTextComponent) component);
            } else if (component instanceof JCheckBox) {
                new JCheckBoxBinder().bind((UIAction<UIActionEvt, ?>) action, (JCheckBox) component);
            } else if (component instanceof JRadioButton) {
                new JRadioButtonBinder().bind((UIAction<UIActionEvt, ?>) action, (JRadioButton) component);
            } else if (component instanceof JComboBox) {
                new JComboBoxBinder().bind((UIAction<UIActionEvt, ?>) action, (JComboBox) component);
            } else if (component instanceof JList) {
                new JListBinder().bind((UIAction<UIActionEvt, ?>) action, (JList) component);
            } else if (component instanceof DateChooser) {
                new ComponentBinder<DateChooser>().bind((UIAction<UIActionEvt, ?>) action, (DateChooser) component);
            } else if (component instanceof ProductTable) {
                new ProductTableBinder().bind((UIAction<UIActionEvt, ?>) action, (ProductTable) component);
            } else if (component instanceof AbstractChart) {
                new ChartBinder().bind((UIAction<UIActionEvt, ?>) action, (AbstractChart) component);
            } else {
                LOG.warn("Component type not implemented !"); //$NON-NLS-1$
            }

            // Update actions states
            updateActionsStates();
        }
    }



    /**
     * Unbind component for action specified
     * 
     * @param action
     *            Action
     * @param component
     *            Component
     */
    public <P extends UIActionEvt> void unbind(UIAction<P, ?> action, JComponent component) {
        if (component != null && action != null) {
            if (component instanceof JButton) {
                new JButtonBinder().unbind((UIAction<UIActionEvt, ?>) action, (JButton) component);
            } else if (component instanceof JMenuItem) {
                new JMenuItemBinder().unbind((UIAction<UIActionEvt, ?>) action, (JMenuItem) component);
            } else if (component instanceof JTextComponent) {
                new JTextComponentBinder().unbind((UIAction<UIActionEvt, ?>) action, (JTextComponent) component);
            } else if (component instanceof JCheckBox) {
                new JCheckBoxBinder().unbind((UIAction<UIActionEvt, ?>) action, (JCheckBox) component);
            } else if (component instanceof JRadioButton) {
                new JRadioButtonBinder().unbind((UIAction<UIActionEvt, ?>) action, (JRadioButton) component);
            } else if (component instanceof JComboBox) {
                new JComboBoxBinder().unbind((UIAction<UIActionEvt, ?>) action, (JComboBox) component);
            } else if (component instanceof JList) {
                new JListBinder().unbind((UIAction<UIActionEvt, ?>) action, (JList) component);
            } else if (component instanceof DateChooser) {
                new ComponentBinder<DateChooser>().unbind((UIAction<UIActionEvt, ?>) action, (DateChooser) component);
            } else if (component instanceof ProductTable) {
                new ProductTableBinder().unbind((UIAction<UIActionEvt, ?>) action, (ProductTable) component);
            } else if (component instanceof AbstractChart) {
                new ChartBinder().unbind((UIAction<UIActionEvt, ?>) action, (AbstractChart) component);
            } else {
                LOG.warn("Component type not implemented !"); //$NON-NLS-1$
            }

            // Update actions states
            updateActionsStates();
        }
    }



    /**
     * Unbind all components for action specified
     * 
     * @param actionParam
     *            Action
     */
    public void unbind(UIAction<UIActionEvt, ?> actionParam) {
        if (actionParam != null) {
            for (JComponent component : new ArrayList<JComponent>(componentsActionsListeners.keySet())) {
                for (UIAction<UIActionEvt, ?> action : new ArrayList<UIAction<UIActionEvt, ?>>(componentsActionsListeners.get(component).keySet())) {
                    if (action.equals(actionParam)) {
                        unbind(action, component);
                    }
                }
            }

            // Update actions states
            updateActionsStates();
        }
    }



    /**
     * Unbind component specified for all its actions binded
     * 
     * @param componentParam
     *            Component
     */
    public void unbind(JComponent componentParam) {
        if (componentParam != null) {

            for (UIAction<UIActionEvt, ?> action : new ArrayList<UIAction<UIActionEvt, ?>>(componentsActionsListeners.get(componentParam).keySet())) {
                unbind(action, componentParam);
            }

            // Update actions states
            updateActionsStates();
        }
    }

    /**
     * Contains all possible listeners used by action and components binded
     * 
     * @author JayJay
     * 
     */
    private static class ListenersMetaData {

        private List<MVCTxListener> mvctxListeners;
        private List<UIActionListener> uiActionListeners;
        private List<KeyListener> keyListeners;
        private List<ActionListener> actionListeners;
        private List<DocumentListener> documentListeners;
        private List<ItemListener> itemListeners;
        private List<MouseListener> mouseListeners;
        private List<ListSelectionListener> listSelectionListeners;



        /**
         * Default constructor that initialize all listeners list
         */
        public ListenersMetaData() {
            setMvcTxListeners(new ArrayList<MVCTxListener>());
            setUiActionListeners(new ArrayList<UIActionListener>());
            setKeyListeners(new ArrayList<KeyListener>());
            setActionListeners(new ArrayList<ActionListener>());
            setDocumentListeners(new ArrayList<DocumentListener>());
            setItemListeners(new ArrayList<ItemListener>());
            setMouseListeners(new ArrayList<MouseListener>());
            setListSelectionListeners(new ArrayList<ListSelectionListener>());
        }



        /**
         * @return the transactionListeners
         */
        public List<MVCTxListener> getMvcTxListeners() {
            return mvctxListeners;
        }



        /**
         * @param transactionListeners
         *            the transactionListeners to set
         */
        public void setMvcTxListeners(List<MVCTxListener> transactionListeners) {
            this.mvctxListeners = transactionListeners;
        }



        /**
         * @return the uiActionListeners
         */
        public List<UIActionListener> getUiActionListeners() {
            return uiActionListeners;
        }



        /**
         * @param uiActionListeners
         *            the uiActionListeners to set
         */
        public void setUiActionListeners(List<UIActionListener> uiActionListeners) {
            this.uiActionListeners = uiActionListeners;
        }



        /**
         * @return the keyListeners
         */
        public List<KeyListener> getKeyListeners() {
            return keyListeners;
        }



        /**
         * @param keyListeners
         *            the keyListeners to set
         */
        public void setKeyListeners(List<KeyListener> keyListeners) {
            this.keyListeners = keyListeners;
        }



        /**
         * @return the actionListeners
         */
        public List<ActionListener> getActionListeners() {
            return actionListeners;
        }



        /**
         * @param actionListeners
         *            the actionListeners to set
         */
        public void setActionListeners(List<ActionListener> actionListeners) {
            this.actionListeners = actionListeners;
        }



        /**
         * @return the documentListeners
         */
        public List<DocumentListener> getDocumentListeners() {
            return documentListeners;
        }



        /**
         * @param documentListeners
         *            the documentListeners to set
         */
        public void setDocumentListeners(List<DocumentListener> documentListeners) {
            this.documentListeners = documentListeners;
        }



        /**
         * @return the itemListeners
         */
        public List<ItemListener> getItemListeners() {
            return itemListeners;
        }



        /**
         * @param itemListeners
         *            the itemListeners to set
         */
        public void setItemListeners(List<ItemListener> itemListeners) {
            this.itemListeners = itemListeners;
        }



        /**
         * @return the mouseListeners
         */
        public List<MouseListener> getMouseListeners() {
            return mouseListeners;
        }



        /**
         * @param mouseListeners
         *            the mouseListeners to set
         */
        public void setMouseListeners(List<MouseListener> mouseListeners) {
            this.mouseListeners = mouseListeners;
        }



        /**
         * @return the listSelectionListeners
         */
        public List<ListSelectionListener> getListSelectionListeners() {
            return listSelectionListeners;
        }



        /**
         * @param listSelectionListeners
         *            the listSelectionListeners to set
         */
        public void setListSelectionListeners(List<ListSelectionListener> listSelectionListeners) {
            this.listSelectionListeners = listSelectionListeners;
        }
    }

    /**
     * Component binder
     * 
     * @author JayJay
     * 
     * @param <C>
     *            Component type
     */
    private class ComponentBinder<C extends JComponent> {

        /**
         * Add listeners for an action independently a component specified
         * 
         * @param action
         *            Action
         * @param actionListeners
         *            Listeners
         */
        public void addListeners(final UIAction<UIActionEvt, ?> action, ListenersMetaData actionListeners) {
            MVCTxListener transactionListener = new MVCTxListener() {

                public void beforeExecution() {
                    setActionsEnabled(false);
                }



                public void afterExecution() {
                    setActionsEnabled(true);
                }
            };
            actionListeners.getMvcTxListeners().add(transactionListener);
            action.addListener(transactionListener);
        }



        /**
         * Add listeners for a component independently an action specified
         * 
         * @param component
         *            Component
         * @param componentListeners
         *            Listeners
         */
        public void addListeners(C component, ListenersMetaData componentListeners) {

        }



        /**
         * Add listeners for a component and an action specified
         * 
         * @param action
         *            Action
         * @param component
         *            Component
         * @param componentActionListeners
         */
        public void addListeners(final UIAction<UIActionEvt, ?> action, final C component, ListenersMetaData componentActionListeners) {
            KeyListener keyListener = new KeyAdapter() {

                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        executeAction(action, component);
                    }
                }
            };
            componentActionListeners.getKeyListeners().add(keyListener);
            component.addKeyListener(keyListener);
        }



        /**
         * Remove listeners for an action independently a component specified
         * 
         * @param action
         *            Action
         * @param actionListeners
         *            Listeners
         */
        public void removeListeners(UIAction<UIActionEvt, ?> action, ListenersMetaData actionListeners) {
            for (UIActionListener listener : actionListeners.getUiActionListeners()) {
                action.removeUIActionListener(listener);
            }
            for (MVCTxListener listener : actionListeners.getMvcTxListeners()) {
                action.removeListener(listener);
            }
        }



        /**
         * Remove listeners for a component independently an action specified
         * 
         * @param component
         *            Component
         * @param componentListeners
         *            Listeners
         */
        public void removeListeners(C component, ListenersMetaData componentListeners) {
            for (KeyListener listener : componentListeners.getKeyListeners()) {
                component.removeKeyListener(listener);
            }
        }



        /**
         * Remove listeners for a component and an action specified
         * 
         * @param action
         *            Action
         * @param component
         *            Component
         * @param componentActionListeners
         */
        public void removeListeners(UIAction<UIActionEvt, ?> action, C component, ListenersMetaData componentActionListeners) {
            for (UIActionListener listener : componentActionListeners.getUiActionListeners()) {
                action.removeUIActionListener(listener);
            }
            for (KeyListener listener : componentActionListeners.getKeyListeners()) {
                component.removeKeyListener(listener);
            }
        }



        /**
         * Get how many components are binded with action specified
         * 
         * @param paramAction
         *            Action
         * 
         * @return Components count
         */
        protected int getComponentCount(UIAction<UIActionEvt, ?> paramAction) {
            int componentsCount = 0;
            for (JComponent component : new ArrayList<JComponent>(componentsActionsListeners.keySet())) {
                for (UIAction<UIActionEvt, ?> action : new ArrayList<UIAction<UIActionEvt, ?>>(componentsActionsListeners.get(component).keySet())) {
                    if (action.equals(paramAction)) {
                        componentsCount++;
                    }
                }
            }
            return componentsCount;
        }



        /**
         * Get how many actions are binded with component specified
         * 
         * @param paramComponent
         *            Component
         * 
         * @return Actions count
         */
        protected int getActionCount(C paramComponent) {
            Map<UIAction<UIActionEvt, ?>, ListenersMetaData> actionsComponent = componentsActionsListeners.get(paramComponent);
            return actionsComponent == null ? 0 : actionsComponent.size();
        }



        /**
         * Bind component for action specified
         * 
         * @param action
         *            Action
         * @param component
         *            Component
         */
        public void bind(UIAction<UIActionEvt, ?> action, C component) {

            if (actionsListeners.containsKey(action) && componentsListeners.containsKey(component)) {
                return;
            }

            ListenersMetaData actionListeners = actionsListeners.get(action);
            if (actionListeners == null) {
                actionListeners = new ListenersMetaData();
                addListeners(action, actionListeners);
                actionsListeners.put(action, actionListeners);
            }

            ListenersMetaData componentListeners = componentsListeners.get(component);
            if (componentListeners == null) {
                componentListeners = new ListenersMetaData();
                addListeners(component, componentListeners);
                componentsListeners.put(component, componentListeners);
            }

            Map<UIAction<UIActionEvt, ?>, ListenersMetaData> componentActionsListeners = componentsActionsListeners.get(component);
            if (componentActionsListeners == null) {
                componentActionsListeners = new HashMap<UIAction<UIActionEvt, ?>, ActionsContext.ListenersMetaData>();
                componentsActionsListeners.put(component, componentActionsListeners);
            }

            ListenersMetaData componentActionListeners = new ListenersMetaData();
            addListeners(action, component, componentActionListeners);
            componentActionsListeners.put(action, componentActionListeners);
        }



        /**
         * Unbind component for action specified
         * 
         * @param action
         *            Action
         * @param component
         *            Component
         */
        public void unbind(UIAction<UIActionEvt, ?> action, C component) {

            if (!actionsListeners.containsKey(action) || !componentsListeners.containsKey(component)) {
                return;
            }

            if (getComponentCount(action) == 1) {
                ListenersMetaData actionListeners = actionsListeners.get(action);
                removeListeners(action, actionListeners);
                actionsListeners.remove(action);
            }

            if (getActionCount(component) == 1) {
                ListenersMetaData componentListeners = componentsListeners.get(component);
                removeListeners(component, componentListeners);
                componentsListeners.remove(component);
            }

            Map<UIAction<UIActionEvt, ?>, ListenersMetaData> componentActionsListeners = componentsActionsListeners.get(component);
            ListenersMetaData componentActionListeners = componentActionsListeners.get(action);
            removeListeners(action, component, componentActionListeners);
            componentActionsListeners.remove(action);
            if (componentActionsListeners.isEmpty()) {
                componentsActionsListeners.remove(component);
            }
        }
    }

    /**
     * JButton binder
     * 
     * @author JayJay
     * 
     */
    private class JButtonBinder extends ComponentBinder<JButton> {

        @Override
        public void addListeners(JButton component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);
        }



        @Override
        public void addListeners(final UIAction<UIActionEvt, ?> action, final JButton component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);

            ActionListener actionListener = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    executeAction(action, component);
                }
            };
            componentActionListeners.getActionListeners().add(actionListener);
            component.addActionListener(actionListener);

            UIActionListener uiActionListener = new UIActionAdapter() {

                @Override
                public void enabledChanged(boolean enabled) {
                    component.setEnabled(enabled);
                    setToolTipText(action, component);
                }



                @Override
                public void visibleChanged(boolean visible) {
                    component.setVisible(visible);
                }



                @Override
                public void nameChanged(String name) {
                    component.setText(name);
                }



                @Override
                public void iconChanged(Icon icon) {
                    component.setIcon(icon);
                }



                @Override
                public void descriptionChanged(String description) {
                    setToolTipText(action, component);
                }



                @Override
                public void disabledCausesChanged(List<String> disabledCauses) {
                    setToolTipText(action, component);
                }
            };
            componentActionListeners.getUiActionListeners().add(uiActionListener);
            action.addUIActionListener(uiActionListener);
        }



        @Override
        public void removeListeners(JButton component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (ActionListener listener : componentListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, JButton component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (ActionListener listener : componentActionListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentActionListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }



        @Override
        public void bind(UIAction<UIActionEvt, ?> action, JButton component) {
            super.bind(action, component);

            component.setEnabled(action.isEnabled());
            component.setVisible(action.isVisible());
            component.setText(action.getName());
            component.setIcon(action.getIcon());
            component.setToolTipText(action.getDescription());
        }
    }

    /**
     * ImageButton binder
     * 
     * @author JayJay
     * 
     */
    private class ImageButtonBinder extends ComponentBinder<ImageLabel> {

        @Override
        public void addListeners(ImageLabel component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);
        }



        @Override
        public void addListeners(final UIAction<UIActionEvt, ?> action, final ImageLabel component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);

            MouseAdapter mouseListener = new MouseAdapter() {

                public void mouseClicked(MouseEvent e) {
                    executeAction(action, component);
                }
            };
            componentActionListeners.getMouseListeners().add(mouseListener);
            component.addMouseListener(mouseListener);

            UIActionListener uiActionListener = new UIActionAdapter() {

                @Override
                public void enabledChanged(boolean enabled) {
                    component.setEnabled(enabled);
                    setToolTipText(action, component);
                }



                @Override
                public void visibleChanged(boolean visible) {
                    component.setVisible(visible);
                }



                @Override
                public void nameChanged(String name) {
                    component.setText(name);
                }



                @Override
                public void iconChanged(Icon icon) {
                }



                @Override
                public void descriptionChanged(String description) {
                    setToolTipText(action, component);
                }



                @Override
                public void disabledCausesChanged(List<String> disabledCauses) {
                    setToolTipText(action, component);
                }
            };
            componentActionListeners.getUiActionListeners().add(uiActionListener);
            action.addUIActionListener(uiActionListener);
        }



        @Override
        public void removeListeners(ImageLabel component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (MouseListener listener : componentListeners.getMouseListeners()) {
                component.removeMouseListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, ImageLabel component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (MouseListener listener : componentActionListeners.getMouseListeners()) {
                component.removeMouseListener(listener);
            }
        }



        @Override
        public void bind(UIAction<UIActionEvt, ?> action, ImageLabel component) {
            super.bind(action, component);

            component.setEnabled(action.isEnabled());
            component.setVisible(action.isVisible());
            component.setText(action.getName());
            component.setToolTipText(action.getDescription());
        }
    }

    /**
     * JMenuItem binder
     * 
     * @author JayJay
     * 
     */
    private class JMenuItemBinder extends ComponentBinder<JMenuItem> {

        @Override
        public void addListeners(JMenuItem component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);
        }



        @Override
        public void addListeners(final UIAction<UIActionEvt, ?> action, final JMenuItem component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);

            ActionListener actionListener = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    executeAction(action, component);
                }
            };
            componentActionListeners.getActionListeners().add(actionListener);
            component.addActionListener(actionListener);

            UIActionListener uiActionListener = new UIActionAdapter() {

                @Override
                public void enabledChanged(boolean enabled) {
                    component.setEnabled(enabled);
                    setToolTipText(action, component);
                }



                @Override
                public void visibleChanged(boolean visible) {
                    component.setVisible(visible);
                }



                @Override
                public void nameChanged(String name) {
                    component.setText(name);
                }



                @Override
                public void iconChanged(Icon icon) {
                    component.setIcon(icon);
                }



                @Override
                public void descriptionChanged(String description) {
                    setToolTipText(action, component);
                }



                @Override
                public void disabledCausesChanged(List<String> disabledCauses) {
                    setToolTipText(action, component);
                }
            };
            componentActionListeners.getUiActionListeners().add(uiActionListener);
            action.addUIActionListener(uiActionListener);
        }



        @Override
        public void removeListeners(JMenuItem component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (ActionListener listener : componentListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, JMenuItem component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (ActionListener listener : componentActionListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentActionListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }



        @Override
        public void bind(UIAction<UIActionEvt, ?> action, JMenuItem component) {
            super.bind(action, component);

            component.setEnabled(action.isEnabled());
            component.setVisible(action.isVisible());
            component.setText(action.getName());
            component.setIcon(action.getIcon());
            component.setToolTipText(action.getDescription());
        }
    }

    /**
     * JTextComponent binder
     * 
     * @author JayJay
     * 
     */
    private class JTextComponentBinder extends ComponentBinder<JTextComponent> {

        @Override
        public void addListeners(JTextComponent component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);

            DocumentListener documentListener = new DocumentListener() {

                public void changedUpdate(DocumentEvent e) {
                    updateActionsStates();
                }



                public void removeUpdate(DocumentEvent e) {
                    updateActionsStates();
                }



                public void insertUpdate(DocumentEvent e) {
                    updateActionsStates();
                }
            };
            componentListeners.getDocumentListeners().add(documentListener);
            component.getDocument().addDocumentListener(documentListener);
        }



        @Override
        public void addListeners(UIAction<UIActionEvt, ?> action, JTextComponent component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);
        }



        @Override
        public void removeListeners(JTextComponent component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (DocumentListener listener : componentListeners.getDocumentListeners()) {
                component.getDocument().removeDocumentListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, JTextComponent component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (DocumentListener listener : componentActionListeners.getDocumentListeners()) {
                component.getDocument().removeDocumentListener(listener);
            }
        }
    }

    /**
     * JCheckBox binder
     * 
     * @author JayJay
     * 
     */
    private class JCheckBoxBinder extends ComponentBinder<JCheckBox> {

        @Override
        public void addListeners(JCheckBox component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);

            ActionListener actionListener = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    updateActionsStates();
                }
            };
            componentListeners.getActionListeners().add(actionListener);
            component.addActionListener(actionListener);
        }



        @Override
        public void addListeners(UIAction<UIActionEvt, ?> action, JCheckBox component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);
        }



        @Override
        public void removeListeners(JCheckBox component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (ActionListener listener : componentListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, JCheckBox component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (ActionListener listener : componentActionListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentActionListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }
    }

    /**
     * JRadioButton binder
     * 
     * @author JayJay
     * 
     */
    private class JRadioButtonBinder extends ComponentBinder<JRadioButton> {

        @Override
        public void addListeners(JRadioButton component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);

            ActionListener actionListener = new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    updateActionsStates();
                }
            };
            componentListeners.getActionListeners().add(actionListener);
            component.addActionListener(actionListener);
        }



        @Override
        public void addListeners(UIAction<UIActionEvt, ?> action, JRadioButton component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);
        }



        @Override
        public void removeListeners(JRadioButton component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (ActionListener listener : componentListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, JRadioButton component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (ActionListener listener : componentActionListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (ItemListener listener : componentActionListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }
        }
    }

    /**
     * JComboBox binder
     * 
     * @author JayJay
     * 
     */
    private class JComboBoxBinder extends ComponentBinder<JComboBox> {

        @Override
        public void addListeners(JComboBox component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);

            ItemListener itemListener = new ItemListener() {

                public void itemStateChanged(ItemEvent e) {
                    updateActionsStates();
                }
            };
            componentListeners.getItemListeners().add(itemListener);
            component.addItemListener(itemListener);

            DocumentListener documentListener = new DocumentListener() {

                public void changedUpdate(DocumentEvent e) {
                    updateActionsStates();
                }



                public void removeUpdate(DocumentEvent e) {
                    updateActionsStates();
                }



                public void insertUpdate(DocumentEvent e) {
                    updateActionsStates();
                }
            };
            componentListeners.getDocumentListeners().add(documentListener);
            ((JTextComponent) component.getEditor().getEditorComponent()).getDocument().addDocumentListener(documentListener);
        }



        @Override
        public void addListeners(UIAction<UIActionEvt, ?> action, JComboBox component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);

            for (KeyListener listener : componentActionListeners.getKeyListeners()) {
                component.getEditor().getEditorComponent().addKeyListener(listener);
            }
        }



        @Override
        public void removeListeners(JComboBox component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (ActionListener listener : componentListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (DocumentListener listener : componentListeners.getDocumentListeners()) {
                ((JTextComponent) component.getEditor().getEditorComponent()).getDocument().removeDocumentListener(listener);
            }

            for (ItemListener listener : componentListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }

            for (KeyListener listener : componentListeners.getKeyListeners()) {
                component.getEditor().getEditorComponent().removeKeyListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, JComboBox component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (ActionListener listener : componentActionListeners.getActionListeners()) {
                component.removeActionListener(listener);
            }

            for (DocumentListener listener : componentActionListeners.getDocumentListeners()) {
                ((JTextComponent) component.getEditor().getEditorComponent()).getDocument().removeDocumentListener(listener);
            }

            for (ItemListener listener : componentActionListeners.getItemListeners()) {
                component.removeItemListener(listener);
            }

            for (KeyListener listener : componentActionListeners.getKeyListeners()) {
                component.getEditor().getEditorComponent().removeKeyListener(listener);
            }
        }
    }

    /**
     * JList binder
     * 
     * @author JayJay
     * 
     */
    private class JListBinder extends ComponentBinder<JList> {

        @Override
        public void addListeners(JList component, ListenersMetaData componentListeners) {
            super.addListeners(component, componentListeners);

            ListSelectionListener listSelectionListener = new ListSelectionListener() {

                public void valueChanged(ListSelectionEvent e) {
                    updateActionsStates();
                }
            };
            componentListeners.getListSelectionListeners().add(listSelectionListener);
            component.addListSelectionListener(listSelectionListener);
        }



        @Override
        public void addListeners(UIAction<UIActionEvt, ?> action, JList component, ListenersMetaData componentActionListeners) {
            super.addListeners(action, component, componentActionListeners);
        }



        @Override
        public void removeListeners(JList component, ListenersMetaData componentListeners) {
            super.removeListeners(component, componentListeners);

            for (ListSelectionListener listener : componentListeners.getListSelectionListeners()) {
                component.removeListSelectionListener(listener);
            }
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> action, JList component, ListenersMetaData componentActionListeners) {
            super.removeListeners(action, component, componentActionListeners);

            for (ListSelectionListener listener : componentActionListeners.getListSelectionListeners()) {
                component.removeListSelectionListener(listener);
            }
        }
    }

    /**
     * ProductTable binder
     * 
     * @author JayJay
     */
    private class ProductTableBinder extends ComponentBinder<ProductTable> {

        @Override
        public void addListeners(final UIAction<UIActionEvt, ?> productAction, ListenersMetaData productActionListeners) {
            super.addListeners(productAction, productActionListeners);
        }



        @Override
        public void addListeners(ProductTable component, ListenersMetaData componentListeners) {
            // Do nothing
        }



        @Override
        public void addListeners(UIAction<UIActionEvt, ?> action, ProductTable component, ListenersMetaData componentActionListeners) {
            // Do nothing
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> productAction, ListenersMetaData productActionListeners) {
            super.removeListeners(productAction, productActionListeners);
        }



        @Override
        public void removeListeners(ProductTable component, ListenersMetaData componentListeners) {
            // Do nothing
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> productAction, ProductTable productTable, ListenersMetaData productTableProductActionListeners) {
            // Do nothing
        }



        @Override
        public void bind(UIAction<UIActionEvt, ?> action, ProductTable component) {
            super.bind(action, component);

            // If first action in table, set actions context
            if (getActionCount(component) == 1) {
                component.setActionsContext(ActionsContext.this);
            }

            // Bind action in table
            component.bind(action);
        }



        @Override
        public void unbind(UIAction<UIActionEvt, ?> action, ProductTable component) {
            super.unbind(action, component);

            // Unbind action in table
            component.unbind(action);

            // If no more action in table, unset actions context
            if (getActionCount(component) == 0) {
                component.setActionsContext(null);
            }
        }
    }

    /**
     * Chart binder
     * 
     * @author JayJay
     */
    private class ChartBinder extends ComponentBinder<AbstractChart> {

        @Override
        public void addListeners(final UIAction<UIActionEvt, ?> chartAction, ListenersMetaData chartActionListeners) {
            super.addListeners(chartAction, chartActionListeners);
        }



        @Override
        public void addListeners(AbstractChart component, ListenersMetaData componentListeners) {
            // Do nothing
        }



        @Override
        public void addListeners(UIAction<UIActionEvt, ?> action, AbstractChart component, ListenersMetaData componentActionListeners) {
            // Do nothing
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> chartAction, ListenersMetaData chartActionListeners) {
            super.removeListeners(chartAction, chartActionListeners);
        }



        @Override
        public void removeListeners(AbstractChart component, ListenersMetaData componentListeners) {
            // Do nothing
        }



        @Override
        public void removeListeners(UIAction<UIActionEvt, ?> chartAction, AbstractChart chartTable, ListenersMetaData chartTableProductActionListeners) {
            // Do nothing
        }



        @Override
        public void bind(UIAction<UIActionEvt, ?> action, AbstractChart component) {
            super.bind(action, component);

            // If first action in table, set actions context
            if (getActionCount(component) == 1) {
                component.setActionsContext(ActionsContext.this);
            }

            // Bind action in table
            component.bind(action);
        }



        @Override
        public void unbind(UIAction<UIActionEvt, ?> action, AbstractChart component) {
            super.unbind(action, component);

            // Unbind action in table
            component.unbind(action);

            // If no more action in table, unset actions context
            if (getActionCount(component) == 0) {
                component.setActionsContext(null);
            }
        }
    }
}
