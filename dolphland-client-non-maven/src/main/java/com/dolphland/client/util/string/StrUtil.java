package com.dolphland.client.util.string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * String util
 * 
 * @author JayJay
 * 
 */
public class StrUtil {

    // Defines an empty string: a zero length string. */
    public static final String EMPTY_STRING = new String();

    // Defines the new line string, i.e. "\n" */
    public static final String NEW_LINE = "\n";

    // Defines the default list separator
    public static final String DEFAULT_LIST_SEPARATOR = ", ";



    /**
     * Tell if string is empty
     * 
     * @param str
     *            String
     * 
     * @return True if string is empty
     */
    public static final boolean isEmpty(String str) {
        if (str == null) {
            return true;
        } else {
            return str.trim().length() == 0;
        }
    }



    /**
     * Format a list
     * 
     * @param list
     *            List
     * 
     * @return List formatted
     */
    public static final String formatList(Object[] list) {
        return formatList(list, DEFAULT_LIST_SEPARATOR);
    }



    /**
     * Format a list
     * 
     * @param list
     *            List
     * @param listSeparator
     *            List separator
     * 
     * @return List formatted
     */
    public static final String formatList(Object[] list, String listSeparator) {
        return formatList(Arrays.asList(list), listSeparator);
    }



    /**
     * Format a list
     * 
     * @param list
     *            List
     * 
     * @return List formatted
     */
    public static final String formatList(List<Object> list) {
        return formatList(list, DEFAULT_LIST_SEPARATOR);
    }



    /**
     * Format a list
     * 
     * @param list
     *            List
     * @param listSeparator
     *            List separator
     * 
     * @return List formatted
     */
    public static final String formatList(List<Object> list, String listSeparator) {
        String result = "";
        Iterator<Object> itItems = list.iterator();
        while (itItems.hasNext()) {
            Object item = itItems.next();
            result += item == null ? EMPTY_STRING : item.toString();
            if (itItems.hasNext()) {
                result += listSeparator;
            }
        }
        return result;
    }



    /**
     * Split a string with a delimiter
     * 
     * @param str
     *            String
     * @param delim
     *            Delimiter
     * 
     * @return String splitted
     */
    public static final String[] split(String str, char delim) {
        if (str == null) {
            return new String[] {};
        }
        List<String> out = new ArrayList<String>();
        String token = null;
        int lastIdx = 0;
        do {
            lastIdx = str.indexOf(delim);
            if (lastIdx >= 0) {
                token = str.substring(0, lastIdx);
                out.add(token);
                str = str.substring(lastIdx + 1);
            } else {
                out.add(str);
                break;
            }
        } while (str.length() > 0);
        return (String[]) out.toArray(new String[out.size()]);
    }
}
