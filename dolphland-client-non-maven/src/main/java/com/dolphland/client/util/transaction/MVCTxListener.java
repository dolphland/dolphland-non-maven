package com.dolphland.client.util.transaction;

/**
 * <p>
 * Listener used by {@link MVCTx} to notify implementors of {@link MVCTx}
 * lifecycle.
 * </p>
 * 
 * @author JayJay
 * @author JayJay
 * 
 */
public interface MVCTxListener {

    /**
     * <p>
     * Called before first {@link MVCTx} method execution, i.e. before the
     * {@link MVCTx#shouldExecute(Object)} is called.
     * </p>
     * <p>
     * This method is always called wihin the AWT event dispatch thread
     * </p>
     */
    public void beforeExecution();



    /**
     * <p>
     * Called after the last {@link MVCTx} method execution, i.e. after the
     * {@link MVCTx#postExecute(Object)} method has been called
     * </p>
     * <p>
     * This method is always called wihin the AWT event dispatch thread
     * </p>
     */
    public void afterExecution();
}
