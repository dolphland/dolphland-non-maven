package com.dolphland.client.util.transaction;

import java.util.LinkedList;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

/**
 * Impl�mente un {@link SwingActionExecutor} constitu� d'une file d'attente des
 * actions � ex�cuter (de FIFO) et d'un thread de traitement des actions.<br>
 * <br>
 * Les actions sont ex�cut�es successivements et dans l'ordre par le thread
 * d'ex�cution.<br>
 * <br>
 * <u><b>Modalit�s de la file d'attente:</b></u> <br>
 * <bR>
 * La file d'attente peut �tre g�r�e de 3 mani�res diff�rentes :
 * <ul>
 * <li><b>Mode file non limit�e</b></li><br>
 * Dans ce mode il est possible de mettre un nombre infini d'actions en attente
 * d'ex�cution.<br>
 * Ce mode est activ� en transmettant une taille de file � z�ro au constructeur
 * (maxQueueSize=0)
 * <li><b>Mode file limit� (mais sup�rieur � 1)</b></li><br>
 * Dans ce mode il n'est pas possible de mettre en attente d'ex�cution plus de
 * maxQueueSize actions. Si on tente d'ajouter une action au del� des
 * maxQueueSize maxi, une exception est lev�e.<br>
 * Ce mode en d�termin� par transmission d'une valeur >1 � maxQueueSize au
 * constructeur.
 * <li><b>Mode file mono-action avec remplacement</b></li>
 * Dans ce mode, � un instant don�e, une seule action peut �tre en attente. Si
 * on ajoute une nouvelle action en attente, l'action en attente est supprim�e
 * (elle ne sera pas ex�cut�e) et remplacement par la nouvelle action.<br>
 * Ce mode est activ� par la transmission de la valeur 1 � maxQueueSize au
 * constructeur.
 * </ul>
 * 
 * @author JayJay
 */
public class SingleThreadedSwingActionExecutor implements SwingActionExecutor {

    private static final Logger log = Logger.getLogger(SingleThreadedSwingActionExecutor.class);

    private static int nextId = 0;

    private volatile Thread thread;

    private volatile LinkedList<SwingActionInvoker<?, ?>> swingActions;

    private volatile boolean shouldRun;

    private int maxQueueSize;



    /**
     * Construit un ex�cutor mono-thread�.<br>
     * La taille de file d'attente est transmise en param�tre
     * 
     * @param maxQueueSize
     *            Taille de la file d'attente de l'ex�cutor.<br>
     *            <li>taille 0 (z�ro) : La file d'attente n'est pas limit�e
     *            (infinie)</li><br>
     *            <li>Taille 1 : La file d'attente est limit� � une seule action
     *            et cette action sera �cras�e (donc non ex�cut�e) par tout
     *            ajout successif d'action. <br> <li>Taille 2 � l'infini : La
     *            file d'attente est limit�e � la atille sp�cifi�e. Toute
     *            tentative d'ajout d'une action alors que la file d�passe le
     *            maxQueueSize sp�cifi� l�vera une exception</li
     */
    public SingleThreadedSwingActionExecutor(int maxQueueSize) {
        thread = new Thread(new Runner(), "SwingExecutor-" + nextId()); //$NON-NLS-1$
        swingActions = new LinkedList<SwingActionInvoker<?, ?>>();
        this.maxQueueSize = maxQueueSize;
    }



    /**
     * Construit un ex�cutor avec un file d'attente infinie
     */
    public SingleThreadedSwingActionExecutor() {
        this(0);
    }



    public SingleThreadedSwingActionExecutor(String name) {
        thread = new Thread(new Runner(), name);
    }



    private static final int nextId() {
        synchronized (SingleThreadedSwingActionExecutor.class) {
            return nextId++;
        }
    }



    /*
     * (non-Javadoc)
     * @see com.vallourec.ctiv.fwk.ui.transaction.ISwingActionExecutor#start()
     */
    public synchronized void start() {
        if (shouldRun)
            return;
        if (thread == null) {
            thread = new Thread(new Runner(), "SwingExecutor-" + nextId()); //$NON-NLS-1$
        }
        shouldRun = true;
        thread.start();
    }



    /*
     * (non-Javadoc)
     * @see com.vallourec.ctiv.fwk.ui.transaction.ISwingActionExecutor#stop()
     */
    public synchronized void stop() {
        if (!shouldRun)
            return;
        shouldRun = false;
        thread.interrupt();
        thread = null;
    }



    /*
     * (non-Javadoc)
     * @see
     * com.vallourec.ctiv.fwk.ui.transaction.ISwingActionExecutor#add(com.vallourec
     * .ctiv.fwk.ui.transaction.SwingAction, P)
     */
    public <P, T> void add(SwingAction<P, T> sa, P param) {
        synchronized (swingActions) {
            if (maxQueueSize > 0) {
                if (maxQueueSize == 1) {
                    if (swingActions.size() == 1) {
                        swingActions.set(0, new SwingActionInvoker<P, T>(sa, param));
                    } else {
                        swingActions.add(new SwingActionInvoker<P, T>(sa, param));
                    }
                } else {
                    if (swingActions.size() >= maxQueueSize) {
                        throw new ArrayIndexOutOfBoundsException("maxQueueSize has been reached :" + swingActions.size() + "(queueSize))/" + maxQueueSize + "(maxQueueSize)"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    } else {
                        swingActions.add(new SwingActionInvoker<P, T>(sa, param));
                    }
                }
            } else {
                swingActions.add(new SwingActionInvoker<P, T>(sa, param));
            }
            swingActions.notify();
        }
    }

    private class Runner implements Runnable {

        public void run() {
            while (shouldRun) {
                SwingActionInvoker<?, ?> action = null;
                synchronized (swingActions) {
                    while (swingActions.size() == 0) {
                        try {
                            swingActions.wait();
                        } catch (InterruptedException ie) {
                            if (shouldRun) {
                                log.error("run(): Unexpected interruption !", ie); //$NON-NLS-1$
                            }
                            shouldRun = false;
                            break;
                        }
                    }
                    if (!shouldRun) {
                        break;
                    }
                    action = swingActions.removeFirst();
                }
                action.invoke();
            }
            thread = null;
        }
    }

    private static class SwingActionInvoker<P, T> {

        public static final int SHOULD_EXECUTE = 0;

        public static final int PRE_EXECUTE = 1;

        public static final int UPDATE_VIEW_SUCCESS = 2;

        public static final int UPDATE_VIEW_ERROR = 3;

        public static final int POST_EXECUTE = 4;

        private SwingAction<P, T> action;

        private P param;



        private SwingActionInvoker(SwingAction<P, T> action, P param) {
            this.action = action;
            this.param = param;
        }



        public final void invoke() {
            if (SwingUtilities.isEventDispatchThread()) {
                throw new IllegalThreadStateException("Cannot invoke SwingAction within AWTEventQueue thread !"); //$NON-NLS-1$
            }
            if (!(Boolean) invokeInEDT(SHOULD_EXECUTE, param, null, null)) {
                // si shouldExecute() a mis son v�to, annuler la transaction
                return;
            }
            invokeInEDT(PRE_EXECUTE, param, null, null);
            try {
                T val = action.doExecute(param);
                invokeInEDT(UPDATE_VIEW_SUCCESS, null, val, null);
            } catch (Exception e) {
                log.error("invoke(): doExecute() a lev�e une exception !", e); //$NON-NLS-1$
                invokeInEDT(UPDATE_VIEW_ERROR, null, null, e);
            } finally {
                invokeInEDT(POST_EXECUTE, param, null, null);
            }
        }



        private Object invokeInEDT(final int service, final P param, final T data, final Exception e) {
            SwingJob<Object> sj = new SwingJob<Object>() {

                @Override
                protected Object run() {
                    switch (service) {
                        case SHOULD_EXECUTE :
                            return action.shouldExecute(param);
                        case PRE_EXECUTE :
                            action.preExecute(param);
                            return null;
                        case UPDATE_VIEW_SUCCESS :
                            action.updateViewSuccess(data);
                            return null;
                        case UPDATE_VIEW_ERROR :
                            action.updateViewError(e);
                            return null;
                        case POST_EXECUTE :
                            action.postExecute();
                            return null;
                    }
                    return null;
                }
            };
            return sj.invokeAndWait();
        }
    }
}
