package com.dolphland.client.util.input;

import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JPasswordField;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.UIHideCtxInfoEvt;
import com.dolphland.client.util.event.UIShowCtxInfoEvt;
import com.dolphland.client.util.i18n.MessageFormater;
import com.dolphland.client.util.string.StrUtil;
import com.dolphland.client.util.swingverifiers.Afficheur;

public class PasswordTextField extends JPasswordField {

    private static final MessageFormater fmt = MessageFormater.getFormater(PasswordTextField.class);

    /**
     * Next focus d�faut
     */
    public static final boolean DEFAULT_FOCUS_NEXT_ENABLED = true;

    /**
     * Liste des policys
     */
    public static final int NORMAL_CASE = 0;
    public static final int ALWAYS_UPPER_CASE = 1;
    public static final int ALWAYS_LOWER_CASE = 2;

    /**
     * Indicateur de si la bulle ctxInfo est active ou non par d�faut
     */
    public final static boolean DEFAULT_CTXINFO_ENABLED = true;

    /**
     * Dur�e d'affichage de l'info bulle par d�faut
     */
    public final static long DEFAULT_CTXINFO_LIFE_TIME = CharacterTextField.DEFAULT_CTXINFO_LIFE_TIME;

    /**
     * Variables de classe
     * */
    private int nbChar;
    private String representation;
    private String enCoursInsertString;
    private boolean focusNextEnabled;
    private int casePolicy;
    private boolean ctxInfoEnabled;
    private long ctxInfoLifeTime;
    private Afficheur afficheur;
    private Afficheur ctxInfoAfficheur;
    private boolean insertionOK;



    /**
     * construit un PasswordTextField
     */
    public PasswordTextField() {
        this(CharacterTextField.MAX_VALUE);
    }



    /**
     * construit un PasswordTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     */
    public PasswordTextField(int nbChar) {
        this(nbChar, null);
    }



    /**
     * construit un PasswordTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     */
    public PasswordTextField(int nbChar, Afficheur afficheur) {
        this(nbChar, afficheur, DEFAULT_FOCUS_NEXT_ENABLED);
    }



    /**
     * construit un PasswordTextField
     * 
     * @param nbChar
     *            : nb caract�re pouvant �tre saisis
     * @param afficheur
     *            : afficheur de message d'erreur
     * @param focusNextEnabled
     *            : transfert le focus au prochain composant focusable � la fin
     *            de la saisie
     */
    public PasswordTextField(int nbChar, Afficheur afficheur, boolean focusNextEnabled) {
        this.nbChar = nbChar;
        this.afficheur = afficheur != null ? afficheur : new DefaultAfficheur();
        this.ctxInfoAfficheur = new CtxInfoAfficheur();
        this.focusNextEnabled = focusNextEnabled;
        this.representation = ""; //$NON-NLS-1$
        this.enCoursInsertString = ""; //$NON-NLS-1$
        this.ctxInfoEnabled = DEFAULT_CTXINFO_ENABLED;
        this.ctxInfoLifeTime = DEFAULT_CTXINFO_LIFE_TIME;
        this.insertionOK = false;
        initialize();
    }



    @Override
    public boolean isFocusable() {
        // Emp�che le focus d'un composant non �ditable, non enabled ou non
        // visible
        return super.isFocusable() && isEditable() && isEnabled() && isVisible();
    }



    // METHODES PUBLIQUES

    /**
     * Fixe le nombre de caract�res
     */
    public void setNbChar(int nbChar) {
        this.nbChar = nbChar;
    }



    /**
     * D�finit l'afficheur
     */
    public void setAfficheur(Afficheur afficheur) {
        this.afficheur = afficheur != null ? afficheur : new DefaultAfficheur();
    }



    /**
     * Fixe le next focus
     */
    public void setFocusNextEnabled(boolean focusNextEnabled) {
        this.focusNextEnabled = focusNextEnabled;
    }



    /**
     * Fixe la policy
     */
    public void setCasePolicy(int casePolicy) {
        if (casePolicy != ALWAYS_UPPER_CASE && casePolicy != ALWAYS_LOWER_CASE && casePolicy != NORMAL_CASE) {
            throw new IllegalArgumentException("invalid casePolicy"); //$NON-NLS-1$
        }
        this.casePolicy = casePolicy;
    }



    /**
     * Fournit le nombre de caract�res
     */
    public int getNbChar() {
        return nbChar;
    }



    /**
     * Fournit la policy
     */
    public int getCasePolicy() {
        return casePolicy;
    }



    /**
     * Indique si le next focus est actif
     */
    public boolean isFocusNextEnabled() {
        return focusNextEnabled;
    }



    /**
     * Fournit l'afficheur
     */
    public Afficheur getAfficheur() {
        return afficheur;
    }



    /**
     * Indique si la bulle ctxInfo est active ou non
     */
    public boolean isCtxInfoEnabled() {
        return ctxInfoEnabled;
    }



    /**
     * Fixe si la bulle ctxInfo est active ou non
     * 
     * @param ctxInfoEnabled
     */
    public void setCtxInfoEnabled(boolean ctxInfoEnabled) {
        this.ctxInfoEnabled = ctxInfoEnabled;
    }



    /**
     * Fournit la dur�e de vie de la bulle ctxInfo
     */
    public long getCtxInfoLifeTime() {
        return ctxInfoLifeTime;
    }



    /**
     * Fixe la dur�e de vie de la bulle ctxInfo
     * 
     * @param ctxInfoLifeTime
     */
    public void setCtxInfoLifeTime(long ctxInfoLifeTime) {
        this.ctxInfoLifeTime = ctxInfoLifeTime;
    }



    /**
     * Renvoie vrai si la valeur est vide
     */
    public boolean isEmpty() {
        return StrUtil.isEmpty(stringValue());
    }



    // METHODES INTERNES AU PACKAGE

    /**
     * Initialise le PasswordTextField
     */
    protected void initialize() {
        addFocusListener(new PasswordTextFieldFocusListener());
        setDocument(new CharacterDocument());
    }



    /**
     * Fixe la valeur � partir d'un texte (String)
     */
    public void setValue(String value) {
        setText(value);
    }



    /**
     * Renvoie la valeur sous la forme d'une cha�ne de caract�res (String)
     */
    public String stringValue() {
        return new String(getPassword());
    }



    /**
     * Affiche message de format invalide
     */
    protected void invalidFormat() {
        insertionOK = false;
        Toolkit.getDefaultToolkit().beep();
        String message = fmt.format("PasswordTextField.RS_INVALID_FORMAT") + getCorrectFormat(); //$NON-NLS-1$
        afficheur.erreur(message);
        ctxInfoAfficheur.erreur(message);
    }



    /**
     * @return le format correct
     */
    protected String getCorrectFormat() {
        return getNbChar() + fmt.format("PasswordTextField.RS_CHARACTERS"); //$NON-NLS-1$
    }



    @Override
    public void transferFocus() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                PasswordTextField.super.transferFocus();
            }
        });
    }

    /**
     * Document permettant de saisir des caract�res
     */
    protected class CharacterDocument extends PlainDocument {

        /**
         * insert la chaine str
         */
        public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
            int len = str.length();
            String currentChar;
            int nbInsertionOK = 0;
            for (int cpt = 0; cpt < len; cpt++) {
                currentChar = String.valueOf(str.charAt(cpt));
                insertOneChar(offs + nbInsertionOK, currentChar, a);
                if (isInsertionOK()) {
                    nbInsertionOK++;
                }
            }
        }



        /**
         * insertion d'un seul caract�re
         */
        public void insertOneChar(int offs, String str, AttributeSet a) throws BadLocationException {
            afficheur.clear();
            ctxInfoAfficheur.clear();

            if (!isDansRepresentation(offs, str) || getRepresentation().length() < nbChar) {
                if (casePolicy == ALWAYS_UPPER_CASE) {
                    str = str.toUpperCase();
                } else {
                    if (casePolicy == ALWAYS_LOWER_CASE) {
                        str = str.toLowerCase();
                    }
                }
                insertionOK = true;
                super.insertString(offs, str, a);
                if (isDansRepresentation(offs, str)) {
                    addStringRepresentation(offs, str);
                    enCoursInsertString = enCoursInsertString.substring(0, offs) + str + enCoursInsertString.substring(offs + 1);
                }
                if (isFocusNextEnabled() && getRepresentation().length() == nbChar) {
                    // passe au champ suivant
                    transferFocus();
                }
                return;
            }
            invalidFormat();
        }



        /**
         * suppression de caract�res
         */
        protected void removeUpdate(DefaultDocumentEvent chng) {
            int offset = chng.getOffset();
            int length = chng.getLength();
            delStringRepresentation(offset, length);
        }



        /**
         * insertion effectu�e
         */
        protected void insertUpdate(DefaultDocumentEvent chng, AttributeSet attr) {
            super.insertUpdate(chng, attr);
            enCoursInsertString = PasswordTextField.this.getText();
        }
    }



    /**
     * ajout du texte dans la repr�sentation
     * 
     * @param offset
     *            : la position de l'ajout
     * @param newString
     *            : la nouvelle chaine � ajouter
     */
    private void addStringRepresentation(int offset, String newString) {
        int offsetInRepresentation = getOffsetInRepresentation(offset);
        if (!getRepresentation().equals("")) { //$NON-NLS-1$
            representation = getRepresentation().substring(0, offsetInRepresentation) + newString + getRepresentation().substring(offsetInRepresentation, getRepresentation().length());
        } else {
            representation = newString;
        }
    }



    /**
     * supprime du texte dans la repr�sentation
     * 
     * @param offset
     *            : la position de la suppression
     * @param lenght
     *            : le nombre de caract�re a supprim�
     */
    private void delStringRepresentation(int offset, int lenght) {
        int offsetInRepresentation = getOffsetInRepresentation(offset);
        if (offsetInRepresentation < 0) {
            setRepresentation("");//efface tout //$NON-NLS-1$
        }
        else {
            StringBuffer resultat = new StringBuffer();
            resultat.append(getRepresentation().substring(0, offsetInRepresentation));

            int lenghtInRepresentation = 0, cpt = 0;
            while (cpt < lenght) {
                if (isDansRepresentation(offset, enCoursInsertString.substring(offset + cpt, offset + cpt + 1))) {
                    lenghtInRepresentation++;
                }
                cpt++;
            }
            resultat.append(getRepresentation().substring(offsetInRepresentation + lenghtInRepresentation, getRepresentation().length()));

            setRepresentation(resultat.toString());
        }
    }



    /**
     * supprime un seul caract�re dans la repr�sentation
     * 
     * @param offset
     *            : la position de la suppression
     */
    protected int getOffsetInRepresentation(int offset) {
        if (offset <= 0)
            return 0;
        int decalage = 0;
        // enlever tous les caract�res pr�c�dents offset qui ne font pas parti
        // de la repr�sentation
        for (int i = 0; i < offset; i++) {
            if (!isDansRepresentation(i, enCoursInsertString.substring(i, i + 1))) {
                decalage++;
            }
        }
        return offset - decalage;
    }



    protected boolean isInsertionOK() {
        return insertionOK;
    }



    /**
     * 
     * @param offset
     * @param newString
     * @return true si le texte donn�e fait partie de la repr�sentation
     */
    protected boolean isDansRepresentation(int offset, String newString) {
        return true;
    }



    /**
     * Fournit la repr�sentation
     */
    protected String getRepresentation() {
        return representation;
    }



    /**
     * Fixe la repr�sentation
     */
    protected void setRepresentation(String representation) {
        this.representation = representation;
    }



    private PasswordTextField getInstance() {
        return this;
    }

    private class PasswordTextFieldFocusListener implements FocusListener {

        /**
         * Si gain de focus, s�lection du texte
         */
        public void focusGained(java.awt.event.FocusEvent e) {
            SwingUtilities.invokeLater(new Runnable() {

                public void run() {
                    PasswordTextField.this.selectAll();
                }
            });
        }



        /**
         * Si perte de focus, efface l'afficheur
         */
        public void focusLost(FocusEvent e) {
            afficheur.clear();
            ctxInfoAfficheur.clear();
        }
    }

    /**
     * Afficheur par d�faut
     * 
     * @author jeremy.scafi
     */
    private class DefaultAfficheur implements Afficheur {

        /**
         * Clear du Afficheur
         */
        public void clear() {
        }



        /**
         * Erreur du Afficheur
         */
        public void erreur(String message) {
        }



        /**
         * Info du Afficheur
         */
        public void info(String message) {
        }
    }

    /**
     * Afficheur en utilisant des bulles CtxInfo
     * 
     * @author jeremy.scafi
     */
    private class CtxInfoAfficheur implements Afficheur {

        /**
         * Clear du Afficheur
         */
        public void clear() {
            if (isCtxInfoEnabled() && getParent() != null) {
                EDT.postEvent(new UIHideCtxInfoEvt(getInstance()));
            }
        }



        /**
         * Erreur du Afficheur
         */
        public void erreur(String message) {
            if (isCtxInfoEnabled() && getParent() != null) {
                UIShowCtxInfoEvt evt = new UIShowCtxInfoEvt(getInstance(), message, UIShowCtxInfoEvt.TOP);
                evt.setLifeTime(ctxInfoLifeTime);
                EDT.postEvent(evt);
            }
        }



        /**
         * Info du Afficheur
         */
        public void info(String message) {
            if (isCtxInfoEnabled() && getParent() != null) {
                UIShowCtxInfoEvt evt = new UIShowCtxInfoEvt(getInstance(), message, UIShowCtxInfoEvt.TOP);
                evt.setLifeTime(ctxInfoLifeTime);
                EDT.postEvent(evt);
            }
        }
    }
}
