package com.dolphland.client.util.service;

import com.dolphland.client.util.exception.AppException;

public interface ServiceResultListener {

    public void setResult(Object o);



    public void setError(AppException o);

}
