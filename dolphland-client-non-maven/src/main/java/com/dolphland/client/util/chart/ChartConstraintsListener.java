package com.dolphland.client.util.chart;

/**
 * Interface permettant de r�percuter les changements de contraintes � r�aliser
 * sur le chart
 * 
 * @author jeremy.scafi
 */
interface ChartConstraintsListener {

    /** Fixe le titre du chart */
    public void doChangeChartTitle();



    /** Fixe le titre de l'axe X du chart */
    public void doChangeAxeXTitle();



    /** Fixe le titre de l'axe Y du chart */
    public void doChangeAxeYTitle();



    /** Fixe la visibilit� de l'axe X du chart */
    public void doChangeAxeXVisible();



    /** Fixe la visibilit� de l'axe Y du chart */
    public void doChangeAxeYVisible();



    /**
     * Indique si le chart est � l'horizontal (si ce n'est pas le cas, il sera �
     * la vertical)
     */
    public void doChangeHorizontal();



    /** Indique le min et le max du range de l'axe Y */
    public void doChangeAxeYRange();



    /** Indique si le range de l'axe Y se pack automatiquement */
    public void doChangeAxeYRangeAutoPackable();



    /** D�finit la marge du range de l'axe Y apr�s l'auto pack */
    public void doChangeAxeYRangeAutoPackMarge();



    /** Indique le min et le max du range de l'axe X */
    public void doChangeAxeXRange();



    /** Indique si le range de l'axe X se pack automatiquement */
    public void doChangeAxeXRangeAutoPackable();



    /** D�finit la marge du range de l'axe X apr�s l'auto pack */
    public void doChangeAxeXRangeAutoPackMarge();



    /** Indique si le zoom par clic est possible */
    public void doChangeZoomWithClickEnabled();



    /** Fixe la position de la barre de boutons */
    public void doChangeButtonsPanelPosition();



    /** Fixe la position de la l�gende */
    public void doChangeLegendePosition();



    /** Indique si les items de la l�gende sont cochables */
    public void doChangeLegendeCheckable();



    /**
     * Indique si la l�gende est sous une forme compress�e pouvant s'�tendre si
     * souris dessus
     */
    public void doChangeLegendeExtendable();



    /** Fixe la visibilit� d'un menu item */
    public void doChangeMenuItemVisibility();



    /** Fixe le pas entre chaque valeur de l'axe des abscisses */
    public void doChangeAxeYPas();



    /** Fixe le pas entre chaque valeur de l'axe des ordonn�es */
    public void doChangeAxeXPas();



    /** Rend visible ou non les bordures du chart */
    public void doChangeBorderVisibility();



    /** Fixe les couleurs des donn�es du chart */
    public void doChangeColorsChartData();



    /** Fixe les formes des donn�es du chart */
    public void doChangeShapesChartData();



    /** Fixe la couleur de s�lection */
    public void doChangeSelectionColor();



    /** Fixe la visibilit� des �tiquettes */
    public void doChangeLabelsVisible();



    /** Fixe l'angle de d�part des donn�es du chart (si le chart en poss�de un) */
    public void doChangeStartAngle();



    /** Fixe le g�n�rateur de tooltip */
    public void doChangeToolTipGenerator();
}
