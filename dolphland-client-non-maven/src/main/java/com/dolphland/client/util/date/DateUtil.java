package com.dolphland.client.util.date;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Date util
 * 
 * @author JayJay
 * 
 */
public class DateUtil {

    // Date formats
    public final static SimpleDateFormat FRM_DATE_TIME_US = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final static SimpleDateFormat FRM_DATE_US = new SimpleDateFormat("yyyy-MM-dd");
    public final static SimpleDateFormat FRM_TIME_US = new SimpleDateFormat("HH:mm:ss");

    public final static SimpleDateFormat FRM_DATE_TIME_FR = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    public final static SimpleDateFormat FRM_DATE_FR = new SimpleDateFormat("dd-MM-yyyy");
    public final static SimpleDateFormat FRM_TIME_FR = new SimpleDateFormat("HH:mm:ss");



    /**
     * Format a date to a xml date
     * 
     * @param date
     *            Date
     * 
     * @return Xml date
     */
    public static XMLGregorianCalendar toXmlDate(Date date) {
        if (date == null) {
            return null;
        }
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
            return null;
        }
        return xmlCalendar;
    }



    /**
     * Format a xml date to a date
     * 
     * @param xmlDate
     *            Xml date
     * 
     * @return Date
     */
    public static Date toDate(XMLGregorianCalendar xmlDate) {
        if (xmlDate == null) {
            return null;
        }
        return xmlDate.toGregorianCalendar().getTime();
    }
}
