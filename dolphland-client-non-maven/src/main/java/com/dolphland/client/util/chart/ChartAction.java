package com.dolphland.client.util.chart;

import javax.swing.Icon;

import com.dolphland.client.util.action.UIAction;

/**
 * Class based on the Swing Actions system to execute a transaction (MVCTx) from
 * one or many Swing components
 * from a chart.<br>
 * <br>
 * Type:
 * <ul>
 * <li>T: Action result from a method <code>doExecute</code>
 * </ul>
 * <br>
 * Recommendation for using actions (<code>UIAction</code> and derivatives): <br>
 * <ul>
 * <li>Create actions inside scenario (internal class) and not in own class.<br>
 * The views are expected to manipulate only <code>UIAction</code> getting by
 * the scenario from a setter.
 * <li>Avoid if possible the using of variables in actions because of
 * multithreading
 * <li>Declare actions events in private package while following the
 * NomActionEvt norm
 * <li>Avoid if possible to make setEnabled from the view.<br>
 * The scenario must take the decision and not the view.
 * </ul>
 * <br>
 * Example respecting the using recommendations:<br>
 * <br>
 * <ol>
 * <li>Create event class extended of <code>UIActionEvt</code>, that contains
 * all necessary parameters by the action <br>
 * <code>
 * <pre>
 * class MonActionEvt extends UIActionEvt{
 * 	private String text;
 * 	public void setText(String text){
 * 		this.text = text;
 * 	}
 * 	public String getText(){
 * 		return text;
 * 	}
 * }
 * </pre>
 * </code>
 * 
 * <li>In the view, declare and then install action from a setter <code>
 * <pre>
 * UIAction&lt;MonActionEvt,?&gt; monAction ;
 *      
 * public void setMonAction(UIAction&lt;MonActionEvt,?&gt; monAction){
 * 	this.monAction = monAction;
 * 	monAction.install(btnMonAction); 
 * }
 * </pre>
 * </code>
 * 
 * <li>In the view, execute action during a Swing event with event action for
 * execution parameter <code>
 * <pre>
 * JButton btnMonAction;
 *      
 * public JButton getBtnMonAction(){
 * 	if(btnMonAction == null){
 * 		btnMonAction = new JButton("Go Action");
 * 		btnMonAction.addActionListener(new ActionListener(){
 * 			public void actionPerformed(ActionEvent e) {
 * 				if(monAction != null){
 * 					MonActionEvt evt = new MonActionEvt();
 * 					evt.setText("Hello");
 * 					monAction.execute(evt);
 * 				}
 * 			}			
 * 		});
 * 	}
 * 	return btnMonAction;
 * }
 * </pre>
 * </code>
 * 
 * <li>In the scenario, create the internal class and then affect action in the
 * view <code>
 * <pre> 
 * private class MonAction extends UIAction&lt;MonActionEvt,Boolean&gt;{
 * 	protected Boolean doExecute(MonActionEvt param) {			
 * 		log.info("Ex�cution de MonAction : " + param.getText());
 * 		return true;
 * 	}
 * }
 * 
 * view.setMonAction(new MonAction());
 * </pre>
 * </code>
 * </ol>
 * 
 * @author J�r�my Scafi
 * @author JayJay
 * 
 */
public class ChartAction<T> extends UIAction<ChartActionEvt, T> {

    /**
     * Default constructor
     */
    public ChartAction() {
        super();
        setTriggerModifier(TRIGGER_CONTEXT);
    }



    /**
     * Constructor with the name specified
     * 
     * @param name
     *            Action's name
     */
    public ChartAction(String name) {
        super(name);
        setTriggerModifier(TRIGGER_CONTEXT);
    }



    /**
     * Constructor where the name and the icon
     * 
     * @param name
     *            Action's name
     * @param icon
     *            Action's icon
     */
    public ChartAction(String name, Icon icon) {
        super(name, icon);
        setTriggerModifier(TRIGGER_CONTEXT);
    }

}
