package com.dolphland.client.util.event;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.action.UIActionEvt;
import com.dolphland.client.util.transaction.MVCTx;

public class RegisterKeyEvent extends UIEvent {

    public static final EventId ID = new EventId(FwkEvents.nextFwkId(), "REGISTER_KEY_ACTION_EVT"); //$NON-NLS-1$

    public static final int DEFAULT_KEY_MODIFIER = 0;

    private Object action;
    private Object postAction;
    private int keyCode;
    private boolean keyReleased;
    private boolean metaDown;
    private boolean ctrlDown;
    private boolean altDown;
    private boolean shiftDown;
    private boolean altGraphDown;
    private boolean repeat;
    private boolean replaceExisting;

    // Indicator used by FwkEventQueue.KeyActionManager to repeat action only is
    // repeat indicator is on
    boolean actionExecuted;



    private RegisterKeyEvent(Runnable action) {
        super(ID);
        this.action = action;
    }



    private RegisterKeyEvent(UIAction<UIActionEvt, ?> action) {
        super(ID);
        this.action = action;
    }



    private RegisterKeyEvent(MVCTx<Object, ?> action) {
        super(ID);
        this.action = action;
    }



    /**
     * Register the specified action to the key manager. This manager execute
     * action if key sequence is typed.
     * 
     * By default, the specified action is adding when one or more actions exist
     * for the key sequence. It's possible replace them when the indicator
     * replaceExisting is on.
     * 
     * @param action
     *            Action
     * 
     * @return Register key event
     */
    public static RegisterKeyEvent forAction(Runnable action) {
        return new RegisterKeyEvent(action);
    }



    /**
     * Register the specified action to the key manager. This manager execute
     * action if key sequence is typed.
     * 
     * By default, the specified action is adding when one or more actions exist
     * for the key sequence. It's possible replace them when the indicator
     * replaceExisting is on.
     * 
     * @param action
     *            Action
     * 
     * @return Register key event
     */
    public static RegisterKeyEvent forAction(UIAction<UIActionEvt, ?> action) {
        return new RegisterKeyEvent(action);
    }



    /**
     * Register the specified action to the key manager. This manager execute
     * action if key sequence is typed.
     * 
     * By default, the specified action is adding when one or more actions exist
     * for the key sequence. It's possible replace them when the indicator
     * replaceExisting is on.
     * 
     * @param action
     *            Action
     * 
     * @return Register key event
     */
    public static RegisterKeyEvent forAction(MVCTx<Object, ?> action) {
        return new RegisterKeyEvent(action);
    }



    /**
     * Set the key code of the key that will trigger the action
     */
    public RegisterKeyEvent keyCode(int keyCode) {
        this.keyCode = keyCode;
        return this;
    }



    /**
     * Set the action to be executed after the main ation
     */
    public RegisterKeyEvent postAction(Runnable postAction) {
        this.postAction = postAction;
        return this;
    }



    /**
     * Set the action to be executed after the main ation
     */
    public RegisterKeyEvent postAction(MVCTx<Object, ?> postAction) {
        this.postAction = postAction;
        return this;
    }



    /**
     * Set the action to be executed after the main ation
     */
    public RegisterKeyEvent postAction(UIAction<UIActionEvt, ?> postAction) {
        this.postAction = postAction;
        return this;
    }



    /**
     * Set wether the action should be tiggered on key released
     */
    public RegisterKeyEvent keyReleased() {
        this.keyReleased = true;
        return this;
    }



    /**
     * Set wether meta key should be down for triggering the action
     */
    public RegisterKeyEvent metaDown() {
        this.metaDown = true;
        return this;
    }



    /**
     * Set wether ctrl key should be down for triggering the action
     */
    public RegisterKeyEvent ctrlDown() {
        this.ctrlDown = true;
        return this;
    }



    /**
     * Set wether alt key should be down for triggering the action
     */
    public RegisterKeyEvent altDown() {
        this.altDown = true;
        return this;
    }



    /**
     * Set wether shift key should be down for triggering the action
     */
    public RegisterKeyEvent shiftDown() {
        this.shiftDown = true;
        return this;
    }



    /**
     * Set wether altGraph key should be down for triggering the action
     */
    public RegisterKeyEvent altGraphDown() {
        this.altGraphDown = true;
        return this;
    }



    /**
     * Set wether the action should be repeated while the key is maintained
     * pressed.
     */
    public RegisterKeyEvent repeat() {
        this.repeat = true;
        return this;
    }



    /**
     * If one or more actions exists for the key sequence, replace them by the
     * action specified
     * 
     * @return True if replace is needed
     */
    public RegisterKeyEvent replaceExisting() {
        this.replaceExisting = true;
        return this;
    }



    /**
     * @return The main action to be executed
     */
    public Object getAction() {
        return action;
    }



    /**
     * @return The keyCode that will trigger the main action
     */
    public int getKeyCode() {
        return keyCode;
    }



    /**
     * @return The action to be executed after the main action
     */
    public Object getPostAction() {
        return postAction;
    }



    /**
     * @return The keyReleased state
     */
    public boolean isKeyReleased() {
        return keyReleased;
    }



    /**
     * @return The metaDown state
     */
    public boolean isMetaDown() {
        return metaDown;
    }



    /**
     * @return The ctrlDown state
     */
    public boolean isCtrlDown() {
        return ctrlDown;
    }



    /**
     * @return The altDown state
     */
    public boolean isAltDown() {
        return altDown;
    }



    /**
     * @return The shiftDown state
     */
    public boolean isShiftDown() {
        return shiftDown;
    }



    /**
     * @return The altGraphDown state
     */
    public boolean isAltGraphDown() {
        return altGraphDown;
    }



    /**
     * @return The repeat state
     */
    public boolean isRepeat() {
        return repeat;
    }



    /**
     * @return The replaceExisting state
     */
    public boolean isReplaceExisting() {
        return replaceExisting;
    }
}
