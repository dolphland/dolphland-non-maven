package com.dolphland.client.util.animator;

import com.dolphland.client.util.action.UIActionEvt;

/**
 * Context for an animation's action
 * 
 * @author p-jeremy.scafi
 * 
 */
public class AnimationActionEvt extends UIActionEvt {

    private String animatedComponentId;



    public String getAnimatedComponentId() {
        return animatedComponentId;
    }



    public void setAnimatedComponentId(String animatedComponentId) {
        this.animatedComponentId = animatedComponentId;
    }
}
