package com.dolphland.client.util.event;

import com.dolphland.client.util.string.StrUtil;

public class StepLoadingAppEvt extends Event {

    private String texte;
    private int compteur;



    public StepLoadingAppEvt(int compteur, String texte) {
        super(FwkEvents.STEP_LOADING_APP_EVT);
        this.texte = texte;
        this.compteur = compteur;
    }



    public StepLoadingAppEvt(int compteur) {
        this(compteur, StrUtil.EMPTY_STRING);
    }



    public String getTexte() {
        return texte;
    }



    public void setTexte(String texte) {
        this.texte = texte;
    }



    public int getCompteur() {
        return compteur;
    }



    public void setCompteur(int compteur) {
        this.compteur = compteur;
    }

}
