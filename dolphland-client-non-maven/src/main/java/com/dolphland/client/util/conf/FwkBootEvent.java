/*
 * $Log: FwkBootEvent.java,v $
 * Revision 1.5 2012/07/23 13:00:38 bvatan
 * - added command line arguments attributes
 * - javadoc
 * Revision 1.4 2008/12/05 14:22:23 bvatan
 * - gestion desl isteners de boot
 */

package com.dolphland.client.util.conf;

/**
 * <p>
 * Event dispatched by <code>FwkBoot</code> and delivered by
 * {@link FwkBootListener} when application is booting.
 * </p>
 * 
 * @author JayJay
 */
public class FwkBootEvent {

    private FwkBoot boot;
    private CEnvironement environment;
    private String[] args;



    /**
     * <p>
     * Builds a new event
     * </p>
     * 
     * @param boot
     *            The booter that is dispatching this event
     * @param args
     *            The application command line arguments.
     */
    FwkBootEvent(FwkBoot boot, String[] args) {
        this(boot, args, null);
    }



    /**
     * <p>
     * Builds a new event
     * </p>
     * 
     * @param boot
     *            The booter that is dispatching this event
     * @param args
     *            The application command line arguments.
     * @param environment
     *            The application environment configuration (null is allowed)
     */
    FwkBootEvent(FwkBoot boot, String[] args, CEnvironement environment) {
        this.boot = boot;
        this.args = args;
        this.environment = environment;
    }



    /**
     * <p>
     * Get the booter instance that is delivering this event
     * </p>
     * 
     * @return the booter instance that is dispatching this event.
     */
    public FwkBoot getBoot() {
        return boot;
    }



    /**
     * <p>
     * Get the selected configuration environment. This might be null when the
     * event is delivered in the very early stage of application startup.
     * </p>
     * 
     * @return The configuration environement or null if none is yet available.
     */
    public CEnvironement getEnvironment() {
        return environment;
    }



    /**
     * <p>
     * Get the application command line arguments.
     * </p>
     * 
     * @return Array of application command line arguments
     */
    public String[] getArgs() {
        return args;
    }
}
