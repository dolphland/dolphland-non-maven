package com.dolphland.client.util.application;

interface Messenger {

    public void showErrorDialog(String message);



    public void showErrorMessage(String message);



    public void showWarningDialog(String message);



    public void showWarningMessage(String message);



    public void showInfoDialog(String message);



    public void showInfoMessage(String message);



    public boolean showConfirmDialog(String message);



    public String showInputDialog(String message);



    public String showInputDialog(String message, String initialInputValue);



    public Object showInputDialog(String message, Object[] elements);



    public String showPasswordDialog(String msg);



    public String showPasswordDialog(String msg, String initialPasswordValue);



    public void clearMessages();
}
