/*
 * $Log: AppManagerTask.java,v $
 * Revision 1.7 2014/03/10 16:06:13 bvatan
 * - Internationalized
 * Revision 1.6 2013/04/18 12:01:02 bvatan
 * - extracted log4j configuration for extra appender (local file system log
 * file creation) in a dedicated class
 * Revision 1.5 2010/11/23 15:53:48 jscafi
 * - initialisation du boolean super utilisateur
 * Revision 1.4 2010/11/23 15:31:06 bvatan
 * - renseignement de FwkAppContext avec le bool�en super utilisateur
 * Revision 1.3 2010/11/23 10:44:56 bvatan
 * - gestion des SuperUserEvent
 * Revision 1.2 2010/07/22 09:02:07 bvatan
 * - gestion des logs de l'application dans un r�pertoire local de la machine
 * qui ex�cute l'appli
 * Revision 1.1 2009/06/22 07:27:34 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import org.apache.log4j.Logger;

import com.dolphland.client.util.event.AppClosedEvt;
import com.dolphland.client.util.event.AppClosingEvt;
import com.dolphland.client.util.event.CloseAppRequestEvt;
import com.dolphland.client.util.event.EDT;
import com.dolphland.client.util.event.Event;
import com.dolphland.client.util.event.EventDestination;
import com.dolphland.client.util.event.EventInterceptor;
import com.dolphland.client.util.event.EventInterceptorException;
import com.dolphland.client.util.event.EventMulticaster;
import com.dolphland.client.util.event.FwkEvents;
import com.dolphland.client.util.task.Task;
import com.dolphland.client.util.task.TaskContext;

public class AppManagerTask extends Task {

    private static final Logger log = Logger.getLogger(AppManagerTask.class);



    public AppManagerTask() {
        super("AppManagerTask"); //$NON-NLS-1$
        registerEvent(FwkEvents.CLOSE_APP_REQUEST_EVT);
    }



    @Override
    protected void init(TaskContext ctx) {
        EventMulticaster.getInstance().addEventInterceptor(new AppManagerEventInterceptor());
    }



    protected void handleEvent(CloseAppRequestEvt e) throws InterruptedException {
        log.info("handleEvent(): Dispatching AppClosingEvt..."); //$NON-NLS-1$
        AppClosingEvt closingEvent = new AppClosingEvt();
        EDT.postEventAndWait(closingEvent);
        if (!closingEvent.isVetoClosing()) {
            AppClosedEvt closedEvt = new AppClosedEvt();
            log.info("handleEvent(): Dispatching AppClosedEvt..."); //$NON-NLS-1$
            EDT.postEventAndWait(closedEvt);
            if (e.shouldSystemExitOnSuccess()) {
                log.info("handleEvent(): Shutting down."); //$NON-NLS-1$
                System.exit(0);
            }
        } else {
            log.warn("handleEvent(): Request for closing has been vetoed ! Not closing..."); //$NON-NLS-1$
        }
    }

    private class AppManagerEventInterceptor extends EventInterceptor {

        @Override
        public boolean processEvent(Event evt, EventDestination dest) throws EventInterceptorException {
            if (evt instanceof AppClosedEvt) {
                if (!Thread.currentThread().getName().equals(getName())) {
                    throw new EventInterceptorException("It's forbidden to send AppClosedEvt, use CloseAppRequestEvt instead !"); //$NON-NLS-1$
                }
            } else if (evt instanceof AppClosingEvt) {
                if (!Thread.currentThread().getName().equals(getName())) {
                    throw new EventInterceptorException("It's forbidden to send AppClosingEvt, use CloseAppRequestEvt instead !"); //$NON-NLS-1$
                }
            }
            return true;
        }
    }

}
