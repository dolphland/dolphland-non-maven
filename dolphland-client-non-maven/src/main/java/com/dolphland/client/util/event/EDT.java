/*
 * $Log: EDT.java,v $
 * Revision 1.8 2010/11/12 15:46:59 bvatan
 * - Ajout support des schedules de type crontab
 * Revision 1.7 2009/03/24 13:52:11 bvatan
 * - ajout raccourci schedule()
 * Revision 1.6 2009/03/06 12:48:33 bvatan
 * - ajout raccourci vers postEvent(Event,EventDestination)
 * Revision 1.5 2009/03/05 14:59:49 bvatan
 * - mise� niveau EventId -> EventDestination
 * Revision 1.4 2009/02/19 08:36:00 bvatan
 * - ajout postEventAndWait()
 * Revision 1.3 2008/12/03 13:30:21 bvatan
 * - gestion des�v�nements barri�re
 * Revision 1.2 2008/08/12 14:03:03 bvatan
 * - ajout services start() et stop()
 * Revision 1.1 2008/07/07 11:22:02 bvatan
 * - initial check in
 */

package com.dolphland.client.util.event;

import java.util.List;

/**
 * Helper pour l'abonnement et l'envoie d'�v�nements par le EventMulticaster.<br>
 * Cette classe de nom court permet d'acc�der aux services du multicaster plus
 * facilement, exemple :<br>
 * <code>
 * 		// abonnement<br>
 * 		EDT.subcribe(eventSubscriber).forEvent(anEvent);<br>
 * 		// envoie d'�v�nement<br>
 * 		EDT.postEvent(anEvent);
 * </code>
 * 
 * @author JayJay
 */
public class EDT {

    /**
     * Raccourci pour EventMulticaster.getInstance().start();
     */
    public static void start() {
        EventMulticaster.getInstance().start();
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().stop();
     */
    public static void stop() {
        EventMulticaster.getInstance().stop();
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().postEvent(Event)
     */
    public static final void postEvent(Event evt) {
        EventMulticaster.getInstance().postEvent(evt);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().schedule(Schedule)
     */
    public static final Schedule schedule(Schedule sched) {
        return EventMulticaster.getInstance().schedule(sched);
    }



    /**
     * Programme l'�mission de l'�v�nement sp�cifi� selon les crit�re d'�mission
     * d�finis par l'expression<br>
     * crontab UNIX sp�cifi�e.<br>
     * La destination de l'�v�nement est celle de l'�v�nement sp�cifi�e.<br>
     * <br>
     * Voir documentation du crontab parser ci-dessous:<br>
     * <br>
     * A representation of a crontab string, as used for scheduling with the
     * *nix job scheduler {@link Cron http://en.wikipedia.org/wiki/Cron}. <br/>
     * <br/>
     * 
     * <h3>
     * Summary</h3>
     * 
     * A {@code CronTabExpression} may represent only one crontab string. Each
     * crontab string consists of five fields, as specified below*.
     * 
     * <pre>
     * .---------------- minute (0 - 59)
     * |  .------------- hour (0 - 23)
     * |  |  .---------- day of month (1 - 31)
     * |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
     * |  |  |  |  .---- day of week (0 - 7) (Sunday=0 or 7)  OR sun,mon,tue,wed,thu,fri,sat
     * |  |  |  |  |
     * *  *  *  *  *
     * </pre>
     * 
     * (* Figure originally from <a href="http://en.wikipedia.org/wiki/Cron">
     * Wikipedia</a>. Used under <a href=
     * "http://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License"
     * > Creative Commons Attribution-ShareAlike License</a> and available here
     * under same license.)
     * 
     * 
     * <h3>
     * Standard Entries</h3>
     * 
     * Fields may contain one of the following.
     * 
     * <ul>
     * <li>
     * An asterisk (*), which matches any value.</li>
     * <li>
     * A range of numbers. Ranges are represented by two numbers separated by a
     * hyphen. Ranges are inclusive.<br/>
     * For example, 8-11 for an "hours" entry matches hours 8, 9, 10 and 11.</li>
     * <li>
     * A list of numbers. A list is a set of numbers (or ranges) separated by
     * commas.<br/>
     * Examples: "1,2,5,9", "0-4,8-12".</li>
     * <li>
     * A range with a step value. A step entry is a range (or an asterisk)
     * followed by "/&lt;step number>". This type of entry matches any value in
     * the range that is exactly divisible by the step number. <br/>
     * For example, "0-23/2" can be used in the hours field to specify command
     * execution every other hour (equivalent to the list entry
     * "0,2,4,6,8,10,12,14,16,18,20,22").<br/>
     * Using an asterisk for the range will match every valid value for the
     * field that is exactly visible by the step number.<br/>
     * For example "*\/3" in the hours field will match every third hour.</li>
     * <li>
     * <b>month and day-of-week fields only:</b> A range or list value, with
     * month and day names (respectively) substituted for numeric values. Names
     * are case insensitive, and may be three-letter abbreviations or full
     * names. <br/>
     * Examples: "mon", "apr", "Friday", "AUGUST", "mon-fri", "jan, feb, dec"</li>
     * </ul>
     * <b>Note:</b> The day can be specified in either of two fields: day of
     * month, and day of week. If both fields are restricted (i.e., aren't *),
     * the expression will match any time at which either field is matched.<br/>
     * For example, "30 4 1,15 * 5" would match 4:30 am on the 1st and 15th of
     * each month, as well as every Friday.<br/>
     * <br/>
     * 
     * 
     * <h3>Special Entries</h3>
     * 
     * Instead of the first five fields, one of eight special strings may be
     * used: **<br/>
     * 
     * <table>
     * <tbody>
     * <tr>
     * <th>
     * Entry</th>
     * <th>
     * Description</th>
     * <th>
     * Equivalent To</th>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@yearly</code></td>
     * <td>
     * Run once a year</td>
     * <td>
     * <code>0 0 1 1 *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@annually</code></td>
     * <td>
     * (same as <code>@yearly</code>)</td>
     * <td>
     * <code>0 0 1 1 *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@monthly</code></td>
     * <td>
     * Run once a month</td>
     * <td>
     * <code>0 0 1 * *</code></td>
     * </tr>
     * <tr>
     * <td>
     * <code>@weekly</code></td>
     * <td>
     * Run once a week</td>
     * <td>
     * <code>0 0 * * 0</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@daily</code></td>
     * <td>
     * Run once a day</td>
     * <td>
     * <code>0 0 * * *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@midnight</code></td>
     * <td>
     * (same as <code>@daily</code>)</td>
     * <td>
     * <code>0 0 * * *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@hourly</code></td>
     * <td>
     * Run once an hour</td>
     * <td>
     * <code>0 * * * *</code></td>
     * </tr>
     * </tbody>
     * </table>
     * <br/>
     * (** Table originally from <a href="http://en.wikipedia.org/wiki/Cron">
     * Wikipedia</a>. Used under <a href=
     * "http://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License"
     * > Creative Commons Attribution-ShareAlike License</a> and available here
     * under same license.)
     * 
     * @param e
     *            L'�v�nement � programmer
     * @param expression
     *            L'expression UNIX crontab qui d�crit les crit�res d'�mission
     *            de l'�v�nement.
     */
    public static final CrontabSchedule schedule(Event e, String expression) {
        return schedule(e, null, expression);
    }



    /**
     * Programme l'�mission de l'�v�nement sp�cifi� versl a destination sp�cifi�
     * et selon les crit�res<br>
     * d'�mission d�finis par l'expression crontab UNIX sp�cifi�e<br>
     * <br>
     * Voir documentation du crontab parser ci-dessous:<br>
     * <br>
     * A representation of a crontab string, as used for scheduling with the
     * *nix job scheduler {@link Cron http://en.wikipedia.org/wiki/Cron}. <br/>
     * <br/>
     * 
     * <h3>
     * Summary</h3>
     * 
     * A {@code CronTabExpression} may represent only one crontab string. Each
     * crontab string consists of five fields, as specified below*.
     * 
     * <pre>
     * .---------------- minute (0 - 59)
     * |  .------------- hour (0 - 23)
     * |  |  .---------- day of month (1 - 31)
     * |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
     * |  |  |  |  .---- day of week (0 - 7) (Sunday=0 or 7)  OR sun,mon,tue,wed,thu,fri,sat
     * |  |  |  |  |
     * *  *  *  *  *
     * </pre>
     * 
     * (* Figure originally from <a href="http://en.wikipedia.org/wiki/Cron">
     * Wikipedia</a>. Used under <a href=
     * "http://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License"
     * > Creative Commons Attribution-ShareAlike License</a> and available here
     * under same license.)
     * 
     * 
     * <h3>
     * Standard Entries</h3>
     * 
     * Fields may contain one of the following.
     * 
     * <ul>
     * <li>
     * An asterisk (*), which matches any value.</li>
     * <li>
     * A range of numbers. Ranges are represented by two numbers separated by a
     * hyphen. Ranges are inclusive.<br/>
     * For example, 8-11 for an "hours" entry matches hours 8, 9, 10 and 11.</li>
     * <li>
     * A list of numbers. A list is a set of numbers (or ranges) separated by
     * commas.<br/>
     * Examples: "1,2,5,9", "0-4,8-12".</li>
     * <li>
     * A range with a step value. A step entry is a range (or an asterisk)
     * followed by "/&lt;step number>". This type of entry matches any value in
     * the range that is exactly divisible by the step number. <br/>
     * For example, "0-23/2" can be used in the hours field to specify command
     * execution every other hour (equivalent to the list entry
     * "0,2,4,6,8,10,12,14,16,18,20,22").<br/>
     * Using an asterisk for the range will match every valid value for the
     * field that is exactly visible by the step number.<br/>
     * For example "*\/3" in the hours field will match every third hour.</li>
     * <li>
     * <b>month and day-of-week fields only:</b> A range or list value, with
     * month and day names (respectively) substituted for numeric values. Names
     * are case insensitive, and may be three-letter abbreviations or full
     * names. <br/>
     * Examples: "mon", "apr", "Friday", "AUGUST", "mon-fri", "jan, feb, dec"</li>
     * </ul>
     * <b>Note:</b> The day can be specified in either of two fields: day of
     * month, and day of week. If both fields are restricted (i.e., aren't *),
     * the expression will match any time at which either field is matched.<br/>
     * For example, "30 4 1,15 * 5" would match 4:30 am on the 1st and 15th of
     * each month, as well as every Friday.<br/>
     * <br/>
     * 
     * 
     * <h3>Special Entries</h3>
     * 
     * Instead of the first five fields, one of eight special strings may be
     * used: **<br/>
     * 
     * <table>
     * <tbody>
     * <tr>
     * <th>
     * Entry</th>
     * <th>
     * Description</th>
     * <th>
     * Equivalent To</th>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@yearly</code></td>
     * <td>
     * Run once a year</td>
     * <td>
     * <code>0 0 1 1 *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@annually</code></td>
     * <td>
     * (same as <code>@yearly</code>)</td>
     * <td>
     * <code>0 0 1 1 *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@monthly</code></td>
     * <td>
     * Run once a month</td>
     * <td>
     * <code>0 0 1 * *</code></td>
     * </tr>
     * <tr>
     * <td>
     * <code>@weekly</code></td>
     * <td>
     * Run once a week</td>
     * <td>
     * <code>0 0 * * 0</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@daily</code></td>
     * <td>
     * Run once a day</td>
     * <td>
     * <code>0 0 * * *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@midnight</code></td>
     * <td>
     * (same as <code>@daily</code>)</td>
     * <td>
     * <code>0 0 * * *</code></td>
     * </tr>
     * 
     * <tr>
     * <td>
     * <code>@hourly</code></td>
     * <td>
     * Run once an hour</td>
     * <td>
     * <code>0 * * * *</code></td>
     * </tr>
     * </tbody>
     * </table>
     * <br/>
     * (** Table originally from <a href="http://en.wikipedia.org/wiki/Cron">
     * Wikipedia</a>. Used under <a href=
     * "http://en.wikipedia.org/wiki/Wikipedia:Text_of_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License"
     * > Creative Commons Attribution-ShareAlike License</a> and available here
     * under same license.)
     * 
     * @param e
     *            Ev�nement � programmer
     * @param dest
     *            Destination de l'�v�nement (pr�empte la desitnatoion de
     *            l'�v�nement sp�cifi�)
     * @param expression
     *            L'expression UNIX crontab qui d�crit les crit�res d'�mission
     *            de l'�v�nement.
     */
    public static final CrontabSchedule schedule(Event e, EventDestination dest, String expression) {
        CrontabSchedule sched = new CrontabSchedule(expression, e);
        if (dest != null) {
            sched.setDestination(dest);
        }
        EventMulticaster.getInstance().schedule(sched);
        return sched;
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().postEvent(Event,
     * EventDestination)
     */
    public static final void postEvent(Event evt, EventDestination destination) {
        EventMulticaster.getInstance().postEvent(evt, destination);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().postEventAndWait(Event)
     */
    public static final void postEventAndWait(Event evt) throws InterruptedException {
        EventMulticaster.getInstance().postEventAndWait(evt);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().subscribe(EventSubscriber)
     */
    public static final EventRegisterer subscribe(EventSubscriber es) {
        return EventMulticaster.getInstance().subscribe(es);
    }



    /**
     * Raccourci pour
     * EventMulticaster.getInstance().unsubscribe(EventSubscriber)
     */
    public static final EventRegisterer unsubscribe(EventSubscriber es) {
        return EventMulticaster.getInstance().unsubscribe(es);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().waitFor(EventId)
     */
    public static Event waitFor(EventDestination evtId) throws InterruptedException {
        return EventMulticaster.getInstance().waitFor(evtId);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().waitFor(EventId, long)
     */
    public static Event waitFor(EventDestination evtId, long millis) throws InterruptedException {
        return EventMulticaster.getInstance().waitFor(evtId, millis);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().waitForAll(long,
     * EventId...)
     */
    public static List<Event> waitForAll(long millis, EventDestination... evtIds) throws InterruptedException {
        return EventMulticaster.getInstance().waitForAll(millis, evtIds);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().waitForAll(EventId...)
     */
    public static List<Event> waitForAll(EventDestination... evtIds) throws InterruptedException {
        return EventMulticaster.getInstance().waitForAll(evtIds);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().waitForAny(long,EventId...)
     */
    public static Event waitForAny(long millis, EventDestination... evtIds) throws InterruptedException {
        return EventMulticaster.getInstance().waitForAny(millis, evtIds);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().waitForAny(EventId...)
     */
    public static Event waitForAny(EventDestination... evtIds) throws InterruptedException {
        return EventMulticaster.getInstance().waitForAny(evtIds);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().postEventAndWait(Event,
     * EventId, Class<T>, long)
     */
    public static <T extends Event> T postEventAndWait(Event evt, final EventDestination eid, final Class<T> eclass, long millis) throws InterruptedException {
        return EventMulticaster.getInstance().postEventAndWait(evt, eid, eclass, millis);
    }



    /**
     * Raccourci pour EventMulticaster.getInstance().postEventAndWait(Event,
     * EventId, Class<T>)
     */
    public static <T extends Event> T postEventAndWait(Event evt, final EventDestination eid, final Class<T> eclass) throws InterruptedException {
        return EventMulticaster.getInstance().postEventAndWait(evt, eid, eclass);
    }

}
