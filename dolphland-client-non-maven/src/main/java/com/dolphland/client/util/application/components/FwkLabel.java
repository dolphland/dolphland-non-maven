package com.dolphland.client.util.application.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * <p>
 * Extends JLabel that make provides shortcut for color, alignment, font style etc.
 * </p>
 *
 * @author JayJay
 */
public class FwkLabel extends JLabel {

    private Map<TextAttribute, Object> attributes = new HashMap<TextAttribute, Object>();



    @SuppressWarnings("unchecked") //$NON-NLS-1$
    public FwkLabel() {
        super();
        this.attributes = (Map<TextAttribute, Object>) getFont().getAttributes();
    }



    @SuppressWarnings("unchecked") //$NON-NLS-1$
    public FwkLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
        this.attributes = (Map<TextAttribute, Object>) getFont().getAttributes();
    }



    @SuppressWarnings("unchecked") //$NON-NLS-1$
    public FwkLabel(Icon image) {
        super(image);
        this.attributes = (Map<TextAttribute, Object>) getFont().getAttributes();
    }



    @SuppressWarnings("unchecked") //$NON-NLS-1$
    public FwkLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
        this.attributes = (Map<TextAttribute, Object>) getFont().getAttributes();
    }



    @SuppressWarnings("unchecked") //$NON-NLS-1$
    public FwkLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        this.attributes = (Map<TextAttribute, Object>) getFont().getAttributes();
    }



    @SuppressWarnings("unchecked") //$NON-NLS-1$
    public FwkLabel(String text) {
        super(text);
        this.attributes = (Map<TextAttribute, Object>) getFont().getAttributes();
    }



    @Override
    public void setFont(Font font) {
        super.setFont(new Font(font.getName(), font.getStyle(), font.getSize()).deriveFont(attributes));
    }



    public FwkLabel text(String text) {
        setText(text);
        return this;
    }



    public FwkLabel left() {
        setHorizontalAlignment(JLabel.LEFT);
        return this;
    }



    public FwkLabel center() {
        setHorizontalAlignment(JLabel.CENTER);
        return this;
    }



    public FwkLabel right() {
        setHorizontalAlignment(JLabel.RIGHT);
        return this;
    }



    public FwkLabel top() {
        setVerticalAlignment(JLabel.TOP);
        return this;
    }



    public FwkLabel middle() {
        setVerticalAlignment(JLabel.CENTER);
        return this;
    }



    public FwkLabel bottom() {
        setVerticalAlignment(JLabel.BOTTOM);
        return this;
    }



    public FwkLabel bold() {
        attributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
        setFont(getFont());
        return this;
    }



    public FwkLabel plain() {
        attributes.remove(TextAttribute.WEIGHT);
        setFont(getFont());
        return this;
    }



    public FwkLabel underline() {
        attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        setFont(getFont());
        return this;
    }



    public FwkLabel noUnderline() {
        attributes.remove(TextAttribute.UNDERLINE);
        setFont(getFont());
        return this;
    }



    public FwkLabel size(float size) {
        attributes.put(TextAttribute.SIZE, new Float(size));
        setFont(getFont());
        return this;
    }



    public FwkLabel italic() {
        attributes.put(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE);
        setFont(getFont());
        return this;
    }



    public FwkLabel noItalic() {
        attributes.put(TextAttribute.POSTURE, TextAttribute.POSTURE_REGULAR);
        setFont(getFont());
        return this;
    }



    public FwkLabel strike() {
        attributes.put(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON);
        setFont(getFont());
        return this;
    }



    public FwkLabel noStrike() {
        attributes.remove(TextAttribute.STRIKETHROUGH);
        setFont(getFont());
        return this;
    }



    public FwkLabel red() {
        return foreground(Color.RED);
    }



    public FwkLabel blue() {
        return foreground(Color.BLUE);
    }



    public FwkLabel green() {
        return foreground(Color.GREEN);
    }



    public FwkLabel yellow() {
        return foreground(Color.YELLOW);
    }



    public FwkLabel orange() {
        return foreground(Color.ORANGE);
    }



    public FwkLabel black() {
        return foreground(Color.BLACK);
    }



    public FwkLabel white() {
        return foreground(Color.WHITE);
    }



    public FwkLabel gray() {
        return foreground(Color.GRAY);
    }



    public FwkLabel lightGray() {
        return foreground(Color.LIGHT_GRAY);
    }



    public FwkLabel darjGray() {
        return foreground(Color.DARK_GRAY);
    }



    public FwkLabel cyan() {
        return foreground(Color.CYAN);
    }



    public FwkLabel magenta() {
        return foreground(Color.MAGENTA);
    }



    public FwkLabel background(Color color) {
        setOpaque(true);
        setBackground(color);
        return this;
    }



    public FwkLabel transparent() {
        setOpaque(false);
        return this;
    }



    public FwkLabel opaque() {
        setOpaque(false);
        return this;
    }



    public FwkLabel foreground(Color color) {
        setForeground(color);
        return this;
    }

}
