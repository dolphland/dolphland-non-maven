/*
 * $Log: CEnvironement.java,v $
 * Revision 1.6 2014/03/10 16:07:11 bvatan
 * - Internationalized
 * Revision 1.5 2013/06/17 16:11:01 bvatan
 * - FWK78: added extractProperies() shortcut for
 * getApplication().extarctproperties(this)
 * Revision 1.4 2012/08/10 13:32:27 bvatan
 * - added sage getter for nested properties and conf.
 * - javadoc
 * Revision 1.3 2009/03/12 08:05:21 bvatan
 * - correction bug, ne levait pas d'exception get getNestedProperty(String) si
 * le conf n'existait pas
 * Revision 1.2 2009/02/26 09:55:57 bvatan
 * - ajotu de raccourci pour atteindre les conf et property � partir d'un chemin
 * d'acc�s hi�rarchique
 * Revision 1.1 2008/11/27 07:38:33 bvatan
 * - initial check in
 */

package com.dolphland.client.util.conf;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.dolphland.client.util.exception.AppException;
import com.dolphland.client.util.string.StrUtil;

/**
 * <p>
 * Holds data and access services for an <code>&lt;env&gt;</code> tag of the
 * application (<code>application.xml</code>) configuration file.
 * </p>
 * 
 * @author JayJay
 */
public class CEnvironement {

    private CApplication application;

    private String id;

    private Map<String, CProperty> properties;

    private Map<String, CConf> confs;



    public CEnvironement() {
        properties = new TreeMap<String, CProperty>();
        confs = new TreeMap<String, CConf>();
    }



    public void setId(String id) {
        this.id = id;
    }



    public String getId() {
        return id;
    }



    public void setApplication(CApplication application) {
        this.application = application;
    }



    public CApplication getApplication() {
        return application;
    }



    public void addProperty(CProperty property) {
        properties.put(property.getName(), property);
    }



    public List<CProperty> getProperties() {
        return new ArrayList<CProperty>(properties.values());
    }



    public CProperty getProperty(String name) {
        return properties.get(name);
    }



    public void addConf(CConf conf) {
        confs.put(conf.getId(), conf);
    }



    public List<CConf> getConfs() {
        return new ArrayList<CConf>(confs.values());
    }



    public CConf getConf(String id) {
        return confs.get(id);
    }



    public int getInt(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? 0 : p.getInt();
    }



    public long getLong(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? 0 : p.getLong();
    }



    public float getFloat(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? 0 : p.getFloat();
    }



    public double getDouble(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? 0 : p.getDouble();
    }



    public boolean getBoolean(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? false : p.getBoolean();
    }



    public String getString(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? null : p.getString();
    }



    public File getFile(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? null : p.getFile();
    }



    public URL getUrl(String path) {
        CProperty p = getNestedProperty(path);
        return p == null ? null : p.getUrl();
    }



    public CProperty getNestedProperty(String path) {
        String lastParent = getLastParent(path);
        if (lastParent == null) {
            return getProperty(path);
        } else {
            CConf conf = getNestedConf(lastParent);
            if (conf == null) {
                throw new AppException("No such conf :" + lastParent); //$NON-NLS-1$
            }
            return conf.getProperty(getLeaf(path));
        }
    }



    /**
     * <p>
     * Returns the property whose path is specified.
     * </p>
     * <p>
     * If the property could not be found, null is returned.
     * </p>
     * 
     * @param path
     *            The path to the requested property.
     * @return The requested property or null if not found.
     */
    public CProperty getSafeNestedProperty(String path) {
        String lastParent = getLastParent(path);
        if (lastParent == null) {
            return getProperty(path);
        } else {
            CConf conf = getSafeNestedConf(lastParent);
            if (conf == null) {
                return null;
            }
            return conf.getProperty(getLeaf(path));
        }
    }



    private String getLastParent(String path) {
        if (StrUtil.isEmpty(path)) {
            throw new IllegalArgumentException("path is null or empty !"); //$NON-NLS-1$
        }
        path = path.trim();
        if (path.startsWith(".") || path.endsWith(".")) { //$NON-NLS-1$ //$NON-NLS-2$
            throw new IllegalArgumentException("path cannot starts or ends with a dot ! (" + path + ")"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        int idx = path.lastIndexOf('.');
        if (idx < 0) {
            return null;
        }
        return path.substring(0, idx);
    }



    private String getLeaf(String path) {
        if (StrUtil.isEmpty(path)) {
            throw new IllegalArgumentException("path is null or empty !"); //$NON-NLS-1$
        }
        path = path.trim();
        if (path.startsWith(".") || path.endsWith(".")) { //$NON-NLS-1$ //$NON-NLS-2$
            throw new IllegalArgumentException("path cannot starts or ends with a dot ! (" + path + ")"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        int idx = path.lastIndexOf('.');
        if (idx < 0) {
            return path;
        }
        return path.substring(idx + 1);
    }



    /**
     * <p>
     * Returns the specified nested conf or null if not found.
     * </p>
     * 
     * @param path
     *            The path to the requested conf element.
     * @return The conf element found or null if the specified path did not lead
     *         to a conf element or does not exists.
     */
    public CConf getSafeNestedConf(String path) {
        try {
            return getNestedConf(path);
        } catch (AppException ve) {
            return null;
        }
    }



    public CConf getNestedConf(String path) {
        if (StrUtil.isEmpty(path)) {
            throw new IllegalArgumentException("path is null or empty !"); //$NON-NLS-1$
        }
        path = path.trim();
        if (path.startsWith(".") || path.endsWith(".")) { //$NON-NLS-1$ //$NON-NLS-2$
            throw new IllegalArgumentException("path cannot starts or ends with a dot ! (" + path + ")"); //$NON-NLS-1$ //$NON-NLS-2$
        }
        String[] ids = path.split("\\."); //$NON-NLS-1$
        CConf conf = null;
        StringBuffer currentPath = new StringBuffer();
        for (int i = 0; i < ids.length; i++) {
            String id = ids[i];
            if (StrUtil.isEmpty(id)) {
                continue;
            }
            CConf oldConf = conf;
            if (i == 0) {
                conf = getConf(id);
            } else {
                conf = conf.getConf(id);
            }
            if (i > 0) {
                currentPath.append("."); //$NON-NLS-1$
            }
            currentPath.append(ids[i]);
            if (conf == null) {
                if (oldConf != null) {
                    if (oldConf.getProperty(id) != null) {
                        throw new AppException("No such conf [" + path + "], " + currentPath.toString() + " is a property !"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    } else {
                        throw new AppException("No such conf [" + path + "], failed when fetching " + currentPath.toString()); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                } else {
                    if (getProperty(id) != null) {
                        throw new AppException("No such conf [" + path + "], " + currentPath.toString() + " is a property !"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
                    } else {
                        throw new AppException("No such conf [" + path + "], failed when fetching " + currentPath.toString()); //$NON-NLS-1$ //$NON-NLS-2$
                    }
                }
            }
        }
        return conf;
    }



    /**
     * <p>
     * Return this environement representation as a {@link Map}
     * </p>
     * 
     * @return A {@link Map} that represnets this environement as properties.
     */
    public Map<String, String> extractProperties() {
        return application.extractProperties(this);
    }



    @Override
    public String toString() {
        return "(Environement:id=" + id + ")"; //$NON-NLS-1$ //$NON-NLS-2$
    }

}
