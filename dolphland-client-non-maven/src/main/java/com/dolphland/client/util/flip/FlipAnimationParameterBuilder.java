package com.dolphland.client.util.flip;

import java.awt.Point;
import java.util.List;

import com.dolphland.client.util.action.UIAction;
import com.dolphland.client.util.animator.AnimationActionEvt;

/**
 * Used to build a {@link FlipAnimationParameter}
 * 
 * @author jeremy.scafi
 * 
 */
public class FlipAnimationParameterBuilder {

    private FlipAnimationParameter parameter;



    /**
     * Default constructor with {@link FlipAnimationType} specified
     * 
     * @param typeAction
     *            {@link FlipAnimationType}
     */
    public FlipAnimationParameterBuilder(FlipAnimationType typeAction) {
        parameter = new FlipAnimationParameter(typeAction);
    }



    /**
     * Get {@link FlipAnimationType}
     * 
     * @return {@link FlipAnimationType}
     */
    public FlipAnimationParameter get() {
        return parameter;
    }



    /**
     * @param selectedApplicationId
     *            the selectedApplicationId to set
     */
    public FlipAnimationParameterBuilder selectedApplicationId(String selectedApplicationId) {
        parameter.setSelectedApplicationId(selectedApplicationId);
        return this;
    }



    /**
     * @param inAttachPoint
     *            the inAttachPoint to set
     */
    public FlipAnimationParameterBuilder inAttachPoint(Point inAttachPoint) {
        parameter.setInAttachPoint(inAttachPoint);
        return this;
    }



    /**
     * @param outAttachPoint
     *            the outAttachPoint to set
     */
    public FlipAnimationParameterBuilder outAttachPoint(Point outAttachPoint) {
        parameter.setOutAttachPoint(outAttachPoint);
        return this;
    }



    /**
     * @param stepsPolicy
     *            the stepsPolicy to set
     */
    public FlipAnimationParameterBuilder stepsPolicy(Integer stepsPolicy) {
        parameter.setStepsPolicy(stepsPolicy);
        return this;
    }



    /**
     * @param actionsBefore
     *            the actionsBefore to set
     */
    public FlipAnimationParameterBuilder actionsBefore(List<UIAction<AnimationActionEvt, ?>> actionsBefore) {
        parameter.setActionsBefore(actionsBefore);
        return this;
    }



    /**
     * @param actionsAfter
     *            the actionsAfter to set
     */
    public FlipAnimationParameterBuilder actionsAfter(List<UIAction<AnimationActionEvt, ?>> actionsAfter) {
        parameter.setActionsAfter(actionsAfter);
        return this;
    }



    /**
     * @param animationDurationMs
     *            the animationDurationMs to set
     */
    public FlipAnimationParameterBuilder animationDurationMs(Long animationDurationMs) {
        parameter.setAnimationDurationMs(animationDurationMs);
        return this;
    }

}
