package com.dolphland.client.util.chart;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;

/**
 * This class represents a chart of scatter plot using the API JFreeChart with
 * an improved legend.
 * 
 * The main improvement of this legend is to check for certain series do not
 * appear on the chart.
 * 
 * The chart's representation is done with the JComponent componentChart that
 * can get by getChartComponent ()
 * 
 * Possibility to define:
 * - data entities. A data entity is identified by a Comparable consisting of
 * values (abscissa, ordinate)
 * - horizontal markers. A horizontal marker can distinguish one area in an
 * abscissa interval or in an abscissa value. The distinction is made by the
 * text and / or by a color
 * - vertical markers. A vertical marker can distinguish one area in an ordinate
 * interval or in an ordinate value. The distinction is made by the text and /
 * or by a color
 * - fixed values. Data set containing only one value. Fixed values are used to
 * represent a limit.
 * 
 * The chart's configuration is done with a ChartConstraint where it is possible
 * to use a builder (ChartConstraintBuilder)
 * 
 * Concrete example of creating a ScatterPlotChat:
 * 
 * VisionScatterPlotChart chart = new
 * VisionScatterPlotChart(ChartConstraintsBuilder.create()
 * .axeXTitle("Position")
 * .axeYTitle("Thickness (mm)")
 * .axeYRangeAutoPackable(true)
 * .menuItemVisible(ChartConstraints.MENU_ITEM_PROPRIETES, false)
 * .menuItemVisible(ChartConstraints.MENU_ITEM_ECHELLE_AUTOMATIQUE, false)
 * .zoomWithClickEnabled(false)
 * .get()
 * );
 * 
 * @author jeremy.scafi
 */
public class ScatterPlotChart extends AbstractScatterPlotChart {

    /**
     * Default constructor
     */
    public ScatterPlotChart() {
        super();
    }



    /**
     * Constructor with chart constraints
     * 
     * @param chartConstraints
     *            Chart constraints
     */
    public ScatterPlotChart(ChartConstraints chartConstraints) {
        super(chartConstraints);
    }



    /**
     * Constructor with chart component
     * 
     * @param componentChart
     *            Chart component
     */
    public ScatterPlotChart(JPanel componentChart) {
        super(componentChart);
    }



    /**
     * Constructor with chart component and chart constraints
     * 
     * @param componentChart
     *            Chart component
     * @param chartConstraints
     *            Chart constraints
     */
    public ScatterPlotChart(JPanel componentChart, ChartConstraints chartConstraints) {
        super(componentChart, chartConstraints);
    }



    /** Create the chart */
    protected JFreeChart createChart() {
        boolean legend = false;
        boolean tooltips = false;
        boolean urls = false;
        PlotOrientation orientation = (getChartConstraints().isHorizontal()) ? PlotOrientation.HORIZONTAL : PlotOrientation.VERTICAL;

        JFreeChart chart = ChartFactory.createScatterPlot(
            getChartConstraints().getChartTitle(),
            getChartConstraints().getAxeXTitle(),
            getChartConstraints().getAxeYTitle(),
            getPlotDataSet(),
            orientation,
            legend,
            tooltips,
            urls);

        // Get plot
        XYPlot plot = chart.getXYPlot();

        // Add plot data set
        plot.setDataset(0, getPlotDataSet());
        plot.setRenderer(0, new LineAndShapeSelectionRenderer(plot.getDataset(0), false, true));

        // Add line data set
        plot.setDataset(1, getDataSet());
        plot.setRenderer(1, new LineAndShapeSelectionRenderer(plot.getDataset(1), true, false));

        return chart;
    }



    @Override
    protected ChartSelection getChartSelection() {
        return (ChartSelection) getChart().getXYPlot().getRenderer(0);
    }
}
