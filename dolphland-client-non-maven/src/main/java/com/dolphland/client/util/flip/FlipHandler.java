package com.dolphland.client.util.flip;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import com.dolphland.client.util.action.UIActionUtil;
import com.dolphland.client.util.animator.AnimatedComponent;
import com.dolphland.client.util.animator.AnimatedPanel;
import com.dolphland.client.util.animator.AnimationParameter;
import com.dolphland.client.util.assertion.AssertUtil;
import com.dolphland.client.util.string.StrUtil;

/**
 * Handler for a flip's panel and its animator
 * 
 * @author JayJay
 * 
 */
class FlipHandler implements MouseMotionListener, MouseListener {

    private FlipPanel flipPanel;
    private AnimatedPanel flipAnimatedPanel;
    private FlipAnimator flipAnimator;

    private String selectedApplicationId = null;
    private boolean mostCenteredApplicationSelected = false;
    private boolean directSelection = false;



    /**
     * Constructor with flip's panel, its animated panel and its animator
     * 
     * @param flipPanel
     *            Flip's panel
     * @param flipAnimatedPanel
     *            Flip's animated panel
     * @param flipAnimator
     *            Flip's animator
     */
    public FlipHandler(FlipPanel flipPanel, AnimatedPanel flipAnimatedPanel, FlipAnimator flipAnimator) {
        AssertUtil.notNull(flipPanel, "flipPanel is null !");
        AssertUtil.notNull(flipAnimatedPanel, "flipAnimatedPanel is null !");
        AssertUtil.notNull(flipAnimator, "flipAnimator is null !");
        this.flipPanel = flipPanel;
        this.flipAnimatedPanel = flipAnimatedPanel;
        this.flipAnimator = flipAnimator;
    }



    @Override
    public void mouseClicked(MouseEvent e) {
        // Get selected application (nearest application from the current
        // mouse's position)
        Component selectedApplication = getNearestApplication(flipAnimatedPanel, e.getPoint());
        if (selectedApplication != null) {
            if (StrUtil.isEmpty(flipPanel.getMaximizedApplicationId())) {
                String applicationId = ((AnimatedComponent) selectedApplication).getId();
                FlipApplicationAdapter application = flipPanel.getApplication(applicationId);
                // If maximize is disabled, do nothing
                if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationMaximize().getActionsBefore(), flipAnimatedPanel.getAnimatedComponent(applicationId))) {
                    return;
                }
                if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationMaximize().getActionsAfter(), flipAnimatedPanel.getAnimatedComponent(applicationId))) {
                    return;
                }
                // Else, maximize the application
                flipAnimator.executeAnimations(application.getFlipAnimationMaximize());
            }
        }
    }



    @Override
    public void mousePressed(MouseEvent e) {
        if (StrUtil.isEmpty(flipPanel.getMaximizedApplicationId())) {
            // Get selected application (nearest application from the current
            // mouse's position)
            Component selectedApplication = getNearestApplication(flipAnimatedPanel, e.getPoint());
            if (selectedApplication != null) {
                // Get the most centered application
                Component mostCenteredApplication = getMostCenteredApplication();

                // Set the indicator for if selected application is the most
                // centered
                mostCenteredApplicationSelected = mostCenteredApplication == selectedApplication;

                // Set the indicator for if it's a direct selection
                directSelection = flipAnimatedPanel.getComponentAt(e.getPoint()) == selectedApplication;

                // Store the id from the selected application
                selectedApplicationId = getApplicationId(selectedApplication);
            }
        }
    }



    @Override
    public void mouseReleased(MouseEvent e) {
        if (StrUtil.isEmpty(flipPanel.getMaximizedApplicationId()) && selectedApplicationId != null && (e.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
            // Get the most centered application
            Component mostCenteredApplication = getMostCenteredApplication();

            // Center the application
            if (mostCenteredApplication != null) {
                flipAnimator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
                    .selectedApplicationId(getApplicationId(mostCenteredApplication))
                    .get());
            }
        }
        selectedApplicationId = null;
    }



    @Override
    public void mouseDragged(MouseEvent e) {
        if (selectedApplicationId != null && (e.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
            // Do nothing if mouse is out of the component
            if (e.getX() < 0 || e.getY() < 0 || e.getX() > ((Component) e.getSource()).getWidth() || e.getY() > ((Component) e.getSource()).getHeight()) {
                // If the most centered application is directly selected and the
                // mouse is out of top of screen, remove application if possible
                if (e.getY() < 0 && mostCenteredApplicationSelected && directSelection) {
                    FlipApplicationAdapter application = flipPanel.getApplication(selectedApplicationId);
                    // If hide is disabled, do nothing
                    if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationHide().getActionsBefore(), flipAnimatedPanel.getAnimatedComponent(selectedApplicationId))) {
                        return;
                    }
                    if (!UIActionUtil.isActionsEnabled(application.getFlipAnimationHide().getActionsAfter(), flipAnimatedPanel.getAnimatedComponent(selectedApplicationId))) {
                        return;
                    }
                    // Else, hide the application
                    flipAnimator.executeAnimations(application.getFlipAnimationHide());
                    selectedApplicationId = null;
                }
                return;
            }

            if (mostCenteredApplicationSelected && directSelection) {
                // If the most centered application is directly selected, move
                // vertically the application
                flipAnimator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_VERTICALLY_SELECTED_APPLICATION)
                    .selectedApplicationId(selectedApplicationId)
                    .inAttachPoint(e.getPoint())
                    .stepsPolicy(AnimationParameter.ONLY_ONE_STEP)
                    .get());
            } else {
                // Else, move horizontally the application
                flipAnimator.executeAnimations(new FlipAnimationParameterBuilder(FlipAnimationType.MOVE_HORIZONTALLY_ALL_APPLICATIONS)
                    .selectedApplicationId(selectedApplicationId)
                    .inAttachPoint(e.getPoint())
                    .stepsPolicy(AnimationParameter.ONLY_ONE_STEP)
                    .get());
            }
        }
    }



    /**
     * Get application's id
     * 
     * @param application
     * 
     * @return Application's id
     */
    private String getApplicationId(Component application) {
        for (String applicationId : flipAnimatedPanel.getAllAnimatedComponentsIds()) {
            if (application == flipAnimatedPanel.getAnimatedComponent(applicationId)) {
                return applicationId;
            }
        }
        return null;
    }



    /**
     * Get the application most centered
     * 
     * @return Application most centered
     */
    private Component getMostCenteredApplication() {

        // Get flip's panel center point
        Point flipCenterPoint = new Point(flipAnimatedPanel.getWidth() / 2, flipAnimatedPanel.getHeight() / 2);

        // Get the nearest application from the flip's panel center point
        return getNearestApplication(flipAnimatedPanel, flipCenterPoint);
    }



    /**
     * Get the nearest application about a point and a component parent
     * 
     * @param compParent
     *            Component parent
     * @param point
     *            Point
     * 
     * @return Nearest application
     */
    private Component getNearestApplication(Component compParent, Point point) {
        Component result = null;

        result = compParent.getComponentAt(point);
        if (result != null && result != compParent) {
            return result;
        }

        boolean crossedLeftLimit = false;
        boolean crossedRightLimit = false;
        int x = 0;
        int hGap = 0;

        while (!crossedLeftLimit || !crossedRightLimit) {

            // Look to the top left
            x = (int) point.getX() - hGap;
            if (x < 1) {
                crossedLeftLimit = true;
            } else {
                for (int y = 1; y <= compParent.getHeight(); y++) {
                    result = compParent.getComponentAt(x, y);
                    if (result != null && result != compParent) {
                        return result;
                    }
                }
            }

            // Look to the top right
            x = (int) point.getX() + hGap;
            if (x > compParent.getWidth()) {
                crossedRightLimit = true;
            } else {
                for (int y = 1; y <= compParent.getHeight(); y++) {
                    result = compParent.getComponentAt(x, y);
                    if (result != null && result != compParent) {
                        return result;
                    }
                }
            }

            hGap++;
        }

        return null;
    }



    @Override
    public void mouseEntered(MouseEvent e) {
    }



    @Override
    public void mouseExited(MouseEvent e) {
    }



    @Override
    public void mouseMoved(MouseEvent e) {
    }
}
