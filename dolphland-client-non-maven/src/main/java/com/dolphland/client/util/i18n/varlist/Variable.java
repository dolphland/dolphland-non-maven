package com.dolphland.client.util.i18n.varlist;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.util.Assert;

/**
 * <p>
 * Holds information relative a a variable that has been parsed by
 * {@link VarParser}
 * </p>
 * <p>
 * This class mainly holds information of type, name and value of a variable
 * </p>
 */
public class Variable {

    private VarType type;

    private String name;

    private Object value;



    public Variable(VarType type, String name, Object value) {
        Assert.notNull(type, "type cannot be null !"); //$NON-NLS-1$
        Assert.notNull(name, "name cannot be null !"); //$NON-NLS-1$
        this.type = type;
        this.name = name;
        this.value = value;
    }



    /**
     * Get this variable's type
     */
    public VarType getType() {
        return type;
    }



    /**
     * Get this variable's name
     */
    public String getName() {
        return name;
    }



    /**
     * <p>
     * Get this variable's value
     * </p>
     * <p>
     * The value class depends on the variable type :<br>
     * <br>
     * <table border=1>
     * <tr>
     * <td>Variable type</td>
     * <td>Corresponding value class</td>
     * </tr>
     * <tr>
     * <td>string</td>
     * <td>{@link String}</td>
     * </tr>
     * <tr>
     * <td>int</td>
     * <td>{@link BigDecimal}</td>
     * </tr>
     * <tr>
     * <td>float</td>
     * <td>{@link BigDecimal}</td>
     * </tr>
     * <tr>
     * <td>char</td>
     * <td>{@link String}</td>
     * </tr>
     * <tr>
     * <td>date</td>
     * <td>{@link Date}</td>
     * </tr>
     * </table>
     * </p>
     */
    public Object getValue() {
        return value;
    }



    @Override
    public String toString() {
        return type.name().toLowerCase() + " " + name + "=" + (value == null ? "null" : value.toString()); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }

}
